#! /usr/bin/ksh
#*******************************************************************
# Copyright 2012 Virtual Trader Ltd. All Rights Reserved. 
#
# vt_concurrent.sh
#
# Main VT APPS installation shell script file. Make sure executable!
# 
# Author: NCM
# Date:   06/03/2012
#*******************************************************************
BASENAME=vt_concurrent
LOGFILE=$PWD/l_${BASENAME}.log
DIR11=$PWD/ddl/apps/11.5.0
DIR12=$PWD/ddl/apps/12.0.0
RETRY_COUNT=1


# Remove any existing log file hanging around

rm -f $LOGFILE
# Generic function to write log data away for the running script
function write_log
{
  WRITE_STRING=$1
  DATE_TIME_VAR=`date +%F--%T`

  echo "${DATE_TIME_VAR} ${WRITE_STRING}" >> $LOGFILE
  echo "${WRITE_STRING}"
}

# Generic function to take in a given Oracle password and
# test that it is valid by creating a dummy SQL file and
# silently executing it. Allows 3 attempts and then aborts.
function get_pwd
{
  # Counter for the number of attempts at the password.
  let RETRY_COUNT=0
  # Flag to indicate whether successful attempt or not.
  VERIFIED=FALSE
  # Type of password that is coming in from the parameter - APPS or XXCP
  PTYPE=$1

  # Create the dummy file to execute
  #echo "exit;" > ./tmp.sql

  write_log "Please enter ${PTYPE} password"

  # Switch off the display to the screen of what the user enters.
  stty -echo

  # Loop 3 times for the password entry.
  while [[ $RETRY_COUNT -lt 3 && "$VERIFIED" = "FALSE" ]]
  do
    read USER_PWD
    if [ -z $USER_PWD ]
    then
      write_log "******************************"
      write_log "No password entered. Aborting."
      write_log "******************************"
      # Clear up the temp file.
      rm -f ./tmp.sql
      # Switch back on the user input to the screen.
      stty echo
      tidy_exit 2
    fi

    echo "WHENEVER SQLERROR EXIT 1;
          connect $PTYPE/$USER_PWD;
          exit;
         " > ./tmp.sql

    STATUS=$?
    if [ $STATUS -ne 0 ]
    then
      write_log "*****************************************************"
      write_log "ERROR: Unable to write ./tmp.sql temporary file."
      write_log "Please check permission on the current location and retry"
      write_log "*****************************************************"
      tidy_exit 20
    fi

    # Execute the the temporary file with the credentials given and return
    # the value back to a variable.
    RESULT=`sqlplus /nolog @./tmp.sql`

    # Test whether a successful execution.
    if login_failure "$RESULT"
    then
      let RETRY_COUNT=$RETRY_COUNT+1
      if [ $RETRY_COUNT -lt 3 ]
      then
        write_log "${PTYPE} password entered is incorrect. Please try again."
      fi
    else
      write_log "${PTYPE} Password Verified."
      VERIFIED=TRUE
    fi
  done

  # Switch back on the user input to the screen.
  stty echo

  rm -f ./tmp.sql

  if [ $RETRY_COUNT -eq 3 ]
  then
    write_log "**********************************************"
    write_log "Unsuccessful entry of ${PTYPE} password. Aborting."
    write_log "**********************************************"
    write_log
    tidy_exit 3
  fi
}

# Generic function to retrieve the Oracle Apps version
function get_apps_version
{

APPS_VER=`sqlplus -s apps/$APPS_PWD <<END
set feedback off;
set heading off;
select substr(release_name,1,2) from fnd_product_groups;
EXIT;
END`
APPS_VER=`echo $APPS_VER | tr [1] [1]`

  if [ "$APPS_VER" = "11" ]
  then
    write_log
    write_log "Oracle Apps 11i detected."
    APPS_VER_LONG="11.5.10"
    ORACLE_VERSION="11.5.10"
    R12_INSTALL="N"
    TEMPLATE_DIR=$DIR11/templates
    APPS_DIR=$DIR11
  elif [ "$APPS_VER" = "12" ]
  then
    write_log
    write_log "Oracle Apps R12 detected"
    APPS_VER_LONG="12.0.0"
    ORACLE_VERSION="12.0.0"
    TEMPLATE_DIR=$DIR12/templates
    APPS_DIR=$DIR12
  else
    write_log
    write_log "Unable to determine Oracle Apps version. Exiting..."
    tidy_exit 2
  fi  
}  

# Generic function to test whether the connection to Oracle was successful
# given the password that was entered.

function login_failure
{
 ERRTXT=`echo "$1" | grep "ORA-"`
 
 if [ "$ERRTXT" != "" ]
 then
   return 0
 else
   return 1
 fi 
}

# Function to test whether the environment is set-up correctly for
# doing a tnsping to an instance for ultimately deriving the JDBC string.
function check_env
{
  VALID=Y
  if [ -z "$TWO_TASK" ]
  then
    VALID=N
  fi
  
  write_log "Attempting to derive JDBC connection from tnsping"
  write_log "Testing tnsping to ${TWO_TASK}"
  
  PINGTEST=`tnsping $TWO_TASK`
  STATUS=$?
  if [ $STATUS -ne 0 ]
  then
    write_log "tnsping test failed - reverting to manual entry"
    VALID=N
  fi

  if [ "$VALID" = "N" ]
  then
    write_log "Please enter the known JDBC connection string"
    write_log "Usually host:port:instance"
    write_log "E.g. server.com:1521:orcl"
    read JDBC_CON
    JDBC_GLOBAL=$JDBC_CON
  fi
}

# Generic function to derive the JDBC connection string
function get_jdbc_string
{
  TNSPING=`tnsping $TWO_TASK | grep -i attemp`
  HOST_PORT_POS=`echo $TNSPING | awk '{print match($0, "PORT")}'`
  let START_POS=$HOST_PORT_POS+5
  let END_POS=$START_POS+3
  HOST_PORT=`echo $TNSPING | cut -c$START_POS-$END_POS`

  HOST_POS=`echo $TNSPING | awk '{print match($0, "HOST")}'`

  let START_HOST_POS=$HOST_POS+5
  let END_HOST_POS=$START_POS-8

  HOST=`echo $TNSPING | cut -c$START_HOST_POS-$END_HOST_POS`

  JDBC_FOUND="${HOST}:${HOST_PORT}:${TWO_TASK}"

  write_log
  write_log "The JDBC connection string has been derived as:"
  write_log "${JDBC_FOUND}"
  write_log
  while [[ "$JDBC_ANS" != "Y" && "$JDBC_ANS" != "N" ]]
  do
    write_log "Is this correct? (Y/N)"
    read JDBC_ANS
    JDBC_ANS=`echo $JDBC_ANS | tr [yn] [YN]`
  done

  if [ "$JDBC_ANS" = "Y" ]
  then
    JDBC_CON=$JDBC_FOUND
    JDBC_GLOBAL=$JDBC_FOUND
  else
    write_log "Please enter the known JDBC connection string"
    write_log "Usually host:port:instance"
    read JDBC_CON
    JDBC_GLOBAL=$JDBC_CON
  fi
}

# Generic function to display exit codes from the script
function tidy_exit
{
  write_log "${BASENAME} exited with status ${1}"
  stty echo
  exit $1 
}

clear
write_log " "
write_log "**************************************************"
write_log "Virtual Trader Application Load   (Version 3.4.3) "
write_log "**************************************************"
write_log "This script installs the Application into Oracle."
write_log "This includes menus, concurrent programs and BI Publisher."
write_log " "

if [ -z "$DB_NAME" ]
then
  # Try to detect the name of the database
  if [ -z "$TWO_TASK" ]
  then
    write_log "Please enter the name of the database"
    read DB_INSTALL
    TWO_TASK=$DB_INSTALL
    export TWO_TASK
  else
    DB_INSTALL=$TWO_TASK
    # Make the database name uppercase to stand out for display to the user.
    DB_INSTALL=`echo $DB_INSTALL| tr '[a-z]' '[A-Z]'`
    write_log "Database environment has been detected as ${DB_INSTALL}."
    # Only allow Y or N answers
    while [[ $DB_ANS != "Y" && $DB_ANS != "N" ]]
    do
      write_log "Is this correct? (Y/N)"
      read DB_ANS
      DB_ANS=`echo $DB_ANS | tr [yn] [YN]`
    done
    if [ "$DB_ANS" = "Y" ]
    then
      write_log 
    else
      write_log "************************************************************"
      write_log "Please set the environment to the correct database and retry"
      write_log "************************************************************"
      exit 100
    fi
  fi
fi
export DB_NAME=$TWO_TASK

# If there is no parameter entered, get the APPS password.
if [ $# -lt 1 ]
then
  # Call the generic password function
  get_pwd APPS
  APPS_PWD=$USER_PWD
else
  APPS_PWD=$1
fi

write_log

get_apps_version

rm *.log
rm *.out

# Detect which files are being used and upload them
if [ -f $DIR11/vt_menus_25.ldt ]
then
  write_log "Uploading Menus"
  FNDLOAD apps/$APPS_PWD 0 Y UPLOAD $FND_TOP/patch/115/import/afsload.lct $DIR11/vt_menus_25.ldt - CUSTOM_MODE=FORCE
else
  write_log "*** No Menus found to upload: ${DIR11}/vt_menus_25.ldt"
fi

if [ -f $DIR11/vt_concurrent_24.ldt ]
then
  write_log "Uploading Concurrent Programs"
  FNDLOAD apps/$APPS_PWD 0 Y UPLOAD $FND_TOP/patch/115/import/afcpprog.lct $DIR11/vt_concurrent_24.ldt - CUSTOM_MODE=FORCE
else
  write_log "*** No Concurrent Programs found to upload: ${DIR11}/vt_concurrent_24.ldt"
fi 

if [ -f $DIR11/vt_requestgrp_24.ldt ]
then
  write_log "Uploading Request Groups"
  FNDLOAD apps/$APPS_PWD 0 Y UPLOAD $FND_TOP/patch/115/import/afcpreqg.lct $DIR11/vt_requestgrp_24.ldt - CUSTOM_MODE=FORCE
else
  write_log "*** No Request Groups found to upload: ${DIR11}/vt_requestgrp_24.ldt"
fi
 
if [ "$APPS_VER" = "12" ]
then
  write_log "R12 Apps"
  
  if [ -f $DIR12/vt_concurrent_24_R12.ldt ] 
  then
    write_log "R12 Concurrent Programs"
    FNDLOAD apps/$APPS_PWD 0 Y UPLOAD $FND_TOP/patch/115/import/afcpprog.lct $DIR12/vt_concurrent_24_R12.ldt - CUSTOM_MODE=FORCE
  else
    write_log "*** No R12 Concurrent Programs found to upload: ${DIR12}/vt_concurrent_24_R12.ldt"
  fi
  
  if [ -f $DIR12/vt_requestgrp_24_R12.ldt ]
  then
    write_log "R12 Request Groups"
    FNDLOAD apps/$APPS_PWD 0 Y UPLOAD $FND_TOP/patch/115/import/afcpreqg.lct $DIR12/vt_requestgrp_24_R12.ldt - CUSTOM_MODE=FORCE
  else
    write_log "*** No R12 Request Groups found: ${DIR12}/vt_requestgrp_24_R12.ldt"
  fi
fi

# Check if there is a BI Publisher definition to upload
if [ -f $APPS_DIR/XMLPData.ldt ]
then
  write_log "Do you want to update the BI Publisher templates? (Y/N)"
  read BIPUB
  BIPUB=`echo $BIPUB | tr [yn] [YN]`
  if [ "$BIPUB" = 'Y' ]
  then  
    write_log "BI Publisher"
    FNDLOAD apps/$APPS_PWD 0 Y UPLOAD $XDO_TOP/patch/115/import/xdotmpl.lct $APPS_DIR/XMLPData.ldt - CUSTOM_MODE=FORCE

    # Use the function to derive and display the JDBC connection
    check_env
    if [ "$VALID" = "Y" ]
    then
      get_jdbc_string
    fi
     
    write_log "Uploading templates"
    # Insert template loads here.
    # Remember to change the LOB_CODE and FILE_NAME.
    java oracle.apps.xdo.oa.util.XDOLoader \
    UPLOAD \
    -DB_USERNAME apps \
    -DB_PASSWORD $APPS_PWD \
    -JDBC_CONNECTION $JDBC_CON \
    -LOB_TYPE TEMPLATE \
    -APPS_SHORT_NAME XXCP \
    -LOB_CODE XXCPTMP \
    -LANGUAGE en \
    -NLS_LANG American_America.UTF8 \
    -XDO_FILE_TYPE RTF \
    -FILE_NAME $TEMPLATE_DIR/xxcp_tmp.rtf \
    -CUSTOM_MODE FORCE    
  fi
fi
write_log " "
write_log "********************************"
write_log " ${BASENAME} Finished"
write_log " Log file is ${LOGFILE}"
write_log "********************************"
write_log " "



