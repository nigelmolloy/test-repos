#!/bin/ksh
clear

# Create a logfile and remove any old versions
LOGFILE=./frmcmp.log
RESTART_LOG=./frmres.log

GLOBAL_SUCCESS=TRUE
SUCC_COUNT=0
FAIL_COUNT=0
TOT_COUNT=`ls *.fmb | wc -l`
INC_COUNT=0
RESTART_FAIL=N

rm -f $LOGFILE
rm -f $RESTART_LOG

# Generic function to display exit codes from the script
function tidy_exit
{
  write_log "${MASTERNAME} exited with status ${1}"
  stty echo
  write_log
  exit $1
}

# Generic function to test whether the connection to Oracle was successful
# given the password that was entered.

function login_failure
{
 ERRTXT=`echo "$1" | grep "ORA-"`

 if [ "$ERRTXT" != "" ]
 then
   return 0
 else
   return 1
 fi
}

######################################################################
# write_log functions
# These should generally be used instead of the echo command. That way
# the values will get written to the screen and the log file for trace
# purposes
######################################################################

# Generic function to write the output to the screen and log file.
function write_log
{
  WRITE_STRING=$1
  DATE_TIME_VAR=`date +%F--%T`

  echo "${DATE_TIME_VAR} ${WRITE_STRING}" >> $LOGFILE
  echo "${WRITE_STRING}"
}

# Generic function to write output to the screen and log file, but
# don't include the date as part of it.
# _nd = no_date
function write_log_nd
{
  WRITE_STRING=$1

  echo "${WRITE_STRING}" >> $LOGFILE
  echo "${WRITE_STRING}"
}

# Generic function to write the output to the screen and log file.
# This version doesn't do a new line at the end.
# _sp = special
function write_log_sp
{
  WRITE_STRING=$1
  DATE_TIME_VAR=`date +%F--%T`

  echo "${DATE_TIME_VAR} ${WRITE_STRING}\c" >> $LOGFILE
  echo "${WRITE_STRING} \c"
}

# Generic function to write the output to just the log file and
# NOT the screen.
# _ns = no_screen
function write_log_ns
{
  WRITE_STRING=$1
  DATE_TIME_VAR=`date +%F--%T`

  echo "${DATE_TIME_VAR} ${WRITE_STRING}" >> $LOGFILE
}

# Generic function to retrieve the Oracle Apps version
function get_apps_version
{
APPS_VER=`sqlplus -s apps/$APPS_PWD <<END
set feedback off;
set heading off;
select substr(release_name,1,2) from fnd_product_groups;
EXIT;
END`

  if [ $APPS_VER = "11" ]
  then
    write_log "Oracle Apps 11i detected."
    APPS_VER_LONG="11.5.10"
    R12_INSTALL="N"
  elif [ $APPS_VER = "12" ]
  then
    write_log "Oracle Apps R12 detected"
    APPS_VER_LONG="12.0.0"
    R12_INSTALL="Y"
  else
    write_log "Unable to determine Oracle Apps version. Exiting..."
    tidyexit 2
  fi

  write_log "Forms directory identified as ${XXCP_TOP}/forms/US"
  if [ "$PWD" != "$XXCP_TOP/forms/US" ]
  then
    write_log "***Current directory is ${PWD}"
  fi

}

# Generic function to take in a given Oracle password and
# test that it is valid by creating a dummy SQL file and
# silently executing it. Allows 3 attempts and then aborts.
function get_pwd
{
  # Counter for the number of attempts at the password.
  let RETRY_COUNT=0
  # Flag to indicate whether successful attempt or not.
  VERIFIED=FALSE
  # Type of password that is coming in from the parameter - APPS or XXCP
  PTYPE=$1

  # Create the dummy file to execute
  #echo "exit;" > ./tmp.sql

  write_log "Please enter ${PTYPE} password"

  # Switch off the display to the screen of what the user enters.
  stty -echo

  # Loop 3 times for the password entry.
  while [[ $RETRY_COUNT -lt 3 && "$VERIFIED" = "FALSE" ]]
  do
    read USER_PWD
    if [ -z $USER_PWD ]
    then
      write_log "******************************"
      write_log "No password entered. Aborting."
      write_log "******************************"
      # Clear up the temp file.
      rm -f ./tmp.sql
      # Switch back on the user input to the screen.
      stty echo
      tidy_exit 2
    fi

    echo "WHENEVER SQLERROR EXIT 1;
          connect $PTYPE/$USER_PWD;
          exit;
         " > ./tmp.sql

    STATUS=$?
    if [ $STATUS -ne 0 ]
    then
      write_log "*****************************************************"
      write_log "ERROR: Unable to write ./tmp.sql temporary file."
      write_log "Please check permission on the current location and retry"
      write_log "*****************************************************"
      tidy_exit 20
    fi

    # Execute the the temporary file with the credentials given and return
    # the value back to a variable.
    RESULT=`sqlplus /nolog @./tmp.sql`

    # Test whether a successful execution.
    if login_failure "$RESULT"
    then
      let RETRY_COUNT=$RETRY_COUNT+1
      if [ $RETRY_COUNT -lt 3 ]
      then
        write_log "${PTYPE} password entered is incorrect. Please try again."
      fi
    else
      write_log "${PTYPE} Password Verified."
      VERIFIED=TRUE
    fi
  done

  # Switch back on the user input to the screen.
  stty echo

  rm -f ./tmp.sql

  if [ $RETRY_COUNT -eq 3 ]
  then
    write_log "**********************************************"
    write_log "Unsuccessful entry of ${PTYPE} password. Aborting."
    write_log "**********************************************"
    write_log
    tidy_exit 3
  fi
}

write_log  "***********************************************************************"
write_log  "** (C)opyright 2003-2011  Virtual Trader Ltd. All Rights Reserved.   **"
write_log  "**                                                                   **"
write_log  "** Release VT273.B2   Forms Compile                        21-NOV-11 **"
write_log  "***********************************************************************"

OpS=`uname -s`

write_log
write_log  "--- System OS ---"
write_log $OpS
write_log

# Try to detect the name of the database
if [ -z "$TWO_TASK" ]
then
  write_log "Please enter the name of the database"
  read DB_INSTALL
  write_log_ns $DB_INSTALL
  TWO_TASK=$DB_INSTALL
  export TWO_TASK
else
  DB_INSTALL=$TWO_TASK
  # Make the database name uppercase to stand out for display to the user.
  DB_INSTALL=`echo $DB_INSTALL| tr '[a-z]' '[A-Z]'`
  write_log "Database environment has been detected as ${DB_INSTALL}."
  write_log "Is this correct? (Y/N)"
  read DB_ANS
  write_log_ns $DB_ANS
  if [[ "$DB_ANS" = "Y" || "$DB_ANS" = "y" ]]
  then
    write_log
  else
    write_log "************************************************************"
    write_log "Please set the environment to the correct database and retry"
    write_log "************************************************************"
    exit 100
  fi
fi

# If there is no parameter entered, get the APPS password.
if [ $# -lt 1 ]
then
  # Call the generic password function
  get_pwd APPS
  APPS_PWD=$USER_PWD
else
  APPS_PWD=$1
fi

write_log
# Call function to get and display the Oracle Apps version
get_apps_version


if [ $OpS = "WINDOWS_NT" ]
then
  OS_CMD="ifcmp60"
  write_log "Windows NT Compile"
else
  if [[ $R12_INSTALL = "N" || $R12_INSTALL = "n" ]]
  then
    write_log "11i: Generating forms for Oracle Applications 11i"
    write_log "11i: using compilation command f60gen."
    write_log
    OS_CMD="f60gen"
  else
    write_log "R12: Generating forms for Oracle Applications R12"
    write_log "R12: using compilation command frmcmp_batch."
    write_log
    OS_CMD="frmcmp_batch"
  fi
fi


for FORM_FILE in `ls *.fmb`
do
  let INC_COUNT=INC_COUNT+1
  write_log "Compiling ${INC_COUNT} of ${TOT_COUNT} ${FORM_FILE}..."
  $OS_CMD $FORM_FILE userid=apps/$APPS_PWD batch=yes
  STATUS=$?
  if [ $STATUS -eq 1 ]
  then
    write_log "${FORM_FILE} failed compilation" >> $LOGFILE
    echo "${FORM_FILE}" >> $RESTART_LOG
    write_log "***Failed Compilation***"
    GLOBAL_SUCCESS="FALSE"
    let FAIL_COUNT=FAIL_COUNT+1
  else
    let SUCC_COUNT=SUCC_COUNT+1
    write_log "Success"
  fi
done

if [ $GLOBAL_SUCCESS = "FALSE" ]
then
  write_log "**************************************************"
  write_log "Generation Finished with errors."
  write_log "Total number of Forms to compile: ${TOT_COUNT}"
  write_log "Forms successfully compiled:      ${SUCC_COUNT}"
  write_log "Forms unsuccessfully compiled:    ${FAIL_COUNT}"
  write_log "**************************************************"
  write_log "The following Forms failed compilation:"
  cat $LOGFILE
  write_log "This information is stored in ${LOGFILE}"
  write_log
  write_log -n "Do you want to restart the failed modules to view error messages ? <Y/N> : "
  read RESTART_FAIL
  if [[ $RESTART_FAIL = "Y" || $RESTART_FAIL = "y" ]]
  then
    for RES_FORM in `cat $RESTART_LOG`
    do
      write_log -n "Start ${RES_FORM} compilation? <Y/N/A> : "
      read START_COMP
      if [[ $START_COMP = "Y" || $START_COMP = "y" ]]
      then
        $OS_CMD $RES_FORM userid=apps/$APPS_PWD
      elif [[ $START_COMP = "N" || $START_COMP = "n" ]]
      then
        write_log "Skipping $RES_FORM"
      elif [ $START_COMP = "A" || $START_COMP = "a" ]
      then
        break
      fi
    done
  fi
else
  write_log "**************************************************"
  write_log "Generation Finished successfully."
  write_log "Total number of Forms to compile:            ${TOT_COUNT}"
  write_log "Total number of Forms successfully compiled: ${INC_COUNT}"
  write_log "**************************************************"
fi


