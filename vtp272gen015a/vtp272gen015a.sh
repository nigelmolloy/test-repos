#! /usr/bin/ksh
#*******************************************************************
# Copyright 2012 Virtual Trader Ltd. All Rights Reserved.
# 
# Main VT installation shell script file. Make sure executable!
# 
# Author: MSS
# Date:   29/11/2012
#*******************************************************************

BASENAME=vtp272gen015a
MLOGDIR=$PWD/logs
DDLLOGS=$PWD/ddl/logs
BASE_DIR=$PWD
LOGFILE=$MLOGDIR/l_${BASENAME}.log
TARFILE=vt_logs.tar
RETRY_COUNT=1

# Remove any existing log file hanging around

rm -f $LOGFILE

# Generic function to write log data away for the running script
function write_log
{
  WRITE_STRING=$1
  DATE_TIME_VAR=`date +%F--%T`  
  
  echo "${DATE_TIME_VAR} ${WRITE_STRING}" >> $LOGFILE
  echo "${WRITE_STRING}"
}

# Generic function to write log data away for the running script
function write_resp_log
{
  WRITE_STRING=$1
  DATE_TIME_VAR=`date`  
  
  echo "${DATE_TIME_VAR} ${WRITE_STRING}" >> $LOGFILE
}

# Generic function to display exit codes from the script
function tidy_exit
{
  write_log "${BASENAME} exited with status ${1}"
  stty echo
  exit $1 
}

function check_env
{
  VALID=Y
  if [ -z "$TWO_TASK" ]
  then
    VALID=N
  fi

  PINGTEST=`tnsping $TWO_TASK`
  STATUS=$?
  if [ $STATUS -ne 0 ]
  then
    VALID=N
  fi

  if [ "$VALID" = "N" ]
  then
    write_log "Please enter the known JDBC connection string"
    write_log "Usually host:port:instance"
    read JDBC_CON
    JDBC_GLOBAL=$JDBC_CON
  fi
}

# Generic function to derive the JDBC connection string
function get_jdbc_string
{
  TNSPING=`tnsping $TWO_TASK | grep -i attemp`
  HOST_PORT_POS=`echo $TNSPING | awk '{print match($0, "PORT")}'`
  let START_POS=$HOST_PORT_POS+5
  let END_POS=$START_POS+3
  HOST_PORT=`echo $TNSPING | cut -c$START_POS-$END_POS`

  HOST_POS=`echo $TNSPING | awk '{print match($0, "HOST")}'`

  let START_HOST_POS=$HOST_POS+5
  let END_HOST_POS=$START_POS-8

  HOST=`echo $TNSPING | cut -c$START_HOST_POS-$END_HOST_POS`

  JDBC_FOUND="${HOST}:${HOST_PORT}:${TWO_TASK}"

  write_log
  write_log "The JDBC connection string has been derived as:"
  write_log "${JDBC_FOUND}"
  write_log
  while [[ "$JDBC_ANS" != "Y" && "$JDBC_ANS" != "N" ]]
  do
    write_log "Is this correct? (Y/N)"
    read JDBC_ANS
    JDBC_ANS=`echo $JDBC_ANS | tr [yn] [YN]`
  done

  if [ "$JDBC_ANS" = "Y" ]
  then
    JDBC_CON=$JDBC_FOUND
    JDBC_GLOBAL=$JDBC_FOUND
  else
    write_log "Please enter the known JDBC connection string"
    write_log "Usually host:port:instance"
    read JDBC_CON
    JDBC_GLOBAL=$JDBC_CON
  fi
}


# Generic function to create sub-directories where needed for logs
function log_directory_check
{
  
  if [ ! -d ./logs ]
  then
    mkdir logs
  fi 
  
}

function create_tarball
{
  write_log "Looking for log files"
  FILECOUNT=`ls $MLOGDIR/*.log 2>/dev/null | wc -l`
  write_log "Found ${FILECOUNT} in ${MLOGDIR}"
  FILECOUNT=`ls $PWD/*.log 2>/dev/null  | wc -l`
  write_log "Found ${FILECOUNT} in ${PWD}"
  FILECOUNT=`ls $DDLLOGS/*.log 2>/dev/null | wc -l`
  write_log "Found ${FILECOUNT} in ${DDLLOGS}"
  
  tar -cvf $MLOGDIR/$TARFILE $MLOGDIR/*.log $DDLLOGS/*.log $PWD/*.log 2>/dev/null

  STATUS=$?
  if [ $STATUS -ne 0 ]
  then
    write_log "Unable to create ${TARFILE} - please create manually and send to Virtual Trader"
    tidy_exit 10
  fi
  
}

# Generic function to test whether the connection to Oracle was successful
# given the password that was entered.

function login_failure
{
 ERRTXT=`echo "$1" | grep "ORA-"`
 
 if [ "$ERRTXT" != "" ]
 then
   return 0
 else
   return 1
 fi 
}

# Generic function to check the return status of a process. Anything
# else but 0 is a failure.

function check_status
{
  if [ $1 -ne 0 ]
  then
    return 100
  else
    return 0
  fi 
}

# Generic function to be called for timing purposes.
function GetDate
{
  DATE_VAR=`date +%F--%T`
  if [ "$1" != "" ]
  then
    echo $1 >> $LOGFILE
  fi
  echo "*** TIMING --> ${DATE_VAR}" >> $LOGFILE
}

#########################################################################
# Generic function to run SQL*Plus processes.
# If the silent running switch is enabled, an automatic exit statement
# is appended so that the user does not have to keep typing exit.
# Parameters are:
#    $1 = the .sql file to run
#    $2 = whether it's APPS or XXCP. A=APPS X=XXCP
#    $3 = and parameters needed for the script
#########################################################################
function run_sql
{
  SQLFILE=$1
  SCHEMA=$2
  PARAM1=$3
  TMPLOCSQL=tmp_$SQLFILE
  if [ "$SILENT_RUNNING" = "Y" ]
  then
    cat ddl/code/$SQLFILE >  ddl/code/$TMPLOCSQL
    echo "exit;" >> ddl/code/$TMPLOCSQL
    if [ "$SCHEMA" = "A" ]
    then
      sqlplus apps/$APPS_PWD@$DB_NAME @ddl/code/$TMPLOCSQL $PARAM1 2>&1 | tee $MLOGDIR/$SQLFILE.log
    else
      sqlplus xxcp/$XXCP_PWD@$DB_NAME @ddl/code/$TMPLOCSQL $PARAM1 2>&1 | tee $MLOGDIR/$SQLFILE.log
    fi
  else
    if [ "$SCHEMA" = "A" ]
    then
      sqlplus apps/$APPS_PWD@$DB_NAME @ddl/code/$SQLFILE $PARAM1 2>&1 | tee $MLOGDIR/$SQLFILE.log
    else
      sqlplus xxcp/$XXCP_PWD@$DB_NAME @ddl/code/$SQLFILE $PARAM1 2>&1 | tee $MLOGDIR/$SQLFILE.log
    fi
  fi  
  
}

# Generic function to get the Oracle APPS version
function get_apps_ver
{
  write_log "Detecting Oracle Apps version"
  APPS_VER=`sqlplus -s apps/$APPS_PWD <<END
set feedback off;
set heading off;
select substr(release_name,1,2) from fnd_product_groups;
EXIT;
END`

  if [ $APPS_VER = "11" ]
  then
    write_log "Oracle Apps 11i detected."
    APPS_VER_LONG="11.5.10"
  elif [ $APPS_VER = "12" ]
  then
    write_log "Oracle Apps R12 detected"
    APPS_VER_LONG="12.0.0"
    # Copy the R12 forms to standard dir so is taken over in one go.
    # Only do this if any exist in the first place.
    R12_FRM=`ls ./forms/US/R12/*.fmb 2>/dev/null | wc -l `
    if [ $R12_FRM -gt 0 ] 
    then
      cp ./forms/US/R12/*.fmb ./forms/US
      write_log "Copied ${R12_FRM} forms from ./forms/US/R12/*.fmb to ./forms/US"
    fi
  else
    write_log "Unable to determine Oracle Apps version. Exiting..."
    exit
  fi
  
  write_log
}

# Generic function to take in a given Oracle password and
# test that it is valid by creating a dummy SQL file and
# silently executing it. Allows 3 attempts and then aborts.
function get_pwd
{
  # Counter for the number of attempts at the password.
  let RETRY_COUNT=0
  # Flag to indicate whether successful attempt or not.
  VERIFIED=FALSE
  # Type of password that is coming in from the parameter - APPS or XXCP
  PTYPE=$1
  
  # Create the dummy file to execute
  #echo "exit;" > ./tmp.sql

  write_log "Please enter ${PTYPE} password"

  # Switch off the display to the screen of what the user enters.
  stty -echo

  # Loop 3 times for the password entry.
  while [[ $RETRY_COUNT -lt 3 && "$VERIFIED" = "FALSE" ]]
  do
    read USER_PWD
    if [ -z $USER_PWD ]
    then
      write_log "******************************"
      write_log "No password entered. Aborting."
      write_log "******************************"
      # Clear up the temp file.
      rm -f ./tmp.sql
      # Switch back on the user input to the screen.
      stty echo
      tidy_exit 2
    fi

    echo "WHENEVER SQLERROR EXIT 1;
          connect $PTYPE/$USER_PWD;
          exit;
         " > ./tmp.sql

    STATUS=$?
    if [ $STATUS -ne 0 ]
    then
      write_log "*****************************************************"
      write_log "ERROR: Unable to write ./tmp.sql temporary file."
      write_log "Please check permission on the current location and retry"
      write_log "*****************************************************"
      tidy_exit 20
    fi

    # Execute the the temporary file with the credentials given and return
    # the value back to a variable.
    RESULT=`sqlplus /nolog @./tmp.sql`
    
    # Test whether a successful execution.
    if login_failure "$RESULT"
    then
      let RETRY_COUNT=$RETRY_COUNT+1  
      if [ $RETRY_COUNT -lt 3 ]
      then
        write_log "${PTYPE} password entered is incorrect. Please try again."
      fi
    else  
      write_log "${PTYPE} Password Verified."
      VERIFIED=TRUE
    fi
  done

  # Switch back on the user input to the screen.
  stty echo
  
  rm -f ./tmp.sql
  
  if [ $RETRY_COUNT -eq 3 ]
  then
    write_log "**********************************************"
    write_log "Unsuccessful entry of ${PTYPE} password. Aborting."
    write_log "**********************************************"
    write_log
    tidy_exit 3
  fi
}

function build_post_install
{
  POST_INSTALL=./ddl/code/vt_post_install.sql
  echo "-- ***********************************************************************************" > $POST_INSTALL
  echo "-- VT Copyright 2012 Virtual Trader Ltd. All Rights Reserved." >> $POST_INSTALL
  echo "-- ***********************************************************************************" >> $POST_INSTALL
  echo >> $POST_INSTALL
  echo "Prompt >" >> $POST_INSTALL
  echo "Prompt > VT Patch: vt_post_install.sql for release VT272 upgrade. (v1)" >> $POST_INSTALL
  echo "Prompt >" >> $POST_INSTALL
  echo "Prompt >" >> $POST_INSTALL
  echo "Prompt > The script should be run as the APPS user" >> $POST_INSTALL
  echo >> $POST_INSTALL
  echo "set define off" >> $POST_INSTALL
  echo "spool ddl/logs/vt_post_install.log" >> $POST_INSTALL
  echo "Prompt > Enable Triggers" >> $POST_INSTALL
  echo "@@ddl/code/vt_trig_enable" >> $POST_INSTALL
  echo >> $POST_INSTALL 
  echo "Prompt > Regenerate Snapshot" >> $POST_INSTALL
  echo >> $POST_INSTALL
  echo "@@ddl/code/vt_snapshot" >> $POST_INSTALL
  echo >> $POST_INSTALL
  echo "Prompt > Reset Engine Names" >> $POST_INSTALL
  echo >> $POST_INSTALL
  echo "exec xxcp_forms.Update_All_User_Engine_Names;" >> $POST_INSTALL
  echo >> $POST_INSTALL
  if [ "$OBJREGEN" = "Y" ]
  then
    echo "Prompt > Run Object Regen" >> $POST_INSTALL
    echo >> $POST_INSTALL
    echo "@@ddl/code/vt_object_regen" >> $POST_INSTALL
    echo >> $POST_INSTALL
  fi
  echo "spool off;  " >> $POST_INSTALL
  
}

#***********************************************************************
# Main Start - The script starts here.
#***********************************************************************
clear

log_directory_check
cd ddl
log_directory_check
cd ..

write_log "*******************************************************************"
write_log "** (C)opyright 2012  Virtual Trader Ltd. All rights reserved.    **"
write_log "**                                                               **"
write_log "** VT272 Patch 001 *** General   ***                             **"
write_log "** 07/12/2012 00:00                                              **"
write_log "**                                                               **"
write_log "** ${BASENAME}.sh                                                **"
write_log "*******************************************************************"
write_log
write_log "--- Detecting Environment ---"
write_log

# Try to detect the name of the database
if [ -z "$TWO_TASK" ]
then
  write_log "Please enter the name of the database"
  read DB_INSTALL
  TWO_TASK=$DB_INSTALL
  export TWO_TASK
else
  DB_INSTALL=$TWO_TASK
  # Make the database name uppercase to stand out for display to the user.
  DB_INSTALL=`echo $DB_INSTALL| tr '[a-z]' '[A-Z]'`
  write_log "Database environment has been detected as ${DB_INSTALL}."
  # Only allow Y or N answers
  while [[ $DB_ANS != "Y" && $DB_ANS != "N" ]]
  do
    write_log "Is this correct? (Y/N)"
    read DB_ANS
    DB_ANS=`echo $DB_ANS | tr [yn] [YN]`
  done
  if [ "$DB_ANS" = "Y" ]
  then
    write_log
  else
    write_log "************************************************************"
    write_log "Please set the environment to the correct database and retry"
    write_log "************************************************************"
    exit 100
  fi
fi

export DB_NAME=$TWO_TASK

# If there is no parameter entered, get the APPS password.
if [ $# -lt 1 ]
then
  # Call the generic password function
  get_pwd APPS
  APPS_PWD=$USER_PWD
else
  APPS_PWD=$1
fi

write_log

# If only one parameter entered, get the XXCP password.
if [ $# -lt 2 ]
then
  # Call the generic password function
  get_pwd XXCP
  XXCP_PWD=$USER_PWD
else
  XXCP_PWD=$2
fi

write_log

if [ -z "$SILENT_RUNNING" ]
then
  while [[ "$SILENT_RUNNING" != "Y" && "$SILENT_RUNNING" != "N" ]]
  do
    write_log "Do you want a silent installation? (Y/N)"
    read SILENT_RUNNING
    SILENT_RUNNING=`echo $SILENT_RUNNING | tr [yn] [YN]`
  done
fi

write_log

#
# Get Oracle Apps Version
#
get_apps_ver

# Database partioning
# Only allow Y or N answers
while [[ $DBP_ACTIVE != "Y" && $DBP_ACTIVE != "N" ]]
do
  write_log "Use Custom Database Partitioning ? <Y/N> : \c"
  read DBP_ACTIVE
  DBP_ACTIVE=`echo $DBP_ACTIVE | tr [yn] [YN]`
done

write_log

# Pre-installation taks
if [ -f ./ddl/code/vt_pre_install.sql ]
then
  write_log "Pre-installation tasks"
  run_sql vt_pre_install.sql A
fi

write_log "Current directory is ${PWD}"

# XXCP DDL Scripts

# Test if there is a XXCP script to run.
if [ -f ./ddl/code/${BASENAME}_xxcp.sql ]
then
  write_log "Launching XXCP SQL Script"
  # Check if database partitioning is being used.
  if [ "$DBP_ACTIVE" = "Y" ]
  then
    run_sql ${BASENAME}_xxcp.sql X Y
    # Check if we are on R12
    if [ $APPS_VER = "12" ]
    then
      # Check if there is an R12 XXCP script.
      # Not an error if not, just nothing to run
      if [ -f ./ddl/code/${BASENAME}_xxcp_12.sql ]
      then
        run_sql ${BASENAME}_xxcp_12.sql X Y
      fi
    fi 
  else
    # No database partitioning.
    run_sql ${BASENAME}_xxcp.sql X N
    # Check if we are on R12
    if [ $APPS_VER = "12" ]
    then
      # Check if there is an R12 XXCP script.
      # Not an error if not, just nothing to run
      if [ -f ./ddl/code/${BASENAME}_xxcp_12.sql ]
      then
        run_sql ${BASENAME}_xxcp_12.sql X N
      fi
    fi     
  fi
else
  write_log "*** No XXCP SQL script found: ${PWD}/${BASENAME}_xxcp.sql"
fi

# Data scripts

# Test if there is a data script
if [ -f ./ddl/code/${BASENAME}_data.sql ]
then
  write_log "Loading data script"
  # Test if R12
  if [ $APPS_VER = "12" ]
  then
    run_sql ${BASENAME}_data.sql X N
    # Check if there is a R12 data script.
    # Not an error if not.
    if [ -f ./ddl/code/${BASENAME}_data_12.sql ]
    then
      run_sql ${BASENAME}_data_12.sql X N
    else
      write_log "No R12 Data script found: ./ddl/code/${BASENAME}_data_12.sql"
    fi
  else
    run_sql ${BASENAME}_data.sql X N
  fi
else
  write_log "*** No Data script found: ./ddl/code/${BASENAME}_data.sql"
fi

# APPS scripts

# Test if there is an APPS script to run.
if [ -f ./ddl/code/${BASENAME}_apps.sql ]
then
  write_log "Launching APPS SQL Script"
  if [ "$DBP_ACTIVE" = "Y" ]
  then
    run_sql ${BASENAME}_apps.sql A Y
    if [ $APPS_VER = "12" ]
    then
    # Check if there is an R12 specific APPS script
      if [ -f ./ddl/code/${BASENAME}_apps_12.sql ]
      then
        run_sql ${BASENAME}_apps_12.sql A Y
      fi
    fi
  else
    run_sql ${BASENAME}_apps.sql A N
    if [ $APPS_VER = "12" ]
    then
    # Check if there is an R12 specific APPS script
      if [ -f ./ddl/code/${BASENAME}_apps_12.sql ]
      then
        run_sql ${BASENAME}_apps_12.sql A N
      fi
    fi    
  fi
else
  write_log "*** No APPS script found: ${PWD}/${BASENAME}_apps.sql"
fi

# Check if there is a single instance views script to run.
if [ -f ./ddl/code/vt_single_instance_views.sql ]
then
  write_log "Loading Single Instance Views"
  run_sql vt_single_instance_views.sql A
else
  write_log "No Single Instance View script found: ${PWD}/vt_single_instance_views.sql"
fi

# Check if there a R12 specific packages script to run.
if [ $APPS_VER = "12" ]
then
  if [ -f ./ddl/code/vt_single_instance_views_12.sql ]
  then
    write_log "Loading Single Instance Views (R12)"
    run_sql vt_single_instance_views_12.sql A
  else
    write_log "No Single Instance View (R12) script found: ${PWD}/vt_single_instance_views_12.sql"
  fi
fi

# Check if there is a general packages script to run.
if [ -f ./ddl/code/vt_packages_11.sql ]
then
  write_log "Launching Packages Script"
  run_sql vt_packages_11.sql A
else
  write_log "*** No Packages 11 script found: ${PWD}/vt_packages_11.sql"
fi

# Check if there a R12 specific packages script to run.
if [ $APPS_VER = "12" ]
then
  if [ -f ./ddl/code/vt_packages_12.sql ]
  then
    write_log "Launching Packages Script (R12)"
    run_sql vt_packages_12.sql A
  else
    write_log "No Packages 12 script found: ${PWD}/vt_packages_12.sql"
  fi
fi

# Check if we have the SQL file to regenerate the Snapshot data.
if [ -f ./ddl/code/vt_snapshot.sql ]
then
  write_log "Regenerating Snapshot Objects"
  run_sql vt_snapshot.sql A
else
  write_log "*** No Snapshot regeneration script found: ${PWD}/vt_snapshot.sql"
fi

cd $BASE_DIR
# Run the concurrent script this will upload the APPS objects including
# BI Publisher if used.
if [ -f ./vt_concurrent.sh ]
then
  write_log "Launching concurrent script"
  chmod 744 ./vt_concurrent.sh
  ./vt_concurrent.sh $APPS_PWD
else
  write_log "No concurrent script found: ${PWD}/vt_concurrent.sh"
fi

OBJREGEN=X

# Post installation tasks

while [[ $OBJREGEN != "Y" && $OBJREGEN != "N" ]]
do
  write_log "Do you want to run Object Regeneration now? (Y/N)"
  read OBJREGEN
  OBJREGEN=`echo $OBJREGEN | tr [yn] [YN]`
  write_resp_log $OBJREGEN
done
write_log "Post installation tasks"
build_post_install
run_sql vt_post_install.sql A 

write_log
write_log "Attempting to build tarball of log files"
create_tarball
clear
write_log "**********************************************"
write_log "*************** Finished *********************"
write_log "**********************************************"
write_log

if [ -f $MLOGDIR/$TARFILE ]
then
  write_log
  write_log "Created logfile tarball ${TARFILE} in: "
  write_log "${MLOGDIR} directory. "
  write_log "Please email to Virtual Trader"
  write_log
fi
