-- ***********************************************************************************
-- VT Copyright 2012 Virtual Trader Ltd. All Rights Reserved.
-- ***********************************************************************************

Prompt >
Prompt > VT Patch: vt_pre_install.sql for release VT272 upgrade. (v1)
Prompt >
Prompt >
Prompt > The script should be run as the APPS user
Prompt >

set define off

Prompt > Disable Triggers

@@ddl/code/vt_trig_disable

Prompt > Refresh Engine Names
exec xxcp_forms.Refresh_Engine_Names;

