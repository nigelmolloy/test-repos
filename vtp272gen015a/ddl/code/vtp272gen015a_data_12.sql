-- ***********************************************************************************
-- VT Copyright 2012 Virtual Trader Ltd. All Rights Reserved.
-- ***********************************************************************************

Prompt >
Prompt > VT Patch: vtp272gen015a_data_12.sql for release VT272 upgrade. (v1)
Prompt > This is for R12 specific data to be manipulated on the database.
Prompt >
Prompt > The script should be run as the XXCP user
Prompt >
set define off;
