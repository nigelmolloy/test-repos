-- ***********************************************************************************
-- VTML Copyright 2012. Virtual Trader Ltd. All rights reserved.
-- ***********************************************************************************
-- "$Revision: 1 $ $Modtime: 19/07/12 00:00 $  $Workfile: vt_packages_11.sql $"
-- ***********************************************************************************
set pagesize 1500
set linesize 200
set define off;

Prompt > Installing VT Packages into APPS Schema (Patch 272.1)

Select to_char(sysdate,'DD-MON-YYYY HH24:MI') "Install Time" from dual;

prompt > Package specifications
@@ddl/code/packages/xxcp_ar_engine.pks
@@ddl/code/packages/xxcp_cpa_collector.pks
@@ddl/code/packages/xxcp_cpa_fcast_engine.pks
@@ddl/code/packages/xxcp_cpa_true_up_engine.pks
@@ddl/code/packages/xxcp_data_collector.pks
@@ddl/code/packages/xxcp_file_upload.pks
@@ddl/code/packages/xxcp_forms.pks
@@ddl/code/packages/xxcp_gateway_utils.pks
@@ddl/code/packages/xxcp_generic_engine.pks
@@ddl/code/packages/xxcp_gl_engine.pks
@@ddl/code/packages/xxcp_memory.pks
@@ddl/code/packages/xxcp_preview_ctl.pks
@@ddl/code/packages/xxcp_pv_ar_engine.pks
@@ddl/code/packages/xxcp_pv_cpa_engine.pks
@@ddl/code/packages/xxcp_pv_gl_engine.pks
@@ddl/code/packages/xxcp_pv_rt_engine.pks
@@ddl/code/packages/xxcp_reporting.pks
@@ddl/code/packages/xxcp_rt_engine.pks
@@ddl/code/packages/xxcp_statement_code.pks
@@ddl/code/packages/xxcp_te_base.pks
@@ddl/code/packages/xxcp_te_config.pks
@@ddl/code/packages/xxcp_te_engine.pks
@@ddl/code/packages/xxcp_vertex_ws.pks
@@ddl/code/packages/xxcp_working.pks


prompt > Package bodies


prompt > XXCP_AR_ENGINE
@@ddl/code/packages/xxcp_ar_engine.pkb
show error;
select XXCP_AR_ENGINE.software_version from dual;

prompt > XXCP_CPA_COLLECTOR
@@ddl/code/packages/xxcp_cpa_collector.pkb
show error;
select XXCP_CPA_COLLECTOR.software_version from dual;

prompt > XXCP_CPA_FCAST_ENGINE
@@ddl/code/packages/xxcp_cpa_fcast_engine.pkb
show error;
select XXCP_CPA_FCAST_ENGINE.software_version from dual;

prompt > XXCP_CPA_TRUE_UP_ENGINE
@@ddl/code/packages/xxcp_cpa_true_up_engine.pkb
show error;
select XXCP_CPA_TRUE_UP_ENGINE.software_version from dual;

prompt > XXCP_DATA_COLLECTOR
@@ddl/code/packages/xxcp_data_collector.pkb
show error;
select XXCP_DATA_COLLECTOR.software_version from dual;

prompt > XXCP_FILE_UPLOAD
@@ddl/code/packages/xxcp_file_upload.pkb
show error;
select XXCP_FILE_UPLOAD.software_version from dual;

prompt > XXCP_FORMS
@@ddl/code/packages/xxcp_forms.pkb
show error;
select XXCP_FORMS.software_version from dual;

prompt > XXCP_GATEWAY_UTILS
@@ddl/code/packages/xxcp_gateway_utils.pkb
show error;
select XXCP_GATEWAY_UTILS.software_version from dual;

prompt > XXCP_GENERIC_ENGINE
@@ddl/code/packages/xxcp_generic_engine.pkb
show error;
select XXCP_GENERIC_ENGINE.software_version from dual;

prompt > XXCP_GL_ENGINE
@@ddl/code/packages/xxcp_gl_engine.pkb
show error;
select XXCP_GL_ENGINE.software_version from dual;

prompt > XXCP_MEMORY_PACK
@@ddl/code/packages/xxcp_memory.pkb
show error;
select XXCP_MEMORY_PACK.software_version from dual;

prompt > XXCP_PREVIEW_CTL
@@ddl/code/packages/xxcp_preview_ctl.pkb
show error;
select XXCP_PREVIEW_CTL.software_version from dual;

prompt > XXCP_PV_AR_ENGINE
@@ddl/code/packages/xxcp_pv_ar_engine.pkb
show error;
select XXCP_PV_AR_ENGINE.software_version from dual;

prompt > XXCP_PV_CPA_ENGINE
@@ddl/code/packages/xxcp_pv_cpa_engine.pkb
show error;
select XXCP_PV_CPA_ENGINE.software_version from dual;

prompt > XXCP_PV_GL_ENGINE
@@ddl/code/packages/xxcp_pv_gl_engine.pkb
show error;
select XXCP_PV_GL_ENGINE.software_version from dual;

prompt > XXCP_PV_RT_ENGINE
@@ddl/code/packages/xxcp_pv_rt_engine.pkb
show error;
select XXCP_PV_RT_ENGINE.software_version from dual;

prompt > XXCP_REPORTING
@@ddl/code/packages/xxcp_reporting.pkb
show error;
select XXCP_REPORTING.software_version from dual;

prompt > XXCP_RT_ENGINE
@@ddl/code/packages/xxcp_rt_engine.pkb
show error;
select XXCP_RT_ENGINE.software_version from dual;

prompt > XXCP_STAEMENT_CODE
@@ddl/code/packages/xxcp_statement_code.pkb
show error;
select xxcp_statement_code.software_version from dual;

prompt > XXCP_TE_BASE
@@ddl/code/packages/xxcp_te_base.pkb
show error;
select XXCP_TE_BASE.software_version from dual;

prompt > XXCP_TE_CONFIGURATOR
@@ddl/code/packages/xxcp_te_config.pkb
show error;
select XXCP_TE_CONFIGURATOR.software_version from dual;

prompt > XXCP_TE_ENGINE
@@ddl/code/packages/xxcp_te_engine.pkb
show error;
select XXCP_TE_ENGINE.software_version from dual;

prompt > XXCP_VERTEX_WS
@@ddl/code/packages/xxcp_vertex_ws.pkb
show error;
select XXCP_VERTEX_WS.software_version from dual;

prompt > XXCP_WKS
@@ddl/code/packages/xxcp_working.pkb
show error;
select XXCP_WKS.software_version from dual;


Prompt > See details see log : vt_packages_11.sql.log

Prompt > Type Exit to move onto the next script.
