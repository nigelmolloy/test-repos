-- ***********************************************************************************
-- VT Copyright 2012 Virtual Trader Ltd. All Rights Reserved.
-- ***********************************************************************************

Prompt >
Prompt > VT Patch: vt_snapshot.sql for release VT272 upgrade. (v1)
Prompt >
Prompt >
Prompt > The script should be run as the APPS user
Prompt >

set define off

Prompt > Re-generate Snapshot Objects

CREATE OR REPLACE VIEW XXCP_EXPORT_V AS 
  SELECT 'TABLE' table_name,xmlelement("TABLES",null) table_data, null max_md_id 
  FROM dual;

exec xxcp_snapshot.generate_import_pkg;
exec xxcp_snapshot.generate_export_view;

@./ddl/code/vt_timing.sql

Commit;

Prompt > Type Exit to move onto the next script.
