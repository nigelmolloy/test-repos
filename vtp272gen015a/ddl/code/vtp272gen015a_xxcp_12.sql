-- ***********************************************************************************
-- VT Copyright 2012 Virtual Trader Ltd. All Rights Reserved.
-- ***********************************************************************************

Prompt >
Prompt > VT Patch: vtp272gen015a_xxcp_12.sql for release VT272 upgrade. (v1)
Prompt > This is for R12 specific DDL
Prompt >
Prompt > The script should be run as the XXCP user
Prompt >

set termout off
column partitioned_tables new_value part_option
select case when nvl('&&1','N') = 'N' then
         '--'
       else
         ''
       end partitioned_tables
from dual;
set termout on
