-- ***********************************************************************************
-- VT Copyright 2012 Virtual Trader Ltd. All Rights Reserved.
-- ***********************************************************************************

Prompt >
Prompt > VT Patch: vtp272gen015a_apps.sql for release VT272 upgrade. (v1)
Prompt >
Prompt >
Prompt > The script should be run as the XXCP user
Prompt >
set define off;
set termout off
column partitioned_tables new_value part_option
select case when nvl('&&1','N') = 'N' then
         '--'
       else
         ''
       end partitioned_tables
from dual;
set termout on

Prompt > Synonym XXCP_WEB_SERVICE_CTL

CREATE OR REPLACE SYNONYM XXCP_WEB_SERVICE_CTL FOR XXCP.XXCP_WEB_SERVICE_CTL;

CREATE OR REPLACE SYNONYM XXCP_TAX_AUTHORITIES FOR XXCP.XXCP_TAX_AUTHORITIES;

CREATE OR REPLACE SYNONYM XXCP_TAX_RATE_QUALIFIERS FOR XXCP.XXCP_TAX_RATE_QUALIFIERS;

CREATE OR REPLACE SYNONYM XXCP_TAX_REGIMES FOR XXCP.XXCP_TAX_REGIMES;

CREATE OR REPLACE SYNONYM XXCP_TAX_RATE_LOOKUPS FOR XXCP.XXCP_TAX_RATE_LOOKUPS;

/*
CREATE OR REPLACE SYNONYM XXCP_TAX_RESULTS FOR XXCP.XXCP_TAX_RESULTS;
CREATE OR REPLACE SYNONYM XXCP_PV_TAX_RESULTS FOR XXCP.XXCP_PV_TAX_RESULTS;
*/

CREATE OR REPLACE VIEW XXCP_INSTANCE_TAX_REGIME_V AS
SELECT TAX_REGIME_CODE, W2.TAX_CURRENCY_CODE, W2.TAX_REGIME_ID, INITCAP(NVL(W2.ROUNDING_RULE_CODE,'NEAREST')) ROUNDING_RULE_CODE
      ,TAX_REGIME_NAME, W2.EFFECTIVE_FROM, W2.EFFECTIVE_TO
FROM ZX_REGIMES_VL W2;

Prompt > XXCP_INSTANCE_TAX_RATES_V

CREATE OR REPLACE VIEW XXCP_INSTANCE_TAX_RATES_V AS
Select a.tax_rate_code
      ,a.tax_regime_code
      ,a.tax_jurisdiction_code
      ,a.tax
      ,a.TAX_STATUS_CODE
      ,a.TAX_RATE_ID
      ,nvl(a.description,a.tax_regime_code) TAX_RATE_NAME
      ,a.effective_from
      ,a.effective_to
      ,a.percentage_rate
      ,a.active_flag
      ,a.default_rec_rate_code
      ,a1.recovery_type_code
      ,a1.percentage_rate recovery_rate
  From zx.zx_rates_b a,
       zx.zx_rates_b a1
 Where a.rate_type_code  = 'PERCENTAGE'
   And a.tax_regime_code = a1.tax_regime_code(+)
   And a.tax = a1.tax(+)
   And a.default_rec_type_code = a1.recovery_type_code(+)
   And a.default_rec_rate_code = a1.tax_rate_code(+);

CREATE OR REPLACE VIEW XXCP_INSTANCE_TAXRATE_LOOKUP_V AS
select distinct bv.TAX_REGIME_CODE,
       bv.TAX_RATE_CODE,
       bv.TAX,
       bv.TAX_JURISDICTION_CODE,
       bv.TAX_RATE_NAME,
       bv.TAX_STATUS_CODE
  from XXCP_INSTANCE_TAX_RATES_V bv
 where bv.TAX_JURISDICTION_CODE is not null
   and active_flag = 'Y'
order by bv.TAX_REGIME_CODE;

Prompt > New Qualifiaction Lists

Delete from xxcp_sys_qual_context where field_name in ('TAX LOOKUP 1 QUALIFIER','TAX LOOKUP 2 QUALIFIER','TAX LOOKUP 3 QUALIFIER','TAX LOOKUP 4 QUALIFIER','TAX LOOKUP 5 QUALIFIER','TAX LOOKUP 6 QUALIFIER');

insert into xxcp_sys_qual_context (FIELD_NAME, QUALIFICATION_NAME, ACTIVE, MANDATORY, COLUMN_NAME, NOTES, SQL_BUILD, FORM_NAME, QC_ID, USAGE, ADDITIONAL_COLUMNS, MD_ID, SOURCE_ID)
values ('TAX LOOKUP 1 QUALIFIER', 'TAX LOOKUP QUALIFIER 1', 'Y', 'Y', 'TAX_REGIME_CODE', null, 'from xxcp_tax_regimes', null, null, 'S', null, null, 0);

insert into xxcp_sys_qual_context (FIELD_NAME, QUALIFICATION_NAME, ACTIVE, MANDATORY, COLUMN_NAME, NOTES, SQL_BUILD, FORM_NAME, QC_ID, USAGE, ADDITIONAL_COLUMNS, MD_ID, SOURCE_ID)
values ('TAX LOOKUP 2 QUALIFIER', 'TAX LOOKUP QUALIFIER 2', 'Y', 'Y', 'TAX_REGIME_CODE', null, 'from xxcp_tax_regimes', null, null, 'S', null, null, 0);


insert into xxcp_sys_qual_context (FIELD_NAME, QUALIFICATION_NAME, ACTIVE, MANDATORY, COLUMN_NAME, NOTES, SQL_BUILD, FORM_NAME, QC_ID, USAGE, ADDITIONAL_COLUMNS, MD_ID, SOURCE_ID)
values ('TAX LOOKUP 3 QUALIFIER', 'TAX LOOKUP QUALIFIER 3', 'Y', 'Y', 'TAX_REGIME_CODE', null, 'from xxcp_tax_regimes', null, null, 'S', null, null, 0);

insert into xxcp_sys_qual_context (FIELD_NAME, QUALIFICATION_NAME, ACTIVE, MANDATORY, COLUMN_NAME, NOTES, SQL_BUILD, FORM_NAME, QC_ID, USAGE, ADDITIONAL_COLUMNS, MD_ID, SOURCE_ID)
values ('TAX LOOKUP 4 QUALIFIER', 'TAX LOOKUP QUALIFIER 4', 'Y', 'Y', 'TAX_REGIME_CODE', null, 'from xxcp_tax_regimes', null, null, 'S', null, null, 0);

insert into xxcp_sys_qual_context (FIELD_NAME, QUALIFICATION_NAME, ACTIVE, MANDATORY, COLUMN_NAME, NOTES, SQL_BUILD, FORM_NAME, QC_ID, USAGE, ADDITIONAL_COLUMNS, MD_ID, SOURCE_ID)
values ('TAX LOOKUP 5 QUALIFIER', 'TAX LOOKUP QUALIFIER 5', 'Y', 'Y', 'TAX_REGIME_CODE', null, 'from xxcp_tax_regimes', null, null, 'S', null, null, 0);

insert into xxcp_sys_qual_context (FIELD_NAME, QUALIFICATION_NAME, ACTIVE, MANDATORY, COLUMN_NAME, NOTES, SQL_BUILD, FORM_NAME, QC_ID, USAGE, ADDITIONAL_COLUMNS, MD_ID, SOURCE_ID)
values ('TAX LOOKUP 6 QUALIFIER', 'TAX LOOKUP QUALIFIER 6', 'Y', 'Y', 'TAX_REGIME_CODE', null, 'from xxcp_tax_regimes', null, null, 'S', null, null, 0);

DELETE FROM XXCP_INTERNAL_CODES WHERE ERROR_CODE IN (3171,3172,3173,3174);


insert into xxcp_internal_codes (ERROR_CODE, ERROR_CODE_RANGE, LANGUAGE, MESSAGE, PACKAGE_NAME, PRIORITY, SUGGESTION, GROUPING_SET_ID, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values (3171, null, 'English', 'Tax Rate required, but not found', 'XXCP_TE_CONFIGURATOR', 3, 'The Tax Rate should be found because the category has been set with that option. A setup is missing for the qualifiers given.', null, null, null, null, null, null, null, 0);

insert into xxcp_internal_codes (ERROR_CODE, ERROR_CODE_RANGE, LANGUAGE, MESSAGE, PACKAGE_NAME, PRIORITY, SUGGESTION, GROUPING_SET_ID, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values (3172, null, 'English', 'Too many Tax Regimes with the same Element number found.', 'XXCP_TE_CONFIGURATOR', 3, 'Check Tax Admin form setup or Tax Lookup Qualifiers', null, null, null, null, null, null, null, 0);

insert into xxcp_internal_codes (ERROR_CODE, ERROR_CODE_RANGE, LANGUAGE, MESSAGE, PACKAGE_NAME, PRIORITY, SUGGESTION, GROUPING_SET_ID, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values (3173, null, 'English', 'Failed to create Tax Results record', 'XXCP_TE_CONFIGURATOR', 2, 'The insert command into the Tax Results table failed', null, null, null, null, null, null, null, 0);

insert into xxcp_internal_codes (ERROR_CODE, ERROR_CODE_RANGE, LANGUAGE, MESSAGE, PACKAGE_NAME, PRIORITY, SUGGESTION, GROUPING_SET_ID, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values (3174, null, 'English', 'Selected Regimes should have the same currency for a transaction', 'XXCP_TE_CONFIGURATOR', 2, 'Check Tax Administration screen', null, null, null, null, null, null, null, 0);


Commit;

DELETE FROM XXCP_TABLE_SET_CTL WHERE ATTRIBUTE1 = 'TX' AND SET_BASE_TABLE = 'CP_SYS_PROFILE';

INSERT INTO XXCP_TABLE_SET_CTL (SET_ID, SET_BASE_TABLE, SOURCE_ID, SET_TYPE, SET_NAME, ATTRIBUTE_SET_ID, ENABLED_FLAG, ATTRIBUTE1, ATTRIBUTE2, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8, ATTRIBUTE9, ATTRIBUTE10)
values (0, 'CP_SYS_PROFILE', 0, 'SETTING', 'TAX SETTINGS', null, null, 'TX', 'Y', null, null, null, -1, SYSDATE, -1, SYSDATE, -1, null, null, null, null, null, null);

DELETE FROM XXCP_SYS_PROFILE WHERE PROFILE_NAME = 'TAX RATES ENABLED' and profile_category = 'TX';

insert into xxcp_sys_profile (PROFILE_NAME, PROFILE_VALUE, PROFILE_CATEGORY, MANDATORY, PREFILLED, DATA_TYPE, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values ('TAX RATES ENABLED', 'Y', 'TX', null, null, null, -1, SYSDATE, -1, SYSDATE, null, null, 0);

Prompt > XXCP_TAX_AUTHORITIES_V

CREATE OR REPLACE VIEW XXCP_TAX_AUTHORITIES_V AS
SELECT TAX_AUTHORITY, DESCRIPTION, ACTIVE, TAX_ENGINE_ID,TAX_AUTHORITY_ID
  FROM XXCP_TAX_AUTHORITIES;

Commit;

Prompt > XXCP_LOOKUPS

DELETE FROM XXCP_LOOKUPS WHERE LOOKUP_TYPE =    'TRANSACTON SUB RULE';

insert into xxcp_lookups (LOOKUP_TYPE, LOOKUP_TYPE_SUBSET, LOOKUP_CODE, NUMERIC_CODE, MEANING, ENABLED_FLAG, SYSTEM_MAINTAINED, START_DATE_ACTIVE, END_DATE_ACTIVE, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values ('TRANSACTON SUB RULE', 'T', '0', null, 'Transaction Tax', 'Y', 'Y', null, null, -1, SYSDATE, -1, SYSDATE, -1, null, 0);

insert into xxcp_lookups (LOOKUP_TYPE, LOOKUP_TYPE_SUBSET, LOOKUP_CODE, NUMERIC_CODE, MEANING, ENABLED_FLAG, SYSTEM_MAINTAINED, START_DATE_ACTIVE, END_DATE_ACTIVE, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values ('TRANSACTON SUB RULE', 'T', '1', null, 'Element 1', 'Y', 'Y', null, null, -1, SYSDATE, -1, SYSDATE, -1, null, 0);

insert into xxcp_lookups (LOOKUP_TYPE, LOOKUP_TYPE_SUBSET, LOOKUP_CODE, NUMERIC_CODE, MEANING, ENABLED_FLAG, SYSTEM_MAINTAINED, START_DATE_ACTIVE, END_DATE_ACTIVE, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values ('TRANSACTON SUB RULE', 'T', '2', null, 'Element 2', 'Y', 'Y', null, null, -1, SYSDATE, -1, SYSDATE, -1, null, 0);

insert into xxcp_lookups (LOOKUP_TYPE, LOOKUP_TYPE_SUBSET, LOOKUP_CODE, NUMERIC_CODE, MEANING, ENABLED_FLAG, SYSTEM_MAINTAINED, START_DATE_ACTIVE, END_DATE_ACTIVE, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values ('TRANSACTON SUB RULE', 'T', '3', null, 'Element 3', 'Y', 'Y', null, null, -1, SYSDATE, -1, SYSDATE, -1, null, 0);

insert into xxcp_lookups (LOOKUP_TYPE, LOOKUP_TYPE_SUBSET, LOOKUP_CODE, NUMERIC_CODE, MEANING, ENABLED_FLAG, SYSTEM_MAINTAINED, START_DATE_ACTIVE, END_DATE_ACTIVE, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values ('TRANSACTON SUB RULE', 'T', '4', null, 'Element 4', 'Y', 'Y', null, null, -1, SYSDATE, -1, SYSDATE, -1, null, 0);

insert into xxcp_lookups (LOOKUP_TYPE, LOOKUP_TYPE_SUBSET, LOOKUP_CODE, NUMERIC_CODE, MEANING, ENABLED_FLAG, SYSTEM_MAINTAINED, START_DATE_ACTIVE, END_DATE_ACTIVE, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values ('TRANSACTON SUB RULE', 'T', '5', null, 'Element 4', 'Y', 'Y', null, null, -1, SYSDATE, -1, SYSDATE, -1, null, 0);

Commit;

Prompt > XXCP_INSTANCE_IC_ADDR_V

CREATE OR REPLACE VIEW XXCP_INSTANCE_IC_ADDR_V AS
Select lnk.Instance_Name
       ,lnk.Instance_id
       ,TAX_REGISTRATION_ID      Tax_Registration_id
       ,BILL_TO_NAME             BT_NAME
       ,xxcp_reporting.Get_Bill_To_Address(d.tax_registration_id) BILL_TO_ADDRESS
       ,BILL_TO_ADDRESS_LINE_1   BT_LINE1
       ,BILL_TO_ADDRESS_LINE_2   BT_LINE2
       ,BILL_TO_ADDRESS_LINE_3   BT_LINE3
       ,BILL_TO_TOWN_OR_CITY     BT_TOWN
       ,BILL_TO_DISPLAY_STYLE    BT_STLYE
       ,BILL_TO_REGION           BT_STATE
       ,BILL_TO_POSTAL_CODE      BT_POSTAL_CODE
       ,BILL_TO_COUNTRY          BT_COUNTRY
       ,BILL_TO_TELEPHONE_NUMBER BT_TELEPHONE
       ,D.Bill_To_State_Code
       ,SHIP_TO_NAME             ST_NAME
       ,xxcp_reporting.Get_Ship_To_Address(d.tax_registration_id)   SHIP_TO_ADDRESS
       ,SHIP_TO_ADDRESS_LINE_1   ST_LINE1
       ,SHIP_TO_ADDRESS_LINE_2   ST_LINE2
       ,SHIP_TO_ADDRESS_LINE_3   ST_LINE3
       ,SHIP_TO_TOWN_OR_CITY     ST_TOWN
       ,SHIP_TO_DISPLAY_STYLE    ST_STLYE
       ,SHIP_TO_REGION           ST_STATE
       ,SHIP_TO_POSTAL_CODE      ST_POSTAL_CODE
       ,SHIP_TO_COUNTRY          ST_COUNTRY
       ,SHIP_TO_TELEPHONE_NUMBER ST_TELEPHONE
       ,D.SHIP_To_State_Code
from xxcp_address_details d,
     xxcp_instance_db_links_v lnk
where d.instance_id = lnk.instance_id;

Prompt > UPDATE INSTANCE VIEW

UPDATE XXCP_INSTANCE_VIEWS F
 SET F.STD_TEXT =
 'Select lnk.Instance_Name
       ,lnk.Instance_id
       ,TAX_REGISTRATION_ID      Tax_Registration_id
       ,BILL_TO_NAME             BT_NAME
       ,xxcp_reporting.Get_Bill_To_Address(d.tax_registration_id) BILL_TO_ADDRESS
       ,BILL_TO_ADDRESS_LINE_1   BT_LINE1
       ,BILL_TO_ADDRESS_LINE_2   BT_LINE2
       ,BILL_TO_ADDRESS_LINE_3   BT_LINE3
       ,BILL_TO_TOWN_OR_CITY     BT_TOWN
       ,BILL_TO_DISPLAY_STYLE    BT_STLYE
       ,BILL_TO_REGION           BT_STATE
       ,BILL_TO_POSTAL_CODE      BT_POSTAL_CODE
       ,BILL_TO_COUNTRY          BT_COUNTRY
       ,BILL_TO_TELEPHONE_NUMBER BT_TELEPHONE
       ,BILL_TO_STATE_CODE
       ,SHIP_TO_NAME             ST_NAME
       ,xxcp_reporting.Get_Ship_To_Address(d.tax_registration_id)   SHIP_TO_ADDRESS
       ,SHIP_TO_ADDRESS_LINE_1   ST_LINE1
       ,SHIP_TO_ADDRESS_LINE_2   ST_LINE2
       ,SHIP_TO_ADDRESS_LINE_3   ST_LINE3
       ,SHIP_TO_TOWN_OR_CITY     ST_TOWN
       ,SHIP_TO_DISPLAY_STYLE    ST_STLYE
       ,SHIP_TO_REGION           ST_STATE
       ,SHIP_TO_POSTAL_CODE      ST_POSTAL_CODE
       ,SHIP_TO_COUNTRY          ST_COUNTRY
       ,SHIP_TO_TELEPHONE_NUMBER ST_TELEPHONE
       ,SHIP_TO_STATE_CODE
from xxcp_address_details d,
     xxcp_instance_db_links_v lnk
where d.instance_id = lnk.instance_id'
WHERE VIEW_NAME = 'XXCP_INSTANCE_IC_ADDR_V';

Prompt > XXCP_INSTANCE_VIEWS

DELETE FROM XXCP_INSTANCE_VIEWS WHERE VIEW_ID IN (51,52);

INSERT INTO XXCP_INSTANCE_VIEWS (VIEW_ID, VIEW_NAME, STD_TEXT, GENERATED, COMPILE_SEQ, COMMENTS, LOCKED, AUTO_GENERATE, ACTIVE, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values (51, 'XXCP_INSTANCE_TAX_REGIME_V', 'SELECT TAX_REGIME_CODE, W2.TAX_CURRENCY_CODE, W2.TAX_REGIME_ID, INITCAP(NVL(W2.ROUNDING_RULE_CODE,''NEAREST'')) ROUNDING_RULE_CODE
      ,TAX_REGIME_NAME, W2.EFFECTIVE_FROM, W2.EFFECTIVE_TO
FROM ZX_REGIMES_VL W2', 'N', 510, 'Used by Tax Administration form', 'N', 'Y', 'Y', null, SYSDATE, null, null, null, null, 0);

INSERT INTO XXCP_INSTANCE_VIEWS (VIEW_ID, VIEW_NAME, STD_TEXT, GENERATED, COMPILE_SEQ, COMMENTS, LOCKED, AUTO_GENERATE, ACTIVE, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values (52, 'XXCP_INSTANCE_TAX_RATES_V',
'Select a.tax_rate_code
      ,a.tax_regime_code
      ,a.tax_jurisdiction_code
      ,a.tax
      ,a.TAX_STATUS_CODE
      ,a.TAX_RATE_ID
      ,nvl(a.description,a.tax_regime_code) TAX_RATE_NAME
      ,a.effective_from
      ,a.effective_to
      ,a.percentage_rate
      ,a.active_flag
      ,a.default_rec_rate_code
      ,a1.recovery_type_code
      ,a1.percentage_rate recovery_rate
  From zx.zx_rates_b a,
       zx.zx_rates_b a1
 Where a.rate_type_code  = ''PERCENTAGE''
   And a.tax_regime_code = a1.tax_regime_code(+)
   And a.tax = a1.tax(+)
   And a.default_rec_type_code = a1.recovery_type_code(+)
   And a.default_rec_rate_code = a1.tax_rate_code(+)'
   , 'Y', 520, 'Used by Tax Lookups form and Assignment Engine to read a tax rates from the base table.', 'N', 'Y', 'Y', null,SYSDATE, null, null, null, null, 0);

Prompt > DB Links

DELETE FROM XXCP_INSTANCE_VIEW_LINKS WHERE VIEW_ID IN (51,52);

insert into xxcp_instance_view_links (VIEW_ID, DB_LINK_ID, INCLUDED, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values (51, 1, 'Y', null, null, null, null, null, Null, 0);

insert into xxcp_instance_view_links (VIEW_ID, DB_LINK_ID, INCLUDED, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values (51, 2, 'N', null, null, null, null, null, Null, 0);

insert into xxcp_instance_view_links (VIEW_ID, DB_LINK_ID, INCLUDED, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values (51, 3, 'N', null, null, null, null, null, Null, 0);

insert into xxcp_instance_view_links (VIEW_ID, DB_LINK_ID, INCLUDED, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values (51, 4, 'N', null, null, null, null, null, Null, 0);

insert into xxcp_instance_view_links (VIEW_ID, DB_LINK_ID, INCLUDED, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values (51, 6, 'N', null, null, null, null, null, Null, 0);

insert into xxcp_instance_view_links (VIEW_ID, DB_LINK_ID, INCLUDED, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values (51, 7, 'N', null, null, null, null, null, Null, 0);

insert into xxcp_instance_view_links (VIEW_ID, DB_LINK_ID, INCLUDED, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values (51, 8, 'N', null, null, null, null, null, Null, 0);

insert into xxcp_instance_view_links (VIEW_ID, DB_LINK_ID, INCLUDED, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values (51, 9, 'N', null, null, null, null, null, Null, 0);

insert into xxcp_instance_view_links (VIEW_ID, DB_LINK_ID, INCLUDED, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values (51, 10, 'N', null, null, null, null, null, Null, 0);

insert into xxcp_instance_view_links (VIEW_ID, DB_LINK_ID, INCLUDED, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values (52, 1, 'Y', null, null, null, null, null, Null, 0);

insert into xxcp_instance_view_links (VIEW_ID, DB_LINK_ID, INCLUDED, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values (52, 2, 'N', null, null, null, null, null, Null, 0);

insert into xxcp_instance_view_links (VIEW_ID, DB_LINK_ID, INCLUDED, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values (52, 3, 'N', null, null, null, null, null, Null, 0);

insert into xxcp_instance_view_links (VIEW_ID, DB_LINK_ID, INCLUDED, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values (52, 4, 'N', null, null, null, null, null, Null, 0);

insert into xxcp_instance_view_links (VIEW_ID, DB_LINK_ID, INCLUDED, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values (52, 6, 'N', null, null, null, null, null, Null, 0);

insert into xxcp_instance_view_links (VIEW_ID, DB_LINK_ID, INCLUDED, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values (52, 7, 'N', null, null, null, null, null, Null, 0);

insert into xxcp_instance_view_links (VIEW_ID, DB_LINK_ID, INCLUDED, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values (52, 8, 'N', null, null, null, null, null, Null, 0);

insert into xxcp_instance_view_links (VIEW_ID, DB_LINK_ID, INCLUDED, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values (52, 9, 'N', null, null, null, null, null, Null, 0);

insert into xxcp_instance_view_links (VIEW_ID, DB_LINK_ID, INCLUDED, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values (52, 10, 'N', null, null, null, null, null, Null, 0);

Commit;

CREATE OR REPLACE VIEW XXCP_ADDR_TERRITORIES_V AS
SELECT X.TERRITORY_CODE Country_Code,
       X.TERRITORY_SHORT_NAME Country_Name,
       X.LANGUAGE
FROM FND_TERRITORIES_TL  X
WHERE X.LANGUAGE = 'US';

Commit;

--
-- Cost Plus (Start)
--

Prompt > XXCP_COST_PLUS_LISTS

Create synonym XXCP_COST_PLUS_LISTS for XXCP.XXCP_COST_PLUS_LISTS;

Create synonym XXCP_COST_PLUS_LISTS_SEQ for XXCP.XXCP_COST_PLUS_LISTS_SEQ;

--
-- Cost Plus (Finish)
--

-- NOV15c
Prompt > XXCP_DATA_LOAD_TMP Synonym

Create or replace Synonym XXCP_DATA_LOAD_TMP for xxcp.XXCP_DATA_LOAD_TMP;

