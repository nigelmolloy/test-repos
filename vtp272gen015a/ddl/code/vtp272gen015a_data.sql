-- ***********************************************************************************
-- VT Copyright 2012 Virtual Trader Ltd. All Rights Reserved.
-- ***********************************************************************************

Prompt >
Prompt > VT Patch: vtp272gen015a_data.sql for release VT272 upgrade. (v1)
Prompt >
Prompt >
Prompt > The script should be run as the XXCP user
Prompt >

set define off;

-- These lines should always be included. Remember to change the patch id etc in both the delete and insert.
Delete from xxcp.vt_patches where patch_id = 2720151;

Insert into xxcp.vt_patches(patch_id, product_id, patch_level, description, release_date, creation_date)  values(2720151,'2','03.05.06','Patch vtp272gen015a','08-JAN-2013',sysdate);

Delete from xxcp_lookups
where lookup_type = 'TAX ADMIN ROUNDING';

insert into xxcp_lookups (LOOKUP_TYPE, LOOKUP_TYPE_SUBSET, LOOKUP_CODE, NUMERIC_CODE, MEANING, ENABLED_FLAG, SYSTEM_MAINTAINED, START_DATE_ACTIVE, END_DATE_ACTIVE, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values ('TAX ADMIN ROUNDING', null, 'Ceiling', null, 'Round Up', 'Y', 'Y', null, null, -1, SYSDATE, -1, SYSDATE, -1, NULL, 0);

insert into xxcp_lookups (LOOKUP_TYPE, LOOKUP_TYPE_SUBSET, LOOKUP_CODE, NUMERIC_CODE, MEANING, ENABLED_FLAG, SYSTEM_MAINTAINED, START_DATE_ACTIVE, END_DATE_ACTIVE, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values ('TAX ADMIN ROUNDING', null, 'Floor', null, 'Round Down', 'Y', 'Y', null, null, -1, SYSDATE, -1, SYSDATE, -1, NULL, 0);

insert into xxcp_lookups (LOOKUP_TYPE, LOOKUP_TYPE_SUBSET, LOOKUP_CODE, NUMERIC_CODE, MEANING, ENABLED_FLAG, SYSTEM_MAINTAINED, START_DATE_ACTIVE, END_DATE_ACTIVE, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values ('TAX ADMIN ROUNDING', null, 'Nearest', null, 'Nearest', 'Y', 'Y', null, null, -1, SYSDATE, -1, SYSDATE, -1, NULL, 0);

delete from xxcp_internal_codes where ERROR_CODE between 3170 and 3174;

insert into xxcp_internal_codes (ERROR_CODE, ERROR_CODE_RANGE, LANGUAGE, MESSAGE, PACKAGE_NAME, PRIORITY, SUGGESTION, GROUPING_SET_ID, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values (3170, null, 'English', 'No Tax Authority Found', 'XXCP_TE_CONFIGURATOR', 3, 'Check the Tax Authority names in Tax Admin with those used in the Entities Form', null, null, null, null, null, null, null, 0);

insert into xxcp_internal_codes (ERROR_CODE, ERROR_CODE_RANGE, LANGUAGE, MESSAGE, PACKAGE_NAME, PRIORITY, SUGGESTION, GROUPING_SET_ID, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values (3171, null, 'English', 'Tax Rate required, but not found', 'XXCP_TE_CONFIGURATOR', 3, 'The Tax Rate should be found because the category has been set with that option. A setup is missing for the qualifiers given.', null, null, null, null, null, null, null, 0);

insert into xxcp_internal_codes (ERROR_CODE, ERROR_CODE_RANGE, LANGUAGE, MESSAGE, PACKAGE_NAME, PRIORITY, SUGGESTION, GROUPING_SET_ID, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values (3172, null, 'English', 'Too many Tax Regimes with the same Element number found.', 'XXCP_TE_CONFIGURATOR', 3, 'Check Tax Admin form setup or Tax Lookup Qualifiers', null, null, null, null, null, null, null, 0);

insert into xxcp_internal_codes (ERROR_CODE, ERROR_CODE_RANGE, LANGUAGE, MESSAGE, PACKAGE_NAME, PRIORITY, SUGGESTION, GROUPING_SET_ID, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values (3173, null, 'English', 'Failed to create Tax Results record', 'XXCP_TE_CONFIGURATOR', 2, 'The insert command into the Tax Results table failed', null, null, null, null, null, null, null, 0);

insert into xxcp_internal_codes (ERROR_CODE, ERROR_CODE_RANGE, LANGUAGE, MESSAGE, PACKAGE_NAME, PRIORITY, SUGGESTION, GROUPING_SET_ID, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID, SOURCE_ID)
values (3174, null, 'English', 'Selected Regimes should have the same currency for a transaction', 'XXCP_TE_CONFIGURATOR', 2, 'Check Tax Administration screen', null, null, null, null, null, null, null, 0);



Prompt > Vertex Internal Codes

Delete from  xxcp_internal_codes where  ERROR_CODE between 15501 and 15505;

insert into xxcp_internal_codes (ERROR_CODE, ERROR_CODE_RANGE, LANGUAGE, MESSAGE, PACKAGE_NAME, PRIORITY, SUGGESTION, GROUPING_SET_ID, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID)
values (15501, null, 'English', 'Error retrieving Vertex request string', 'XXCP_VERTEX_WS', null, '', null, -1, SYSDATE, -1, SYSDATE, -1, null);

insert into xxcp_internal_codes (ERROR_CODE, ERROR_CODE_RANGE, LANGUAGE, MESSAGE, PACKAGE_NAME, PRIORITY, SUGGESTION, GROUPING_SET_ID, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID)
values (15502, null, 'English', 'Error sending Vertex request', 'XXCP_VERTEX_WS', null, '', null, -1, SYSDATE, -1, SYSDATE, -1, null);

insert into xxcp_internal_codes (ERROR_CODE, ERROR_CODE_RANGE, LANGUAGE, MESSAGE, PACKAGE_NAME, PRIORITY, SUGGESTION, GROUPING_SET_ID, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID)
values (15503, null, 'English', 'Error reading Vertex response', 'XXCP_VERTEX_WS', null, '', null, -1, SYSDATE, -1, SYSDATE, -1, null);

insert into xxcp_internal_codes (ERROR_CODE, ERROR_CODE_RANGE, LANGUAGE, MESSAGE, PACKAGE_NAME, PRIORITY, SUGGESTION, GROUPING_SET_ID, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID)
values (15504, null, 'English', 'Error populating Tax Results', 'XXCP_VERTEX_WS', null, '', null, -1, SYSDATE, -1, SYSDATE, -1, null);

insert into xxcp_internal_codes (ERROR_CODE, ERROR_CODE_RANGE, LANGUAGE, MESSAGE, PACKAGE_NAME, PRIORITY, SUGGESTION, GROUPING_SET_ID, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, MD_ID)
values (15505, null, 'English', 'Vertex returned a Fault', 'XXCP_VERTEX_WS', null, '', null, -1, SYSDATE, -1, SYSDATE, -1, null);


Prompt > Populate Vertex Web Service Control

delete from xxcp_web_service_ctl where ws_id = 2;

insert into xxcp_web_service_ctl (WS_ID, WS_NAME, WS_HOSTNAME, WS_PORT, WS_ENDPOINT, WS_REQUEST, SOURCE_ID, MD_ID, CREATED_BY, CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN)
values (2, 'VERTEX TAX CALCULATION', null , null, null, null, 0, null, -1, sysdate, null, null, null);

update xxcp_web_service_ctl
set ws_request = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:vertexinc:o-series:tps:5:0">
         <soapenv:Header/>
         <soapenv:Body>
          <VertexEnvelope xmlns="urn:vertexinc:o-series:tps:5:0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="urn:vertexinc:o-series:tps:5:0 VertexInc_Envelope.xsd">
            <Login>
              <UserName>:USERNAME</UserName>
              <Password>:PASSWORD</Password>
            </Login>
       <:REQUEST_TYPE returnAssistedParametersIndicator="true" transactionType="SALE">
          <Seller>
           <PhysicalOrigin>
              <PostalCode>:SELLER_POSTCODE</PostalCode>
              <Country>:SELLER_COUNTRY</Country>
            </PhysicalOrigin>
          </Seller>
          <Customer>
            <Destination>
              <PostalCode>:CUST_POSTCODE</PostalCode>
              <Country>:CUST_COUNTRY</Country>
            </Destination>
          </Customer>
          <LineItem>
            <Quantity>1</Quantity>
            <UnitPrice>:UNIT_PRICE</UnitPrice>
          </LineItem>
        </:REQUEST_TYPE>
          </VertexEnvelope>
         </soapenv:Body>
      </soapenv:Envelope>'
where ws_id = 2;

Prompt > Assignment Qualifiers

update xxcp_assignment_ctl
  set assign_ctl_group_id = 0
  where assign_ctl_group_id is null;

update xxcp_assignment_ctl
  set rank = 1
  where rank is null;

Insert Into xxcp_sys_qualifier_groups
  (source_id
  ,qualifier_group_name
  ,qualifier_group_id
  ,qualifier_usage
  ,qualifier1_id
  ,qualifier2_id
  ,qualifier3_id
  ,qualifier4_id
  ,created_by
  ,creation_date
  ,qualifier_sequence
  )
  Select t.source_id, 'Default', 0, 'A', 0, 0, 0, 0, -1, Sysdate,0
    From xxcp_sys_sources t
   Where Not t.source_id In (14, 40)
     And Not t.source_id = Any
   (Select w1.source_id
            From xxcp_sys_qualifier_groups w1
           Where w1.qualifier_usage = 'A'
             And w1.qualifier_group_id = 0);


Prompt > Seeded Custom Data (Tax Agreement Details)

DECLARE
  vSeq         Number;

BEGIN

  select max(category_id)+1
    into vSeq
    from xxcp.xxcp_cust_data_ctl;


  dbms_output.put_line('Using category number '||to_char(vSeq));
  dbms_output.put_line('TAX AGREEMENT DETAILS');

  -- Category
  insert into XXCP_CUST_DATA_CTL (source_id, category_id, category_name, description, restricted_view, restricted_layout, created_by, creation_date, last_updated_by, last_update_date, last_update_login, md_id, locked)
  values (31, vSeq, 'TAX AGREEMENT DETAILS', 'Tax Agreement Details', 'N', 'N', 1013435, to_date('17-12-2012 11:48:02', 'dd-mm-yyyy hh24:mi:ss'), 1013435, to_date('17-12-2012 11:48:02', 'dd-mm-yyyy hh24:mi:ss'), 5598838, null, 'Y');

  -- Attributes
  insert into xxcp.XXCP_CUST_DATA_CTL_ATT (category_id, attribute_id, data_type, active, justification, case_restriction, prompt, prompt_justification, mandatory, width, qualification_id, uniqueness_key, validation_condition, validation_message, min_length, max_length, format_mask, hint_text, weighting, highlight_condition, highlight_colour, conditional_mandatory_id, lov_min_filter_length, qualification_display_id, restricted_layout, lov_dependant_sql, created_by, creation_date, last_updated_by, last_update_date, last_update_login, md_id, source_id, locked)
  values (vSeq, 1, 'C', 'Y', 'L', 'M', 'TA Number', 'S', null, null, null, null, null, null, null, null, null, null, 0, null, null, null, null, null, null, null, 1013435, to_date('17-12-2012 11:48:02', 'dd-mm-yyyy hh24:mi:ss'), 1013435, to_date('17-12-2012 11:48:02', 'dd-mm-yyyy hh24:mi:ss'), 5598838, null, 31, 'Y');

  insert into xxcp.XXCP_CUST_DATA_CTL_ATT (category_id, attribute_id, data_type, active, justification, case_restriction, prompt, prompt_justification, mandatory, width, qualification_id, uniqueness_key, validation_condition, validation_message, min_length, max_length, format_mask, hint_text, weighting, highlight_condition, highlight_colour, conditional_mandatory_id, lov_min_filter_length, qualification_display_id, restricted_layout, lov_dependant_sql, created_by, creation_date, last_updated_by, last_update_date, last_update_login, md_id, source_id, locked)
  values (vSeq, 2, 'C', 'Y', 'L', 'M', 'Payee', 'S', null, null, null, null, null, null, null, null, null, null, 0, null, null, null, null, null, null, null, 1013435, to_date('17-12-2012 11:48:02', 'dd-mm-yyyy hh24:mi:ss'), 1013435, to_date('17-12-2012 11:48:02', 'dd-mm-yyyy hh24:mi:ss'), 5598838, null, 31, 'Y');

  insert into xxcp.XXCP_CUST_DATA_CTL_ATT (category_id, attribute_id, data_type, active, justification, case_restriction, prompt, prompt_justification, mandatory, width, qualification_id, uniqueness_key, validation_condition, validation_message, min_length, max_length, format_mask, hint_text, weighting, highlight_condition, highlight_colour, conditional_mandatory_id, lov_min_filter_length, qualification_display_id, restricted_layout, lov_dependant_sql, created_by, creation_date, last_updated_by, last_update_date, last_update_login, md_id, source_id, locked)
  values (vSeq, 3, 'C', 'Y', 'L', 'M', 'Payor', 'S', null, null, null, null, null, null, null, null, null, null, 0, null, null, null, null, null, null, null, 1013435, to_date('17-12-2012 11:48:02', 'dd-mm-yyyy hh24:mi:ss'), 1013435, to_date('17-12-2012 11:48:02', 'dd-mm-yyyy hh24:mi:ss'), 5598838, null, 31, 'Y');

  insert into xxcp.XXCP_CUST_DATA_CTL_ATT (category_id, attribute_id, data_type, active, justification, case_restriction, prompt, prompt_justification, mandatory, width, qualification_id, uniqueness_key, validation_condition, validation_message, min_length, max_length, format_mask, hint_text, weighting, highlight_condition, highlight_colour, conditional_mandatory_id, lov_min_filter_length, qualification_display_id, restricted_layout, lov_dependant_sql, created_by, creation_date, last_updated_by, last_update_date, last_update_login, md_id, source_id, locked)
  values (vSeq, 4, 'C', 'Y', 'L', 'M', 'Slice', 'S', null, null, null, null, null, null, null, null, null, null, 0, null, null, null, null, null, null, null, 1013435, to_date('17-12-2012 11:48:02', 'dd-mm-yyyy hh24:mi:ss'), 1013435, to_date('17-12-2012 11:48:02', 'dd-mm-yyyy hh24:mi:ss'), 5598838, null, 31, 'Y');

  insert into xxcp.XXCP_CUST_DATA_CTL_ATT (category_id, attribute_id, data_type, active, justification, case_restriction, prompt, prompt_justification, mandatory, width, qualification_id, uniqueness_key, validation_condition, validation_message, min_length, max_length, format_mask, hint_text, weighting, highlight_condition, highlight_colour, conditional_mandatory_id, lov_min_filter_length, qualification_display_id, restricted_layout, lov_dependant_sql, created_by, creation_date, last_updated_by, last_update_date, last_update_login, md_id, source_id, locked)
  values (vSeq, 5, 'C', 'Y', 'L', 'M', 'Method', 'S', null, null, null, null, null, null, null, null, null, null, 0, null, null, null, null, null, null, null, 1013435, to_date('17-12-2012 11:48:02', 'dd-mm-yyyy hh24:mi:ss'), 1013435, to_date('17-12-2012 11:48:02', 'dd-mm-yyyy hh24:mi:ss'), 5598838, null, 31, 'Y');

  insert into xxcp.XXCP_CUST_DATA_CTL_ATT (category_id, attribute_id, data_type, active, justification, case_restriction, prompt, prompt_justification, mandatory, width, qualification_id, uniqueness_key, validation_condition, validation_message, min_length, max_length, format_mask, hint_text, weighting, highlight_condition, highlight_colour, conditional_mandatory_id, lov_min_filter_length, qualification_display_id, restricted_layout, lov_dependant_sql, created_by, creation_date, last_updated_by, last_update_date, last_update_login, md_id, source_id, locked)
  values (vSeq, 6, 'C', 'Y', 'L', 'M', 'Calc ' || chr(10) || 'Currency', 'S', null, null, null, null, null, null, null, null, null, null, 0, null, null, null, null, null, null, null, 1013435, to_date('17-12-2012 15:52:25', 'dd-mm-yyyy hh24:mi:ss'), 1013435, to_date('17-12-2012 15:52:25', 'dd-mm-yyyy hh24:mi:ss'), 5598838, null, 31, 'Y');

  insert into xxcp.XXCP_CUST_DATA_CTL_ATT (category_id, attribute_id, data_type, active, justification, case_restriction, prompt, prompt_justification, mandatory, width, qualification_id, uniqueness_key, validation_condition, validation_message, min_length, max_length, format_mask, hint_text, weighting, highlight_condition, highlight_colour, conditional_mandatory_id, lov_min_filter_length, qualification_display_id, restricted_layout, lov_dependant_sql, created_by, creation_date, last_updated_by, last_update_date, last_update_login, md_id, source_id, locked)
  values (vSeq, 7, 'N', 'Y', 'R', 'M', 'Seq', 'S', null, .5, null, null, null, null, null, null, null, null, 0, null, null, null, null, null, null, null, 1013435, to_date('02-01-2013 10:20:27', 'dd-mm-yyyy hh24:mi:ss'), 1013435, to_date('02-01-2013 10:20:27', 'dd-mm-yyyy hh24:mi:ss'), 5599072, null, 31, 'Y');


END;
/

Prompt > XXCP_INSTANCE_CPA_COLLECTOR_V Instance View

Delete from XXCP_INSTANCE_VIEWS
where view_id = 36;

INSERT INTO XXCP_INSTANCE_VIEWS ( VIEW_ID, VIEW_NAME, STD_TEXT,  GENERATED, CREATED_BY,
CREATION_DATE, LAST_UPDATED_BY, LAST_UPDATE_DATE, LAST_UPDATE_LOGIN, COMPILE_SEQ, LOCKED, COMMENTS,
ACTIVE ) VALUES (
36, 'XXCP_INSTANCE_CPA_COLLECTOR_V', 'select ''VT'' data_source,k.instance_id,k.chart_of_accounts_id,k.collector_id,k.period_set_name_id,k.company_from,
k.company_to,k.company_list,k.department_from,k.department_to,k.department_list,k.summarize_department,k.account_from,k.account_to,k.account_list,
k.summarize_account,k.extra_segment1_from,k.extra_segment1_to,k.extra_segment1_list,k.summarize_extra_segment1,k.extra_segment2_from,k.extra_segment2_to,
k.extra_segment2_list,k.summarize_extra_segment2,k.extra_segment3_from,k.extra_segment3_to,k.extra_segment3_list,k.summarize_extra_segment3,
k.cost_category_id,k.cost_category,k.forecast,k.true_up,k.effective_from_date,k.effective_to_date,k.active,k.category_data_source,k.creation_date,
k.created_by,k.last_update_date,k.last_updated_by,k.last_update_login,k.cost_plus_set_id
from XXCP_cost_plus_collector_ctl/*datalink*/ k'
,  'Y', NULL,   SYSDATE, -1,  SYSDATE, -1, 360, 'N', 'Cost-Plus Collector data', 'Y');


Commit;


