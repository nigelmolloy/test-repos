-- ***********************************************************************************
-- VT Copyright 2005-2012. Virtual Trader Ltd. All rights reserved.
-- ***********************************************************************************
-- "$Revision: 2 $ $Modtime: 30/04/12 10:49 $  $Workfile: vt_single_instance_views.sql $"
-- ***********************************************************************************

Prompt >>
Prompt >> ADDING Virtual Trader Instance VIEWS
Prompt >>


Prompt > XXCP_INSTANCE_CPA_COLLECTOR_V

create or replace view xxcp_instance_cpa_collector_v as
select 'VT' data_source,
       k.instance_id,
       k.chart_of_accounts_id,
       k.collector_id,
       k.period_set_name_id,
       k.company_from,
       k.company_to,
       k.company_list,
       k.department_from,
       k.department_to,
       k.department_list,
       k.summarize_department,
       k.account_from,
       k.account_to,
       k.account_list,
       k.summarize_account,
       k.extra_segment1_from,
       k.extra_segment1_to,
       k.extra_segment1_list,
       k.summarize_extra_segment1,
       k.extra_segment2_from,
       k.extra_segment2_to,
       k.extra_segment2_list,
       k.summarize_extra_segment2,
       k.extra_segment3_from,
       k.extra_segment3_to,
       k.extra_segment3_list,
       k.summarize_extra_segment3,
       k.cost_category_id,
       k.cost_category,
       k.forecast,
       k.true_up,
       k.effective_from_date,
       k.effective_to_date,
       k.active,
       k.category_data_source,
       k.creation_date,
       k.created_by,
       k.last_update_date,
       k.last_updated_by,
       k.last_update_login,
       k.cost_plus_set_id
from   XXCP_cost_plus_collector_ctl k;


Prompt > Type Exit to move onto the next script.
