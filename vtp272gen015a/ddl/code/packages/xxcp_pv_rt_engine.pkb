CREATE OR REPLACE PACKAGE BODY XXCP_PV_RT_ENGINE AS
/********************************************************************************************
                        V I R T A L  T R A D E R  L I M I T E D
  		      (Copyright 2005-2012 Virtual Trader Ltd. All rights Reserved)

   NAME:       XXCP_PV_RT_ENGINE
   PURPOSE:    RT Engine in Preview Mode

   REVISIONS:
   Ver        Date      Author  Description
   ---------  --------  ------- ------------------------------------
   02.04.00   28/06/02  Keith   First Coding
   02.04.01   07/02/08  Keith   Changed RT Column names
   02.04.02   19/02/08  Keith   Assignment Only does not create Cache Records
   02.04.03   05/04/08  Keith   Added Investigate and IC Price check
   02.04.04   03/03/08  Nigel   Added vt_result_columns to pv_interface.
   02.04.05   13/03/09  Nigel   Added diagnostics
   02.06.01   21/10/09  Simon   Grouping now done by transaction_table not assignment_id..
   02.06.02   21/12/09  Simon   Added vt_results_attributes31..50 to control
   02.06.03   28/12/09  Keith   Added Calc_legal_exch_rate
   02.06.04   08/02/10  Keith   Added ic_currency_code and Price update. 
   02.06.05   03/06/10  Keith   Added logic to stop whole batch if one record fails.
   03.06.06   15/06/12  Keith   Updated for Cisco to change order by clause in CFG cursor
   03.06.07   11/10/12  Mat     OOD-349 Merge in changes - trace in control
   03.06.08   02/01/12  Mat     OOD-597 Init - gDDL_Interface_Select - ref to CP_ changed to XXCP_
*********************************************************************************************/

  vTiming          xxcp_global.gTiming_rec;
	vTimingCnt       pls_integer;
  -- Current Records

  gBalancing_Array       XXCP_DYNAMIC_ARRAY := XXCP_DYNAMIC_ARRAY(' ',' ',' ',' ',' ',' ',' ',' ',' ',' ');
  gTrans_Table_Array     XXCP_DYNAMIC_ARRAY := XXCP_DYNAMIC_ARRAY();

  -- New Dynamic SQL 02.06.06
  gDDL_Interface_Select    varchar2(12000);
  gDDL_Interface_Order_By  varchar2(2000);

  gInvestigate_Errors    Varchar2(1) := 'Y';
  gIC_Price_Mandatory    Varchar2(1) := 'N';
  gErrorWholeRequest     varchar2(1) := 'Y';

  TYPE gCursors_Rec is RECORD(
    v_cursorId     pls_Integer := 0,
    v_Target_Table XXCP_column_rules.transaction_table%type,
    v_usage        varchar2(1),
    v_OpenCur      varchar2(1) := 'N',
    v_LastCur      pls_integer := -1);

  TYPE gCUR_REC is TABLE of gCursors_Rec INDEX by BINARY_INTEGER;
  gCursors gCUR_REC;


  -- *************************************************************
  --                     Software Version Control
  --                   -----------------------------
  -- This package can be checked with SQL to ensure it's installed
  -- select packagename.software_version from dual;
  -- *************************************************************
  Function Software_Version RETURN VARCHAR2 IS
   BEGIN
     RETURN('Version [03.06.08] Build Date [02-JAN-2013] Name [XXCP_PV_RT_ENGINE]');
   END Software_Version;

	 
  -- !! ***********************************************************************************************************
  -- !!
  -- !!                                             Fetch_Source_Rowid 
  -- !!
  -- !! ***********************************************************************************************************
	Function Fetch_Source_Rowid(cRowid in rowid) Return Rowid is

   Cursor SX (pSource_Rowid in rowid) is
      Select vt_source_rowid
        from XXCP_PV_INTERFACE l
       where rowid            = pSource_Rowid
         and l.vt_preview_id  = xxcp_global.gCommon(1).Preview_id;
				 
  vReturn_rowid rowid;
				 
  Begin
	  For SXRec in SX(cRowid) Loop
      vReturn_rowid := SXRec.vt_source_rowid;
    End Loop;
		
		Return(vReturn_rowid);
                 
  End Fetch_Source_Rowid; 	
	
	 -- !! ***********************************************************************************************************
  -- !!
  -- !!                                             Open_Cursor
  -- !!
  -- !! ***********************************************************************************************************
  Function Open_Cursor(cUsage in varchar2, cTarget_Table in varchar2) Return pls_integer is
  
    i    pls_integer;
    vPos pls_integer := 0;
  
  Begin
    For i in 1 .. gCursors.count loop
      If (gCursors(i).v_Target_Table = cTarget_Table) AND (gCursors(i).v_usage = cUsage) then
        vPos := i;
        If gCursors(i).v_OpenCur = 'N' then
          gCursors(i).v_cursorId := DBMS_SQL.OPEN_CURSOR;
          gCursors(i).v_OpenCur := 'Y';
          gCursors(i).v_LastCur := -1;
          exit;
        End If;
      End If;
    End Loop;
  
    Return(vPos);
  End Open_Cursor;

  -- *************************************************************
  --
  --                  ClearWorkingStorage
  --
  -- *************************************************************
  Procedure ClearWorkingStorage is
  
  begin

    -- Processed Records
  	xxcp_wks.WORKING_CNT := 0;
	  xxcp_wks.WORKING_RCD.Delete;

    -- Current Records before processing
    xxcp_wks.SOURCE_CNT := 0;
	  XXCP_WKS.SOURCE_ROWID.Delete;
    XXCP_WKS.SOURCE_STATUS.Delete;
    XXCP_WKS.SOURCE_SUB_CODE.Delete;

  End ClearWorkingStorage;

--   !! ***********************************************************************************************************
 --   !!
 --   !!                                   Error_Whole_Request
 --   !!
 --   !! ***********************************************************************************************************
 --
 Procedure Error_Whole_Request(cInternalErrorCode in out Number) is
 
 
 Begin
 
   If gErrorWholeRequest = 'Y' then
     -- Set all transactions in group to error status
     Begin
      Update XXCP_PV_INTERFACE
         set vt_status = 'ERROR'
            ,vt_internal_Error_code    = 12823 -- Associated errors
       where vt_Source_Assignment_id   = xxcp_global.gCommon(1).current_Assignment_id
         and vt_preview_id             = xxcp_global.gCommon(1).Preview_id
         and vt_Transaction_Table      = xxcp_global.gCommon(1).current_Transaction_Table;
     End;
   
   End If;
  
   ClearWorkingStorage;

End Error_Whole_Request;

 --
 --   !! ***********************************************************************************************************
 --   !!
 --   !!                                   Error_Whole_Transaction
 --   !!                If one part of the wrapper finds a problem whilst processing a record,
 --   !!                          then we need to error the whole transaction.
 --   !!
 --   !! ***********************************************************************************************************
 --
 Procedure Error_Whole_Transaction(cInternalErrorCode in out Number) is
 
 vVT_Source_Rowid Rowid;
 
 Begin
   -- Set all transactions in group to error status
   Begin
    Update XXCP_PV_INTERFACE
      set vt_status = 'ERROR'
         ,vt_internal_Error_code    = 12823 -- Associated errors
    where vt_Source_Assignment_id   = xxcp_global.gCommon(1).current_Assignment_id
      and vt_Parent_Trx_id          = xxcp_global.gCommon(1).current_Parent_Trx_id
      and vt_preview_id             = xxcp_global.gCommon(1).Preview_id
      and vt_Transaction_Table      = xxcp_global.gCommon(1).current_Transaction_Table;
   End;
   -- Update Record that actually caused the problem.
   Begin
     Update XXCP_PV_INTERFACE
        set VT_STATUS              = 'ERROR'
           ,VT_INTERNAL_ERROR_CODE = cInternalErrorCode
           ,VT_DATE_PROCESSED      = Sysdate
      where Rowid                  = xxcp_global.gCommon(1).current_source_rowid
        and vt_preview_id          = xxcp_global.gCommon(1).Preview_id;
   End;

    If xxcp_global.gCommon(1).Custom_events = 'Y' then
    	
			vVT_Source_Rowid := fetch_source_rowid(xxcp_global.gCommon(1).current_source_rowid);

      xxcp_custom_events.ON_TRANSACTION_ERROR(xxcp_global.gCommon(1).Source_id,
                                              xxcp_global.gCommon(1).current_assignment_id,
                                              vVT_Source_Rowid,
                                              xxcp_global.gCommon(1).current_Transaction_Table,
                                              xxcp_global.gCommon(1).current_Parent_Trx_id,
                                              xxcp_global.gCommon(1).current_transaction_id,
                                              cInternalErrorCode);
    End If;
  
    ClearWorkingStorage;

End Error_Whole_Transaction;

--
--   !! ***********************************************************************************************************
--   !!
--   !!                                     No_Action_Transaction
--   !!                                If there are no records to process.
--   !!
--   !! ***********************************************************************************************************
--
Procedure No_Action_Transaction(cSource_Rowid in rowid) is
Begin
  -- Update Record that actually had not action required
  Begin
    Update XXCP_PV_INTERFACE
       set VT_STATUS              = 'SUCCESSFUL'
          ,VT_INTERNAL_ERROR_CODE = Null
          ,VT_DATE_PROCESSED      = xxcp_global.SystemDate
          ,VT_STATUS_CODE         = 7003
     where Rowid                  = cSource_Rowid
       and vt_preview_id          = xxcp_global.gCommon(1).Preview_id;
  End;
End No_Action_Transaction;

/*
  !! ***********************************************************************************************************
  !!
  !!                                   Transaction_Group_Stamp
  !!                 Stamp Incomming record with parent Trx to create groups of
  ||                    transactions that resemble on document transaction.
  !!
  !! ***********************************************************************************************************
*/
Procedure Transaction_Group_Stamp(cStamp_Parent_Trx in varchar2,cSource_assignment_id in number, cSource_Rowid in Rowid, cUser_id in number, cInternalErrorCode out number) is


 vStatement          varchar2(2000);
 vInternalErrorCode  Number(8) := 0;

 -- Distinct list of Transaction Tables
 -- populated in Set_Running_Status
 CURSOR cTrxTbl is
   SELECT transaction_table,
          nvl(grouping_rule_id,0) grouping_rule_id
   FROM   XXCP_sys_source_tables
   where  source_id = xxcp_global.gCommon(1).Source_id
   and    transaction_table in (select * from TABLE(CAST(gTrans_Table_Array AS XXCP_DYNAMIC_ARRAY)));

 Cursor c1 (pSource_assignment_id in number,
            pTransaction_Table    IN VARCHAR2) IS
      select distinct 'Update XXCP_pv_interface j
                          set vt_parent_trx_id = (select '||t.Parent_trx_column|| ' from XXCP_rt_interface k where k.rowid = j.vt_source_rowid)
                        where vt_status = ''ASSIGNMENT''
                          and vt_preview_id  = '||to_char(xxcp_global.gCommon(1).Preview_id)||'
                          and vt_source_assignment_id = '||to_char(pSource_assignment_id)||'
                          and vt_transaction_table = '''||g.vt_transaction_table||'''' Statement_line
           ,t.parent_trx_column
           ,t.Transaction_Table
        from XXCP_pv_interface g
            ,XXCP_sys_source_tables t
  	        ,XXCP_source_assignments x
       where g.vt_transaction_table    = t.transaction_table
         and t.source_id               = x.source_id
         and g.vt_preview_id           = xxcp_global.gCommon(1).preview_id
         and x.source_assignment_id    = psource_assignment_id
         and g.vt_source_assignment_id = x.source_assignment_id
         and g.vt_transaction_table    = pTransaction_Table;     -- 02.06.01

 Cursor er1(psource_assignment_id in number,
            pTransaction_Table    IN VARCHAR2) IS
      select g.vt_interface_id, g.rowid source_rowid, g.vt_transaction_table
        from XXCP_pv_interface g
       where g.vt_status = 'ASSIGNMENT'
         and g.vt_source_assignment_id = psource_assignment_id
         and g.vt_preview_id           = xxcp_global.gCommon(1).preview_id
         and g.vt_transaction_table    = pTransaction_Table      -- 02.06.01
         and g.vt_parent_trx_id is null;


 Begin
 
     For cTblRec in cTrxTbl Loop  -- Loop for all Transaction Tables within SA.

        If cTblRec.Grouping_Rule_Id > 0 THEN  --02.06.02
          XXCP_DYNAMIC_SQL.Apply_Grouping_Rules(cInternalErrorCode => vInternalErrorCode,
                                                cTransaction_Table => cTblRec.Transaction_Table);

          -- Error Checking for Null Parent Trx id
          For er1Rec in er1(cSource_assignment_id,
                            cTblRec.transaction_table) LOOP
           update XXCP_pv_interface l
              set vt_status                = 'ERROR'
                 ,vt_parent_trx_id         = Null
                 ,l.vt_internal_error_code = 11090
            where l.vt_transaction_table = er1rec.vt_transaction_table
              and l.vt_preview_id        = xxcp_global.gCommon(1).preview_id;
          End loop;
  		  
        Else

          -- Mass update of Parent Trx id
          For rec in c1(cSource_assignment_id,
                        cTblRec.transaction_table) LOOP

             If rec.parent_trx_column is null then
                vInternalErrorCode := 10665;
                xxcp_foundation.FndWriteError(vInternalErrorCode,'Transaction Table <'||Rec.Transaction_Table||'>');
             Else
               vStatement := rec.Statement_line;
               Begin
                Execute Immediate vStatement;

                Exception when OTHERS then
                   vInternalErrorCode := 10667;
                    xxcp_foundation.FndWriteError(vInternalErrorCode,SQLERRM,vStatement);
               End;
             End If;
          End loop;
        End If;
      End Loop;
      
      xxcp_te_base.Preview_Mode_Restrictions(cCalling_Wrapper      => 'XXCP_PV_RT_ENGINE', 
			                                       cSource_assignment_id => cSource_assignment_id, 
																             cSource_Rowid         => cSource_Rowid  
																             ); 

      cInternalErrorCode := vInternalErrorCode;

  End Transaction_Group_Stamp;

 --                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
 -- !! ***********************************************************************************************************                                                                                                                                                                                                                                                                                                                                                                                                  
 -- !!                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
 -- !!                                   FlushRecordsToTarget
 -- !!     Control The process of moving the Array elements throught to creating the new output records
 -- !!
 -- !! ***********************************************************************************************************
  Function FlushRecordsToTarget(cGlobal_Rounding_Tolerance in number,
                                cGlobalPrecision           in number,
                                cTransaction_Type          in varchar2,
                                cInternalErrorCode         in out number)
    return number is

    vCnt           pls_integer := 0;
    c              pls_integer := 0;
  
    vEntered_DR    Number := 0;
    vEntered_CR    Number := 0;
    j              pls_integer := 0;
    fe             pls_integer := xxcp_wks.WORKING_CNT;
  
    vRC            pls_integer := xxcp_wks.SOURCE_CNT;
    vRC_Records    pls_integer := 0;
    vRC_Error      pls_integer := 0;
    
    vStatus XXCP_gl_interface.vt_status%type;
    vErrorMessage varchar2(3000);
    UDI_Cnt       pls_integer := 0; 
    
		vVT_Source_Rowid  Rowid;
    vZero_Action      pls_integer;
 
	
BEGIN
 
    -- Rounding is Optional in vt_engine
     If fe > 0  then
      xxcp_te_base.Account_Rounding(cGlobal_Rounding_Tolerance,
                                    cGlobalPrecision,
                                    cTransaction_Type,
                                    vEntered_DR,
                                    vEntered_CR,
                                    cInternalErrorCode);
    End If;

    -- Check that for each attribute record we have a records for output.
    For c in 1 .. vRC Loop
      vRC_Error := 0;
      FOR j in 1 .. fe LOOP
        If xxcp_wks.Source_rowid(c) = xxcp_wks.WORKING_RCD(j).Source_rowid then
		      xxcp_wks.WORKING_RCD(j).Source_Pos := c;
		      xxcp_wks.SOURCE_STATUS(c) := 'FLUSH';
          vRC_Error   := 1;
          vRC_Records := vRC_Records + 1;
        end if;
      End loop;
      IF vRC_Error = 0 THEN
        xxcp_wks.SOURCE_SUB_CODE(c) := 7003;
		    xxcp_wks.SOURCE_STATUS(c) := 'NO RULES';
		    No_Action_Transaction(xxcp_wks.Source_rowid(c));
      END IF;
    END LOOP;
   
    Savepoint TARGET_INSERT;
  
    If cInternalErrorCode = 0 and vRC_Records > 0 then
 

      For j in 1 .. fe Loop
      
        Exit when cInternalErrorCode <> 0;
      
        vCnt := vCnt + 1;

        -- Custom Events 
        If xxcp_global.gCommon(1).Custom_events = 'Y' then
   				xxcp_te_base.Custom_Event_Insert_Row(j,cInternalErrorCode);	        
        End If;

				
        -- Tag Zero Account Value rows
      
        If cInternalErrorCode = 0 then
        
		      xxcp_global.gCommon(1).current_source_rowid := xxcp_wks.Source_rowid(xxcp_wks.WORKING_RCD(j).Source_Pos);
					
        	-- Find the Action to take for Zero Accounted values	
          vZero_Action := xxcp_te_base.Zero_Suppression_Rule(j);

          IF vZero_Action > 0 THEN
                 Select XXCP_PREVIEW_HIST_SEQ.nextval into xxcp_wks.WORKING_RCD(j) .Process_History_id from dual;
                 -- don't post ZAV
                 
                 -- Don't Post to Oracle !!

                  vVT_Source_Rowid := fetch_source_rowid(xxcp_wks.WORKING_RCD(j).Source_rowid);
          
                  If cInternalErrorCode = 0 then
                 

                    ---
                    --- XXCP_PROCESS_HISTORY
                    ---
                    XXCP_PV_PROCESS_HIST_RT.Process_History_Insert(
										                                          cSource_Rowid            => vVT_Source_Rowid,  -- Preview Mode only,
																											        cParent_Rowid            => Null,
                                                              cTarget_Table            => xxcp_wks.WORKING_RCD(j).Target_Table,
                                                              cTarget_Instance_id      => xxcp_wks.WORKING_RCD(j).Target_Instance_id,
                                                              cProcess_History_id      => xxcp_wks.WORKING_RCD(j).Process_History_id,
                                                              cTarget_assignment_id    => xxcp_wks.WORKING_RCD(j).Target_Assignment_id,
                                                              cAttribute_id            => xxcp_wks.WORKING_RCD(j).Attribute_id,
                                                              cRecord_type             => xxcp_wks.WORKING_RCD(j).Record_type,
                                                              cRequest_id              => xxcp_global.gCommon(1).current_request_id,
                                                              cStatus                  => xxcp_wks.WORKING_RCD(j).Record_Status,
                                                              cInterface_id            => xxcp_wks.WORKING_RCD(j).Interface_id,
                                                              cModel_ctl_id            => xxcp_wks.WORKING_RCD(j).Model_ctl_id,
                                                              cRule_id                 => xxcp_wks.WORKING_RCD(j).Rule_id,
                                                              cTax_registration_id     => xxcp_wks.WORKING_RCD(j).Tax_Registration_id,
																															cEntered_rounding_amount => xxcp_wks.WORKING_RCD(j).Entered_Rnd_Amount,
																															cAccount_rounding_amount => xxcp_wks.WORKING_RCD(j).Accounted_Rnd_Amount,
                                                              cD1001_Array             => xxcp_wks.WORKING_RCD(j).D1001_Array,
                                                              cD1002_Array             => xxcp_wks.WORKING_RCD(j).D1002_Array,
                                                              cD1003_Array             => xxcp_wks.WORKING_RCD(j).D1003_Array,
                                                              cD1004_Array             => xxcp_wks.WORKING_RCD(j).D1004_Array,
																											        cD1005_Array             => xxcp_wks.WORKING_RCD(j).D1005_Array,
                                                              cD1006_Array             => xxcp_wks.WORKING_RCD(j).D1006_Array,
                                                              cI1009_Array             => xxcp_wks.WORKING_RCD(j).I1009_Array,
                                                              cErrorMessage            => vErrorMessage,
                                                              cInternalErrorCode       => cInternalErrorCode);                                                              
                     If cInternalErrorCode = 0 then
                       vTiming(vTimingCnt).Flush_Records := vTiming(vTimingCnt).Flush_Records + 1;
										 End If;					  
                  End If;
                
                  If cInternalErrorCode = 0 then
                     vStatus := 'SUCCESSFUL';
				          Else
                     vStatus := 'ERROR';
                     xxcp_wks.SOURCE_SUB_CODE(xxcp_wks.WORKING_RCD(j).Source_Pos):= 7008;
                  End If;
			      
				      xxcp_wks.SOURCE_STATUS(xxcp_wks.WORKING_RCD(j).Source_Pos) := vStatus;
            Else
              -- ZAV
              xxcp_wks.SOURCE_SUB_CODE(xxcp_wks.WORKING_RCD(j).Source_Pos):= 7008;
			        xxcp_wks.SOURCE_STATUS(xxcp_wks.WORKING_RCD(j).Source_Pos) := 'SUCCESSFUL';
            End If;
            
			      UDI_Cnt := UDI_Cnt + 1;
          
          
          END IF;
   
      END LOOP;
    
      If cInternalErrorCode = 0 and UDI_Cnt > 0 then
        FORALL j in 1 .. xxcp_wks.SOURCE_CNT
          Update XXCP_pv_interface
             Set vt_Status              = xxcp_wks.SOURCE_STATUS(j),
                 vt_internal_Error_code = Null,
                 vt_date_processed      = xxcp_global.SystemDate,
                 vt_Status_Code         = xxcp_wks.SOURCE_SUB_CODE(j)
           where rowid         = xxcp_wks.Source_rowid(j)
             and vt_preview_id = xxcp_global.gCommon(1).Preview_id;
      End If;
    
    END IF;
	
	 -- Source_Pos
  
    If cInternalErrorCode <> 0 then
      -- Rollback
      If vCnt > 0 then
        Rollback To TARGET_INSERT;
        xxcp_foundation.FndWriteError(cInternalErrorCode, vErrorMessage);
      End If;

      Error_Whole_Transaction(cInternalErrorCode);
    
      If cInternalErrorCode = 12800 then
        xxcp_foundation.FndWriteError(cInternalErrorCode,
          'Entered DR <' ||to_char(vEntered_DR) ||'> Entered CR <' ||to_char(vEntered_CR) || '>');
      End If;
    
    End If;
  
    -- Remove Array Elements
    ClearWorkingStorage;
  
    Return(vCnt);
  End FlushRecordsToTarget;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
  --  !! ***********************************************************************************************************
  --  !!
  --  !!                                   Reset_Errored_Transactions                                                                                                                                                                                                                                                                                                                                                                                                                                                   
  --  !!                              Reset Transactions from a previous run.                                                                                                                                                                                                                                                                                                                                                                                                                                           
  --  !!                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
  --  !! ***********************************************************************************************************                                                                                                                                                                                                                                                                                                                                                                                                    
  Procedure Reset_Errored_Transactions(cSource_assignment_id in number,
                                       cRequest_id           in number) is
  begin
  
    vTiming(vTimingCnt).Reset_Start := to_char(sysdate, 'HH24:MI:SS');
  
    -- Update Interface
    Begin
    
      Update XXCP_pv_interface r
         set vt_status              = 'NEW',
             vt_Internal_Error_Code = Null,
             vt_status_code         = Null,
             vt_request_id          = cRequest_id
       where vt_source_assignment_id = cSource_assignment_id
         and vt_request_id          = cRequest_id
			   and vt_preview_id           = xxcp_global.gCommon(1).Preview_id
         and (vt_status in
             ('ERROR', 'PROCESSING', 'TRANSACTION', 'ASSIGNMENT'));
    
    exception
      when OTHERS then
        null;
    End;
  
    -- Remove Errors 
    Begin
      Delete from XXCP_pv_errors cx
       where source_assignment_id = cSource_assignment_id
			 and preview_id             = xxcp_global.gCommon(1).Preview_id;

    Exception
      when OTHERS then
        null;
    End;
    Commit;
  End RESET_ERRORED_TRANSACTIONS;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
 -- !! ***********************************************************************************************************                                                                                                                                                                                                                                                                                                                                                                                                    
 -- !!                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
 -- !!                                  Set_Running_Status                                                                                                                                                                                                                                                                                                                                                                                                                                                            
 -- !!                      Flag the records that are going to be processed.                                                                                                                                                                                                                                                                                                                                                                                                                                          
 -- !!                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
 -- !! ***********************************************************************************************************                                                                                                                                                                                                                                                                                                                                                                                                    
  Procedure Set_Running_Status(cSource_id            in number,
                               cSource_assignment_id in number,
                               cRequest_id           in number) is
  
    cursor rx(pSource_id in number, pSource_Assignment_id in number, pRequest_id in number) is
      select r.vt_transaction_table,
             r.vt_transaction_id,
             r.rowid source_rowid,
             r.vt_transaction_type,
             nvl(t.grouping_rule_id,0) grouping_rule_id
        from XXCP_pv_interface r, 
             XXCP_sys_source_tables t
       where r.vt_preview_id = xxcp_global.gCommon(1).Preview_id
         and vt_source_assignment_id = pSource_assignment_id
         and vt_status               = 'NEW'
         and r.vt_transaction_table  = t.transaction_table
         and t.source_id             = pSource_id
    order by t.transaction_table
         for update;
  
    vGrouping_rule_id XXCP_gl_interface.vt_grouping_rule_id%type;
  
    -- Find records with no vt_grouping_rule_id
    cursor er2(pSource_assignment_id in number) is
      select Distinct g.vt_transaction_table
        from XXCP_pv_interface g, XXCP_sys_source_tables t
       where g.vt_request_id = cRequest_id
         and g.vt_preview_id = xxcp_global.gCommon(1).Preview_id
         and g.vt_status = 'ASSIGNMENT'
         AND g.vt_transaction_table    = t.transaction_table
         and g.vt_source_assignment_id = pSource_assignment_id
         AND t.grouping_rule_id IS NOT NULL  -- 02.06.01
         and g.vt_grouping_rule_id is null;
  
    -- Find records with invalid Grouping rule 
    cursor er3(pSource_assignment_id in number) is
      select Distinct g.vt_transaction_table
        from XXCP_pv_interface   g, 
             XXCP_grouping_rules r, 
             XXCP_sys_source_tables t
       where g.vt_preview_id = xxcp_global.gCommon(1).Preview_id
         and g.vt_status = 'ASSIGNMENT'
         AND g.vt_transaction_table    = t.transaction_table
         and g.vt_source_assignment_id = pSource_assignment_id
         and g.vt_grouping_rule_id = r.grouping_rule_id(+)
         AND t.grouping_rule_id IS NOT NULL  -- 02.06.01
         and r.grouping_rule_id is null;
  
    vCurrent_request_id number;
    vGroupingRuleExists BOOLEAN     := FALSE;
    vPrevTransaction_Table XXCP_sys_source_tables.transaction_table%TYPE := '~NULL~';
  
  begin
  
    vTiming(vTimingCnt).Set_Running_Start := to_char(sysdate, 'HH24:MI:SS');

    vCurrent_Request_id := nvl(cRequest_id, 0);
  
    For rec in rx(cSource_id, cSource_assignment_id, vCurrent_Request_id) Loop
      vGrouping_rule_id := Null;

      -- Populate Transaction_Table Array
      if vPrevTransaction_Table <> rec.vt_transaction_table then
        gTrans_Table_Array.extend;
        gTrans_Table_Array(gTrans_Table_Array.count) := rec.vt_transaction_table;
      End if;

      vPrevTransaction_Table := rec.vt_transaction_table;

      IF Rec.grouping_rule_id is not null then  -- 02.06.02
      
        vGroupingRuleExists := TRUE;
      
				xxcp_global.gCommon(1).current_transaction_table := Rec.vt_Transaction_Table;
        xxcp_global.gCommon(1).current_transaction_id    := Rec.vt_Transaction_id;

        vGrouping_rule_id := xxcp_custom_events.get_group_rule_id(cSource_id,
                                                                  cSource_assignment_id,
                                                                  rec.source_rowid,
                                                                  rec.vt_transaction_table,
                                                                  rec.vt_transaction_type);

        If nvl(vGrouping_rule_id, 0) = 0 then
          vGrouping_rule_id := xxcp_foundation.Fetch_Default_Group_Rule(cSource_assignment_id,
                                                                        rec.vt_transaction_table);
        End If;
      End If;
      -- Set Processing Status   
      begin
        update XXCP_pv_interface r
           set vt_status           = 'ASSIGNMENT',
               vt_date_processed   = xxcp_global.SystemDate,
               vt_status_code      = Null,
               vt_request_id       = cRequest_id,
               vt_Grouping_rule_id = vGrouping_rule_id
         where r.rowid = rec.source_rowid
           and vt_preview_id             = xxcp_global.gCommon(1).Preview_id;
      
      Exception
        when OTHERS then null;
      End;
    End Loop;
  
    -- Error Checking 
    If vGroupingRuleExists then  -- 02.06.01 Don't Check If no Grouping Rule used.
      -- Find records with no vt_grouping_rule_id 
      For Rec in er2(cSource_assignment_id) loop
        update XXCP_pv_interface g
           set g.vt_status              = 'ERROR',
               g.vt_date_processed      = xxcp_global.SystemDate,
               g.vt_internal_error_code = 11088
         where g.vt_preview_id           = xxcp_global.gCommon(1).Preview_id
           and g.vt_status               = 'ASSIGNMENT'
           and g.vt_source_assignment_id = cSource_assignment_id
           and g.vt_transaction_table    = rec.vt_transaction_table;
      End Loop;
      -- Find records with invalid Grouping rule 
      For Rec in er3(cSource_assignment_id) loop
        update XXCP_pv_interface g
           set g.vt_status              = 'ERROR',
               g.vt_date_processed      = xxcp_global.SystemDate,
               g.vt_internal_error_code = 11089
         where g.vt_preview_id           = xxcp_global.gCommon(1).Preview_id
           and g.vt_status               = 'ASSIGNMENT'
           and g.vt_source_assignment_id = cSource_assignment_id
           and g.vt_transaction_table = rec.vt_transaction_table;
      End Loop;
    End If;
  
    Commit;
  
  End Set_Running_Status;
  
 --
 -- Investigate 
 --
 Function Investigate(cSource_Rowid in rowid, cPreview_id in number) return number is
 
   cursor c1 (pSource_Rowid in rowid, pPreview_id in number) is
   select f.vt_transaction_id, f.vt_transaction_table, f.vt_transaction_type, f.vt_transaction_class, f.vt_source_assignment_id,
          f.vt_parent_trx_id, f.vt_transaction_ref, f.vt_interface_id, f.vt_transaction_date
   from XXCP_pv_interface       f,
        XXCP_source_assignments s
   where s.source_assignment_id = f.vt_source_assignment_id
     and f.rowid          = pSource_Rowid
     and f.vt_preview_id  = pPreview_id;
     
   cursor c2 (pSource_Assignment_id in number, pTransaction_Table in varchar2) is
   select t.source_table_id
   from XXCP_sys_source_tables  t,
        XXCP_source_assignments s
   where t.source_id            = s.source_id
     and s.source_assignment_id = pSource_Assignment_id
     and t.transaction_table    = pTransaction_Table; 
 
  cursor c3 (pSource_table_id in number, pType in varchar2) is
   select c.transaction_set_id
   from XXCP_sys_source_types c,
        XXCP_sys_source_tables  t
   where c.source_table_id     = pSource_table_id
     and c.type                = pType;

  cursor c4 (pSource_table_id in number, pClass in varchar2) is
   select c.class
   from XXCP_sys_source_classes c,
        XXCP_sys_source_tables  t
   where c.source_table_id      = pSource_table_id
     and c.class                = pClass;
     
  cursor c5 (pSource_Assignment_id in number) is
   select 'Y' Found
   from xxcp_source_assignments c,
        xxcp_sys_sources s
   where c.Source_Assignment_id = pSource_Assignment_id
     and c.Source_id            = s.source_id
     and s.preview_ctl          = 5;
     
   vMsg      XXCP_errors.error_message%type;
   vErrCode  XXCP_errors.internal_error_code%type := 0;
     
 Begin 
   If cSource_Rowid is not null And gInvestigate_Errors = 'Y' then 
  
     For Rec1 in c1(pSource_Rowid => cSource_Rowid, pPreview_id => cPreview_id) loop
      
        XXCP_GLOBAL.GCOMMON(1).CURRENT_TRANSACTION_TABLE := REC1.VT_TRANSACTION_TABLE;
        XXCP_GLOBAL.GCOMMON(1).CURRENT_TRANSACTION_ID    := REC1.VT_TRANSACTION_ID;
        XXCP_GLOBAL.GCOMMON(1).CURRENT_PARENT_TRX_ID     := REC1.VT_PARENT_TRX_ID;
        XXCP_GLOBAL.GCOMMON(1).CURRENT_INTERFACE_ID      := REC1.VT_INTERFACE_ID; 
				XXCP_GLOBAL.GCOMMON(1).CURRENT_INTERFACE_REF     := REC1.VT_TRANSACTION_REF; 
             
        vErrCode := 201;
        vMsg     := 'Transaction Table <'||REC1.VT_TRANSACTION_TABLE||'>';
        
        For Rec2 in c2(Rec1.vt_source_assignment_id, rec1.vt_transaction_table) Loop
          vErrCode := 202;
          vMsg     := 'Transaction Type <'||Rec1.vt_transaction_type||'>';
          For Rec3 in c3(Rec2.Source_table_id, Rec1.vt_transaction_type) Loop
            vErrCode := 203;
            vMsg     := 'Transaction Class <'||Rec1.vt_transaction_class||'>';
            For Rec4 in c4(Rec2.Source_table_id, Rec1.vt_transaction_class) Loop 
              vErrCode := 0;
            End Loop;
            vErrCode := 209;
            vMsg     := 'Source Assignment <'||to_char(Rec1.vt_source_assignment_id)||'>';
            For Rec5 in c5(Rec1.vt_source_assignment_id) Loop
              vErrCode := 0;
            End Loop;            
          End Loop;
        End Loop;  
        
        If vErrCode = 0 then         
       
          If nvl(REC1.VT_PARENT_TRX_ID,0) = 0 then
           vErrCode := 204;
           vMsg := 'Invalid Parent Trx Id';
          ElsIf nvl(Rec1.VT_TRANSACTION_ID,0) = 0 then
           vErrCode := 205;
           vMsg := 'Invalid Transaction Id';
          ElsIf Rec1.VT_TRANSACTION_DATE is null then
           vErrCode := 206;
           vMsg := 'Invalid Transaction Date';
          ElsIf Rec1.VT_TRANSACTION_REF is null then
           vErrCode := 207;
           vMsg := 'Invalid Transaction Ref';
          ElsIf Rec1.VT_SOURCE_ASSIGNMENT_ID is null then
           vErrCode := 208;
           vMsg := 'Invalid Source Assignment_id';
          End If;
        End If;
        
        If vErrCode != 0 then
          xxcp_foundation.FndWriteError(vErrCode,vMsg);
        End If;
     End Loop;
   End If; 
   
   Return(vErrCode);
   
 End Investigate;
  
  --
  -- InterfaceFailureCheck
  --
  Procedure InterfaceFailureCheck(cSource_Assignment_id  in number,
                                  cRequest_id            in number,
                                  cPreview_id            in number) is
                                  
   Cursor FailureCheck(pRequest_id in number, pPreview_id in number) is
      select  f.vt_status              
             ,f.vt_interface_id        
             ,f.vt_source_assignment_id
             ,f.vt_transaction_table   
             ,f.vt_transaction_type    
             ,f.vt_transaction_class   
             ,f.vt_transaction_ref     
             ,f.vt_transaction_id      
             ,f.vt_parent_trx_id       
             ,f.vt_transaction_date
             ,f.Rowid Source_Rowid
        from XXCP_pv_interface f
       where f.vt_request_id = pRequest_id
         and f.vt_preview_id = pPreview_id
         and NOT f.vt_status in ('ERROR','SUCCESSFUL') for Update;
             
     vInternalErrorCode XXCP_pv_interface.vt_internal_error_code%type;
             
   Begin
   
    
     For Rec in FailureCheck(pRequest_id => cRequest_id, pPreview_id => cPreview_id) Loop
     
       xxcp_global.gCommon(1).current_transaction_table := Rec.vt_transaction_table;
       xxcp_global.gCommon(1).current_transaction_id    := Rec.vt_transaction_id;
			 xxcp_global.gcommon(1).current_interface_ref     := Rec.vt_transaction_ref; 
       xxcp_global.gCommon(1).current_parent_trx_id     := Rec.vt_parent_trx_id;
       xxcp_global.gCommon(1).current_source_activity   := 'PV';
       xxcp_global.gCommon(1).current_process_name      := 'Preview Engine';
       xxcp_global.gCommon(1).current_source_table_id   := 0;
       xxcp_global.gCommon(1).current_assignment_id     := cSource_Assignment_id;
       xxcp_global.gCommon(1).current_table_group_id    := 0;
       xxcp_global.gCommon(1).current_trx_group_id      := 0;
       xxcp_global.gCommon(1).current_source_rowid      := Rec.Source_Rowid;
       xxcp_global.gCommon(1).current_request_id        := cRequest_id; 

       vInternalErrorCode := Investigate(cSource_Rowid => Rec.Source_Rowid, cPreview_id => cPreview_id);
       
       If vInternalErrorCode = 0 then
         vInternalErrorCode := 299;
         xxcp_foundation.FndWriteError(vInternalErrorCode, 'Unable to Process Record.');
       End If;     
                              
       update XXCP_pv_interface x
          set x.vt_status = 'ERROR',
              x.vt_internal_error_code = vInternalErrorCode
          where x.rowid = Rec.Source_Rowid;
     End Loop;
     
     Commit;
     
   End InterfaceFailureCheck;

  --  Init 
  Procedure Init is
  
     Cursor pf2(pProfile_Name in varchar2, pCategory in varchar2) is
      select Profile_Value
        from XXCP_SYS_PROFILE 
       where profile_category = pCategory
         and profile_name = pProfile_Name; 
         
   Begin
   
     For Rec in pf2('REAL TIME IC PRICE MANDATORY','PV') Loop
       gIC_Price_Mandatory  := Rec.Profile_Value;
     End Loop;

     For Rec in pf2('INVESTIGATE UNPROCESSED ROWS','PV') Loop
       gInvestigate_Errors  := Rec.Profile_Value;   
     End Loop;

      For Rec in pf2('REAL TIME ERROR WHOLE REQUEST','EV') Loop
        gErrorWholeRequest := rec.profile_value;
      End Loop;     
      
      -- 02.06.06
      gDDL_Interface_Order_By := 'int.vt_transaction_id ';
      For Rec in pf2('INTERFACE ADDITIONAL ORDER BY','RT') Loop
        gDDL_Interface_Order_By := rec.profile_value;
      End Loop;

     gDDL_Interface_Select  := 
'Select int.vt_transaction_table
       ,int.vt_transaction_type
       ,int.vt_transaction_class
       ,int.vt_transaction_id
       ,int.vt_transaction_ref
       ,int.vt_parent_trx_id
       ,int.vt_source_assignment_id
       ,int.vt_transaction_date
       ,int.vt_interface_id
       ,s.set_of_books_id
       ,x.rowid source_rowid
       ,x.vt_source_rowid
       ,int.currency_code
       ,int.entered_dr
       ,int.entered_cr
       ,s.instance_id source_instance_id
       ,s.source_assignment_id
       ,s.source_id
       ,s.set_of_books_id source_set_of_books_id
       ,w.transaction_set_name
       ,nvl(w.transaction_set_id,0) transaction_set_id
       ,y.source_table_id
       ,y.source_type_id
       ,cx.source_class_id
       ,t.assignment_only
       ,t.calc_legal_exch_rate
from xxcp_pv_interface             x,
     xxcp_rt_interface           int,
     xxcp_source_assignments       s,
     xxcp_sys_source_tables        t,
     xxcp_sys_source_types         y,
     xxcp_sys_source_classes      cx,
     xxcp_source_transaction_sets  w
where x.vt_request_id           = :1
  and x.vt_status               = ''ASSIGNMENT''
  and x.vt_source_assignment_id = :2
  and x.vt_source_assignment_id = s.source_assignment_id
  and int.vt_transaction_table  = t.transaction_table
  and int.rowid                 = x.vt_source_rowid
  and t.source_id               = :3
  and t.table_group_id          = :4
  and int.vt_transaction_type   = y.type
  and y.latch_only              = ''N''
  and y.source_table_id         = t.source_table_id
  and int.vt_transaction_class  = cx.class
  and cx.latch_only             = ''N''
  and cx.source_table_id        = t.source_table_id
  and y.Transaction_Set_id      = w.Transaction_Set_id
  and x.vt_preview_id           = :5
order by int.vt_transaction_table ,
           w.sequence,
         int.vt_parent_trx_id';

                         
       If gDDL_Interface_Order_By is not null then
         gDDL_Interface_Select := gDDL_Interface_Select || ','||chr(10)||gDDL_Interface_Order_By;
       End If;     
   
   End Init;

 
 -- !! ***********************************************************************************************************
 -- !!
 -- !!                                    CONTROL
 -- !!                  External Procedure (Entry Point) to start the AR Engine.
 -- !!
 -- !! ***********************************************************************************************************

  Function Control(cSource_Group_id         IN NUMBER,
	                 cSource_Assignment_id    IN NUMBER,
									 cUnique_Request_id       IN NUMBER,
                   cPreview_id              IN NUMBER,
                   cParent_trx_id           IN NUMBER   DEFAULT 0,
                   cUser_id                 IN NUMBER,
                   cLogin_id                IN NUMBER,
									 cPV_Zero_Flag            IN VARCHAR2,
									 cSource_Rowid            IN Rowid default null) RETURN NUMBER is								 

  type gli_type is REF CURSOR;

  -- Record Type of required xxcp_rt_interface fields
  type gli_map_rec is record( 
          vt_transaction_table        xxcp_rt_interface.vt_transaction_table%type
         ,vt_transaction_type         xxcp_rt_interface.vt_transaction_type%type
         ,vt_transaction_class        xxcp_rt_interface.vt_transaction_class%type
         ,vt_transaction_id           xxcp_rt_interface.vt_transaction_id%type
         ,vt_transaction_ref          xxcp_rt_interface.vt_transaction_ref%type
         ,vt_parent_trx_id            xxcp_rt_interface.vt_parent_trx_id%type
         ,vt_source_assignment_id     xxcp_rt_interface.vt_source_assignment_id%type
         ,vt_transaction_date         xxcp_rt_interface.vt_transaction_date%type
         ,vt_interface_id             xxcp_rt_interface.vt_interface_id%type
         ,set_of_books_id             xxcp_source_assignments.set_of_books_id%type
         ,source_rowid                Rowid
         ,vt_source_rowid             Rowid
         ,currency_code               xxcp_rt_interface.currency_code%type
         ,entered_dr                  xxcp_rt_interface.entered_dr%type
         ,entered_cr                  xxcp_rt_interface.entered_cr%type
         ,source_instance_id          xxcp_source_assignments.instance_id%type
         ,source_assignment_id        xxcp_source_assignments.source_assignment_id%type
         ,source_id                   xxcp_source_assignments.source_id%type
         ,source_set_of_books_id      xxcp_source_assignments.set_of_books_id%type
         ,transaction_set_name        xxcp_source_transaction_sets.transaction_set_name%type
         ,transaction_set_id          xxcp_source_transaction_sets.transaction_set_id%type
         ,source_table_id             xxcp_sys_source_tables.source_table_id%type
         ,source_type_id              xxcp_sys_source_types.source_type_id%type
         ,source_class_id             xxcp_sys_source_classes.source_class_id%type
         ,assignment_only             xxcp_sys_source_tables.assignment_only%type
         ,calc_legal_exch_rate        xxcp_sys_source_tables.calc_legal_exch_rate%type
         );

    -- Assignment Cursor      
   cfg     gli_type;
   cfgRec  gli_map_rec;
  
  
    -- Transaction 
    cursor GLI(pSource_id in number, pSource_Assignment_id in Number, pRequest_id in number) is
       SELECT       G.VT_TRANSACTION_TABLE
                    ,G.VT_TRANSACTION_TYPE
                    ,G.VT_INTERFACE_ID
                    ,G.VT_TRANSACTION_CLASS
                    ,X.VT_SOURCE_ROWID
                    ,G.VT_TRANSACTION_ID
                    ,X.VT_TRANSACTION_REF
                    ,S.INSTANCE_ID VT_INSTANCE_ID
                    --
                    ,'RT' CATEGORY
                    ,'RT' SOURCE
                    ,G.VT_TRANSACTION_DATE ACCOUNTING_DATE
                    --
                    ,X.VT_PARENT_TRX_ID
                    ,X.ROWID SOURCE_ROWID
                    ,S.SET_OF_BOOKS_ID
                    ,NVL(G.ENTERED_DR,0)+NVL(G.ENTERED_CR,0) PARENT_ENTERED_AMOUNT
                    ,G.CURRENCY_CODE PARENT_ENTERED_CURRENCY
                    ,S.SOURCE_ASSIGNMENT_ID
                    ,S.SOURCE_ID
                    ,W.TRANSACTION_SET_NAME
                    ,W.TRANSACTION_SET_ID,
                    --
                    Y.SOURCE_TABLE_ID, Y.SOURCE_TYPE_ID, CX.SOURCE_CLASS_ID, T.CLASS_MAPPING_REQ
                    ,0 CODE_COMBINATION_ID
                    ,0 TRANSACTION_COST
                    ,t.calc_legal_exch_rate
                     from XXCP_rt_interface          g,
                          XXCP_pv_interface          x,
                          XXCP_source_assignments    s,
                          -- New
                          XXCP_sys_source_tables       t,
                          XXCP_sys_source_types        y,
                          XXCP_sys_source_classes      cx,
                          XXCP_source_transaction_sets w
                     where g.rowid                   = x.vt_source_rowid
										   and g.VT_REQUEST_ID           = pRequest_id
										   and x.vt_preview_id           = xxcp_global.gCommon(1).Preview_id
										   and x.vt_status               = 'TRANSACTION'
										   and x.vt_source_assignment_id = pSource_Assignment_ID
										   and x.vt_source_assignment_id = s.source_assignment_id
										   -- Table 
										   and g.vt_transaction_table = t.transaction_table
										   and t.source_id            = pSource_id
										   -- Type 
										   and g.vt_transaction_type  = y.type
										   and y.latch_only           = 'N'
										   and t.source_table_id      = y.source_table_id      
										   and t.source_table_id      = cx.source_table_id
										   and cx.latch_only          = 'N'
										   and cx.class               = g.vt_transaction_class
										   -- Transaction_Set_id
										   and y.Transaction_Set_id   = w.Transaction_Set_id
										   --
										   ORDER BY g.vt_transaction_table DESC, w.sequence, g.vt_parent_trx_id, g.vt_transaction_id;
                
    GLIRec GLI%Rowtype;
  
    Cursor ConfErr(pSource_Assignment_id in number, pRequest_id in number) is
      select Distinct k.vt_parent_trx_id, k.vt_transaction_table
        from XXCP_pv_interface k
       where k.vt_request_id = pRequest_id
         and k.vt_source_assignment_id = pSource_Assignment_id
         and vt_preview_id             = xxcp_global.gCommon(1).Preview_id
         and k.vt_status = 'ERROR';
  
    -- Assignments
    Cursor sr1(pSource_Assignment_id in number) is
      select set_of_books_id,
             Source_Assignment_id,
             instance_id source_instance_id,
             Stamp_Parent_Trx,
             Source_id
        from XXCP_source_assignments t
       where t.Source_Assignment_id = pSource_Assignment_id
			   and t.active = 'Y';
  
    vInternalErrorCode        XXCP_errors.INTERNAL_ERROR_CODE%type := 0;
    vExchange_Rate_Type       XXCP_tax_registrations.EXCHANGE_RATE_TYPE%type;
    vCommon_Exch_Curr         XXCP_tax_registrations.COMMON_EXCH_CURR%type;

    vGlobalPrecision          pls_integer := 2;
    CommitCnt                 pls_Integer := 0;
    vSource_Activity          varchar2(4) := 'RT';
    vJob_Status               Number(1) := 0;
  
    vTransaction_Class        XXCP_sys_source_classes.Class%type;
    vRequest_ID               XXCP_process_history.request_id%type;
  
    -- NEW PARAMETERS
    vCurrent_Parent_Trx_id    XXCP_transaction_attributes.parent_trx_id%type := 0;
    vTransaction_Type         XXCP_sys_source_types.Type%type;
 
    --
    i                          pls_integer := 0;
    k                          Number;
    j                          Integer := 0;
    
    vGlobal_Rounding_Tolerance Number := 0;
    vTransaction_Error         Boolean := False;
  
    -- Used for the REC Distribution
    vExtraLoop          boolean := False;
    vExtraClass         varchar2(30);
    --
    vSource_Assignment_id XXCP_source_assignments.source_assignment_id%type;
    vStamp_Parent_Trx     XXCP_source_assignments.stamp_parent_trx%type := 'N';
    vSource_instance_id   XXCP_source_assignments.instance_id%type;
    vSource_ID            XXCP_sys_sources.source_id%type;

    g number;
   
    Cursor SF(pSource_Group_id in number) is
      select s.source_activity,
             s.Source_id,
             Preview_ctl,
             Timing,
             Custom_events,
             s.Cached,
             s.DML_Compiled
         from XXCP_source_assignment_groups g, XXCP_sys_sources s
       where source_group_id = pSource_Group_id
         and s.source_id = g.source_id
         and s.active = 'Y';
 
    Cursor Tolx is
      Select Numeric_Code Global_Rounding_Tolerance
        from XXCP_lookups
       where lookup_type = 'ROUNDING TOLERANCE'
         and lookup_code = 'GLOBAL';
  
    vClass_Mapping_Name  XXCP_sys_source_classes.Class%type;
	
    xw number := 0;
    vTiming_Start      number;
    vDML_Compiled      varchar2(1);
    
    vLastICPrice       XXCP_rt_interface.vt_ic_price%type;
    vLastICCurrency    XXCP_rt_interface.vt_ic_currency_code%type;
    vLastICPMID        XXCP_rt_interface.vt_ic_pricing_method_id%type;    
    vLastDftPMused     varchar2(1);
          
		Cursor ColTables(pSource_id in number) is
		select s.source_base_table from_table, t.transaction_table to_table 
      from XXCP_sys_target_tables t,
           XXCP_sys_sources s
     where t.source_id = s.source_id
       and s.source_id = pSource_id;		
  
  BEGIN
		
    Init; -- Put here for preview mode only.
  
    xxcp_global.User_id    := fnd_global.user_id;
    xxcp_global.Login_id   := fnd_global.login_id;
    xxcp_global.Trace_on   := xxcp_reporting.Get_Trace_Mode(cUser_id);
    xxcp_global.Preview_on := 'Y';
    xxcp_global.SystemDate := Sysdate;
    xxcp_global.gCommon(1).Preview_id := cPreview_id;
		xxcp_global.gCommon(1).Unique_Request_Id := cPreview_id;
		xxcp_global.gCommon(1).Preview_Zero_flag  := cPV_Zero_Flag;
    vTimingCnt := 1;
  
    For SFRec in SF(cSource_Group_id) loop
      xxcp_global.gCommon(1).Source_id   := SFRec.Source_id;
      xxcp_global.gCommon(1).Preview_ctl := SFRec.Preview_ctl;
      xxcp_global.gCommon(1).Preview_on  := xxcp_global.Preview_on;
      xxcp_global.gCommon(1).Custom_events           := SFRec.Custom_events;
      xxcp_global.gCommon(1).current_Source_activity := SFRec.Source_Activity;
      xxcp_global.gCommon(1).Cached_Attributes       := SFRec.Cached;
      xxcp_global.gCommon(1).Grouping_Rule           := vStamp_Parent_Trx;

      vSource_Activity := SFRec.Source_Activity;
      vSource_id       := SFRec.Source_id;
  	  vDML_Compiled    := SFRec.DML_Compiled;
    End Loop;
   
    xxcp_global.SET_SOURCE_ID(cSource_id => vSource_id);
  
    --     SYS.DBMS_SUPPORT.START_TRACE( waits=>true, binds=>true );
  
    vTiming(vTimingCnt).CF_Records    := 0;
    vTiming(vTimingCnt).Flush_Records := 0;
    vTiming(vTimingCnt).Start_time    := to_char(sysdate, 'HH24:MI:SS');
    vTiming(vTimingCnt).Flush         := 0;
    vTiming_Start            := xxcp_reporting.Get_Seconds;
  
    If xxcp_global.gCommon(1).Custom_events = 'Y' then
      xxcp_custom_events.Set_system_mode(xxcp_global.gCommon);
    End If;
	
	  If vDML_Compiled = 'N' then
	    -- Prevent Processing and the column rules have changed and
	    -- they need compiling.
	    vJob_Status := 4;
	    xxcp_foundation.show('ERROR: Column Rules have changed, but not re-generated'); 
	    xxcp_foundation.FndWriteError(10999,'Column Rules have changed, but not re-generated');

	  End If;
		
    --
    -- ## *******************************************************************************************************
    -- ##
    -- ##                            Check to See the process is already in use
    -- ##
    -- ## *******************************************************************************************************
    --
  
    If vJob_Status = 0 then
  	 		
      vRequest_id := cUnique_Request_id;
 
      xxcp_foundation.show('Activity locked....' || to_char(vRequest_id));
    
      xxcp_global.gCommon(1).Current_request_id := vRequest_id;
    
      If xxcp_global.gCommon(1).Custom_events = 'Y' then
        vInternalErrorCode := xxcp_custom_events.BEFORE_PROCESSING(vSource_id);
      End If;
      --
      -- ## **************************************************************************************************
      -- ##
      -- ##                                Setup Ready for the Run
      -- ##
      -- ## **************************************************************************************************
      --
      For REC in sr1(cSource_Assignment_id) Loop

        Exit when vJob_Status <> 0;
      
        vSource_instance_id   := rec.source_instance_id;
        vSource_Assignment_id := rec.Source_Assignment_id;
        --vStamp_Parent_Trx     := rec.Stamp_Parent_Trx;

        If vTimingCnt = 1 then
          vTiming(vTimingCnt).Init_Start := to_char(sysdate, 'HH24:MI:SS');
          xxcp_te_base.Engine_Init(cSource_id         => vSource_id,
				                           cSource_Group_id   => cSource_Group_id,
						  										 cInternalErrorCode => vInternalErrorCode);
      	
        End If;
								
				vTiming(vTimingCnt).Source_Assignment_id := vSource_Assignment_id; 
        -- Setup Globals
        xxcp_global.gCommon(1).current_process_name  := 'XXCP_RTENG';
        xxcp_global.gCommon(1).current_assignment_id := vSource_Assignment_ID;

        -- 02.06.01
        gTrans_Table_Array.delete;
      
        --
        -- *********************************************************************************************************
        --                            Populate Column Rules and Initialize Cursors
        -- *********************************************************************************************************
        --
        vTiming(vTimingCnt).Memory_Start := to_char(sysdate, 'HH24:MI:SS');

		    -- For debug purposes !!
			  For RecColTables in ColTables(pSource_id => vSource_id) loop
			     xxcp_memory_Pack.LoadColumnRules(vSource_id, RecColTables.from_table,vSource_instance_id,RecColTables.To_table,vRequest_id,vInternalErrorCode);
      	   XXCP_TE_BASE.Init_Cursors(RecColTables.To_table);
        End Loop;
 
        --
        -- *******************************************************************************************************
        --     Reset Previosly Errored Transactions and Assign Parent Trx Id to Incomming transactions
        -- *******************************************************************************************************
        --
        --   Reset_Errored_Transactions(cSource_assignment_id => vSource_Assignment_id,
        --	                           cRequest_id           => vRequest_id);
				
        Set_Running_Status(cSource_id            => vSource_id,
				                   cSource_assignment_id => vSource_Assignment_id, 
													 cRequest_id           => vRequest_id);

        Transaction_Group_Stamp(cStamp_Parent_Trx     => vStamp_Parent_Trx,
				                        cSource_assignment_id => vSource_Assignment_id, 
                                cSource_Rowid         => cSource_Rowid,
																cUser_id              => cUser_id,
																cInternalErrorCode    => vInternalErrorCode);

      
        vTiming(vTimingCnt).Init_End := to_char(sysdate, 'HH24:MI:SS');
  	
        --
        -- GLOBAL ROUNDING TOLERANCE
        --
        vGlobal_Rounding_Tolerance := 0;
        For Rec in Tolx loop
          vGlobal_Rounding_Tolerance := Rec.Global_Rounding_Tolerance;
        End loop;
      
        If vInternalErrorCode = 0 then
          --
          -- Get Standard Parameters
          --
					vInternalErrorCode := xxcp_foundation.fnd_gl_lookup(cExchange_Rate_Type => vExchange_Rate_Type,
                                                              cGlobalPrecision    => vGlobalPrecision,
                                                              cCommon_Exch_Curr   => vCommon_Exch_Curr);
        End If;
		
        Commit;
	  
        If vInternalErrorCode = 0 then
        
          --
          -- ## ***********************************************************************************************************
          -- ##
          -- ##                                Assignment
          -- ##
          -- ## ***********************************************************************************************************
          --
          i := 0;
        
          vTiming(vTimingCnt).CF_last     := xxcp_reporting.Get_Seconds;
          vTiming(vTimingCnt).CF          := 0;
          vTiming(vTimingCnt).CF_Records  := 0;
          vTiming(vTimingCnt).CF_Start_Date    := Sysdate;
          xxcp_global.SystemDate := sysdate;

          If xxcp_global.Trace_on = 'Y' and vTimingCnt = 1 then -- new trace
             xxcp_foundation.fndwriteerror(100,'TRACE(Interface Additional Order by)',gDDL_Interface_Order_By);
          End If;  
  	         
          Open CFG for gDDL_Interface_Select
                   Using vRequest_id, vSource_Assignment_id, vSource_id, 0 ,xxcp_global.gCommon(1).Preview_id;
          Loop

                Fetch CFG into CFGRec;
                Exit when CFG%notfound;
                
                i := i + 1;
              
                vTiming(vTimingCnt).CF_Records := vTiming(vTimingCnt).CF_Records + 1;
              
                xxcp_global.gCommon(1).current_source_rowid      := CFGRec.Source_Rowid;
                xxcp_global.gCommon(1).current_transaction_date  := CFGRec.vt_Transaction_Date;
                xxcp_global.gCommon(1).current_transaction_table := CFGRec.vt_Transaction_Table;
                xxcp_global.gCommon(1).current_transaction_id    := CFGRec.vt_Transaction_id;
								xxcp_global.gcommon(1).current_interface_ref     := CFGRec.vt_transaction_ref; 
                xxcp_global.gCommon(1).calc_legal_exch_rate      := CFGRec.calc_legal_exch_rate;
                xxcp_global.gCommon(1).current_parent_trx_id     := CFGRec.vt_Parent_Trx_id;
                xxcp_global.gCommon(1).current_Interface_id      := CFGRec.vt_interface_id;
                xxcp_global.gCommon(1).preview_parent_trx_id     := CFGRec.vt_parent_trx_id;
                xxcp_global.gCommon(1).preview_source_rowid      := CFGRec.vt_source_rowid;
              
                vTransaction_Class := CFGRec.VT_Transaction_Class;
              
                If XXCP_TE_BASE.Ignore_Class(CFGRec.VT_Transaction_Table, vTransaction_Class) then
                  Begin
                    update XXCP_pv_interface
                       set vt_status              = 'IGNORED',
                           vt_internal_error_code = Null,
                           vt_date_processed      = xxcp_global.SystemDate
                     where rowid = CFGRec.Source_Rowid
                       and vt_preview_id  = xxcp_global.gCommon(1).Preview_id;

                   Exception When OTHERS then Null;
                  End;
                Else

                  If CFGRec.Assignment_Only = 'Y' and xxcp_global.Trace_on = 'N' then
                    xxcp_global.gCommon(1).Cached_Attributes := 'N';
                  End If;
                
                  XXCP_TE_CONFIGURATOR.Control(
                                               cSource_Assignment_ID     => vSource_assignment_id,
                                               cSet_of_books_id          => CFGRec.Source_Set_of_books_id,
																							 cTransaction_Date         => CFGRec.VT_Transaction_Date,
																							 cSource_id                => vSource_id,
                                               cSource_Table_id          => CFGRec.Source_table_id,
                                               cSource_Type_id           => CFGRec.Source_type_id,
                                               cSource_Class_id          => CFGRec.Source_class_id,
                                               cTransaction_id           => CFGRec.VT_Transaction_ID,
                                               cSourceRowid              => CFGRec.Source_Rowid,
                                               cParent_Trx_id            => CFGRec.VT_Parent_Trx_id,
                                               cTransaction_Set_id       => CFGRec.Transaction_Set_id,
                                               cSpecial_Array            => 'N', 
                                               cKey                      => Null, 
																							 cTransaction_Table        => CFGRec.VT_Transaction_Table,
																							 cTransaction_Type         => CFGRec.VT_Transaction_Type,
																							 cTransaction_Class        => CFGRec.VT_Transaction_Class,
                                               cInternalErrorCode        => vInternalErrorCode);
                   															 
                  -- Check Status
                  If vInternalErrorCode = -1 then
                    Update XXCP_PV_INTERFACE
                       set vt_status              = 'SUCCESSFUL',
                           vt_internal_error_code = Null,
                           vt_date_processed      = xxcp_global.SystemDate,
                           vt_status_code         = 7001,
                           -- 02.05.07
                           vt_diagnostics1        = xxcp_global.Add_Diagnostics(null,1),
                           vt_diagnostics2        = xxcp_global.Add_Diagnostics(null,2) 
                     where rowid = CFGRec.Source_Rowid
                       and vt_preview_id  = xxcp_global.gCommon(1).Preview_id;
                  ElsIf nvl(vInternalErrorCode, 0) <> 0 then
                  
                    Error_Whole_Request(vInternalErrorCode);
                  
                    Update XXCP_PV_INTERFACE
                       set vt_status              = 'ERROR',
                           vt_internal_error_code = 12000,
                           vt_date_processed      = xxcp_global.SystemDate,
                           vt_results_attribute1   = xxcp_wks.gRealTime_Results.Attribute1, -- Cisco
                           vt_results_attribute2   = xxcp_wks.gRealTime_Results.Attribute2,
                           vt_results_attribute3   = xxcp_wks.gRealTime_Results.Attribute3,
                           vt_results_attribute4   = xxcp_wks.gRealTime_Results.Attribute4,
                           vt_results_attribute5   = xxcp_wks.gRealTime_Results.Attribute5,
                           vt_results_attribute6   = xxcp_wks.gRealTime_Results.Attribute6,
                           vt_results_attribute7   = xxcp_wks.gRealTime_Results.Attribute7,
                           vt_results_attribute8   = xxcp_wks.gRealTime_Results.Attribute8,
                           vt_results_attribute9   = xxcp_wks.gRealTime_Results.Attribute9,
                           vt_results_attribute10  = xxcp_wks.gRealTime_Results.Attribute10,
                           vt_results_attribute11  = xxcp_wks.gRealTime_Results.Attribute11, -- Cisco
                           vt_results_attribute12  = xxcp_wks.gRealTime_Results.Attribute12,
                           vt_results_attribute13  = xxcp_wks.gRealTime_Results.Attribute13,
                           vt_results_attribute14  = xxcp_wks.gRealTime_Results.Attribute14,
                           vt_results_attribute15  = xxcp_wks.gRealTime_Results.Attribute15,
                           vt_results_attribute16  = xxcp_wks.gRealTime_Results.Attribute16,
                           vt_results_attribute17  = xxcp_wks.gRealTime_Results.Attribute17,
                           vt_results_attribute18  = xxcp_wks.gRealTime_Results.Attribute18,
                           vt_results_attribute19  = xxcp_wks.gRealTime_Results.Attribute19,
                           vt_results_attribute20  = xxcp_wks.gRealTime_Results.Attribute20,
                           vt_results_attribute21  = xxcp_wks.gRealTime_Results.Attribute21, -- Cisco
                           vt_results_attribute22  = xxcp_wks.gRealTime_Results.Attribute22,
                           vt_results_attribute23  = xxcp_wks.gRealTime_Results.Attribute23,
                           vt_results_attribute24  = xxcp_wks.gRealTime_Results.Attribute24,
                           vt_results_attribute25  = xxcp_wks.gRealTime_Results.Attribute25,
                           vt_results_attribute26  = xxcp_wks.gRealTime_Results.Attribute26,
                           vt_results_attribute27  = xxcp_wks.gRealTime_Results.Attribute27,
                           vt_results_attribute28  = xxcp_wks.gRealTime_Results.Attribute28,
                           vt_results_attribute29  = xxcp_wks.gRealTime_Results.Attribute29,
                           vt_results_attribute30  = xxcp_wks.gRealTime_Results.Attribute30,
                            -- 02.06.02
                            vt_results_attribute31   = xxcp_wks.gRealTime_Results.Attribute31,
                            vt_results_attribute32   = xxcp_wks.gRealTime_Results.Attribute32,
                            vt_results_attribute33   = xxcp_wks.gRealTime_Results.Attribute33,
                            vt_results_attribute34   = xxcp_wks.gRealTime_Results.Attribute34,
                            vt_results_attribute35   = xxcp_wks.gRealTime_Results.Attribute35,
                            vt_results_attribute36   = xxcp_wks.gRealTime_Results.Attribute36,
                            vt_results_attribute37   = xxcp_wks.gRealTime_Results.Attribute37,
                            vt_results_attribute38   = xxcp_wks.gRealTime_Results.Attribute38,
                            vt_results_attribute39   = xxcp_wks.gRealTime_Results.Attribute39,
                            vt_results_attribute40   = xxcp_wks.gRealTime_Results.Attribute40,
                            vt_results_attribute41   = xxcp_wks.gRealTime_Results.Attribute41,
                            vt_results_attribute42   = xxcp_wks.gRealTime_Results.Attribute42,
                            vt_results_attribute43   = xxcp_wks.gRealTime_Results.Attribute43,
                            vt_results_attribute44   = xxcp_wks.gRealTime_Results.Attribute44,
                            vt_results_attribute45   = xxcp_wks.gRealTime_Results.Attribute45,
                            vt_results_attribute46   = xxcp_wks.gRealTime_Results.Attribute46,
                            vt_results_attribute47   = xxcp_wks.gRealTime_Results.Attribute47,
                            vt_results_attribute48   = xxcp_wks.gRealTime_Results.Attribute48,
                            vt_results_attribute49   = xxcp_wks.gRealTime_Results.Attribute49,
                            vt_results_attribute50   = xxcp_wks.gRealTime_Results.Attribute50,                           
                            -- 02.05.07
                            vt_diagnostics1        = xxcp_global.Add_Diagnostics(null,1),
                            vt_diagnostics2        = xxcp_global.Add_Diagnostics(null,2) 
                     where rowid = CFGRec.Source_Rowid
                       and vt_preview_id  = xxcp_global.gCommon(1).Preview_id;
                       
                       
                     If gErrorWholeRequest = 'Y' then
                       Exit;
                     End If;   

                  ElsIf nvl(vInternalErrorCode, 0) = 0 then
                  
                       xxcp_te_configurator.Get_Last_IC_Values(cLastICPrice    => vLastICPrice,
                                                               cLastICCurrency => vLastICCurrency,
                                                               cLastICPMID     => vLastICPMID,
                                                               cLastDftPMused  => vLastDftPMused
                                                               );
                                                               
                       If nvl(vLastICPrice,0) = 0 And  gIC_Price_Mandatory = 'Y' Then
                       
                         Error_Whole_Request(vInternalErrorCode);
                       
                         Update XXCP_pv_interface
                            set vt_status              = 'ERROR',
                                vt_internal_error_code = 12055,
                                vt_date_processed      = xxcp_global.SystemDate,
                                -- 02.04.04 
                                vt_status_code         = Decode(vLastDftPMused,'Y',7550,Null),
                                vt_results_attribute1   = xxcp_wks.gRealTime_Results.Attribute1, -- Cisco
                                vt_results_attribute2   = xxcp_wks.gRealTime_Results.Attribute2,
                                vt_results_attribute3   = xxcp_wks.gRealTime_Results.Attribute3,
                                vt_results_attribute4   = xxcp_wks.gRealTime_Results.Attribute4,
                                vt_results_attribute5   = xxcp_wks.gRealTime_Results.Attribute5,
                                vt_results_attribute6   = xxcp_wks.gRealTime_Results.Attribute6,
                                vt_results_attribute7   = xxcp_wks.gRealTime_Results.Attribute7,
                                vt_results_attribute8   = xxcp_wks.gRealTime_Results.Attribute8,
                                vt_results_attribute9   = xxcp_wks.gRealTime_Results.Attribute9,
                                vt_results_attribute10  = xxcp_wks.gRealTime_Results.Attribute10,
                                vt_results_attribute11   = xxcp_wks.gRealTime_Results.Attribute11, -- Cisco
                                vt_results_attribute12   = xxcp_wks.gRealTime_Results.Attribute12,
                                vt_results_attribute13   = xxcp_wks.gRealTime_Results.Attribute13,
                                vt_results_attribute14   = xxcp_wks.gRealTime_Results.Attribute14,
                                vt_results_attribute15   = xxcp_wks.gRealTime_Results.Attribute15,
                                vt_results_attribute16   = xxcp_wks.gRealTime_Results.Attribute16,
                                vt_results_attribute17   = xxcp_wks.gRealTime_Results.Attribute17,
                                vt_results_attribute18   = xxcp_wks.gRealTime_Results.Attribute18,
                                vt_results_attribute19   = xxcp_wks.gRealTime_Results.Attribute19,
                                vt_results_attribute20  = xxcp_wks.gRealTime_Results.Attribute20,
                                vt_results_attribute21   = xxcp_wks.gRealTime_Results.Attribute21, -- Cisco
                                vt_results_attribute22   = xxcp_wks.gRealTime_Results.Attribute22,
                                vt_results_attribute23   = xxcp_wks.gRealTime_Results.Attribute23,
                                vt_results_attribute24   = xxcp_wks.gRealTime_Results.Attribute24,
                                vt_results_attribute25   = xxcp_wks.gRealTime_Results.Attribute25,
                                vt_results_attribute26   = xxcp_wks.gRealTime_Results.Attribute26,
                                vt_results_attribute27   = xxcp_wks.gRealTime_Results.Attribute27,
                                vt_results_attribute28   = xxcp_wks.gRealTime_Results.Attribute28,
                                vt_results_attribute29   = xxcp_wks.gRealTime_Results.Attribute29,
                                vt_results_attribute30  = xxcp_wks.gRealTime_Results.Attribute30,
                                -- 02.06.02
                                vt_results_attribute31   = xxcp_wks.gRealTime_Results.Attribute31,
                                vt_results_attribute32   = xxcp_wks.gRealTime_Results.Attribute32,
                                vt_results_attribute33   = xxcp_wks.gRealTime_Results.Attribute33,
                                vt_results_attribute34   = xxcp_wks.gRealTime_Results.Attribute34,
                                vt_results_attribute35   = xxcp_wks.gRealTime_Results.Attribute35,
                                vt_results_attribute36   = xxcp_wks.gRealTime_Results.Attribute36,
                                vt_results_attribute37   = xxcp_wks.gRealTime_Results.Attribute37,
                                vt_results_attribute38   = xxcp_wks.gRealTime_Results.Attribute38,
                                vt_results_attribute39   = xxcp_wks.gRealTime_Results.Attribute39,
                                vt_results_attribute40   = xxcp_wks.gRealTime_Results.Attribute40,
                                vt_results_attribute41   = xxcp_wks.gRealTime_Results.Attribute41,
                                vt_results_attribute42   = xxcp_wks.gRealTime_Results.Attribute42,
                                vt_results_attribute43   = xxcp_wks.gRealTime_Results.Attribute43,
                                vt_results_attribute44   = xxcp_wks.gRealTime_Results.Attribute44,
                                vt_results_attribute45   = xxcp_wks.gRealTime_Results.Attribute45,
                                vt_results_attribute46   = xxcp_wks.gRealTime_Results.Attribute46,
                                vt_results_attribute47   = xxcp_wks.gRealTime_Results.Attribute47,
                                vt_results_attribute48   = xxcp_wks.gRealTime_Results.Attribute48,
                                vt_results_attribute49   = xxcp_wks.gRealTime_Results.Attribute49,
                                vt_results_attribute50   = xxcp_wks.gRealTime_Results.Attribute50,                           
                                -- 02.05.07
                                vt_diagnostics1        = xxcp_global.Add_Diagnostics(null,1),
                                vt_diagnostics2        = xxcp_global.Add_Diagnostics(null,2)                                 
                          where rowid = CFGRec.Source_Rowid
                            and vt_preview_id  = xxcp_global.gCommon(1).Preview_id;
                            
                          xxcp_foundation.FndWriteError(12055,'IC Price <'||nvl(vLastICPrice,0)||'>');
                       Else
                         Update XXCP_PV_INTERFACE
                            set vt_status              = Decode(CFGRec.Assignment_only,'Y','SUCCESSFUL','TRANSACTION'),
                                vt_internal_error_code = Null,
                                vt_date_processed      = xxcp_global.SystemDate,
                                vt_status_code         = Decode(vLastDftPMused,'Y',7550,Null),
                                -- 02.04.06
                                vt_ic_price             = nvl(vLastICPrice,0),
                                vt_ic_currency_code     = vLastICCurrency,
                                vt_ic_pricing_method_id = vLastICPMID,
                                -- 02.04.04 
                                vt_results_attribute1   = xxcp_wks.gRealTime_Results.Attribute1, -- Cisco
                                vt_results_attribute2   = xxcp_wks.gRealTime_Results.Attribute2,
                                vt_results_attribute3   = xxcp_wks.gRealTime_Results.Attribute3,
                                vt_results_attribute4   = xxcp_wks.gRealTime_Results.Attribute4,
                                vt_results_attribute5   = xxcp_wks.gRealTime_Results.Attribute5,
                                vt_results_attribute6   = xxcp_wks.gRealTime_Results.Attribute6,
                                vt_results_attribute7   = xxcp_wks.gRealTime_Results.Attribute7,
                                vt_results_attribute8   = xxcp_wks.gRealTime_Results.Attribute8,
                                vt_results_attribute9   = xxcp_wks.gRealTime_Results.Attribute9,
                                vt_results_attribute10   = xxcp_wks.gRealTime_Results.Attribute10,     
                                vt_results_attribute11   = xxcp_wks.gRealTime_Results.Attribute11, -- Cisco
                                vt_results_attribute12   = xxcp_wks.gRealTime_Results.Attribute12,
                                vt_results_attribute13   = xxcp_wks.gRealTime_Results.Attribute13,
                                vt_results_attribute14   = xxcp_wks.gRealTime_Results.Attribute14,
                                vt_results_attribute15   = xxcp_wks.gRealTime_Results.Attribute15,
                                vt_results_attribute16   = xxcp_wks.gRealTime_Results.Attribute16,
                                vt_results_attribute17   = xxcp_wks.gRealTime_Results.Attribute17,
                                vt_results_attribute18   = xxcp_wks.gRealTime_Results.Attribute18,
                                vt_results_attribute19   = xxcp_wks.gRealTime_Results.Attribute19,
                                vt_results_attribute20   = xxcp_wks.gRealTime_Results.Attribute20,
                                vt_results_attribute21   = xxcp_wks.gRealTime_Results.Attribute21, -- Cisco
                                vt_results_attribute22   = xxcp_wks.gRealTime_Results.Attribute22,
                                vt_results_attribute23   = xxcp_wks.gRealTime_Results.Attribute23,
                                vt_results_attribute24   = xxcp_wks.gRealTime_Results.Attribute24,
                                vt_results_attribute25   = xxcp_wks.gRealTime_Results.Attribute25,
                                vt_results_attribute26   = xxcp_wks.gRealTime_Results.Attribute26,
                                vt_results_attribute27   = xxcp_wks.gRealTime_Results.Attribute27,
                                vt_results_attribute28   = xxcp_wks.gRealTime_Results.Attribute28,
                                vt_results_attribute29   = xxcp_wks.gRealTime_Results.Attribute29,
                                vt_results_attribute30   = xxcp_wks.gRealTime_Results.Attribute30, 
                                vt_results_attribute31   = xxcp_wks.gRealTime_Results.Attribute31,
                                vt_results_attribute32   = xxcp_wks.gRealTime_Results.Attribute32,
                                vt_results_attribute33   = xxcp_wks.gRealTime_Results.Attribute33,
                                vt_results_attribute34   = xxcp_wks.gRealTime_Results.Attribute34,
                                vt_results_attribute35   = xxcp_wks.gRealTime_Results.Attribute35,
                                vt_results_attribute36   = xxcp_wks.gRealTime_Results.Attribute36,
                                vt_results_attribute37   = xxcp_wks.gRealTime_Results.Attribute37,
                                vt_results_attribute38   = xxcp_wks.gRealTime_Results.Attribute38,
                                vt_results_attribute39   = xxcp_wks.gRealTime_Results.Attribute39,
                                vt_results_attribute40   = xxcp_wks.gRealTime_Results.Attribute40,
                                vt_results_attribute41   = xxcp_wks.gRealTime_Results.Attribute41,
                                vt_results_attribute42   = xxcp_wks.gRealTime_Results.Attribute42,
                                vt_results_attribute43   = xxcp_wks.gRealTime_Results.Attribute43,
                                vt_results_attribute44   = xxcp_wks.gRealTime_Results.Attribute44,
                                vt_results_attribute45   = xxcp_wks.gRealTime_Results.Attribute45,
                                vt_results_attribute46   = xxcp_wks.gRealTime_Results.Attribute46,
                                vt_results_attribute47   = xxcp_wks.gRealTime_Results.Attribute47,
                                vt_results_attribute48   = xxcp_wks.gRealTime_Results.Attribute48,
                                vt_results_attribute49   = xxcp_wks.gRealTime_Results.Attribute49,
                                vt_results_attribute50   = xxcp_wks.gRealTime_Results.Attribute50,                           
                                -- 02.05.07
                                vt_diagnostics1        = xxcp_global.Add_Diagnostics(null,1),
                                vt_diagnostics2        = xxcp_global.Add_Diagnostics(null,2)                                                                     
                          where rowid = CFGRec.Source_Rowid
                            and vt_preview_id  = xxcp_global.gCommon(1).Preview_id;
                       End If;
                  End If;
                
                End If; -- End Assignment
                
                -- ##
                -- ## Commit Configured rows
                -- ##
              
                If i >= 1000 then
                  xxcp_global.SystemDate    := Sysdate;
                  Commit;
                  i           := 0;
                  -- !! Dont test for termination
                  Exit when vJob_Status > 0; -- Controlled Exit
                End If;
    			
    			    END Loop;
          Close CFG;   
          xxcp_dynamic_sql.Close_Session;
          
          -- #
          -- # Clear Common vars
          -- #
          xxcp_global.gCommon(1).current_transaction_table := Null;
          xxcp_global.gCommon(1).current_transaction_id    := Null;
				  xxcp_global.gcommon(1).current_interface_ref     := Null; 
          xxcp_global.gCommon(1).current_parent_trx_id     := Null;
        
          Commit; -- Final Configuration Commit.
        
          --
          If xxcp_global.gCommon(1).Custom_events = 'Y' then
            xxcp_custom_events.AFTER_ASSIGNMENT_PROCESSING(vSource_id,
                                                           vSource_assignment_id);
          End If;
        
          vTiming(vTimingCnt).CF := xxcp_reporting.Get_Seconds_Diff(vTiming(vTimingCnt).CF_last,xxcp_reporting.Get_Seconds);
          vTiming(vTimingCnt).CF_End_Date    := Sysdate;
        
          -- ***********************************************************************************************************
          --
          --  Set associated Transactions to error if anyone of the Transactions is a set has errored in configuration
          --
          -- ***********************************************************************************************************
          --
          xxcp_foundation.show('Reporting Configuration errors....');
          xxcp_global.SystemDate := Sysdate;
          vInternalErrorCode := 0;
          
          -- Check for Failures
          InterfaceFailureCheck(cSource_Assignment_id, cUnique_Request_id, cPreview_id);
          
          For ConfErrRec in ConfErr(vSource_Assignment_id, vRequest_id) loop
          
            Begin
              update XXCP_pv_interface
                 set vt_status              = 'ERROR',
                     vt_internal_Error_code = 12824, -- Associated errors
                     vt_date_processed      = xxcp_global.SystemDate
               where vt_request_id           = vRequest_id
                 and vt_Source_Assignment_id = vSource_Assignment_id
                 and vt_status               IN ('ASSIGNMENT', 'TRANSACTION')
                 and vt_parent_trx_id        = ConfErrRec.vt_parent_trx_id
                 and vt_transaction_table    = ConfErrRec.vt_Transaction_table
                 and vt_preview_id             = xxcp_global.gCommon(1).Preview_id;
            
            Exception
              when OTHERS then
                vInternalErrorCode := 12819; -- Update Failed.
            
            End;
          End Loop;
                  
          --## ***********************************************************************************************************
          --##
          --##                                    Transaction Engine
          --##
          --## ***********************************************************************************************************
        
          vTiming(vTimingCnt).TE_Last    := xxcp_reporting.Get_Seconds;
          vTiming(vTimingCnt).TE_Records := 0;
          vTiming(vTimingCnt).TE_Start_Date    := Sysdate;
        
          vExtraLoop             := False;
          vExtraClass            := Null;
          xxcp_global.SystemDate := Sysdate;
          i                := 0;
          
          If vInternalErrorCode = 0 and vJob_Status = 0 then
          
            xxcp_foundation.show('Transactions....');
            xxcp_wks.Reset_Source_Segments;
            gBalancing_Array      := xxcp_wks.Clear_Segments;
            ClearWorkingStorage;
            
            For  GLIRec in  GLI(vSOURCE_ID, vSOURCE_ASSIGNMENT_ID, vRequest_id) Loop
            
                  vInternalErrorCode := 0;
              
                  vTiming(vTimingCnt).TE_Records := vTiming(vTimingCnt).TE_Records + 1;
                  --
                  -- ***********************************************************************************************************
                  --        Set the balancing Setments to be written to the Column Rules Array
                  -- ***********************************************************************************************************
                  --
                  -- Poplulate the Balancing Array
                  xxcp_wks.Source_Balancing(1) := Null;
 
                  
                  xxcp_global.gCommon(1).preview_parent_trx_id   := GLIRec.VT_PARENT_TRX_ID;
                  xxcp_global.gCommon(1).preview_source_rowid    := GLIRec.vt_source_rowid;
                  xxcp_global.New_Transaction := 'N';

                  --
                  -- ***********************************************************************************************************
                  --         When a New Transaction is detected then flush the current buffer
                  -- ***********************************************************************************************************
                  --
                  If (vCurrent_Parent_Trx_id <> GLIRec.vt_parent_Trx_id) then
                  
                    vTransaction_Error := False;
                  
                    --
                    -- !! ******************************************************************************************************
                    --                             FLUSH TRANSACTION 
                    -- !! ******************************************************************************************************
                    --
                    If vCurrent_Parent_Trx_id <> 0 then
                      CommitCnt := CommitCnt + 1; -- Number of records process since last commit.
                    
                      k := FlushRecordsToTarget(vGlobal_Rounding_Tolerance,
                                                vGlobalPrecision,
                                                vTransaction_Type,
                                                vInternalErrorCode);
                    End If;
                    
                    --
                    -- ***********************************************************************************************************
                    --            Store Variables that are only set once per Parent Trx Id
                    -- ***********************************************************************************************************
                    --
                    xxcp_wks.Trace_Log     := Null;
                    xxcp_global.gCommon(1).current_transaction_table := GLIRec.vt_Transaction_Table;
			              xxcp_global.gcommon(1).current_interface_ref     := GLIRec.vt_Transaction_Ref;
                    xxcp_global.gCommon(1).current_parent_trx_id     := GLIRec.vt_Parent_Trx_id;
                    xxcp_global.gCommon(1).calc_legal_exch_rate      := GLIRec.calc_legal_exch_rate;
                    xxcp_global.New_Transaction := 'Y';

                    vCurrent_Parent_Trx_id := GLIRec.vt_Parent_Trx_id;
                    vTransaction_Type      := GLIRec.vt_Transaction_Type;
                    --
                    -- ***********************************************************************************************************
                    --                         Extra Loops for different Classes
                    -- ***********************************************************************************************************
                    --                  
                    vExtraLoop  := False;
                    vExtraClass := Null;
                    For j in 1 .. xxcp_global.gSRE.Count loop
                      xw := j;
                      If GLIRec.vt_Transaction_Table = xxcp_global.gSRE(j).Transaction_table then
                        vExtraClass := xxcp_global.gSRE(j).Extra_Record_type;
                        vExtraLoop  := TRUE;
                        Exit;
                      End If;
                    End Loop;
                  
                  Else
                    vExtraLoop  := FALSE; -- !!!!
                    vExtraClass := Null;
                  End If;
                
                  xxcp_global.gCommon(1).current_transaction_id := GLIRec.vt_Transaction_id;
                  --
                  -- ***********************************************************************************************************
                  --                                   Store Working Variables
                  -- ***********************************************************************************************************
                  --
                  xxcp_global.gCommon(1).current_source_rowid    := GLIRec.Source_Rowid;
                  xxcp_global.gCommon(1).current_Interface_id    := GLIRec.vt_interface_id;
                  xxcp_global.gCommon(1).current_Accounting_date := GLIRec.Accounting_Date;
                  --
                  -- ***********************************************************************************************************
                  --                                   Call the Transaction Engine
                  -- ***********************************************************************************************************
                  --
                  If vTransaction_Error = False then
                    vInternalErrorCode := 0; -- Reset for each row
                  
                    i := i + 1; -- Count the number of rows processed.
                  
                    xxcp_wks.Source_CNT  := xxcp_wks.SOURCE_CNT + 1;
                    xxcp_wks.Source_rowid(xxcp_wks.source_cnt)   := GLIRec.Source_Rowid;
                    xxcp_wks.SOURCE_SUB_CODE(xxcp_wks.source_cnt):= 0;
                    xxcp_wks.SOURCE_STATUS(xxcp_wks.source_cnt)  := '?'; 
                  
                    -- *********************************************************************
                    -- Class Mapping
                    -- *********************************************************************
										If GLIRec.class_mapping_req = 'Y' then
  									    vClass_Mapping_Name := xxcp_te_base.class_mapping(
												                                   cSource_id              => vSource_id,
										                                       cClass_Mapping_Required => GLIRec.class_mapping_req,
																													 cTransaction_Table      => GLIRec.vt_transaction_table,
																													 cTransaction_Type       => GLIRec.vt_transaction_type,
																													 cTransaction_Class      => GLIRec.vt_transaction_class,
																													 cTransaction_id         => GLIRec.vt_transaction_id,
																													 cCode_Combination_id    => GLIRec.code_combination_id,
																													 cAmount                 => GLIRec.Transaction_Cost);
                    End If;		
                    -- *********************************************************************
                    -- End Class Mapping
                    -- *********************************************************************
                  
                    XXCP_TE_ENGINE.Control(cSource_assignment_id    => vSource_Assignment_id,
                                           cSource_Table_id         => GLIRec.Source_Table_id,
																					 cSource_Type_id          => GLIRec.Source_Type_id,
                                           cSource_Class_id         => GLIRec.Source_Class_id,
                                           cTransaction_id          => GLIRec.vt_Transaction_id,
                                           cParent_Trx_id           => GLIRec.vt_Parent_Trx_id,
                                           cSourceRowid             => GLIRec.Source_Rowid,
                                           cParent_Entered_Amount   => GLIRec.Parent_Entered_Amount,
                                           cParent_Entered_Currency => GLIRec.Parent_Entered_Currency,
                                           cClass_Mapping           => GLIRec.class_mapping_req,
                                           cClass_Mapping_Name      => vClass_Mapping_Name,
                                           --
                                           cInterface_ID            => GLIRec.VT_Interface_id,
                                           cExtraLoop               => vExtraLoop,
                                           cExtraClass              => vExtraClass,
                                           cTransaction_set_id      => GLIRec.Transaction_Set_id,
																					 --
																					 cTransaction_Table       => GLIRec.VT_Transaction_Table,
																					 cTransaction_Type        => GLIRec.VT_Transaction_Type,
                                           cTransaction_Class       => GLIRec.vt_Transaction_Class,
                                           cInternalErrorCode       => vInternalErrorCode);
                  
     
                    xxcp_global.gCommon(1).current_trading_set := Null;
                  
                    -- Report Errors
                    If vInternalErrorCode <> 0 then
                      Error_Whole_Transaction(vInternalErrorCode);
                      vTransaction_Error := True;
                    End If;
                  
                  End If;
                  --
                  -- ***********************************************************************************************************
                  --                     Test to See if Oracle or the User has terminated this process
                  --                                   And Commit after Each 1000 Records processed
                  -- ***********************************************************************************************************
                  --
                  IF CommitCnt >= 100 THEN
                    CommitCnt := 0;
                    xxcp_global.SystemDate    := sysdate;
                    Commit;
                    -- !! Dont test for termination in Preview Mode
                    Exit when vJob_Status > 0; -- Controlled Exit
                  END IF;
                
                 END LOOP;
              
            xxcp_te_base.Close_Cursors;

            --
            --  !!***********************************************************************************************************
            --                     Flush the Buffer for a Final Time
            -- !! ***********************************************************************************************************
            --
            IF vCurrent_Parent_Trx_id <> 0 and vInternalErrorCode = 0 then
              If vInternalErrorCode = 0 then
                k := FlushRecordsToTarget(vGlobal_Rounding_Tolerance,
                                          vGlobalPrecision,
                                          vTransaction_type,
                                          vInternalErrorCode);
              End If;
            End If;
          End If; -- Internal Error Check GLI
        End If; -- End Internal Error Check
      
        vTiming(vTimingCnt).TE := xxcp_reporting.Get_Seconds_Diff(vTiming(vTimingCnt).TE_last,xxcp_reporting.Get_Seconds);
      
        xxcp_foundation.show('Clear down...');
        xxcp_global.SystemDate := Sysdate;
        -- *******************************************************************
        -- Error out records remaining with a Processing status after the run 
        -- *******************************************************************
        -- Check for Failures
        InterfaceFailureCheck(cSource_Assignment_id, cUnique_Request_id, cPreview_id);
    
        -- #
        -- # Clear Common vars
        -- #
        xxcp_global.gCommon(1).current_transaction_table := Null;
        xxcp_global.gCommon(1).current_transaction_id    := Null;
			  xxcp_global.gcommon(1).current_interface_ref     := Null; 
        xxcp_global.gCommon(1).current_parent_trx_id     := Null;
        --
        -- ***********************************************************************************************************
        --                     Clear Down
        -- ***********************************************************************************************************
        --
        ClearWorkingStorage;
        --
        -- ***********************************************************************************************************
        --                     DeActive the Lock in XXCP_ACTIVITY_CONTROL
        -- ***********************************************************************************************************
        --
        Commit;
        vTiming(vTimingCnt).TE_End_Date    := Sysdate;
				vTimingCnt := vTimingCnt + 1;
      End loop; -- SR1
    

      xxcp_te_base.ClearDown;
      xxcp_memory_pack.ClearDown;
      xxcp_dynamic_sql.Close_Session;
    
      If xxcp_global.gCommon(1).Custom_events = 'Y' then
        xxcp_custom_events.After_processing(cSource_id       => vSource_id,
                                            cSource_Group_id => cSource_Group_id);
      End If;
      Commit;

    End If;
  
    -- Check for Failures
    InterfaceFailureCheck(cSource_Assignment_id, cUnique_Request_id, cPreview_id);
    --
    xxcp_te_base.Write_Timings(vTiming, vTiming_Start);
    --
    -- ************************************************************************************************
    --                     Show Time Statistics when called from SQL*Plus
    -- ************************************************************************************************
    --
    xxcp_global.gCommon(1).Current_Request_id := Null;
    --
    -- ***********************************************************************************************************
    --                     Return Job Status Code and Finish!!
    -- ***********************************************************************************************************
    --
    Return(vJob_Status);
  End Control;


Begin
  -- Get System Date once
  xxcp_global.SystemDate := Sysdate;
  
  Init;

END XXCP_PV_RT_ENGINE;
/
