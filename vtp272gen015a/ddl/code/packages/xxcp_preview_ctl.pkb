CREATE OR REPLACE PACKAGE BODY XXCP_PREVIEW_CTL AS
/******************************************************************************
                        V I R T A L  T R A D E R  L I M I T E D
            (Copyright 2004 - 2012 Virtual Trader Ltd. All rights Reserved)

   NAME:       XXCP_PREVIEW_CTL
   PURPOSE:    Preview Mode Control

   REVISIONS:
   Ver        Date      Author  Description
   ---------  --------  ------- ------------------------------------
   02.01.00  28/01/04   Keith   First Creation
   02.01.01  16/03/04   Keith   Added Preview Control
   02.01.02  25/03/04   Keith   Added Validation call
   02.02.01  05/08/05   Keith   Added xxcp_vt_interface preview mode
   02.02.02  24/10/05   Keith   Control panel introduced
   02.02.03  30/10/05   Keith   Change Clear_Old_Preview_Rows
   02.02.04  06/12/05   Keith   Change to use_je_source for source 1
   02.03.02  28/04/06   Keith   Removed the STATUS from the default copy routine
   02.04.00  03/01/07   Keith   Added IM, OE, AP Sources.
   02.04.01  28/03/07   Keith   Added ED_QUALIFIER3 and 4
   02.04.03  29/01/08   Keith   Added Source 10,11,12 and Investigate code
   02.04.04  04/03/08   Keith   RT now using Preview_id = 5
   02.04.05  04/04/08   Keith   RT Now passing Preview_id to Copy routine.
   02.04.06  17/12/08   Simon   Added XXCPA.
   02.04.07  29/01/09   Keith   Added cleardown of xxcp_pv_cpa_history
   02.04.08  31/03/09   Nigel   Changed preview to treat every trx as a new trx.
   02.04.09  18/04/09   Keith   Call Investigate mode only when preview find an error
   02.04.10  18/05/09   Nigel   Changed copy_input_row to not include duplicates
   02.04.11  18/06/09   Simon   Change XXCPA to copy single record.
   02.04.12  28/10/09   Simon   XXCPA to look at XXCP_CPA_TRANS_HEADER_V.
   02.05.01  28/02/11   Simon   Added POH source 36.
   02.05.02  07/07/11   Simon   Added SO source 37.
   02.05.03  12/07/11   Keith   Generalized sources added. 8,16,17
   02.05.03A 21/11/11   Keith   Addec vt_reference1-5 to xxcp_gl_interface
   02.05.04  02/12/11   Keith   Preview Mode for Manual Journals
   02.05.05  10/01/12   Mark    Make Journal tables XXCP
   02.05.06  13/01/12   Mark    In Make_PV_GL_Interface, write the line_id to transaction_id.
   03.05.07  23/01/12   Mark    In Set_Restrictions test for cSource_Table - allow for XXCP_PV_GL_INTERFACE.   
   03.05.08  29/02/12   Nigel   Included a extra predicate to set_restrictions for preview manual journals.  
   03.05.09  04/04/12   Keith   Added XXCP_PV_TP_ENGINE 
   03.05.10  28/04/12   Mark    Remap reference fields for make_pv_gl_interface
   03.05.11  14/05/12   Nigel   Fix issue with previewing manual journals. 
   03.05.12  27/06/12   Nigel   Removed records from xxcp_pv_ic_Settlements_iface
                                Fixed Close_Cursor issue (OOD-120).
   03.05.13  17/07/12   Mat     Copy_Input_Row, Copy_Input_Row_31 - removed call to xxcp_pv_je_engine.Make_JE_Lines - not used
   03.05.14  12/10/12   Mat     OOD-349 -  Added Transaction References delete  
   03.05.15  31/10/12   Keith   Clear Tax Results table 
******************************************************************************/

  gInvestigate_Errors varchar2(1) := 'N';

  -- *************************************************************
  --                     Software Version Control
  --                   -----------------------------
  -- This package can be checked with SQL to ensure it's installed
  -- select packagename.software_version from dual;
  -- *************************************************************
  Function Software_Version Return varchar2 IS
   BEGIN
     RETURN('Version [03.05.15] Build Date [31-OCT-2012] Name [XXCP_PREVIEW_CTL]');
   END Software_Version;

  -- *************************************************************
  -- Set_Restrictions
  -- *************************************************************
 Procedure Set_Restrictions(cSource_Rowid in rowid, cPreview_Ctl in number, cPreview_id in number, cSource_Table in varchar2, cStatement in out varchar2) is

    -- User Profiles
    Cursor pf(pUser_id in number) is
     select f.pv_config_warning,  f.pv_restrict_parent
       from xxcp_user_profiles f
      where f.user_id = puser_id;

    vRS_GROUP_ID       Varchar2(50);
    vRS_Parent_Trx_id  Number;

   Begin
     
     -- 03.05.08 Make sure that you only look at your own records. 
     If cPreview_Ctl = 1  and upper(cSource_Table) = 'XXCP_PV_GL_INTERFACE' then
       cStatement := cStatement||' and vt_preview_id = '||cPreview_id;
     end if; 
     
     For Rec in pf(cPreview_id) Loop
      If Rec.pv_restrict_parent = 'Y' then
         If cPreview_Ctl = 1 then
           if upper(cSource_Table) = 'XXCP_PV_GL_INTERFACE' then
              Select to_char(nvl(Group_id,0)), vt_parent_trx_id
                     into vRS_GROUP_ID, vRS_Parent_Trx_id
                from xxcp_pv_gl_interface
               where rowid = cSource_Rowid;
           else
              Select to_char(nvl(Group_id,0)), vt_parent_trx_id
                into vRS_GROUP_ID, vRS_Parent_Trx_id
                from xxcp_gl_interface
               where rowid = cSource_Rowid;
           end if;               
              -- Group
              cStatement := cStatement||
                ' and nvl(Group_id,0) = '||vRS_GROUP_ID;
         End If;
      End If;

      Exit;
    End Loop;
  End Set_Restrictions;

 -- 
 -- MAKE_AP_CASH_CLEARING
 --
 Function Make_AP_Cash_Clearing(cSource_Group_id in number) return number is

  Cursor MX(pSource_Group_id in number) is
    select g.rowid master_payable_rowid
      from xxcp_gl_interface g,
           xxcp_source_assignments a
      where g.vt_status               = 'STAGED'
        and g.vt_transaction_table    = 'AP_INVOICE_PAYMENTS'
        and g.vt_transaction_class    in ('CASH CLEARING','CASH_CLEARING')
        and g.vt_source_assignment_id = a.source_assignment_id
        and a.source_group_id         = pSource_group_id
        and substr(a.active,1,1)      = 'Y'
        and a.source_id               = 9
        for update;

  vInternalErrorCode pls_integer := 0;        
                 
Begin    

   FOR REC IN MX(cSource_Group_id) LOOP
     Begin
        
       Insert into xxcp_gl_interface(
             STATUS
            ,SET_OF_BOOKS_ID
            ,ACCOUNTING_DATE
            ,CURRENCY_CODE
            ,DATE_CREATED
            ,CREATED_BY
            ,ACTUAL_FLAG
            ,USER_JE_CATEGORY_NAME
            ,USER_JE_SOURCE_NAME
            ,CURRENCY_CONVERSION_DATE
            ,ENCUMBRANCE_TYPE_ID
            ,BUDGET_VERSION_ID
            ,USER_CURRENCY_CONVERSION_TYPE
            ,CURRENCY_CONVERSION_RATE
            ,SEGMENT1,SEGMENT2,SEGMENT3,SEGMENT4,SEGMENT5
            ,SEGMENT6,SEGMENT7,SEGMENT8,SEGMENT9,SEGMENT10
            ,SEGMENT11,SEGMENT12,SEGMENT13,SEGMENT14,SEGMENT15
            ,ENTERED_DR
            ,ENTERED_CR
            ,ACCOUNTED_DR,ACCOUNTED_CR
            ,TRANSACTION_DATE
            ,REFERENCE1,REFERENCE2,REFERENCE3,REFERENCE4,REFERENCE5
            ,REFERENCE6,REFERENCE7,REFERENCE8,REFERENCE9,REFERENCE10
            ,REFERENCE11,REFERENCE12,REFERENCE13,REFERENCE14,REFERENCE15
            ,REFERENCE16,REFERENCE17,REFERENCE18,REFERENCE19,REFERENCE20
            ,REFERENCE21,REFERENCE22,REFERENCE23,REFERENCE24,REFERENCE25
            ,REFERENCE26,REFERENCE27,REFERENCE28,REFERENCE29,REFERENCE30
            ,JE_BATCH_ID,PERIOD_NAME,JE_HEADER_ID,JE_LINE_NUM
            ,CHART_OF_ACCOUNTS_ID
            ,FUNCTIONAL_CURRENCY_CODE
            ,CODE_COMBINATION_ID
            ,DATE_CREATED_IN_GL
            ,WARNING_CODE
            ,STATUS_DESCRIPTION
            ,STAT_AMOUNT
            ,GROUP_ID
            ,REQUEST_ID
            ,SUBLEDGER_DOC_SEQUENCE_ID
            ,SUBLEDGER_DOC_SEQUENCE_VALUE
            ,ATTRIBUTE1,ATTRIBUTE2,ATTRIBUTE3,ATTRIBUTE4,ATTRIBUTE5
            ,ATTRIBUTE6,ATTRIBUTE7,ATTRIBUTE8,ATTRIBUTE9,ATTRIBUTE10
            ,ATTRIBUTE11,ATTRIBUTE12,ATTRIBUTE13,ATTRIBUTE14,ATTRIBUTE15
            ,ATTRIBUTE16,ATTRIBUTE17,ATTRIBUTE18,ATTRIBUTE19,ATTRIBUTE20
            ,CONTEXT
            ,CONTEXT2
            ,INVOICE_DATE
            ,TAX_CODE
            ,INVOICE_IDENTIFIER
            ,INVOICE_AMOUNT
            ,CONTEXT3
            ,USSGL_TRANSACTION_CODE
            ,DESCR_FLEX_ERROR_MESSAGE
            ,JGZZ_RECON_REF
            ,AVERAGE_JOURNAL_FLAG
            ,VT_TRANSACTION_TABLE
            ,VT_TRANSACTION_CLASS
            ,VT_TRANSACTION_TYPE
            ,VT_TRANSACTION_ID
            ,VT_TRANSACTION_DATE
            ,VT_STATUS
            ,VT_SOURCE_ASSIGNMENT_ID
            ,VT_TRANSACTION_REF
            ,VT_TRANSACTION_LINE
            ,VT_REQUEST_ID
            ,VT_REFERENCE1
            ,VT_REFERENCE2
            ,VT_REFERENCE3
            ,VT_REFERENCE4
            ,VT_REFERENCE5
            )
           Select
             GLREC.STATUS
            ,GLREC.SET_OF_BOOKS_ID
            ,GLREC.ACCOUNTING_DATE
            ,GLREC.CURRENCY_CODE
            ,GLREC.DATE_CREATED
            ,GLREC.CREATED_BY
            ,GLREC.ACTUAL_FLAG
            ,GLREC.USER_JE_CATEGORY_NAME
            ,GLREC.USER_JE_SOURCE_NAME
            ,GLREC.CURRENCY_CONVERSION_DATE
            ,GLREC.ENCUMBRANCE_TYPE_ID
            ,GLREC.BUDGET_VERSION_ID
            ,GLREC.USER_CURRENCY_CONVERSION_TYPE
            ,GLREC.CURRENCY_CONVERSION_RATE
            ,SEGMENT1,SEGMENT2,SEGMENT3,SEGMENT4,SEGMENT5
            ,SEGMENT6,SEGMENT7,SEGMENT8,SEGMENT9,SEGMENT10
            ,SEGMENT11,SEGMENT12,SEGMENT13,SEGMENT14,SEGMENT15
            ,Decode(user_je_category_name,
                          'Reconciled Payments',decode(sign(p.amount),-1,null,abs(p.amount)),
                          decode(sign(p.amount),-1,abs(p.amount),null)) entered_DR
            ,Decode(user_je_category_name,
                          'Reconciled Payments',decode(sign(p.amount),-1,abs(p.amount),null),
                          decode(sign(p.amount),-1,null,abs(p.amount))) entered_CR
            ,NULL ACCOUNTED_DR
            ,NULL ACCOUNTED_CR
            ,GLREC.TRANSACTION_DATE
            ,GLREC.REFERENCE1,GLREC.REFERENCE2,GLREC.REFERENCE3,GLREC.REFERENCE4,GLREC.REFERENCE5
            ,GLREC.REFERENCE6,GLREC.REFERENCE7,GLREC.REFERENCE8,GLREC.REFERENCE9,GLREC.REFERENCE10
            ,GLREC.REFERENCE11,GLREC.REFERENCE12,GLREC.REFERENCE13,GLREC.REFERENCE14,GLREC.REFERENCE15
            ,GLREC.REFERENCE16,GLREC.REFERENCE17,GLREC.REFERENCE18,GLREC.REFERENCE19,GLREC.REFERENCE20
            ,GLREC.REFERENCE21,GLREC.REFERENCE22,GLREC.REFERENCE23,GLREC.REFERENCE24,GLREC.REFERENCE25
            ,GLREC.REFERENCE26,GLREC.REFERENCE27,GLREC.REFERENCE28,GLREC.REFERENCE29,GLREC.REFERENCE30
            ,GLREC.JE_BATCH_ID,GLREC.PERIOD_NAME,GLREC.JE_HEADER_ID,GLREC.JE_LINE_NUM
            ,GLREC.CHART_OF_ACCOUNTS_ID
            ,GLREC.FUNCTIONAL_CURRENCY_CODE
            ,GLREC.CODE_COMBINATION_ID
            ,GLREC.DATE_CREATED_IN_GL
            ,GLREC.WARNING_CODE
            ,GLREC.STATUS_DESCRIPTION
            ,GLREC.STAT_AMOUNT
            ,GLREC.GROUP_ID
            ,GLREC.REQUEST_ID
            ,GLREC.SUBLEDGER_DOC_SEQUENCE_ID
            ,GLREC.SUBLEDGER_DOC_SEQUENCE_VALUE
            ,GLREC.ATTRIBUTE1,GLREC.ATTRIBUTE2,GLREC.ATTRIBUTE3,GLREC.ATTRIBUTE4,GLREC.ATTRIBUTE5
            ,GLREC.ATTRIBUTE6,GLREC.ATTRIBUTE7,GLREC.ATTRIBUTE8,GLREC.ATTRIBUTE9,GLREC.ATTRIBUTE10
            ,GLREC.ATTRIBUTE11,GLREC.ATTRIBUTE12,GLREC.ATTRIBUTE13,GLREC.ATTRIBUTE14,GLREC.ATTRIBUTE15
            ,GLREC.ATTRIBUTE16,GLREC.ATTRIBUTE17,GLREC.ATTRIBUTE18,GLREC.ATTRIBUTE19,GLREC.ATTRIBUTE20
            ,GLREC.CONTEXT
            ,GLREC.CONTEXT2
            ,GLREC.INVOICE_DATE
            ,GLREC.TAX_CODE
            ,GLREC.INVOICE_IDENTIFIER
            ,GLREC.INVOICE_AMOUNT
            ,GLREC.CONTEXT3
            ,GLREC.USSGL_TRANSACTION_CODE
            ,GLREC.DESCR_FLEX_ERROR_MESSAGE
            ,GLREC.JGZZ_RECON_REF
            ,GLREC.AVERAGE_JOURNAL_FLAG
            --
            ,'AP_INVOICE_PAYMENTS'
            ,VT_TRANSACTION_CLASS
            ,VT_TRANSACTION_TYPE
            ,XXCP_TRANSACTION_ID_SEQ.NEXTVAL
            ,VT_TRANSACTION_DATE
            ,'NEW'
            ,VT_SOURCE_ASSIGNMENT_ID
            ,VT_TRANSACTION_REF                       
            ,TO_CHAR(P.INVOICE_PAYMENT_ID)
            ,xxcp_global.gCommon(1).Current_Request_id
            ,GLREC.VT_REFERENCE1
            ,GLREC.VT_REFERENCE2
            ,GLREC.VT_REFERENCE3
            ,GLREC.VT_REFERENCE4
            ,GLREC.VT_REFERENCE5
            FROM XXCP_GL_INTERFACE         GLREC
                ,AP_INVOICE_PAYMENTS_ALL P
            WHERE GLREC.ROWID                  = Rec.MASTER_PAYABLE_ROWID 
              AND TO_NUMBER(GLREC.REFERENCE23) = P.CHECK_ID;


            -- Remove temporary record. (Staged)
            DELETE from  XXCP_GL_INTERFACE F
              WHERE F.ROWID = REC.MASTER_PAYABLE_ROWID;
              
            EXCEPTION WHEN NO_DATA_FOUND THEN NULL;
                      WHEN OTHERS then vInternalErrorCode := 4013;
      End;
      
      Exit when vInternalErrorCode > 0;
      
    End Loop;
    
    If vInternalErrorCode > 0 then
      Rollback; 
    End If;    
    
    Return(vInternalErrorCode);
  End MAKE_AP_CASH_CLEARING;

--  !! ***********************************************************************************************************
--  !!                                    Copy_Input_Row
--  !!                  Create the Enties for XXCP_PV_INTERFACE Records
--  !! ***********************************************************************************************************
   Function Copy_Input_Row( cSource_id            in number
                           ,cSource_Rowid         in Rowid
                           ,cSource_Table         in varchar2
                           ,cPreview_id           in number
                           ,cSource_Assignment_id in number
                           ,cTransaction_Table    in varchar2
                           ,cTransaction_id       in number
                           ,cParent_Trx_id        in number
                           ,cUnique_Request_id    in number
                           ,cPV_Mode              in varchar2 default null
                           ,cPreview_ctl          in number) return boolean is

    v_cursorId   Integer;
    v_dummy      Integer;
    v_SelectSmt  varchar2(4000);
    vResult      boolean;
    vErrMsg      varchar2(512);
    vSQL_Call    varchar2(100);

    Cursor TH(pSource_Assignment_id in number, pTransaction_Table in varchar2, pParent_Trx_id in number) is
      Select x.header_id
      from xxcp_transaction_header_v x
      where x.source_assignment_id = pSource_Assignment_id
        and x.transaction_table    = pTransaction_Table
        and x.parent_trx_id        = pParent_Trx_id;

    vInternalErrorCode number;

    -- Source Assignments
    Cursor sr(pSource_id in number, pSource_Assignment_id in number) is
      select nvl(r.staged_records,'N') staged_records, f.source_group_id
      from xxcp_sys_sources r,
           xxcp_source_assignments f
      where r.source_id = psource_id
       and r.source_id = f.source_id
       and f.source_assignment_id = psource_assignment_id;

   BEGIN

      If cSource_id = 9 then
        For Rec in SR(cSource_id, cSource_Assignment_id) loop
          -- Explode AP Cash Clearing
          If Rec.Staged_Records = 'Y' then
            vInternalErrorCode := Make_AP_Cash_Clearing(Rec.Source_Group_id);
          End If;
        End Loop;
/*      ElsIf cSource_id = 25 then -- JE Lines
        For Rec in SR(cSource_id, cSource_Assignment_id) loop
          If Rec.Staged_Records = 'Y' then
            vInternalErrorCode := xxcp_pv_je_engine.Make_JE_Lines(Rec.Source_Group_id);
          End If;
        End Loop;*/
      End If;

      If cSource_id = 1 then
        v_SelectSmt :=
         'Insert into XXCP_PV_INTERFACE(
                 VT_PREVIEW_ID
                ,vt_source_rowid
                ,VT_INTERFACE_ID
                ,VT_STATUS
                ,vt_source_assignment_id
                ,VT_TRANSACTION_TABLE
                ,VT_TRANSACTION_CLASS
                ,VT_TRANSACTION_TYPE
                ,VT_TRANSACTION_ID
                ,VT_TRANSACTION_REF
                ,VT_TRANSACTION_GROUP
                ,VT_PARENT_TRX_ID
                ,VT_INTERNAL_ERROR_CODE
                ,VT_TRANSACTION_DATE
                ,VT_DATE_PROCESSED
                ,VT_CREATED_DATE
                )
                select
                 '||to_char(cPreview_id)||'
                ,Rowid
                ,VT_INTERFACE_ID
                ,Decode(vt_status,''FILTERED'',VT_STATUS,''NEW'')
                ,vt_source_assignment_id
                ,VT_TRANSACTION_TABLE
                ,VT_TRANSACTION_CLASS
                ,VT_TRANSACTION_TYPE
                ,VT_TRANSACTION_ID
                ,VT_TRANSACTION_REF
                ,VT_TRANSACTION_GROUP
                ,VT_PARENT_TRX_ID
                ,VT_INTERNAL_ERROR_CODE
                ,VT_TRANSACTION_DATE
                ,VT_DATE_PROCESSED
                ,SYSDATE
            from '||cSource_Table||' f '||
           ' where (f.vt_transaction_ref,f.vt_source_assignment_id,
                    f.user_je_source_name, f.user_je_category_name ) =
               (select r.vt_transaction_ref,r.vt_source_assignment_id,
                       r.user_je_source_name, r.user_je_category_name
                  from '||cSource_Table||' r
                 where r.rowid = :M0)
              and not exists (select ''x'' from xxcp_pv_interface where vt_preview_id = '||to_char(cPreview_id)||'
                                     and vt_source_rowid = f.rowid)
              and ''MATCH'' = (case
                               when rowid != :M0 and
                                    vt_transaction_id = '||to_char(cTransaction_id)||'   and 
                                    vt_transaction_table = '''||cTransaction_Table||''' and 
                                    vt_source_assignment_id = '||cSource_Assignment_id||' then
                                ''NO MATCH''
                               when rowid != :M0 and vt_status = ''DUPLICATE'' then
                                ''NO MATCH'' 
                               else
                                ''MATCH''
                             end)';

              Set_Restrictions(cSource_Rowid => cSource_Rowid, 
                               cPreview_Ctl  => cPreview_Ctl, 
                               cPreview_id   => cPreview_id, 
                               cSource_Table => cSource_Table,
                               cStatement    => v_SelectSmt);

      ElsIf cPreview_ctl = 1 then
             v_SelectSmt :=
         'Insert into XXCP_PV_INTERFACE(
                 VT_PREVIEW_ID
                ,vt_source_rowid
                ,VT_INTERFACE_ID
                ,VT_STATUS
                ,vt_source_assignment_id
                ,VT_TRANSACTION_TABLE
                ,VT_TRANSACTION_CLASS
                ,VT_TRANSACTION_TYPE
                ,VT_TRANSACTION_ID
                ,VT_TRANSACTION_REF
                ,VT_TRANSACTION_GROUP
                ,VT_PARENT_TRX_ID
                ,VT_INTERNAL_ERROR_CODE
                ,VT_TRANSACTION_DATE
                ,VT_DATE_PROCESSED
                ,VT_CREATED_DATE
                ,VT_REFERENCE1
                ,VT_REFERENCE2
                ,VT_REFERENCE3
                ,VT_REFERENCE4
                ,VT_REFERENCE5
                )
                select
                 '||to_char(cPreview_id)||'
                ,Rowid
                ,VT_INTERFACE_ID
                ,Decode(vt_status,''FILTERED'',VT_STATUS,''NEW'')
                ,vt_source_assignment_id
                ,VT_TRANSACTION_TABLE
                ,VT_TRANSACTION_CLASS
                ,VT_TRANSACTION_TYPE
                ,VT_TRANSACTION_ID
                ,VT_TRANSACTION_REF
                ,VT_TRANSACTION_GROUP
                ,VT_PARENT_TRX_ID
                ,VT_INTERNAL_ERROR_CODE
                ,VT_TRANSACTION_DATE
                ,VT_DATE_PROCESSED
                ,SYSDATE
                ,VT_REFERENCE1
                ,VT_REFERENCE2
                ,VT_REFERENCE3
                ,VT_REFERENCE4
                ,VT_REFERENCE5
           from '||cSource_Table||
           ' where (vt_transaction_ref,vt_transaction_table,vt_source_assignment_id) =
               (select r.vt_transaction_ref,vt_transaction_table,vt_source_assignment_id
                  from '||cSource_Table||' r
                  where rowid = :M0)
              and not exists (select ''x'' from xxcp_pv_interface where vt_preview_id = '||to_char(cPreview_id)||'
                                 and vt_source_rowid = :M0)
              and ''MATCH'' = (case
                               when rowid != :M0 and
                                    vt_transaction_id = '||to_char(cTransaction_id)||'   and 
                                    vt_transaction_table = '''||cTransaction_Table||''' and 
                                    vt_source_assignment_id = '||cSource_Assignment_id||' then
                                ''NO MATCH''
                               when rowid != :M0 and vt_status = ''DUPLICATE'' then
                                ''NO MATCH'' 
                               else
                                ''MATCH''
                             end)';

              Set_Restrictions(cSource_Rowid => cSource_Rowid, 
                               cPreview_Ctl  => cPreview_Ctl, 
                               cPreview_id   => cPreview_id, 
                               cSource_Table => cSource_Table,                               
                               cStatement    => v_SelectSmt);

      ElsIf cSource_id = 15 then
        v_SelectSmt :=
         'Insert into XXCP_PV_INTERFACE(
                 VT_PREVIEW_ID
                ,vt_source_rowid
                ,VT_INTERFACE_ID
                ,VT_STATUS
                ,vt_source_assignment_id
                ,VT_TRANSACTION_TABLE
                ,VT_TRANSACTION_CLASS
                ,VT_TRANSACTION_TYPE
                ,VT_TRANSACTION_ID
                ,VT_TRANSACTION_REF
                ,VT_TRANSACTION_GROUP
                ,VT_PARENT_TRX_ID
                ,VT_INTERNAL_ERROR_CODE
                ,VT_TRANSACTION_DATE
                ,VT_DATE_PROCESSED
                ,VT_CREATED_DATE
                )
                select
                 '||to_char(cPreview_id)||'
                ,Rowid
                ,VT_INTERFACE_ID
                ,''NEW''
                ,vt_source_assignment_id
                ,VT_TRANSACTION_TABLE
                ,VT_TRANSACTION_CLASS
                ,VT_TRANSACTION_TYPE
                ,VT_TRANSACTION_ID
                ,VT_TRANSACTION_REF
                ,VT_TRANSACTION_GROUP
                ,VT_PARENT_TRX_ID
                ,VT_INTERNAL_ERROR_CODE
                ,VT_TRANSACTION_DATE
                ,VT_DATE_PROCESSED
                ,SYSDATE
            from XXCP_VALIDATION_INTERFACE
           where rowid = :M0
              and not exists (select ''x'' from xxcp_pv_interface where vt_preview_id = '||to_char(cPreview_id)||'
                             and vt_source_rowid = :M0)';

      ElsIf cPreview_ctl in (5,19) then
        v_SelectSmt :=
         'Insert into XXCP_PV_INTERFACE(
                 VT_PREVIEW_ID
                ,VT_REQUEST_ID
                ,vt_source_rowid
                ,VT_INTERFACE_ID
                ,VT_STATUS
                ,vt_source_assignment_id
                ,VT_TRANSACTION_TABLE
                ,VT_TRANSACTION_CLASS
                ,VT_TRANSACTION_TYPE
                ,VT_TRANSACTION_ID
                ,VT_TRANSACTION_REF
                ,VT_TRANSACTION_GROUP
                ,VT_PARENT_TRX_ID
                ,VT_INTERNAL_ERROR_CODE
                ,VT_TRANSACTION_DATE
                ,VT_DATE_PROCESSED
                ,VT_CREATED_DATE
                )
                select
                 '||to_char(cPreview_id)||'
                ,VT_REQUEST_ID
                ,Rowid
                ,VT_INTERFACE_ID
                ,Decode(vt_status,''FILTERED'',VT_STATUS,''NEW'')
                ,vt_source_assignment_id
                ,VT_TRANSACTION_TABLE
                ,VT_TRANSACTION_CLASS
                ,VT_TRANSACTION_TYPE
                ,VT_TRANSACTION_ID
                ,VT_TRANSACTION_REF
                ,VT_TRANSACTION_GROUP
                ,VT_PARENT_TRX_ID
                ,VT_INTERNAL_ERROR_CODE
                ,VT_TRANSACTION_DATE
                ,VT_DATE_PROCESSED
                ,SYSDATE
            from '||cSource_Table||
           ' where (vt_transaction_ref,vt_transaction_table,vt_source_assignment_id, vt_request_id) =
               (select r.vt_transaction_ref,vt_transaction_table,vt_source_assignment_id, vt_request_id
                  from '||cSource_Table||' r
                  where rowid = :M0)
                  and not exists (select ''x'' from xxcp_pv_interface where vt_preview_id = '||to_char(cPreview_id)||'
                                     and vt_source_rowid = :M0)
                  and ''MATCH'' = (case
                                   when rowid != :M0 and
                                        vt_transaction_id = '||to_char(cTransaction_id)||'   and 
                                        vt_transaction_table = '''||cTransaction_Table||''' and 
                                        vt_source_assignment_id = '||cSource_Assignment_id||' then
                                    ''NO MATCH''
                                   when rowid != :M0 and vt_status = ''DUPLICATE'' then
                                    ''NO MATCH'' 
                                   else
                                    ''MATCH''
                                 end)';


      ElsIf cPreview_ctl = 13 then
      
        v_SelectSmt :=
         'Insert into XXCP_PV_INTERFACE(
                 VT_PREVIEW_ID
                ,vt_source_rowid
                ,VT_INTERFACE_ID
                ,VT_STATUS
                ,vt_source_assignment_id
                ,VT_TRANSACTION_TABLE
                ,VT_TRANSACTION_CLASS
                ,VT_TRANSACTION_TYPE
                ,VT_TRANSACTION_ID
                ,VT_TRANSACTION_REF
                ,VT_TRANSACTION_GROUP
                ,VT_PARENT_TRX_ID
                ,VT_INTERNAL_ERROR_CODE
                ,VT_TRANSACTION_DATE
                ,VT_DATE_PROCESSED
                ,VT_CREATED_DATE
                )
                select
                 '||to_char(cPreview_id)||'
                ,Rowid
                ,VT_INTERFACE_ID
                ,Decode(vt_status,''FILTERED'',VT_STATUS,''NEW'')
                ,vt_source_assignment_id
                ,VT_TRANSACTION_TABLE
                ,VT_TRANSACTION_CLASS
                ,VT_TRANSACTION_TYPE
                ,VT_TRANSACTION_ID
                ,VT_TRANSACTION_REF
                ,VT_TRANSACTION_GROUP
                ,VT_PARENT_TRX_ID
                ,VT_INTERNAL_ERROR_CODE
                ,VT_TRANSACTION_DATE
                ,VT_DATE_PROCESSED
                ,SYSDATE
            from xxcp_cost_plus_interface '||
           ' where (vt_transaction_table, vt_transaction_type,vt_transaction_ref, vt_source_assignment_id, vt_transaction_id) =
               (select r.vt_transaction_table, r.vt_transaction_type,r.vt_transaction_ref,vt_source_assignment_id, vt_transaction_id
                  from xxcp_cost_plus_interface r
                  where rowid = :M0)
                  and not exists (select ''x'' from xxcp_pv_interface where vt_preview_id = '||to_char(cPreview_id)||'
                                     and vt_source_rowid = :M0)';
      
      Else

        v_SelectSmt :=
         'Insert into XXCP_PV_INTERFACE(
                 VT_PREVIEW_ID
                ,vt_source_rowid
                ,VT_INTERFACE_ID
                ,VT_STATUS
                ,vt_source_assignment_id
                ,VT_TRANSACTION_TABLE
                ,VT_TRANSACTION_CLASS
                ,VT_TRANSACTION_TYPE
                ,VT_TRANSACTION_ID
                ,VT_TRANSACTION_REF
                ,VT_TRANSACTION_GROUP
                ,VT_PARENT_TRX_ID
                ,VT_INTERNAL_ERROR_CODE
                ,VT_TRANSACTION_DATE
                ,VT_DATE_PROCESSED
                ,VT_CREATED_DATE
                )
                select
                 '||to_char(cPreview_id)||'
                ,Rowid
                ,VT_INTERFACE_ID
                ,Decode(vt_status,''FILTERED'',VT_STATUS,''NEW'')
                ,vt_source_assignment_id
                ,VT_TRANSACTION_TABLE
                ,VT_TRANSACTION_CLASS
                ,VT_TRANSACTION_TYPE
                ,VT_TRANSACTION_ID
                ,VT_TRANSACTION_REF
                ,VT_TRANSACTION_GROUP
                ,VT_PARENT_TRX_ID
                ,VT_INTERNAL_ERROR_CODE
                ,VT_TRANSACTION_DATE
                ,VT_DATE_PROCESSED
                ,SYSDATE
            from '||cSource_Table||
           ' where (vt_transaction_table, vt_transaction_ref,vt_source_assignment_id) =
               (select r.vt_transaction_table, r.vt_transaction_ref,vt_source_assignment_id
                  from '||cSource_Table||' r
                  where rowid = :M0)
                  and not exists (select ''x'' from xxcp_pv_interface where vt_preview_id = '||to_char(cPreview_id)||'
                                     and vt_source_rowid = :M0)
                  and ''MATCH'' = (case
                                   when rowid != :M0 and
                                        vt_transaction_id = '||to_char(cTransaction_id)||'   and 
                                        vt_transaction_table = '''||cTransaction_Table||''' and 
                                        vt_source_assignment_id = '||cSource_Assignment_id||' then
                                    ''NO MATCH''
                                   when rowid != :M0 and vt_status = ''DUPLICATE'' then
                                    ''NO MATCH'' 
                                   else
                                    ''MATCH''
                                 end)';

       End If;

  Begin

     /*
     ## *****************************************************
     ##                 Bind Variables
     ## *****************************************************
     */
      vSQL_Call := 'Interface';

      v_cursorid  := DBMS_SQL.OPEN_CURSOR;
      DBMS_SQL.PARSE(v_cursorId, v_selectsmt, DBMS_SQL.native);
      DBMS_SQL.BIND_VARIABLE(v_cursorId, ':M0',cSource_Rowid);

     /*
     ## *****************************************************
     ##                 Execute SQL
     ## *****************************************************
     */
      v_dummy := DBMS_SQL.Execute(v_cursorid);

      vResult := True;
      DBMS_SQL.CLOSE_CURSOR(v_CursorId);
      commit;

      If Upper(substr(cPV_MODE,1,1)) = 'A' and nvl(cParent_Trx_id,0) > 0 then

        For Rec in TH(cSource_Assignment_id, cTransaction_Table, cParent_trx_id) loop
          Begin  -- 03.05.12
             vSQL_Call := 'Header';
             Insert into xxcp_pv_transaction_header(
                Preview_id
               ,HEADER_ID
               ,SOURCE_ASSIGNMENT_ID
               ,PARENT_TRX_ID
               ,SOURCE_TABLE_ID
               ,TRADING_SET_ID
               ,TRANSACTION_DATE
               ,SET_OF_BOOKS_ID
               ,OWNER_TAX_REG_ID
               ,OWNER_LEGAL_CURR
               ,OWNER_LEGAL_EXCH_RATE
               ,OWNER_ASSOC_ID
               ,OWNER_ASSIGN_RULE_ID
               ,EXCHANGE_DATE
               ,FROZEN
               ,INTERNAL_ERROR_CODE
               ,TRANSACTION_REF1
               ,TRANSACTION_REF2
               ,TRANSACTION_REF3
               ,TRANSACTION_CURRENCY
               ,TRANSACTION_EXCH_RATE
               ,CREATED_BY
               ,CREATION_DATE
               ,LAST_UPDATED_BY
               ,LAST_UPDATE_DATE
               ,LAST_UPDATE_LOGIN
              )
             Select
                cPreview_id
               ,HEADER_ID
               ,SOURCE_ASSIGNMENT_ID
               ,PARENT_TRX_ID
               ,SOURCE_TABLE_ID
               ,TRADING_SET_ID
               ,TRANSACTION_DATE
               ,SET_OF_BOOKS_ID
               ,OWNER_TAX_REG_ID
               ,OWNER_LEGAL_CURR
               ,OWNER_LEGAL_EXCH_RATE
               ,OWNER_ASSOC_ID
               ,OWNER_ASSIGN_RULE_ID
               ,EXCHANGE_DATE
               ,FROZEN
               ,INTERNAL_ERROR_CODE
               ,TRANSACTION_REF1
               ,TRANSACTION_REF2
               ,TRANSACTION_REF3
               ,TRANSACTION_CURRENCY
               ,TRANSACTION_EXCH_RATE
               ,CREATED_BY
               ,CREATION_DATE
               ,LAST_UPDATED_BY
               ,LAST_UPDATE_DATE
               ,LAST_UPDATE_LOGIN
              from xxcp_transaction_header h
                where h.header_id = rec.header_id;

              vSQL_Call := 'Attributes';

              Insert into xxcp_pv_transaction_attributes(
                    PREVIEW_ID
                   ,ATTRIBUTE_ID
                   ,HEADER_ID
                   ,SOURCE_ASSIGNMENT_ID
                   ,PARENT_TRX_ID
                   ,TRANSACTION_ID
                   ,TRADING_SET_ID
                   ,SOURCE_TABLE_ID
                   ,SOURCE_TYPE_ID
                   ,SOURCE_CLASS_ID
                   ,TRANSACTION_DATE
                   ,SET_OF_BOOKS_ID
                   ,PARTNER_REQUIRED
                   ,PARTNER_ASSOC_ID
                   ,PARTNER_TAX_REG_ID
                   ,PARTNER_LEGAL_EXCH_RATE
                   ,PARTNER_LEGAL_CURR
                   ,PARTNER_ASSIGN_RULE_ID
                   ,EXCHANGE_DATE
                   ,TC_QUALIFIER1
                   ,TC_QUALIFIER2
                   ,MC_QUALIFIER1
                   ,MC_QUALIFIER3
                   ,MC_QUALIFIER4
                   ,MC_QUALIFIER2
                   ,ED_QUALIFIER1
                   ,ED_QUALIFIER2
                   ,ED_QUALIFIER3
                   ,ED_QUALIFIER4
                   ,FROZEN
                   ,INTERNAL_ERROR_CODE
                   ,TRANSACTION_REF1
                   ,TRANSACTION_REF2
                   ,TRANSACTION_REF3
                   ,OWNER_TAX_REG_ID
                   ,ADJUSTMENT_RATE
                   ,ADJUSTMENT_RATE_ID
                   ,DOCUMENT_CURRENCY
                   ,DOCUMENT_EXCH_RATE
                   ,QUANTITY
                   ,UOM
                   ,IC_UNIT_PRICE
                   ,IC_TRADE_TAX_ID
                   ,IC_TAX_RATE
                   ,IC_CURRENCY
                   ,IC_CONTROL
                   ,PRICE_METHOD_ID
                   ,ATTRIBUTE1
                   ,ATTRIBUTE2
                   ,ATTRIBUTE3
                   ,ATTRIBUTE4
                   ,ATTRIBUTE5
                   ,ATTRIBUTE6
                   ,ATTRIBUTE7
                   ,ATTRIBUTE8
                   ,ATTRIBUTE9
                   ,ATTRIBUTE10
                   ,TAX_QUALIFIER1
                   ,TAX_QUALIFIER2
                   ,STEP_NUMBER
                   ,OLS_DATE
                   ,CREATED_BY
                   ,CREATION_DATE
                   ,LAST_UPDATED_BY
                   ,LAST_UPDATE_DATE
                   ,LAST_UPDATE_LOGIN
                )
                Select
                    cPreview_id
                   ,ATTRIBUTE_ID
                   ,HEADER_ID
                   ,SOURCE_ASSIGNMENT_ID
                   ,PARENT_TRX_ID
                   ,TRANSACTION_ID
                   ,TRADING_SET_ID
                   ,SOURCE_TABLE_ID
                   ,SOURCE_TYPE_ID
                   ,SOURCE_CLASS_ID
                   ,TRANSACTION_DATE
                   ,SET_OF_BOOKS_ID
                   ,PARTNER_REQUIRED
                   ,PARTNER_ASSOC_ID
                   ,PARTNER_TAX_REG_ID
                   ,PARTNER_LEGAL_EXCH_RATE
                   ,PARTNER_LEGAL_CURR
                   ,PARTNER_ASSIGN_RULE_ID
                   ,EXCHANGE_DATE
                   ,TC_QUALIFIER1
                   ,TC_QUALIFIER2
                   ,MC_QUALIFIER1
                   ,MC_QUALIFIER3
                   ,MC_QUALIFIER4
                   ,MC_QUALIFIER2
                   ,ED_QUALIFIER1
                   ,ED_QUALIFIER2
                   ,ED_QUALIFIER3
                   ,ED_QUALIFIER4
                   ,FROZEN
                   ,INTERNAL_ERROR_CODE
                   ,TRANSACTION_REF1
                   ,TRANSACTION_REF2
                   ,TRANSACTION_REF3
                   ,OWNER_TAX_REG_ID
                   ,ADJUSTMENT_RATE
                   ,ADJUSTMENT_RATE_ID
                   ,DOCUMENT_CURRENCY
                   ,DOCUMENT_EXCH_RATE
                   ,QUANTITY
                   ,UOM
                   ,IC_UNIT_PRICE
                   ,IC_TRADE_TAX_ID
                   ,IC_TAX_RATE
                   ,IC_CURRENCY
                   ,IC_CONTROL
                   ,PRICE_METHOD_ID
                   ,ATTRIBUTE1
                   ,ATTRIBUTE2
                   ,ATTRIBUTE3
                   ,ATTRIBUTE4
                   ,ATTRIBUTE5
                   ,ATTRIBUTE6
                   ,ATTRIBUTE7
                   ,ATTRIBUTE8
                   ,ATTRIBUTE9
                   ,ATTRIBUTE10
                   ,TAX_QUALIFIER1
                   ,TAX_QUALIFIER2
                   ,STEP_NUMBER
                   ,OLS_DATE
                   ,CREATED_BY
                   ,CREATION_DATE
                   ,LAST_UPDATED_BY
                   ,LAST_UPDATE_DATE
                   ,LAST_UPDATE_LOGIN
                from xxcp_transaction_attributes a
                where a.header_id = rec.header_id;

                vSQL_Call := 'Cache';
                Insert into xxcp_pv_transaction_cache(
                 PREVIEW_ID
                ,ATTRIBUTE_ID
                ,SEQ
                ,CACHED_QTY
                ,CACHED_VALUE1
                ,CACHED_VALUE2
                ,CACHED_VALUE3
                ,CACHED_VALUE4
                ,CACHED_VALUE5
                ,CACHED_VALUE6
                ,CACHED_VALUE7
                ,CACHED_VALUE8
                ,CACHED_VALUE9
                ,CACHED_VALUE10
                ,CACHED_VALUE11
                ,CACHED_VALUE12
                ,CACHED_VALUE13
                ,CACHED_VALUE14
                ,CACHED_VALUE15
                ,CACHED_VALUE16
                ,CACHED_VALUE17
                ,CACHED_VALUE18
                ,CACHED_VALUE19
                ,CACHED_VALUE20
                ,CACHED_VALUE21
                ,CACHED_VALUE22
                ,CACHED_VALUE23
                ,CACHED_VALUE24
                ,CACHED_VALUE25
                ,CACHED_VALUE26
                ,CACHED_VALUE27
                ,CACHED_VALUE28
                ,CACHED_VALUE29
                ,CACHED_VALUE30
                ,CACHED_VALUE31
                ,CACHED_VALUE32
                ,CACHED_VALUE33
                ,CACHED_VALUE34
                ,CACHED_VALUE35
                ,CACHED_VALUE36
                ,CACHED_VALUE37
                ,CACHED_VALUE38
                ,CACHED_VALUE39
                ,CACHED_VALUE40
                ,CACHED_VALUE41
                ,CACHED_VALUE42
                ,CACHED_VALUE43
                ,CACHED_VALUE44
                ,CACHED_VALUE45
                ,CACHED_VALUE46
                ,CACHED_VALUE47
                ,CACHED_VALUE48
                ,CACHED_VALUE49
                ,CACHED_VALUE50
                )
                Select
                 cPreview_id
                ,ATTRIBUTE_ID
                ,SEQ
                ,CACHED_QTY
                ,CACHED_VALUE1
                ,CACHED_VALUE2
                ,CACHED_VALUE3
                ,CACHED_VALUE4
                ,CACHED_VALUE5
                ,CACHED_VALUE6
                ,CACHED_VALUE7
                ,CACHED_VALUE8
                ,CACHED_VALUE9
                ,CACHED_VALUE10
                ,CACHED_VALUE11
                ,CACHED_VALUE12
                ,CACHED_VALUE13
                ,CACHED_VALUE14
                ,CACHED_VALUE15
                ,CACHED_VALUE16
                ,CACHED_VALUE17
                ,CACHED_VALUE18
                ,CACHED_VALUE19
                ,CACHED_VALUE20
                ,CACHED_VALUE21
                ,CACHED_VALUE22
                ,CACHED_VALUE23
                ,CACHED_VALUE24
                ,CACHED_VALUE25
                ,CACHED_VALUE26
                ,CACHED_VALUE27
                ,CACHED_VALUE28
                ,CACHED_VALUE29
                ,CACHED_VALUE30
                ,CACHED_VALUE31
                ,CACHED_VALUE32
                ,CACHED_VALUE33
                ,CACHED_VALUE34
                ,CACHED_VALUE35
                ,CACHED_VALUE36
                ,CACHED_VALUE37
                ,CACHED_VALUE38
                ,CACHED_VALUE39
                ,CACHED_VALUE40
                ,CACHED_VALUE41
                ,CACHED_VALUE42
                ,CACHED_VALUE43
                ,CACHED_VALUE44
                ,CACHED_VALUE45
                ,CACHED_VALUE46
                ,CACHED_VALUE47
                ,CACHED_VALUE48
                ,CACHED_VALUE49
                ,CACHED_VALUE50
                from xxcp_transaction_cache c
                where c.attribute_id = any(
                  select a.attribute_id
                     from xxcp_transaction_attributes a
                   where a.header_id = rec.header_id);
                Commit;
           Exception
             When Others then
               Null;
           End;
           
           Begin
             
/*             insert into xxcp_pv_tax_results(
                preview_id,
                attribute_id,
                tax_engine_id,
                tax_rate,
                tax_code,
                tax_amount,
                tax_rate_id,
                tax_gross_amount
--                tax_authority,
  --              tax_category_name
             )
             select cPreview_id,
                attribute_id,
                tax_engine_id,
                tax_rate,
                tax_code,
                tax_amount,
                tax_rate_id,
                tax_gross_amount
            --    tax_authority,
                --tax_category_name
             from xxcp_tax_results w
                where w.attribute_id = any(
                  select a.attribute_id
                     from xxcp_transaction_attributes a
                   where a.header_id = rec.header_id);*/

             Commit;
           
           Exception
             When Others then
               Null;
           End;
           
         End Loop;
      End If;

    Exception when OTHERS then
       vErrMsg := SQLERRM;
       vResult := False;
       xxcp_foundation.FndWriteError(506,vSQL_Call||' - '||vErrMsg,v_selectsmt);

       -- 03.06.12
       if DBMS_SQL.is_open(v_CursorId) then 
         DBMS_SQL.CLOSE_CURSOR(v_CursorId);
       end if;
  End ;

  -- Reset Status
  Begin
    Update xxcp_pv_interface
       set vt_status = 'NEW'
     where vt_status != 'ERROR' and vt_status != 'FILTERED'
       and vt_preview_id = cPreview_id;

    Exception when OTHERS then null;
  End;

  Return(vResult);

 End Copy_Input_Row;

--  !! ***********************************************************************************************************
--  !!                                    Copy_Input_Row_31
--  !!                  Create the Enties for XXCP_PV_INTERFACE Records for CPA (Source 31)
--  !! ***********************************************************************************************************
-- 02.04.12
   Function Copy_Input_Row_31( cSource_id            in number
                              ,cSource_Rowid         in Rowid
                              ,cSource_Table         in varchar2
                              ,cPreview_id           in number
                              ,cSource_Assignment_id in number
                              ,cTransaction_Table    in varchar2
                              ,cTransaction_id       in number
                              ,cParent_Trx_id        in number
                              ,cPV_Mode              in varchar2 default null
                              ,cPreview_ctl          in number) return boolean is

    v_cursorId   Integer;
    v_dummy      Integer;
    v_SelectSmt  varchar2(4000);
    vResult      boolean;
    vErrMsg      varchar2(512);
    vSQL_Call    varchar2(100);

    Cursor TH(pSource_Assignment_id in number, pTransaction_Table in varchar2, pParent_Trx_id in number) is
      Select x.header_id
      from xxcp_cpa_trans_header_v x
      where x.source_assignment_id = pSource_Assignment_id
        and x.transaction_table    = pTransaction_Table
        and x.parent_trx_id        = pParent_Trx_id;

    vInternalErrorCode number;

    -- Source Assignments
    Cursor sr(pSource_id in number, pSource_Assignment_id in number) is
      select nvl(r.staged_records,'N') staged_records, f.source_group_id
      from xxcp_sys_sources r,
           xxcp_source_assignments f
      where r.source_id = psource_id
       and r.source_id = f.source_id
       and f.source_assignment_id = psource_assignment_id;

   BEGIN

      If cSource_id = 9 then
        For Rec in SR(cSource_id, cSource_Assignment_id) loop
          -- Explode AP Cash Clearing
          If Rec.Staged_Records = 'Y' then
            vInternalErrorCode := Make_AP_Cash_Clearing(Rec.Source_Group_id);
          End If;
        End Loop;
/*      ElsIf cSource_id = 25 then -- JE Lines
        For Rec in SR(cSource_id, cSource_Assignment_id) loop
          If Rec.Staged_Records = 'Y' then
            vInternalErrorCode := xxcp_pv_je_engine.Make_JE_Lines(Rec.Source_Group_id);
          End If;
        End Loop;*/
      End If;

      If cSource_id = 1 then
        v_SelectSmt :=
         'Insert into XXCP_PV_INTERFACE(
                 VT_PREVIEW_ID
                ,vt_source_rowid
                ,VT_INTERFACE_ID
                ,VT_STATUS
                ,vt_source_assignment_id
                ,VT_TRANSACTION_TABLE
                ,VT_TRANSACTION_CLASS
                ,VT_TRANSACTION_TYPE
                ,VT_TRANSACTION_ID
                ,VT_TRANSACTION_REF
                ,VT_TRANSACTION_GROUP
                ,VT_PARENT_TRX_ID
                ,VT_INTERNAL_ERROR_CODE
                ,VT_TRANSACTION_DATE
                ,VT_DATE_PROCESSED
                ,VT_CREATED_DATE
                )
                select
                 '||to_char(cPreview_id)||'
                ,Rowid
                ,VT_INTERFACE_ID
                ,''NEW''
                ,vt_source_assignment_id
                ,VT_TRANSACTION_TABLE
                ,VT_TRANSACTION_CLASS
                ,VT_TRANSACTION_TYPE
                ,VT_TRANSACTION_ID
                ,VT_TRANSACTION_REF
                ,VT_TRANSACTION_GROUP
                ,VT_PARENT_TRX_ID
                ,VT_INTERNAL_ERROR_CODE
                ,VT_TRANSACTION_DATE
                ,VT_DATE_PROCESSED
                ,SYSDATE
            from '||cSource_Table||' f '||
           ' where (f.vt_transaction_ref,f.vt_source_assignment_id,
                    f.user_je_source_name, f.user_je_category_name ) =
               (select r.vt_transaction_ref,r.vt_source_assignment_id,
                       r.user_je_source_name, r.user_je_category_name
                  from '||cSource_Table||' r
                 where r.rowid = :M0)
              and not exists (select ''x'' from xxcp_pv_interface where vt_preview_id = '||to_char(cPreview_id)||'
                                     and vt_source_rowid = f.rowid)
              and ''MATCH'' = (case
                               when rowid != :M0 and
                                    vt_transaction_id = '||to_char(cTransaction_id)||'   and 
                                    vt_transaction_table = '''||cTransaction_Table||''' and 
                                    vt_source_assignment_id = '||cSource_Assignment_id||' then
                                ''NO MATCH''
                               when rowid != :M0 and vt_status = ''DUPLICATE'' then
                                ''NO MATCH'' 
                               else
                                ''MATCH''
                             end)';

              Set_Restrictions(cSource_Rowid => cSource_Rowid, 
                               cPreview_Ctl  => cPreview_Ctl, 
                               cPreview_id   => cPreview_id, 
                               cSource_Table => cSource_Table,                               
                               cStatement    => v_SelectSmt);

      ElsIf cPreview_ctl = 1 then
             v_SelectSmt :=
         'Insert into XXCP_PV_INTERFACE(
                 VT_PREVIEW_ID
                ,vt_source_rowid
                ,VT_INTERFACE_ID
                ,VT_STATUS
                ,vt_source_assignment_id
                ,VT_TRANSACTION_TABLE
                ,VT_TRANSACTION_CLASS
                ,VT_TRANSACTION_TYPE
                ,VT_TRANSACTION_ID
                ,VT_TRANSACTION_REF
                ,VT_TRANSACTION_GROUP
                ,VT_PARENT_TRX_ID
                ,VT_INTERNAL_ERROR_CODE
                ,VT_TRANSACTION_DATE
                ,VT_DATE_PROCESSED
                ,VT_CREATED_DATE
                )
                select
                 '||to_char(cPreview_id)||'
                ,Rowid
                ,VT_INTERFACE_ID
                ,''NEW''
                ,vt_source_assignment_id
                ,VT_TRANSACTION_TABLE
                ,VT_TRANSACTION_CLASS
                ,VT_TRANSACTION_TYPE
                ,VT_TRANSACTION_ID
                ,VT_TRANSACTION_REF
                ,VT_TRANSACTION_GROUP
                ,VT_PARENT_TRX_ID
                ,VT_INTERNAL_ERROR_CODE
                ,VT_TRANSACTION_DATE
                ,VT_DATE_PROCESSED
                ,SYSDATE
            from '||cSource_Table||
           ' where (vt_transaction_ref,vt_transaction_table,vt_source_assignment_id) =
               (select r.vt_transaction_ref,vt_transaction_table,vt_source_assignment_id
                  from '||cSource_Table||' r
                  where rowid = :M0)
              and not exists (select ''x'' from xxcp_pv_interface where vt_preview_id = '||to_char(cPreview_id)||'
                                 and vt_source_rowid = :M0)
              and ''MATCH'' = (case
                               when rowid != :M0 and
                                    vt_transaction_id = '||to_char(cTransaction_id)||'   and 
                                    vt_transaction_table = '''||cTransaction_Table||''' and 
                                    vt_source_assignment_id = '||cSource_Assignment_id||' then
                                ''NO MATCH''
                               when rowid != :M0 and vt_status = ''DUPLICATE'' then
                                ''NO MATCH'' 
                               else
                                ''MATCH''
                             end)';

              Set_Restrictions(cSource_Rowid => cSource_Rowid, 
                               cPreview_Ctl  => cPreview_Ctl, 
                               cPreview_id   => cPreview_id, 
                               cSource_Table => cSource_Table,                               
                               cStatement    => v_SelectSmt);

      ElsIf cSource_id = 15 then -- Validations
        v_SelectSmt :=
         'Insert into XXCP_PV_INTERFACE(
                 VT_PREVIEW_ID
                ,vt_source_rowid
                ,VT_INTERFACE_ID
                ,VT_STATUS
                ,vt_source_assignment_id
                ,VT_TRANSACTION_TABLE
                ,VT_TRANSACTION_CLASS
                ,VT_TRANSACTION_TYPE
                ,VT_TRANSACTION_ID
                ,VT_TRANSACTION_REF
                ,VT_TRANSACTION_GROUP
                ,VT_PARENT_TRX_ID
                ,VT_INTERNAL_ERROR_CODE
                ,VT_TRANSACTION_DATE
                ,VT_DATE_PROCESSED
                ,VT_CREATED_DATE
                )
                select
                 '||to_char(cPreview_id)||'
                ,Rowid
                ,VT_INTERFACE_ID
                ,''NEW''
                ,vt_source_assignment_id
                ,VT_TRANSACTION_TABLE
                ,VT_TRANSACTION_CLASS
                ,VT_TRANSACTION_TYPE
                ,VT_TRANSACTION_ID
                ,VT_TRANSACTION_REF
                ,VT_TRANSACTION_GROUP
                ,VT_PARENT_TRX_ID
                ,VT_INTERNAL_ERROR_CODE
                ,VT_TRANSACTION_DATE
                ,VT_DATE_PROCESSED
                ,SYSDATE
            from XXCP_VALIDATION_INTERFACE
           where rowid = :M0
              and not exists (select ''x'' from xxcp_pv_interface where vt_preview_id = '||to_char(cPreview_id)||'
                             and vt_source_rowid = :M0)';

      ElsIf cPreview_ctl in (5,19) then -- Realtime
        v_SelectSmt :=
         'Insert into XXCP_PV_INTERFACE(
                 VT_PREVIEW_ID
                ,VT_REQUEST_ID
                ,vt_source_rowid
                ,VT_INTERFACE_ID
                ,VT_STATUS
                ,vt_source_assignment_id
                ,VT_TRANSACTION_TABLE
                ,VT_TRANSACTION_CLASS
                ,VT_TRANSACTION_TYPE
                ,VT_TRANSACTION_ID
                ,VT_TRANSACTION_REF
                ,VT_TRANSACTION_GROUP
                ,VT_PARENT_TRX_ID
                ,VT_INTERNAL_ERROR_CODE
                ,VT_TRANSACTION_DATE
                ,VT_DATE_PROCESSED
                ,VT_CREATED_DATE
                )
                select
                 '||to_char(cPreview_id)||'
                ,VT_REQUEST_ID
                ,Rowid
                ,VT_INTERFACE_ID
                ,''NEW''
                ,vt_source_assignment_id
                ,VT_TRANSACTION_TABLE
                ,VT_TRANSACTION_CLASS
                ,VT_TRANSACTION_TYPE
                ,VT_TRANSACTION_ID
                ,VT_TRANSACTION_REF
                ,VT_TRANSACTION_GROUP
                ,VT_PARENT_TRX_ID
                ,VT_INTERNAL_ERROR_CODE
                ,VT_TRANSACTION_DATE
                ,VT_DATE_PROCESSED
                ,SYSDATE
            from '||cSource_Table||
           ' where (vt_transaction_ref,vt_transaction_table,vt_source_assignment_id, vt_request_id) =
               (select r.vt_transaction_ref,vt_transaction_table,vt_source_assignment_id, vt_request_id
                  from '||cSource_Table||' r
                  where rowid = :M0)
                  and not exists (select ''x'' from xxcp_pv_interface where vt_preview_id = '||to_char(cPreview_id)||'
                                     and vt_source_rowid = :M0)
                  and ''MATCH'' = (case
                                   when rowid != :M0 and
                                        vt_transaction_id = '||to_char(cTransaction_id)||'   and 
                                        vt_transaction_table = '''||cTransaction_Table||''' and 
                                        vt_source_assignment_id = '||cSource_Assignment_id||' then
                                    ''NO MATCH''
                                   when rowid != :M0 and vt_status = ''DUPLICATE'' then
                                    ''NO MATCH'' 
                                   else
                                    ''MATCH''
                                 end)';
      ElsIf cPreview_ctl = 13 then -- CPA
        v_SelectSmt :=
         'Insert into XXCP_PV_INTERFACE(
                 VT_PREVIEW_ID
                ,vt_source_rowid
                ,VT_INTERFACE_ID
                ,VT_STATUS
                ,vt_source_assignment_id
                ,VT_TRANSACTION_TABLE
                ,VT_TRANSACTION_CLASS
                ,VT_TRANSACTION_TYPE
                ,VT_TRANSACTION_ID
                ,VT_TRANSACTION_REF
                ,VT_TRANSACTION_GROUP
                ,VT_PARENT_TRX_ID
                ,VT_INTERNAL_ERROR_CODE
                ,VT_TRANSACTION_DATE
                ,VT_DATE_PROCESSED
                ,VT_CREATED_DATE
                )
                select
                 '||to_char(cPreview_id)||'
                ,Rowid
                ,VT_INTERFACE_ID
                ,''NEW''
                ,vt_source_assignment_id
                ,VT_TRANSACTION_TABLE
                ,VT_TRANSACTION_CLASS
                ,VT_TRANSACTION_TYPE
                ,VT_TRANSACTION_ID
                ,VT_TRANSACTION_REF
                ,VT_TRANSACTION_GROUP
                ,VT_PARENT_TRX_ID
                ,VT_INTERNAL_ERROR_CODE
                ,VT_TRANSACTION_DATE
                ,VT_DATE_PROCESSED
                ,SYSDATE
            from xxcp_cost_plus_interface '||
           ' where (vt_transaction_table, vt_transaction_type,vt_transaction_ref, vt_source_assignment_id, vt_transaction_id) =
               (select r.vt_transaction_table, r.vt_transaction_type,r.vt_transaction_ref,vt_source_assignment_id, vt_transaction_id
                  from xxcp_cost_plus_interface r
                  where rowid = :M0)
                  and not exists (select ''x'' from xxcp_pv_interface where vt_preview_id = '||to_char(cPreview_id)||'
                                     and vt_source_rowid = :M0)';
      Else -- Generic Sources
        v_SelectSmt :=
         'Insert into XXCP_PV_INTERFACE(
                 VT_PREVIEW_ID
                ,vt_source_rowid
                ,VT_INTERFACE_ID
                ,VT_STATUS
                ,vt_source_assignment_id
                ,VT_TRANSACTION_TABLE
                ,VT_TRANSACTION_CLASS
                ,VT_TRANSACTION_TYPE
                ,VT_TRANSACTION_ID
                ,VT_TRANSACTION_REF
                ,VT_TRANSACTION_GROUP
                ,VT_PARENT_TRX_ID
                ,VT_INTERNAL_ERROR_CODE
                ,VT_TRANSACTION_DATE
                ,VT_DATE_PROCESSED
                ,VT_CREATED_DATE
                )
                select
                 '||to_char(cPreview_id)||'
                ,Rowid
                ,VT_INTERFACE_ID
                ,''NEW''
                ,vt_source_assignment_id
                ,VT_TRANSACTION_TABLE
                ,VT_TRANSACTION_CLASS
                ,VT_TRANSACTION_TYPE
                ,VT_TRANSACTION_ID
                ,VT_TRANSACTION_REF
                ,VT_TRANSACTION_GROUP
                ,VT_PARENT_TRX_ID
                ,VT_INTERNAL_ERROR_CODE
                ,VT_TRANSACTION_DATE
                ,VT_DATE_PROCESSED
                ,SYSDATE
            from '||cSource_Table||
           ' where (vt_transaction_table, vt_transaction_ref,vt_source_assignment_id) =
               (select r.vt_transaction_table, r.vt_transaction_ref,vt_source_assignment_id
                  from '||cSource_Table||' r
                  where rowid = :M0)
                  and not exists (select ''x'' from xxcp_pv_interface where vt_preview_id = '||to_char(cPreview_id)||'
                                     and vt_source_rowid = :M0)
                  and ''MATCH'' = (case
                                   when rowid != :M0 and
                                        vt_transaction_id = '||to_char(cTransaction_id)||'   and 
                                        vt_transaction_table = '''||cTransaction_Table||''' and 
                                        vt_source_assignment_id = '||cSource_Assignment_id||' then
                                    ''NO MATCH''
                                   when rowid != :M0 and vt_status = ''DUPLICATE'' then
                                    ''NO MATCH'' 
                                   else
                                    ''MATCH''
                                 end)';

              Set_Restrictions(cSource_Rowid => cSource_Rowid, 
                               cPreview_Ctl  => cPreview_Ctl, 
                               cPreview_id   => cPreview_id, 
                               cSource_Table => cSource_Table,                               
                               cStatement    => v_SelectSmt);
       End If;

  Begin

     /*
     ## *****************************************************
     ##                 Bind Variables
     ## *****************************************************
     */
      vSQL_Call := 'Interface';

      v_cursorid  := DBMS_SQL.OPEN_CURSOR;
      DBMS_SQL.PARSE(v_cursorId, v_selectsmt, DBMS_SQL.native);
      DBMS_SQL.BIND_VARIABLE(v_cursorId, ':M0',cSource_Rowid);

     /*
     ## *****************************************************
     ##                 Execute SQL
     ## *****************************************************
     */
      v_dummy := DBMS_SQL.Execute(v_cursorid);

      vResult := True;
      DBMS_SQL.CLOSE_CURSOR(v_CursorId);
      commit;

      If Upper(substr(cPV_MODE,1,1)) = 'A' and nvl(cParent_Trx_id,0) > 0 then

        For Rec in TH(cSource_Assignment_id, cTransaction_Table, cParent_trx_id) loop
           vSQL_Call := 'Header';
           Insert into xxcp_pv_transaction_header(
              Preview_id
             ,HEADER_ID
             ,SOURCE_ASSIGNMENT_ID
             ,PARENT_TRX_ID
             ,SOURCE_TABLE_ID
             ,TRADING_SET_ID
             ,TRANSACTION_DATE
             ,SET_OF_BOOKS_ID
             ,OWNER_TAX_REG_ID
             ,OWNER_LEGAL_CURR
             ,OWNER_LEGAL_EXCH_RATE
             ,OWNER_ASSOC_ID
             ,OWNER_ASSIGN_RULE_ID
             ,EXCHANGE_DATE
             ,FROZEN
             ,INTERNAL_ERROR_CODE
             ,TRANSACTION_REF1
             ,TRANSACTION_REF2
             ,TRANSACTION_REF3
             ,TRANSACTION_CURRENCY
             ,TRANSACTION_EXCH_RATE
             ,CREATED_BY
             ,CREATION_DATE
             ,LAST_UPDATED_BY
             ,LAST_UPDATE_DATE
             ,LAST_UPDATE_LOGIN
            )
           Select
              cPreview_id
             ,HEADER_ID
             ,SOURCE_ASSIGNMENT_ID
             ,PARENT_TRX_ID
             ,SOURCE_TABLE_ID
             ,TRADING_SET_ID
             ,TRANSACTION_DATE
             ,SET_OF_BOOKS_ID
             ,OWNER_TAX_REG_ID
             ,OWNER_LEGAL_CURR
             ,OWNER_LEGAL_EXCH_RATE
             ,OWNER_ASSOC_ID
             ,OWNER_ASSIGN_RULE_ID
             ,EXCHANGE_DATE
             ,FROZEN
             ,INTERNAL_ERROR_CODE
             ,TRANSACTION_REF1
             ,TRANSACTION_REF2
             ,TRANSACTION_REF3
             ,TRANSACTION_CURRENCY
             ,TRANSACTION_EXCH_RATE
             ,CREATED_BY
             ,CREATION_DATE
             ,LAST_UPDATED_BY
             ,LAST_UPDATE_DATE
             ,LAST_UPDATE_LOGIN
            from xxcp_cpa_trans_header_v h
              where h.header_id = rec.header_id;

            vSQL_Call := 'Attributes';

            Insert into xxcp_pv_transaction_attributes(
                  PREVIEW_ID
                 ,ATTRIBUTE_ID
                 ,HEADER_ID
                 ,SOURCE_ASSIGNMENT_ID
                 ,PARENT_TRX_ID
                 ,TRANSACTION_ID
                 ,TRADING_SET_ID
                 ,SOURCE_TABLE_ID
                 ,SOURCE_TYPE_ID
                 ,SOURCE_CLASS_ID
                 ,TRANSACTION_DATE
                 ,SET_OF_BOOKS_ID
                 ,PARTNER_REQUIRED
                 ,PARTNER_ASSOC_ID
                 ,PARTNER_TAX_REG_ID
                 ,PARTNER_LEGAL_EXCH_RATE
                 ,PARTNER_LEGAL_CURR
                 ,PARTNER_ASSIGN_RULE_ID
                 ,EXCHANGE_DATE
                 ,TC_QUALIFIER1
                 ,TC_QUALIFIER2
                 ,MC_QUALIFIER1
                 ,MC_QUALIFIER3
                 ,MC_QUALIFIER4
                 ,MC_QUALIFIER2
                 ,ED_QUALIFIER1
                 ,ED_QUALIFIER2
                 ,ED_QUALIFIER3
                 ,ED_QUALIFIER4
                 ,FROZEN
                 ,INTERNAL_ERROR_CODE
                 ,TRANSACTION_REF1
                 ,TRANSACTION_REF2
                 ,TRANSACTION_REF3
                 ,OWNER_TAX_REG_ID
                 ,ADJUSTMENT_RATE
                 ,ADJUSTMENT_RATE_ID
                 ,DOCUMENT_CURRENCY
                 ,DOCUMENT_EXCH_RATE
                 ,QUANTITY
                 ,UOM
                 ,IC_UNIT_PRICE
                 ,IC_TRADE_TAX_ID
                 ,IC_TAX_RATE
                 ,IC_CURRENCY
                 ,IC_CONTROL
                 ,PRICE_METHOD_ID
                 ,ATTRIBUTE1
                 ,ATTRIBUTE2
                 ,ATTRIBUTE3
                 ,ATTRIBUTE4
                 ,ATTRIBUTE5
                 ,ATTRIBUTE6
                 ,ATTRIBUTE7
                 ,ATTRIBUTE8
                 ,ATTRIBUTE9
                 ,ATTRIBUTE10
                 ,TAX_QUALIFIER1
                 ,TAX_QUALIFIER2
                 ,STEP_NUMBER
                 ,OLS_DATE
                 ,CREATED_BY
                 ,CREATION_DATE
                 ,LAST_UPDATED_BY
                 ,LAST_UPDATE_DATE
                 ,LAST_UPDATE_LOGIN
              )
              Select
                  cPreview_id
                 ,ATTRIBUTE_ID
                 ,HEADER_ID
                 ,SOURCE_ASSIGNMENT_ID
                 ,PARENT_TRX_ID
                 ,TRANSACTION_ID
                 ,TRADING_SET_ID
                 ,SOURCE_TABLE_ID
                 ,SOURCE_TYPE_ID
                 ,SOURCE_CLASS_ID
                 ,TRANSACTION_DATE
                 ,SET_OF_BOOKS_ID
                 ,PARTNER_REQUIRED
                 ,PARTNER_ASSOC_ID
                 ,PARTNER_TAX_REG_ID
                 ,PARTNER_LEGAL_EXCH_RATE
                 ,PARTNER_LEGAL_CURR
                 ,PARTNER_ASSIGN_RULE_ID
                 ,EXCHANGE_DATE
                 ,TC_QUALIFIER1
                 ,TC_QUALIFIER2
                 ,MC_QUALIFIER1
                 ,MC_QUALIFIER3
                 ,MC_QUALIFIER4
                 ,MC_QUALIFIER2
                 ,ED_QUALIFIER1
                 ,ED_QUALIFIER2
                 ,ED_QUALIFIER3
                 ,ED_QUALIFIER4
                 ,FROZEN
                 ,INTERNAL_ERROR_CODE
                 ,TRANSACTION_REF1
                 ,TRANSACTION_REF2
                 ,TRANSACTION_REF3
                 ,OWNER_TAX_REG_ID
                 ,ADJUSTMENT_RATE
                 ,ADJUSTMENT_RATE_ID
                 ,DOCUMENT_CURRENCY
                 ,DOCUMENT_EXCH_RATE
                 ,QUANTITY
                 ,UOM
                 ,IC_UNIT_PRICE
                 ,IC_TRADE_TAX_ID
                 ,IC_TAX_RATE
                 ,IC_CURRENCY
                 ,IC_CONTROL
                 ,PRICE_METHOD_ID
                 ,ATTRIBUTE1
                 ,ATTRIBUTE2
                 ,ATTRIBUTE3
                 ,ATTRIBUTE4
                 ,ATTRIBUTE5
                 ,ATTRIBUTE6
                 ,ATTRIBUTE7
                 ,ATTRIBUTE8
                 ,ATTRIBUTE9
                 ,ATTRIBUTE10
                 ,TAX_QUALIFIER1
                 ,TAX_QUALIFIER2
                 ,STEP_NUMBER
                 ,OLS_DATE
                 ,CREATED_BY
                 ,CREATION_DATE
                 ,LAST_UPDATED_BY
                 ,LAST_UPDATE_DATE
                 ,LAST_UPDATE_LOGIN
              from xxcp_cpa_trans_attributes_v a
              where a.header_id = rec.header_id;

              vSQL_Call := 'Cache';
              Insert into xxcp_pv_transaction_cache(
               PREVIEW_ID
              ,ATTRIBUTE_ID
              ,SEQ
              ,CACHED_QTY
              ,CACHED_VALUE1
              ,CACHED_VALUE2
              ,CACHED_VALUE3
              ,CACHED_VALUE4
              ,CACHED_VALUE5
              ,CACHED_VALUE6
              ,CACHED_VALUE7
              ,CACHED_VALUE8
              ,CACHED_VALUE9
              ,CACHED_VALUE10
              ,CACHED_VALUE11
              ,CACHED_VALUE12
              ,CACHED_VALUE13
              ,CACHED_VALUE14
              ,CACHED_VALUE15
              ,CACHED_VALUE16
              ,CACHED_VALUE17
              ,CACHED_VALUE18
              ,CACHED_VALUE19
              ,CACHED_VALUE20
              ,CACHED_VALUE21
              ,CACHED_VALUE22
              ,CACHED_VALUE23
              ,CACHED_VALUE24
              ,CACHED_VALUE25
              ,CACHED_VALUE26
              ,CACHED_VALUE27
              ,CACHED_VALUE28
              ,CACHED_VALUE29
              ,CACHED_VALUE30
              ,CACHED_VALUE31
              ,CACHED_VALUE32
              ,CACHED_VALUE33
              ,CACHED_VALUE34
              ,CACHED_VALUE35
              ,CACHED_VALUE36
              ,CACHED_VALUE37
              ,CACHED_VALUE38
              ,CACHED_VALUE39
              ,CACHED_VALUE40
              ,CACHED_VALUE41
              ,CACHED_VALUE42
              ,CACHED_VALUE43
              ,CACHED_VALUE44
              ,CACHED_VALUE45
              ,CACHED_VALUE46
              ,CACHED_VALUE47
              ,CACHED_VALUE48
              ,CACHED_VALUE49
              ,CACHED_VALUE50
              )
              Select
               cPreview_id
              ,ATTRIBUTE_ID
              ,SEQ
              ,CACHED_QTY
              ,CACHED_VALUE1
              ,CACHED_VALUE2
              ,CACHED_VALUE3
              ,CACHED_VALUE4
              ,CACHED_VALUE5
              ,CACHED_VALUE6
              ,CACHED_VALUE7
              ,CACHED_VALUE8
              ,CACHED_VALUE9
              ,CACHED_VALUE10
              ,CACHED_VALUE11
              ,CACHED_VALUE12
              ,CACHED_VALUE13
              ,CACHED_VALUE14
              ,CACHED_VALUE15
              ,CACHED_VALUE16
              ,CACHED_VALUE17
              ,CACHED_VALUE18
              ,CACHED_VALUE19
              ,CACHED_VALUE20
              ,CACHED_VALUE21
              ,CACHED_VALUE22
              ,CACHED_VALUE23
              ,CACHED_VALUE24
              ,CACHED_VALUE25
              ,CACHED_VALUE26
              ,CACHED_VALUE27
              ,CACHED_VALUE28
              ,CACHED_VALUE29
              ,CACHED_VALUE30
              ,CACHED_VALUE31
              ,CACHED_VALUE32
              ,CACHED_VALUE33
              ,CACHED_VALUE34
              ,CACHED_VALUE35
              ,CACHED_VALUE36
              ,CACHED_VALUE37
              ,CACHED_VALUE38
              ,CACHED_VALUE39
              ,CACHED_VALUE40
              ,CACHED_VALUE41
              ,CACHED_VALUE42
              ,CACHED_VALUE43
              ,CACHED_VALUE44
              ,CACHED_VALUE45
              ,CACHED_VALUE46
              ,CACHED_VALUE47
              ,CACHED_VALUE48
              ,CACHED_VALUE49
              ,CACHED_VALUE50
              from xxcp_cpa_trans_cache_v c
              where c.attribute_id = any(
                select a.attribute_id
                   from xxcp_transaction_attributes a
                 where a.header_id = rec.header_id);
              Commit;
         End Loop;
      End If;

    Exception when OTHERS then
       vErrMsg := SQLERRM;
       vResult := False;
       xxcp_foundation.FndWriteError(506,vSQL_Call||' - '||vErrMsg,v_selectsmt);

       -- 03.06.12
       if DBMS_SQL.is_open(v_CursorId) then 
         DBMS_SQL.CLOSE_CURSOR(v_CursorId);
       end if;
  End ;

  -- Reset Status
  Begin
    Update xxcp_pv_interface
       set vt_status = 'NEW'
     where vt_status != 'ERROR'
       and vt_preview_id = cPreview_id;

    Exception when OTHERS then null;
  End;

  Return(vResult);

 End Copy_Input_Row_31;
 -- !! ***********************************************************************************************************
 -- !!                                    Clear_Old_Preview_Rows
 -- !!                  Remove Preview records that are over 25 sessions ago to prevent build up.
 -- !! ***********************************************************************************************************
 Procedure Clear_Old_Preview_Rows(cRemoval_Process in Number ) is
 Begin
     Begin
          If cRemoval_Process = 3 then -- All

          --  Delete from xxcp_pv_tax_results             where preview_id = xxcp_global.gCommon(1).Preview_id;
            Delete from xxcp_pv_transaction_cache       where preview_id = xxcp_global.gCommon(1).Preview_id;
            Delete from xxcp_pv_transaction_attributes  where preview_id = xxcp_global.gCommon(1).Preview_id;
            Delete from xxcp_pv_transaction_header      where preview_id = xxcp_global.gCommon(1).preview_id;
            Delete from xxcp_pv_process_history         where preview_id = xxcp_global.gCommon(1).preview_id;
            Delete from xxcp_pv_validation_results      where preview_id = xxcp_global.gCommon(1).preview_id;
            Delete from xxcp_pv_cpa_history             where preview_id = xxcp_global.gCommon(1).preview_id;
            -- 03.06.12
            Delete from xxcp_pv_ic_Settlement_iface     where preview_id = xxcp_global.gCommon(1).preview_id;
            -- 03.06.14
            Delete from xxcp_pv_transaction_references  where preview_id = xxcp_global.gCommon(1).preview_id; 

          ElsIf cRemoval_Process = 0 then -- None, Just the history
            Delete from xxcp_pv_process_history
             where preview_id = xxcp_global.gCommon(1).preview_id
              and interface_id =
               any(select vt_interface_id from xxcp_pv_interface where preview_id = xxcp_global.gCommon(1).preview_id);
          End If;

          Delete from xxcp_pv_interface where vt_preview_id = xxcp_global.gCommon(1).preview_id;
          Delete from xxcp_pv_errors    where preview_id = xxcp_global.gCommon(1).preview_id and internal_error_code > 0;

        Exception when OTHERS then null;
      End;
 End Clear_Old_Preview_Rows;

-- !! ***********************************************************************************************************
-- !! Investigate
-- !! ***********************************************************************************************************
 Procedure Investigate(cSource_Rowid in rowid, cPreview_id in number) is

   cursor c1 (pSource_Rowid in rowid, pPreview_id in number) is
   select f.vt_transaction_id, f.vt_transaction_table, f.vt_transaction_type, f.vt_transaction_class, f.vt_source_assignment_id,
          f.vt_parent_trx_id, f.vt_transaction_ref, f.vt_interface_id, s.source_id, f.vt_transaction_date
   from xxcp_pv_interface       f,
        xxcp_source_assignments s
   where s.source_assignment_id = f.vt_source_assignment_id
     and nvl(f.vt_internal_error_code,0) != 0
     and f.vt_source_rowid      = pSource_Rowid
     and f.vt_preview_id        = pPreview_id;

   cursor c2 (pSource_Assignment_id in number, pTransaction_Table in varchar2) is
   select t.source_table_id
   from xxcp_sys_source_tables  t,
        xxcp_source_assignments s
   where t.source_id            = s.source_id
     and s.source_assignment_id = pSource_Assignment_id
     and t.transaction_table    = pTransaction_Table;

  cursor c3 (pSource_table_id in number, pType in varchar2) is
   select c.transaction_set_id
   from xxcp_sys_source_types   c,
        xxcp_sys_source_tables  t
   where c.source_table_id = pSource_table_id
     and c.type            = pType;

  cursor c4 (pSource_table_id in number, pClass in varchar2) is
   select c.class
   from xxcp_sys_source_classes c,
        xxcp_sys_source_tables  t
   where c.source_table_id = pSource_table_id
     and c.class           = pClass;

  cursor c5 (pSource_id in number, pSource_Assignment_id in number) is
   select 'Y' Found
   from xxcp_source_assignments c
   where c.Source_Assignment_id = pSource_Assignment_id
     and c.Source_id = pSource_id;

   vMsg       varchar2(2000);
   vErrCode   number(6) := 0;

 Begin

   If cSource_Rowid is not null and gInvestigate_Errors = 'Y' then
     -- Only fires in Transaction is in error
     For Rec1 in c1(pSource_Rowid => cSource_Rowid, pPreview_id => cPreview_id) loop

        XXCP_GLOBAL.GCOMMON(1).CURRENT_TRANSACTION_TABLE := REC1.VT_TRANSACTION_TABLE;
        XXCP_GLOBAL.GCOMMON(1).CURRENT_TRANSACTION_ID    := REC1.VT_TRANSACTION_ID;
        XXCP_GLOBAL.GCOMMON(1).CURRENT_PARENT_TRX_ID     := REC1.VT_PARENT_TRX_ID;
        XXCP_GLOBAL.GCOMMON(1).CURRENT_INTERFACE_ID      := REC1.VT_INTERFACE_ID;
        XXCP_GLOBAL.GCOMMON(1).CURRENT_INTERFACE_REF     := REC1.VT_TRANSACTION_REF;

        vErrCode := 201;
        vMsg     := 'Transaction Table <'||REC1.VT_TRANSACTION_TABLE||'>';

        For Rec2 in c2(pSource_Assignment_id => Rec1.vt_source_assignment_id, 
                       pTransaction_Table    => rec1.vt_transaction_table) 
        Loop
          vErrCode := 202;
          vMsg     := 'Transaction Type <'||Rec1.vt_transaction_type||'>';
          For Rec3 in c3(pSource_Table_id => Rec2.Source_table_id, pType => Rec1.vt_transaction_type) Loop
            vErrCode := 203;
            vMsg     := 'Transaction Class <'||Rec1.vt_transaction_class||'>';
            For Rec4 in c4(pSource_Table_id => Rec2.Source_table_id, pClass => Rec1.vt_transaction_class) Loop
              vErrCode := 0;
            End Loop;
            vErrCode := 209;
            vMsg     := 'Source Assignment <'||to_char(Rec1.vt_source_assignment_id)||'>';
            For Rec5 in c5(pSource_Id => rec1.Source_id, pSource_Assignment_id => Rec1.vt_source_assignment_id) Loop
              vErrCode := 0;
            End Loop;
          End Loop;
        End Loop;

        If vErrCode = 0 then

          If nvl(REC1.VT_PARENT_TRX_ID,0) = 0 then
           vErrCode := 204;
           vMsg := 'Invalid Parent Trx Id';
          ElsIf nvl(Rec1.VT_TRANSACTION_ID,0) = 0 then
           vErrCode := 205;
           vMsg := 'Invalid Transaction Id';
          ElsIf Rec1.VT_TRANSACTION_DATE is null then
           vErrCode := 206;
           vMsg := 'Invalid Transaction Date';
          ElsIf Rec1.VT_TRANSACTION_REF is null then
           vErrCode := 207;
           vMsg := 'Invalid Transaction Ref';
          ElsIf Rec1.VT_SOURCE_ASSIGNMENT_ID is null then
           vErrCode := 208;
           vMsg := 'Invalid Source Assignment_id';
          End If;
        End If;

        If vErrCode != 0 then
          xxcp_foundation.FndWriteError(vErrCode,vMsg);
        End If;
     End Loop;
   End If;
 End Investigate;
--  !! ***********************************************************************************************************
--  !!                                    CONTROL
--  !!                  External Procedure (Entry Point) to start Preview Mode
--  !! ***********************************************************************************************************
  Function Control( cSource_Assignment_id    IN Number,
                    cSource_Rowid            IN Rowid,
                    cParent_trx_id           IN Number,
                    cPreview_id              IN Number,
                    cTransaction_Table       IN VARCHAR2,
                    cTransaction_Type        IN VARCHAR2,
                    cTransaction_Id          IN Number,
                    cRemoval_Process         IN Number,
                    cUser_id                 IN NUMBER,
                    cLogin_id                IN NUMBER,
                    cPV_Mode                 IN VARCHAR2,
                    cPV_Zero_Flag            IN VARCHAR2 Default 'N',
                    cJobStatus              OUT Number
                    ) return Number is

  Cursor SRC (pSource_Assignment_id in number) is
  select s.source_id, g.source_group_id, s.preview_ctl
        ,Nvl(a.Stamp_Parent_Trx,'N') Stamp_parent_trx
        ,s.Source_base_table, e.preview_interface_view
    from xxcp_sys_sources              s,
         xxcp_sys_engine_ctl           e,
         xxcp_source_Assignment_groups g,
         xxcp_source_Assignments       a
   where s.source_id            = g.source_id
     and g.source_group_id      = a.source_group_id
     and s.preview_ctl          = e.preview_ctl
     and a.source_assignment_id = pSource_Assignment_id;

  Cursor UR(pSource_Rowid in rowid) is
    select r.vt_request_id Unique_Request_id
      from xxcp_rt_interface r
     where r.rowid = pSource_Rowid;

/*  Cursor UR2(pSource_Rowid in rowid) is
    select r.vt_request_id Unique_Request_id
      from xxcp_tp_interface r
     where r.rowid = pSource_Rowid;*/

  vUnique_Request_id      Number;
  vSource_id              pls_integer := 0;
  vSource_Group_id        pls_integer := 0;
  vSuccess                Boolean     := False;
  vJobStatus              pls_integer := 0;
  vPreview_ctl            pls_integer := 0;
  vStamp_parent_trx       Varchar2(1);
  vSource_base_table      Varchar2(30);
  vPreview_interface_view Varchar2(30) := Null;
  vInternalErrorCode      pls_integer  := -1;

  BEGIN

     xxcp_global.gCommon(1).preview_id            := cPreview_id;
     xxcp_global.gCommon(1).current_Assignment_id := cSource_Assignment_id;
     xxcp_global.Preview_on            := 'Y';
     xxcp_wks.Last_Transaction_Table   := '~#~';
     xxcp_global.SystemDate := Sysdate;
     
      xxcp_global.Set_Journal_Preview('N');
      
     -- 02.04.08
     -- reset the parent_trx_id so that every transaction in preview is classed a new transaction.
     xxcp_te_configurator.reset_parent_trx_id;
     --
     --## ***********************************************************************************************************
     --##                            Find the Source Id for the Source Assignment Augument.
     --## ***********************************************************************************************************
     --
     For SRCRec in SRC(pSource_Assignment_id => cSource_Assignment_ID) Loop
        vSource_id              := SRCRec.Source_id;
        vSource_Group_id        := SRCRec.Source_Group_id;
        vPreview_ctl            := nvl(SRCRec.preview_ctl,0);
        vStamp_parent_trx       := SRCRec.Stamp_parent_trx;
        vSource_base_table      := upper(SRCRec.Source_base_table);
        vPreview_interface_view := SRcRec.Preview_Interface_View;

        xxcp_global.SET_SOURCE_ID(cSource_id => vSource_id);
     End Loop;

     If vPreview_ctl <> 0 then
        -- **********************************************************************************
        --                      Remove Preview Transactions from a previous Run
        -- **********************************************************************************
        Clear_Old_Preview_Rows(cRemoval_Process => cRemoval_Process);

        If vPreview_ctl = 5 then
         For Rec in UR(cSource_Rowid) Loop
           vUnique_Request_id := Rec.Unique_Request_id;
         End Loop;
/*        ElsIf vPreview_ctl = 19 then
         For Rec in UR2(cSource_Rowid) Loop
           vUnique_Request_id := Rec.Unique_Request_id;
         End Loop;*/
        Else
          vUnique_Request_id := 0;
        End If;
         -- **********************************************************************************
         --                         Populate the XXCP_PV_INTERFACE
         -- **********************************************************************************
        If vPreview_ctl = 13 then
           vSuccess :=  Copy_Input_Row_31(cSource_id            => vSource_id, 
                                          cSource_Rowid         => cSource_Rowid,
                                          cSource_Table         => vSource_base_table,
                                          cPreview_id           => cPreview_id,
                                          cSource_Assignment_id => cSource_Assignment_id, 
                                          cTransaction_Table    => cTransaction_Table, 
                                          cTransaction_id       => cTransaction_id, 
                                          cParent_Trx_id        => cParent_Trx_id,
                                          cPV_Mode              => cPV_Mode, 
                                          cPreview_Ctl          => vPreview_ctl);
        Else
          vSuccess :=  Copy_Input_Row( cSource_id            => vSource_id,
                                       cSource_Rowid         => cSource_Rowid,
                                       cSource_Table         => vSource_base_table,
                                       cPreview_id           => cPreview_id,
                                       cSource_Assignment_id => cSource_Assignment_id, 
                                       cTransaction_Table    => cTransaction_Table, 
                                       cTransaction_id       => cTransaction_id, 
                                       cParent_Trx_id        => cParent_Trx_id,
                                       cUnique_Request_id    => vUnique_Request_id,
                                       cPV_Mode              => cPV_Mode, 
                                       cPreview_Ctl          => vPreview_ctl);
        End If;
        
        If vSuccess then
            -- Set Validation Variables
            xxcp_global.gCOMMON(1).validation := 'N';
            If vPreview_ctl = 1 then -- Journals
            -- **********************************************************************************
            --                                 Run GL Preview Package
            -- **********************************************************************************         
             vJobStatus := XXCP_PV_GL_ENGINE.Control(cSource_Group_id      => vSource_Group_id,
                                                     cSource_Assignment_id => cSource_Assignment_id,
                                                      cPreview_id           => xxcp_global.gCommon(1).preview_id,
                                                      cParent_Trx_id        => cParent_trx_id,
                                                      cUser_id              => cUser_id,
                                                      cLogin_id             => cLogin_id,
                                                     cPV_Zero_Flag         => cPV_Zero_Flag,
                                                     cSource_Rowid         => cSource_Rowid);
            -- **********************************************************************************
            --                                 Run AR Preview Package
            -- **********************************************************************************
            ElsIf vPreview_ctl = 2 then
                vJobStatus := XXCP_PV_AR_ENGINE.Control(
                                                    cSource_Group_id      => vSource_Group_id,
                                                    cSource_Assignment_id => cSource_Assignment_id,
                                                    cPreview_id           => xxcp_global.gCommon(1).preview_id,
                                                    cParent_Trx_id        => cParent_trx_id,
                                                    cUser_id              => cUser_id,
                                                    cLogin_id             => cLogin_id,
                                                    cPV_Zero_Flag         => cPV_Zero_Flag,
                                                    cSource_Rowid         => cSource_Rowid);
            ElsIf vPreview_ctl = 3 then -- Validations
            -- **********************************************************************************
            --                                 Run Validation Preview Package
            -- **********************************************************************************
              -- Set Validation Variables
              xxcp_global.gCOMMON(1).validation := 'Y';
              vJobStatus := XXCP_PV_VALIDATIONS_ENGINE.Control(cSource_Group_ID      => vSource_Group_id,
                                                               cSource_Assignment_id => cSource_Assignment_id,
                                                               cPreview_Id           => xxcp_global.gCommon(1).preview_id,
                                                               cParent_trx_id        => cParent_trx_id,
                                                               cUser_id              => cUser_id,
                                                               cLogin_id             => cLogin_id);
              
            ElsIf vPreview_ctl = 5 then -- Real Time Engine
            -- **********************************************************************************
            --                                 Run RT Preview Package
              -- **********************************************************************************
            vJobStatus := XXCP_PV_RT_ENGINE.Control(cSource_Group_id      => vSource_Group_id,
                                                    cSource_Assignment_id => cSource_Assignment_id,
                                                    cUnique_Request_id    => vUnique_Request_id,
                                                    cPreview_id           => xxcp_global.gCommon(1).preview_id,
                                                    cParent_trx_id        => cParent_Trx_id,
                                                    cUser_id              => cUser_id,
                                                    cLogin_id             => cLogin_id,
                                                    cPV_Zero_Flag         => cPV_Zero_Flag,
                                                    cSource_Rowid         => cSource_Rowid);
/*            ElsIf vPreview_ctl = 19 then -- TP Real Time Engine
            -- **********************************************************************************
            --                                 Run RT Preview Package
              -- **********************************************************************************
            vJobStatus := XXCP_PV_TP_ENGINE.Control(cSource_Group_id      => vSource_Group_id,
                                                    cSource_Assignment_id => cSource_Assignment_id,
                                                    cUnique_Request_id    => vUnique_Request_id,
                                                    cPreview_id           => xxcp_global.gCommon(1).preview_id,
                                                    cParent_trx_id        => cParent_Trx_id,
                                                    cUser_id              => cUser_id,
                                                    cLogin_id             => cLogin_id,
                                                    cPV_Zero_Flag         => cPV_Zero_Flag,
                                                    cSource_Rowid         => cSource_Rowid);*/
            ElsIf vPreview_ctl = 13 then -- CPA
           -- **********************************************************************************
           --                                 Run CPA Preview Package
           -- **********************************************************************************
             vJobStatus := XXCP_PV_CPA_ENGINE.Control(cSource_Group_id      => vSource_Group_id,
                                                      cSource_Assignment_id => cSource_Assignment_id,
                                                      cPreview_Id           => xxcp_global.gCommon(1).preview_id,
                                                      cParent_trx_id        => cParent_Trx_id,
                                                      cUser_id              => cUser_id,
                                                      cLogin_id             => cLogin_id,
                                                      cPV_Zero_Flag         => cPV_Zero_Flag);
           ElsIf vPreview_ctl between 4 and 999 and vPreview_Interface_View is not null then
           -- **********************************************************************************
           --                          Run Generalized Preview Package
           -- **********************************************************************************
             vJobStatus :=  XXCP_PREVIEW_ENGINE.Control(
                                         cSource_Group_id      => vSource_Group_id,
                                         cSource_Assignment_id => cSource_Assignment_id,
                                         cPreview_Id           => xxcp_global.gCommon(1).preview_id,
                                         cParent_trx_id        => cParent_Trx_id,
                                         cUser_id              => cUser_id,
                                         cLogin_id             => cLogin_id,
                                         cPV_Zero_Flag         => cPV_Zero_Flag);            
           End If; -- End Sucess Check
        Else
          xxcp_foundation.fndwriteerror(100,'Preview Control Id Unknown');
        End If;

        Investigate(cSource_Rowid => cSource_Rowid, cPreview_id => cPreview_id);
        -- Check to See
        vInternalErrorCode := 0;

     End If;

     Commit;

     cJobStatus := nvl(vJobStatus,0);
     Return(nvl(vInternalErrorCode,0));
  END CONTROL;
  
    -- Make_PV_GL_Interface
  Function Make_PV_GL_Interface(  cPreview_id            in number,
                                  cJournal_Header_id     in number,
                                  cSource_Assignment_id out number) return number is


    vInternalErrorCode    number(8) := 130;

    cursor c1(pJournal_Header_id in number) is
     select y.source_assignment_id, y.responsibility_id, y.source_table_id, t.transaction_table
     from xxcp_journal_entry_headers y,
          xxcp_sys_source_tables t
      where y.je_header_id = pJournal_header_id
        and y.source_table_id = t.source_table_id;
            
    vTransaction_Table    varchar2(30);
    vSource_Assignment_id xxcp_source_assignments.source_assignment_id%type;        
            
  Begin
    
     For Rec in c1(cJournal_header_id) loop
        vSource_Assignment_id := rec.source_assignment_id;
        vTransaction_Table    := rec.transaction_table;
     End Loop;
  
     cSource_Assignment_id := vSource_Assignment_id ;
     
     Begin
       delete from xxcp_pv_gl_interface w
         where w.vt_preview_id = cPreview_id;
        
       Commit;
     
       Exception when others then  Null;
     End;
         
     Begin     
  
     Insert into xxcp_pv_gl_interface(
       vt_preview_id,
       vt_status,
       vt_source_assignment_id,
       vt_transaction_table,
       vt_transaction_type,
       vt_transaction_class,
       vt_transaction_ref,
       vt_transaction_id,
       vt_parent_trx_id,
       vt_transaction_date,
       status,
       actual_flag,
       ledger_id,
       set_of_books_id,
       entered_dr,
       entered_cr,
       accounted_dr,
       accounted_cr,
       currency_code,
       Reference1,
       Reference2,
       Reference3,
       Reference4,
       Reference5,
       Reference6,
       Reference7,
       Reference8,
       Reference9,
       Reference10,
       Reference11,
       Segment1,
       Segment2,
       Segment3,
       Segment4,
       Segment5,
       Segment6,
       Segment7,
       Segment8,
       Segment9,
       Segment10,
       Segment11,
       Segment12,
       Segment13,
       Segment14,
       Segment15,
       Accounting_Date,
       date_created,
       created_by,
       user_je_source_name,
       user_je_category_name,
       Attribute1,
       Attribute2,
       Attribute3,
       Attribute4,
       Attribute5,
       Attribute6,
       Attribute7,
       Attribute8,
       Attribute9,
       Attribute10,
       Attribute11,
       Attribute12,
       Attribute13,
       Attribute14,
       Attribute15,
       User_currency_conversion_type,
       Currency_conversion_date       
       )
  select
        cPreview_id 
        ,'NEW'
        ,vSource_Assignment_id
        ,vTransaction_Table
        ,h.transaction_type
        ,l.accounting_class
        ,to_char(h.je_header_id)
        ,l.je_line_id
        ,h.je_header_id
        ,h.accounting_date
        ,'NEW'
        ,'A'
        ,l.set_of_books_id -- ledger
        ,-1
        ,l.entered_dr
        ,l.entered_cr
        ,l.accounted_dr
        ,l.accounted_cr
        ,h.currency_code
        ,h.name --reference1
        ,h.description --reference2
        ,h.clearing_company --reference3
        ,h.name --reference4
        ,h.description --reference5
        ,to_char(l.quantity) --reference6
        ,null --reference7
        ,null --reference8
        ,null --reference9
        ,l.description --reference10
        ,null --reference11
        ,l.Segment1
        ,l.Segment2
        ,l.Segment3
        ,l.Segment4
        ,l.Segment5
        ,l.Segment6
        ,l.Segment7
        ,l.Segment8
        ,l.Segment9
        ,l.Segment10
        ,l.Segment11
        ,l.Segment12
        ,l.Segment13
        ,l.Segment14
        ,l.Segment15
        ,h.Accounting_Date
        ,sysdate
        ,cPreview_id
        ,h.je_source
        ,h.je_category
        ,l.Attribute1
        ,l.Attribute2
        ,l.Attribute3
        ,l.Attribute4
        ,l.Attribute5
        ,l.Attribute6
        ,l.Attribute7
        ,l.Attribute8
        ,l.Attribute9
        ,l.Attribute10
        ,l.Attribute11
        ,l.Attribute12
        ,l.Attribute13
        ,l.Attribute14
        ,l.Attribute15
        ,h.exchange_type
        ,h.exchange_date        
     from xxcp_journal_entry_headers h,
          xxcp_journal_entry_lines l
    where h.je_header_id = l.je_header_id
      and h.je_header_id = cJournal_Header_id;
      
    vInternalErrorCode := 0;

    Commit;
    
    Exception when others then 
          vInternalErrorCode := 14551;
          xxcp_foundation.FndWriteError(vInternalErrorCode,'Could not create preview record',SQLERRM);
  
    End;
    
    Return(vInternalErrorCode);
  
  End Make_PV_GL_Interface;
  
  
 /*
  !! ***********************************************************************************************************
  !!                                   Journal_Control
  !!                  External Procedure (Entry Point) to start the Preview Journal Engine.
  !! ***********************************************************************************************************
 */
  Function Journal_Control( 
                    cPreview_id              IN Number,
                    cJournal_Header_id       IN Number
                    ) return Number is

  Cursor SRC (pSource_Assignment_id in number) is
  select s.source_id, g.source_group_id, s.preview_ctl
        ,Nvl(a.Stamp_Parent_Trx,'N') Stamp_parent_trx
        ,Source_base_table
    from xxcp_sys_sources              s,
         xxcp_source_Assignment_groups g,
         xxcp_source_Assignments       a
   where s.source_id            = g.source_id
     and g.source_group_id      = a.source_group_id
     and a.source_assignment_id = pSource_Assignment_id;

  Cursor UR(pSource_Rowid in rowid) is
  select r.VT_REQUEST_ID Unique_Request_id
  from xxcp_rt_interface r
  where rowid = pSource_Rowid;


  vUnique_Request_id Number;

  i                  Integer ;
  vSource_id         Integer := 0;
  vSource_Group_id   Integer := 0;
  vSuccess           Boolean := False;
  vJobStatus         Integer := 0;
  vPreview_ctl       Integer := 0;
  vStamp_parent_trx  Varchar2(1);
  vSource_base_table Varchar2(30);

  vInternalErrorCode    Integer := -1;
  vSource_Assignment_id number := 0;
  
  vSource_Rowid rowid;

   Cursor f1(pSource_Rowid in rowid) is
    select b.vt_transaction_table, b.vt_transaction_type, b.vt_parent_trx_id, b.vt_transaction_id
    from xxcp_pv_gl_interface b
    where b.rowid = pSource_Rowid;

  BEGIN


     xxcp_global.Set_Journal_Preview('Y');

     vInternalErrorCode := Make_PV_GL_Interface(cPreview_id, cJournal_header_id, vSource_Assignment_id);
     
     xxcp_global.gCommon(1).preview_id            := cPreview_id;
     xxcp_global.gCommon(1).current_Assignment_id := vSource_Assignment_id;
     xxcp_global.Preview_on            := 'Y';
     xxcp_wks.Last_Transaction_Table   := '~#~';
     
     
     If vInternalErrorCode = 0 then
        select max(rowid) into vSource_Rowid 
          from xxcp_pv_gl_interface r
          where r.vt_parent_trx_id = cJournal_Header_id
            and r.vt_preview_id    = cPreview_id;
     End If;
     
     -- 02.04.08
     -- reset the parent_trx_id so that every transaction in preview is classed a new transaction.
     xxcp_te_configurator.reset_parent_trx_id;

     --
     --## ***********************************************************************************************************
     --##                            Find the Source Id for the Source Assignment Augument.
     --## ***********************************************************************************************************
     --
     For SRCRec in SRC(vSource_Assignment_ID)
      Loop
        vSource_id        := SRCRec.Source_id;
        vSource_Group_id  := SRCRec.Source_Group_id;
        vPreview_ctl      := nvl(SRCRec.preview_ctl,0);
        vStamp_parent_trx := SRCRec.Stamp_parent_trx;
        vSource_base_table := SRCRec.Source_base_table;
      End Loop;

     xxcp_global.SET_SOURCE_ID(cSource_id => vSource_id);

     If vPreview_ctl <> 0 and vInternalErrorCode = 0 then

        --
         -- **********************************************************************************
         --                      Remove Preview Transactions from a previous Run
         -- **********************************************************************************
        --
        Clear_Old_Preview_Rows(cRemoval_Process => 3);

        If vPreview_ctl = 1 then

         /*
         -- **********************************************************************************
         --                            Populate the XXCP_PV_INTERFACE
         -- **********************************************************************************
         */
         For recF1 in F1(vSource_Rowid) Loop
          vSuccess :=  Copy_Input_Row( vSource_id , vSource_Rowid, 'XXCP_PV_GL_INTERFACE',cPreview_id, vSource_Assignment_id, recF1.vt_Transaction_Table, recF1.vt_Transaction_id, recF1.vt_Parent_Trx_id, 0, 'N', vPreview_ctl);
         End Loop;
         /*
         -- **********************************************************************************
         --                                 Run GL Preview Package
         -- **********************************************************************************
         */
          If vSuccess then
             -- Set Validation Variables
             xxcp_global.gCOMMON(1).validation := 'N';

             vJobStatus := XXCP_PV_MJE_ENGINE.Control(
                              cSource_Group_id      => vSource_Group_id, 
                              cSource_Assignment_id => vSource_Assignment_id, 
                              cPreview_id           => xxcp_global.gCommon(1).preview_id, 
                              cParent_trx_id        => cJournal_Header_id, 
                              cUser_id              => cPreview_id, 
                              cLogin_id             => cPreview_id, 
                              cPV_Zero_Flag         => 'T', 
                              cSource_Rowid         => vSource_Rowid);
									                               

          End If;
        End If;

        commit;
        -- Check to See
        vInternalErrorCode := 0;

     End If;

     Commit;

     Return(nvl(vInternalErrorCode,0));
  END Journal_Control;
  
  --
  -- Bug Test
  --  
  Function Bug_Test(cUser_Name            in varchar2,
                    cSource_assignment_id in number,
                    cInterface_id         in number) return number is
  -- Local variables here
  i integer;
  
   type  gli_type is REF CURSOR;

    -- Record Type of required gl_interface fields
    type gli_map_rec is record(
              SOURCE_ROWID                    Rowid,
              VT_INTERFACE_ID                 xxcp_gl_eng_v.vt_interface_id%type,
              VT_PARENT_TRX_ID                xxcp_gl_eng_v.vt_parent_trx_id%type,
              VT_SOURCE_ASSIGNMENT_ID         xxcp_gl_eng_v.vt_source_assignment_id%type,
              VT_TRANSACTION_ID               xxcp_gl_eng_v.vt_transaction_id%type,
              VT_TRANSACTION_REF              xxcp_gl_eng_v.vt_transaction_ref%type,
              VT_TRANSACTION_TABLE            xxcp_gl_eng_v.vt_transaction_table%type,
              VT_TRANSACTION_TYPE             xxcp_gl_eng_v.vt_transaction_type%type
         );
   
 cfg     gli_type;
 cfgRec  gli_map_rec;

 vDDL_Interface_Select varchar2(32000);  

 Cursor sy(pSource_assginment_id in number) is
  select c.source_name, c.source_id,
         a.source_assignment_name, c.source_base_table
  from xxcp_sys_sources c,
       xxcp_source_assignments a
  where c.source_id = a.source_id
   and a.source_assignment_id = pSource_assginment_id; 
    
  Cursor USR(pUser_Name in varchar2) is
  select pr.pv_trx_removal,
         pr.user_id,
         pr.pv_zero_flag,
         pr.user_name
  from xxcp_user_profiles pr
  where pr.user_name = pUser_Name;
  
  vJobstatus number := 0;
  
  vInternalErrorCode number := 0;
  
begin
  -- Test statements here
  
  For rec2 in USR(cUser_Name) loop
    
    xxcp_foundation.show('Found User:'||rec2.user_name);
    
    For Rec3 in sy(cSource_assignment_id) Loop

     vDDL_Interface_Select := 
       'select 
          int.rowid SOURCE_ROWID,
          int.VT_INTERFACE_ID,
          int.VT_PARENT_TRX_ID,
          int.VT_SOURCE_ASSIGNMENT_ID,
          int.VT_TRANSACTION_ID,
          int.VT_TRANSACTION_REF,
          int.VT_TRANSACTION_TABLE,
          int.VT_TRANSACTION_TYPE 
       From '||rec3.source_base_table||' int '||chr(10)||
       ' where int.vt_interface_id = :1';

    End Loop;
    
    Open CFG for vDDL_Interface_Select Using cInterface_id;
    Loop

      Fetch CFG into CFGRec;
      Exit when CFG%notfound;

      xxcp_foundation.show('Making Call..');
   
      Begin
     
       vInternalErrorCode := 
           XXCP_PREVIEW_CTL.Control
                              (cSource_Assignment_id    => CFGRec.vt_Source_Assignment_id,
                               cSource_Rowid            => CFGRec.Source_rowid,
                               cParent_trx_id           => CFGRec.vt_parent_trx_id,
                               cPreview_id              => rec2.user_id ,
                               cTransaction_Table       => CFGRec.vt_transaction_Table,
                               cTransaction_Type        => CFGRec.vt_Transaction_Type,
                               cTransaction_Id          => CFGRec.vt_Transaction_id,
                               cRemoval_Process         => rec2.PV_TRX_REMOVAL,
                               cUser_id                 => rec2.user_id,
                               cLogin_id                => -1,
                               cPV_Mode                 => 'ACTUAL',
                               cPV_Zero_Flag            => rec2.pv_zero_flag,
                               cJobStatus               => vJobStatus);

--        Exception when others then 
  --        DBMS_OUTPUT.PUT_LINE( DBMS_UTILITY.FORMAT_ERROR_STACK );
    --         xxcp_foundation.show(SQLERRM); 
    
    -- DBMS_UTILITY.FORMAT_ERROR_BACKTRACE  (10g and above)

      End;

    End loop;
    CLOSE CFG;
    
  end loop;
  
  xxcp_foundation.show('Internal Error Code <'||vInternalErrorCode||'> Job Status <'||vJobStatus||'>');
  
 End Bug_Test;
  
    -- **********************************
  -- Init
  -- **********************************
  Procedure Init is

     Cursor pf(pProfile_Name in varchar2) is
      select Profile_Value
        from XXCP_SYS_PROFILE
       where profile_category = 'PV'
         and profile_name = pProfile_Name;
  
   Begin
   
     For Rec in pf('INVESTIGATE UNPROCESSED ROWS') Loop
       gInvestigate_Errors  := Rec.Profile_Value;   
     End Loop;
   
   End Init;

Begin
  Init;
End XXCP_PREVIEW_CTL;
/
