CREATE OR REPLACE PACKAGE XXCP_PV_CPA_ENGINE AS
/******************************************************************************
                    V I R T A L  T R A D E R  L I M I T E D
		    (Copyright 2008-2009 Virtual Trader Ltd. All rights Reserved)

   NAME:       XXCP_PV_CPA_ENGINE
   PURPOSE:    CPA Engine in Preview Mode
	             (XXCP_cost_plus_interface)

   Version [03.06.05] Build Date [19-DEC-2012] Name [XXCP_PV_CPA_ENGINE]

******************************************************************************/


  Function Software_Version RETURN VARCHAR2;

  Procedure Transaction_Group_Stamp(cStamp_Parent_Trx in varchar2,cSource_assignment_id in number, cInternalErrorCode out number);

  Function Control(cSource_Group_id         IN NUMBER,
      	           cSource_Assignment_id    IN NUMBER,
                   cPreview_Id              IN NUMBER,
                   cParent_trx_id           IN NUMBER   DEFAULT 0,
                   cUser_id                 IN NUMBER,
                   cLogin_id                IN NUMBER,
		               cPV_Zero_Flag            IN VARCHAR2) RETURN NUMBER;


END XXCP_PV_CPA_ENGINE;
/
