CREATE OR REPLACE PACKAGE BODY XXCP_GATEWAY_UTILS AS
/******************************************************************************
                        V I R T A L  T R A D E R  L I M I T E D
            (Copyright 2004-2012 Virtual Trader Ltd. All rights Reserved)

   NAME:       XXCP_GATEWAY_UTILS
   PURPOSE:    This package gives clients access to Virtual Trader functionality
               protected from Virtual Trader development.

   REVISIONS:
   Ver        Date      Author  Description
   ---------  --------  ------- ------------------------------------
   02.00.00   28/04/05  Keith   First Coding
   02.00.01   03/10/05  Keith   Added substr to Set_custom_data
   02.00.02   24/10/05  Keith   Safe_Divide
   02.03.00   16/01/06  Keith   Added Get_Grouping_Rule_by_Name
   02.03.01   23/03/06  Keith   Added Entered_Value_Calculation
   02.03.02   05/04/06  Keith   Added Last_Step_Value
   02.03.03   13/04/06  Keith   Updated Last_step_value
   02.03.04   15/08/06  Keith   Changed frozen exit logic in the last_step_value
   02.03.05   14/09/06  Keith   Added Highest Ship Actual
   02.03.06   17/11/06  Keith   Changed 'Yes'
   02.04.01   04/01/07  Keith   Added CurrConv function and Debug_Write_Trace
   02.04.02   29/03/07  Keith   Added CurrPrecission and CurrRounding
   02.04.03   06/04/07  Keith   Added CurrConvRate function
   02.04.04   25/02/08  Keith   Renamed Live_Assignment_Value
   02.04.05   30/04/08  Keith   added nvl to  CustLookupFetchGroup
   02.04.06   15/07/08  Simon   Added CustomData_BestMatch and Engine_Value
   02.04.07   24/11/08  Simon   Added Add_Weighting, CustomData_Weighting, CustomData_Weighting2
   02.04.08   10/03/09  Nigel   Added vt_results_attributes11..30 to set_realtime_attribute
   02.04.09   12/03/09  Keith   Added Diag
   02.04.10   14/07/09  Keith   Added company_number
   02.04.11   21/10/09  Keith   Added Get_Internal_Conversion_Type
   02.06.01   21/12/09  Simon   Added vt_results_attributes31..50 to set_realtime_attribute
   02.06.02   03/02/10  Keith   Improved Set_Engine_Value with ID
   02.06.03   12/08/10  Keith   Added Application to Source Assignment Lookup.
   02.06.04   18/08/10  Keith   Added override verion of Source Assignment lookup
   02.06.05   28/02/11  Keith   Added Explosion functions
   02.06.06   20/07/11  Keith   Added Cache by Name ABS and Cache by Id ABS
   03.06.07   12/01/12  Keith   Added IsNumber function
   03.06.08   29/01/12  Keith   Added Preview_Mode function
   03.06.09   16/06/12  Keith   Added Trading Set and get_before_insert_value
   03.06.10   05/10/12  Simon   Increased Dyn Attr length from 100 to 250 (OOD-195).
   03.06.11   11/10/12  Mat     OOD-349 - Added Set_Long_Reference;
                                Added Set_RT_Interface;
                                CustomData_BestMatch2 - Added missing vFound := TRUE;   
   03.06.12   19/11/12  Keith   Added Element Tax 
******************************************************************************/

 gLSV_Source_id xxcp_sys_sources.source_id%type;

  -- *************************************************************
  --                     Software Version Control
  --                   -----------------------------
  -- This package can be checked with SQL to ensure it's installed
  -- select packagename.software_version from dual;
  -- *************************************************************
  Function Software_Version return varchar2 is
   Begin
     Return('Version [03.06.12] Build Date [19-NOV-2012] NAME [XXCP_GATEWAY_UTILS]');
   End Software_Version;

  -- **************************************************************************
  --     Show
  -- **************************************************************************
  Procedure Show(cMessage in varchar2) is
   Begin
      xxcp_foundation.Show(cMessage => cMessage);
   End Show;

  -- **************************************************************************
  --     Get_Source_Assignment_Name
  -- **************************************************************************
  Function Get_Source_Assignment_Name(cSource_Assignment_id in number) return varchar2 is
  Begin
     return(xxcp_translations.Source_Assignments(cSource_Assignment_id => cSource_Assignment_id));
  End Get_Source_Assignment_Name;

  -- **************************************************************************
  --     Get_Source_Group_by_Name - By Name
  -- **************************************************************************
  Function Get_Source_Group_by_Name(cSource_Group_Name in varchar2
                                  , cSource_id         in number default Null
                                  , cInstance_id       in number default 0 ) Return Number is


  Cursor c1(pSource_Group_Name in varchar2, pInstance_id in number, pSource_id in number) is
  select a.SOURCE_GROUP_ID
   from XXCP_source_assignment_groups a
  where a.source_group_name = pSource_Group_Name
    and instance_id         = pInstance_id
    and source_id           = nvl(pSource_id,source_id)
    and nvl(a.active, 'Y')  = 'Y';

   vResult XXCP_source_assignment_groups.source_group_id%type := 0;

   Begin
    For Rec in c1(cSource_Group_Name, cInstance_id, cSource_id)  loop
      vResult := Rec.Source_Group_id;
    End Loop;
    Return(vResult);
  End Get_Source_Group_by_Name;


  -- **************************************************************************
  --     Get_Source_Assignment_by_Name - By Name (override)
  -- **************************************************************************
  Function Get_Source_Assignment_by_Name(cSource_Assignment_Name in varchar2
                                       , cSource_id              in number default Null
                                       , cInstance_id            in number default 0
                                       , cApplication_Name       in varchar2) Return Number is


  Cursor c1(pSource_Assignment_Name in varchar2, pInstance_id in number, pSource_id in number, pApplication_Name in varchar2) is
  select Source_Assignment_id
   from XXCP_source_assignments a
  where a.source_assignment_name = pSource_Assignment_Name
    and instance_id = pInstance_id
    and source_id = nvl(pSource_id,source_id)
    and nvl(a.active, 'Y') = 'Y'
    and a.application_usage_id = any(select x.numeric_code  from XXCP_lookups x 
                                      where x.lookup_type = 'SOURCE ASSIGNMENT USAGE'
                                        and x.lookup_code = pApplication_Name);

   vResult XXCP_source_assignments.source_assignment_id%type := 0;

   Begin
    For Rec in c1(cSource_Assignment_Name, cInstance_id, cSource_id, cApplication_Name)  loop
      vResult := Rec.Source_Assignment_id;
    End Loop;
    Return(vResult);
  End Get_Source_Assignment_by_Name;

  -- **************************************************************************
  --     Get_Source_Assignment_by_Name - By Name
  -- **************************************************************************
  Function Get_Source_Assignment_by_Name(cSource_Assignment_Name in varchar2
                                       , cSource_id              in number default Null
                                       , cInstance_id            in number default 0) Return Number is

  Cursor c1(pSource_Assignment_Name in varchar2, pInstance_id in number, pSource_id in number) is
  select Source_Assignment_id
   from XXCP_source_assignments a
  where a.source_assignment_name = pSource_Assignment_Name
    and instance_id = pInstance_id
    and source_id = nvl(pSource_id,source_id)
    and nvl(a.active, 'Y') = 'Y';

   vResult XXCP_source_assignments.source_assignment_id%type := 0;

   Begin
    For Rec in c1(cSource_Assignment_Name, cInstance_id, cSource_id)  loop
      vResult := Rec.Source_Assignment_id;
    End Loop;
    Return(vResult);
  End Get_Source_Assignment_by_Name;

  -- **************************************************************************
  --     Get_Source_Assignment_id - By Set of Books (override)
  -- **************************************************************************
  Function Get_Source_Assignment_by_Book(cSet_of_books_id        in number
                                       , cSource_id              in number default Null
                                       , cInstance_id            in number default 0
                                       , cApplication_Name       in varchar2) Return Number is

  Cursor c1(pSet_of_books_id in number, pInstance_id in number, pSource_id in number, pApplication_Name in varchar2) is
  select Source_Assignment_id
   from XXCP_source_assignments a
  where a.SET_OF_BOOKS_ID = pSet_of_books_id
    and instance_id = pInstance_id
    and source_id = nvl(pSource_id,source_id)
    and nvl(a.active, 'Y') = 'Y'
    and a.application_usage_id = any(select x.numeric_code  from XXCP_lookups x 
                                      where x.lookup_type = 'SOURCE ASSIGNMENT USAGE'
                                       and x.lookup_code = pApplication_Name);


   vResult XXCP_source_assignments.source_assignment_id%type := 0;

   Begin
    For Rec in c1(cSet_of_books_id, cInstance_id, cSource_id, cApplication_Name)  loop
      vResult := Rec.Source_Assignment_id;
    End Loop;
    Return(vResult);
  End Get_Source_Assignment_by_Book;

  -- **************************************************************************
  --     Get_Source_Assignment_id - By Set of Books
  -- **************************************************************************
  Function Get_Source_Assignment_by_Book(cSet_of_books_id        in number
                                       , cSource_id              in number default Null
                                       , cInstance_id            in number default 0) Return Number is

  Cursor c1(pSet_of_books_id in number, pInstance_id in number, pSource_id in number) is
  select Source_Assignment_id
   from XXCP_source_assignments a
  where a.SET_OF_BOOKS_ID = pSet_of_books_id
    and instance_id = pInstance_id
    and source_id = nvl(pSource_id,source_id)
    and nvl(a.active, 'Y') = 'Y';

   vResult XXCP_source_assignments.source_assignment_id%type := 0;

   Begin
    For Rec in c1(cSet_of_books_id, cInstance_id, cSource_id)  loop
      vResult := Rec.Source_Assignment_id;
    End Loop;
    Return(vResult);
  End Get_Source_Assignment_by_Book;

  -- **************************************************************************
  --     Get_Source_Assignment_by_Org (override)
  -- **************************************************************************
  Function Get_Source_Assignment_by_Org( cOrg_id                 in number
                                       , cSource_id              in number default Null
                                       , cInstance_id            in number default 0
                                       , cApplication_Name       in varchar2) Return Number is

  Cursor c1(pOrg_id in number, pInstance_id in number, pSource_id in number, pApplication_Name in varchar2) is
  select Source_Assignment_id
   from XXCP_source_assignments a
  where a.org_id      = pOrg_id
    and a.instance_id = pInstance_id
    and a.source_id   = nvl(pSource_id,source_id)
    and nvl(a.active, 'Y') = 'Y'
    and a.application_usage_id = any(select x.numeric_code  from XXCP_lookups x 
                                      where x.lookup_type = 'SOURCE ASSIGNMENT USAGE'
                                        and x.lookup_code = pApplication_Name);

   vResult XXCP_source_assignments.source_assignment_id%type := 0;

   Begin
    For Rec in c1(cOrg_id, cInstance_id, cSource_id, cApplication_Name)  loop
      vResult := Rec.Source_Assignment_id;
    End Loop;
    Return(vResult);
  End Get_Source_Assignment_by_Org;

  -- **************************************************************************
  --     Get_Source_Assignment_by_Org
  -- **************************************************************************
  Function Get_Source_Assignment_by_Org( cOrg_id                 in number
                                       , cSource_id              in number default Null
                                       , cInstance_id            in number default 0) Return Number is

  Cursor c1(pOrg_id in number, pInstance_id in number, pSource_id in number) is
  select Source_Assignment_id
   from XXCP_source_assignments a
  where a.org_id      = pOrg_id
    and a.instance_id = pInstance_id
    and a.source_id   = nvl(pSource_id,source_id)
    and nvl(a.active, 'Y') = 'Y';

   vResult XXCP_source_assignments.source_assignment_id%type := 0;

   Begin
    For Rec in c1(cOrg_id, cInstance_id, cSource_id)  loop
      vResult := Rec.Source_Assignment_id;
    End Loop;
    Return(vResult);
  End Get_Source_Assignment_by_Org;

  -- **************************************************************************
  --     Get_Grouping_Rule_by_Name
  -- **************************************************************************
   Function Get_Grouping_Rule_by_Name( cGrouping_Rule_Name     in varchar2
                                     , cSource_id              in number) Return Number is

    Cursor c1(pGrouping_Rule_Name in varchar2, pSource_id in number) is
      Select g.GROUPING_RULE_ID
       from XXCP_grouping_rules g
       where g.source_id = pSource_id
         and g.GROUPING_RULE_NAME = pGrouping_Rule_Name;

   vResult XXCP_grouping_rules.grouping_rule_id%type := 0;

   Begin

      For Rec in c1(cGrouping_Rule_Name, cSource_id) loop
        vResult := rec.GROUPING_RULE_ID;
      End Loop;
      Return(vResult);

   End Get_Grouping_Rule_by_Name;


  -- **************************************************************************
  --     Get_Material_Transaction_Type
  -- **************************************************************************
  Function Get_Material_Transaction_Type(cTransaction_id in Number)
    return number is

    vTransaction_type_id mtl_material_transactions.transaction_type_id%type;

    Cursor MT(pTransaction_id in number) is
      Select t.transaction_type_id
        from mtl_material_transactions m, mtl_transaction_types t
       where m.transaction_type_id = t.transaction_type_id
         and m.transaction_id = pTransaction_id;

  begin
    vTransaction_type_id := Null;

    If nvl(cTransaction_id, 0) > 0 then
      For MTRec in MT(cTransaction_id) loop
        vTransaction_type_id := MTRec.Transaction_type_id;
      End Loop;
    End If;
    Return(vTransaction_type_id);
  End Get_Material_Transaction_Type;

  -- **************************************************************************
  --
  -- Assignment_Control_Active
  --
  -- NOTES
  --
  --  This function checks to see if we need to process the GL row or Not.
  -- **************************************************************************
  Function Assignment_Control_Active(cInstance_id          in number,
                                     cSet_of_books_id      in number,
                                     cSource_Name          in varchar2,
                                     cCategory_Name        in varchar2,
                                     cTransaction_Table    in varchar2,
                                     cTransaction_Type     in out varchar2,
                                     cTransaction_id       in varchar2, -- must be varchar2
                                     cSource_Assignment_id out number)
    Return Boolean is

    vACF                  boolean := False;
    vSource_Assignment_id XXCP_source_assignments.source_assignment_id%type := 0;

    Cursor ACF(pInstance_id in number, pSet_of_books_id in number, pTransaction_Table in varchar2, pTransaction_Type in varchar2) is
      select a.source_assignment_id
        from XXCP_assignment_ctl     c,
             XXCP_source_assignments a,
             XXCP_sys_source_types   x,
             XXCP_sys_source_tables  w,
             XXCP_sys_sources        s
       where c.source_id = a.source_id
         and a.instance_id = pInstance_id
         and a.set_of_books_id = pSet_of_books_id
         and c.active = 'Y'
         and s.active = 'Y'
         and a.source_id = s.source_id
         and c.Transaction_set_id = x.Transaction_set_id
         and x.TYPE               = pTransaction_Type
         and x.SOURCE_TABLE_ID    = w.source_table_id
         and w.Transaction_Table  = pTransaction_Table;

    vTransaction_Type   XXCP_sys_source_types.type%type;
    vTransaction_Set_id XXCP_assignment_ctl.transaction_set_id%type := 0;

    Cursor DefTyp(pTransaction_Table in varchar2, pSystem_Latch in varchar2) is
      select c.type New_Type, c.TRANSACTION_SET_ID
        from XXCP_sys_source_tables t, XXCP_sys_source_types c
       where t.TRANSACTION_TABLE = pTransaction_Table
         and c.SOURCE_TABLE_ID   = t.SOURCE_TABLE_ID
         and t.Type_MAPPING_REQ  = 'Y'
         and (c.SYSTEM_LATCH     = pSystem_Latch or c.DEFAULT_TYPE = 'Y')
       order by Decode(System_latch, Null, 99, 0);

  Begin

    vTransaction_Type := cTransaction_Type;

    If cSource_Name = 'Inventory' then
      -- MTL
      If cCategory_Name = 'MTL' then
        vTransaction_Type := to_char(Get_Material_Transaction_Type(to_number(cTransaction_id)));
      End If;
    End If;

    For DefRec in DefTyp(cTransaction_Table, vTransaction_Type) Loop
      vTransaction_Type   := DefRec.New_Type;
      vTransaction_Set_id := DefRec.Transaction_Set_id;
      Exit;
    End Loop;

    For ACFRec in ACF(cInstance_id,
                      cSet_of_books_id,
                      cTransaction_Table,
                      vTransaction_Type) Loop
      vSource_Assignment_id := ACFRec.Source_Assignment_id;
      vACF                  := True;
      Exit;
    End Loop;

    cSource_Assignment_id := vSource_Assignment_id;
    cTransaction_Type     := vTransaction_Type;

    Return(vACF);
  End Assignment_Control_Active;

  -- ****************************************************************************
  --
  -- Transaction_Attribute_Value
  --
  -- NOTES
  --
  --  This funcation gets data from a Transaction Attribute based on Column Name
  -- ****************************************************************************
  Function Attribute_Value(cColumn_Name in varchar2, cAttribute_id in number) return varchar2 is

   vResult varchar2(250);

  begin

    if nvl(cAttribute_id,0) > 0 and cColumn_Name is not null then
     Begin
       If nvl(xxcp_global.Preview_on,'N') = 'N' then
       -- Normal Mode
              Select decode(upper(cColumn_Name)
              ,'ADJUSTMENT_RATE',TO_CHAR(ADJUSTMENT_RATE)
              ,'ADJUSTMENT_RATE_ID',TO_CHAR(ADJUSTMENT_RATE_ID)
              ,'ATTRIBUTE1',ATTRIBUTE1
              ,'ATTRIBUTE2',ATTRIBUTE2
              ,'ATTRIBUTE3',ATTRIBUTE3
              ,'ATTRIBUTE4',ATTRIBUTE4
              ,'ATTRIBUTE5',ATTRIBUTE5
              ,'CREATED_BY',TO_CHAR(CREATED_BY)
              ,'CREATION_DATE',TO_CHAR(CREATION_DATE)
              ,'ED_QUALIFIER1',ED_QUALIFIER1
              ,'ED_QUALIFIER2',ED_QUALIFIER2
              ,'EXCHANGE_DATE',TO_CHAR(EXCHANGE_DATE)
              ,'FROZEN',FROZEN
              ,'HEADER_ID',TO_CHAR(HEADER_ID)
              ,'IC_CONTROL',IC_CONTROL
              ,'IC_CURRENCY',IC_CURRENCY
              ,'IC_TAX_RATE',TO_CHAR(IC_TAX_RATE)
              ,'IC_TRADE_TAX_ID',TO_CHAR(IC_TRADE_TAX_ID)
              ,'IC_UNIT_PRICE',TO_CHAR(IC_UNIT_PRICE)
              ,'INTERNAL_ERROR_CODE',TO_CHAR(INTERNAL_ERROR_CODE)
              ,'LAST_UPDATED_BY',TO_CHAR(LAST_UPDATED_BY)
              ,'LAST_UPDATE_DATE',TO_CHAR(LAST_UPDATE_DATE)
              ,'LAST_UPDATE_LOGIN',TO_CHAR(LAST_UPDATE_LOGIN)
              ,'MC_QUALIFIER1',MC_QUALIFIER1
              ,'MC_QUALIFIER2',MC_QUALIFIER2
              ,'MC_QUALIFIER3',MC_QUALIFIER3
              ,'MC_QUALIFIER4',MC_QUALIFIER4
              ,'OLS_DATE',TO_CHAR(OLS_DATE)
              ,'OWNER_TAX_REG_ID',TO_CHAR(OWNER_TAX_REG_ID)
              ,'PARENT_TRX_ID',TO_CHAR(PARENT_TRX_ID)
              ,'PARTNER_ASSIGN_RULE_ID',TO_CHAR(PARTNER_ASSIGN_RULE_ID)
              ,'PARTNER_ASSOC_ID',TO_CHAR(PARTNER_ASSOC_ID)
              ,'PARTNER_LEGAL_CURR',PARTNER_LEGAL_CURR
              ,'PARTNER_LEGAL_EXCH_RATE',TO_CHAR(PARTNER_LEGAL_EXCH_RATE)
              ,'PARTNER_REQUIRED',PARTNER_REQUIRED
              ,'PARTNER_TAX_REG_ID',TO_CHAR(PARTNER_TAX_REG_ID)
              ,'PRICE_METHOD_ID',TO_CHAR(PRICE_METHOD_ID)
              ,'QUANTITY',TO_CHAR(QUANTITY)
              ,'SET_OF_BOOKS_ID',TO_CHAR(SET_OF_BOOKS_ID)
              ,'SOURCE_ASSIGNMENT_ID',TO_CHAR(SOURCE_ASSIGNMENT_ID)
              ,'TC_QUALIFIER1',TC_QUALIFIER1
              ,'TC_QUALIFIER2',TC_QUALIFIER2
              ,'TRADING_SET',TRADING_SET
              ,'TRANSACTION_CLASS',TRANSACTION_CLASS
              ,'TRANSACTION_DATE',TO_CHAR(TRANSACTION_DATE)
              ,'TRANSACTION_ID',TO_CHAR(TRANSACTION_ID)
              ,'TRANSACTION_REF1',TRANSACTION_REF1
              ,'TRANSACTION_REF2',TRANSACTION_REF2
              ,'TRANSACTION_REF3',TRANSACTION_REF3
              ,'TRANSACTION_TABLE',TRANSACTION_TABLE
              ,'TRANSACTION_TYPE',TRANSACTION_TYPE
              ,'UOM',UOM
              ) into vResult
              from xxcp_transaction_attributes_v a
              where attribute_id = cAttribute_id;
       Else
              Select decode(upper(cColumn_Name)
              ,'ADJUSTMENT_RATE',TO_CHAR(ADJUSTMENT_RATE)
              ,'ADJUSTMENT_RATE_ID',TO_CHAR(ADJUSTMENT_RATE_ID)
              ,'ATTRIBUTE1',ATTRIBUTE1
              ,'ATTRIBUTE2',ATTRIBUTE2
              ,'ATTRIBUTE3',ATTRIBUTE3
              ,'ATTRIBUTE4',ATTRIBUTE4
              ,'ATTRIBUTE5',ATTRIBUTE5
              ,'CREATED_BY',TO_CHAR(CREATED_BY)
              ,'CREATION_DATE',TO_CHAR(CREATION_DATE)
              ,'ED_QUALIFIER1',ED_QUALIFIER1
              ,'ED_QUALIFIER2',ED_QUALIFIER2
              ,'EXCHANGE_DATE',TO_CHAR(EXCHANGE_DATE)
              ,'FROZEN',FROZEN
              ,'HEADER_ID',TO_CHAR(HEADER_ID)
              ,'IC_CONTROL',IC_CONTROL
              ,'IC_CURRENCY',IC_CURRENCY
              ,'IC_TAX_RATE',TO_CHAR(IC_TAX_RATE)
              ,'IC_TRADE_TAX_ID',TO_CHAR(IC_TRADE_TAX_ID)
              ,'IC_UNIT_PRICE',TO_CHAR(IC_UNIT_PRICE)
              ,'INTERNAL_ERROR_CODE',TO_CHAR(INTERNAL_ERROR_CODE)
              ,'LAST_UPDATED_BY',TO_CHAR(LAST_UPDATED_BY)
              ,'LAST_UPDATE_DATE',TO_CHAR(LAST_UPDATE_DATE)
              ,'LAST_UPDATE_LOGIN',TO_CHAR(LAST_UPDATE_LOGIN)
              ,'MC_QUALIFIER1',MC_QUALIFIER1
              ,'MC_QUALIFIER2',MC_QUALIFIER2
              ,'MC_QUALIFIER3',MC_QUALIFIER3
              ,'MC_QUALIFIER4',MC_QUALIFIER4
              ,'OLS_DATE',TO_CHAR(OLS_DATE)
              ,'OWNER_TAX_REG_ID',TO_CHAR(OWNER_TAX_REG_ID)
              ,'PARENT_TRX_ID',TO_CHAR(PARENT_TRX_ID)
              ,'PARTNER_ASSIGN_RULE_ID',TO_CHAR(PARTNER_ASSIGN_RULE_ID)
              ,'PARTNER_ASSOC_ID',TO_CHAR(PARTNER_ASSOC_ID)
              ,'PARTNER_LEGAL_CURR',PARTNER_LEGAL_CURR
              ,'PARTNER_LEGAL_EXCH_RATE',TO_CHAR(PARTNER_LEGAL_EXCH_RATE)
              ,'PARTNER_REQUIRED',PARTNER_REQUIRED
              ,'PARTNER_TAX_REG_ID',TO_CHAR(PARTNER_TAX_REG_ID)
              ,'PRICE_METHOD_ID',TO_CHAR(PRICE_METHOD_ID)
              ,'QUANTITY',TO_CHAR(QUANTITY)
              ,'SET_OF_BOOKS_ID',TO_CHAR(SET_OF_BOOKS_ID)
              ,'SOURCE_ASSIGNMENT_ID',TO_CHAR(SOURCE_ASSIGNMENT_ID)
              ,'TC_QUALIFIER1',TC_QUALIFIER1
              ,'TC_QUALIFIER2',TC_QUALIFIER2
              ,'TRADING_SET',TRADING_SET
              ,'TRANSACTION_CLASS',TRANSACTION_CLASS
              ,'TRANSACTION_DATE',TO_CHAR(TRANSACTION_DATE)
              ,'TRANSACTION_ID',TO_CHAR(TRANSACTION_ID)
              ,'TRANSACTION_REF1',TRANSACTION_REF1
              ,'TRANSACTION_REF2',TRANSACTION_REF2
              ,'TRANSACTION_REF3',TRANSACTION_REF3
              ,'TRANSACTION_TABLE',TRANSACTION_TABLE
              ,'TRANSACTION_TYPE',TRANSACTION_TYPE
              ,'UOM',UOM
              ) into vResult
              from xxcp_pv_transaction_attrib_v a
              where a.attribute_id = cAttribute_id
                and a.preview_id = xxcp_global.gCommon(1).Preview_ctl;
       End If;

       Exception when others then null;
     End;
    End If;

    Return(vResult);

  End Attribute_Value;


  -- ****************************************************************************
  --
  -- Set_Custom_Data
  --
  -- NOTES
  --
  --  This funcation sets the Attributes 1-10 on the Transaction Attributes table
  -- ****************************************************************************
  Function Set_Custom_Data(cAttribute_Number in number, cOriginal_Value in varchar2) return varchar2 is

    vValue XXCP_transaction_attributes.attribute1%type;

  Begin

       xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE_CHECK := 'Y';

       vValue := substr(cOriginal_Value,1,100);

       Case
         when cAttribute_Number = 1 then
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE1 := vValue;
         when cAttribute_Number = 2 then
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE2 := vValue;
         when cAttribute_Number = 3 then
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE3 := vValue;
         when cAttribute_Number = 4 then
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE4 := vValue;
         when cAttribute_Number = 5 then
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE5 := vValue;
         when cAttribute_Number = 6 then
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE6 := vValue;
         when cAttribute_Number = 7 then
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE7 := vValue;
         when cAttribute_Number = 8 then
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE8 := vValue;
         when cAttribute_Number = 9 then
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE9 := vValue;
         when cAttribute_Number = 10 then
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE10 := vValue;
       End Case;

       Return(vValue);

  End Set_Custom_Data;

  --
  -- Set RealTime Results Record (vt_results_attribute1-5)
  --
  Function Set_RealTime_Attribute(cAttribute_Number in number,cValue in varchar2) return varchar2 is

    vValue XXCP_transaction_attributes.attribute1%type;

  Begin

       vValue := substr(cValue,1,100);

       Case
         when cAttribute_Number = 1 then
           xxcp_wks.gRealTime_Results.Attribute1 := vValue;
         when cAttribute_Number = 2 then
           xxcp_wks.gRealTime_Results.Attribute2 := vValue;
         when cAttribute_Number = 3 then
           xxcp_wks.gRealTime_Results.Attribute3 := vValue;
         when cAttribute_Number = 4 then
           xxcp_wks.gRealTime_Results.Attribute4 := vValue;
         when cAttribute_Number = 5 then
           xxcp_wks.gRealTime_Results.Attribute5 := vValue;
         when cAttribute_Number = 6 then
           xxcp_wks.gRealTime_Results.Attribute6 := vValue;
         when cAttribute_Number = 7 then
           xxcp_wks.gRealTime_Results.Attribute7 := vValue;
         when cAttribute_Number = 8 then
           xxcp_wks.gRealTime_Results.Attribute8 := vValue;
         when cAttribute_Number = 9 then
           xxcp_wks.gRealTime_Results.Attribute9 := vValue;
         when cAttribute_Number = 10 then
           xxcp_wks.gRealTime_Results.Attribute10 := vValue;
         -- 02.04.08           
         when cAttribute_Number = 11 then
           xxcp_wks.gRealTime_Results.Attribute11 := vValue;
         when cAttribute_Number = 12 then
           xxcp_wks.gRealTime_Results.Attribute12 := vValue;
         when cAttribute_Number = 13 then
           xxcp_wks.gRealTime_Results.Attribute13 := vValue;
         when cAttribute_Number = 14 then
           xxcp_wks.gRealTime_Results.Attribute14 := vValue;
         when cAttribute_Number = 15 then
           xxcp_wks.gRealTime_Results.Attribute15 := vValue;
         when cAttribute_Number = 16 then
           xxcp_wks.gRealTime_Results.Attribute16 := vValue;
         when cAttribute_Number = 17 then
           xxcp_wks.gRealTime_Results.Attribute17 := vValue;
         when cAttribute_Number = 18 then
           xxcp_wks.gRealTime_Results.Attribute18 := vValue;
         when cAttribute_Number = 19 then
           xxcp_wks.gRealTime_Results.Attribute19 := vValue;
         when cAttribute_Number = 20 then
           xxcp_wks.gRealTime_Results.Attribute20 := vValue;    
         when cAttribute_Number = 21 then
           xxcp_wks.gRealTime_Results.Attribute21 := vValue;
         when cAttribute_Number = 22 then
           xxcp_wks.gRealTime_Results.Attribute22 := vValue;
         when cAttribute_Number = 23 then
           xxcp_wks.gRealTime_Results.Attribute23 := vValue;
         when cAttribute_Number = 24 then
           xxcp_wks.gRealTime_Results.Attribute24 := vValue;
         when cAttribute_Number = 25 then
           xxcp_wks.gRealTime_Results.Attribute25 := vValue;
         when cAttribute_Number = 26 then
           xxcp_wks.gRealTime_Results.Attribute26 := vValue;
         when cAttribute_Number = 27 then
           xxcp_wks.gRealTime_Results.Attribute27 := vValue;
         when cAttribute_Number = 28 then
           xxcp_wks.gRealTime_Results.Attribute28 := vValue;
         when cAttribute_Number = 29 then
           xxcp_wks.gRealTime_Results.Attribute29 := vValue;
         when cAttribute_Number = 30 then
           xxcp_wks.gRealTime_Results.Attribute30 := vValue;             
         -- 02.06.01           
         when cAttribute_Number = 31 then
           xxcp_wks.gRealTime_Results.Attribute31 := vValue;
         when cAttribute_Number = 32 then
           xxcp_wks.gRealTime_Results.Attribute32 := vValue;
         when cAttribute_Number = 33 then
           xxcp_wks.gRealTime_Results.Attribute33 := vValue;
         when cAttribute_Number = 34 then
           xxcp_wks.gRealTime_Results.Attribute34 := vValue;
         when cAttribute_Number = 35 then
           xxcp_wks.gRealTime_Results.Attribute35 := vValue;
         when cAttribute_Number = 36 then
           xxcp_wks.gRealTime_Results.Attribute36 := vValue;
         when cAttribute_Number = 37 then
           xxcp_wks.gRealTime_Results.Attribute37 := vValue;
         when cAttribute_Number = 38 then
           xxcp_wks.gRealTime_Results.Attribute38 := vValue;
         when cAttribute_Number = 39 then
           xxcp_wks.gRealTime_Results.Attribute39 := vValue;
         when cAttribute_Number = 40 then
           xxcp_wks.gRealTime_Results.Attribute40 := vValue;
         when cAttribute_Number = 41 then
           xxcp_wks.gRealTime_Results.Attribute41 := vValue;
         when cAttribute_Number = 42 then
           xxcp_wks.gRealTime_Results.Attribute42 := vValue;
         when cAttribute_Number = 43 then
           xxcp_wks.gRealTime_Results.Attribute43 := vValue;
         when cAttribute_Number = 44 then
           xxcp_wks.gRealTime_Results.Attribute44 := vValue;
         when cAttribute_Number = 45 then
           xxcp_wks.gRealTime_Results.Attribute45 := vValue;
         when cAttribute_Number = 46 then
           xxcp_wks.gRealTime_Results.Attribute46 := vValue;
         when cAttribute_Number = 47 then
           xxcp_wks.gRealTime_Results.Attribute47 := vValue;
         when cAttribute_Number = 48 then
           xxcp_wks.gRealTime_Results.Attribute48 := vValue;
         when cAttribute_Number = 49 then
           xxcp_wks.gRealTime_Results.Attribute49 := vValue;
         when cAttribute_Number = 50 then
           xxcp_wks.gRealTime_Results.Attribute50 := vValue;
        Else
           Null;
       End Case;

       Return(vValue);

  End Set_RealTime_Attribute;

  --
  -- Same as Set_RealTime_Attribute but does not return a value
  -- for use in Gateway Utils.
  --
  Procedure Put_RealTime_Attribute(cAttribute_Number in number,cValue in varchar2)is

    vValue XXCP_transaction_attributes.attribute1%type;

  Begin

       vValue := substr(cValue,1,100);

       Case
         when cAttribute_Number = 1 then
           xxcp_wks.gRealTime_Results.Attribute1 := vValue;
         when cAttribute_Number = 2 then
           xxcp_wks.gRealTime_Results.Attribute2 := vValue;
         when cAttribute_Number = 3 then
           xxcp_wks.gRealTime_Results.Attribute3 := vValue;
         when cAttribute_Number = 4 then
           xxcp_wks.gRealTime_Results.Attribute4 := vValue;
         when cAttribute_Number = 5 then
           xxcp_wks.gRealTime_Results.Attribute5 := vValue;
         when cAttribute_Number = 6 then
           xxcp_wks.gRealTime_Results.Attribute6 := vValue;
         when cAttribute_Number = 7 then
           xxcp_wks.gRealTime_Results.Attribute7 := vValue;
         when cAttribute_Number = 8 then
           xxcp_wks.gRealTime_Results.Attribute8 := vValue;
         when cAttribute_Number = 9 then
           xxcp_wks.gRealTime_Results.Attribute9 := vValue;
         when cAttribute_Number = 10 then
           xxcp_wks.gRealTime_Results.Attribute10 := vValue;
         -- 02.04.08          
         when cAttribute_Number = 11 then
           xxcp_wks.gRealTime_Results.Attribute11 := vValue;
         when cAttribute_Number = 12 then
           xxcp_wks.gRealTime_Results.Attribute12 := vValue;
         when cAttribute_Number = 13 then
           xxcp_wks.gRealTime_Results.Attribute13 := vValue;
         when cAttribute_Number = 14 then
           xxcp_wks.gRealTime_Results.Attribute14 := vValue;
         when cAttribute_Number = 15 then
           xxcp_wks.gRealTime_Results.Attribute15 := vValue;
         when cAttribute_Number = 16 then
           xxcp_wks.gRealTime_Results.Attribute16 := vValue;
         when cAttribute_Number = 17 then
           xxcp_wks.gRealTime_Results.Attribute17 := vValue;
         when cAttribute_Number = 18 then
           xxcp_wks.gRealTime_Results.Attribute18 := vValue;
         when cAttribute_Number = 19 then
           xxcp_wks.gRealTime_Results.Attribute19 := vValue;
         when cAttribute_Number = 20 then
           xxcp_wks.gRealTime_Results.Attribute20 := vValue;    
         when cAttribute_Number = 21 then
           xxcp_wks.gRealTime_Results.Attribute21 := vValue;
         when cAttribute_Number = 22 then
           xxcp_wks.gRealTime_Results.Attribute22 := vValue;
         when cAttribute_Number = 23 then
           xxcp_wks.gRealTime_Results.Attribute23 := vValue;
         when cAttribute_Number = 24 then
           xxcp_wks.gRealTime_Results.Attribute24 := vValue;
         when cAttribute_Number = 25 then
           xxcp_wks.gRealTime_Results.Attribute25 := vValue;
         when cAttribute_Number = 26 then
           xxcp_wks.gRealTime_Results.Attribute26 := vValue;
         when cAttribute_Number = 27 then
           xxcp_wks.gRealTime_Results.Attribute27 := vValue;
         when cAttribute_Number = 28 then
           xxcp_wks.gRealTime_Results.Attribute28 := vValue;
         when cAttribute_Number = 29 then
           xxcp_wks.gRealTime_Results.Attribute29 := vValue;
         when cAttribute_Number = 30 then
           xxcp_wks.gRealTime_Results.Attribute30 := vValue;  
         -- 02.06.01           
         when cAttribute_Number = 31 then
           xxcp_wks.gRealTime_Results.Attribute31 := vValue;
         when cAttribute_Number = 32 then
           xxcp_wks.gRealTime_Results.Attribute32 := vValue;
         when cAttribute_Number = 33 then
           xxcp_wks.gRealTime_Results.Attribute33 := vValue;
         when cAttribute_Number = 34 then
           xxcp_wks.gRealTime_Results.Attribute34 := vValue;
         when cAttribute_Number = 35 then
           xxcp_wks.gRealTime_Results.Attribute35 := vValue;
         when cAttribute_Number = 36 then
           xxcp_wks.gRealTime_Results.Attribute36 := vValue;
         when cAttribute_Number = 37 then
           xxcp_wks.gRealTime_Results.Attribute37 := vValue;
         when cAttribute_Number = 38 then
           xxcp_wks.gRealTime_Results.Attribute38 := vValue;
         when cAttribute_Number = 39 then
           xxcp_wks.gRealTime_Results.Attribute39 := vValue;
         when cAttribute_Number = 40 then
           xxcp_wks.gRealTime_Results.Attribute40 := vValue;
         when cAttribute_Number = 41 then
           xxcp_wks.gRealTime_Results.Attribute41 := vValue;
         when cAttribute_Number = 42 then
           xxcp_wks.gRealTime_Results.Attribute42 := vValue;
         when cAttribute_Number = 43 then
           xxcp_wks.gRealTime_Results.Attribute43 := vValue;
         when cAttribute_Number = 44 then
           xxcp_wks.gRealTime_Results.Attribute44 := vValue;
         when cAttribute_Number = 45 then
           xxcp_wks.gRealTime_Results.Attribute45 := vValue;
         when cAttribute_Number = 46 then
           xxcp_wks.gRealTime_Results.Attribute46 := vValue;
         when cAttribute_Number = 47 then
           xxcp_wks.gRealTime_Results.Attribute47 := vValue;
         when cAttribute_Number = 48 then
           xxcp_wks.gRealTime_Results.Attribute48 := vValue;
         when cAttribute_Number = 49 then
           xxcp_wks.gRealTime_Results.Attribute49 := vValue;
         when cAttribute_Number = 50 then
           xxcp_wks.gRealTime_Results.Attribute50 := vValue;
        Else
           Null;
       End Case;

  End Put_RealTime_Attribute;

  --
  -- Clear_RealTime_Attributes
  --
  Procedure Clear_RealTime_Attributes is
    begin
      xxcp_wks.gRealTime_Results := xxcp_wks.gEmpty_RealTime_Results;
    End Clear_RealTime_Attributes;


  -- *******************************************************************************
  --
  -- Debug_Write_Trace
  --
  -- NOTES
  --
  --  If the Transactions is being used in Trace mode then the value will be written
  --  out to the ERRORS table. This can be used by Dynamic Interfaces when trying
  --  to debug SQL values.
  -- *******************************************************************************
  Function Debug_Write_Trace(cOriginal_Value in varchar2) return varchar2 is

  Begin

    If xxcp_global.Trace_on = 'Y' then
      xxcp_foundation.FndWriteError(100,'TRACE: GATEWAY = '||cOriginal_Value);
    End If;

    Return(cOriginal_Value);

  End Debug_Write_Trace;


  -- *******************************************************************************
  --
  -- Cache_by_ID
  --
  -- NOTES
  --
  --  This funcation gets data out of the cache based on its Dynamic Attribute ID
  -- *******************************************************************************
  Function Cache_by_id_base
                        (cSource_id            in number, 
                         cDynamic_Attribute_id in number, 
                         cAttribute_id         in number,
                         cIgnorePreviewMode    in varchar2 default 'N') return varchar2 is

    vResult     varchar2(250);
    vMaxValues  number(2) := 51;

    vData_Array XXCP_DYNAMIC_ARRAY :=  XXCP_DYNAMIC_ARRAY(' ');
    j           pls_integer := 0;
    k           integer;
    vCached_qty integer;
    vSeq        XXCP_transaction_cache.seq%type;
    vMatchFound varchar2(1) := 'N';

  Begin

    For vSeq in 1..2 Loop

      vData_Array.Delete;
      vData_Array.Extend(vMaxValues);

      Begin
        If nvl(xxcp_global.Preview_on,'N') = 'N' or cIgnorePreviewMode = 'Y' then
             -- Normal Mode
             Select
               CACHED_VALUE1 ,CACHED_VALUE2 ,CACHED_VALUE3 ,CACHED_VALUE4 ,CACHED_VALUE5
              ,CACHED_VALUE6 ,CACHED_VALUE7 ,CACHED_VALUE8 ,CACHED_VALUE9 ,CACHED_VALUE10
              ,CACHED_VALUE11,CACHED_VALUE12,CACHED_VALUE13,CACHED_VALUE14,CACHED_VALUE15
              ,CACHED_VALUE16,CACHED_VALUE17,CACHED_VALUE18,CACHED_VALUE19,CACHED_VALUE20
              ,CACHED_VALUE21,CACHED_VALUE22,CACHED_VALUE23,CACHED_VALUE24,CACHED_VALUE25
              ,CACHED_VALUE26,CACHED_VALUE27,CACHED_VALUE28,CACHED_VALUE29,CACHED_VALUE30
              ,CACHED_VALUE31,CACHED_VALUE32,CACHED_VALUE33,CACHED_VALUE34,CACHED_VALUE35
              ,CACHED_VALUE36,CACHED_VALUE37,CACHED_VALUE38,CACHED_VALUE39,CACHED_VALUE40
              ,CACHED_VALUE41,CACHED_VALUE42,CACHED_VALUE43,CACHED_VALUE44,CACHED_VALUE45
              ,CACHED_VALUE46,CACHED_VALUE47,CACHED_VALUE48,CACHED_VALUE49,CACHED_VALUE50,CACHED_QTY
             into
              vData_Array(1), vData_Array(2), vData_Array(3), vData_Array(4), vData_Array(5),
              vData_Array(6), vData_Array(7), vData_Array(8), vData_Array(9), vData_Array(10),
              vData_Array(11), vData_Array(12), vData_Array(13), vData_Array(14), vData_Array(15),
              vData_Array(16), vData_Array(17), vData_Array(18), vData_Array(19), vData_Array(20),
              vData_Array(21), vData_Array(22), vData_Array(23), vData_Array(24), vData_Array(25),
              vData_Array(26), vData_Array(27), vData_Array(28), vData_Array(29), vData_Array(30),
              vData_Array(31), vData_Array(32), vData_Array(33), vData_Array(34), vData_Array(35),
              vData_Array(36), vData_Array(37), vData_Array(38), vData_Array(39), vData_Array(40),
              vData_Array(41), vData_Array(42), vData_Array(43), vData_Array(44), vData_Array(45),
              vData_Array(46), vData_Array(47), vData_Array(48), vData_Array(49), vData_Array(50),
              vData_Array(51)
             from XXCP_transaction_cache c
             where attribute_id = cAttribute_id
              and seq = vSeq;
              
        Else
              -- Preview Mode
              Select
               CACHED_VALUE1 ,CACHED_VALUE2 ,CACHED_VALUE3 ,CACHED_VALUE4 ,CACHED_VALUE5
              ,CACHED_VALUE6 ,CACHED_VALUE7 ,CACHED_VALUE8 ,CACHED_VALUE9 ,CACHED_VALUE10
              ,CACHED_VALUE11,CACHED_VALUE12,CACHED_VALUE13,CACHED_VALUE14,CACHED_VALUE15
              ,CACHED_VALUE16,CACHED_VALUE17,CACHED_VALUE18,CACHED_VALUE19,CACHED_VALUE20
              ,CACHED_VALUE21,CACHED_VALUE22,CACHED_VALUE23,CACHED_VALUE24,CACHED_VALUE25
              ,CACHED_VALUE26,CACHED_VALUE27,CACHED_VALUE28,CACHED_VALUE29,CACHED_VALUE30
              ,CACHED_VALUE31,CACHED_VALUE32,CACHED_VALUE33,CACHED_VALUE34,CACHED_VALUE35
              ,CACHED_VALUE36,CACHED_VALUE37,CACHED_VALUE38,CACHED_VALUE39,CACHED_VALUE40
              ,CACHED_VALUE41,CACHED_VALUE42,CACHED_VALUE43,CACHED_VALUE44,CACHED_VALUE45
              ,CACHED_VALUE46,CACHED_VALUE47,CACHED_VALUE48,CACHED_VALUE49,CACHED_VALUE50,CACHED_QTY
              into
               vData_Array(1), vData_Array(2), vData_Array(3), vData_Array(4), vData_Array(5),
               vData_Array(6), vData_Array(7), vData_Array(8), vData_Array(9), vData_Array(10),
               vData_Array(11), vData_Array(12), vData_Array(13), vData_Array(14), vData_Array(15),
               vData_Array(16), vData_Array(17), vData_Array(18), vData_Array(19), vData_Array(20),
               vData_Array(21), vData_Array(22), vData_Array(23), vData_Array(24), vData_Array(25),
               vData_Array(26), vData_Array(27), vData_Array(28), vData_Array(29), vData_Array(30),
               vData_Array(31), vData_Array(32), vData_Array(33), vData_Array(34), vData_Array(35),
               vData_Array(36), vData_Array(37), vData_Array(38), vData_Array(39), vData_Array(40),
               vData_Array(41), vData_Array(42), vData_Array(43), vData_Array(44), vData_Array(45),
               vData_Array(46), vData_Array(47), vData_Array(48), vData_Array(49), vData_Array(50),
               vData_Array(51)
              from XXCP_pv_transaction_cache c
              where attribute_id = cAttribute_id
                and c.preview_id = xxcp_global.gCommon(1).Preview_ctl
              and seq = vSeq;
        End If;

        vCached_qty := to_number(nvl(vData_Array(51),'0'));

        For k in 1..vCached_qty Loop
          If substr(vData_Array(k),1,4) = to_char(cDynamic_Attribute_id) Then
            vResult := substr(vData_Array(k),5,250);
            vMatchFound := 'Y';
            Exit;
          End If;
        End Loop;

        Exception when others then null;
            xxcp_foundation.show('Cache by Id Error='||SQLERRM);
      End;
    
       If vMatchFound = 'Y' then
         Exit;
       End If;
    
    End Loop;

    Return(vResult);
  End Cache_by_ID_Base;

  -- *******************************************************************************
  -- Cache_by_ID - Externally Called
  -- *******************************************************************************
  Function Cache_by_ID(cSource_id            in number, 
                       cDynamic_Attribute_id in number, 
                       cAttribute_id         in number) return varchar2 is

    vResult varchar2(250);

  Begin
    
    vResult := Cache_by_id_base
                          (cSource_id            => cSource_id,
                           cDynamic_Attribute_id => cDynamic_Attribute_id,
                           cAttribute_id         => cAttribute_id,
                           cIgnorePreviewMode    => 'N');

    return(vResult);
    
  End Cache_by_id;
  -- *******************************************************************************
  -- Cache_by_ID - Externally Called
  -- *******************************************************************************
  Function Cache_by_ID_Abs(cDynamic_Attribute_id  in number, 
                           cAttribute_id          in number,
                           cSource_id             in number default 0) return varchar2 is

    vResult    varchar2(250);
    vSource_id number(4);
    
  Begin
    
    If cSource_id > 0 then
      xxcp_global.set_source_id(cSource_id);
      vSource_id := cSource_id;
    Else
      vSource_id := xxcp_global.GET_SOURCE_ID;
    End If;
    
    vResult := Cache_by_id_base(cSource_id            => vSource_id,
                                cDynamic_Attribute_id => cDynamic_Attribute_id,
                                cAttribute_id         => cAttribute_id,
                                cIgnorePreviewMode    => 'Y');

    return(vResult);
    
  End Cache_by_id_Abs;
  -- *******************************************************************************
  --
  -- Cache_by_Name
  --
  -- NOTES
  --
  --  This funcation gets data out of the cache based on its Dynamic Attribute name
  -- *******************************************************************************
  Function Cache_by_Name(cSource_id in number, cDynamic_Attribute_Name in varchar2, cAttribute_id in number) return varchar2 is

    vResult varchar2(250);

    Cursor cda(pDynamic_Attribute_Name in varchar2) is
    Select id Dynamic_Attribute_id
     from xxcp_dynamic_attributes_v a
     where a.Attribute_name = pDynamic_Attribute_Name
       and a.activity_id in (1001, 1002);

    vDynamic_Attribute_id XXCP_dynamic_attributes.column_id%type;

  Begin
    xxcp_global.set_source_id(cSource_id);
    For Rec in cda(cDynamic_Attribute_Name)
    Loop
      vDynamic_Attribute_id := Rec.Dynamic_Attribute_id;
      vResult := Cache_by_ID_base(cSource_id,vDynamic_Attribute_id,cAttribute_id,'N');
      Exit;
    End Loop;

    Return(vResult);

  End Cache_by_Name;

  -- *******************************************************************************
  -- Cache_By_Name_Abs (Absolute)
  --
  --  This funcation translates the Internal Error Number into a Text Description.
  -- *******************************************************************************
  Function Cache_By_Name_Abs(
                       cDynamic_Attribute_Name in varchar2, 
                       cAttribute_id           in number,
                       cSource_id              in number default 0) return varchar2 is

    vResult               XXCP_transaction_cache.cached_value3%type;
    vDynamic_Attribute_id XXCP_dynamic_attributes.column_id%type;
    vSource_id            XXCP_sys_sources.source_id%type;

    Cursor cda(pDynamic_Attribute_Name in varchar2) is
    Select id Dynamic_Attribute_id
     from xxcp_dynamic_attributes_v a
     where a.Attribute_name = pDynamic_Attribute_Name
       and a.Activity_id in (1001,1002);

  Begin

    If cSource_id > 0 then
      xxcp_global.set_source_id(cSource_id);
      vSource_id := cSource_id;
    Else
      vSource_id := xxcp_global.GET_SOURCE_ID;
    End If;

    For Rec in cda(cDynamic_Attribute_Name)
    Loop
      vDynamic_Attribute_id := Rec.Dynamic_Attribute_id;
      vResult := Cache_by_ID_Base(
                             cSource_id            => vSource_id,
                             cDynamic_Attribute_id => vDynamic_Attribute_id,
                             cAttribute_id         => cAttribute_id,
                             cIgnorePreviewMode    => 'Y'
                             );

      Exit;
    End Loop;

    Return(vResult);

  End Cache_By_Name_Abs;
  -- *******************************************************************************
  -- Internal_Error_Message
  --
  -- NOTES
  --
  --  This funcation translates the Internal Error Number into a Text Description.
  -- *******************************************************************************
  Function Internal_Error_Message(cInternal_Code in number,cLanguage in varchar2 Default 'English') return varchar2 is
    vErrMessage XXCP_internal_codes.message%type;
  Begin
    vErrMessage := xxcp_foundation.Get_MessageII(cInternal_Code, cLanguage);
    Return(vErrMessage);
  End Internal_Error_Message;

  -- *******************************************************************************
  --
  -- Safe Divide by Zero
  --
  -- NOTES: Updated by Keith on 12/APR/08
  --
  --  This ensures that the divide by zero
  -- *******************************************************************************
  Function Safe_Divide (cValue in number, cQty in number) return number is

      vSafe_Divide number;

    Begin

      vSafe_Divide := xxcp_foundation.Safe_Divide(cValue,cQty);

      Return(vSafe_Divide);
    End Safe_Divide;



  -- *******************************************************************************
  -- Engine_Value
  -- *******************************************************************************
  Function Engine_Value(cDynamic_Attribute_Name in varchar2) return varchar2 is

  Cursor c1(pDynamic_Attribute_Name in varchar2) is
   Select c.id Attribute_id
        , c.column_type type
   from xxcp_dynamic_attributes_v c
   where c.attribute_name = pDynamic_Attribute_Name;

   vResult varchar2(250);

  Begin

    xxcp_global.set_source_id(xxcp_global.gCommon(1).Source_id);

    For c1rec in c1(cDynamic_Attribute_Name) Loop
      vResult := xxcp_foundation.Find_DynamicAttribute(c1rec.attribute_id);
   End Loop;

    Return(vResult);

  End Engine_Value;

  -- *******************************************************************************
  -- Engine_Value
  -- This can be used by the Custom Event routine
  -- *******************************************************************************
  Function Engine_Value(cDynamic_Attribute_id in number) return varchar2 is
    Begin
      Return(xxcp_foundation.Find_DynamicAttribute(cDynamic_Attribute_id));
    End Engine_Value;

  -- *******************************************************************************
  -- Set_Engine_Value
  -- *******************************************************************************
  Procedure Set_Engine_Value(cDynamic_Attribute_Name in varchar2, cValue in varchar2) is

  Cursor c1(pDynamic_Attribute_Name in varchar2) is
   Select c.id Attribute_id
        , c.column_type type
   from xxcp_dynamic_attributes_v c
   where c.attribute_name = pDynamic_Attribute_Name;

  Begin

    xxcp_global.set_source_id(xxcp_global.gCommon(1).Source_id);

    For c1rec in c1(cDynamic_Attribute_Name) Loop
      If c1rec.type = 1001 then
        xxcp_wks.D1001_Array(c1rec.attribute_id-1000) := cValue;
      Elsif c1rec.type = 1002 then
        xxcp_wks.D1002_Array(c1rec.attribute_id-2000) := cValue;
      Elsif c1rec.type = 1003 then
        xxcp_wks.D1003_Array(c1rec.attribute_id-3000) := cValue;
      Elsif c1rec.type = 1004 then
        xxcp_wks.D1004_Array(c1rec.attribute_id-4000) := cValue;
      End If;
    End Loop;

  End Set_Engine_Value;


  -- *******************************************************************************
  -- Set Engine_Value
  -- *******************************************************************************
  Procedure Set_Engine_Value(cDynamic_Attribute_id in number, cValue in varchar2) is
  Begin

    If (xxcp_wks.Max_Dynamic_Attribute > 0) and (cDynamic_Attribute_id <= (1000+xxcp_wks.Max_Dynamic_Attribute)) then
      If cDynamic_Attribute_id between 1010 and 1999 then
        xxcp_wks.D1001_Array(cDynamic_Attribute_id-1000) := cValue;
      ElsIf cDynamic_Attribute_id between 2001 and 2999 then
        xxcp_wks.D1001_Array(cDynamic_Attribute_id-2000) := cValue;
      ElsIf cDynamic_Attribute_id between 3001 and 3999 then
        xxcp_wks.D1001_Array(cDynamic_Attribute_id-3000) := cValue;
      ElsIf cDynamic_Attribute_id between 4001 and 4999 then
        xxcp_wks.D1001_Array(cDynamic_Attribute_id-4000) := cValue;
      End If;
    End If;

  End Set_Engine_Value;


  -- *******************************************************************************
  --
  -- User_Preview_id
  --
  -- NOTES
  --
  --
  -- *******************************************************************************
  Function User_Preview_id return Number is
    vPw  number := 0;

    Begin
     vPw := nvl(xxcp_global.gCommon(1).Preview_id,0);
     Return(vPw);
    End User_Preview_id;

  -- *******************************************************************************
  --
  -- Set_Preview_id
  --
  -- NOTES
  --
  --
  -- *******************************************************************************
  Function Set_Preview_id(cPreview_id in number) return Number is
    vPw  number := 0;

    Begin
     xxcp_global.gCommon(1).Preview_id := nvl(cPreview_id,0);
     vPw := nvl(xxcp_global.gCommon(1).Preview_id,0);
     Return(vPw);
  End Set_Preview_id;

  -- *******************************************************************************
  --
  -- Full_Sysdate
  --
  -- NOTES: Used to cache the System Date the first time this is called per session
  --
  -- *******************************************************************************
  Function Full_Sysdate return date is
    Begin
      Return(nvl(gSysdate,Sysdate));
    End Full_Sysdate;


  -- *******************************************************************************
  --
  -- Truncated_Sysdate
  --
  -- NOTES: Used to cache and truncate, the System Date the first time this is called per session
  --
  -- *******************************************************************************
  Function Truncated_Sysdate return date is
    Begin
      Return(nvl(gTruncSysdate,Trunc(Sysdate)));
    End Truncated_Sysdate;


  -- *******************************************************************************
  --
  -- Grouping_Rule_Type
  --
  -- NOTES
  --
  --
  -- *******************************************************************************
  Function Grouping_Rule_Type(cGrouping_Rule_id in number) return varchar2 is
    vResult XXCP_grouping_rules.grouping_type%type;

    Cursor c1(pGrouping_rule_id in number) is
      Select grouping_type
        from XXCP_grouping_rules
        where Grouping_Rule_id = pGrouping_rule_id;

   Begin

      For Rec in C1(cGrouping_Rule_id) loop
        vResult := Rec.grouping_type;
      End Loop;

      Return(vResult);

   End Grouping_Rule_Type;


  -- *******************************************************************************
  --
  -- AR_Highest_Delivery_id
  --
  -- NOTES
  --
  --
  -- *******************************************************************************
   Function AR_Highest_Delivery_id return varchar2 is
    vResult Number := 0;

    Cursor c1(pTransaction_table in varchar2, pParent_Trx_id in number, pRequest_id in number) is
     select  max(nvl(r.interface_line_attribute3,0)) Highest_Delivery_id
       from XXCP_ra_interface_lines r
      where vt_transaction_table = pTransaction_table
        and vt_parent_trx_id     = pParent_Trx_id
        and vt_request_id        = pRequest_id
        and nvl(amount,0)        != 0;

    Cursor c2(pTransaction_table in varchar2, pParent_Trx_id in number, pRequest_id in number) is
     select max(nvl(r.interface_line_attribute3,0)) Highest_Delivery_id
       from XXCP_ra_interface_lines r,
            XXCP_pv_interface       p
      where r.rowid                = p.VT_SOURCE_ROWID
        and p.vt_transaction_table = pTransaction_table
        and p.vt_parent_trx_id     = pParent_Trx_id
        and p.vt_preview_id        = xxcp_global.User_id
        and nvl(r.amount,0)        != 0;

    vRequest_id number;

   Begin


     vRequest_id := nvl(xxcp_global.gCommon(1).Current_request_id,0);

     -- Live
     If xxcp_global.Preview_on = 'N' then

      For Rec in C1(xxcp_global.gCommon(1).Current_Transaction_table,xxcp_global.gCommon(1).Current_Parent_Trx_id,vRequest_id ) Loop
        vResult := Rec.Highest_Delivery_id;
      End Loop;
      -- Preview Mode
     Else
      For Rec in C2(xxcp_global.gCommon(1).Current_Transaction_table,xxcp_global.gCommon(1).Current_Parent_Trx_id,vRequest_id ) Loop
        vResult := Rec.Highest_Delivery_id;
      End Loop;
     End If;

     Return(vResult);

   End AR_Highest_Delivery_id;

  -- *******************************************************************************
  --
  -- AR_Highest_Ship_to
  --
  -- NOTES: Values for the cField_name are either ADDRESS_ID or CUSTOMER_ID
  --
  --
  -- *******************************************************************************
   Function AR_Highest_Ship_to(cField_Name in varchar2) return varchar2 is
    vResult Number := 0;

    Cursor c1(pTransaction_table in varchar2, pParent_Trx_id in number, pRequest_id in number) is
     select  max(nvl(r.orig_system_ship_address_id,0)) Highest_ship_address_id
       from XXCP_ra_interface_lines r
      where vt_transaction_table = pTransaction_table
        and vt_parent_trx_id     = pParent_Trx_id
        and vt_request_id        = pRequest_id
        and nvl(amount,0)        != 0;

    Cursor c2(pTransaction_table in varchar2, pParent_Trx_id in number, pRequest_id in number) is
     select  max(nvl(r.orig_system_ship_customer_id,0)) Highest_ship_customer_id
       from XXCP_ra_interface_lines r
      where vt_transaction_table = pTransaction_table
        and vt_parent_trx_id     = pParent_Trx_id
        and vt_request_id        = pRequest_id
        and nvl(amount,0)        != 0;


    Cursor c3(pTransaction_table in varchar2, pParent_Trx_id in number, pRequest_id in number) is
     select  max(nvl(r.orig_system_ship_address_id,0)) Highest_ship_address_id
       from XXCP_ra_interface_lines r,
            XXCP_pv_interface       p
      where r.rowid                = p.VT_SOURCE_ROWID
        and p.vt_transaction_table = pTransaction_table
        and p.vt_parent_trx_id     = pParent_Trx_id
        and p.vt_preview_id        = xxcp_global.User_id
        and nvl(r.amount,0)        != 0;

    Cursor c4(pTransaction_table in varchar2, pParent_Trx_id in number, pRequest_id in number) is
     select  max(nvl(r.orig_system_ship_customer_id,0)) Highest_ship_customer_id
       from XXCP_ra_interface_lines r,
            XXCP_pv_interface       p
      where r.rowid                = p.VT_SOURCE_ROWID
        and p.vt_transaction_table = pTransaction_table
        and p.vt_parent_trx_id     = pParent_Trx_id
        and p.vt_preview_id        = xxcp_global.User_id
        and nvl(r.amount,0)        != 0;

    vRequest_id number;

   Begin

      vRequest_id := nvl(xxcp_global.gCommon(1).Current_request_id,0);

      If Upper(cField_Name) = 'ADDRESS_ID' then
        If xxcp_global.Preview_on = 'N' then
           For Rec in C1(xxcp_global.gCommon(1).Current_Transaction_table,xxcp_global.gCommon(1).Current_Parent_Trx_id,vRequest_id ) Loop
            vResult := Rec.Highest_ship_address_id;
          End Loop;
        -- Preview
        Else
           For Rec in C3(xxcp_global.gCommon(1).Current_Transaction_table,xxcp_global.gCommon(1).Current_Parent_Trx_id,vRequest_id ) Loop
            vResult := Rec.Highest_ship_address_id;
          End Loop;
        End If;
      ElsIf Upper(cField_Name) = 'CUSTOMER_ID' then
        If xxcp_global.Preview_on = 'N' then
           For Rec in C2(xxcp_global.gCommon(1).Current_Transaction_table,xxcp_global.gCommon(1).Current_Parent_Trx_id,vRequest_id ) Loop
            vResult := Rec.Highest_ship_customer_id;
          End Loop;
        Else
        -- Preview
           For Rec in C4(xxcp_global.gCommon(1).Current_Transaction_table,xxcp_global.gCommon(1).Current_Parent_Trx_id,vRequest_id ) Loop
            vResult := Rec.Highest_ship_customer_id;
          End Loop;
        End If;
      End If;
      Return(vResult);

   End AR_Highest_Ship_to;


  -- *******************************************************************************
  --
  -- AR_Highest_Ship_to
  --
  -- NOTES: Values for the cField_name are either ADDRESS_ID or CUSTOMER_ID
  --
  --
  -- *******************************************************************************
   Function AR_Highest_Ship_Actual return varchar2 is

    vResult varchar2(50) := Null;

    Cursor c1(pTransaction_table in varchar2, pParent_Trx_id in number, pRequest_id in number, pSource_Assignment_id in number) is
     select  max(nvl(ship_date_actual,gSysdate))  Highest_Ship_Actual
       from XXCP_ra_interface_lines r
      where vt_transaction_table    = pTransaction_table
        and vt_parent_trx_id        = pParent_Trx_id
        and vt_request_id           = pRequest_id
        and vt_source_assignment_id = pSource_Assignment_id
        and nvl(amount,0)          != 0;


    Cursor c2(pTransaction_table in varchar2, pParent_Trx_id in number, pRequest_id in number, pSource_Assignment_id in number) is
     select   max(ship_date_actual)  Highest_Ship_Actual
       from XXCP_ra_interface_lines r,
            XXCP_pv_interface       p
      where r.rowid                 = p.VT_SOURCE_ROWID
        and p.vt_transaction_table  = pTransaction_table
        and p.vt_parent_trx_id      = pParent_Trx_id
        and p.vt_preview_id         = xxcp_global.User_id
        and p.vt_source_assignment_id = pSource_Assignment_id
        and nvl(r.amount,0)        != 0;


    vRequest_id number;

   Begin


      vRequest_id := nvl(xxcp_global.gCommon(1).Current_request_id,0);

        If xxcp_global.Preview_on = 'N' then
           For Rec in C1(xxcp_global.gCommon(1).Current_Transaction_table,xxcp_global.gCommon(1).Current_Parent_Trx_id,vRequest_id,xxcp_global.gCommon(1).Current_Assignment_id ) Loop
            vResult := Rec.Highest_Ship_Actual;
          End Loop;
        -- Preview
        Else
           For Rec in C2(xxcp_global.gCommon(1).Current_Transaction_table,xxcp_global.gCommon(1).Current_Parent_Trx_id,vRequest_id,xxcp_global.gCommon(1).Current_Assignment_id ) Loop
            vResult := Rec.Highest_Ship_Actual;
          End Loop;
        End If;

      Return(vResult);

   End AR_Highest_Ship_Actual;
  -- *******************************************************************************
  --
  -- Entered_Value_Calculation
  --
  -- NOTES: This function does the same calculation as the VT Engine.
  --
  -- *******************************************************************************
  Function Entered_Value_Calculation( cRule_Value_Name           in varchar2,
                                      cEntered_Value             in number,
                                      cAdjustment_Rate           in number,
                                      cTax_Rate                  in number,
                                      cChangeSign                in varchar2,
                                        cTransaction_Quantity      in number,
                                      cPrecision                 in number,
                                      cExtended_Entered_Value   out number,
                                      cInternalErrorCode     in out number) Return Number is

   vResult     Number;
   vChangeSign Varchar2(3);

  Begin
    -- Change Sign.
    vChangeSign := nvl(cChangeSign,'Y');

    vResult :=
    xxcp_foundation.FndEnteredValueCalculation(cRule_Value_Name,
                                      cEntered_Value,
                                      cAdjustment_Rate,
                                      cTax_Rate,
                                      vChangeSign,
                                        cTransaction_Quantity,
                                      cPrecision,
                                      cExtended_Entered_Value,
                                      cInternalErrorCode);
   Return(vResult);


End Entered_Value_Calculation;


-- ******************************************************************************
--                            LAST_STEP_VALUE
--                         --------------------
--
-- Purpose: This funcation call was designed for Tektronix to help out with OMAR
-- Accounting, but it could be useful to others. It takes into account preview mode
-- when fetching data from transaction attributes that must be done in sequence.
-- It uses the STEP_NUMBER column to find the right attribute and orders them
-- by Trading set sequence. If the attribute it finds is "Frozen" then it returns
-- values from the custom attributes (1-10). If it is not, then a null is returned.
-- The null return value can then be captured and treated as an error.
-- 05-APR-2006.
--
-- ******************************************************************************
Function Last_Step_Value( cSource_Assignment_id    in Number,
                          cParent_Trx_id           in Number,
                          cTransaction_Table       in varchar2,
                          cStep                    in varchar2,
                          cCustom_Attribute_Num    in Number) return varchar2 is


vSource_id  XXCP_sys_sources.source_id%type;
vReturn     XXCP_transaction_attributes.attribute1%type;
vAS_NEW     XXCP_user_profiles.PV_AS_IF_NEW%type := 'N';


-- Live Run: Just use the base table.
Cursor R1(pSource_Assignment_id in number, pParent_Trx_id in number, pTransaction_Table in varchar2, pStep in varchar2  ) is
 select  t.SEQUENCE
       , a.Attribute_id
        , a.Frozen
       , a.ATTRIBUTE1
       , a.ATTRIBUTE2
       , a.ATTRIBUTE3
       , a.ATTRIBUTE4
       , nvl(to_char(a.IC_UNIT_PRICE),a.ATTRIBUTE5) ATTRIBUTE5
       , a.ATTRIBUTE6
       , a.ATTRIBUTE7
       , a.ATTRIBUTE8
       , a.ATTRIBUTE9
       , a.ATTRIBUTE10
   from xxcp_transaction_attributes_v a,
        XXCP_trading_sets t
  where a.parent_trx_id        = pParent_Trx_id
    and a.transaction_table    = pTransaction_Table
     and t.source_id            = vSource_id
    and a.trading_set          = t.trading_set
     and a.source_assignment_id = pSource_Assignment_id
    and a.STEP_NUMBER          = pStep
  order by 1 desc, 2 desc;

-- Join live and preview tables
Cursor J1(pPreview_id in number, pSource_Assignment_id in number, pParent_Trx_id in number, pTransaction_Table in varchar2, pStep in varchar2) is
 select  1 Seq
       , t.SEQUENCE
       , w.Attribute_id
       , w.Frozen
       , w.ATTRIBUTE1
       , w.ATTRIBUTE2
       , w.ATTRIBUTE3
       , w.ATTRIBUTE4
       , nvl(to_char(w.IC_UNIT_PRICE),w.ATTRIBUTE5) ATTRIBUTE5
       , w.ATTRIBUTE6
       , w.ATTRIBUTE7
       , w.ATTRIBUTE8
       , w.ATTRIBUTE9
       , w.ATTRIBUTE10
 from xxcp_pv_transaction_attrib_v w,
      XXCP_trading_sets t
 where w.preview_id           = Decode(pPreview_id,0,w.preview_id , pPreview_id)
   and w.parent_trx_id        = pParent_Trx_id
   and w.transaction_table    = pTransaction_Table
   and w.trading_set          = t.trading_set
   and t.source_id            = vSource_id
   and w.source_assignment_id = pSource_Assignment_id
   and w.STEP_NUMBER          = pStep
 UNION ALL
 select 2
      , t.SEQUENCE
      , a.Attribute_id
      , a.Frozen
      , a.ATTRIBUTE1
      , a.ATTRIBUTE2
      , a.ATTRIBUTE3
      , a.ATTRIBUTE4
       , nvl(to_char(a.IC_UNIT_PRICE),a.ATTRIBUTE5) ATTRIBUTE5
      , a.ATTRIBUTE6
      , a.ATTRIBUTE7
      , a.ATTRIBUTE8
      , a.ATTRIBUTE9
      , a.ATTRIBUTE10
  from xxcp_transaction_attributes_v a,
       XXCP_trading_sets t
 where a.parent_trx_id        = pParent_Trx_id
   and a.transaction_table    = pTransaction_Table
   and a.source_assignment_id = pSource_Assignment_id
   and t.source_id            = vSource_id
   and a.trading_set          = t.trading_set
   and a.STEP_NUMBER          = pStep
 order by 1, 2 desc, 3 desc;

 -- Preview tables Only
Cursor P1(pPreview_id in number, pSource_Assignment_id in number, pParent_Trx_id in number, pTransaction_Table in varchar2, pStep in varchar2) is
 select  1 Seq
       , t.SEQUENCE
       , w.Attribute_id
       , w.Frozen
       , w.ATTRIBUTE1
       , w.ATTRIBUTE2
       , w.ATTRIBUTE3
       , w.ATTRIBUTE4
       , nvl(to_char(w.IC_UNIT_PRICE),w.ATTRIBUTE5) ATTRIBUTE5
       , w.ATTRIBUTE6
       , w.ATTRIBUTE7
       , w.ATTRIBUTE8
       , w.ATTRIBUTE9
       , w.ATTRIBUTE10
 from xxcp_pv_transaction_attrib_v w,
      XXCP_trading_sets t
 where w.preview_id           = Decode(pPreview_id,0,w.preview_id , pPreview_id)
   and w.parent_trx_id        = pParent_Trx_id
   and w.transaction_table    = pTransaction_Table
   and w.trading_set          = t.trading_set
   and t.source_id            = vSource_id
   and w.source_assignment_id = pSource_Assignment_id
   and w.STEP_NUMBER          = pStep
 order by 1, 2 desc, 3 desc;


 -- Source Assignments
 Cursor SrcAssign(pSource_Assignment_id in number) is
  Select source_id
  from XXCP_source_assignments
  where source_assignment_id = pSource_Assignment_id;

 -- User Profiles
 Cursor UsrProfile is
  Select nvl(p.PV_AS_IF_NEW,'N') AS_NEW
  from XXCP_user_profiles p
  where user_id = xxcp_global.User_id;

 Begin

    -- Get Source id into Global Memory
    If gLSV_Source_id = 0 then
     For Rec in SrcAssign(cSource_Assignment_id) Loop
      gLSV_Source_id := Rec.Source_id;
     End Loop;
    End If;

    vSource_id := gLSV_Source_id;

    If xxcp_global.Preview_on = 'N' then
     -- Live: Look at Transaction Attributes only
     -- Take the last transaction (Frozen) for the highest Trading Set Sequence
     For Rec in R1(cSource_Assignment_id, cParent_Trx_id, cTransaction_Table, cStep) Loop
       If Substr(Rec.Frozen,1,1) = 'Y' then
         vReturn :=
           Case cCustom_Attribute_Num
              when 1  then Rec.Attribute1
              when 2  then Rec.Attribute2
              when 3  then Rec.Attribute3
              when 4  then Rec.Attribute4
              when 5  then Rec.Attribute5
              when 6  then Rec.Attribute6
              when 7  then Rec.Attribute7
              when 8  then Rec.Attribute8
              when 9  then Rec.Attribute9
              when 10 then Rec.Attribute10
           End;
       End If;
       Exit; -- Only once!
     End Loop;


    Else
     -- Preview Mode
     -- Get the AS New Flag
     For Rec4 in UsrProfile Loop
       vAS_NEW := Rec4.AS_NEW;
     End Loop;
     xxcp_foundation.show('Preview Mode: As New = '||vAS_NEW);

     If vAS_NEW = 'Y' then
       -- AS NEW (Exclude Live Tables)
       For Rec in P1(xxcp_global.User_id, cSource_Assignment_id,cParent_Trx_id, cTransaction_Table, cStep) Loop
         If Substr(Rec.Frozen,1,1) = 'Y' then

           vReturn :=
           Case cCustom_Attribute_Num
              when 1  then Rec.Attribute1
              when 2  then Rec.Attribute2
              when 3  then Rec.Attribute3
              when 4  then Rec.Attribute4
              when 5  then Rec.Attribute5
              when 6  then Rec.Attribute6
              when 7  then Rec.Attribute7
              when 8  then Rec.Attribute8
              when 9  then Rec.Attribute9
              when 10 then Rec.Attribute10
           End;

         End If;
         Exit; -- Only once!
       End Loop;

     Else
       -- Include Live tables and Preview tables.
       For Rec in J1(xxcp_global.User_id, cSource_Assignment_id,cParent_Trx_id, cTransaction_Table, cStep) Loop
         If Substr(Rec.Frozen,1,1) = 'Y' then

           vReturn :=
           Case cCustom_Attribute_Num
              when 1  then Rec.Attribute1
              when 2  then Rec.Attribute2
              when 3  then Rec.Attribute3
              when 4  then Rec.Attribute4
              when 5  then Rec.Attribute5
              when 6  then Rec.Attribute6
              when 7  then Rec.Attribute7
              when 8  then Rec.Attribute8
              when 9  then Rec.Attribute9
              when 10 then Rec.Attribute10
           End;

         End If;
         Exit; -- Only once!
       End Loop;
     End If;
    End If;

   Return(vReturn);

 End LAST_STEP_VALUE;

-- ******************************************************************************
--                            LAST_STEP_VALUES
--                         --------------------
-- ******************************************************************************
 Function Last_Step_Values( cSource_Assignment_id    in Number,
                            cParent_Trx_id           in Number,
                            cTransaction_Table       in varchar2,
                            cStep                    in varchar2,
                            --
                            cIC_Unit_Price           out number,
                            cIC_Currency             out varchar2,
                            cAdjustment_Rate         out number,
                            cIC_Tax_Rate             out number,
                            cCustom_Attribute1       out varchar2,
                            cCustom_Attribute2       out varchar2,
                            cCustom_Attribute3       out varchar2,
                            cCustom_Attribute4       out varchar2,
                            cCustom_Attribute5       out varchar2,
                            cCustom_Attribute6       out varchar2,
                            cCustom_Attribute7       out varchar2,
                            cCustom_Attribute8       out varchar2,
                            cCustom_Attribute9       out varchar2,
                            cCustom_Attribute10      out varchar2
                            ) return varchar2 is

vReturn     pls_integer := -1;
vSource_id  XXCP_sys_sources.source_id%type;
vAS_NEW     XXCP_user_profiles.PV_AS_IF_NEW%type := 'N';


-- Live Run: Just use the base table.
Cursor R1(pSource_Assignment_id in number, pParent_Trx_id in number, pTransaction_Table in varchar2, pStep in varchar2  ) is
 select  t.SEQUENCE
       , a.Attribute_id
        , a.Frozen
       , a.IC_Currency
       , a.IC_Unit_Price
       , nvl(a.ADJUSTMENT_RATE,0) Adjustment_Rate
       , nvl(a.IC_TAX_RATE,0) IC_Tax_Rate
       , a.ATTRIBUTE1
       , a.ATTRIBUTE2
       , a.ATTRIBUTE3
       , a.ATTRIBUTE4
       , a.ATTRIBUTE5
       , a.ATTRIBUTE6
       , a.ATTRIBUTE7
       , a.ATTRIBUTE8
       , a.ATTRIBUTE9
       , a.ATTRIBUTE10
   from xxcp_transaction_attributes_v a,
        XXCP_trading_sets t
  where a.parent_trx_id        = pParent_Trx_id
    and a.transaction_table    = pTransaction_Table
     and t.source_id            = vSource_id
    and a.trading_set          = t.trading_set
     and a.source_assignment_id = pSource_Assignment_id
    and a.STEP_NUMBER          = pStep
  order by 1 desc, 2 desc;

-- Join live and preview tables
Cursor J1(pPreview_id in number, pSource_Assignment_id in number, pParent_Trx_id in number, pTransaction_Table in varchar2, pStep in varchar2) is
 select  1 Seq
       , t.SEQUENCE
       , w.Attribute_id
       , w.Frozen
       , w.IC_Currency
       , w.IC_Unit_Price
       , nvl(w.ADJUSTMENT_RATE,0) Adjustment_Rate
       , nvl(w.IC_TAX_RATE,0) IC_Tax_Rate
       , w.ATTRIBUTE1
       , w.ATTRIBUTE2
       , w.ATTRIBUTE3
       , w.ATTRIBUTE4
       , w.ATTRIBUTE5
       , w.ATTRIBUTE6
       , w.ATTRIBUTE7
       , w.ATTRIBUTE8
       , w.ATTRIBUTE9
       , w.ATTRIBUTE10
 from xxcp_pv_transaction_attrib_v w,
      XXCP_trading_sets t
 where w.preview_id           = Decode(pPreview_id,0,w.preview_id , pPreview_id)
   and w.parent_trx_id        = pParent_Trx_id
   and w.transaction_table    = pTransaction_Table
   and w.trading_set          = t.trading_set
   and t.source_id            = vSource_id
   and w.source_assignment_id = pSource_Assignment_id
   and w.STEP_NUMBER          = pStep
 UNION ALL
 select  2
       , t.SEQUENCE
       , a.Attribute_id
       , a.Frozen
       , a.IC_Currency
       , a.IC_Unit_Price
       , nvl(a.ADJUSTMENT_RATE,0) Adjustment_Rate
       , nvl(a.IC_TAX_RATE,0) IC_Tax_Rate
       , a.ATTRIBUTE1
       , a.ATTRIBUTE2
       , a.ATTRIBUTE3
       , a.ATTRIBUTE4
       , a.ATTRIBUTE5
       , a.ATTRIBUTE6
       , a.ATTRIBUTE7
       , a.ATTRIBUTE8
       , a.ATTRIBUTE9
       , a.ATTRIBUTE10
  from xxcp_transaction_attributes_v a,
       XXCP_trading_sets t
 where a.parent_trx_id        = pParent_Trx_id
   and a.transaction_table    = pTransaction_Table
   and a.source_assignment_id = pSource_Assignment_id
   and t.source_id            = vSource_id
   and a.trading_set          = t.trading_set
   and a.STEP_NUMBER          = pStep
 order by 1, 2 desc, 3 desc;

 -- Preview tables Only
Cursor P1(pPreview_id in number, pSource_Assignment_id in number, pParent_Trx_id in number, pTransaction_Table in varchar2, pStep in varchar2) is
 select  1 Seq
       , t.SEQUENCE
       , w.Attribute_id
       , w.Frozen
       , w.IC_Currency
       , w.IC_Unit_Price
       , nvl(w.ADJUSTMENT_RATE,0) Adjustment_Rate
       , nvl(w.IC_TAX_RATE,0) IC_Tax_Rate
       , w.ATTRIBUTE1
       , w.ATTRIBUTE2
       , w.ATTRIBUTE3
       , w.ATTRIBUTE4
       , w.ATTRIBUTE5
       , w.ATTRIBUTE6
       , w.ATTRIBUTE7
       , w.ATTRIBUTE8
       , w.ATTRIBUTE9
       , w.ATTRIBUTE10
 from xxcp_pv_transaction_attrib_v w,
      XXCP_trading_sets t
 where w.preview_id           = Decode(pPreview_id,0,w.preview_id , pPreview_id)
   and w.parent_trx_id        = pParent_Trx_id
   and w.transaction_table    = pTransaction_Table
   and w.trading_set          = t.trading_set
   and t.source_id            = vSource_id
   and w.source_assignment_id = pSource_Assignment_id
   and w.STEP_NUMBER          = pStep
 order by 1, 2 desc, 3 desc;


 -- Source Assignments
 Cursor SrcAssign(pSource_Assignment_id in number) is
  Select source_id
  from XXCP_source_assignments
  where source_assignment_id = pSource_Assignment_id;

 -- User Profiles
 Cursor UsrProfile is
  Select nvl(p.PV_AS_IF_NEW,'N') AS_NEW
  from XXCP_user_profiles p
  where user_id = xxcp_global.User_id;


 Begin

    -- Get Source id into Global Memory
    If gLSV_Source_id = 0 then
     For Rec in SrcAssign(cSource_Assignment_id) Loop
      gLSV_Source_id := Rec.Source_id;
     End Loop;
    End If;

    vSource_id := gLSV_Source_id;

    If xxcp_global.Preview_on = 'N' then
     -- Live: Look at Transaction Attributes only
     -- Take the last transaction (Frozen) for the highest Trading Set Sequence
     For Rec in R1(cSource_Assignment_id, cParent_Trx_id, cTransaction_Table, cStep) Loop
       If Substr(Rec.Frozen,1,1) = 'Y' then
         cIC_Unit_Price     := Rec.IC_Unit_Price;
         cIC_Currency       := Rec.IC_Currency;
         cAdjustment_Rate   := Rec.Adjustment_Rate;
         cIC_Tax_Rate       := Rec.IC_Tax_Rate;
         cCustom_Attribute1 := Rec.Attribute1;
         cCustom_Attribute2 := Rec.Attribute2;
         cCustom_Attribute3 := Rec.Attribute3;
         cCustom_Attribute4 := Rec.Attribute4;
         cCustom_Attribute5 := Rec.Attribute5;
         cCustom_Attribute6 := Rec.Attribute6;
         cCustom_Attribute7 := Rec.Attribute7;
         cCustom_Attribute8 := Rec.Attribute8;
         cCustom_Attribute9 := Rec.Attribute9;
         cCustom_Attribute10:= Rec.Attribute10;
         vReturn           := 0;
       Exit; -- Only once!
       End If;
     End Loop;


    Else
     -- Preview Mode
     -- Get the AS New Flag
     For Rec4 in UsrProfile Loop
       vAS_NEW := Rec4.AS_NEW;
     End Loop;
     xxcp_foundation.show('Preview Mode: As New = '||vAS_NEW);

     If vAS_NEW = 'Y' then
       -- AS NEW (Exclude Live Tables)
       For Rec in P1(xxcp_global.User_id, cSource_Assignment_id,cParent_Trx_id, cTransaction_Table, cStep) Loop
         If Substr(Rec.Frozen,1,1) = 'Y' then

         cIC_Unit_Price     := Rec.IC_Unit_Price;
         cIC_Currency       := Rec.IC_Currency;
         cAdjustment_Rate   := Rec.Adjustment_Rate;
         cIC_Tax_Rate       := Rec.IC_Tax_Rate;
         cCustom_Attribute1 := Rec.Attribute1;
         cCustom_Attribute2 := Rec.Attribute2;
         cCustom_Attribute3 := Rec.Attribute3;
         cCustom_Attribute4 := Rec.Attribute4;
         cCustom_Attribute5 := Rec.Attribute5;
         cCustom_Attribute6 := Rec.Attribute6;
         cCustom_Attribute7 := Rec.Attribute7;
         cCustom_Attribute8 := Rec.Attribute8;
         cCustom_Attribute9 := Rec.Attribute9;
         cCustom_Attribute10:= Rec.Attribute10;
         vReturn           := 0;

         Exit; -- Only once!
         End If;
       End Loop;

     Else
       -- Include Live tables and Preview tables.
       For Rec in J1(xxcp_global.User_id, cSource_Assignment_id,cParent_Trx_id, cTransaction_Table, cStep) Loop
         If Substr(Rec.Frozen,1,1) = 'Y' then

         cIC_Unit_Price     := Rec.IC_Unit_Price;
         cIC_Currency       := Rec.IC_Currency;
         cAdjustment_Rate   := Rec.Adjustment_Rate;
         cIC_Tax_Rate       := Rec.IC_Tax_Rate;
         cCustom_Attribute1 := Rec.Attribute1;
         cCustom_Attribute2 := Rec.Attribute2;
         cCustom_Attribute3 := Rec.Attribute3;
         cCustom_Attribute4 := Rec.Attribute4;
         cCustom_Attribute5 := Rec.Attribute5;
         cCustom_Attribute6 := Rec.Attribute6;
         cCustom_Attribute7 := Rec.Attribute7;
         cCustom_Attribute8 := Rec.Attribute8;
         cCustom_Attribute9 := Rec.Attribute9;
         cCustom_Attribute10:= Rec.Attribute10;
         vReturn           := 0;

         Exit; -- Only once!
         End If;
       End Loop;
     End If;
    End If;

   Return(vReturn);

 End LAST_STEP_VALUES;

 --
 --                              LongToCharFromTable
 --
 -- This function converts a Long data type in a table to varchar2 so it can be used with
 -- SQL criteria such as "LIKE"
 --
 FUNCTION LongToCharFromTable( cTable_Name in varchar2, cColumn_Name in varchar2, cRowid in rowid ) return varchar2 AS

   l_cursor    integer default dbms_sql.open_cursor;
   l_n         number;
   l_long_val  varchar2(4000);
   l_long_len  number;
   l_buflen    number := 4000;
   l_curpos    number := 0;

   Begin
       dbms_sql.parse( l_cursor,
                      'select ' || cColumn_Name || ' from ' || cTable_Name ||
                                                          ' where rowid = :x',
                       dbms_sql.native );
       dbms_sql.bind_variable( l_cursor, ':x', cRowid );

       dbms_sql.define_column_long(l_cursor, 1);
       l_n := dbms_sql.execute(l_cursor);

       if (dbms_sql.fetch_rows(l_cursor)>0)
       then
          dbms_sql.column_value_long(l_cursor, 1, l_buflen, l_curpos ,
                                     l_long_val, l_long_len );
      end if;
      dbms_sql.close_cursor(l_cursor);
      return l_long_val;
   End LongToCharFromTable;

 -- *******************************************************************************
 --
 -- CurrConv
 --
 -- *******************************************************************************
    FUNCTION CurrConv(      cAmount              in number
                         , cCurrency_Code       in varchar2
                         , cPrecision_Indicator in varchar2
                         , cExchange_Date       in Date     default null
                         , cExchange_Type       in varchar2 default null
                         , cTarget_Currency     in varchar2 default null
                         , cLabel               in varchar2 default null
   ) return number is

   vValue number;

   Begin

    vValue :=
     xxcp_te_base.Utils_Curr_Conv(cAmount        => cAmount
                         , cCurrency_Code       => cCurrency_Code
                         , cPrecision_Indicator => cPrecision_Indicator
                         , cExchange_Date       => cExchange_Date
                         , cExchange_Type       => cExchange_Type
                         , cTarget_Currency     => cTarget_Currency
                         , cLabel               => cLabel
                         );


   return(vValue);

   End CurrConv;

   Function CurrPrecision(cCurrency_Code          in Varchar2,
                          cUse_Extended_Precision in varchar2 default 'N',
                          cGlobalPrecision        in Number default 2
                         ) return number is

   Begin

      Return(xxcp_foundation.Get_CurrencyPrecision(cSourceCurrency  => cCurrency_Code,
                                                   cGlobalPrecision => cGlobalPrecision,
                                                   cUse_Extended_Precision => cUse_Extended_Precision ));

   End CurrPrecision;

   Function CurrRound    (cAmount                 in Number,
                          cCurrency_Code          in Varchar2,
                          cUse_Extended_Precision in varchar2 default 'N',
                          cGlobalPrecision        in Number default 2
                        ) return number is
   Begin

     Return(Round(cAmount, CurrPrecision( cCurrency_Code => cCurrency_Code, cUse_Extended_Precision => cUse_Extended_Precision,  cGlobalPrecision => cGlobalPrecision)));

   End CurrRound;

   FUNCTION CurrConvRate(  cCurrency_Code       in varchar2
                         , cPrecision_Indicator in varchar2
                         , cExchange_Date       in Date     default null
                         , cExchange_Type       in varchar2 default null
                         , cTarget_Currency     in varchar2 default null
   ) return number is

   vValue number;

   Begin

    vValue :=
     xxcp_te_base.Utils_Curr_ConvRate(
                           cCurrency_Code       => cCurrency_Code
                         , cPrecision_Indicator => cPrecision_Indicator
                         , cExchange_Date       => cExchange_Date
                         , cExchange_Type       => cExchange_Type
                         , cTarget_Currency     => cTarget_Currency );


     return(vValue);

   End CurrConvRate;


  FUNCTION CustLookupFetch(cCategory            in varchar2
                         , cCategory_Subset     in varchar2 default null
                         , cCode                in varchar2
                         , cTransaction_Date    in date
                         , cAttribute_Number    in number)
                         return Varchar2 is

    Cursor c1 (pAttribute_Number in number, pCategory  in varchar2, pCategory_Subset in varchar2, pCode in varchar2, pTransaction_Date in date) is
      select decode(pAttribute_number,
                     1, v.attribute1,
                     2, v.attribute2,
                     3, v.attribute3,
                     4, v.attribute4,
                     5, v.attribute5,
                     6, v.attribute6,
                     7, v.attribute7,
                     8, v.attribute8,
                     9, v.attribute9,
                     10, v.attribute10
                     ) Attribute_Value
      from XXCP_custom_lookups v
      where v.lookup_type        = pCategory
        and v.lookup_type_subset = pCategory_Subset
        and v.lookup_code        = pCode
        and pTransaction_Date between nvl(v.effective_from_date,'01-JAN-1900') and nvl(v.effective_to_date, '31-DEC-2999');

   vAttribute_Value XXCP_custom_lookups.attribute1%type;

  Begin

    For Rec in C1(pAttribute_number => cAttribute_Number,
                  pCategory         => cCategory,
                  pCategory_Subset  => cCategory_Subset,
                  pCode             => cCode,
                  pTransaction_Date => cTransaction_Date
                  )
    Loop

       vAttribute_Value := Rec.Attribute_Value;

    End Loop;

    return(vAttribute_Value);
  End CustLookupFetch;

  -- Custom Lookup Exists
  FUNCTION CustLookupExists(
                           cCategory            in varchar2
                         , cCategory_Subset     in varchar2 default null
                         , cCode                in varchar2
                         , cTransaction_Date    in date)
                         return Boolean is

    Cursor c1 (pCategory  in varchar2, pCategory_Subset in varchar2, pCode in varchar2, pTransaction_Date in date) is
      select 'X' Attribute_Value
      from XXCP_custom_lookups v
      where v.lookup_type                 = pCategory
        and nvl(v.lookup_type_subset,'~') = nvl(pCategory_Subset,'~')
        and v.lookup_code                 = pCode
        and pTransaction_Date between nvl(v.effective_from_date,'01-JAN-1900') and nvl(v.effective_to_date, '31-DEC-2999');

   vExists boolean := False;

  Begin

    For Rec in C1(pCategory         => cCategory,
                  pCategory_Subset  => cCategory_Subset,
                  pCode             => cCode,
                  pTransaction_Date => cTransaction_Date
                  )
    Loop

       vExists := True;
       Exit;

    End Loop;

    Return(vExists);
  End CustLookupExists;

  --  CustLookupFetchGroup
  Procedure CustLookupFetchGroup(
                           cCategory            in varchar2
                         , cCategory_Subset     in varchar2 default null
                         , cCode                in varchar2
                         , cTransaction_Date    in date
                         , cAttribute1           out varchar2
                         , cAttribute2           out varchar2
                         , cAttribute3           out varchar2
                         , cAttribute4           out varchar2
                         , cAttribute5           out varchar2
                         , cAttribute6           out varchar2
                         , cAttribute7           out varchar2
                         , cAttribute8           out varchar2
                         , cAttribute9           out varchar2
                         , cAttribute10          out varchar2
                         ) is

    Cursor c1 (pCategory  in varchar2, pCategory_Subset in varchar2, pCode in varchar2, pTransaction_Date in date) is
      select v.attribute1,
             v.attribute2,
             v.attribute3,
             v.attribute4,
             v.attribute5,
             v.attribute6,
             v.attribute7,
             v.attribute8,
             v.attribute9,
             v.attribute10
      from XXCP_custom_lookups v
      where v.lookup_type                 = pCategory
        and nvl(v.lookup_type_subset,'~') = nvl(pCategory_Subset,'~')
        and v.lookup_code                 = pCode
        and pTransaction_Date between nvl(v.effective_from_date,'01-JAN-1900') and nvl(v.effective_to_date, '31-DEC-2999');


  Begin

    For Rec in C1(pCategory         => cCategory,
                  pCategory_Subset  => cCategory_Subset,
                  pCode             => cCode,
                  pTransaction_Date => Trunc(cTransaction_Date)
                  )
    Loop

       cAttribute1 := Rec.Attribute1;
       cAttribute2 := Rec.Attribute2;
       cAttribute3 := Rec.Attribute3;
       cAttribute3 := Rec.Attribute3;
       cAttribute4 := Rec.Attribute4;
       cAttribute5 := Rec.Attribute5;
       cAttribute6 := Rec.Attribute6;
       cAttribute7 := Rec.Attribute7;
       cAttribute8 := Rec.Attribute8;
       cAttribute9 := Rec.Attribute9;
       cAttribute10 := Rec.Attribute10;

    End Loop;
  End CustLookupFetchGroup;

  FUNCTION CustLookupCodeFetch(cCategory        in varchar2
                             , cCategory_Subset     in varchar2 default null
                             , cTransaction_Date    in date)
                         return Varchar2 is

    Cursor c1 (pCategory  in varchar2, pCategory_Subset in varchar2, pTransaction_Date in date) is
      select Lookup_Code
      from XXCP_custom_lookups v
      where v.lookup_type                 = pCategory
        and nvl(v.lookup_type_subset,'~') = nvl(pCategory_Subset,'~')
        and pTransaction_Date between nvl(v.effective_from_date,'01-JAN-1900') and nvl(v.effective_to_date, '31-DEC-2999');

   vAttribute_Value XXCP_custom_lookups.attribute1%type;

  Begin

    For Rec in C1(pCategory         => cCategory,
                  pCategory_Subset  => cCategory_Subset,
                  pTransaction_Date => trunc(cTransaction_Date)
                  )
    Loop

       vAttribute_Value := Rec.Lookup_Code;
       Exit;

    End Loop;

    return(vAttribute_Value);
  End CustLookupCodeFetch;


 -- *******************************************************************************
 --
 -- Live_Assignment_Value
 --
 -- *******************************************************************************
 Function Live_Assignment_Value(cDynamic_Attribute_Name in varchar2 default null,cDynamic_Attribute_id in number default 0) return varchar2 is

   vDynamic_Attribute_id XXCP_dynamic_attributes.COLUMN_ID%type;
   vDynamic_Attribute    XXCP_dynamic_attributes.COLUMN_NAME%type;

   Cursor DT(pDynamic_Attribute_Name in varchar2) is
     select d.ID Dynamic_Attribute_id
     from xxcp_dynamic_attributes_v d
     where d.Attribute_name = pDynamic_Attribute_Name;

   vDynamic_Value varchar2(300);

  begin

    vDynamic_Attribute_id := cDynamic_Attribute_id;
    Begin

      If vDynamic_Attribute_id <= 0 and cDynamic_Attribute_Name is not null then
        For Rec in DT(cDynamic_Attribute_Name) loop
          vDynamic_Attribute_id := Rec.Dynamic_Attribute_id;
        End Loop;
      End If;

      If vDynamic_Attribute_id between 1001 and 1999 then
        vDynamic_Value := xxcp_wks.D1001_Array(vDynamic_Attribute_id-1000);
      ElsIf vDynamic_Attribute_id between 2001 and 2999 then
        vDynamic_Value := xxcp_wks.D1002_Array(vDynamic_Attribute_id-1000);
      End If;

      Exception when Others then null;
    End;

    Return(vDynamic_Value);

  end Live_Assignment_Value;

  Function FindAdjustmentRate( cRate_Set_Name      IN VARCHAR2,
                               cRate_Set_id        IN NUMBER Default 0,
                               cTransaction_Date   IN DATE,
                               cOwner              IN VARCHAR2,
                               cPartner            IN VARCHAR2  default null,
                               cQualifier1         IN VARCHAR2  default null,
                               cQualifier2         IN VARCHAR2  default null
                               ) Return Number is

    CURSOR adn(pSet_Name in varchar2) is
     select set_id
     from XXCP_table_set_ctl t
     where set_base_table = 'CP_ADJUSTMENT_RATES'
       and set_type = 'ADJUSTMENT RATES'
       and set_name = pSet_Name;

    CURSOR aj(pRate_Set_id IN NUMBER, pOwner IN VARCHAR2, pPartner IN VARCHAR2, pQualifier1 IN VARCHAR2, pQualifier2 IN VARCHAR2, pTransaction_Date IN DATE) IS
      SELECT Rate, Adjustment_Rate_id
        FROM XXCP_adjustment_rates r
       WHERE rate_set_id = pRate_Set_id
         AND NVL(owner, '~null~') = pOwner
         AND NVL(partner, '~null~') = pPartner
         AND NVL(ar_qualifier1, '~null~') = pQualifier1
         AND NVL(ar_qualifier2, '~null~') = pQualifier2
         AND pTransaction_Date BETWEEN
             NVL(r.Effective_From_Date, TO_DATE('01-JAN-1980')) AND
             NVL(r.Effective_To_Date, TO_DATE('31-DEC-2999'));

    vCounter     PLS_INTEGER := 0;
    vRate        NUMBER;
    vRate_Set_id NUMBER;

  BEGIN

   vRate_Set_id := cRate_Set_id;

    If cRate_Set_id = 0 then
      For Rec in adn(PSet_Name =>  cRate_Set_Name) Loop
         vRate_Set_id := Rec.Set_Id;
      End Loop;
    End If;

    If vRate_Set_Id > 0 then

      FOR AdRec IN Aj(vRate_Set_id,
                    NVL(cOwner, '~null~'),
                    NVL(cPartner, '~null~'),
                    NVL(cQualifier1, '~null~'),
                    NVL(cQualifier2, '~null~'),
                    TRUNC(cTransaction_Date)) LOOP
        vRate        := AdRec.Rate;
        Exit;
      END LOOP;
    End If;

    Return(vRate );

    end  FindAdjustmentRate;

  Procedure Report_Error_Details(cInternalErrorCode in Number,
                                 cErrorMessage      in varchar2,
                                 cLong_Message      in Long Default Null) is
  Begin

    xxcp_foundation.FndWriteError(cInternalErrorCode, cErrorMessage, cLong_Message);

  End Report_Error_Details;

  -- *******************************************************************************
  -- CustData_BestMatch_Trace
  --
  -- *******************************************************************************
  Function CustData_BestMatch_Trace(cCategory    in varchar2,
                                     cAttribute1  in varchar2,
                                     cAttribute2  in varchar2,
                                     cAttribute3  in varchar2,
                                     cAttribute4  in varchar2,
                                     cAttribute5  in varchar2,
                                     cAttribute6  in varchar2,
                                     cAttribute7  in varchar2,
                                     cAttribute8  in varchar2,
                                     cAttribute9  in varchar2,
                                     cAttribute10 in varchar2,
                                     cAttribute11 in varchar2,
                                     cAttribute12 in varchar2,
                                     cAttribute13 in varchar2,
                                     cAttribute14 in varchar2,
                                     cAttribute15 in varchar2,
                                     cAttribute16 in varchar2,
                                     cAttribute17 in varchar2,
                                     cAttribute18 in varchar2,
                                     cAttribute19 in varchar2,
                                     cAttribute20 in varchar2,
                                     cAttribute21 in varchar2,
                                     cAttribute22 in varchar2,
                                     cAttribute23 in varchar2,
                                     cAttribute24 in varchar2,
                                     cAttribute25 in varchar2,
                                     cAttribute26 in varchar2,
                                     cAttribute27 in varchar2,
                                     cAttribute28 in varchar2,
                                     cAttribute29 in varchar2,
                                     cAttribute30 in varchar2
                                     ) return varchar2 is

  Cursor CDN(pCategory in varchar2) is
    select c.attribute_id, lpad(c.attribute_id,3,'0')||':'||replace(c.prompt,chr(10),chr(32)) prompt
    from XXCP_cust_data_ctl w,
         XXCP_cust_data_ctl_att c
    where w.category_name = pCategory
      and w.category_id = c.category_id
      and c.prompt is not null
      order by c.attribute_id;

   vMsg   varchar2(4000) := 'Input Parameters'||Chr(10);

   Begin

      For Rec in CDN(pCategory => cCategory) Loop
        Case Rec.Attribute_Id
         When  1  Then  vMsg := vMsg ||rec.prompt||' <'||cAttribute1||'>'||chr(10);
         When  2  Then  vMsg := vMsg ||rec.prompt||' <'||cAttribute2||'>'||chr(10);
         When  3  Then  vMsg := vMsg ||rec.prompt||' <'||cAttribute3||'>'||chr(10);
         When  4  Then  vMsg := vMsg ||rec.prompt||' <'||cAttribute4||'>'||chr(10);
         When  5  Then  vMsg := vMsg ||rec.prompt||' <'||cAttribute5||'>'||chr(10);
         When  6  Then  vMsg := vMsg ||rec.prompt||' <'||cAttribute6||'>'||chr(10);
         When  7  Then  vMsg := vMsg ||rec.prompt||' <'||cAttribute7||'>'||chr(10);
         When  8  Then  vMsg := vMsg ||rec.prompt||' <'||cAttribute8||'>'||chr(10);
         When  9  Then  vMsg := vMsg ||rec.prompt||' <'||cAttribute9||'>'||chr(10);
         When 10  Then  vMsg := vMsg ||rec.prompt||' <'||cAttribute10||'>'||chr(10);
         When 11  Then  vMsg := vMsg ||rec.prompt||' <'||cAttribute11||'>'||chr(10);
         When 12  Then  vMsg := vMsg ||rec.prompt||' <'||cAttribute12||'>'||chr(10);
         When 13  Then  vMsg := vMsg ||rec.prompt||' <'||cAttribute13||'>'||chr(10);
         When 14  Then  vMsg := vMsg ||rec.prompt||' <'||cAttribute14||'>'||chr(10);
         When 15  Then  vMsg := vMsg ||rec.prompt||' <'||cAttribute15||'>'||chr(10);
         When 16  Then  vMsg := vMsg ||rec.prompt||' <'||cAttribute16||'>'||chr(10);
         When 17  Then  vMsg := vMsg ||rec.prompt||' <'||cAttribute17||'>'||chr(10);
         When 18  Then  vMsg := vMsg ||rec.prompt||' <'||cAttribute18||'>'||chr(10);
         When 19  Then  vMsg := vMsg ||rec.prompt||' <'||cAttribute19||'>'||chr(10);
         When 20  Then  vMsg := vMsg ||rec.prompt||' <'||cAttribute20||'>'||chr(10);
         When 21  Then  vMsg := vMsg ||rec.prompt||' <'||cAttribute21||'>'||chr(10);
         When 22  Then  vMsg := vMsg ||rec.prompt||' <'||cAttribute22||'>'||chr(10);
         When 23  Then  vMsg := vMsg ||rec.prompt||' <'||cAttribute23||'>'||chr(10);
         When 24  Then  vMsg := vMsg ||rec.prompt||' <'||cAttribute24||'>'||chr(10);
         When 25  Then  vMsg := vMsg ||rec.prompt||' <'||cAttribute25||'>'||chr(10);
         When 26  Then  vMsg := vMsg ||rec.prompt||' <'||cAttribute26||'>'||chr(10);
         When 27  Then  vMsg := vMsg ||rec.prompt||' <'||cAttribute27||'>'||chr(10);
         When 28  Then  vMsg := vMsg ||rec.prompt||' <'||cAttribute28||'>'||chr(10);
         When 29  Then  vMsg := vMsg ||rec.prompt||' <'||cAttribute29||'>'||chr(10);
         When 30  Then  vMsg := vMsg ||rec.prompt||' <'||cAttribute30||'>'||chr(10);
       Else
         Null;
       End Case;
      End Loop;

    Return(vMsg);

   End CustData_BestMatch_Trace;

  -- *******************************************************************************
  --
  -- CustData_BestMatch
  --
  -- *******************************************************************************
 Function CustomData_BestMatch(    cCategory              IN varchar2,
                                   cAttributeNumber       IN number   default 1,
                                   cTransaction_Date      IN date,
                                   cAttribute1        IN varchar2 default '~',
                                   cAttribute2        IN varchar2 default '~',
                                   cAttribute3        IN varchar2 default '~',
                                   cAttribute4        IN varchar2 default '~',
                                   cAttribute5        IN varchar2 default '~',
                                   cAttribute6        IN varchar2 default '~',
                                   cAttribute7        IN varchar2 default '~',
                                   cAttribute8        IN varchar2 default '~',
                                   cAttribute9        IN varchar2 default '~',
                                   cAttribute10       IN varchar2 default '~',
                                   cAttribute11       IN varchar2 default '~',
                                   cAttribute12       IN varchar2 default '~',
                                   cAttribute13       IN varchar2 default '~',
                                   cAttribute14       IN varchar2 default '~',
                                   cAttribute15       IN varchar2 default '~',
                                   cAttribute16       IN varchar2 default '~',
                                   cAttribute17       IN varchar2 default '~',
                                   cAttribute18       IN varchar2 default '~',
                                   cAttribute19       IN varchar2 default '~',
                                   cAttribute20       IN varchar2 default '~',
                                   cAttribute21       IN varchar2 default '~',
                                   cAttribute22       IN varchar2 default '~',
                                   cAttribute23       IN varchar2 default '~',
                                   cAttribute24       IN varchar2 default '~',
                                   cAttribute25       IN varchar2 default '~',
                                   cAttribute26       IN varchar2 default '~',
                                   cAttribute27       IN varchar2 default '~',
                                   cAttribute28       IN varchar2 default '~',
                                   cAttribute29       IN varchar2 default '~',
                                   cAttribute30       IN varchar2 default '~'
                                   ) return varchar2 is
 
    cursor c1(pCategory in varchar2, pTransaction_Date in Date) is
                 select
                        Decode(Attribute1,cAttribute1,1000,'*',1,0)+
                        Decode(Attribute2,cAttribute2,1000,'*',1,0)+
                        Decode(Attribute3,cAttribute3,1000,'*',1,0)+
                        Decode(Attribute4,cAttribute4,1000,'*',1,0)+
                        Decode(Attribute5,cAttribute5,1000,'*',1,0)+
                        Decode(Attribute6,cAttribute6,1000,'*',1,0)+
                        Decode(Attribute7,cAttribute7,1000,'*',1,0)+
                        Decode(Attribute8,cAttribute8,1000,'*',1,0)+
                        Decode(Attribute9,cAttribute9,1000,'*',1,0)+
                        Decode(Attribute10,cAttribute10,1000,'*',1,0)+
                        Decode(Attribute11,cAttribute11,1000,'*',1,0)+
                        Decode(Attribute12,cAttribute12,1000,'*',1,0)+
                        Decode(Attribute13,cAttribute13,1000,'*',1,0)+
                        Decode(Attribute14,cAttribute14,1000,'*',1,0)+
                        Decode(Attribute15,cAttribute15,1000,'*',1,0)+
                        Decode(Attribute16,cAttribute16,1000,'*',1,0)+
                        Decode(Attribute17,cAttribute17,1000,'*',1,0)+
                        Decode(Attribute18,cAttribute18,1000,'*',1,0)+
                        Decode(Attribute19,cAttribute19,1000,'*',1,0)+
                        Decode(Attribute20,cAttribute20,1000,'*',1,0)+
                        Decode(Attribute21,cAttribute21,1000,'*',1,0)+
                        Decode(Attribute22,cAttribute22,1000,'*',1,0)+
                        Decode(Attribute23,cAttribute23,1000,'*',1,0)+
                        Decode(Attribute24,cAttribute24,1000,'*',1,0)+
                        Decode(Attribute25,cAttribute25,1000,'*',1,0)+
                        Decode(Attribute26,cAttribute26,1000,'*',1,0)+
                        Decode(Attribute27,cAttribute27,1000,'*',1,0)+
                        Decode(Attribute28,cAttribute28,1000,'*',1,0)+
                        Decode(Attribute29,cAttribute29,1000,'*',1,0)+
                        Decode(Attribute30,cAttribute30,1000,'*',1,0) Total_Match
                       ,decode(cAttributeNumber,1,attribute1,
                                                2,attribute2,
                                                3,attribute3,
                                                4,attribute4,
                                                5,attribute5,
                                                6,attribute6,
                                                7,attribute7,
                                                8,attribute8,
                                                9,attribute9,
                                                10,attribute10,
                                                11,attribute11,
                                                12,attribute12,
                                                13,attribute13,
                                                14,attribute14,
                                                15,attribute15,
                                                16,attribute16,
                                                17,attribute17,
                                                18,attribute18,
                                                19,attribute19,
                                                20,attribute20,
                                                21,attribute21,
                                                22,attribute22,
                                                23,attribute23,
                                                24,attribute24,
                                                25,attribute25,
                                                26,attribute26,
                                                27,attribute27,
                                                28,attribute28,
                                                29,attribute29,
                                                30,attribute30) best_match_attribute
                        ,rowid Row_id
                        ,inl.Cust_Data_id
                        ,nvl(inl.trace_best_Match,'N') trace_best_Match
                 from
                  (select w.*, c.trace_best_match
                   from xxcp_custom_data_v w,
                        xxcp_cust_data_ctl c
                   where w.category = pCategory
                     and w.category_id = c.category_id
                     and  pTransaction_Date Between nvl(w.EFFECTIVE_FROM_DATE,to_date('01-JAN-2000')) and nvl(w.EFFECTIVE_TO_DATE,to_date('31-DEC-2999'))
                     and ((w.ATTRIBUTE1 = cAttribute1 or cAttribute1 = '~' or w.Attribute1 = '*')
                     and (w.Attribute2 = cAttribute2 or cAttribute2 = '~' or w.Attribute2 = '*')
                     and (w.Attribute3 = cAttribute3 or cAttribute3 = '~' or w.Attribute3 = '*')
                     and (w.Attribute4 = cAttribute4 or cAttribute4 = '~' or w.Attribute4 = '*')
                     and (w.Attribute5 = cAttribute5 or cAttribute5 = '~' or w.Attribute5 = '*')
                     and (w.Attribute6 = cAttribute6 or cAttribute6 = '~' or w.Attribute6 = '*')
                     and (w.Attribute7 = cAttribute7 or cAttribute7 = '~' or w.Attribute7 = '*')
                     and (w.Attribute8 = cAttribute8 or cAttribute8 = '~' or w.Attribute8 = '*')
                     and (w.Attribute9 = cAttribute9 or cAttribute9 = '~' or w.Attribute9 = '*')
                     and (w.Attribute10 = cAttribute10 or cAttribute10 = '~' or w.Attribute10 = '*')
                     and (w.Attribute11 = cAttribute11 or cAttribute11 = '~' or w.Attribute11 = '*')
                     and (w.Attribute12 = cAttribute12 or cAttribute12 = '~' or w.Attribute12 = '*')
                     and (w.Attribute13 = cAttribute13 or cAttribute13 = '~' or w.Attribute13 = '*')
                     and (w.Attribute14 = cAttribute14 or cAttribute14 = '~' or w.Attribute14 = '*')
                     and (w.Attribute15 = cAttribute15 or cAttribute15 = '~' or w.Attribute15 = '*')
                     and (w.Attribute16 = cAttribute16 or cAttribute16 = '~' or w.Attribute16 = '*')
                     and (w.Attribute17 = cAttribute17 or cAttribute17 = '~' or w.Attribute17 = '*')
                     and (w.Attribute18 = cAttribute18 or cAttribute18 = '~' or w.Attribute18 = '*')
                     and (w.Attribute19 = cAttribute19 or cAttribute19 = '~' or w.Attribute19 = '*')
                     and (w.Attribute20 = cAttribute20 or cAttribute20 = '~' or w.Attribute20 = '*')
                     and (w.Attribute21 = cAttribute21 or cAttribute21 = '~' or w.Attribute21 = '*')
                     and (w.Attribute22 = cAttribute22 or cAttribute22 = '~' or w.Attribute22 = '*')
                     and (w.Attribute23 = cAttribute23 or cAttribute23 = '~' or w.Attribute23 = '*')
                     and (w.Attribute24 = cAttribute24 or cAttribute24 = '~' or w.Attribute24 = '*')
                     and (w.Attribute25 = cAttribute25 or cAttribute25 = '~' or w.Attribute25 = '*')
                     and (w.Attribute26 = cAttribute26 or cAttribute26 = '~' or w.Attribute26 = '*')
                     and (w.Attribute27 = cAttribute27 or cAttribute27 = '~' or w.Attribute27 = '*')
                     and (w.Attribute28 = cAttribute28 or cAttribute28 = '~' or w.Attribute28 = '*')
                     and (w.Attribute29 = cAttribute29 or cAttribute29 = '~' or w.Attribute29 = '*')
                     and (w.Attribute30 = cAttribute30 or cAttribute30 = '~' or w.Attribute30 = '*'))
                   ) inl
                    order by 1 desc            -- If Tied order by Only Exact Match
                      ;

  vReturnAttribute   varchar2(250);
  vReturnTotalMatch  number := 0;
  vTransaction_Date  Date;
  vFoundTxt          varchar2(1) := 'N';
  vMsg               varchar2(4000);
  vFoundRowid        Rowid;
  vFoundCustDataId   Number;
  vRecCount          Number := 0;
  

  -- Reporting
  vMax_ManyMatches       Number(3) := 20;
  vTrace_Best_Match      varchar2(1) := 'N';
  vMultipleMatchesFound  varchar2(1) := 'N';  
  vRep_Selected          varchar2(20);
  vRep_Matches_Cnt       Number := 0;
  vRep_Matches           varchar2(1000);

  begin

    vTransaction_Date := trunc(nvl(cTransaction_Date,xxcp_global.SystemDate));
    vRecCount := 0;
    
    For Rec in c1(pCategory => cCategory, pTransaction_date => vTransaction_Date) Loop
      vRecCount := vRecCount + 1;
      vTrace_Best_Match := Rec.Trace_Best_Match;
      If vRecCount = 1 then
        vReturnTotalMatch := Rec.Total_Match;
        vReturnAttribute  := Rec.Best_Match_Attribute;
        vFoundTxt         := 'Y';
        vFoundRowid       := Rec.Row_id;
        vFoundCustDataId  := Rec.Cust_Data_id;

        vRep_Selected := to_char(Rec.Cust_Data_id);
      ElsIf vRecCount between 2 and  vMax_ManyMatches then
        vMultipleMatchesFound  := 'Y';
        vRep_Matches_Cnt  := vRecCount;      
        If vRecCount = 2 then
          vRep_Matches := to_char(Rec.Cust_Data_id);
        Else
          vRep_Matches := vRep_Matches ||','||to_char(Rec.Cust_Data_id);
        End If;
      End If;
      -- Exit if possible
      If  vTrace_Best_Match != 'Y' or (vRecCount >= vMax_ManyMatches) then
        Exit;
      End If;
    End Loop;


    If xxcp_global.Trace_on = 'Y' or  vTrace_Best_Match = 'Y' then

       vMsg :=  CustData_BestMatch_Trace(cCategory    => cCategory,
                                         cAttribute1  => cAttribute1,
                                         cAttribute2  => cAttribute2,
                                         cAttribute3  => cAttribute3,
                                         cAttribute4  => cAttribute4,
                                         cAttribute5  => cAttribute5,
                                         cAttribute6  => cAttribute6,
                                         cAttribute7  => cAttribute7,
                                         cAttribute8  => cAttribute8,
                                         cAttribute9  => cAttribute9,
                                         cAttribute10 => cAttribute10,
                                         cAttribute11 => cAttribute11,
                                         cAttribute12 => cAttribute12,
                                         cAttribute13 => cAttribute13,
                                         cAttribute14 => cAttribute14,
                                         cAttribute15 => cAttribute15,
                                         cAttribute16 => cAttribute16,
                                         cAttribute17 => cAttribute17,
                                         cAttribute18 => cAttribute18,
                                         cAttribute19 => cAttribute19,
                                         cAttribute20 => cAttribute20,
                                         cAttribute21 => cAttribute21,
                                         cAttribute22 => cAttribute22,
                                         cAttribute23 => cAttribute23,
                                         cAttribute24 => cAttribute24,
                                         cAttribute25 => cAttribute25,
                                         cAttribute26 => cAttribute26,
                                         cAttribute27 => cAttribute27,
                                         cAttribute28 => cAttribute28,
                                         cAttribute29 => cAttribute29,
                                         cAttribute30 => cAttribute30
                                        );

      vMsg := vMsg || chr(10)|| 'Return <'||vReturnAttribute||'>';

      If xxcp_global.Trace_on = 'Y' then
         xxcp_foundation.FndWriteError(100,'TRACE (BestMatch): Category <'||cCategory||'> Found <'||vFoundTxt||'> Trx Date <'||cTransaction_Date||'> Rowid <'||vFoundRowid||'>' , vMsg);
      End If;
                   
      If vTrace_Best_Match = 'Y' and (vMultipleMatchesFound = 'Y' and vRep_Matches_Cnt > 1) then
         Begin
            Insert into xxcp_bestmatch_report(
              source_assignment_id,
              category_name,
              transaction_table,
              transaction_type,
              transaction_date,
              transaction_ref,
              transaction_id,
              cust_data_Selected,
              matches_Count,
              other_cust_data_matches,
              calling_process_name,
              calling_parameters,
              Request_id,
              Function_id,
              creation_date)
              values(
                xxcp_global.gCommon(1).current_assignment_id,
                cCategory,
                xxcp_global.gCommon(1).current_Transaction_table,
                xxcp_global.gCommon(1).current_Transaction_type,
                xxcp_global.gCommon(1).current_Transaction_date,
                xxcp_global.gCommon(1).current_Transaction_Ref,
                xxcp_global.gCommon(1).current_Transaction_id,
                vRep_Selected,
                vRep_Matches_Cnt,
                vRep_Matches,
                xxcp_global.gCommon(1).current_process_name,
                vMsg,
                xxcp_global.gCOMMON(1).Current_Request_id,
                15,
                sysdate);
                
              Exception when others then Null; -- ensure that this does not break the calling program  
                
            End;
        End If;
    End If;

    Return(vReturnAttribute);

  End CustomData_BestMatch;

  -- *******************************************************************************
  --
  -- CustData_BestMatch External
  --
  -- *******************************************************************************
 Function CustomData_BestMatch_Ex( cCategory              IN varchar2,
                                   cAttributeNumber       IN number   default 1,
                                   cTransaction_Date      IN date,
                                   cRequest_id            IN number,
                                   cCalling_Program_Name  IN varchar2,
                                   cSource_Assignment_ID  IN Number,
                                   cTransaction_Table     IN varchar2,
                                   cTransaction_Type      IN varchar2,
                                   cTransaction_Ref       IN varchar2,
                                   cTransaction_id        IN Number,
                                   cAttribute1        IN varchar2 default '~',
                                   cAttribute2        IN varchar2 default '~',
                                   cAttribute3        IN varchar2 default '~',
                                   cAttribute4        IN varchar2 default '~',
                                   cAttribute5        IN varchar2 default '~',
                                   cAttribute6        IN varchar2 default '~',
                                   cAttribute7        IN varchar2 default '~',
                                   cAttribute8        IN varchar2 default '~',
                                   cAttribute9        IN varchar2 default '~',
                                   cAttribute10       IN varchar2 default '~',
                                   cAttribute11       IN varchar2 default '~',
                                   cAttribute12       IN varchar2 default '~',
                                   cAttribute13       IN varchar2 default '~',
                                   cAttribute14       IN varchar2 default '~',
                                   cAttribute15       IN varchar2 default '~',
                                   cAttribute16       IN varchar2 default '~',
                                   cAttribute17       IN varchar2 default '~',
                                   cAttribute18       IN varchar2 default '~',
                                   cAttribute19       IN varchar2 default '~',
                                   cAttribute20       IN varchar2 default '~',
                                   cAttribute21       IN varchar2 default '~',
                                   cAttribute22       IN varchar2 default '~',
                                   cAttribute23       IN varchar2 default '~',
                                   cAttribute24       IN varchar2 default '~',
                                   cAttribute25       IN varchar2 default '~',
                                   cAttribute26       IN varchar2 default '~',
                                   cAttribute27       IN varchar2 default '~',
                                   cAttribute28       IN varchar2 default '~',
                                   cAttribute29       IN varchar2 default '~',
                                   cAttribute30       IN varchar2 default '~'
                                   ) return varchar2 is
 
 
  vReturnAttribute   varchar2(250);

  begin

    xxcp_global.gCommon(1).current_assignment_id     := cSource_Assignment_id;
    xxcp_global.gCommon(1).current_Transaction_table := cTransaction_Table;
    xxcp_global.gCommon(1).current_Transaction_type  := cTransaction_Type;
    xxcp_global.gCommon(1).current_Transaction_date  := cTransaction_Date;
    xxcp_global.gCommon(1).current_Transaction_Ref   := cTransaction_Ref;
    xxcp_global.gCommon(1).current_Transaction_id    := cTransaction_id;
    xxcp_global.gCommon(1).current_process_name      := cCalling_Program_Name; 
    xxcp_global.gCOMMON(1).Current_Request_id        := cRequest_id;

    vReturnAttribute := 
                   CustomData_BestMatch( cCategory         => cCategory,
                                         cAttributeNumber  => cAttributeNumber,
                                         cTransaction_Date => cTransaction_Date,
                                         cAttribute1  => cAttribute1,
                                         cAttribute2  => cAttribute2,
                                         cAttribute3  => cAttribute3,
                                         cAttribute4  => cAttribute4,
                                         cAttribute5  => cAttribute5,
                                         cAttribute6  => cAttribute6,
                                         cAttribute7  => cAttribute7,
                                         cAttribute8  => cAttribute8,
                                         cAttribute9  => cAttribute9,
                                         cAttribute10 => cAttribute10,
                                         cAttribute11 => cAttribute11,
                                         cAttribute12 => cAttribute12,
                                         cAttribute13 => cAttribute13,
                                         cAttribute14 => cAttribute14,
                                         cAttribute15 => cAttribute15,
                                         cAttribute16 => cAttribute16,
                                         cAttribute17 => cAttribute17,
                                         cAttribute18 => cAttribute18,
                                         cAttribute19 => cAttribute19,
                                         cAttribute20 => cAttribute20,
                                         cAttribute21 => cAttribute21,
                                         cAttribute22 => cAttribute22,
                                         cAttribute23 => cAttribute23,
                                         cAttribute24 => cAttribute24,
                                         cAttribute25 => cAttribute25,
                                         cAttribute26 => cAttribute26,
                                         cAttribute27 => cAttribute27,
                                         cAttribute28 => cAttribute28,
                                         cAttribute29 => cAttribute29,
                                         cAttribute30 => cAttribute30
                                        );

    Return(vReturnAttribute);

  End CustomData_BestMatch_Ex;


  -- *******************************************************************************
  --
  -- CustData_BestMatch2 (Multiple Records)
  --
  -- *******************************************************************************
  Function CustomData_BestMatch2 
                                 ( cCategory              IN varchar2,
                                   cTransaction_Date      IN date,
                                   cAttribute1       IN varchar2 default '~',
                                   cAttribute2       IN varchar2 default '~',
                                   cAttribute3       IN varchar2 default '~',
                                   cAttribute4       IN varchar2 default '~',
                                   cAttribute5       IN varchar2 default '~',
                                   cAttribute6       IN varchar2 default '~',
                                   cAttribute7       IN varchar2 default '~',
                                   cAttribute8       IN varchar2 default '~',
                                   cAttribute9       IN varchar2 default '~',
                                   cAttribute10      IN varchar2 default '~',
                                   cAttribute11      IN varchar2 default '~',
                                   cAttribute12      IN varchar2 default '~',
                                   cAttribute13      IN varchar2 default '~',
                                   cAttribute14      IN varchar2 default '~',
                                   cAttribute15      IN varchar2 default '~',
                                   cAttribute16      IN varchar2 default '~',
                                   cAttribute17      IN varchar2 default '~',
                                   cAttribute18      IN varchar2 default '~',
                                   cAttribute19      IN varchar2 default '~',
                                   cAttribute20      IN varchar2 default '~',
                                   cAttribute21      IN varchar2 default '~',
                                   cAttribute22      IN varchar2 default '~',
                                   cAttribute23      IN varchar2 default '~',
                                   cAttribute24      IN varchar2 default '~',
                                   cAttribute25      IN varchar2 default '~',
                                   cAttribute26      IN varchar2 default '~',
                                   cAttribute27      IN varchar2 default '~',
                                   cAttribute28      IN varchar2 default '~',
                                   cAttribute29      IN varchar2 default '~',
                                   cAttribute30      IN varchar2 default '~',
                                   cBestMatch_Record OUT gBestMatchRec%type
                                   )  Return Boolean is

    cursor c1(pTransaction_Date in Date) is
       Select
                        Decode(Attribute1,cAttribute1,1000,'*',1,0)+
                        Decode(Attribute2,cAttribute2,1000,'*',1,0)+
                        Decode(Attribute3,cAttribute3,1000,'*',1,0)+
                        Decode(Attribute4,cAttribute4,1000,'*',1,0)+
                        Decode(Attribute5,cAttribute5,1000,'*',1,0)+
                        Decode(Attribute6,cAttribute6,1000,'*',1,0)+
                        Decode(Attribute7,cAttribute7,1000,'*',1,0)+
                        Decode(Attribute8,cAttribute8,1000,'*',1,0)+
                        Decode(Attribute9,cAttribute9,1000,'*',1,0)+
                        Decode(Attribute10,cAttribute10,1000,'*',1,0)+
                        Decode(Attribute11,cAttribute11,1000,'*',1,0)+
                        Decode(Attribute12,cAttribute12,1000,'*',1,0)+
                        Decode(Attribute13,cAttribute13,1000,'*',1,0)+
                        Decode(Attribute14,cAttribute14,1000,'*',1,0)+
                        Decode(Attribute15,cAttribute15,1000,'*',1,0)+
                        Decode(Attribute16,cAttribute16,1000,'*',1,0)+
                        Decode(Attribute17,cAttribute17,1000,'*',1,0)+
                        Decode(Attribute18,cAttribute18,1000,'*',1,0)+
                        Decode(Attribute19,cAttribute19,1000,'*',1,0)+
                        Decode(Attribute20,cAttribute20,1000,'*',1,0)+
                        Decode(Attribute21,cAttribute21,1000,'*',1,0)+
                        Decode(Attribute22,cAttribute22,1000,'*',1,0)+
                        Decode(Attribute23,cAttribute23,1000,'*',1,0)+
                        Decode(Attribute24,cAttribute24,1000,'*',1,0)+
                        Decode(Attribute25,cAttribute25,1000,'*',1,0)+
                        Decode(Attribute26,cAttribute26,1000,'*',1,0)+
                        Decode(Attribute27,cAttribute27,1000,'*',1,0)+
                        Decode(Attribute28,cAttribute28,1000,'*',1,0)+
                        Decode(Attribute29,cAttribute29,1000,'*',1,0)+
                        Decode(Attribute30,cAttribute30,1000,'*',1,0) Total_Match
                        ,
                        attribute1,
                        attribute2,
                        attribute3,
                        attribute4,
                        attribute5,
                        attribute6,
                        attribute7,
                        attribute8,
                        attribute9,
                        attribute10,
                        attribute11,
                        attribute12,
                        attribute13,
                        attribute14,
                        attribute15,
                        attribute16,
                        attribute17,
                        attribute18,
                        attribute19,
                        attribute20,
                        attribute21,
                        attribute22,
                        attribute23,
                        attribute24,
                        attribute25,
                        attribute26,
                        attribute27,
                        attribute28,
                        attribute29,
                        attribute30,
                        rowid Row_id
                        ,inl.Cust_Data_id
                        ,nvl(inl.trace_best_Match,'N') trace_best_Match
                 from
                  (select
                      w.*, c.trace_best_match
                    from xxcp_custom_data_v w,
                         xxcp_cust_data_ctl c
                   where w.category = cCategory
                     and w.category_id = c.category_id
                     and pTransaction_Date Between nvl(w.EFFECTIVE_FROM_DATE,to_date('01-JAN-2000')) and nvl(w.EFFECTIVE_TO_DATE,to_date('31-DEC-2999'))
                     and ((w.ATTRIBUTE1 = cAttribute1 or cAttribute1 = '~' or w.Attribute1 = '*')
                     and (w.Attribute2 = cAttribute2 or cAttribute2 = '~' or w.Attribute2 = '*')
                     and (w.Attribute3 = cAttribute3 or cAttribute3 = '~' or w.Attribute3 = '*')
                     and (w.Attribute4 = cAttribute4 or cAttribute4 = '~' or w.Attribute4 = '*')
                     and (w.Attribute5 = cAttribute5 or cAttribute5 = '~' or w.Attribute5 = '*')
                     and (w.Attribute6 = cAttribute6 or cAttribute6 = '~' or w.Attribute6 = '*')
                     and (w.Attribute7 = cAttribute7 or cAttribute7 = '~' or w.Attribute7 = '*')
                     and (w.Attribute8 = cAttribute8 or cAttribute8 = '~' or w.Attribute8 = '*')
                     and (w.Attribute9 = cAttribute9 or cAttribute9 = '~' or w.Attribute9 = '*')
                     and (w.Attribute10 = cAttribute10 or cAttribute10 = '~' or w.Attribute10 = '*')
                     and (w.Attribute11 = cAttribute11 or cAttribute11 = '~' or w.Attribute11 = '*')
                     and (w.Attribute12 = cAttribute12 or cAttribute12 = '~' or w.Attribute12 = '*')
                     and (w.Attribute13 = cAttribute13 or cAttribute13 = '~' or w.Attribute13 = '*')
                     and (w.Attribute14 = cAttribute14 or cAttribute14 = '~' or w.Attribute14 = '*')
                     and (w.Attribute15 = cAttribute15 or cAttribute15 = '~' or w.Attribute15 = '*')
                     and (w.Attribute16 = cAttribute16 or cAttribute16 = '~' or w.Attribute16 = '*')
                     and (w.Attribute17 = cAttribute17 or cAttribute17 = '~' or w.Attribute17 = '*')
                     and (w.Attribute18 = cAttribute18 or cAttribute18 = '~' or w.Attribute18 = '*')
                     and (w.Attribute19 = cAttribute19 or cAttribute19 = '~' or w.Attribute19 = '*')
                     and (w.Attribute20 = cAttribute20 or cAttribute20 = '~' or w.Attribute20 = '*')
                     and (w.Attribute21 = cAttribute21 or cAttribute21 = '~' or w.Attribute21 = '*')
                     and (w.Attribute22 = cAttribute22 or cAttribute22 = '~' or w.Attribute22 = '*')
                     and (w.Attribute23 = cAttribute23 or cAttribute23 = '~' or w.Attribute23 = '*')
                     and (w.Attribute24 = cAttribute24 or cAttribute24 = '~' or w.Attribute24 = '*')
                     and (w.Attribute25 = cAttribute25 or cAttribute25 = '~' or w.Attribute25 = '*')
                     and (w.Attribute26 = cAttribute26 or cAttribute26 = '~' or w.Attribute26 = '*')
                     and (w.Attribute27 = cAttribute27 or cAttribute27 = '~' or w.Attribute27 = '*')
                     and (w.Attribute28 = cAttribute28 or cAttribute28 = '~' or w.Attribute28 = '*')
                     and (w.Attribute29 = cAttribute29 or cAttribute29 = '~' or w.Attribute29 = '*')
                     and (w.Attribute30 = cAttribute30 or cAttribute30 = '~' or w.Attribute30 = '*'))
                   ) inl
                order by 1 desc            -- If Tied order by Only Exact Match
                     ;

  vTransaction_Date Date;
  vFound            boolean := FALSE;
  vFoundTxt         varchar2(1) := 'N';
  vMsg              varchar2(4000);

  vFoundRowid        Rowid;
  vFoundCustDataId   Number;
  vRecCount          Number := 0;
  
  -- Reporting
  vMax_ManyMatches       Number(3) := 20;
  vTrace_Best_Match      varchar2(1) := 'N';
  vMultipleMatchesFound  varchar2(1) := 'N';  
  vRep_Selected          varchar2(20);
  vRep_Matches_Cnt       Number := 0;
  vRep_Matches           varchar2(1000);
  
  vReturnTotalMatch      number := 0;
  
  begin

    vTransaction_Date := trunc(nvl(cTransaction_Date,xxcp_global.SystemDate));
    cBestMatch_Record.Attribute1 := Null;

    For Rec in C1(pTransaction_date => vTransaction_Date) Loop

      vRecCount := vRecCount + 1;
      vTrace_Best_Match := Rec.Trace_Best_Match;

      If vRecCount = 1 then
        cBestMatch_Record.Attribute1  := Rec.Attribute1;
        cBestMatch_Record.Attribute2  := Rec.Attribute2;
        cBestMatch_Record.Attribute3  := Rec.Attribute3;
        cBestMatch_Record.Attribute4  := Rec.Attribute4;
        cBestMatch_Record.Attribute5  := Rec.Attribute5;
        cBestMatch_Record.Attribute6  := Rec.Attribute6;
        cBestMatch_Record.Attribute7  := Rec.Attribute7;
        cBestMatch_Record.Attribute8  := Rec.Attribute8;
        cBestMatch_Record.Attribute9  := Rec.Attribute9;
        cBestMatch_Record.Attribute10 := Rec.Attribute10;
        cBestMatch_Record.Attribute11 := Rec.Attribute11;
        cBestMatch_Record.Attribute12 := Rec.Attribute12;
        cBestMatch_Record.Attribute13 := Rec.Attribute13;
        cBestMatch_Record.Attribute14 := Rec.Attribute14;
        cBestMatch_Record.Attribute15 := Rec.Attribute15;
        cBestMatch_Record.Attribute16 := Rec.Attribute16;
        cBestMatch_Record.Attribute17 := Rec.Attribute17;
        cBestMatch_Record.Attribute18 := Rec.Attribute18;
        cBestMatch_Record.Attribute19 := Rec.Attribute19;
        cBestMatch_Record.Attribute20 := Rec.Attribute20;
        cBestMatch_Record.Attribute21 := Rec.Attribute21;
        cBestMatch_Record.Attribute22 := Rec.Attribute22;
        cBestMatch_Record.Attribute23 := Rec.Attribute23;
        cBestMatch_Record.Attribute24 := Rec.Attribute24;
        cBestMatch_Record.Attribute25 := Rec.Attribute25;
        cBestMatch_Record.Attribute26 := Rec.Attribute26;
        cBestMatch_Record.Attribute27 := Rec.Attribute27;
        cBestMatch_Record.Attribute28 := Rec.Attribute28;
        cBestMatch_Record.Attribute29 := Rec.Attribute29;
        cBestMatch_Record.Attribute30 := Rec.Attribute30;
        vFound            := TRUE;
        vReturnTotalMatch := Rec.Total_Match;
        vFoundTxt         := 'Y';
        vFoundRowid       := Rec.Row_id;
        vFoundCustDataId  := Rec.Cust_Data_id;

        vRep_Selected := to_char(Rec.Cust_Data_id);
      ElsIf vRecCount between 2 and  vMax_ManyMatches then
        vMultipleMatchesFound  := 'Y';
        vRep_Matches_Cnt  := vRecCount;      
        If vRecCount = 2 then
          vRep_Matches := to_char(Rec.Cust_Data_id);
        Else
          vRep_Matches := vRep_Matches ||','||to_char(Rec.Cust_Data_id);
        End If;
      End If;
      -- Exit if possible
      If  vTrace_Best_Match != 'Y' or (vRecCount >= vMax_ManyMatches) then
        Exit;
      End If;
        
    End Loop;

    If xxcp_global.Trace_on = 'Y' or vTrace_Best_Match = 'Y' then

       vMsg :=  CustData_BestMatch_Trace(cCategory    => cCategory,
                                         cAttribute1  => cAttribute1,
                                         cAttribute2  => cAttribute2,
                                         cAttribute3  => cAttribute3,
                                         cAttribute4  => cAttribute4,
                                         cAttribute5  => cAttribute5,
                                         cAttribute6  => cAttribute6,
                                         cAttribute7  => cAttribute7,
                                         cAttribute8  => cAttribute8,
                                         cAttribute9  => cAttribute9,
                                         cAttribute10 => cAttribute10,
                                         cAttribute11 => cAttribute11,
                                         cAttribute12 => cAttribute12,
                                         cAttribute13 => cAttribute13,
                                         cAttribute14 => cAttribute14,
                                         cAttribute15 => cAttribute15,
                                         cAttribute16 => cAttribute16,
                                         cAttribute17 => cAttribute17,
                                         cAttribute18 => cAttribute18,
                                         cAttribute19 => cAttribute19,
                                         cAttribute20 => cAttribute20,
                                         cAttribute21 => cAttribute21,
                                         cAttribute22 => cAttribute22,
                                         cAttribute23 => cAttribute23,
                                         cAttribute24 => cAttribute24,
                                         cAttribute25 => cAttribute25,
                                         cAttribute26 => cAttribute26,
                                         cAttribute27 => cAttribute27,
                                         cAttribute28 => cAttribute28,
                                         cAttribute29 => cAttribute29,
                                         cAttribute30 => cAttribute30
                                        );

      If xxcp_global.Trace_On = 'Y' then
        xxcp_foundation.FndWriteError(100,'TRACE (BestMatch2): Category <'||cCategory||'> Found <'||vFoundTxt||'> Trx Date <'||cTransaction_Date||'> Rowid <'||vFoundRowid||'>', vMsg);
      End If;

      If vTrace_Best_Match = 'Y' and (vMultipleMatchesFound = 'Y' and vRep_Matches_Cnt > 1) then
      
         Begin
            Insert into xxcp_bestmatch_report(
              source_assignment_id,
              category_name,
              transaction_table,
              transaction_type,
              transaction_date,
              transaction_ref,
              transaction_id,
              cust_data_Selected,
              matches_Count,
              other_cust_data_matches,
              calling_process_name,
              calling_parameters,
              Request_id,
              Function_id,
              creation_date)
              values(
                xxcp_global.gCommon(1).current_assignment_id,
                cCategory,
                xxcp_global.gCommon(1).current_Transaction_table,
                xxcp_global.gCommon(1).current_Transaction_type,
                xxcp_global.gCommon(1).current_Transaction_date,
                xxcp_global.gCommon(1).current_Transaction_Ref,
                xxcp_global.gCommon(1).current_Transaction_id,
                vRep_Selected,
                vRep_Matches_Cnt,
                vRep_Matches,
                xxcp_global.gCommon(1).current_process_name,
                vMsg,
                xxcp_global.gCOMMON(1).Current_Request_id,
                16,
                sysdate);
                
              Exception when others then Null; -- ensure that this does not break the calling program  
                
            End;
        End If;

    End If;

    Return(vFound);

  end CustomData_BestMatch2;

  -- *******************************************************************************
  --
  -- CustData_BestMatch2 (Multiple Records) External Call
  --
  -- *******************************************************************************
  Function CustomData_BestMatch2_Ex 
                                 ( cCategory              IN varchar2,
                                   cTransaction_Date      IN date,
                                   cRequest_id            IN number,
                                   cCalling_Program_Name  IN varchar2,
                                   cSource_Assignment_ID  IN Number,
                                   cTransaction_Table     IN varchar2,
                                   cTransaction_Type      IN varchar2,
                                   cTransaction_Ref       IN varchar2,
                                   cTransaction_id        IN Number,
                                   cAttribute1       IN varchar2 default '~',
                                   cAttribute2       IN varchar2 default '~',
                                   cAttribute3       IN varchar2 default '~',
                                   cAttribute4       IN varchar2 default '~',
                                   cAttribute5       IN varchar2 default '~',
                                   cAttribute6       IN varchar2 default '~',
                                   cAttribute7       IN varchar2 default '~',
                                   cAttribute8       IN varchar2 default '~',
                                   cAttribute9       IN varchar2 default '~',
                                   cAttribute10      IN varchar2 default '~',
                                   cAttribute11      IN varchar2 default '~',
                                   cAttribute12      IN varchar2 default '~',
                                   cAttribute13      IN varchar2 default '~',
                                   cAttribute14      IN varchar2 default '~',
                                   cAttribute15      IN varchar2 default '~',
                                   cAttribute16      IN varchar2 default '~',
                                   cAttribute17      IN varchar2 default '~',
                                   cAttribute18      IN varchar2 default '~',
                                   cAttribute19      IN varchar2 default '~',
                                   cAttribute20      IN varchar2 default '~',
                                   cAttribute21      IN varchar2 default '~',
                                   cAttribute22      IN varchar2 default '~',
                                   cAttribute23      IN varchar2 default '~',
                                   cAttribute24      IN varchar2 default '~',
                                   cAttribute25      IN varchar2 default '~',
                                   cAttribute26      IN varchar2 default '~',
                                   cAttribute27      IN varchar2 default '~',
                                   cAttribute28      IN varchar2 default '~',
                                   cAttribute29      IN varchar2 default '~',
                                   cAttribute30      IN varchar2 default '~',
                                   cBestMatch_Record OUT gBestMatchRec%type
                                   )  Return Boolean is


  vFound            boolean := FALSE;
  
  begin

    xxcp_global.gCommon(1).current_assignment_id     := cSource_Assignment_id;
    xxcp_global.gCommon(1).current_Transaction_table := cTransaction_Table;
    xxcp_global.gCommon(1).current_Transaction_type  := cTransaction_Type;
    xxcp_global.gCommon(1).current_Transaction_date  := cTransaction_Date;
    xxcp_global.gCommon(1).current_Transaction_Ref   := cTransaction_Ref;
    xxcp_global.gCommon(1).current_Transaction_id    := cTransaction_id;
    xxcp_global.gCommon(1).current_process_name      := cCalling_Program_Name; 
    xxcp_global.gCOMMON(1).Current_Request_id        := cRequest_id;

    vFound :=     CustomData_BestMatch2( cCategory         => cCategory,
                                         cTransaction_Date => cTransaction_Date,
                                         cAttribute1  => cAttribute1,
                                         cAttribute2  => cAttribute2,
                                         cAttribute3  => cAttribute3,
                                         cAttribute4  => cAttribute4,
                                         cAttribute5  => cAttribute5,
                                         cAttribute6  => cAttribute6,
                                         cAttribute7  => cAttribute7,
                                         cAttribute8  => cAttribute8,
                                         cAttribute9  => cAttribute9,
                                         cAttribute10 => cAttribute10,
                                         cAttribute11 => cAttribute11,
                                         cAttribute12 => cAttribute12,
                                         cAttribute13 => cAttribute13,
                                         cAttribute14 => cAttribute14,
                                         cAttribute15 => cAttribute15,
                                         cAttribute16 => cAttribute16,
                                         cAttribute17 => cAttribute17,
                                         cAttribute18 => cAttribute18,
                                         cAttribute19 => cAttribute19,
                                         cAttribute20 => cAttribute20,
                                         cAttribute21 => cAttribute21,
                                         cAttribute22 => cAttribute22,
                                         cAttribute23 => cAttribute23,
                                         cAttribute24 => cAttribute24,
                                         cAttribute25 => cAttribute25,
                                         cAttribute26 => cAttribute26,
                                         cAttribute27 => cAttribute27,
                                         cAttribute28 => cAttribute28,
                                         cAttribute29 => cAttribute29,
                                         cAttribute30 => cAttribute30,
                                         cBestMatch_Record => cBestMatch_Record
                                        );


    Return(vFound);

  end CustomData_BestMatch2_Ex;



  -- *******************************************************************************
  --
  -- CustData_Weighting
  --
  -- *******************************************************************************
  Function CustomData_Weighting   (cCategory         IN varchar2,
                                   cAttributeNumber  IN number   default 1,
                                   cTransaction_Date IN date,
                                   cAttribute1       IN varchar2 default '~',
                                   cAttribute2       IN varchar2 default '~',
                                   cAttribute3       IN varchar2 default '~',
                                   cAttribute4       IN varchar2 default '~',
                                   cAttribute5       IN varchar2 default '~',
                                   cAttribute6       IN varchar2 default '~',
                                   cAttribute7       IN varchar2 default '~',
                                   cAttribute8       IN varchar2 default '~',
                                   cAttribute9       IN varchar2 default '~',
                                   cAttribute10      IN varchar2 default '~',
                                   cAttribute11      IN varchar2 default '~',
                                   cAttribute12      IN varchar2 default '~',
                                   cAttribute13      IN varchar2 default '~',
                                   cAttribute14      IN varchar2 default '~',
                                   cAttribute15      IN varchar2 default '~',
                                   cAttribute16      IN varchar2 default '~',
                                   cAttribute17      IN varchar2 default '~',
                                   cAttribute18      IN varchar2 default '~',
                                   cAttribute19      IN varchar2 default '~',
                                   cAttribute20      IN varchar2 default '~',
                                   cAttribute21      IN varchar2 default '~',
                                   cAttribute22      IN varchar2 default '~',
                                   cAttribute23      IN varchar2 default '~',
                                   cAttribute24      IN varchar2 default '~',
                                   cAttribute25      IN varchar2 default '~',
                                   cAttribute26      IN varchar2 default '~',
                                   cAttribute27      IN varchar2 default '~',
                                   cAttribute28      IN varchar2 default '~',
                                   cAttribute29      IN varchar2 default '~',
                                   cAttribute30      IN varchar2 default '~'
                                   ) return varchar2 is

   vStop_Matching    number(3);

    cursor c1(pCategory in varchar2, pTransaction_Date in Date) is
                 select
                        Decode(Attribute1,cAttribute1,1000,'*',1,0)+
                        Decode(Attribute2,cAttribute2,1000,'*',1,0)+
                        Decode(Attribute3,cAttribute3,1000,'*',1,0)+
                        Decode(Attribute4,cAttribute4,1000,'*',1,0)+
                        Decode(Attribute5,cAttribute5,1000,'*',1,0)+
                        Decode(Attribute6,cAttribute6,1000,'*',1,0)+
                        Decode(Attribute7,cAttribute7,1000,'*',1,0)+
                        Decode(Attribute8,cAttribute8,1000,'*',1,0)+
                        Decode(Attribute9,cAttribute9,1000,'*',1,0)+
                        Decode(Attribute10,cAttribute10,1000,'*',1,0)+
                        Decode(Attribute11,cAttribute11,1000,'*',1,0)+
                        Decode(Attribute12,cAttribute12,1000,'*',1,0)+
                        Decode(Attribute13,cAttribute13,1000,'*',1,0)+
                        Decode(Attribute14,cAttribute14,1000,'*',1,0)+
                        Decode(Attribute15,cAttribute15,1000,'*',1,0)+
                        Decode(Attribute16,cAttribute16,1000,'*',1,0)+
                        Decode(Attribute17,cAttribute17,1000,'*',1,0)+
                        Decode(Attribute18,cAttribute18,1000,'*',1,0)+
                        Decode(Attribute19,cAttribute19,1000,'*',1,0)+
                        Decode(Attribute20,cAttribute20,1000,'*',1,0)+
                        Decode(Attribute21,cAttribute21,1000,'*',1,0)+
                        Decode(Attribute22,cAttribute22,1000,'*',1,0)+
                        Decode(Attribute23,cAttribute23,1000,'*',1,0)+
                        Decode(Attribute24,cAttribute24,1000,'*',1,0)+
                        Decode(Attribute25,cAttribute25,1000,'*',1,0)+
                        Decode(Attribute26,cAttribute26,1000,'*',1,0)+
                        Decode(Attribute27,cAttribute27,1000,'*',1,0)+
                        Decode(Attribute28,cAttribute28,1000,'*',1,0)+
                        Decode(Attribute29,cAttribute29,1000,'*',1,0)+
                        Decode(Attribute30,cAttribute30,1000,'*',1,0) Total_Match,
                        Weighting_Total
                       ,decode(cAttributeNumber,1,attribute1,
                                                2,attribute2,
                                                3,attribute3,
                                                4,attribute4,
                                                5,attribute5,
                                                6,attribute6,
                                                7,attribute7,
                                                8,attribute8,
                                                9,attribute9,
                                                10,attribute10,
                                                11,attribute11,
                                                12,attribute12,
                                                13,attribute13,
                                                14,attribute14,
                                                15,attribute15,
                                                16,attribute16,
                                                17,attribute17,
                                                18,attribute18,
                                                19,attribute19,
                                                20,attribute20,
                                                21,attribute21,
                                                22,attribute22,
                                                23,attribute23,
                                                24,attribute24,
                                                25,attribute25,
                                                26,attribute26,
                                                27,attribute27,
                                                28,attribute28,
                                                29,attribute29,
                                                30,attribute30) best_match_attribute
                        ,rowid Row_id
                 from
                  (select *
                   from xxcp_custom_data_v w
                   where w.category = pCategory
                   and  pTransaction_Date Between nvl(w.EFFECTIVE_FROM_DATE,to_date('01-JAN-2000')) and nvl(w.EFFECTIVE_TO_DATE,to_date('31-DEC-2999'))
                     and ((w.ATTRIBUTE1 = cAttribute1 or cAttribute1 = '~' or w.Attribute1 = '*')
                     and (w.Attribute2 = cAttribute2 or cAttribute2 = '~' or w.Attribute2 = '*')
                     and (w.Attribute3 = cAttribute3 or cAttribute3 = '~' or w.Attribute3 = '*')
                     and (w.Attribute4 = cAttribute4 or cAttribute4 = '~' or w.Attribute4 = '*')
                     and (w.Attribute5 = cAttribute5 or cAttribute5 = '~' or w.Attribute5 = '*')
                     and (w.Attribute6 = cAttribute6 or cAttribute6 = '~' or w.Attribute6 = '*')
                     and (w.Attribute7 = cAttribute7 or cAttribute7 = '~' or w.Attribute7 = '*')
                     and (w.Attribute8 = cAttribute8 or cAttribute8 = '~' or w.Attribute8 = '*')
                     and (w.Attribute9 = cAttribute9 or cAttribute9 = '~' or w.Attribute9 = '*')
                     and (w.Attribute10 = cAttribute10 or cAttribute10 = '~' or w.Attribute10 = '*')
                     and (w.Attribute11 = cAttribute11 or cAttribute11 = '~' or w.Attribute11 = '*')
                     and (w.Attribute12 = cAttribute12 or cAttribute12 = '~' or w.Attribute12 = '*')
                     and (w.Attribute13 = cAttribute13 or cAttribute13 = '~' or w.Attribute13 = '*')
                     and (w.Attribute14 = cAttribute14 or cAttribute14 = '~' or w.Attribute14 = '*')
                     and (w.Attribute15 = cAttribute15 or cAttribute15 = '~' or w.Attribute15 = '*')
                     and (w.Attribute16 = cAttribute16 or cAttribute16 = '~' or w.Attribute16 = '*')
                     and (w.Attribute17 = cAttribute17 or cAttribute17 = '~' or w.Attribute17 = '*')
                     and (w.Attribute18 = cAttribute18 or cAttribute18 = '~' or w.Attribute18 = '*')
                     and (w.Attribute19 = cAttribute19 or cAttribute19 = '~' or w.Attribute19 = '*')
                     and (w.Attribute20 = cAttribute20 or cAttribute20 = '~' or w.Attribute20 = '*')
                     and (w.Attribute21 = cAttribute21 or cAttribute21 = '~' or w.Attribute21 = '*')
                     and (w.Attribute22 = cAttribute22 or cAttribute22 = '~' or w.Attribute22 = '*')
                     and (w.Attribute23 = cAttribute23 or cAttribute23 = '~' or w.Attribute23 = '*')
                     and (w.Attribute24 = cAttribute24 or cAttribute24 = '~' or w.Attribute24 = '*')
                     and (w.Attribute25 = cAttribute25 or cAttribute25 = '~' or w.Attribute25 = '*')
                     and (w.Attribute26 = cAttribute26 or cAttribute26 = '~' or w.Attribute26 = '*')
                     and (w.Attribute27 = cAttribute27 or cAttribute27 = '~' or w.Attribute27 = '*')
                     and (w.Attribute28 = cAttribute28 or cAttribute28 = '~' or w.Attribute28 = '*')
                     and (w.Attribute29 = cAttribute29 or cAttribute29 = '~' or w.Attribute29 = '*')
                     and (w.Attribute30 = cAttribute30 or cAttribute30 = '~' or w.Attribute30 = '*'))
                   )
                    order by Total_Match desc, Weighting_Total desc;            -- If Tied order by Only Exact Match

  vReturnAttribute  varchar2(100);
  vReturnTotalMatch number := 0;
  vTransaction_Date Date;
  vFoundTxt         varchar2(1) := 'N';
  vMsg              varchar2(4000);
  vFoundRowid       Rowid;
  vWeighting        Number(10,1);

  begin
    vTransaction_Date := trunc(nvl(cTransaction_Date,xxcp_global.SystemDate));

    For Rec in c1(pCategory => cCategory, pTransaction_date => vTransaction_Date) Loop
      vReturnAttribute  := Rec.Best_Match_Attribute;
      vFoundTxt := 'Y';
      vFoundRowid:= Rec.Row_id;
      vWeighting:= Rec.Weighting_Total;
      Exit;
    End Loop;

    If xxcp_global.Trace_on = 'Y' then

       vMsg :=  CustData_BestMatch_Trace(cCategory    => cCategory,
                                         cAttribute1  => cAttribute1,
                                         cAttribute2  => cAttribute2,
                                         cAttribute3  => cAttribute3,
                                         cAttribute4  => cAttribute4,
                                         cAttribute5  => cAttribute5,
                                         cAttribute6  => cAttribute6,
                                         cAttribute7  => cAttribute7,
                                         cAttribute8  => cAttribute8,
                                         cAttribute9  => cAttribute9,
                                         cAttribute10 => cAttribute10,
                                         cAttribute11 => cAttribute11,
                                         cAttribute12 => cAttribute12,
                                         cAttribute13 => cAttribute13,
                                         cAttribute14 => cAttribute14,
                                         cAttribute15 => cAttribute15,
                                         cAttribute16 => cAttribute16,
                                         cAttribute17 => cAttribute17,
                                         cAttribute18 => cAttribute18,
                                         cAttribute19 => cAttribute19,
                                         cAttribute20 => cAttribute20,
                                         cAttribute21 => cAttribute21,
                                         cAttribute22 => cAttribute22,
                                         cAttribute23 => cAttribute23,
                                         cAttribute24 => cAttribute24,
                                         cAttribute25 => cAttribute25,
                                         cAttribute26 => cAttribute26,
                                         cAttribute27 => cAttribute27,
                                         cAttribute28 => cAttribute28,
                                         cAttribute29 => cAttribute29,
                                         cAttribute30 => cAttribute30
                                        );

      vMsg := vMsg || chr(10)|| 'Return <'||vReturnAttribute||'>';

      xxcp_foundation.FndWriteError(100,'TRACE (Weighting): Category <'||cCategory||'> Found <'||vFoundTxt||'> Trx Date <'||cTransaction_Date||'> Rowid <'||vFoundRowid||'> Weighting <'||vWeighting||'>' , vMsg);

    End If;

    Return(vReturnAttribute);

  end CustomData_Weighting;

  -- *******************************************************************************
  --
  -- CustomData_Weighting2 (Multiple Records)
  --
  -- *******************************************************************************
  Function CustomData_Weighting2 ( cCategory         IN varchar2,
                                   cTransaction_Date IN date,
                                   cAttribute1       IN varchar2 default '~',
                                   cAttribute2       IN varchar2 default '~',
                                   cAttribute3       IN varchar2 default '~',
                                   cAttribute4       IN varchar2 default '~',
                                   cAttribute5       IN varchar2 default '~',
                                   cAttribute6       IN varchar2 default '~',
                                   cAttribute7       IN varchar2 default '~',
                                   cAttribute8       IN varchar2 default '~',
                                   cAttribute9       IN varchar2 default '~',
                                   cAttribute10      IN varchar2 default '~',
                                   cAttribute11      IN varchar2 default '~',
                                   cAttribute12      IN varchar2 default '~',
                                   cAttribute13      IN varchar2 default '~',
                                   cAttribute14      IN varchar2 default '~',
                                   cAttribute15      IN varchar2 default '~',
                                   cAttribute16      IN varchar2 default '~',
                                   cAttribute17      IN varchar2 default '~',
                                   cAttribute18      IN varchar2 default '~',
                                   cAttribute19      IN varchar2 default '~',
                                   cAttribute20      IN varchar2 default '~',
                                   cAttribute21      IN varchar2 default '~',
                                   cAttribute22      IN varchar2 default '~',
                                   cAttribute23      IN varchar2 default '~',
                                   cAttribute24      IN varchar2 default '~',
                                   cAttribute25      IN varchar2 default '~',
                                   cAttribute26      IN varchar2 default '~',
                                   cAttribute27      IN varchar2 default '~',
                                   cAttribute28      IN varchar2 default '~',
                                   cAttribute29      IN varchar2 default '~',
                                   cAttribute30      IN varchar2 default '~',
                                   cBestMatch_Record OUT gBestMatchRec%type
                                   )  Return Boolean is

    cursor c1(pTransaction_Date in Date) is
       Select
                        Decode(Attribute1,cAttribute1,1000,'*',1,0)+
                        Decode(Attribute2,cAttribute2,1000,'*',1,0)+
                        Decode(Attribute3,cAttribute3,1000,'*',1,0)+
                        Decode(Attribute4,cAttribute4,1000,'*',1,0)+
                        Decode(Attribute5,cAttribute5,1000,'*',1,0)+
                        Decode(Attribute6,cAttribute6,1000,'*',1,0)+
                        Decode(Attribute7,cAttribute7,1000,'*',1,0)+
                        Decode(Attribute8,cAttribute8,1000,'*',1,0)+
                        Decode(Attribute9,cAttribute9,1000,'*',1,0)+
                        Decode(Attribute10,cAttribute10,1000,'*',1,0)+
                        Decode(Attribute11,cAttribute11,1000,'*',1,0)+
                        Decode(Attribute12,cAttribute12,1000,'*',1,0)+
                        Decode(Attribute13,cAttribute13,1000,'*',1,0)+
                        Decode(Attribute14,cAttribute14,1000,'*',1,0)+
                        Decode(Attribute15,cAttribute15,1000,'*',1,0)+
                        Decode(Attribute16,cAttribute16,1000,'*',1,0)+
                        Decode(Attribute17,cAttribute17,1000,'*',1,0)+
                        Decode(Attribute18,cAttribute18,1000,'*',1,0)+
                        Decode(Attribute19,cAttribute19,1000,'*',1,0)+
                        Decode(Attribute20,cAttribute20,1000,'*',1,0)+
                        Decode(Attribute21,cAttribute21,1000,'*',1,0)+
                        Decode(Attribute22,cAttribute22,1000,'*',1,0)+
                        Decode(Attribute23,cAttribute23,1000,'*',1,0)+
                        Decode(Attribute24,cAttribute24,1000,'*',1,0)+
                        Decode(Attribute25,cAttribute25,1000,'*',1,0)+
                        Decode(Attribute26,cAttribute26,1000,'*',1,0)+
                        Decode(Attribute27,cAttribute27,1000,'*',1,0)+
                        Decode(Attribute28,cAttribute28,1000,'*',1,0)+
                        Decode(Attribute29,cAttribute29,1000,'*',1,0)+
                        Decode(Attribute30,cAttribute30,1000,'*',1,0) Total_Match,
                        Weighting_Total,
                        attribute1,
                        attribute2,
                        attribute3,
                        attribute4,
                        attribute5,
                        attribute6,
                        attribute7,
                        attribute8,
                        attribute9,
                        attribute10,
                        attribute11,
                        attribute12,
                        attribute13,
                        attribute14,
                        attribute15,
                        attribute16,
                        attribute17,
                        attribute18,
                        attribute19,
                        attribute20,
                        attribute21,
                        attribute22,
                        attribute23,
                        attribute24,
                        attribute25,
                        attribute26,
                        attribute27,
                        attribute28,
                        attribute29,
                        attribute30,
                        rowid Row_id
                 from
                  (select
                      w.*
                    from xxcp_custom_data_v w
                   where w.category = cCategory
                     and pTransaction_Date Between nvl(w.EFFECTIVE_FROM_DATE,to_date('01-JAN-2000')) and nvl(w.EFFECTIVE_TO_DATE,to_date('31-DEC-2999'))
                     and ((w.ATTRIBUTE1 = cAttribute1 or cAttribute1 = '~' or w.Attribute1 = '*')
                     and (w.Attribute2 = cAttribute2 or cAttribute2 = '~' or w.Attribute2 = '*')
                     and (w.Attribute3 = cAttribute3 or cAttribute3 = '~' or w.Attribute3 = '*')
                     and (w.Attribute4 = cAttribute4 or cAttribute4 = '~' or w.Attribute4 = '*')
                     and (w.Attribute5 = cAttribute5 or cAttribute5 = '~' or w.Attribute5 = '*')
                     and (w.Attribute6 = cAttribute6 or cAttribute6 = '~' or w.Attribute6 = '*')
                     and (w.Attribute7 = cAttribute7 or cAttribute7 = '~' or w.Attribute7 = '*')
                     and (w.Attribute8 = cAttribute8 or cAttribute8 = '~' or w.Attribute8 = '*')
                     and (w.Attribute9 = cAttribute9 or cAttribute9 = '~' or w.Attribute9 = '*')
                     and (w.Attribute10 = cAttribute10 or cAttribute10 = '~' or w.Attribute10 = '*')
                     and (w.Attribute11 = cAttribute11 or cAttribute11 = '~' or w.Attribute11 = '*')
                     and (w.Attribute12 = cAttribute12 or cAttribute12 = '~' or w.Attribute12 = '*')
                     and (w.Attribute13 = cAttribute13 or cAttribute13 = '~' or w.Attribute13 = '*')
                     and (w.Attribute14 = cAttribute14 or cAttribute14 = '~' or w.Attribute14 = '*')
                     and (w.Attribute15 = cAttribute15 or cAttribute15 = '~' or w.Attribute15 = '*')
                     and (w.Attribute16 = cAttribute16 or cAttribute16 = '~' or w.Attribute16 = '*')
                     and (w.Attribute17 = cAttribute17 or cAttribute17 = '~' or w.Attribute17 = '*')
                     and (w.Attribute18 = cAttribute18 or cAttribute18 = '~' or w.Attribute18 = '*')
                     and (w.Attribute19 = cAttribute19 or cAttribute19 = '~' or w.Attribute19 = '*')
                     and (w.Attribute20 = cAttribute20 or cAttribute20 = '~' or w.Attribute20 = '*')
                     and (w.Attribute21 = cAttribute21 or cAttribute21 = '~' or w.Attribute21 = '*')
                     and (w.Attribute22 = cAttribute22 or cAttribute22 = '~' or w.Attribute22 = '*')
                     and (w.Attribute23 = cAttribute23 or cAttribute23 = '~' or w.Attribute23 = '*')
                     and (w.Attribute24 = cAttribute24 or cAttribute24 = '~' or w.Attribute24 = '*')
                     and (w.Attribute25 = cAttribute25 or cAttribute25 = '~' or w.Attribute25 = '*')
                     and (w.Attribute26 = cAttribute26 or cAttribute26 = '~' or w.Attribute26 = '*')
                     and (w.Attribute27 = cAttribute27 or cAttribute27 = '~' or w.Attribute27 = '*')
                     and (w.Attribute28 = cAttribute28 or cAttribute28 = '~' or w.Attribute28 = '*')
                     and (w.Attribute29 = cAttribute29 or cAttribute29 = '~' or w.Attribute29 = '*')
                     and (w.Attribute30 = cAttribute30 or cAttribute30 = '~' or w.Attribute30 = '*'))
                   )
                order by Total_Match desc,Weighting_Total desc;            -- If Tied order by Only Exact Match


  vReturnAttribute  varchar2(100);
  vReturnTotalMatch number := 0;
  vTransaction_Date Date;
  vFound            boolean := FALSE;
  vFoundTxt         varchar2(1) := 'N';
  vMsg              varchar2(4000);

  vFoundRowid       Rowid;
  vWeighting        Number(10,1);

  begin

    vTransaction_Date := trunc(nvl(cTransaction_Date,xxcp_global.SystemDate));

    cBestMatch_Record.Attribute1 := Null;

    For Rec in C1(pTransaction_date => vTransaction_Date)
      Loop

        cBestMatch_Record.Attribute1  := Rec.Attribute1;
        cBestMatch_Record.Attribute2  := Rec.Attribute2;
        cBestMatch_Record.Attribute3  := Rec.Attribute3;
        cBestMatch_Record.Attribute4  := Rec.Attribute4;
        cBestMatch_Record.Attribute5  := Rec.Attribute5;
        cBestMatch_Record.Attribute6  := Rec.Attribute6;
        cBestMatch_Record.Attribute7  := Rec.Attribute7;
        cBestMatch_Record.Attribute8  := Rec.Attribute8;
        cBestMatch_Record.Attribute9  := Rec.Attribute9;
        cBestMatch_Record.Attribute10 := Rec.Attribute10;
        cBestMatch_Record.Attribute11 := Rec.Attribute11;
        cBestMatch_Record.Attribute12 := Rec.Attribute12;
        cBestMatch_Record.Attribute13 := Rec.Attribute13;
        cBestMatch_Record.Attribute14 := Rec.Attribute14;
        cBestMatch_Record.Attribute15 := Rec.Attribute15;
        cBestMatch_Record.Attribute16 := Rec.Attribute16;
        cBestMatch_Record.Attribute17 := Rec.Attribute17;
        cBestMatch_Record.Attribute18 := Rec.Attribute18;
        cBestMatch_Record.Attribute19 := Rec.Attribute19;
        cBestMatch_Record.Attribute20 := Rec.Attribute20;
        cBestMatch_Record.Attribute21 := Rec.Attribute21;
        cBestMatch_Record.Attribute22 := Rec.Attribute22;
        cBestMatch_Record.Attribute23 := Rec.Attribute23;
        cBestMatch_Record.Attribute24 := Rec.Attribute24;
        cBestMatch_Record.Attribute25 := Rec.Attribute25;
        cBestMatch_Record.Attribute26 := Rec.Attribute26;
        cBestMatch_Record.Attribute27 := Rec.Attribute27;
        cBestMatch_Record.Attribute28 := Rec.Attribute28;
        cBestMatch_Record.Attribute29 := Rec.Attribute29;
        cBestMatch_Record.Attribute30 := Rec.Attribute30;
        vFound := TRUE;
        vFoundTxt := 'Y';
        vFoundRowid:= Rec.Row_id;
        Exit;
    End Loop;

    If xxcp_global.Trace_on = 'Y' then

       vMsg :=  CustData_BestMatch_Trace(cCategory    => cCategory,
                                         cAttribute1  => cAttribute1,
                                         cAttribute2  => cAttribute2,
                                         cAttribute3  => cAttribute3,
                                         cAttribute4  => cAttribute4,
                                         cAttribute5  => cAttribute5,
                                         cAttribute6  => cAttribute6,
                                         cAttribute7  => cAttribute7,
                                         cAttribute8  => cAttribute8,
                                         cAttribute9  => cAttribute9,
                                         cAttribute10 => cAttribute10,
                                         cAttribute11 => cAttribute11,
                                         cAttribute12 => cAttribute12,
                                         cAttribute13 => cAttribute13,
                                         cAttribute14 => cAttribute14,
                                         cAttribute15 => cAttribute15,
                                         cAttribute16 => cAttribute16,
                                         cAttribute17 => cAttribute17,
                                         cAttribute18 => cAttribute18,
                                         cAttribute19 => cAttribute19,
                                         cAttribute20 => cAttribute20,
                                         cAttribute21 => cAttribute21,
                                         cAttribute22 => cAttribute22,
                                         cAttribute23 => cAttribute23,
                                         cAttribute24 => cAttribute24,
                                         cAttribute25 => cAttribute25,
                                         cAttribute26 => cAttribute26,
                                         cAttribute27 => cAttribute27,
                                         cAttribute28 => cAttribute28,
                                         cAttribute29 => cAttribute29,
                                         cAttribute30 => cAttribute30
                                        );

      xxcp_foundation.FndWriteError(100,'TRACE (BestMatch2): Category <'||cCategory||'> Found <'||vFoundTxt||'> Trx Date <'||cTransaction_Date||'> Rowid <'||vFoundRowid||'> Weighting <'||vWeighting||'>', vMsg);

    End If;

    Return(vFound);

  end CustomData_Weighting2;

  -- *******************************************************************************
  --
  -- Add_Weighting
  --
  -- *******************************************************************************
  Procedure Add_Weighting is

  vCount number;
  begin
       	update XXCP_cust_data a
			  set    weighting_total = (select b.weighting_total
			                      from   xxcp_cust_data_weighting_v b
			                      where  a.category_name = b.category_name
			                      and    a.cust_data_id  = b.cust_data_id);

        vCount := SQL%ROWCOUNT;
        commit;

        dbms_output.put_line(vCount || ' Custom Data records have been updated.');

  end Add_Weighting;


 -- *******************************************************************************
 --            CustData
 -- *******************************************************************************
  Function CustData(               cCategory         IN varchar2,
                                   cAttributeNumber  IN number   default 1,
                                   cTransaction_Date IN date,
                                   cAttribute1       IN varchar2 default '~',
                                   cAttribute2       IN varchar2 default '~',
                                   cAttribute3       IN varchar2 default '~',
                                   cAttribute4       IN varchar2 default '~',
                                   cAttribute5       IN varchar2 default '~',
                                   cAttribute6       IN varchar2 default '~',
                                   cAttribute7       IN varchar2 default '~',
                                   cAttribute8       IN varchar2 default '~',
                                   cAttribute9       IN varchar2 default '~',
                                   cAttribute10      IN varchar2 default '~',
                                   cAttribute11      IN varchar2 default '~',
                                   cAttribute12      IN varchar2 default '~',
                                   cAttribute13      IN varchar2 default '~',
                                   cAttribute14      IN varchar2 default '~',
                                   cAttribute15      IN varchar2 default '~',
                                   cAttribute16      IN varchar2 default '~',
                                   cAttribute17      IN varchar2 default '~',
                                   cAttribute18      IN varchar2 default '~',
                                   cAttribute19      IN varchar2 default '~',
                                   cAttribute20      IN varchar2 default '~',
                                   cAttribute21      IN varchar2 default '~',
                                   cAttribute22      IN varchar2 default '~',
                                   cAttribute23      IN varchar2 default '~',
                                   cAttribute24      IN varchar2 default '~',
                                   cAttribute25      IN varchar2 default '~',
                                   cAttribute26      IN varchar2 default '~',
                                   cAttribute27      IN varchar2 default '~',
                                   cAttribute28      IN varchar2 default '~',
                                   cAttribute29      IN varchar2 default '~',
                                   cAttribute30      IN varchar2 default '~'
                                   ) return varchar2 is

   vStop_Matching    number(3);

    cursor c1(pCategory in varchar2, pTransaction_Date in Date) is
                 select
                        decode(cAttributeNumber,1,attribute1,
                                                2,attribute2,
                                                3,attribute3,
                                                4,attribute4,
                                                5,attribute5,
                                                6,attribute6,
                                                7,attribute7,
                                                8,attribute8,
                                                9,attribute9,
                                                10,attribute10,
                                                11,attribute11,
                                                12,attribute12,
                                                13,attribute13,
                                                14,attribute14,
                                                15,attribute15,
                                                16,attribute16,
                                                17,attribute17,
                                                18,attribute18,
                                                19,attribute19,
                                                20,attribute20,
                                                21,attribute21,
                                                22,attribute22,
                                                23,attribute23,
                                                24,attribute24,
                                                25,attribute25,
                                                26,attribute26,
                                                27,attribute27,
                                                28,attribute28,
                                                29,attribute29,
                                                30,attribute30) best_match_attribute
                 from
                  (select *
                   from xxcp_custom_data_v w
                   where w.category = pCategory
                   and  pTransaction_Date Between nvl(w.EFFECTIVE_FROM_DATE,to_date('01-JAN-2000')) and nvl(w.EFFECTIVE_TO_DATE,to_date('31-DEC-2999'))
                     and ((w.ATTRIBUTE1 = cAttribute1 or cAttribute1 = '~' or w.Attribute1 = '*')
                     and (w.Attribute2 = cAttribute2 or cAttribute2 = '~' or w.Attribute2 = '*')
                     and (w.Attribute3 = cAttribute3 or cAttribute3 = '~' or w.Attribute3 = '*')
                     and (w.Attribute4 = cAttribute4 or cAttribute4 = '~' or w.Attribute4 = '*')
                     and (w.Attribute5 = cAttribute5 or cAttribute5 = '~' or w.Attribute5 = '*')
                     and (w.Attribute6 = cAttribute6 or cAttribute6 = '~' or w.Attribute6 = '*')
                     and (w.Attribute7 = cAttribute7 or cAttribute7 = '~' or w.Attribute7 = '*')
                     and (w.Attribute8 = cAttribute8 or cAttribute8 = '~' or w.Attribute8 = '*')
                     and (w.Attribute9 = cAttribute9 or cAttribute9 = '~' or w.Attribute9 = '*')
                     and (w.Attribute10 = cAttribute10 or cAttribute10 = '~' or w.Attribute10 = '*')
                     and (w.Attribute11 = cAttribute11 or cAttribute11 = '~' or w.Attribute11 = '*')
                     and (w.Attribute12 = cAttribute12 or cAttribute12 = '~' or w.Attribute12 = '*')
                     and (w.Attribute13 = cAttribute13 or cAttribute13 = '~' or w.Attribute13 = '*')
                     and (w.Attribute14 = cAttribute14 or cAttribute14 = '~' or w.Attribute14 = '*')
                     and (w.Attribute15 = cAttribute15 or cAttribute15 = '~' or w.Attribute15 = '*')
                     and (w.Attribute16 = cAttribute16 or cAttribute16 = '~' or w.Attribute16 = '*')
                     and (w.Attribute17 = cAttribute17 or cAttribute17 = '~' or w.Attribute17 = '*')
                     and (w.Attribute18 = cAttribute18 or cAttribute18 = '~' or w.Attribute18 = '*')
                     and (w.Attribute19 = cAttribute19 or cAttribute19 = '~' or w.Attribute19 = '*')
                     and (w.Attribute20 = cAttribute20 or cAttribute20 = '~' or w.Attribute20 = '*')
                     and (w.Attribute21 = cAttribute21 or cAttribute21 = '~' or w.Attribute21 = '*')
                     and (w.Attribute22 = cAttribute22 or cAttribute22 = '~' or w.Attribute22 = '*')
                     and (w.Attribute23 = cAttribute23 or cAttribute23 = '~' or w.Attribute23 = '*')
                     and (w.Attribute24 = cAttribute24 or cAttribute24 = '~' or w.Attribute24 = '*')
                     and (w.Attribute25 = cAttribute25 or cAttribute25 = '~' or w.Attribute25 = '*')
                     and (w.Attribute26 = cAttribute26 or cAttribute26 = '~' or w.Attribute26 = '*')
                     and (w.Attribute27 = cAttribute27 or cAttribute27 = '~' or w.Attribute27 = '*')
                     and (w.Attribute28 = cAttribute28 or cAttribute28 = '~' or w.Attribute28 = '*')
                     and (w.Attribute29 = cAttribute29 or cAttribute29 = '~' or w.Attribute29 = '*')
                     and (w.Attribute30 = cAttribute30 or cAttribute30 = '~' or w.Attribute30 = '*'))
                   )
                    order by 1 desc;

  vReturnAttribute  varchar2(100);
  vReturnTotalMatch number := 0;

  begin
    For Rec in c1(pCategory => cCategory, pTransaction_date => cTransaction_Date) Loop
      vReturnAttribute  := Rec.Best_Match_Attribute;
      Exit;
    End Loop;

    Return(vReturnAttribute);

   End CustData;

Function CustomData_BestMatch_Children(cCategory         IN varchar2,
                                         cTransaction_Date IN date,
                                         cAttribute1       IN varchar2 default '~',
                                         cAttribute2       IN varchar2 default '~',
                                         cAttribute3       IN varchar2 default '~',
                                         cAttribute4       IN varchar2 default '~',
                                         cAttribute5       IN varchar2 default '~',
                                         cAttribute6       IN varchar2 default '~',
                                         cAttribute7       IN varchar2 default '~',
                                         cAttribute8       IN varchar2 default '~',
                                         cAttribute9       IN varchar2 default '~',
                                         cAttribute10      IN varchar2 default '~',
                                         cAttribute11      IN varchar2 default '~',
                                         cAttribute12      IN varchar2 default '~',
                                         cAttribute13      IN varchar2 default '~',
                                         cAttribute14      IN varchar2 default '~',
                                         cAttribute15      IN varchar2 default '~',
                                         cAttribute16      IN varchar2 default '~',
                                         cAttribute17      IN varchar2 default '~',
                                         cAttribute18      IN varchar2 default '~',
                                         cAttribute19      IN varchar2 default '~',
                                         cAttribute20      IN varchar2 default '~',
                                         cAttribute21      IN varchar2 default '~',
                                         cAttribute22      IN varchar2 default '~',
                                         cAttribute23      IN varchar2 default '~',
                                         cAttribute24      IN varchar2 default '~',
                                         cAttribute25      IN varchar2 default '~',
                                         cAttribute26      IN varchar2 default '~',
                                         cAttribute27      IN varchar2 default '~',
                                         cAttribute28      IN varchar2 default '~',
                                         cAttribute29      IN varchar2 default '~',
                                         cAttribute30      IN varchar2 default '~',
                                         cChild_Tab        OUT gBestMatchTab%type
                                         ) return number is  

    cursor c1(pTransaction_Date in Date) is
       Select
                        Decode(Attribute1,cAttribute1,1000,'*',1,0)+
                        Decode(Attribute2,cAttribute2,1000,'*',1,0)+
                        Decode(Attribute3,cAttribute3,1000,'*',1,0)+
                        Decode(Attribute4,cAttribute4,1000,'*',1,0)+
                        Decode(Attribute5,cAttribute5,1000,'*',1,0)+
                        Decode(Attribute6,cAttribute6,1000,'*',1,0)+
                        Decode(Attribute7,cAttribute7,1000,'*',1,0)+
                        Decode(Attribute8,cAttribute8,1000,'*',1,0)+
                        Decode(Attribute9,cAttribute9,1000,'*',1,0)+
                        Decode(Attribute10,cAttribute10,1000,'*',1,0)+
                        Decode(Attribute11,cAttribute11,1000,'*',1,0)+
                        Decode(Attribute12,cAttribute12,1000,'*',1,0)+
                        Decode(Attribute13,cAttribute13,1000,'*',1,0)+
                        Decode(Attribute14,cAttribute14,1000,'*',1,0)+
                        Decode(Attribute15,cAttribute15,1000,'*',1,0)+
                        Decode(Attribute16,cAttribute16,1000,'*',1,0)+
                        Decode(Attribute17,cAttribute17,1000,'*',1,0)+
                        Decode(Attribute18,cAttribute18,1000,'*',1,0)+
                        Decode(Attribute19,cAttribute19,1000,'*',1,0)+
                        Decode(Attribute20,cAttribute20,1000,'*',1,0)+
                        Decode(Attribute21,cAttribute21,1000,'*',1,0)+
                        Decode(Attribute22,cAttribute22,1000,'*',1,0)+
                        Decode(Attribute23,cAttribute23,1000,'*',1,0)+
                        Decode(Attribute24,cAttribute24,1000,'*',1,0)+
                        Decode(Attribute25,cAttribute25,1000,'*',1,0)+
                        Decode(Attribute26,cAttribute26,1000,'*',1,0)+
                        Decode(Attribute27,cAttribute27,1000,'*',1,0)+
                        Decode(Attribute28,cAttribute28,1000,'*',1,0)+
                        Decode(Attribute29,cAttribute29,1000,'*',1,0)+
                        Decode(Attribute30,cAttribute30,1000,'*',1,0) Total_Match
                        ,
                        cust_data_id,
                        rowid Row_id
                 from
                  (select
                      w.*
                    from xxcp_custom_data_v w
                   where w.category = cCategory
                     and pTransaction_Date Between nvl(w.EFFECTIVE_FROM_DATE,to_date('01-JAN-2000')) and nvl(w.EFFECTIVE_TO_DATE,to_date('31-DEC-2999'))
                     and ((w.ATTRIBUTE1 = cAttribute1 or cAttribute1 = '~' or w.Attribute1 = '*')
                     and (w.Attribute2 = cAttribute2 or cAttribute2 = '~' or w.Attribute2 = '*')
                     and (w.Attribute3 = cAttribute3 or cAttribute3 = '~' or w.Attribute3 = '*')
                     and (w.Attribute4 = cAttribute4 or cAttribute4 = '~' or w.Attribute4 = '*')
                     and (w.Attribute5 = cAttribute5 or cAttribute5 = '~' or w.Attribute5 = '*')
                     and (w.Attribute6 = cAttribute6 or cAttribute6 = '~' or w.Attribute6 = '*')
                     and (w.Attribute7 = cAttribute7 or cAttribute7 = '~' or w.Attribute7 = '*')
                     and (w.Attribute8 = cAttribute8 or cAttribute8 = '~' or w.Attribute8 = '*')
                     and (w.Attribute9 = cAttribute9 or cAttribute9 = '~' or w.Attribute9 = '*')
                     and (w.Attribute10 = cAttribute10 or cAttribute10 = '~' or w.Attribute10 = '*')
                     and (w.Attribute11 = cAttribute11 or cAttribute11 = '~' or w.Attribute11 = '*')
                     and (w.Attribute12 = cAttribute12 or cAttribute12 = '~' or w.Attribute12 = '*')
                     and (w.Attribute13 = cAttribute13 or cAttribute13 = '~' or w.Attribute13 = '*')
                     and (w.Attribute14 = cAttribute14 or cAttribute14 = '~' or w.Attribute14 = '*')
                     and (w.Attribute15 = cAttribute15 or cAttribute15 = '~' or w.Attribute15 = '*')
                     and (w.Attribute16 = cAttribute16 or cAttribute16 = '~' or w.Attribute16 = '*')
                     and (w.Attribute17 = cAttribute17 or cAttribute17 = '~' or w.Attribute17 = '*')
                     and (w.Attribute18 = cAttribute18 or cAttribute18 = '~' or w.Attribute18 = '*')
                     and (w.Attribute19 = cAttribute19 or cAttribute19 = '~' or w.Attribute19 = '*')
                     and (w.Attribute20 = cAttribute20 or cAttribute20 = '~' or w.Attribute20 = '*')
                     and (w.Attribute21 = cAttribute21 or cAttribute21 = '~' or w.Attribute21 = '*')
                     and (w.Attribute22 = cAttribute22 or cAttribute22 = '~' or w.Attribute22 = '*')
                     and (w.Attribute23 = cAttribute23 or cAttribute23 = '~' or w.Attribute23 = '*')
                     and (w.Attribute24 = cAttribute24 or cAttribute24 = '~' or w.Attribute24 = '*')
                     and (w.Attribute25 = cAttribute25 or cAttribute25 = '~' or w.Attribute25 = '*')
                     and (w.Attribute26 = cAttribute26 or cAttribute26 = '~' or w.Attribute26 = '*')
                     and (w.Attribute27 = cAttribute27 or cAttribute27 = '~' or w.Attribute27 = '*')
                     and (w.Attribute28 = cAttribute28 or cAttribute28 = '~' or w.Attribute28 = '*')
                     and (w.Attribute29 = cAttribute29 or cAttribute29 = '~' or w.Attribute29 = '*')
                     and (w.Attribute30 = cAttribute30 or cAttribute30 = '~' or w.Attribute30 = '*'))
                   )
                order by 1 desc;         
                   -- If Tied order by Only Exact Match     
                     
  Cursor c2 (pParent_Cust_Data_id IN NUMBER)
         is select attribute1,attribute2,attribute3,attribute4,attribute5,
                   attribute6,attribute7,attribute8,attribute9,attribute10,
                   attribute11,attribute12,attribute13,attribute14,attribute15,
                   attribute16,attribute17,attribute18,attribute19,attribute20,
                   attribute21,attribute22,attribute23,attribute24,attribute25,
                   attribute26,attribute27,attribute28,attribute29,attribute30
             from  xxcp_cust_data
             where parent_cust_data_id = pParent_Cust_Data_id; 

  vTransaction_Date Date;
  vChildrenCount    number  := 0;
  vFoundTxt         varchar2(1) := 'N';
  vMsg              varchar2(4000);

  vFoundRowid      Rowid;

  begin

    vTransaction_Date := trunc(nvl(cTransaction_Date,xxcp_global.SystemDate));

    --cBestMatch_Record.Attribute1 := Null;

    For Rec in C1(pTransaction_date => vTransaction_Date)
      Loop
        vFoundTxt   := 'Y';
        vFoundRowid := Rec.Row_id;

        -- Get Children
        For c2_rec in c2(Rec.cust_data_id) loop
          vChildrenCount := vChildrenCount + 1;
          
          cChild_Tab(vChildrenCount).Attribute1  := c2_rec.attribute1;
          cChild_Tab(vChildrenCount).Attribute2  := c2_rec.attribute2;
          cChild_Tab(vChildrenCount).Attribute3  := c2_rec.attribute3;
          cChild_Tab(vChildrenCount).Attribute4  := c2_rec.attribute4;
          cChild_Tab(vChildrenCount).Attribute5  := c2_rec.attribute5;
          cChild_Tab(vChildrenCount).Attribute6  := c2_rec.attribute6;
          cChild_Tab(vChildrenCount).Attribute7  := c2_rec.attribute7;
          cChild_Tab(vChildrenCount).Attribute8  := c2_rec.attribute8;
          cChild_Tab(vChildrenCount).Attribute9  := c2_rec.attribute9;
          cChild_Tab(vChildrenCount).Attribute10 := c2_rec.attribute10;
          cChild_Tab(vChildrenCount).Attribute11 := c2_rec.attribute11;
          cChild_Tab(vChildrenCount).Attribute12 := c2_rec.attribute12;
          cChild_Tab(vChildrenCount).Attribute13 := c2_rec.attribute13;
          cChild_Tab(vChildrenCount).Attribute14 := c2_rec.attribute14;
          cChild_Tab(vChildrenCount).Attribute15 := c2_rec.attribute15;
          cChild_Tab(vChildrenCount).Attribute16 := c2_rec.attribute16;
          cChild_Tab(vChildrenCount).Attribute17 := c2_rec.attribute17;
          cChild_Tab(vChildrenCount).Attribute18 := c2_rec.attribute18;
          cChild_Tab(vChildrenCount).Attribute19 := c2_rec.attribute19;
          cChild_Tab(vChildrenCount).Attribute20 := c2_rec.attribute20;
          cChild_Tab(vChildrenCount).Attribute21 := c2_rec.attribute21;
          cChild_Tab(vChildrenCount).Attribute22 := c2_rec.attribute22;
          cChild_Tab(vChildrenCount).Attribute23 := c2_rec.attribute23;
          cChild_Tab(vChildrenCount).Attribute24 := c2_rec.attribute24;
          cChild_Tab(vChildrenCount).Attribute25 := c2_rec.attribute25;
          cChild_Tab(vChildrenCount).Attribute26 := c2_rec.attribute26;
          cChild_Tab(vChildrenCount).Attribute27 := c2_rec.attribute27;
          cChild_Tab(vChildrenCount).Attribute28 := c2_rec.attribute28;
          cChild_Tab(vChildrenCount).Attribute29 := c2_rec.attribute29;
          cChild_Tab(vChildrenCount).Attribute30 := c2_rec.attribute30;

        End loop;

        Exit;
    End Loop;

    If xxcp_global.Trace_on = 'Y' then

       vMsg :=  CustData_BestMatch_Trace(cCategory    => cCategory,
                                         cAttribute1  => cAttribute1,
                                         cAttribute2  => cAttribute2,
                                         cAttribute3  => cAttribute3,
                                         cAttribute4  => cAttribute4,
                                         cAttribute5  => cAttribute5,
                                         cAttribute6  => cAttribute6,
                                         cAttribute7  => cAttribute7,
                                         cAttribute8  => cAttribute8,
                                         cAttribute9  => cAttribute9,
                                         cAttribute10 => cAttribute10,
                                         cAttribute11 => cAttribute11,
                                         cAttribute12 => cAttribute12,
                                         cAttribute13 => cAttribute13,
                                         cAttribute14 => cAttribute14,
                                         cAttribute15 => cAttribute15,
                                         cAttribute16 => cAttribute16,
                                         cAttribute17 => cAttribute17,
                                         cAttribute18 => cAttribute18,
                                         cAttribute19 => cAttribute19,
                                         cAttribute20 => cAttribute20,
                                         cAttribute21 => cAttribute21,
                                         cAttribute22 => cAttribute22,
                                         cAttribute23 => cAttribute23,
                                         cAttribute24 => cAttribute24,
                                         cAttribute25 => cAttribute25,
                                         cAttribute26 => cAttribute26,
                                         cAttribute27 => cAttribute27,
                                         cAttribute28 => cAttribute28,
                                         cAttribute29 => cAttribute29,
                                         cAttribute30 => cAttribute30
                                        );

      xxcp_foundation.FndWriteError(100,'TRACE (CustomData_BestMatch_Children): Category <'||cCategory||'> Level1 Found <'||vFoundTxt||'> Trx Date <'||cTransaction_Date||'> Level1 Rowid <'||vFoundRowid||'>', vMsg);

    End If;

    Return(vChildrenCount);

  end CustomData_BestMatch_Children;

  Function CustomData_Weighting_Children(cCategory         IN varchar2,
                                         cTransaction_Date IN date,
                                         cAttribute1       IN varchar2 default '~',
                                         cAttribute2       IN varchar2 default '~',
                                         cAttribute3       IN varchar2 default '~',
                                         cAttribute4       IN varchar2 default '~',
                                         cAttribute5       IN varchar2 default '~',
                                         cAttribute6       IN varchar2 default '~',
                                         cAttribute7       IN varchar2 default '~',
                                         cAttribute8       IN varchar2 default '~',
                                         cAttribute9       IN varchar2 default '~',
                                         cAttribute10      IN varchar2 default '~',
                                         cAttribute11      IN varchar2 default '~',
                                         cAttribute12      IN varchar2 default '~',
                                         cAttribute13      IN varchar2 default '~',
                                         cAttribute14      IN varchar2 default '~',
                                         cAttribute15      IN varchar2 default '~',
                                         cAttribute16      IN varchar2 default '~',
                                         cAttribute17      IN varchar2 default '~',
                                         cAttribute18      IN varchar2 default '~',
                                         cAttribute19      IN varchar2 default '~',
                                         cAttribute20      IN varchar2 default '~',
                                         cAttribute21      IN varchar2 default '~',
                                         cAttribute22      IN varchar2 default '~',
                                         cAttribute23      IN varchar2 default '~',
                                         cAttribute24      IN varchar2 default '~',
                                         cAttribute25      IN varchar2 default '~',
                                         cAttribute26      IN varchar2 default '~',
                                         cAttribute27      IN varchar2 default '~',
                                         cAttribute28      IN varchar2 default '~',
                                         cAttribute29      IN varchar2 default '~',
                                         cAttribute30      IN varchar2 default '~',
                                         cChild_Tab        OUT gBestMatchTab%type
                                         ) return number is  

    cursor c1(pTransaction_Date in Date) is
       Select
                        Decode(Attribute1,cAttribute1,1000,'*',1,0)+
                        Decode(Attribute2,cAttribute2,1000,'*',1,0)+
                        Decode(Attribute3,cAttribute3,1000,'*',1,0)+
                        Decode(Attribute4,cAttribute4,1000,'*',1,0)+
                        Decode(Attribute5,cAttribute5,1000,'*',1,0)+
                        Decode(Attribute6,cAttribute6,1000,'*',1,0)+
                        Decode(Attribute7,cAttribute7,1000,'*',1,0)+
                        Decode(Attribute8,cAttribute8,1000,'*',1,0)+
                        Decode(Attribute9,cAttribute9,1000,'*',1,0)+
                        Decode(Attribute10,cAttribute10,1000,'*',1,0)+
                        Decode(Attribute11,cAttribute11,1000,'*',1,0)+
                        Decode(Attribute12,cAttribute12,1000,'*',1,0)+
                        Decode(Attribute13,cAttribute13,1000,'*',1,0)+
                        Decode(Attribute14,cAttribute14,1000,'*',1,0)+
                        Decode(Attribute15,cAttribute15,1000,'*',1,0)+
                        Decode(Attribute16,cAttribute16,1000,'*',1,0)+
                        Decode(Attribute17,cAttribute17,1000,'*',1,0)+
                        Decode(Attribute18,cAttribute18,1000,'*',1,0)+
                        Decode(Attribute19,cAttribute19,1000,'*',1,0)+
                        Decode(Attribute20,cAttribute20,1000,'*',1,0)+
                        Decode(Attribute21,cAttribute21,1000,'*',1,0)+
                        Decode(Attribute22,cAttribute22,1000,'*',1,0)+
                        Decode(Attribute23,cAttribute23,1000,'*',1,0)+
                        Decode(Attribute24,cAttribute24,1000,'*',1,0)+
                        Decode(Attribute25,cAttribute25,1000,'*',1,0)+
                        Decode(Attribute26,cAttribute26,1000,'*',1,0)+
                        Decode(Attribute27,cAttribute27,1000,'*',1,0)+
                        Decode(Attribute28,cAttribute28,1000,'*',1,0)+
                        Decode(Attribute29,cAttribute29,1000,'*',1,0)+
                        Decode(Attribute30,cAttribute30,1000,'*',1,0) Total_Match,
                        Weighting_total
                        ,
                        cust_data_id,
                        rowid Row_id
                 from
                  (select
                      w.*
                    from xxcp_custom_data_v w
                   where w.category = cCategory
                     and pTransaction_Date Between nvl(w.EFFECTIVE_FROM_DATE,to_date('01-JAN-2000')) and nvl(w.EFFECTIVE_TO_DATE,to_date('31-DEC-2999'))
                     and ((w.ATTRIBUTE1 = cAttribute1 or cAttribute1 = '~' or w.Attribute1 = '*')
                     and (w.Attribute2 = cAttribute2 or cAttribute2 = '~' or w.Attribute2 = '*')
                     and (w.Attribute3 = cAttribute3 or cAttribute3 = '~' or w.Attribute3 = '*')
                     and (w.Attribute4 = cAttribute4 or cAttribute4 = '~' or w.Attribute4 = '*')
                     and (w.Attribute5 = cAttribute5 or cAttribute5 = '~' or w.Attribute5 = '*')
                     and (w.Attribute6 = cAttribute6 or cAttribute6 = '~' or w.Attribute6 = '*')
                     and (w.Attribute7 = cAttribute7 or cAttribute7 = '~' or w.Attribute7 = '*')
                     and (w.Attribute8 = cAttribute8 or cAttribute8 = '~' or w.Attribute8 = '*')
                     and (w.Attribute9 = cAttribute9 or cAttribute9 = '~' or w.Attribute9 = '*')
                     and (w.Attribute10 = cAttribute10 or cAttribute10 = '~' or w.Attribute10 = '*')
                     and (w.Attribute11 = cAttribute11 or cAttribute11 = '~' or w.Attribute11 = '*')
                     and (w.Attribute12 = cAttribute12 or cAttribute12 = '~' or w.Attribute12 = '*')
                     and (w.Attribute13 = cAttribute13 or cAttribute13 = '~' or w.Attribute13 = '*')
                     and (w.Attribute14 = cAttribute14 or cAttribute14 = '~' or w.Attribute14 = '*')
                     and (w.Attribute15 = cAttribute15 or cAttribute15 = '~' or w.Attribute15 = '*')
                     and (w.Attribute16 = cAttribute16 or cAttribute16 = '~' or w.Attribute16 = '*')
                     and (w.Attribute17 = cAttribute17 or cAttribute17 = '~' or w.Attribute17 = '*')
                     and (w.Attribute18 = cAttribute18 or cAttribute18 = '~' or w.Attribute18 = '*')
                     and (w.Attribute19 = cAttribute19 or cAttribute19 = '~' or w.Attribute19 = '*')
                     and (w.Attribute20 = cAttribute20 or cAttribute20 = '~' or w.Attribute20 = '*')
                     and (w.Attribute21 = cAttribute21 or cAttribute21 = '~' or w.Attribute21 = '*')
                     and (w.Attribute22 = cAttribute22 or cAttribute22 = '~' or w.Attribute22 = '*')
                     and (w.Attribute23 = cAttribute23 or cAttribute23 = '~' or w.Attribute23 = '*')
                     and (w.Attribute24 = cAttribute24 or cAttribute24 = '~' or w.Attribute24 = '*')
                     and (w.Attribute25 = cAttribute25 or cAttribute25 = '~' or w.Attribute25 = '*')
                     and (w.Attribute26 = cAttribute26 or cAttribute26 = '~' or w.Attribute26 = '*')
                     and (w.Attribute27 = cAttribute27 or cAttribute27 = '~' or w.Attribute27 = '*')
                     and (w.Attribute28 = cAttribute28 or cAttribute28 = '~' or w.Attribute28 = '*')
                     and (w.Attribute29 = cAttribute29 or cAttribute29 = '~' or w.Attribute29 = '*')
                     and (w.Attribute30 = cAttribute30 or cAttribute30 = '~' or w.Attribute30 = '*'))
                   )
                order by Total_Match desc,Weighting_Total desc;        -- If Tied order by Exact Match then weighting
                     

  Cursor c2 (pParent_Cust_Data_id IN NUMBER)
         is select attribute1,attribute2,attribute3,attribute4,attribute5,
                   attribute6,attribute7,attribute8,attribute9,attribute10,
                   attribute11,attribute12,attribute13,attribute14,attribute15,
                   attribute16,attribute17,attribute18,attribute19,attribute20,
                   attribute21,attribute22,attribute23,attribute24,attribute25,
                   attribute26,attribute27,attribute28,attribute29,attribute30
             from  xxcp_cust_data
             where parent_cust_data_id = pParent_Cust_Data_id; 
                  
  vTransaction_Date Date;
  vChildrenCount    number  := 0;
  vFoundTxt         varchar2(1) := 'N';
  vMsg              varchar2(4000);
  vFoundRowid       Rowid;

  begin

    vTransaction_Date := trunc(nvl(cTransaction_Date,xxcp_global.SystemDate));

    --cBestMatch_Record.Attribute1 := Null;

    For Rec in C1(pTransaction_date => vTransaction_Date)
      Loop
        vFoundTxt   := 'Y';
        vFoundRowid := Rec.Row_id;

        -- Get Children
        For c2_rec in c2(Rec.cust_data_id) loop
          vChildrenCount := vChildrenCount + 1;
          
          cChild_Tab(vChildrenCount).Attribute1  := c2_rec.attribute1;
          cChild_Tab(vChildrenCount).Attribute2  := c2_rec.attribute2;
          cChild_Tab(vChildrenCount).Attribute3  := c2_rec.attribute3;
          cChild_Tab(vChildrenCount).Attribute4  := c2_rec.attribute4;
          cChild_Tab(vChildrenCount).Attribute5  := c2_rec.attribute5;
          cChild_Tab(vChildrenCount).Attribute6  := c2_rec.attribute6;
          cChild_Tab(vChildrenCount).Attribute7  := c2_rec.attribute7;
          cChild_Tab(vChildrenCount).Attribute8  := c2_rec.attribute8;
          cChild_Tab(vChildrenCount).Attribute9  := c2_rec.attribute9;
          cChild_Tab(vChildrenCount).Attribute10 := c2_rec.attribute10;
          cChild_Tab(vChildrenCount).Attribute11 := c2_rec.attribute11;
          cChild_Tab(vChildrenCount).Attribute12 := c2_rec.attribute12;
          cChild_Tab(vChildrenCount).Attribute13 := c2_rec.attribute13;
          cChild_Tab(vChildrenCount).Attribute14 := c2_rec.attribute14;
          cChild_Tab(vChildrenCount).Attribute15 := c2_rec.attribute15;
          cChild_Tab(vChildrenCount).Attribute16 := c2_rec.attribute16;
          cChild_Tab(vChildrenCount).Attribute17 := c2_rec.attribute17;
          cChild_Tab(vChildrenCount).Attribute18 := c2_rec.attribute18;
          cChild_Tab(vChildrenCount).Attribute19 := c2_rec.attribute19;
          cChild_Tab(vChildrenCount).Attribute20 := c2_rec.attribute20;
          cChild_Tab(vChildrenCount).Attribute21 := c2_rec.attribute21;
          cChild_Tab(vChildrenCount).Attribute22 := c2_rec.attribute22;
          cChild_Tab(vChildrenCount).Attribute23 := c2_rec.attribute23;
          cChild_Tab(vChildrenCount).Attribute24 := c2_rec.attribute24;
          cChild_Tab(vChildrenCount).Attribute25 := c2_rec.attribute25;
          cChild_Tab(vChildrenCount).Attribute26 := c2_rec.attribute26;
          cChild_Tab(vChildrenCount).Attribute27 := c2_rec.attribute27;
          cChild_Tab(vChildrenCount).Attribute28 := c2_rec.attribute28;
          cChild_Tab(vChildrenCount).Attribute29 := c2_rec.attribute29;
          cChild_Tab(vChildrenCount).Attribute30 := c2_rec.attribute30;

        End loop;

        Exit;
    End Loop;

    If xxcp_global.Trace_on = 'Y' then

       vMsg :=  CustData_BestMatch_Trace(cCategory    => cCategory,
                                         cAttribute1  => cAttribute1,
                                         cAttribute2  => cAttribute2,
                                         cAttribute3  => cAttribute3,
                                         cAttribute4  => cAttribute4,
                                         cAttribute5  => cAttribute5,
                                         cAttribute6  => cAttribute6,
                                         cAttribute7  => cAttribute7,
                                         cAttribute8  => cAttribute8,
                                         cAttribute9  => cAttribute9,
                                         cAttribute10 => cAttribute10,
                                         cAttribute11 => cAttribute11,
                                         cAttribute12 => cAttribute12,
                                         cAttribute13 => cAttribute13,
                                         cAttribute14 => cAttribute14,
                                         cAttribute15 => cAttribute15,
                                         cAttribute16 => cAttribute16,
                                         cAttribute17 => cAttribute17,
                                         cAttribute18 => cAttribute18,
                                         cAttribute19 => cAttribute19,
                                         cAttribute20 => cAttribute20,
                                         cAttribute21 => cAttribute21,
                                         cAttribute22 => cAttribute22,
                                         cAttribute23 => cAttribute23,
                                         cAttribute24 => cAttribute24,
                                         cAttribute25 => cAttribute25,
                                         cAttribute26 => cAttribute26,
                                         cAttribute27 => cAttribute27,
                                         cAttribute28 => cAttribute28,
                                         cAttribute29 => cAttribute29,
                                         cAttribute30 => cAttribute30
                                        );

      xxcp_foundation.FndWriteError(100,'TRACE (CustomData_Weighting_Children): Category <'||cCategory||'> Level1 Found <'||vFoundTxt||'> Trx Date <'||cTransaction_Date||'> Level1 Rowid <'||vFoundRowid||'>', vMsg);

    End If;

    Return(vChildrenCount);

  end CustomData_Weighting_Children;
  
  
 -- *******************************************************************************
 --            Clear_Diag
 -- *******************************************************************************
 Procedure Clear_Diag(cDiag_num pls_integer default 1) is 
  begin
     xxcp_global.Clear_Diagnostics(cDiag_num => cDiag_num);     
  end Clear_Diag;

 -- *******************************************************************************
 --            Diag
 -- *******************************************************************************
 Procedure Diag(cText in varchar2 default null,cDiag_num pls_integer default 1) is
    vResult varchar2(512);
  begin
     vResult := xxcp_global.Add_Diagnostics(cDiagnostics_code => cText, cDiag_num => cDiag_num);
  end Diag;
  
 -- *******************************************************************************
 --            Diag
 -- *******************************************************************************
 Function Diag(cText in varchar2 default null,cDiag_num pls_integer default 1) return varchar2 is
    vResult varchar2(512);
  begin
     Return(xxcp_global.Add_Diagnostics(cDiagnostics_code => cText, cDiag_num => cDiag_num));
 End Diag;

 -- *******************************************************************************
 --            Diag
 -- *******************************************************************************
 Function Diag(cDiag_num pls_integer default 1) return varchar2 is
    vResult varchar2(512);
  begin
     Return(xxcp_global.Add_Diagnostics(cDiagnostics_code => Null, cDiag_num => cDiag_num));
 End Diag;
 
 --
 -- Company Number
 --
 Function Company_Number(cTax_Reg_id in number) return varchar2 is
 
   cursor tc(pTax_Reg_id in number) is
     select c.company_number
       from XXCP_tax_registrations c
     where c.tax_registration_id = pTax_Reg_id;
 
    vCompany_Number XXCP_tax_registrations.company_number%type;
 
   Begin
      For Rec in tc(pTax_Reg_id => cTax_Reg_id) Loop
        vCompany_Number := Rec.Company_Number;
      End Loop;
      Return(vCompany_Number);
   End Company_Number;
 --
 -- Tax_Reg_Code
 --
 Function Tax_Reg_Code(cTax_Reg_id in number) return varchar2 is
 
   cursor tc(pTax_Reg_id in number) is
     select c.short_code
       from XXCP_tax_registrations c
     where c.tax_registration_id = pTax_Reg_id;
 
    vTax_Reg_Code XXCP_tax_registrations.short_code%type;
 
   Begin
      For Rec in tc(pTax_Reg_id => cTax_Reg_id) Loop
        vTax_Reg_Code := Rec.Short_Code;
      End Loop;
      Return(vTax_Reg_Code);
   End Tax_Reg_Code;
 
 --
 -- Get_Internal_Conversion_Type
 --  
 Function Get_Internal_Conversion_Type(cUser_Conversion_Type in varchar2) Return varchar2 is  
   
  cursor c1 (pUser_Conversion_Type in varchar2) is 
   select Conversion_Type
   from gl_daily_conversion_types f
   where f.user_conversion_type = pUser_Conversion_Type;
   
   vUser_Conversion_Type gl_daily_conversion_types.conversion_type%type := 'ERROR';
   
 begin
 
    For Rec in c1(cUser_Conversion_Type) Loop
      vUser_Conversion_Type := Rec.Conversion_Type;
    End Loop;
    
    Return(vUser_Conversion_Type);
 
 End Get_Internal_Conversion_Type;
 
 -- Set_Long_Reference
 --
 Procedure Set_Long_Reference(cLong_Reference_Number in number, cLong_Data varchar2) is
 Begin
   If cLong_Reference_Number between 1 and xxcp_wks.gLongRef_Count then
     xxcp_wks.L1001_Array(cLong_Reference_Number) := cLong_Data; 
   End If;
 End Set_Long_Reference;
 
 --
 -- Set_RT_Interface
 --
 Procedure Set_RT_Interface(cInterface_id in Number, cColumn_Name in Varchar2, cColumn_Value in Varchar2, cInternalErrorCode out Number) is
 -- Realtime engine only
 
   vUpdate_Command    Varchar2(2000);
   vInternalErrorCode Number(6) := 0;  
 
 Begin
   
    If NOT upper(cColumn_Name) in(
            'VT_CREATED_DATE',
            'VT_DATE_PROCESSED',
            'VT_INTERFACE_ID',
            'VT_PREVIEW_ID',
            'VT_PARENT_TRX_ID',
            'VT_REQUEST_ID',
            'VT_SOURCE_ASSIGNMENT_ID',
            'VT_STATUS',
            'VT_STATUS_CODE',
            'VT_TRANSACTION_CLASS',
            'VT_TRANSACTION_DATE',
            'VT_TRANSACTION_ID',
            'VT_TRANSACTION_REF',
            'VT_TRANSACTION_TABLE',
            'VT_TRANSACTION_TYPE') 
       And cInterface_id > 0 
       And cColumn_Name is not Null
     Then
       
       If nvl(xxcp_global.Preview_on,'N') = 'Y' then
         
         vUpdate_Command := 'Update XXCP_PV_INTERFACE p
                                Set '||cColumn_Name||' = :1 
                              where p.vt_request_id   = :2
                                and p.vt_interface_id = :3
                                and p.VT_PREVIEW_ID   = :4 ';
         Begin       
            Execute Immediate vUpdate_Command 
              using cColumn_Value,
                    xxcp_global.gCommon(1).Current_Request_id,
                    cInterface_id,
                    xxcp_global.User_id;

            Exception when others then
              vInternalErrorCode := 10518;
              xxcp_foundation.FndWriteError(10518,'Update failed in XXCP_GWU.Set_RT_Interface for '||to_char(cInterface_id)||' '||SQLERRM,vUpdate_Command);
         End;

       Else

         vUpdate_Command := 'Update XXCP_RT_INTERFACE p
                                Set '||cColumn_Name||' = :1 
                              where p.vt_request_id   = :2
                                and p.vt_interface_id = :3 ';
          
         Begin       
           Execute Immediate vUpdate_Command 
              using cColumn_Value,
                    xxcp_global.gCommon(1).Current_Request_id,
                    cInterface_id;

            Exception when others then
              vInternalErrorCode := 10518;
              xxcp_foundation.FndWriteError(10518,'Update failed in XXCP_GWU.Set_RT_Interface for '||to_char(cInterface_id)||' '||SQLERRM,vUpdate_Command);
         End;

       End If;
       
     Else
       vInternalErrorCode := 10519;
       If xxcp_global.Trace_on = 'Y' then
         xxcp_foundation.fndwriteerror(10519,'XXCP_GWU: Invalid Set_RT_Interface command for '||to_char(cInterface_id),vUpdate_Command);
       End If;
     End If;
     
     cInternalErrorCode := vInternalErrorCode;
 End Set_RT_Interface;
  
  
  -- 02.06.11
  Function Get_ExplosionTrxId Return Number is
      begin    
        return (xxcp_global.gCOMMON(1).Explosion_Transaction_id);
      End Get_ExplosionTrxId;

  Function Get_ExplosionRowid Return Rowid is
      begin
        return (xxcp_global.gCOMMON(1).Explosion_Rowid);
      End Get_ExplosionRowid;

  Function Get_InterfaceRowid Return Rowid is
     Begin
        Return (xxcp_global.gCommon(1).preview_source_rowid);
     End Get_InterfaceRowid;
   
  -- Get_Custom_Line_Number
  Function Get_Custom_Line_Number Return Number is
    
    j               number := xxcp_wks.gLineCount.Count;
    fe              number := 0;
    w               number := 0;

    vLine_Number    number := 0;
    
    Begin
      
      fe := xxcp_global.gCommon(1).Record_Number;  -- Fred

      If fe between 1 and xxcp_wks.WORKING_RCD.count then
        
        If xxcp_wks.working_rcd(fe).Line_Number_Assigned = 0 then

          If j > 0 then
            For x in 1..j loop
              If xxcp_wks.gLineCount(x).Target_Table  = xxcp_wks.working_rcd(fe).Target_Table and
                 xxcp_wks.gLineCount(x).Trading_Set_id = xxcp_wks.working_rcd(fe).Trading_Set_id and
                 xxcp_wks.gLineCount(x).Target_Instance_id = xxcp_wks.working_rcd(fe).Target_Instance_id then
                --
                xxcp_wks.gLineCount(x).Line_Number := xxcp_wks.gLineCount(x).Line_Number +1;
                vLine_Number := xxcp_wks.gLineCount(x).Line_Number;
                xxcp_wks.working_rcd(fe).Line_Number_Assigned :=  vLine_Number;
                --
                Exit;
              End If; 
            End Loop;
          End If;
          
          If vLine_Number = 0 then
            -- create base entry
            w := xxcp_wks.gLineCount.Count + 1;
            xxcp_wks.gLineCount(w).Target_Table       := xxcp_wks.working_rcd(fe).Target_Table;
            xxcp_wks.gLineCount(w).Trading_Set_id     := xxcp_wks.working_rcd(fe).Trading_Set_id;
            xxcp_wks.gLineCount(w).Target_Instance_id := xxcp_wks.working_rcd(fe).Target_Instance_id;
            xxcp_wks.gLineCount(w).Line_Number    := 1;
            xxcp_wks.working_rcd(fe).Line_Number_Assigned :=  1;   
            vLine_Number := 1;
          End If;
        Else -- return current assigned number
         vLine_Number := xxcp_wks.working_rcd(fe).Line_Number_Assigned;
        End If;
      End If;

      Return(vLine_Number);
    
    End Get_Custom_Line_Number;
  
  -- *******************************************************************************
  -- After_Assignment_User_Confirm
  -- *******************************************************************************
  Procedure After_Assignment_User_Confirm is
  Begin
    commit;  
  End After_Assignment_User_Confirm;  
    
  -- *************************************************************
  --                           IsNumber
  -- *************************************************************
	Function IsNumber(cStr in varchar2) return varchar2 is
	  vResult Char := 'N';
		vNum    Number;

	 Begin
	    Begin
		    vNum := to_number(cStr);
				vResult := 'Y';
			  Exception when OTHERS then vResult := 'N';
			End;
		  Return(vResult);
	 End IsNumber;  
   
  -- *************************************************************
  --                           Preview_Mode
  -- *************************************************************
   Function Preview_Mode return varchar2 is
   Begin
      Return(nvl(xxcp_global.Preview_on,'N'));  
   End Preview_Mode;

  -- *************************************************************
  --                           Trading_Set
  -- *************************************************************
	Function Trading_Set(cTrading_Set_id in Number) Return varchar2 is
    vResult   Varchar2(100);

    Cursor c1(pTrading_Set_id in number) is
     Select s.Trading_Set
       from xxcp_trading_sets s
     where s.trading_set_id = pTrading_Set_id;

   Begin
	   For Rec in c1(cTrading_Set_id) loop
	     vResult := Rec.Trading_Set;
	   End Loop;
     Return (vResult);
	End Trading_Set;
  
  -- *************************************************************
  --                   Get_Before_Insert_Value
  -- *************************************************************
  Function Get_Before_Insert_Value(cColumn_Name in varchar2) Return varchar2 is

    vResult varchar2(100);    
    fe      number := nvl(xxcp_global.gCommon(1).record_number,0);

  Begin
    
     If fe > 0 then  
       If cColumn_Name = 'TARGET_TABLE' then
         vResult := xxcp_wks.WORKING_RCD(fe).Target_Table;  
       ElsIf cColumn_Name = 'TARGET_INSTANCE_ID' then
         vResult := xxcp_wks.WORKING_RCD(fe).Target_Instance_id;         
       ElsIf cColumn_Name = 'RECORD_NUMBER' then
         vResult := to_char(fe);      
       End if;       
     End If;
     
     Return(vResult);
  
  End Get_Before_Insert_Value;
  
 -- *******************************************************************************
 --  Set_Invoice_Paid_Flag
 -- *******************************************************************************
 Procedure Set_Invoice_Paid_Flag(cInvoice_Number in varchar2, cInvoice_Type_Code in varchar2, cPayment_Reference in varchar2 default null) is
    
  Begin
    
      xxcp_ic_invoice_numbering.Set_Invoice_Paid_Flag(cInvoice_Number    => cInvoice_Number,
                                                      cInvoice_Type_Code => cInvoice_Type_Code,
                                                      cPayment_Reference => cPayment_Reference);
    
  End Set_Invoice_Paid_Flag;   
  
  --
  -- Tax Rate
  -- 
  Function Element_Tax_Rate(cElement_Number in varchar2) return number is
     
   k       number;
   vResult number := Null;
   
  Begin
    If xxcp_te_base.gTax_Element_Set.count > 0 then        
       For k in 1..xxcp_te_base.gTax_Element_Set.count loop
         If xxcp_te_base.gTax_Element_Set(k).Tax_element_number = cElement_Number then
           vResult := xxcp_te_base.gTax_Element_Set(k).tax_rate;
           Exit;
         End If; 
       End Loop;
    End If;    
    Return(vResult);
  End Element_Tax_Rate;

   -- Tax Amount
  Function Element_Tax_Amount(cElement_Number in varchar2) return number is
     
    k       number;
    vResult number := Null;
   
  Begin
    If xxcp_te_base.gTax_Element_Set.count > 0 then
      For k in 1..xxcp_te_base.gTax_Element_Set.count loop
        if xxcp_te_base.gTax_Element_Set(k).Tax_element_number = cElement_Number then
          vResult :=xxcp_te_base.gTax_Element_Set(k).tax_amount;
          Exit;
        End If; 
     
      End Loop;
    End If;        
    Return(vResult);
  End Element_Tax_Amount;
   
  -- Tax_Rate_Code
  Function Element_Tax_Code(cElement_Number in varchar2) return varchar2 is
     
    k       number;
    vResult varchar2(500) := Null;
   
  Begin
       
    If xxcp_te_base.gTax_Element_Set.count > 0 then
        
      For k in 1..xxcp_te_base.gTax_Element_Set.count loop
        if xxcp_te_base.gTax_Element_Set(k).Tax_element_number = cElement_Number then
          vResult := xxcp_te_base.gTax_Element_Set(k).Tax_Rate_Code;
          Exit;
        End If; 
     
      End Loop;
    End If;
        
    Return(vResult);
   End Element_Tax_Code;

  -- Element_Tax_Descr
  Function Element_Tax_Descr(cElement_Number in varchar2) return varchar2 is
     
    k       number;
    vResult varchar2(500) := Null;
   
  Begin
       
    If xxcp_te_base.gTax_Element_Set.count > 0 then
        
      For k in 1..xxcp_te_base.gTax_Element_Set.count loop
        if xxcp_te_base.gTax_Element_Set(k).Tax_element_number = cElement_Number then
          vResult := xxcp_te_base.gTax_Element_Set(k).Imposition;
          Exit;
        End If; 
     
      End Loop;
    End If;
        
    Return(vResult);
   End Element_Tax_Descr;
 -- *******************************************************************************
 --
 -- Init
 --
 -- *******************************************************************************
 Procedure Init is
 Begin
   -- Used to cache the System Date the first time this is called per session
   gSysdate       := Sysdate;
   gTruncSysdate  := Trunc(Sysdate);
   gLSV_Source_id := 0;
 End Init;


 BEGIN
    Init;
 END XXCP_GATEWAY_UTILS;
/
