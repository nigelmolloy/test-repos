CREATE OR REPLACE PACKAGE BODY XXCP_GL_ENGINE AS
  /******************************************************************************
                          V I R T U A L  T R A D E R  L I M I T E D
              (Copyright 2001-2012 Virtual Trader Ltd. All rights Reserved)

     NAME:       XXCP_GL_ENGINE
     PURPOSE:    GL Engine and Inventory GL Engine

     REVISIONS:
     Ver        Date      Author  Description
     ---------  --------  ------- ------------------------------------
     02.01.00   28/06/02  Keith   First Coding
     02.01.01   07/01/04  Keith   Restructure for VT 2.1
     02.01.02   20/05/04  Keith   transaction Stamp by vt_transaction_group
     02.01.03   28/05/04  Keith   Fixed Rounding Amount Issue
     02.01.05   29/09/04  Keith   Added debug on Entered DR, CR Not matching
     02.01.07   20/10/04  Keith   Added debug on Entered DR, CR Not matching
     02.01.08   29/01/04  Keith   Included Inventory logic to reduce maintenance
     02.02.00   01/02/05  Keith   Timing Logic added to help develope quick transactions
     02.02.01   07/02/05  Keith   Adding Inventory Logic s
     02.02.02   14/02/05  Keith   Fixed Error check logic
     02.02.05   12/05/05  Keith   Moved Rounding out to TE BASE
     02.02.06   03/10/05  Keith   Added Unposted logic. Optional
     02.02.07   20/10/05  Keith   Added Summaries
     02.02.08   27/12/05  Keith   Exit and Rollback if out of disk space (3550)
     02.03.00   09/01/06  Keith   Added GROUPING
     02.03.01   08/02/06  Keith   Moved .init upwards
     02.03.02   18/04/06  Keith   Now using Engine_Init
     02.03.03   22/04/06  Keith   Changing timing report to work per source assignment,
                                  not just at group level.
     02.04.00   03/01/07  Keith   Table_Group_id added to Trx Stamp
     02.04.01   17/04/07  Keith   Added Decode for class_transaction_id
     02.04.02   11/05/07  Keith   Changed After_Processing Paramaters
     02.04.03   14/05/07  Keith   Distribution must have null in request_id
     02.04.04   17/07/07  Keith   Group_id nvl.
     02.05.01   15/09/08  Keith   Changed First_Rows to All_Rows
     02.05.02   30/03/09  Nigel   Added duplicate check. 
     02.05.03   18/05/09  Nigel   vCurrent_parent_trx_id not being reset for each source_assignment     
     02.06.01   29/09/09  Simon   Improved Pass Through Logic.
     02.06.02   14/10/09  Simon   Grouping now done by transaction_table not assignment_id..
     02.06.03   28/12/09  Keith   Added calc_legal_exch_rate 
     02.06.04   16/05/11  Simon   Added Reset_HAC_Tables and Reset_Parent_Trx.
                                  Fixed Apply_Grouping bug.
     02.06.05SW 14/06/11  Keith   Added Waiting to Reset_HAC_Tables
     02.06.06   04/07/11  Keith   Added Partition Dates to interface tables
     02.06.07   19/08/11  Simon   Sys Profile added for Prev Request Range. Id 642.(Merged sw10c).
     02.06.08   19/08/11  Simon   Fix gEnd_Partition_Date.
     02.06.09   16/01/12  Nigel   Added call to export file directly after engine
     03.06.10   16/01/12  Keith   Fixed Ignore status
     03.06.11   06/02/12  Mark    Call invoice numbering package where switched on by source.
     03.06.12   10/02/12  Mark    Make OOD compliant
     03.06.13   09/03/12  Nigel   Added gPartition_Months_end system profile. 
     03.06.14   12/03/12  Simon   Add facility to optionally end in warning/error (OOD-33). 
     03.06.15   15/03/12  Nigel   Flat File ditribution only gets called when active (OOD-47).
     03.06.16   25/05/12  Keith   Multiple processing streams  
     03.06.17   31/05/12  Keith   Added Apply_Exclude_Rule and 11090 issue
     03.06.18   27/06/12  Nigel   ICS interface changes.
     03.06.19   03/07/12  Nigel   Split out ICS DML packages to one per perview_ctl
     03.06.20   05/07/12  Simon   gTrans_Table_Array moved to xxcp_global package.
     03.06.21   10/07/12  Nigel   Changed the integrity check, and removed check for classification
     03.06.22   09/08/12  Keith   Dynamic Explosion added
     03.06.23   16/10/12  Mat     OOD-358 PPG Merge - Improve Transaction_Group_Stamp performance.
                                  Remove Er2 and Er3 from Set_Running_Status.
                                  Add RUNNING STATUS FREQUENCY system profile. 
     03.06.24   30/10/12  Mat     Dynamic Explosion issue (OOD-249).
  *************************************************************************************************/

  -- Editor Setting: 2 space tab stop

  gEngine_Trace               varchar2(1);
  gRequest_Id_Range           NUMBER;

  gPartition_Months_start     number := -6; -- This should be a negative number
  gPartition_Months_end       number := 1;  -- This should be a positive number
  
  gStart_Partition_Date       date;
  gEnd_Partition_Date         date := Sysdate;

  gIgnored_Removal_Flag       Varchar2(1) := 'Y';
  gIgnored_Delete_Months      Number := -12; -- This should be a negative number
  gIgnored_Start_Removal_Date date;
  gIgnored_End_Removal_Date   date := trunc(Add_Months(Sysdate,gIgnored_Delete_Months));

  vTiming               xxcp_global.gTiming_rec;
  vTimingCnt            PLS_INTEGER;

  gBalancing_Array      XXCP_DYNAMIC_ARRAY := XXCP_DYNAMIC_ARRAY(' ',' ',' ',' ',' ',' ',' ',' ',' ',' ');
  gTrans_Table_Array    XXCP_DYNAMIC_ARRAY := XXCP_DYNAMIC_ARRAY();  
  
  vIC_Numbering_Flag    Varchar2(1);
  -- 03.06.15
  vFlat_File_Flag       Varchar2(1);
  
  gEngine_Error_Found   boolean := FALSE;
  gForce_Job_Error      boolean := FALSE;
  gForce_Job_Warning    boolean := FALSE;
  
  gOracle_Check_Count   Number(4) := 1000;
  gAssignment_Commit    Number(4) := 100;
  gTransaction_Commit   Number(4) := 10;
  gRunning_Status_Count Number(6) := 30000; -- to keep rollback space small
  
  -- *************************************************************
  --                     Software Version Control
  -- This package can be checked with SQL to ensure it's installed
  -- select packagename.software_version from dual;
  -- *************************************************************
  FUNCTION Software_Version RETURN VARCHAR2 IS
  BEGIN
    RETURN('Version [03.06.24] Build Date [30-OCT-2012] Name [XXCP_GL_ENGINE]');
  END Software_Version;

  -- *************************************************************
  --                  ClearWorkingStorage
  -- *************************************************************
  PROCEDURE ClearWorkingStorage IS

  BEGIN
    -- Processed Records
    xxcp_wks.WORKING_CNT := 0;
    xxcp_wks.WORKING_RCD.DELETE;
    -- Current Records before processing
    xxcp_wks.SOURCE_CNT := 0;
    xxcp_wks.SOURCE_ROWID.DELETE;
    xxcp_wks.SOURCE_STATUS.DELETE;
    xxcp_wks.SOURCE_SUB_CODE.DELETE;
    -- Balancing
    xxcp_wks.BALANCING_CNT := 0;
    xxcp_wks.BALANCING_RCD.DELETE;
     
  END ClearWorkingStorage;

  --  !! ***********************************************************************************************************
  --  !!   Stop_Whole_Transaction
  --  !! ***********************************************************************************************************
  PROCEDURE Stop_Whole_Transaction is
  Begin
   -- Set all transactions in group to error status
   Begin
     Update xxcp_gl_interface
      set vt_status              = 'WAITING'
         ,vt_internal_Error_code = 0 -- Associated errors 
         ,VT_DATE_PROCESSED  = xxcp_global.SystemDate
       WHERE vt_request_id = xxcp_global.gCommon(1).Current_request_id
         AND vt_Source_Assignment_id = xxcp_global.gCommon(1).current_assignment_id
         AND vt_parent_trx_id = xxcp_global.gCommon(1).current_parent_Trx_id
         AND NOT vt_status IN ('TRASH', 'IGNORED');
     End;
     -- Update Record that actually caused the problem.
     Begin
     Update xxcp_gl_interface
        set vt_status          = 'WAITING'
           ,VT_DATE_PROCESSED  = sysdate 
       WHERE ROWID = xxcp_global.gCommon(1).current_source_rowid
         AND vt_request_id = xxcp_global.gCommon(1).current_request_id;
     End;

    ClearWorkingStorage;

  End Stop_Whole_Transaction;
  --  !! ***********************************************************************************************************
  --  !!                                   Error_Whole_Transaction
  --  !!                If one part of the wrapper finds a problem whilst processing a record,
  --  !!                          then we need to error the whole transaction.
  --  !! ***********************************************************************************************************
  PROCEDURE Error_Whole_Transaction(cInternalErrorCode IN OUT NUMBER) IS
  BEGIN

    -- Flag Error
    gEngine_Error_Found := TRUE;  

    -- Set all transactions in group to error status
    BEGIN
      UPDATE xxcp_gl_interface
         SET vt_status = 'ERROR', vt_internal_Error_code = 12823 -- Associated errors
       WHERE vt_request_id = xxcp_global.gCommon(1).Current_request_id
         AND vt_Source_Assignment_id = xxcp_global.gCommon(1).current_assignment_id
         AND vt_parent_trx_id = xxcp_global.gCommon(1).current_parent_Trx_id
         AND NOT vt_status IN ('TRASH', 'IGNORED');
    END;
    -- Update Record that actually caused the problem.
    BEGIN
      UPDATE xxcp_gl_interface
         SET vt_status              = 'ERROR',
             vt_internal_error_code = cInternalErrorCode,
             vt_date_processed      = xxcp_global.SystemDate
       WHERE ROWID = xxcp_global.gCommon(1).current_source_rowid
         AND vt_request_id = xxcp_global.gCommon(1).current_request_id;
    END;
  
    IF xxcp_global.gCommon(1).Custom_events = 'Y' THEN
    
      xxcp_custom_events.ON_TRANSACTION_ERROR(xxcp_global.gCommon(1).Source_id,
                                              xxcp_global.gCommon(1).current_assignment_id,
                                              xxcp_global.gCommon(1).current_source_rowid,
                                              xxcp_global.gCommon(1).current_Transaction_Table,
                                              xxcp_global.gCommon(1).current_Parent_Trx_id,
                                              xxcp_global.gCommon(1).current_transaction_id,
                                              cInternalErrorCode);
    END IF;
  
    ClearWorkingStorage;
  
  END Error_Whole_Transaction;

  --
  --   !! ***********************************************************************************************************
  --   !!                                     No_Action_Transaction
  --   !!                                If there are no records to process.
  --   !! ***********************************************************************************************************
  --
  PROCEDURE No_Action_Transaction(cSource_Rowid IN ROWID) IS
  BEGIN
    -- Update Record that actually had not action required
    BEGIN
      UPDATE xxcp_gl_interface
         SET vt_status              = 'SUCCESSFUL',
             vt_internal_error_code = NULL,
             vt_date_processed      = xxcp_global.SystemDate,
             vt_status_code         = 7003
       WHERE ROWID = cSource_rowid;
    END;
  END NO_Action_Transaction;
 --   !! ***********************************************************************************************************
 --   !!                                     Pass_Through_Whole_Trx 
 --   !!                                If there are no records to process.
 --   !! ***********************************************************************************************************
 Procedure Pass_Through_Whole_Trx(cSource_Rowid in Rowid, cPassThroughCode in number) is

  vStatus xxcp_gl_interface.vt_status%type;
 
 Begin
 
   If cPassThroughCode between 7021 AND 7024 then
     vStatus := 'IGNORED';
   Else
     vStatus := 'PASSTHROUGH';
   End If;
 
   If cPassThroughCode in (7022, 7026) then -- Parent level   
     xxcp_global.gPassThroughCode := cPassThroughCode;
   End if;
      
   Update xxcp_gl_interface p
      set p.vt_status               = vStatus
         ,p.vt_internal_Error_code  = Null
         ,p.vt_status_code          = cPassThroughCode
         ,p.vt_date_processed       = xxcp_global.SystemDate
    where p.rowid         = cSource_Rowid
      and p.vt_request_id = xxcp_global.gCommon(1).Current_request_id;
   
 End Pass_Through_Whole_Trx;

  --  !! ***********************************************************************************************************
  --  !!                                   Transaction_Group_Stamp
  --  !!                 Stamp Incomming record with parent Trx to create groups of
  --  ||                    transactions that resemble on document transaction.
  --  !! ***********************************************************************************************************
 PROCEDURE Transaction_Group_Stamp(cSource_assignment_id IN NUMBER,
                                    cTable_Group_id       IN NUMBER, 
                                    cRequest_id           IN NUMBER,
                                    cInternalErrorCode    OUT NUMBER) IS
  
    vInternalErrorCode NUMBER(8) := 0;
    vStatement         VARCHAR2(6000);

    -- Distinct list of Transaction Tables
    -- populated in Set_Running_Status
    CURSOR cTrxTbl is
      SELECT transaction_table,
             nvl(grouping_rule_id,0) grouping_rule_id
      FROM   xxcp_sys_source_tables
      where  source_id = xxcp_global.gCommon(1).Source_id
      and    transaction_table in (select * from TABLE(CAST(xxcp_global.gTrans_Table_Array AS XXCP_DYNAMIC_ARRAY)));

   
    CURSOR Stmp(pSource_assignment_id IN NUMBER, 
                pTable_Group_id       IN NUMBER,
                pRequest_id           IN NUMBER,
                pTransaction_Table    IN VARCHAR2) IS
      SELECT DISTINCT 'update xxcp_gl_interface ' ||
                      '  set vt_parent_trx_id = ' || t.Parent_trx_column || ' ' ||
                      'where vt_status = :1 ' ||
                      '  and vt_source_assignment_id = :2 ' ||
                      '  and vt_transaction_table = :3 ' ||
                      '  and vt_request_id = :4 ' Statement_line,
                      t.Parent_trx_column,
                      t.Transaction_Table
        FROM xxcp_gl_interface       g,
             xxcp_sys_source_tables  t,
             xxcp_source_assignments x
       WHERE g.vt_request_id           = pRequest_id 
         AND g.vt_status               = 'GROUPING'
         AND g.vt_transaction_table    = t.transaction_table
         AND t.table_group_id          = pTable_group_id
         AND t.source_id               = x.source_id
         AND x.source_assignment_id    = pSource_assignment_id
         AND g.vt_source_assignment_id = x.source_assignment_id
         and g.vt_transaction_table    = pTransaction_Table;     -- 02.06.02
  
  BEGIN

    vTiming(vTimingCnt).Group_Stamp_Start := TO_CHAR(SYSDATE, 'HH24:MI:SS');

    For cTblRec in cTrxTbl Loop  -- Loop for all Transaction Tables in Array within SA.
      
      If cTblRec.Grouping_Rule_Id > 0 THEN  --02.06.02
        --
        -- Apply Grouping Rules
        --
        XXCP_DYNAMIC_SQL.Apply_Grouping_Rules(cNew_Status        => 'GROUPING', 
                                              cInternalErrorCode => vInternalErrorCode,
                                              cTransaction_Table => cTblRec.Transaction_Table);
      
        -- Error Checking for Null Parent Trx id
        
          UPDATE xxcp_gl_interface l
             SET vt_status                = 'ERROR',
                 vt_parent_trx_id         = NULL,
                 l.vt_internal_error_code = 11090
           WHERE l.vt_request_id        = cRequest_id
             AND l.vt_source_assignment_id = cSource_assignment_id
             AND l.vt_status               = 'GROUPING'
             AND l.vt_transaction_table    = cTblRec.transaction_table
             AND l.vt_parent_trx_id IS NULL;  
          
          Commit;        
        
      Else 

        --
        -- Mass update of Parent Trx id
        --
        FOR rec IN Stmp(pSource_assignment_id => cSource_assignment_id, 
                        pRequest_id           => cRequest_id,
                        pTable_Group_id       => cTable_Group_id,
                        pTransaction_Table    => cTblRec.transaction_table) LOOP
          IF rec.Parent_trx_column IS NULL THEN
            vInternalErrorCode := 10665;
            xxcp_foundation.FndWriteError(vInternalErrorCode,'Transaction Table <' || Rec.Transaction_Table || '>');
          ELSE
            vStatement := rec.Statement_line;
            BEGIN

              EXECUTE IMMEDIATE vStatement
                USING 'GROUPING', cSource_Assignment_id, rec.transaction_table, xxcp_global.gCommon(1).current_request_id;
            
            EXCEPTION
              WHEN OTHERS THEN
                vInternalErrorCode := 10667;
                xxcp_foundation.FndWriteError(vInternalErrorCode, SQLERRM,vStatement);
            END;
          END IF;
        END LOOP;

      End if;

    End Loop;
  
    cInternalErrorCode := vInternalErrorCode;
  
  END Transaction_Group_Stamp;


  -- !! ***********************************************************************************************************
  -- !!                                   FlushRecordsToTarget
  -- !!     Control The process of moving the Array elements throught to creating the new output records
  -- !! ***********************************************************************************************************
  FUNCTION FlushRecordsToTarget(cGlobal_Rounding_Tolerance IN NUMBER,
                                cGlobalPrecision           IN NUMBER,
                                cTransaction_Type          IN VARCHAR2,
                                cInternalErrorCode         IN OUT NUMBER)
    RETURN NUMBER IS
  
    vCnt           PLS_INTEGER := 0;
    c              PLS_INTEGER := 0;
  
    vEntered_DR    NUMBER := 0;
    vEntered_CR    NUMBER := 0;
    j              PLS_INTEGER := 0;
    fe             PLS_INTEGER := xxcp_wks.WORKING_CNT;
  
    vRC            PLS_INTEGER := xxcp_wks.SOURCE_CNT;
    vRC_Records    PLS_INTEGER := 0;
    vRC_Error      PLS_INTEGER := 0;
  
    vStatus        xxcp_gl_interface.vt_status%TYPE;
    vErrorMessage  VARCHAR2(3000);
    vZero_Action   PLS_INTEGER := 0;
  
    UDI_Cnt        PLS_INTEGER := 0;
    
    -- 03.06.18
    vICS_Cnt       PLS_INTEGER := 0;    

  BEGIN

    IF fe > 0 THEN
      xxcp_te_base.Account_Rounding(cGlobal_Rounding_Tolerance,
                                    cGlobalPrecision,
                                    cTransaction_Type,
                                    vEntered_DR,
                                    vEntered_CR,
                                    cInternalErrorCode);
    END IF;
  
    -- Check that for each attribute record we have a records for output.
    FOR c IN 1 .. vRC LOOP
      vRC_Error := 0;
      FOR j IN 1 .. fe LOOP
        IF xxcp_wks.Source_rowid(c) = xxcp_wks.WORKING_RCD(j).Source_rowid THEN
          xxcp_wks.WORKING_RCD(j).Source_Pos := c;
          xxcp_wks.SOURCE_STATUS(c) := 'FLUSH';
          vRC_Error   := 1;
          vRC_Records := vRC_Records + 1;
        END IF;
      END LOOP;
      IF vRC_Error = 0 THEN
        xxcp_wks.SOURCE_SUB_CODE(c) := 7003;
        xxcp_wks.SOURCE_STATUS(c) := 'NO RULES';
        No_Action_Transaction(xxcp_wks.Source_rowid(c));
      END IF;
    END LOOP;
   
    SAVEPOINT TARGET_INSERT;
  
    IF cInternalErrorCode = 0 AND vRC_Records > 0 THEN

      FOR j IN 1 .. fe LOOP
      
        EXIT WHEN cInternalErrorCode <> 0;

        vCnt := vCnt + 1;
      
        -- Custom Events 
        IF xxcp_global.gCommon(1).Custom_events = 'Y' THEN
          xxcp_te_base.Custom_Event_Insert_Row(j,cInternalErrorCode);
        END IF;

        -- Tag Zero Account Value rows
        IF cInternalErrorCode = 0 THEN

          xxcp_global.gCommon(1).current_source_rowid := xxcp_wks.Source_rowid(xxcp_wks.WORKING_RCD(j).Source_Pos);

          -- Find the Action to take for Zero Accounted values
          vZero_Action := xxcp_te_base.Zero_Suppression_Rule(j);


          IF vZero_Action > 0 THEN

               SELECT xxcp_process_history_seq.NEXTVAL INTO xxcp_wks.WORKING_RCD(j) .Process_History_id FROM dual;
               -- don't post ZAV

                 IF vZero_Action = 3 THEN

                    XXCP_PROCESS_DML_GL.Process_Oracle_Insert(
                                                      cSource_Rowid        => xxcp_wks.WORKING_RCD(j).Source_rowid,
                                                      cParent_Rowid        => NULL,
                                                      cTarget_Table        => xxcp_wks.WORKING_RCD(j).Target_Table,
                                                      cTarget_Instance_id  => xxcp_wks.WORKING_RCD(j).Target_Instance_id,
                                                      cProcess_History_id  => xxcp_wks.WORKING_RCD(j).Process_History_id,
                                                      cRule_id             => xxcp_wks.WORKING_RCD(j).Rule_id,
                                                      cD1001_Array         => xxcp_wks.WORKING_RCD(j).D1001_Array,
                                                      cD1002_Array         => xxcp_wks.WORKING_RCD(j).D1002_Array,
                                                      cD1003_Array         => xxcp_wks.WORKING_RCD(j).D1003_Array,
                                                      cD1004_Array         => xxcp_wks.WORKING_RCD(j).D1004_Array,
                                                      cD1005_Array         => xxcp_wks.WORKING_RCD(j).D1005_Array,
                                                      cD1006_Array         => xxcp_wks.WORKING_RCD(j).D1006_Array,
                                                      cI1009_Array         => xxcp_wks.WORKING_RCD(j).I1009_Array,
                                                      cErrorMessage        => vErrorMessage,
                                                      cInternalErrorCode   => cInternalErrorCode);
                                                      
                 END IF;
                 
                 IF cInternalErrorCode = 0 THEN
                    ---
                    --- XXCP_PROCESS_HISTORY
                    ---
                                                              
                    XXCP_PROCESS_HIST_GL.Process_History_Insert(
                                                              cSource_Rowid            => xxcp_wks.WORKING_RCD(j).Source_rowid,
                                                              cParent_Rowid            => NULL,
                                                              cTarget_Table            => xxcp_wks.WORKING_RCD(j).Target_Table,
                                                              cTarget_Instance_id      => xxcp_wks.WORKING_RCD(j).Target_Instance_id,
                                                              cProcess_History_id      => xxcp_wks.WORKING_RCD(j).Process_History_id,
                                                              cTarget_assignment_id    => xxcp_wks.WORKING_RCD(j).Target_Assignment_id,
                                                              cAttribute_id            => xxcp_wks.WORKING_RCD(j).Attribute_id,
                                                              cRecord_type             => xxcp_wks.WORKING_RCD(j).Record_type,
                                                              cRequest_id              => xxcp_global.gCommon(1).current_request_id,
                                                              cStatus                  => xxcp_wks.WORKING_RCD(j).Record_Status,
                                                              cInterface_id            => xxcp_wks.WORKING_RCD(j).Interface_id,
                                                              cModel_ctl_id            => xxcp_wks.WORKING_RCD(j).Model_ctl_id,
                                                              cRule_id                 => xxcp_wks.WORKING_RCD(j).Rule_id,
                                                              cTax_registration_id     => xxcp_wks.WORKING_RCD(j).Tax_Registration_id,
                                                              cEntered_rounding_amount => xxcp_wks.WORKING_RCD(j).Entered_Rnd_Amount,
                                                              cAccount_rounding_amount => xxcp_wks.WORKING_RCD(j).Accounted_Rnd_Amount,
                                                              cD1001_Array             => xxcp_wks.WORKING_RCD(j).D1001_Array,
                                                              cD1002_Array             => xxcp_wks.WORKING_RCD(j).D1002_Array,
                                                              cD1003_Array             => xxcp_wks.WORKING_RCD(j).D1003_Array,
                                                              cD1004_Array             => xxcp_wks.WORKING_RCD(j).D1004_Array,
                                                              cD1005_Array             => xxcp_wks.WORKING_RCD(j).D1005_Array,
                                                              cD1006_Array             => xxcp_wks.WORKING_RCD(j).D1006_Array,
                                                              cI1009_Array             => xxcp_wks.WORKING_RCD(j).I1009_Array,
                                                              cErrorMessage            => vErrorMessage,
                                                              cInternalErrorCode       => cInternalErrorCode);

                     IF cInternalErrorCode = 0 THEN
                       vTiming(vTimingCnt).Flush_Records := vTiming(vTimingCnt).Flush_Records + 1;
                     ELSE
                       xxcp_foundation.FndWriteError(cInternalErrorCode, vErrorMessage);
           END IF;
                  ELSE
                    xxcp_foundation.FndWriteError(cInternalErrorCode, vErrorMessage);
          END IF;
          
          -- 03.06.18
          IF cInternalErrorCode = 0 THEN
          
            -- Check the report settlement rule.
            If xxcp_global.gCommon(1).ic_settlements = 'Y' and xxcp_wks.WORKING_RCD(j).I1009_Array(151) is not null then   -- If ICS Rule
            
              if cInternalErrorCode = 0 then                                                               
                xxcp_Process_Dml_Ics_GL.Process_Oracle_Insert (cSource_Rowid        => xxcp_wks.WORKING_RCD(j).Source_rowid,
                                                               cParent_Rowid        => NULL,
                                                               cTarget_Table        => xxcp_wks.WORKING_RCD(j).Target_Table,
                                                               cTarget_Instance_id  => xxcp_wks.WORKING_RCD(j).Target_Instance_id,
                                                               cProcess_History_id  => xxcp_wks.WORKING_RCD(j).Process_History_id,
                                                               cRule_id             => xxcp_wks.WORKING_RCD(j).Rule_id,
                                                               cD1001_Array         => xxcp_wks.WORKING_RCD(j).D1001_Array,
                                                               cD1002_Array         => xxcp_wks.WORKING_RCD(j).D1002_Array,
                                                               cD1003_Array         => xxcp_wks.WORKING_RCD(j).D1003_Array,
                                                               cD1004_Array         => xxcp_wks.WORKING_RCD(j).D1004_Array,
                                                               cD1005_Array         => xxcp_wks.WORKING_RCD(j).D1005_Array,
                                                               cD1006_Array         => xxcp_wks.WORKING_RCD(j).D1006_Array,
                                                               cI1009_Array         => xxcp_wks.WORKING_RCD(j).I1009_Array,
                                                               cErrorMessage        => vErrorMessage,
                                                               cInternalErrorCode   => cInternalErrorCode);             
              end if;
                                                          
              vICS_Cnt := vICS_Cnt + 1;                                                         
            end if;          
          end if;          

          IF cInternalErrorCode = 0 THEN
             vStatus := 'SUCCESSFUL';
          ELSE
             vStatus := 'ERROR';
             xxcp_wks.SOURCE_SUB_CODE(xxcp_wks.WORKING_RCD(j).Source_Pos):= 7008;
          END IF;

          xxcp_wks.SOURCE_STATUS(xxcp_wks.WORKING_RCD(j).Source_Pos) := vStatus;
        ELSE
          -- ZAV
          xxcp_wks.SOURCE_SUB_CODE(xxcp_wks.WORKING_RCD(j).Source_Pos):= 7008;
          xxcp_wks.SOURCE_STATUS(xxcp_wks.WORKING_RCD(j).Source_Pos) := 'SUCCESSFUL';
        END IF;
        
      UDI_Cnt := UDI_Cnt + 1;
      
      
      END IF;
   
      END LOOP;
      
      -- 03.06.18
      -- validate the records inserted into the interface table.                                           
      IF cInternalErrorCode = 0 and xxcp_global.gCommon(1).ic_settlements = 'Y' and vICS_Cnt > 0  then -- If ICS Rule and processed one or more settlement rules
        if xxcp_global.gCommon(1).current_Transaction_table != 'PAYMENTS' then  -- don't perform the integrity check for intercompany payments
          xxcp_ic_applications.integrity_check(cParent_Trx_id       => xxcp_global.gCommon(1).current_parent_trx_id,
                                               cRequest_id          => xxcp_global.gCommon(1).current_request_id,
                                               cErrorMessage        => vErrorMessage,
                                               cInternalErrorCode   => cInternalErrorCode);                                                          
        end if;                                               
      end if;        
    
      IF cInternalErrorCode = 0 AND UDI_Cnt > 0 THEN
        FORALL j IN 1 .. xxcp_wks.SOURCE_CNT
          UPDATE xxcp_gl_interface
             SET vt_Status              = xxcp_wks.SOURCE_STATUS(j),
                 vt_internal_Error_code = NULL,
                 vt_date_processed      = xxcp_global.SystemDate,
                 vt_Status_Code         = xxcp_wks.SOURCE_SUB_CODE(j)
           WHERE ROWID = xxcp_wks.Source_rowid(j);
      END IF;
    
    END IF;
  
   -- Source_Pos
  
    IF cInternalErrorCode <> 0 THEN
      -- Rollback
      IF vCnt > 0 THEN
        ROLLBACK TO TARGET_INSERT;
        xxcp_foundation.FndWriteError(cInternalErrorCode, vErrorMessage);
      END IF;
    
      Error_Whole_Transaction(cInternalErrorCode => cInternalErrorCode);
    
      IF cInternalErrorCode = 12800 THEN
        xxcp_foundation.FndWriteError(cInternalErrorCode,
          'Entered DR <' ||TO_CHAR(vEntered_DR) ||'> Entered CR <' ||TO_CHAR(vEntered_CR) || '>');
      END IF;
    
    END IF;
  
    -- Remove Array Elements
    ClearWorkingStorage;
  
    RETURN(vCnt);
  END FlushRecordsToTarget;

  --  !! ***********************************************************************************************************
  --  !!                                     Reset_Parent_Trx
  --  !!                      Reset Parent_Trx for non Successful Transactions
  --  !! ***********************************************************************************************************
  PROCEDURE Reset_Parent_Trx(cSource_Group_id    IN NUMBER,
                             cTransaction_Table  IN VARCHAR2) IS
 
  Begin

    Update xxcp_gl_interface g
       set vt_parent_trx_id = Null
     where g.vt_transaction_date between gStart_Partition_Date and gEnd_Partition_Date
       and g.vt_source_assignment_id = any(select w.source_assignment_id
                                             from xxcp_source_assignments w 
                                            where w.source_group_id = cSource_Group_id)
       and g.vt_status              IN ('NEW','ERROR','WAITING','ASSIGNMENT','TRANSACTION','PASSTHROUGH')
       and g.vt_transaction_table    = cTransaction_Table
       and not g.vt_request_id = any(select c.request_id from xxcp_activity_control c where c.active = 'Y');

  End Reset_Parent_Trx;

  --  !! ***********************************************************************************************************
  --  !!                                     Reset_HAC_Tables
  --  !!                      Reset Headers, Attributes, Cache for Errored Transactions
  --  !! ***********************************************************************************************************
  PROCEDURE Reset_HAC_Tables(cSource_Group_id      IN NUMBER,
                             cTransaction_Table    IN VARCHAR2) IS
    
   
  BEGIN

      -- Clear HAC tables
      
      -- Header
      DELETE from xxcp_transaction_header h
      WHERE  h.header_id in (select a.header_id 
                           from   xxcp_transaction_attributes a
                           where  a.source_assignment_id = any(select w.source_assignment_id
                                                                  from xxcp_source_assignments w 
                                                                 where w.source_group_id = cSource_Group_id)
                           and    a.interface_id in (select g.vt_interface_id
                                                     from xxcp_gl_interface g
                                                    where g.vt_transaction_date between gStart_Partition_Date and gEnd_Partition_Date 
                                                      and g.vt_source_assignment_id = any(select w.source_assignment_id
                                                                                            from xxcp_source_assignments w 
                                                                                           where w.source_group_id = cSource_Group_id)
                                                      and (g.vt_status = 'ERROR' or g.vt_status = 'WAITING')
                                                      and g.vt_transaction_table = cTransaction_Table
                                                      and not g.vt_request_id = any(select c.request_id from xxcp_activity_control c where c.active = 'Y')
                                                  )
                          ); 
      
      -- Cache
      DELETE from xxcp_transaction_cache c
      WHERE  c.attribute_id in (select a.attribute_id 
                              from   xxcp_transaction_attributes a
                              where  a.source_assignment_id = any(select w.source_assignment_id
                                                                    from xxcp_source_assignments w 
                                                                   where w.source_group_id = cSource_Group_id)
                              and    a.interface_id in (select vt_interface_id
                                                      from  xxcp_gl_interface g
                                                      where vt_transaction_date between gStart_Partition_Date and gEnd_Partition_Date
                                                        and g.vt_source_assignment_id = any(select w.source_assignment_id
                                                                                              from xxcp_source_assignments w 
                                                                                             where w.source_group_id = cSource_Group_id)
                                                        and (g.vt_status = 'ERROR' or g.vt_status = 'WAITING')
                                                        and g.vt_transaction_table = cTransaction_Table
                                                        and not g.vt_request_id = any(select c.request_id from xxcp_activity_control c where c.active = 'Y')
                                                     )
                             ); 
        
      -- Attributes
      DELETE from xxcp_transaction_attributes a
      where  a.source_assignment_id = any(select w.source_assignment_id
                                             from xxcp_source_assignments w 
                                            where w.source_group_id = cSource_Group_id)
      and    a.interface_id in (select vt_interface_id g
                              from  xxcp_gl_interface g
                              where g.vt_transaction_date between gStart_Partition_Date and gEnd_Partition_Date
                                and g.vt_source_assignment_id = any(select w.source_assignment_id
                                                                      from xxcp_source_assignments w 
                                                                      where w.source_group_id = cSource_Group_id)
                                and (g.vt_status = 'ERROR' or g.vt_status = 'WAITING')
                                and g.vt_transaction_table = cTransaction_Table
                                and not g.vt_request_id = any(select c.request_id from xxcp_activity_control c where c.active = 'Y')
                             );
    
  END Reset_HAC_Tables;
   
  --  !! ***********************************************************************************************************
  --  !!                                   Reset_Errored_Transactions
  --  !!                              Reset Transactions from a previous run.
  --  !! ***********************************************************************************************************
  PROCEDURE Reset_Errored_Transactions(cSource_id            IN NUMBER, 
                                       cSource_Group_id      IN NUMBER,
                                       cRequest_id           IN NUMBER, 
                                       cTable_Group_id       IN NUMBER, 
                                       cTrx_Group_id         IN NUMBER,
                                       cTransaction_Table    IN VARCHAR2 default Null 
                                       ) IS

    -- Distinct list of Transaction Tables
    CURSOR c1 is
      SELECT t.transaction_table,
             t.clear_hac,
             t.clear_parent_trx,
             nvl(t.grouping_rule_id,0) grouping_rule_id,
             max(nvl(y.explosion_id,0)) max_explosion_id  -- needed for Dynamic Explosion as the transaction id may change
       FROM xxcp_sys_source_tables t,
            xxcp_sys_source_types y
      WHERE t.source_id      = cSource_id
        AND t.table_group_id = cTable_Group_id
        AND t.source_table_id = y.source_table_id
      Group by t.transaction_table, t.clear_hac, t.clear_parent_trx, nvl(t.grouping_rule_id,0);

    vPrevious_Request_id NUMBER;

  BEGIN

    vTiming(vTimingCnt).Reset_Start := TO_CHAR(SYSDATE, 'HH24:MI:SS');

     -- Remove Errors
     BEGIN
      DELETE FROM xxcp_errors cx
       WHERE cx.source_assignment_id = any(select w.source_assignment_id
                                             from xxcp_source_assignments w 
                                            where w.source_group_id = cSource_Group_id)
        AND cx.table_group_id        = cTable_Group_id
        AND cx.trx_group_id          = cTrx_Group_id
        AND not cx.request_id = any(select c.request_id from xxcp_activity_control c where c.active = 'Y');

      EXCEPTION WHEN OTHERS THEN NULL;
     END;

     For c1_rec in c1 loop -- Loop for all distinct tables in SA

      -- Reset HAC
      If c1_rec.clear_hac = 'Y' or c1_rec.max_explosion_id > 0 then
        Reset_HAC_Tables(cSource_Group_id    => cSource_Group_id,
                         cTransaction_Table  => c1_rec.Transaction_Table);
      end if;
      
      -- Reset Parent Trx
      If c1_rec.clear_parent_trx = 'Y' and c1_rec.grouping_rule_id <> 0 then
        Reset_Parent_Trx(cSource_Group_id      => cSource_Group_id,
                         cTransaction_Table    => c1_rec.Transaction_Table);

       End if; 
     End Loop;
     
 
    
    -- Ignored Status Removal
    If gIgnored_Removal_Flag = 'Y' Then
     Begin
      -- Delete Records with VT_STATUS = IGNORED that are over 1 year old.
      -- Specifying transaction date to use Partitioning.
      Delete xxcp_gl_interface r
       where r.vt_source_assignment_id = any(select w.source_assignment_id
                                             from xxcp_source_assignments w 
                                            where w.source_group_id = cSource_Group_id)
         and r.vt_transaction_date between gIgnored_Start_Removal_Date and gIgnored_End_Removal_Date
         and r.vt_status = 'IGNORED'
         and not r.vt_request_id = any(select c.request_id from xxcp_activity_control c where c.active = 'Y');

      Commit;
         
      Exception when OTHERS then Null;
     End;        
    End If;

    -- Update Interface
    BEGIN

      -- Three updates are used rather than one so that we hit the vt_status
      -- index. This is important when you have 30 million rows.
 
      vPrevious_Request_id := cRequest_id - gRequest_Id_Range;  -- 02.06.07
 

      UPDATE xxcp_gl_interface r
         SET vt_status              = 'NEW',
             vt_Internal_Error_Code = NULL,
             vt_status_code         = NULL,
             vt_request_id          = cRequest_id
       WHERE vt_transaction_date between gStart_Partition_Date and gEnd_Partition_Date
         AND r.vt_source_assignment_id = any(select w.source_assignment_id
                                             from xxcp_source_assignments w 
                                            where w.source_group_id = cSource_Group_id)
         AND r.vt_request_id       >= vPrevious_Request_id
         AND r.vt_status IN ('ASSIGNMENT','ERROR','GROUPING','TRANSACTION','WAITING','PASSTHROUGH')
         AND nvl(r.Group_id,0)      = DECODE(cTrx_Group_id,0,nvl(r.Group_id,0),cTrx_Group_id)
         and not r.vt_request_id = any(select c.request_id from xxcp_activity_control c where c.active = 'Y')
         AND r.vt_transaction_Table = ANY(Select w.transaction_table 
                                            from xxcp_sys_source_tables w
                                           where w.source_id      = cSource_id
                                             and w.table_group_id = cTable_Group_id);
 

      EXCEPTION WHEN OTHERS THEN NULL;
    END;

    COMMIT;
  END RESET_ERRORED_TRANSACTIONS;
  
  --  !! ***********************************************************************************************************
  --  !!                                  Set_Assignment_Status
  --  !!                      Flag the records that are going to be processed.
  --  !! ***********************************************************************************************************
  PROCEDURE Set_Assignment_Status(cSource_id            IN NUMBER,
                                  cSource_assignment_id IN NUMBER,
                                  cRequest_id           IN NUMBER,
                                  cInternalErrorCode    IN OUT NUMBER) IS
 
    vDV_Statement       VARCHAR2(32000);
    vSQLERRM            VARCHAR2(512);

  BEGIN
    cInternalErrorCode := 0; 
  
    vDV_Statement := xxcp_dynamic_sql.Build_Interface_Selection(cSource_id, cSource_assignment_id, cRequest_id);
    
    IF vDV_Statement IS NOT NULL THEN
     EXECUTE IMMEDIATE vDV_Statement;
    END IF;  

    BEGIN
        UPDATE xxcp_gl_interface g
           SET g.vt_status          = 'NEW',
               g.vt_date_processed  = xxcp_global.SystemDate,
               g.VT_STATUS_CODE = 7004
         WHERE g.vt_request_id = cRequest_id
           AND g.vt_status     = 'GROUPING'
           AND g.vt_source_assignment_id = cSource_assignment_id;
    END;

    COMMIT;
          
    EXCEPTION
      WHEN OTHERS THEN
        vSQLERRM := SQLERRM; -- New to trap Oracle's Reason for not working.
        xxcp_foundation.Set_InternalErrorCode(cInternalErrorCode, 10884);
        xxcp_foundation.FndWriteError(10884, vSQLERRM);

  END Set_Assignment_Status;
  
  --  !! ***********************************************************************************************************
  --  !!                                   Grab_Records_To_Process
  --  !!                      Flag the records that are going to be processed.
  --  !! ***********************************************************************************************************
  PROCEDURE Grab_Records_To_Process(cSource_id            IN NUMBER,
                                    cSource_Group_id      IN NUMBER,
                                    cRequest_id           IN NUMBER,
                                    cTable_Group_id       IN NUMBER,
                                    cTrx_Group_id         IN NUMBER) IS

  Begin
      
     Update xxcp_gl_interface r 
        set vt_request_id     = cRequest_id,
            vt_date_processed = sysdate
       Where r.vt_transaction_date between gStart_Partition_Date and gEnd_Partition_Date
         and r.vt_source_assignment_id = any(select v.source_assignment_id 
                                               from xxcp_source_assignments v 
                                              where v.source_group_id = cSource_Group_id 
                                                and v.source_id = cSource_id)
         and r.vt_status             = 'NEW'
         and nvl(r.group_id,0)       = DECODE(cTrx_Group_id,0,nvl(r.group_id,0),cTrx_Group_id)
         and not nvl(r.vt_request_id,0) = any(select c.request_id from xxcp_activity_control c where c.active = 'Y')
         and r.vt_transaction_table  = any(select t.transaction_table 
                                             from xxcp_sys_source_tables t
                                            where t.Table_Group_id  = cTable_Group_id);
  
     Commit;
     
     Exception when others then 
          xxcp_foundation.fndwriteerror(303,'Grap Records to Process:'||SQLERRM);
     
  End Grab_Records_To_Process;
   
  --  !! ***********************************************************************************************************
  --  !!                                  Set_Running_Status
  --  !!                      Flag the records that are going to be processed.
  --  !! ***********************************************************************************************************
  PROCEDURE Set_Running_Status(cSource_id            IN NUMBER,
                               cSource_Group_id      IN NUMBER,
                               cRequest_id           IN NUMBER,
                               cTable_Group_id       IN NUMBER,
                               cTrx_Group_id         IN NUMBER,
                               cInternalErrorCode   OUT NUMBER,
                               cTransaction_Table    IN VARCHAR2 default Null) IS

    CURSOR InterfaceSet(pSource_id IN NUMBER, pSource_Assignment_id IN NUMBER, pRequest_id IN NUMBER, pTable_Group_id IN NUMBER) IS
      SELECT r.vt_transaction_table,
             r.vt_transaction_id,
             r.ROWID source_rowid,
             r.vt_transaction_type,
             nvl(t.grouping_rule_id,0) grouping_rule_id
        FROM xxcp_gl_interface r, 
             xxcp_sys_source_tables t
       WHERE r.vt_transaction_date between gStart_Partition_Date and gEnd_Partition_Date
         AND r.vt_request_id           = pRequest_id
         AND r.vt_source_assignment_id = pSource_assignment_id
         AND r.vt_status               = 'NEW'
         AND r.vt_transaction_table    = t.transaction_table 
         AND t.table_group_id          = pTable_Group_id
         AND t.source_id               = pSource_id 
    ORDER BY vt_transaction_table;
 
    vGrouping_rule_id xxcp_gl_interface.vt_grouping_rule_id%TYPE;
 
    vAction_flag           VARCHAR2(1) := 'N';
    vGroupingRuleExists    BOOLEAN     := FALSE;
    vPrevTransaction_Table xxcp_sys_source_tables.transaction_table%TYPE := '~NULL~';
    
    Cursor SA(pSource_Group_id in number) is
      select sar.source_assignment_id, sar.source_assignment_name
        from xxcp_source_assignments sar
       where sar.source_group_id = pSource_Group_id
         and sar.active          = 'Y'
      order by sar.source_assignment_id;
  
      vCommit_Count Number(6) := 0;
  
  BEGIN
  
    vTiming(vTimingCnt).Set_Running_Start := TO_CHAR(SYSDATE, 'HH24:MI:SS');
  
    Grab_Records_To_Process(cSource_id            => cSource_id,
                            cSource_Group_id      => cSource_Group_id,
                            cRequest_id           => cRequest_id,
                            cTable_Group_id       => cTable_Group_id,
                            cTrx_Group_id         => cTrx_Group_id);
                            
    For SARec in SA(pSource_Group_id => cSource_Group_id) Loop                        
  
     vCommit_Count := 0;
  
     For rec IN InterfaceSet(pSource_id            => cSource_id,
                             pSource_Assignment_id => SARec.Source_Assignment_id,
                             pRequest_id           => cRequest_id,
                             pTable_Group_id       => cTable_Group_id) 
     Loop
     
      vGrouping_rule_id := NULL;
      
      vCommit_Count := vCommit_Count +1;
      
      If vCommit_Count = gRunning_Status_Count then
         vCommit_Count := 0;
         Commit;
      End If;
      
      -- Populate Transaction_Table Array
      If vPrevTransaction_Table <> rec.vt_transaction_table then
        xxcp_global.gTrans_Table_Array.extend;
        xxcp_global.gTrans_Table_Array(xxcp_global.gTrans_Table_Array.count) := rec.vt_transaction_table;
      End If;
      
      vPrevTransaction_Table := rec.vt_transaction_table;
 
      IF Rec.Grouping_Rule_id > 0 then  -- 02.06.02

        vGroupingRuleExists := TRUE;
                
        xxcp_global.gCommon(1).current_transaction_table := Rec.vt_Transaction_Table;
        xxcp_global.gCommon(1).current_transaction_id    := Rec.vt_Transaction_id;

        vGrouping_rule_id := xxcp_custom_events.get_group_rule_id(cSource_id,
                                                                  SARec.Source_assignment_id,
                                                                  rec.source_rowid,
                                                                  rec.vt_transaction_table,
                                                                  rec.vt_transaction_type);

        IF NVL(vGrouping_rule_id, 0) = 0 THEN
          vGrouping_rule_id := Rec.Grouping_rule_id; -- Default;
        END IF;
      END IF;
      
      -- Set Processing Status   
      BEGIN
        UPDATE xxcp_gl_interface r
           SET vt_status           = 'GROUPING',
               vt_date_processed   = xxcp_global.SystemDate,
               vt_status_code      = NULL,
               vt_request_id       = cRequest_id,
               vt_Grouping_rule_id = vGrouping_rule_id
         WHERE r.ROWID = rec.source_rowid;
      
     
      EXCEPTION
        WHEN OTHERS THEN NULL;
      END;
     END LOOP;

     Commit;
    -- Error Checking  

    IF vGroupingRuleExists then  -- 02.06.02 Don't Check If no Grouping Rule used.

      -- Find records with no vt_grouping_rule_id 
        UPDATE xxcp_gl_interface g
           SET g.vt_status              = 'ERROR',
               g.vt_date_processed      = xxcp_global.SystemDate,
               g.vt_internal_error_code = 11088
         WHERE g.vt_request_id = cRequest_id
           AND g.vt_status     = 'GROUPING'
           AND g.vt_source_assignment_id = SARec.Source_Assignment_id
           AND nvl(g.group_id,0)       = DECODE(cTable_Group_id,0,nvl(g.group_id,0),cTable_Group_id)
           AND g.vt_grouping_rule_id IS NULL
           AND g.vt_transaction_table = any(select t.transaction_table 
                                              from xxcp_sys_source_tables t
                                             where t.Table_Group_id = cTable_Group_id
                                               and nvl(t.grouping_rule_id,0) > 0);
        Commit;

        -- Find records with invalid Grouping rule 
        UPDATE xxcp_gl_interface g
           SET g.vt_status              = 'ERROR',
               g.vt_date_processed      = xxcp_global.SystemDate,
               g.vt_internal_error_code = 11089
         WHERE g.vt_request_id = cRequest_id
           AND g.vt_status     = 'GROUPING'
           AND g.vt_source_assignment_id = SARec.Source_Assignment_id
           AND nvl(g.group_id,0)       = DECODE(cTable_Group_id,0,nvl(g.group_id,0),cTable_Group_id)
           AND g.vt_grouping_rule_id IS NULL
           AND NOT g.vt_grouping_rule_id = any(select w.grouping_rule_id 
                                                 from xxcp_grouping_rules w
                                                 where w.source_id = cSource_id)
           AND g.vt_transaction_table = any(select t.transaction_table 
                                              from xxcp_sys_source_tables t
                                             where t.Table_Group_id = cTable_Group_id
                                               and nvl(t.grouping_rule_id,0) > 0);
      
         Commit;
         
    END IF;
  
    -- UNPOSTED TRANSACTIONS 
    --
    -- Check to see if we should test for Unposted Transactions.
    -- If the database trigger is being used, this is not necessary!! 
    --
    If cSource_id = 4 then
     Begin
      -- Check Lookup 
      SELECT UPPER(Lookup_Code) INTO vAction_flag 
        FROM xxcp_lookups
       WHERE numeric_code = 4
         AND lookup_type = 'RECEIVABLE JOURNALS'
         AND lookup_type_subset = 'CHECK UNPOSTED'
         AND Enabled_flag = 'Y';
         
      IF vAction_flag = 'CHECK' Then
       BEGIN      
         Update xxcp_gl_interface g
             SET vt_status =  
               DECODE(XXCP_UNPOSTED_TRANSACTIONS.Check_Unposted_Rows(g.vt_transaction_table
                                                                   , g.vt_transaction_id
                                                                   , g.set_of_books_id),'Y','UNPOSTED',g.vt_status)
             WHERE vt_request_id = cRequest_id;
          
         EXCEPTION WHEN NO_DATA_FOUND THEN NULL;
       END;
    
      END IF;
      
      EXCEPTION WHEN NO_DATA_FOUND THEN Null; 

     End;   
    End If;  
   
    Commit;

    cInternalErrorCode := xxcp_dynamic_sql.Apply_Exclude_Rule(cSource_Assignment_id => SARec.Source_assignment_id); 
    
    If  cInternalErrorCode = 0 then
        
        Transaction_Group_Stamp(cSource_Assignment_id => SARec.Source_Assignment_id, 
                                cTable_Group_id       => cTable_Group_id,
                                cRequest_id           => cRequest_id,
                                cInternalErrorCode    => cInternalErrorCode);
                            
    End If;                        
                            
    -- Set Assignment Status -- (Not used in Preview)
    IF cInternalErrorCode = 0 THEN 
      Set_Assignment_Status(cSource_id            => cSource_id,
                            cSource_Assignment_id => SARec.Source_Assignment_id,
                            cRequest_id           => cRequest_id,
                            cInternalErrorCode    => cInternalErrorCode);                               
    END IF;                         

    If nvl(cInternalErrorCode,0) > 0 then
      Exit;
    End If;
   
   End Loop; -- Source Assignments 
  
  END Set_Running_Status;
  --
  -- Engine_Controller
  --
  Procedure Read_Engine_Controls is
    
   Cursor LK(pLookup_type in varchar2) is  
     select lk1.Lookup_Code, lk1.numeric_code
       from xxcp_lookups lk1
      WHERE lookup_type = pLookup_type
        AND enabled_flag = 'Y';
         
   Cursor SP(pProfile_Category in varchar2, pProfile_Name in varchar2) is
    select p.profile_value
      from xxcp_sys_profile p
     where p.profile_name = pProfile_Name
       and p.profile_category = pProfile_Category;         
         
  Begin
     -- Trace Mode 
    gEngine_Trace := 'N';
    For Rec in LK('TRACE GL ENGINE') loop
       gEngine_Trace := Rec.Lookup_Code;
    End Loop;
        
    -- Assignment Commit Count
    For Rec in SP('EV','ASSIGNMENT COMMIT FREQUENCY') loop
       gAssignment_Commit := Rec.profile_value; 
       If NOT gAssignment_Commit between 1 and 2000 then
        gAssignment_Commit := 100;
       End If;
    End Loop;   

    -- Transaction Commit Count
    For Rec in SP('EV','TRANSACTION COMMIT FREQUENCY') loop
       gTransaction_Commit := Rec.profile_value; 
       
       If NOT gTransaction_Commit between 1 and 2000 then
         gTransaction_Commit := 100;
       End If;   
    End Loop;       
  
    -- Terminate Check
    For Rec in SP('EV','TERMINATE CHECK FREQUENCY') loop
       gOracle_Check_Count := Rec.profile_value; 
       
       If NOT gTransaction_Commit between 1 and 4000 then
         gOracle_Check_Count := 1000;
       End If;   
    End Loop;       
  
    -- Running Status
    For Rec in SP('EV','RUNNING STATUS FREQUENCY') loop
       gRunning_Status_Count := Rec.profile_value; 
       
       If NOT gTransaction_Commit between 1 and 100000 then
         gRunning_Status_Count := 30000;
       End If;   
    End Loop;  
    
  End Read_Engine_Controls;
  
  --   !! ***********************************************************************************************************
  --   !!                                     Record_Checks
  --   !!                       Ignore, Duplicates and Pass through checks
  --   !! ***********************************************************************************************************
  --
  Function Record_Checks(cSource_Rowid      in rowid, 
                         cTransaction_Table in varchar2,
                         cTransaction_Class in varchar2,
                         cDuplicate_check   in varchar2,
                         cDuplicate_found   in varchar2
                         ) Return Number is

  Begin

      IF XXCP_TE_BASE.Ignore_Class(cTransaction_Table, cTransaction_Class) THEN
          UPDATE xxcp_gl_interface r
             SET r.VT_STATUS = 'IGNORED',
                 r.VT_INTERNAL_ERROR_CODE = NULL,
                 r.VT_DATE_PROCESSED = sysdate
           WHERE r.ROWID = cSource_Rowid;

      -- If it is a duplicate then it will be marked as DUPLICATE and not processed.
      ELSIF cDuplicate_check = 'Y' and cDuplicate_found = 'Y' then
        UPDATE xxcp_gl_interface r
             Set r.VT_STATUS = 'DUPLICATE',
                 r.VT_INTERNAL_ERROR_CODE = NULL,
                 r.VT_DATE_PROCESSED      = Sysdate
             Where r.ROWID = cSource_Rowid;
      -- First/Second Passthrough
      ELSIF xxcp_global.gPassThroughCode > 0 then
        Update xxcp_gl_interface  r
              set r.vt_status = decode(xxcp_global.gPassThroughCode,7022,'SUCCESSFUL',
                                                                    7023,'SUCCESSFUL',
                                                                    7026,'PASSTHROUGH',
                                                                    7027,'PASSTHROUGH')
                 ,r.vt_internal_Error_code  = Null
                 ,r.vt_status_code          = xxcp_global.gPassThroughCode
                 ,r.vt_date_processed       = Sysdate
            where r.rowid                   = cSource_Rowid;


       END IF;
    Return(0);
  End Record_Checks;  
  

  --  !! ***********************************************************************************************************
  --  !!                                    CONTROL
  --  !!                  External Procedure (Entry Point) to start the AR Engine.
  --  !! ***********************************************************************************************************
  FUNCTION Control(cSource_Group_id    IN NUMBER,
                   cConc_Request_Id    IN NUMBER,
                   cTable_Group_id     IN NUMBER   DEFAULT 0,
                   cClearDownFrequency IN NUMBER   DEFAULT 0,
                   cDebug_on           IN VARCHAR2 DEFAULT 'N') RETURN NUMBER IS

    -- Assignment
    CURSOR cfg(pSource_id IN NUMBER, pSource_Assignment_id IN NUMBER, pRequest_id IN NUMBER, pTable_Group_id IN NUMBER) IS
      SELECT /*+ Leading(g) */
             g.vt_transaction_table,
             g.vt_transaction_type,
             g.vt_transaction_class,
             g.vt_transaction_id,
             g.vt_parent_trx_id,
             g.vt_Interface_id,
             g.vt_Transaction_Ref,
             s.set_of_books_id,
             g.ROWID Source_rowid,
             g.Currency_code,
             g.Entered_DR,
             g.Entered_CR,
             s.source_assignment_id,
             s.set_of_books_id Source_Set_of_books_id,
             w.transaction_set_name,
             NVL(w.Transaction_Set_id,0) Transaction_Set_id,
             g.vt_transaction_date
             ,y.source_table_id
             ,y.source_type_id
             ,cx.source_class_id
             ,t.calc_legal_exch_rate
             ,nvl(y.explosion_id,0) ASG_EXPLOSION_ID
        FROM xxcp_source_assignments      s,
             xxcp_gl_interface            g,
             -- New
             xxcp_sys_source_tables       t,
             xxcp_sys_source_types        y,
             xxcp_sys_source_classes      cx,
             xxcp_source_transaction_sets w
       WHERE g.vt_request_id           = pRequest_id
         AND g.vt_status               = 'ASSIGNMENT'
         AND g.vt_source_assignment_id = pSource_Assignment_ID
         AND g.vt_source_assignment_id = s.source_assignment_id
         -- Table
         AND g.vt_transaction_table    = t.transaction_table
         AND t.source_id               = pSource_id
         AND t.table_group_id          = pTable_Group_id
         -- Type
         AND g.vt_transaction_type     = y.TYPE
         AND y.latch_only             = 'N'
         AND y.source_table_id         = t.source_table_id
         -- Class
         AND g.vt_transaction_class    = cx.CLASS
         AND cx.latch_only             = 'N'
         AND t.source_table_id         = cx.source_table_id
         -- Transaction_Set_id
         AND y.Transaction_Set_id      = w.Transaction_Set_id
       ORDER BY DECODE(psource_id,1,g.user_je_source_name,g.vt_transaction_table),
                DECODE(psource_id, 1, g.user_je_category_name, NULL),w.SEQUENCE,  g.vt_parent_trx_id, cx.SEQUENCE, vt_transaction_id;

    -- Transactions
    CURSOR GLI(pSource_id IN NUMBER, pSource_Assignment_id IN NUMBER, pRequest_id IN NUMBER, pTable_Group_id IN NUMBER) IS
      SELECT /*+ Leading(g) */
             g.vt_transaction_table,
             g.vt_transaction_type,
             g.vt_transaction_class,
             g.vt_Interface_id,
             g.vt_transaction_id,
             g.vt_transaction_Ref,
             s.Instance_id vt_Instance_id,
             g.Code_Combination_id,
             g.User_je_category_name,
             -- Balancing segment
             g.USER_JE_SOURCE_NAME   be2,
             g.GROUP_ID              be3,
             g.USER_JE_CATEGORY_NAME be4,
             g.Reference1            be6,
             g.Reference2            be7,
             g.Reference5            be8,
             -- Inventory
             DECODE(g.ACCOUNTED_DR,
                    NULL,
                    g.ACCOUNTED_CR * -1,
                    g.ACCOUNTED_DR) Transaction_Cost,
             -- History
             g.Group_id,
             g.USER_JE_CATEGORY_NAME Category_name,
             g.USER_JE_SOURCE_NAME Source_name,
             g.ACCOUNTING_DATE,
             ((NVL(g.Entered_CR, 0) * -1) + NVL(g.Entered_DR, 0)) Parent_Entered_Amount,
             g.Currency_Code Parent_Entered_Currency,
             g.Accounted_CR,
             g.Accounted_DR,
             g.Currency_Code,
             g.Entered_DR,
             g.Entered_CR,
             g.vt_parent_trx_id,
             g.ROWID Source_Rowid,
             s.set_of_books_id,
             NVL(g.segment1, c.segment1) Segment1,
             NVL(g.segment2, c.segment2) Segment2,
             NVL(g.segment3, c.segment3) Segment3,
             NVL(g.segment4, c.segment4) Segment4,
             NVL(g.segment5, c.segment5) Segment5,
             NVL(g.segment6, c.segment6) Segment6,
             NVL(g.segment7, c.segment7) Segment7,
             NVL(g.segment8, c.segment8) Segment8,
             NVL(g.segment9, c.segment9) Segment9,
             NVL(g.segment10, c.segment10) Segment10,
             NVL(g.segment11, c.segment11) Segment11,
             NVL(g.segment12, c.segment12) Segment12,
             NVL(g.segment13, c.segment13) Segment13,
             NVL(g.segment14, c.segment14) Segment14,
             NVL(g.segment15, c.segment15) Segment15,
             s.source_assignment_id,
             s.set_of_books_id Source_Set_of_books_id,
             w.transaction_set_name,
             w.Transaction_Set_id,
             g.date_created  source_creation_date
             --
             ,y.source_table_id
             ,y.source_type_id
             ,cx.source_class_id
             ,t.class_mapping_req
             ,g.gl_sl_link_id
             ,DECODE(g.user_je_source_name,'Inventory',g.Reference23, g.vt_transaction_id) class_transaction_id
             ,t.calc_legal_exch_rate
             ,nvl(y.explosion_id,0) ASG_Explosion_id
             ,nvl(y.trx_explosion_id,0)   TRX_EXPLOSION_ID
        FROM xxcp_gl_interface            g,
             gl_code_combinations         c,
             xxcp_source_assignments      s,
             -- New
             xxcp_sys_source_tables       t,
             xxcp_sys_source_types        y,
             xxcp_sys_source_classes      cx,
             xxcp_source_transaction_sets w
       WHERE g.vt_status               = 'TRANSACTION'
         AND g.vt_source_assignment_id = pSource_Assignment_ID
         AND g.vt_source_assignment_id = s.source_assignment_id
         AND s.source_id               = pSource_id
         and nvl(g.vt_status_code,0)   = 0
         -- Table
         AND g.vt_transaction_table    = t.transaction_table
         AND t.source_id               = pSource_id
         AND t.table_group_id          = pTable_Group_id
         -- Type
         AND g.vt_transaction_type     = y.TYPE
         AND y.latch_only             = 'N'
         AND t.source_table_id         = y.source_table_id
         -- Class
         AND g.vt_transaction_class    = cx.CLASS
         AND cx.latch_only             = 'N'
         AND t.source_table_id         = cx.source_table_id
         -- Transaction_Set_id
         AND y.Transaction_Set_id      = w.Transaction_Set_id
         -- GL Code Combinations
         AND g.vt_request_id           = pRequest_id
         AND g.code_combination_id     = c.code_combination_id(+)
       ORDER BY DECODE(pSource_id,1,g.user_je_source_name,g.vt_transaction_table),
                DECODE(psource_id, 1, g.user_je_category_name, NULL),
                w.SEQUENCE, vt_parent_trx_id,cx.SEQUENCE,vt_transaction_id;

    CURSOR ConfErr(pSource_Assignment_id IN NUMBER, pRequest_id IN NUMBER) IS
      SELECT DISTINCT k.vt_parent_trx_id, k.vt_transaction_table
        FROM xxcp_gl_interface k
       WHERE k.vt_request_id           = pRequest_id
         AND k.vt_source_assignment_id = pSource_Assignment_id
         AND k.vt_status               = 'ERROR';

    -- Assignments
    CURSOR sr1(pSource_Group_id IN NUMBER) IS
      SELECT set_of_books_id,
             Source_Assignment_id,
             instance_id source_instance_id,
             Source_id,
             stamp_parent_trx
        FROM xxcp_source_assignments g
       WHERE g.Source_Group_id = pSource_Group_id
         AND g.active          = 'Y';
  
    vInternalErrorCode         xxcp_errors.internal_error_code%TYPE := 0;
    vExchange_Rate_Type        xxcp_tax_registrations.exchange_rate_type%TYPE;
    vCommon_Exch_Curr          xxcp_tax_registrations.common_exch_curr%TYPE;
  
    i                          PLS_INTEGER := 0;
    j                          INTEGER := 0;
    k                          NUMBER;
    vTiming_Start              NUMBER;
    vDML_Compiled              VARCHAR2(1);
    vGlobal_Rounding_Tolerance NUMBER := 0;
    vTransaction_Error         BOOLEAN := FALSE;
  
    vGlobalPrecision           PLS_INTEGER := 2;

    vOracleCommitCnt           PLS_INTEGER := 0;
    vAssignmentCommitCnt       PLS_INTEGER := 0;
    vTransactionCommitCnt      PLS_INTEGER := 0;

    vJob_Status                PLS_INTEGER := 0;
    vSource_Activity           xxcp_sys_sources.source_activity%TYPE := 'GL';
    vStaged_Records            xxcp_sys_sources.staged_records%TYPE;
  
    vTransaction_Class         xxcp_sys_source_classes.CLASS%TYPE;
    vRequest_ID                xxcp_process_history.request_id%TYPE;
  
    -- NEW PARAMETERS
    vCurrent_Parent_Trx_id     xxcp_transaction_attributes.parent_trx_id%TYPE := 0;
    vTransaction_Type          xxcp_sys_source_types.TYPE%TYPE;
    --
    -- Used for the REC Distribution
    vExtraLoop                 BOOLEAN := FALSE;
    vExtraClass                xxcp_sys_source_classes.CLASS%TYPE;
    --
    vSource_Assignment_id      xxcp_source_assignments.source_assignment_id%TYPE; 
    vSource_instance_id        xxcp_source_assignments.instance_id%TYPE;
    vSource_ID                 xxcp_sys_sources.source_id%TYPE; 
    vClass_Mapping_Name        xxcp_sys_source_classes.CLASS%TYPE;
    -- 02.05.02
    vDuplicate_check           xxcp_sys_sources.duplicate_check%type;
    vDuplicate_found           varchar2(1);
    
    vError_Completion_Status   xxcp_source_assignment_groups.error_completion_status%TYPE := 'N';                                     
    
    CURSOR SF(pSource_Group_id IN NUMBER) IS
      SELECT s.source_activity,
             s.Source_id,
             s.Preview_ctl,
             s.Timing,
             s.Custom_events,
             s.Cached,
             s.DML_Compiled,
             s.Staged_Records,
             g.instance_id source_instance_id,
             -- 02.05.02
             nvl(s.duplicate_check,'N') duplicate_check,
             nvl(g.error_completion_status,'N') error_completion_status,
             nvl(s.multithread_engine,'N') multithread_engine,
             -- 03.06.18
             nvl(s.flat_file,'N') flat_file,
             nvl(s.ics_flag,'N') ic_settlements             
        FROM xxcp_source_assignment_groups g, 
             xxcp_sys_sources s
       WHERE source_group_id = pSource_Group_id
         AND s.source_id = g.source_id;
  
    CURSOR Tolx IS
      SELECT Numeric_Code Global_Rounding_Tolerance
        FROM xxcp_lookups
       WHERE lookup_type = 'ROUNDING TOLERANCE'
         AND lookup_code = 'GLOBAL';
  
    CURSOR CRL(pSource_id IN NUMBER) IS
     SELECT DISTINCT Transaction_table, source_id
       FROM xxcp_column_rules
      WHERE source_id = pSource_id; 
      
    CURSOR TGR(pSource_id IN NUMBER, pTrx_Group_id IN NUMBER) IS
      select g.trx_group_required
      from xxcp_sys_table_groups g
      where g.source_id = psource_id
        and g.table_group_id = ptrx_group_id;

    vTrx_Group_id       xxcp_gl_interface.group_id%type := 0;     
    vProcess_Run_Id     Number;    
    vErrorMessage       VARCHAR2(3000); 
    
    -- Dynamic Explosion 
    vExplosion_summary      NUMBER;
    vLast_source_type_id    xxcp_sys_source_types.source_type_id%TYPE := 0;
    vExplosion_sql          VARCHAR2(4000);
    vRowsfound              NUMBER;
    jx                      NUMBER;
    vExplosion_trx_id       NUMBER;
    vExplosion_source_rowid VARCHAR2(32);
    vExplosion_trx_type_id  NUMBER(15);
    vExplosion_trx_class_id NUMBER(15);
    
  BEGIN

    xxcp_global.Trace_on   := NVL(gEngine_Trace,'N');
    xxcp_global.Preview_on := 'N';
    xxcp_global.SystemDate := SYSDATE;
    vTimingCnt             := 1;
    gEngine_Error_Found    := FALSE;
    gForce_Job_Error       := FALSE;
    gForce_Job_Warning     := FALSE;

    FOR SFRec IN SF(cSource_Group_id) LOOP
      
      vSource_Activity    := SFRec.Source_Activity;
      vSource_id          := SFRec.Source_id;
      vDML_Compiled       := SFRec.DML_Compiled;
      vStaged_Records     := SFRec.Staged_Records;
      vSource_Instance_id := SFRec.Source_Instance_id;
      -- 02.05.02
      vDuplicate_check    := SFRec.Duplicate_Check;

      xxcp_global.gCommon(1).Source_id     := vSource_id;
      xxcp_global.gCommon(1).Preview_ctl   := SFRec.Preview_ctl;
      xxcp_global.gCommon(1).Preview_on    := xxcp_global.Preview_on;
      xxcp_global.gCommon(1).Custom_events := SFRec.Custom_events;
      
      xxcp_global.gCommon(1).Current_Source_activity := vSource_Activity;
      xxcp_global.gCommon(1).Cached_Attributes       := SFRec.Cached; 
      xxcp_global.gCommon(1).Current_Table_Group_id  := cTable_Group_id;
      xxcp_global.gCommon(1).Current_Trx_Group_id    := nvl(xxcp_global.Get_Interface_Parameters('GPRM_JOURNAL_GROUP_ID'),0);
      
      -- 03.06.18
      xxcp_global.gCommon(1).flat_file := SFRec.flat_file;
      xxcp_global.gCommon(1).ic_settlements := SFRec.ic_settlements;          
      
      vProcess_Run_Id := 0; -- needs to be zero if not mutlitreading
      If nvl(xxcp_global.gCommon(1).Current_Trx_Group_id,0) > 0 Then
        vTrx_Group_id   := to_number(xxcp_global.gCommon(1).Current_Trx_Group_id);   
      ElsIf SFRec.Multithread_engine = 'Y' Then
        -- Use the concurrent request id. This number may be recycled
        -- but as long a the number is not reused whilst the job
        -- is active then it will not matter.
        vProcess_Run_Id := cConc_Request_Id;
      End If;
      vError_Completion_Status := SFRec.Error_Completion_Status;
    END LOOP;

    xxcp_global.set_source_id(cSource_id => vSource_id);
    
    --     SYS.DBMS_SUPPORT.START_TRACE( waits=>true, binds=>true );

    vTiming(vTimingCnt).CF_Records    := 0;
    vTiming(vTimingCnt).Flush_Records := 0;
    vTiming(vTimingCnt).Start_time    := TO_CHAR(SYSDATE, 'HH24:MI:SS');
    vTiming(vTimingCnt).FLUSH         := 0;

    vTiming_Start  := xxcp_reporting.Get_Seconds;
  
    IF xxcp_global.gCommon(1).Custom_events = 'Y' THEN
      xxcp_custom_events.Set_system_mode(xxcp_global.gCommon);
    END IF;
  
    --
    -- ## *******************************************************************************************************
    -- ##
    -- ##                 Check to See Column Rules are compiled
    -- ##
    -- ## *******************************************************************************************************
    --
    IF vDML_Compiled = 'N' THEN
     -- Prevent Processing and the column rules have changed and
     -- they need compiling.
     vJob_Status := 4;
     xxcp_foundation.show('ERROR: Column Rules have changed, but not re-generated'); 
     xxcp_foundation.FndWriteError(10999,'Column Rules have changed, but not re-generated');

    END IF;

    xxcp_global.Set_Last_Request_id(0);

    --
    -- ## *******************************************************************************************************
    -- ##
    -- ##      Check the Journal Group against Table Groups
    -- ## If the transaction Group is required and the Transaction Group has not been provided
    -- ## The job will terminate with an error code of 8
    -- ##
    -- ## *******************************************************************************************************
    --
    FOR Rec IN TGR(vSource_id, cTable_Group_id) LOOP
      IF rec.trx_group_required = 'Y' AND  vTrx_Group_id = 0 THEN
        vJob_Status := 8;
      END IF;
    END LOOP;
    
    --
    -- ## *******************************************************************************************************
    -- ##
    -- ##                            Check to See the process is already in use
    -- ##
    -- ## *******************************************************************************************************
    --
    IF NOT xxcp_management.Locked_Process(cSource_Activity => vSource_Activity,
                                          cSource_Group_id => cSource_Group_id,
                                          cTable_Group_id  => cTable_Group_id, 
                                          cTrx_Group_id    => vTrx_Group_id,
                                          cProcess_Run_Id  => vProcess_Run_Id
                                         ) AND vJob_Status = 0 THEN
                                         
      vRequest_id := xxcp_management.Activate_Lock_Process(cSource_Activity => vSource_Activity,
                                                           cSource_Group_id => cSource_Group_id,
                                                           cConc_Request_Id => cConc_Request_ID,
                                                           cTable_Group_id  => cTable_Group_id,
                                                           cTrx_Group_id    => vTrx_Group_id,
                                                           cProcess_Run_Id  => vProcess_Run_Id);

      xxcp_global.Set_Last_Request_id(vRequest_id);
      xxcp_foundation.show('Activity locked....' || TO_CHAR(vRequest_id));

      xxcp_global.User_id  := fnd_global.user_id;
      xxcp_global.Login_id := fnd_global.login_id;

      xxcp_global.gCommon(1).Current_request_id := vRequest_id;

      IF xxcp_global.gCommon(1).Custom_events = 'Y' THEN
        vInternalErrorCode := xxcp_custom_events.Before_Processing(vSource_id);
      END IF;
      --
      -- ## **************************************************************************************************
      -- ##
      -- ##                                Setup Ready for the Run
      -- ##
      -- ## **************************************************************************************************
      --
      FOR REC IN sr1(cSource_Group_id) LOOP
      
        EXIT WHEN vJob_Status <> 0;
      
        vSource_Instance_id   := rec.source_instance_id;
        vSource_Assignment_id := rec.Source_Assignment_id; 
        
        vTiming(vTimingCnt).Init_Start := to_char(sysdate, 'HH24:MI:SS');
        vTiming(vTimingCnt).Flush_Records := 0;
        
        IF vTimingCnt = 1 THEN
            xxcp_foundation.show('Initialization....');
            xxcp_te_base.Engine_Init(cSource_id         => vSource_id,
                                     cSource_Group_id   => cSource_Group_id,
                                     cInternalErrorCode => vInternalErrorCode);
        END IF;
        
        vTiming(vTimingCnt).Source_Assignment_id := vSource_Assignment_id;
      
        -- Setup Globals
        xxcp_global.gCommon(1).current_process_name  := 'XXCP_GLENG';
        xxcp_global.gCommon(1).current_assignment_id := vSource_Assignment_ID;
        -- 02.05.03
        vCurrent_Parent_Trx_id := 0;
        -- 02.06.02
        xxcp_global.gTrans_Table_Array.delete;
        --
        -- *********************************************************************************************************
        --                            Populate Column Rules and Initialize Cursors
        -- *********************************************************************************************************
        --
        vTiming(vTimingCnt).Memory_Start := TO_CHAR(SYSDATE, 'HH24:MI:SS');

        FOR Rec IN CRL(pSource_id => vSource_id) LOOP
          -- Used for column Attributes and error messages 
          --
          xxcp_memory_Pack.LoadColumnRules(cSource_id          => vSource_id, 
                                           cSource_Table       => 'XXCP_GL_INTERFACE',
                                           cSource_Instance_id => vSource_instance_id,
                                           cTarget_Table       => Rec.Transaction_table,
                                           cRequest_id         => vRequest_id,
                                           cInternalErrorCode  => vInternalErrorCode);

          xxcp_te_base.Init_Cursors(cTarget_Table => Rec.Transaction_table);
        END LOOP;
      

        --
        -- *******************************************************************************************************
        --     Reset Previosly Errored Transactions and Assign Parent Trx Id to Incomming transactions
        -- *******************************************************************************************************
        --
        
        vInternalErrorCode := 0; 

        If vTimingCnt = 1 then
            
           Reset_Errored_Transactions(cSource_id            => vSource_id,
                                      cSource_Group_id      => cSource_Group_id, 
                                      cRequest_id           => vRequest_id,
                                      cTable_Group_id       => cTable_Group_id,
                                      cTrx_Group_id         => vTrx_Group_id);
                                   
           Set_Running_Status(     cSource_id            => vSource_id, 
                                   cSource_Group_id      => cSource_Group_id,  
                                   cRequest_id           => vRequest_id,
                                   cTable_Group_id       => cTable_Group_id,
                                   cTrx_Group_id         => vTrx_Group_id,
                                   cInternalErrorCode    => vInternalErrorCode);
        Else
          -- All stamping and groups is done up front now.
          vTiming(vTimingCnt).Reset_Start       := to_char(sysdate, 'HH24:MI:SS');
          vTiming(vTimingCnt).Group_Stamp_Start := to_char(sysdate, 'HH24:MI:SS');
          vTiming(vTimingCnt).Set_Running_Start := to_char(sysdate, 'HH24:MI:SS');
        End If;


        -- Data Staging 
        IF NVL(vInternalErrorCode,0) = 0 THEN
          vInternalErrorCode :=
            xxcp_custom_events.Data_Staging(cSource_id            => vSource_id,
                                            cSource_Assignment_id => vSource_Assignment_id,
                                            cRequest_id           => vRequest_id, 
                                            cPreview_id           => NULL);
        END IF;
        
        vTiming(vTimingCnt).Init_End := TO_CHAR(SYSDATE, 'HH24:MI:SS');
        --
        -- GLOBAL ROUNDING TOLERANCE
        --
        vGlobal_Rounding_Tolerance := 0;
        FOR Rec IN Tolx LOOP
          vGlobal_Rounding_Tolerance := Rec.Global_Rounding_Tolerance;
        END LOOP;

        IF vInternalErrorCode = 0 THEN
          --
          -- Get Standard Parameters
          --
          vInternalErrorCode := 
            xxcp_foundation.fnd_gl_lookup(cExchange_Rate_Type => vExchange_Rate_Type,
                                          cGlobalPrecision    => vGlobalPrecision,
                                          cCommon_Exch_Curr   => vCommon_Exch_Curr);
        END IF;

        COMMIT;

        IF vInternalErrorCode = 0 THEN

          --
          -- ## ***********************************************************************************************************
          -- ##                                Assignment
          -- ## ***********************************************************************************************************
          --
          i := 0;
          
          vAssignmentCommitCnt := 0;

          vTiming(vTimingCnt).CF_last     := xxcp_reporting.Get_Seconds;
          vTiming(vTimingCnt).CF          := 0;
          vTiming(vTimingCnt).CF_Records  := 0;
          vTiming(vTimingCnt).CF_Start_Date := Sysdate;
          xxcp_global.SystemDate := SYSDATE;

          FOR CFGRec IN  CFG(pSource_id             => vSource_id, 
                             pSource_Assignment_id  => vSource_Assignment_id,
                             pRequest_id            => vRequest_id,
                             pTable_Group_id        => cTable_Group_id)
          LOOP

                i := i + 1;
                
                vOracleCommitCnt     := vOracleCommitCnt + 1;
                vAssignmentCommitCnt := vAssignmentCommitCnt + 1;

                vTiming(vTimingCnt).CF_RECORDS := vTiming(vTimingCnt).CF_RECORDS + 1;

                xxcp_global.gCommon(1).CURRENT_SOURCE_ROWID      := CFGREC.SOURCE_ROWID;
                xxcp_global.gCommon(1).CURRENT_TRANSACTION_TABLE := CFGREC.VT_TRANSACTION_TABLE;
                xxcp_global.gCommon(1).CURRENT_TRANSACTION_ID    := CFGREC.VT_TRANSACTION_ID;
                xxcp_global.gCommon(1).Calc_legal_exch_rate      := CFGRec.calc_legal_exch_rate;
                xxcp_global.gCommon(1).CURRENT_PARENT_TRX_ID     := CFGREC.VT_PARENT_TRX_ID;
                xxcp_global.gCommon(1).CURRENT_INTERFACE_ID      := CFGREC.VT_INTERFACE_ID;
                xxcp_global.gCommon(1).CURRENT_TRANSACTION_DATE  := CFGREC.VT_TRANSACTION_DATE;
                xxcp_global.gCommon(1).CURRENT_INTERFACE_REF     := CFGREC.VT_TRANSACTION_REF; 
                xxcp_global.gCommon(1).explosion_id              := CFGRec.ASG_Explosion_Id;

                xxcp_global.gCommon(1).PREVIEW_PARENT_TRX_ID     := NULL;
                xxcp_global.gCommon(1).PREVIEW_SOURCE_ROWID      := NULL;

                vTransaction_Class := CFGREC.VT_TRANSACTION_CLASS;
                                
                -- 02.05.02
                -- If the duplicate check is enabled for the source
                IF vDuplicate_check = 'Y' then 
                
                  vDuplicate_found := 'N';
                                    
                  -- this cursor checks to see if the record already exists. 
                  for x in (select 'Y'
                            from xxcp_gl_interface 
                            where vt_source_assignment_id =  vSource_assignment_id
                            and   vt_transaction_table    =  CFGRec.VT_Transaction_Table
                            and   vt_transaction_id       =  CFGRec.VT_Transaction_ID
                            and   rowid                  !=  CFGRec.Source_Rowid 
                            and   vt_status              != 'DUPLICATE') loop
                     
                    vDuplicate_found := 'Y';                             
                         
                   end loop;            
                end if;
                
                If vCurrent_Parent_Trx_id <> CFGREC.vt_Parent_Trx_id then  -- New Parent Trx Id
                  xxcp_global.gPassThroughCode := 0;
                End if;
                
                vCurrent_Parent_Trx_id := CFGREC.vt_Parent_Trx_id;
                
                -- Explosion           
                
                -- Branch for Dynamic Explosions
                IF cfgrec.Asg_Explosion_id > 0 THEN
                   -- Explosion Processing
                   IF cfgrec.Source_Type_id != vLast_Source_Type_id THEN
                      vInternalErrorCode := xxcp_dynamic_sql.ReadExplosionDef(cExplosion_id    => cfgrec.Asg_Explosion_id,
                                                                              cExplosion_sql   => vExplosion_Sql,
                                                                              cSummary_columns => vExplosion_Summary);

                      vLast_Source_Type_id := cfgrec.Source_Type_id;
                   END IF;

                   If vInternalErrorCode = 0 then
                     -- Call Explosion
                     vInternalErrorCode := xxcp_dynamic_sql.ExplosionInitProcess(
                                                           cSource_type_id    => cfgrec.Source_type_id,
                                                           cSource_rowid      => cfgrec.Source_rowid,
                                                           cInterface_id      => cfgrec.vt_interface_id,
                                                           cExplosion_sql     => vExplosion_Sql,
                                                           cExplosion_summary => vExplosion_summary,
                                                           cColumncount       => 2,
                                                           cRowsfound         => vRowsfound,
                                                           cTransaction_type  => cfgrec.vt_transaction_type,
                                                           cTransaction_class => cfgrec.vt_transaction_class);
                   End If;

                   IF vRowsFound > 0 AND vInternalErrorCode = 0 THEN

                        FOR jx IN 1 .. vRowsFound LOOP

                           i := i + 1;

                           vExplosion_Source_rowid := xxcp_dynamic_sql.gDataColumn1(jx);
                           vExplosion_Trx_id       := xxcp_dynamic_sql.gDataColumn2(jx);
                           vTransaction_Type       := xxcp_dynamic_sql.gDataColumn3(jx);
                           vTransaction_Class      := xxcp_dynamic_sql.gDataColumn4(jx);
                           vExplosion_Trx_type_id  := xxcp_dynamic_sql.gDataColumnTypeId(jx);
                           vExplosion_Trx_class_id := xxcp_dynamic_sql.gDataColumnClassId(jx);
                           
                           xxcp_global.gCommon(1).Explosion_Rowid          := vExplosion_Source_Rowid;
                           xxcp_global.gCommon(1).Explosion_Transaction_id := vExplosion_Trx_id;
                           xxcp_global.gCommon(1).Explosion_RowsFound      := vRowsFound;

                           -- Record Checks must work with the orginial interface values
                           If Record_Checks(cSource_Rowid      => CFGRec.SOURCE_ROWID, 
                                            cTransaction_Table => CFGRec.VT_Transaction_Table,
                                            cTransaction_Class => CFGRec.VT_Transaction_Class,
                                            cDuplicate_check   => vDuplicate_Check,
                                            cDuplicate_found   => vDuplicate_Found) = 0 then
                        
                                  XXCP_TE_CONFIGURATOR.Control(
                                               cSource_Assignment_ID     => vSource_Assignment_id,
                                               cSet_of_books_id          => CFGRec.Source_Set_of_books_id,
                                               cTransaction_Date         => CFGRec.VT_Transaction_Date,
                                               cSource_id                => vSource_id,
                                               cSource_Table_id          => CFGRec.Source_table_id,
                                               cSource_Type_id           => vExplosion_Trx_Type_id,
                                               cSource_Class_id          => vExplosion_Trx_Class_id,
                                               cTransaction_id           => vExplosion_Trx_id,
                                               cSourceRowid              => CFGRec.Source_Rowid,
                                               cParent_Trx_id            => CFGRec.VT_Parent_Trx_id,
                                               cTransaction_Set_id       => CFGRec.Transaction_Set_id,
                                               cSpecial_Array            => 'N', 
                                               cKey                      => NULL, 
                                               cTransaction_Table        => CFGRec.VT_Transaction_Table,
                                               cTransaction_Type         => CFGRec.VT_Transaction_Type,
                                               cTransaction_Class        => vTransaction_Class,
                                               cInternalErrorCode        => vInternalErrorCode);
                             End If;     
                                                                       
                           EXIT WHEN vInternalErrorCode > 0;
                           
                        END LOOP;
                     
                     ELSIF vInternalErrorCode = 0 THEN
                        vInternalErrorCode := 3441;
                     END IF;
                  
             Else -- Normal Processing  
                IF XXCP_TE_BASE.IGNORE_CLASS(CFGREC.VT_TRANSACTION_TABLE, vTransaction_Class) THEN
                  BEGIN
                    UPDATE XXCP_GL_INTERFACE
                       SET VT_STATUS              = 'IGNORED',
                           VT_INTERNAL_ERROR_CODE = NULL,
                           VT_DATE_PROCESSED      = XXCP_GLOBAL.SYSTEMDATE
                     WHERE ROWID = CFGREC.SOURCE_ROWID;

                     EXCEPTION WHEN OTHERS THEN NULL;
                  END;

                -- 02.05.02
                -- If it is a duplicate then it will be marked as DUPLICATE and not processed.
                ELSIF vDuplicate_check = 'Y' and vDuplicate_found = 'Y' then 
                
                     UPDATE XXCP_GL_INTERFACE
                       SET VT_STATUS              = 'DUPLICATE',
                           VT_INTERNAL_ERROR_CODE = NULL,
                           VT_DATE_PROCESSED      = XXCP_GLOBAL.SYSTEMDATE
                     WHERE ROWID = CFGREC.SOURCE_ROWID;
                     
                -- First/Second Passthrough
                ELSIF xxcp_global.gPassThroughCode > 0 then
              
                     Update xxcp_gl_interface p
                        set p.vt_status               = decode(xxcp_global.gPassThroughCode,7022,'SUCCESSFUL',
                                                                                            7023,'SUCCESSFUL',
                                                                                            7026,'PASSTHROUGH',
                                                                                            7027,'PASSTHROUGH')
                           ,p.vt_internal_Error_code  = Null
                           ,p.vt_status_code          = xxcp_global.gPassThroughCode
                           ,p.vt_date_processed       = xxcp_global.SystemDate
                      where p.rowid                   = CFGREC.SOURCE_ROWID;
     
                  ELSE

                      XXCP_TE_CONFIGURATOR.Control(
                                               cSource_Assignment_ID     => vSource_assignment_id,
                                               cSet_of_books_id          => CFGRec.Source_Set_of_books_id,
                                               cTransaction_Date         => CFGRec.VT_Transaction_Date,
                                               cSource_id                => vSource_id,
                                               cSource_Table_id          => CFGRec.Source_table_id,
                                               cSource_Type_id           => CFGRec.Source_type_id,
                                               cSource_Class_id          => CFGRec.Source_Class_id,
                                               cTransaction_id           => CFGRec.VT_Transaction_ID,
                                               cSourceRowid              => CFGRec.Source_Rowid,
                                               cParent_Trx_id            => CFGRec.VT_Parent_Trx_id,
                                               cTransaction_Set_id       => CFGRec.Transaction_Set_id,
                                               cSpecial_Array            => 'N', 
                                               cKey                      => NULL, 
                                               cTransaction_Table        => CFGRec.VT_Transaction_Table,
                                               cTransaction_Type         => CFGRec.VT_Transaction_Type,
                                               cTransaction_Class        => vTransaction_Class,
                                               cInternalErrorCode        => vInternalErrorCode);
                 
                  End If; -- NEW              

             END IF; -- End Assignment

              -- check status
              IF xxcp_global.Get_Release_Date IS NOT NULL THEN -- Release Date
                Stop_Whole_Transaction; 
              ELSIF NVL(vInternalErrorCode, 0) = 0 THEN
                UPDATE xxcp_gl_interface
                   SET vt_status              = 'TRANSACTION',
                       vt_internal_error_code = NULL,
                       vt_date_processed      = xxcp_global.systemdate
                 WHERE ROWID = cfgrec.source_rowid
                   AND vt_status NOT IN ('IGNORED','DUPLICATE');
              ELSIF vInternalerrorcode = -1 THEN
                UPDATE xxcp_gl_interface
                   SET vt_status              = 'SUCCESSFUL',
                       vt_internal_error_code = NULL,
                       vt_date_processed      = xxcp_global.systemdate,
                       vt_status_code         = 7001
                 WHERE ROWID = cfgrec.source_rowid;
              ELSIF vInternalErrorCode BETWEEN 7021 and 7028 then
                Pass_Through_Whole_Trx(CFGRec.Source_Rowid, vInternalErrorCode);
              ELSIF NVL(vInternalErrorCode, 0) <> 0 THEN
                UPDATE xxcp_gl_interface
                   SET vt_status              = 'ERROR',
                       vt_internal_error_code = 12000,
                       vt_date_processed      = xxcp_global.systemdate
                 WHERE ROWID = cfgrec.source_rowid;
                       
                 -- Flag Error
                 gEngine_Error_Found := TRUE;
                       
              END IF;
                    
              -- emergency exit (out of disk space) 
              IF vInternalErrorCode BETWEEN 3550 AND 3559 THEN
                ROLLBACK;
                vjob_status := 6;
                EXIT; 
              END IF;


                -- ##
                -- ## COMMIT CONFIGURED ROWS
                -- ##
              
                IF vAssignmentCommitCnt >= gAssignment_Commit THEN
                  xxcp_global.systemdate := SYSDATE;
                  Read_Engine_Controls;
                  COMMIT;
                  vAssignmentCommitCnt  := 0; 
                End If;
                
                If vOracleCommitCnt >= gOracle_Check_Count then
                  vOracleCommitCnt := 0;
                  vJob_status := 
                    xxcp_management.Has_Oracle_Been_Terminated(
                                         cRequest_id      => vRequest_id,
                                         cSource_Activity => vSource_activity,
                                         cSource_Group_id => cSource_group_id,
                                         cConc_Request_Id => cConc_request_id
                                         );
                  EXIT WHEN vjob_status > 0; -- controlled exit
                END IF;

          END LOOP; 

          xxcp_dynamic_sql.Close_Session;
          -- #
          -- # Clear Common vars
          -- #
          xxcp_global.gCommon(1).current_transaction_table := NULL;
          xxcp_global.gCommon(1).current_transaction_id    := NULL;
          xxcp_global.gCommon(1).current_parent_trx_id     := NULL;
          xxcp_global.gCommon(1).current_interface_ref     := NULL; 
          xxcp_global.Set_New_Trx_Flag('N');

          COMMIT; -- Final Configuration Commit.
        
          --
          IF xxcp_global.gCommon(1).Custom_events = 'Y' THEN
            xxcp_custom_events.After_assignment_processing(cSource_id            => vSource_id,
                                                           cSource_assignment_id => vSource_assignment_id);
          END IF;
        
          vTiming(vTimingCnt).CF     := xxcp_reporting.Get_Seconds_Diff(cSec1 => vTiming(vTimingCnt).CF_last, cSec2 => xxcp_reporting.Get_Seconds);
          vTiming(vTimingCnt).CF_End_Date := Sysdate;
        
          -- ***********************************************************************************************************
          --
          --  Set associated Transactions to error if anyone of the Transactions is a set has errored in configuration
          --
          -- ***********************************************************************************************************
          --
          xxcp_foundation.show('Reporting Configuration errors....');
          xxcp_global.SystemDate := SYSDATE;
          vInternalErrorCode := 0;
          FOR ConfErrRec IN ConfErr(vSource_Assignment_id, vRequest_id) LOOP
          
            BEGIN
              UPDATE xxcp_gl_interface
                 SET vt_status              = 'ERROR',
                     vt_internal_Error_code = 12824, -- Associated errors
                     vt_date_processed      = xxcp_global.SystemDate
               WHERE vt_request_id = vRequest_id
                 AND vt_Source_Assignment_id = vSource_Assignment_id
                 AND vt_status IN ('ASSIGNMENT', 'TRANSACTION')
                 AND vt_parent_trx_id = ConfErrRec.vt_parent_trx_id
                 AND vt_transaction_table = ConfErrRec.vt_Transaction_table;

               -- Flag Error
               If SQL%ROWCOUNT > 0 then
                 gEngine_Error_Found := TRUE;  
               End If;            

            EXCEPTION
              WHEN OTHERS THEN
                vInternalErrorCode := 12819; -- Update Failed.
            
            END;
          END LOOP;
        

          --## ***********************************************************************************************************
          --##
          --##                                    Transaction Engine
          --##
          --## ***********************************************************************************************************
        
          vTiming(vTimingCnt).TE_Last    := xxcp_reporting.Get_Seconds;
          vTiming(vTimingCnt).TE_Records := 0;
          vTiming(vTimingCnt).TE_Start_Date := Sysdate;
        
          vExtraLoop             := FALSE;
          vExtraClass            := NULL;
          xxcp_global.SystemDate := SYSDATE;
          i := 0;
          vTransactionCommitCnt := 0;
          
          IF vInternalErrorCode = 0 AND vJob_Status = 0 THEN
          
            xxcp_foundation.show('Transactions....');
            xxcp_wks.Reset_Source_Segments;
            gBalancing_Array      := xxcp_wks.Clear_Segments;
            ClearWorkingStorage;
            vCurrent_Parent_Trx_id := 0;
 
            FOR GLIRec IN  GLI(pSource_id            => vSource_id, 
                               pSource_Assignment_id => vSource_assignment_id,
                               pRequest_id           => vRequest_id,
                               pTable_Group_id       => cTable_Group_id
                               )
            LOOP
              
                  vInternalErrorCode := 0;
                  EXIT WHEN vJob_Status > 0;
                  vTiming(vTimingCnt).TE_Records := vTiming(vTimingCnt).TE_Records + 1;
                  xxcp_global.gCommon(1).explosion_id := GLIRec.ASG_Explosion_Id; 
                  --
                  -- ***********************************************************************************************************
                  --        Set the balancing Setments to be written to the Column Rules Array
                  -- ***********************************************************************************************************
                  --
                  -- Poplulate the Balancing Array
                  xxcp_wks.Source_Balancing(1) := NULL;
                  xxcp_wks.Source_Balancing(2) := GLIRec.be2; -- User_Je_Source_Name
                  xxcp_wks.Source_Balancing(3) := GLIRec.be3; -- Group Id
                  xxcp_wks.Source_Balancing(4) := GLIRec.be4; -- User Je Category Name
                  xxcp_wks.Source_Balancing(5) := NULL;
                  xxcp_wks.Source_Balancing(6) := GLIRec.be6; -- Reference 1
                  xxcp_wks.Source_Balancing(7) := GLIRec.be7; -- Reference 2
                  xxcp_wks.Source_Balancing(8) := GLIRec.be8; -- Reference 3
                  xxcp_wks.Source_Balancing(9) := NULL;
                  xxcp_wks.Source_Balancing(10):= NULL;

                  --
                  -- ***********************************************************************************************************
                  --         Move Accounting Codes from Master Record to Array
                  -- ***********************************************************************************************************
                  -- Populate Source Account Segments
                  xxcp_wks.Source_Segments(01) := GLIRec.Segment1;
                  xxcp_wks.Source_Segments(02) := GLIRec.Segment2;
                  xxcp_wks.Source_Segments(03) := GLIRec.Segment3;
                  xxcp_wks.Source_Segments(04) := GLIRec.Segment4;
                  xxcp_wks.Source_Segments(05) := GLIRec.Segment5;
                  xxcp_wks.Source_Segments(06) := GLIRec.Segment6;
                  xxcp_wks.Source_Segments(07) := GLIRec.Segment7;
                  xxcp_wks.Source_Segments(08) := GLIRec.Segment8;
                  xxcp_wks.Source_Segments(09) := GLIRec.Segment9;
                  xxcp_wks.Source_Segments(10) := GLIRec.Segment10;
                  xxcp_wks.Source_Segments(11) := GLIRec.Segment11;
                  xxcp_wks.Source_Segments(12) := GLIRec.Segment12;
                  xxcp_wks.Source_Segments(13) := GLIRec.Segment13;
                  xxcp_wks.Source_Segments(14) := GLIRec.Segment14;
                  xxcp_wks.Source_Segments(15) := GLIRec.Segment15;

                  xxcp_global.Set_New_Trx_Flag('N');

                  --
                  -- ***********************************************************************************************************
                  --         When a New Transaction is detected then flush the current buffer
                  -- ***********************************************************************************************************
                  --
                  IF (vCurrent_Parent_Trx_id <> GLIRec.vt_parent_Trx_id) THEN
                  
                    vTransaction_Error := FALSE;
                  
                    --
                    -- !! ******************************************************************************************************
                    --                             FLUSH TRANSACTION 
                    -- !! ******************************************************************************************************
                    --
                    IF vCurrent_Parent_Trx_id <> 0 THEN
                      vTransactionCommitCnt := vTransactionCommitCnt + 1; -- Number of records process since last commit.
                      vOracleCommitCnt      := vOracleCommitCnt + 1;
                      k := FlushRecordsToTarget(cGlobal_Rounding_Tolerance => vGlobal_Rounding_Tolerance,
                                                cGlobalPrecision           => vGlobalPrecision,
                                                cTransaction_Type          => vTransaction_Type,
                                                cInternalErrorCode         => vInternalErrorCode);
                    END IF;
                   
                    xxcp_global.Set_New_Trx_Flag('Y');

                    --
                    -- ***********************************************************************************************************
                    --            Store Variables that are only set once per Parent Trx Id
                    -- ***********************************************************************************************************
                    --
                    xxcp_wks.Trace_Log     := NULL;
                    xxcp_global.gCommon(1).current_transaction_table := GLIRec.vt_Transaction_Table;
                    xxcp_global.gCommon(1).current_parent_trx_id     := GLIRec.vt_Parent_Trx_id;
                    vCurrent_Parent_Trx_id := GLIRec.vt_Parent_Trx_id;
                    vTransaction_Type      := GLIRec.vt_Transaction_Type;
                    ClearWorkingStorage;
                    --
                    -- ***********************************************************************************************************
                    --                         Extra Loops for different Classes
                    -- ***********************************************************************************************************
                    --                  
                    vExtraLoop  := FALSE;
                    vExtraClass := NULL;
                    FOR j IN 1 .. xxcp_global.gSRE.COUNT LOOP

                      IF GLIRec.vt_Transaction_Table = xxcp_global.gSRE(j).Transaction_table THEN
                        vExtraClass := xxcp_global.gSRE(j).Extra_Record_type;
                        vExtraLoop  := TRUE;
                        EXIT;
                      END IF;
                    END LOOP;
                  
                  ELSE
                    vExtraLoop  := FALSE; -- !!!!
                    vExtraClass := NULL;
                  END IF;
                
                  xxcp_global.gCommon(1).current_transaction_id := GLIRec.vt_Transaction_id;
                  --
                  -- ***********************************************************************************************************
                  --                                   Store Working Variables
                  -- ***********************************************************************************************************
                  --
                  xxcp_global.gCommon(1).current_source_rowid    := GLIRec.Source_Rowid;
                  xxcp_global.gCommon(1).current_Interface_id    := GLIRec.vt_interface_id;
                  xxcp_global.gCommon(1).current_Accounting_date := GLIRec.Accounting_Date;
                  xxcp_global.gCommon(1).current_Batch_number    := GLIRec.Group_id;
                  xxcp_global.gCommon(1).current_Category_name   := GLIRec.Category_Name;
                  xxcp_global.gCommon(1).current_Source_name     := GLIRec.Source_name;
                  xxcp_global.gCommon(1).current_interface_ref   := GLIRec.vt_transaction_ref; 
                  xxcp_global.gCommon(1).current_creation_date   := GLIRec.source_creation_date;
                  xxcp_global.gCommon(1).calc_legal_exch_rate    := GLIRec.calc_legal_exch_rate;
                  --
                  -- ***********************************************************************************************************
                  --                                   Call the Transaction Engine
                  -- ***********************************************************************************************************
                  --
                  IF vTransaction_Error = FALSE THEN
                    vInternalErrorCode := 0; -- Reset for each row

                    i := i + 1; -- Count the number of rows processed.
                  
                    xxcp_wks.SOURCE_CNT  := xxcp_wks.SOURCE_CNT + 1;
                    xxcp_wks.SOURCE_ROWID(xxcp_wks.source_cnt)   := GLIRec.Source_Rowid;
                    xxcp_wks.SOURCE_SUB_CODE(xxcp_wks.source_cnt):= 0;
                    xxcp_wks.SOURCE_STATUS(xxcp_wks.source_cnt)  := '?';

                    -- *********************************************************************
                    -- Class Mapping 
                    -- *********************************************************************
                    IF GLIRec.class_mapping_req = 'Y' THEN
                        vClass_Mapping_Name := xxcp_te_base.class_mapping(
                                                           cSource_id              => vSource_id,
                                                           cClass_Mapping_Required => GLIRec.class_mapping_req,
                                                           cTransaction_Table      => GLIRec.vt_transaction_table,
                                                           cTransaction_Type       => GLIRec.vt_transaction_type,
                                                           cTransaction_Class      => GLIRec.vt_transaction_class,
                                                           cTransaction_id         => GLIRec.class_transaction_id,
                                                           cCode_Combination_id    => GLIRec.code_combination_id,
                                                           cAmount                 => GLIRec.Transaction_Cost,
                                                           cSystem_Link_id         => GLIRec.gl_sl_link_id);
                    END IF;                                                          
                                                           
                    -- *********************************************************************
                    -- End Inventory Logic
                    -- *********************************************************************


                    XXCP_TE_ENGINE.Control(cSource_assignment_id    => vSource_Assignment_id,
                                           cSource_Table_id         => GLIRec.Source_Table_id,
                                           cSource_Type_id          => GLIRec.Source_Type_id,
                                           cSource_Class_id         => GLIRec.Source_Class_id,
                                           cTransaction_id          => GLIRec.vt_Transaction_id,
                                           cParent_Trx_id           => GLIRec.vt_Parent_Trx_id,
                                           cSourceRowid             => GLIRec.Source_Rowid,
                                           cParent_Entered_Amount   => GLIRec.Parent_Entered_Amount,
                                           cParent_Entered_Currency => GLIRec.Parent_Entered_Currency,
                                           cClass_Mapping           => GLIRec.class_mapping_req,
                                           cClass_Mapping_Name      => vClass_Mapping_Name,
                                           --
                                           cInterface_ID            => GLIRec.VT_Interface_id,
                                           cExtraLoop               => vExtraLoop,
                                           cExtraClass              => vExtraClass,
                                           cTransaction_set_id      => GLIRec.Transaction_Set_id,
                                           --
                                           cTransaction_Table       => GLIRec.VT_Transaction_Table,
                                           cTransaction_Type        => GLIRec.VT_Transaction_Type,
                                           cTransaction_Class       => GLIRec.vt_Transaction_Class,
                                           cInternalErrorCode       => vInternalErrorCode);

                    xxcp_global.gCommon(1).current_trading_set := NULL;

                    -- Report Errors
                    IF vInternalErrorCode <> 0 THEN
                      Error_Whole_Transaction(cInternalErrorCode => vInternalErrorCode);
                      vTransaction_Error := TRUE;
                    END IF;

                  END IF;
                  --
                  -- ***********************************************************************************************************
                  --                     Test to See if Oracle or the User has terminated this process
                  --                                   And Commit after Each 1000 Records processed
                  -- ***********************************************************************************************************
                  --
                  IF vTransactionCommitCnt >= gTransaction_Commit THEN
                    vTransactionCommitCnt := 0;
                    xxcp_global.SystemDate := SYSDATE;
                    Read_Engine_Controls;
                    COMMIT;
                  END IF;

                  IF vOracleCommitCnt >= gOracle_Check_Count THEN
                    
                    vOracleCommitCnt := 0;
                    
                    vJob_Status := 
                     xxcp_management.Has_Oracle_been_Terminated(cRequest_id      => vRequest_id,
                                                                cSource_Activity => vSource_activity,
                                                                cSource_Group_id => cSource_group_id,
                                                                cConc_Request_Id => cConc_request_id);
                                         
                    EXIT WHEN vJob_Status > 0; -- Controlled Exit
                  END IF;

            END LOOP;
            
            -- Pass Through 
            If vJob_Status = 0 and xxcp_te_base.GetPassThroughActive = 'Y' then
               Begin
           
                 XXCP_PROCESS_DML_GL.Process_Pass_Through(
                           cSource_table         => 'GL_INTERFACE'
                          ,cSource_assignment_id => vSource_Assignment_id
                          ,cSource_Instance_id   => vSource_Instance_id
                          ,cProcess_history_id   => 0 --vPass_Through_History_id
                          ,cRequest_id           => xxcp_global.gCommon(1).Current_request_id
                          ,cErrorMessage         => vErrorMessage
                          ,cInternalErrorCode    => vInternalErrorCode);
                 
                 If vInternalErrorCode = 0 then
               
                   update xxcp_gl_interface g
                     set vt_status   = 'SUCCESSFUL' 
                    where g.vt_Source_assignment_id = vSource_Assignment_id
                      and g.vt_request_id           = xxcp_global.gCommon(1).Current_request_id
                      and g.vt_source_assignment_id = vSource_Assignment_id
                      and g.vt_status               = 'PASSTHROUGH'
                      and g.vt_status_code          between 7025 and 7028;
                 Else
                   update xxcp_gl_interface g
                     set vt_status  = 'ERROR'
                        ,vt_internal_Error_code = 12827
                    where g.vt_Source_assignment_id = vSource_Assignment_id
                      and g.vt_request_id           = xxcp_global.gCommon(1).Current_request_id
                      and g.vt_source_assignment_id = vSource_Assignment_id
                      and g.vt_status               = 'PASSTHROUGH'
                      and g.vt_status_code          between 7025 and 7028;
                    
                 End If;     
                 Commit;
                 
                 Exception when OTHERS then 
                     vInternalErrorCode := 12827;
                     xxcp_foundation.FndWriteError(vInternalErrorCode,'Pass Through Insert Failed');
                 
               End;
            End If;           

            xxcp_te_base.Close_Cursors;

            --
            --  !!***********************************************************************************************************
            --                     Flush the Buffer for a Final Time
            -- !! ***********************************************************************************************************
            --
            IF vCurrent_Parent_Trx_id <> 0 AND vInternalErrorCode = 0 THEN
              IF vInternalErrorCode = 0 THEN

                k := FlushRecordsToTarget(cGlobal_Rounding_Tolerance => vGlobal_Rounding_Tolerance,
                                          cGlobalPrecision           => vGlobalPrecision,
                                          cTransaction_Type          => vTransaction_type,
                                          cInternalErrorCode         => vInternalErrorCode);
              END IF;
            END IF;
          END IF; -- Internal Error Check GLI
        END IF; -- End Internal Error Check

        vTiming(vTimingCnt).TE := xxcp_reporting.Get_Seconds_Diff(cSec1 => vTiming(vTimingCnt).TE_last, cSec2 => xxcp_reporting.Get_Seconds);

        xxcp_foundation.show('Clear down...');
        xxcp_global.SystemDate := SYSDATE;
        -- *******************************************************************
        -- Error out records remaining with a Processing status after the run
        -- *******************************************************************

        BEGIN
          UPDATE xxcp_gl_interface k1
             SET vt_status              = 'ERROR',
                 vt_internal_error_code = DECODE(vt_status,'TRANSACTION',12999,'ASSIGNMENT',12998,'GROUPING',12996),
                 vt_date_processed      = xxcp_global.SystemDate
           WHERE k1.ROWID = ANY (SELECT g.ROWID Source_Rowid
                                   FROM xxcp_gl_interface g
                                  WHERE g.vt_request_id = xxcp_global.gCommon(1).current_request_id
                                    AND g.vt_source_assignment_id = vSource_Assignment_id
                                    AND g.vt_status IN ('ASSIGNMENT', 'TRANSACTION', 'GROUPING'));

           -- Flag Error
           If SQL%ROWCOUNT > 0 then
             gEngine_Error_Found := TRUE;  
           End If;            

           EXCEPTION WHEN OTHERS THEN NULL;
        END;

        -- #
        -- # Clear Common vars
        -- #
        xxcp_global.gCommon(1).current_transaction_table := NULL;
        xxcp_global.gCommon(1).current_transaction_id    := NULL;
        xxcp_global.gCommon(1).current_parent_trx_id     := NULL;
        xxcp_global.gCommon(1).current_interface_ref     := NULL; 

        --
        -- ***********************************************************************************************************
        --                     Clear Down
        -- ***********************************************************************************************************
        --
        ClearWorkingStorage;
        --
        -- ***********************************************************************************************************
        --                     DeActive the Lock in XXCP_ACTIVITY_CONTROL
        -- ***********************************************************************************************************
        --
        COMMIT;
        vTiming(vTimingCnt).TE_End_Date := Sysdate;
        vTimingCnt := vTimingCnt + 1;
      END LOOP; -- SR1

      -- Error Completion Status
      If gEngine_Error_Found and vJob_Status = 0 then
        If vError_Completion_Status  = 'W' then
          gForce_Job_Warning := TRUE;
        Elsif vError_Completion_Status  = 'E' then
          gForce_Job_Error := TRUE;
        End if;
      End If;
      
      -- Distributer
      IF vJob_Status = 0 and not gForce_Job_Error THEN  -- Normal or Warning. Dont Distribute if Error
         vJob_Status := xxcp_data_distributor.Control(
                                cSource_id        => vSource_id,
                                cSource_Group_id  => cSource_Group_id,
                                cRequest_id       => Null, -- Leave as NULL 
                                cJournal_Group_id => xxcp_global.gCommon(1).current_trx_group_id);
      END IF;     
           
      -- 03.06.15 
      -- Flat File Output  
      vFlat_File_Flag := 'N';
      
      BEGIN
        select nvl(flat_file,'N')
          into vFlat_File_Flag
          from xxcp_sys_sources
         where source_id = vSource_id;
      EXCEPTION
        when others then
          vFlat_File_Flag := 'N';
      END;       

      -- Export File         
      -- 02.06.09 Write record out to file only if Flat file is enabled. 
      IF vJob_Status = 0 and xxcp_global.gCommon(1).flat_file = 'Y' and not gForce_Job_Error THEN

        vJob_Status := xxcp_file_upload.exportfile(csource_id  => vSource_id,
                                                   cdebug      => gEngine_Trace,
                                                   cRequest_id => vRequest_id);

      END IF; 
      
      IF xxcp_global.gCommon(1).Custom_events = 'Y' THEN
        xxcp_custom_events.After_Processing(cSource_id       => vSource_id,
                                            cSource_Group_id => cSource_Group_id,
                                            cRequest_id      => vRequest_id,
                                            cJob_Status      => vJob_Status);
      END IF;

      xxcp_management.DeActivate_lock_process(cRequest_id      => vRequest_id,
                                              cSource_Activity => vSource_Activity,
                                              cSource_Group_id => cSource_Group_id
                                              );


      xxcp_te_base.ClearDown;
      xxcp_memory_pack.ClearDown;
      xxcp_dynamic_sql.Close_Session;

      COMMIT;

    ELSIF NVL(vJob_Status,0) = 0 THEN

      --
      -- Report If this process was already in use
      --
      xxcp_foundation.show('Engine already in use');
      vJob_Status := 1;
    END IF;

    -- Force Warning/Error
    If gForce_Job_Warning then
      vJob_Status := 15;
    Elsif gForce_Job_Error then
      vJob_Status := 16;
    End if;

    xxcp_te_base.Write_Timings(vTiming, vTiming_Start);
    --
    -- ************************************************************************************************
    --                     Email Job Details to Users
    -- ************************************************************************************************
    --
    xxcp_comms.Send_job_details(cRequest_id => xxcp_global.gCommon(1).Current_Request_id,
                                cJob_Status => vJob_Status);
   --
    -- ************************************************************************************************
    --                     Show Time Statistics when called from SQL*Plus
    -- ************************************************************************************************
    --
    xxcp_global.gCommon(1).Current_Request_id := NULL;
    --
    -- ***********************************************************************************************************
    --                     Return Job Status Code and Finish!!
    -- ***********************************************************************************************************
    --

    RETURN(vJob_Status);

  END Control;
    
  --
  -- Init
  --
  Procedure Init is
    
   Cursor LK(pLookup_type in varchar2) is  
     select lk1.Lookup_Code, lk1.numeric_code
       from xxcp_lookups lk1
      WHERE lookup_type = pLookup_type
         AND enabled_flag = 'Y';
         
   Cursor SP(pProfile_Category in varchar2, pProfile_Name in varchar2) is
    select p.profile_value
      from xxcp_sys_profile p
     where p.profile_name = pProfile_Name
       and p.profile_category = pProfile_Category;         
         
    vMonths varchar2(5);
         
  Begin
     -- Trace Mode 
    gEngine_Trace := 'N';
    For Rec in LK('TRACE GL ENGINE') loop
       gEngine_Trace := Rec.Lookup_Code;
    End Loop;
    
    -- Partition Months Start
    For Rec in SP('EV','PARTITIONING MONTHS START') loop
       vMonths := Rec.profile_value;
       gPartition_Months_start := ABS(to_number(vMonths))*-1;
    End Loop;   
    
    -- 03.06.13
    -- Partition Months End
    For Rec in SP('EV','PARTITIONING MONTHS END') loop
       vMonths := Rec.profile_value;
       gPartition_Months_end := ABS(to_number(vMonths));
    End Loop;   

    -- Ignored Status Months
    For Rec in SP('EV','IGNORED STATUS REMOVAL MONTHS') loop
       vMonths := Rec.profile_value;
       gIgnored_Delete_Months := ABS(to_number(vMonths))*-1;
    End Loop;   
    
    -- Ignored Removal Flag
    For Rec in SP('EV','IGNORED STATUS REMOVAL ON') loop
       gIgnored_Removal_Flag := Rec.profile_value; 
    End Loop;   
    
    -- Assignment Commit Count
    For Rec in SP('EV','ASSIGNMENT COMMIT FREQUENCY') loop
       gAssignment_Commit := Rec.profile_value; 
       If NOT gAssignment_Commit between 1 and 2000 then
        gAssignment_Commit := 10;
       End If;
    End Loop;   

    -- Transaction Commit Count
    For Rec in SP('EV','TRANSACTION COMMIT FREQUENCY') loop
       gTransaction_Commit := Rec.profile_value; 
       
       If NOT gTransaction_Commit between 1 and 2000 then
         gTransaction_Commit := 100;
       End If;   
    End Loop;       
  
    -- Terminate Check
    For Rec in SP('EV','TERMINATE CHECK FREQUENCY') loop
       gOracle_Check_Count := Rec.profile_value; 
       
       If NOT gTransaction_Commit between 1 and 4000 then
         gOracle_Check_Count := 1000;
       End If;   
    End Loop; 
    
    -- Running Status
    For Rec in SP('EV','RUNNING STATUS FREQUENCY') loop
       gRunning_Status_Count := Rec.profile_value; 
       
       If NOT gTransaction_Commit between 1 and 100000 then
         gRunning_Status_Count := 30000;
       End If;   
    End Loop;        
     
    -- Previous Request Id Range
    BEGIN
      SELECT profile_value
      INTO   gRequest_Id_Range
      FROM   xxcp_sys_profile
      WHERE  profile_category = 'EV'
      AND    profile_name     = 'INTERFACE VT REQUEST ID RANGE';
    EXCEPTION
      WHEN OTHERS THEN
        gRequest_Id_Range := 10000;    
    END;
    
    -- Set Partition Date (sysdate minus a number of months) It should be negitive number     
    gStart_Partition_Date := trunc(add_months(sysdate,gPartition_Months_start));     
    gEnd_Partition_Date   := trunc(add_months(sysdate,gPartition_Months_end));    
    
    -- Removing Ignored Records
    gIgnored_End_Removal_Date   := trunc(add_months(sysdate,gIgnored_Delete_Months)); 
    gIgnored_Start_Removal_Date := trunc(Add_Months(gIgnored_End_Removal_Date,-3));
    
    If gEngine_Trace = 'Y' then 
      xxcp_foundation.FndWriteError(100,'Partitioning RunTime Months: '||gStart_Partition_Date||' to '||gEnd_Partition_Date);  
      xxcp_foundation.FndWriteError(100,'Ignored Status Removal: '||gIgnored_Start_Removal_Date||' to '|| gIgnored_End_Removal_Date);  
    End If;  
    
  End Init;


BEGIN
  
  Init;

END XXCP_GL_ENGINE;
/
