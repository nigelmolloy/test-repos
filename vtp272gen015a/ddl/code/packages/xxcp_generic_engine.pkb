CREATE OR REPLACE PACKAGE BODY XXCP_GENERIC_ENGINE AS
  /******************************************************************************
                          V I R T U A L  T R A D E R  L I M I T E D
              (Copyright 2001-2012 Virtual Trader Ltd. All rights Reserved)

     NAME:       XXCP_GENERIC_ENGINE
     PURPOSE:    Generic Wrapper

     REVISIONS:
     Ver        Date      Author  Description
     ---------  --------  ------- ------------------------------------
     03.06.15   24/03/12  Keith   Generic Engine to reduce the number of wrappers 
     03.06.15   20/05/12  Keith   added gParent_Commit_Count 
     03.06.17   27/06/12  Mark    Remove call to invoice numbering
     03.06.18   27/06/12  Nigel   ICS interface changes.
     03.06.19   03/07/12  Nigel   Changed Flush package for ICS
     03.06.20   10/07/12  Nigel   Changed the integrity check, and removed check for classification
     03.06.21   12/07/12  Mat     OOD-206 Control - Fix invalid Duplicate Found Exe Immediate statement that was failing upon execution
     03.06.22   10/08/12  Simon   Fixed Grouping Rule syntax error (OOD-251).
     03.06.23   12/09/12  Nigel   Added Unmatched transactions
     03.06.24   01/10/12  Nigel   Added ICS Approval_restricted 
     03.06.25   26/10/12  Nigel   Added Multi-threading (OOD-138)
     03.06.26   29/10/12  Simon   Added Duplicate Check Days
     03.06.27   30/10/12  Simon   Dynamic Explosion issue (OOD-249).
                                  Added Date Range Limit check (OOD-378)
  *************************************************************************************************/

  -- Editor Setting: 2 space tab stop

  gInterface_Table_Name       xxcp_sys_sources.source_base_table%type;
  gInterface_View_Name        xxcp_sys_engine_ctl.engine_interface_view%type;
  gSource_Short_Name          xxcp_sys_sources.source_short_name%type;

  -- New Dynamic SQL
  gDDL_Interface_Select       varchar2(12000);
  gDDL_Command                varchar2(32000);
  gDDL_Flush_DML              varchar2(2000);
  gDDL_Flush_Hist             varchar2(5000);
  -- 03.06.18
  gDDL_Flush_ICS_DML          varchar2(2000);
  
  gEngine_Trace               varchar2(1);
  gRequest_Id_Range           NUMBER;
  
  gStart_Partition_Date       date;
  gEnd_Partition_Date         date := Sysdate;
  gParent_Commit_Count        Number(7) := 2000;

  vTiming                     xxcp_global.gTiming_rec;
  vTimingCnt                  PLS_INTEGER;

  gBalancing_Array            XXCP_DYNAMIC_ARRAY := XXCP_DYNAMIC_ARRAY(' ',' ',' ',' ',' ',' ',' ',' ',' ',' ');
  
  vIC_Numbering_Flag          varchar2(1);
  vFlat_File_Flag             varchar2(1);
  gEngine_Error_Found         boolean := FALSE;
  gForce_Job_Error            boolean := FALSE;
  gForce_Job_Warning          boolean := FALSE;
 
  gUse_OE_Headers             varchar2(1) := 'N';
  gOE_SOURCE_id               xxcp_sys_sources.source_id%type := 19;
  
  -- *************************************************************
  --                     Software Version Control
  -- This package can be checked with SQL to ensure it's installed
  -- select packagename.software_version from dual;
  -- *************************************************************
  FUNCTION Software_Version RETURN VARCHAR2 IS
  BEGIN
    RETURN('Version [03.06.27] Build Date [30-OCT-2012] Name [XXCP_GENERIC_ENGINE]');
  END Software_Version;
  --
  -- Setup_Wrapper_Source_Group
  --
  Procedure Setup_Wrapper_Source_Group(cSource_Short_Name in varchar2) is
     
    Cursor LK(pLookup_type in varchar2) is  
     select lk1.Lookup_Code, lk1.numeric_code
       from xxcp_lookups lk1
      WHERE lookup_type = pLookup_type
         AND enabled_flag = 'Y';

    vIgnored_Delete_Months      Number := -12; -- This should be a negative number
    vIgnored_Start_Removal_Date date;
    vIgnored_End_Removal_Date   date := trunc(Add_Months(Sysdate,vIgnored_Delete_Months));
    vRemove_Filtered_Date date; 
    
    vTrace_Lookup xxcp_lookups.lookup_type%type;    
    vTrace_Msg    xxcp_errors.error_message%type;
                 
  Begin
     -- Trace Mode 
    gEngine_Trace := 'N';
    vTrace_Lookup := 'TRACE '||cSource_Short_Name||' ENGINE';
    gSource_Short_Name := cSource_Short_Name;
    
    For Rec in LK(vTrace_Lookup) loop
       gEngine_Trace := Rec.Lookup_Code;
    End Loop;
   
    xxcp_te_base.Get_Engine_Dates(cStart_Partition_Date       => gStart_Partition_Date, 
                                  cEnd_Partition_Date         => gEnd_Partition_Date,
                                  cRemove_Filtered_Date       => vRemove_Filtered_Date,
                                  cIgnored_Start_Removal_Date => vIgnored_Start_Removal_Date,
                                  cIgnored_End_Removal_Date   => vIgnored_End_Removal_Date,
                                  cRequest_Id_Range           => gRequest_Id_Range);
    
    If gEngine_Trace = 'Y' then 
      vTrace_Msg := 'Partitioning RunTime Months: '||gStart_Partition_Date||' to '||gEnd_Partition_Date;
      vTrace_Msg := vTrace_Msg || chr(10)||'Ignored Status Removal: '||vIgnored_Start_Removal_Date||' to '|| vIgnored_End_Removal_Date;
      vTrace_Msg := vTrace_Msg || chr(10)||'Filter Removal Date: '||vRemove_Filtered_Date;  
      
      xxcp_foundation.FndWriteError(100,'Runtime Engine Dates', vTrace_Msg);
      
    End If;  
    
   End Setup_Wrapper_Source_Group;
  
  -- *************************************************************
  --                  ClearWorkingStorage
  -- *************************************************************
  PROCEDURE ClearWorkingStorage IS

  BEGIN
    -- Processed Records
    xxcp_wks.Trace_Log     := Null;
    xxcp_wks.WORKING_CNT := 0;
    xxcp_wks.WORKING_RCD.DELETE;
    -- Current Records before processing
    xxcp_wks.SOURCE_CNT := 0;
    xxcp_wks.SOURCE_ROWID.DELETE;
    xxcp_wks.SOURCE_STATUS.DELETE;
    xxcp_wks.SOURCE_SUB_CODE.DELETE;
    -- Balancing
    xxcp_wks.BALANCING_CNT := 0;
    xxcp_wks.BALANCING_RCD.DELETE;
     
  END ClearWorkingStorage;

  --  !! ***********************************************************************************************************
  --  !!   Stop_Whole_Transaction
  --  !! ***********************************************************************************************************
  PROCEDURE Stop_Whole_Transaction is
  Begin
   -- Set all transactions in group to error status
   gDDL_Command :=
       'Update '||gInterface_Table_Name||' r  
         set r.vt_status = ''WAITING''
            ,r.vt_internal_Error_code = 0 
            ,r.vt_date_processed  = Sysdate
         WHERE r.vt_request_id           = :1
           AND r.vt_Source_Assignment_id = :2
           AND r.vt_parent_trx_id        = :3
           AND NOT r.vt_status IN (''TRASH'', ''IGNORED'')';
         
     Begin
     
       Execute Immediate gDDL_Command using xxcp_global.gCommon(1).Current_request_id, 
                                            xxcp_global.gCommon(1).Current_assignment_id,
                                            xxcp_global.gCommon(1).Current_parent_Trx_id;
         
       Exception When Others Then
          xxcp_foundation.FndWriteError(150,'Stop Whole Transaction-1: SQLERRM='||SQLERRM, gDDL_Command);
          Raise INVALID_CURSOR;       
         
     End;
     -- Update Record that actually caused the problem.
     Begin
       gDDL_Command :=
          'Update '||gInterface_Table_Name||' r 
              Set r.vt_status          = ''WAITING''
                 ,r.VT_DATE_PROCESSED  = sysdate 
            WHERE r.ROWID         = :1
              AND r.vt_request_id = :2 ';

       Execute Immediate gDDL_Command using xxcp_global.gCommon(1).Current_source_rowid, 
                                            xxcp_global.gCommon(1).Current_request_id;

       Exception When Others Then
          xxcp_foundation.FndWriteError(150,'Stop Whole Transaction-2: SQLERRM='||SQLERRM, gDDL_Command);
          Raise INVALID_CURSOR;       
     End;

     ClearWorkingStorage;

  End Stop_Whole_Transaction;
  --  !! ***********************************************************************************************************
  --  !!                                   Error_Whole_Transaction
  --  !!                If one part of the wrapper finds a problem whilst processing a record,
  --  !!                          then we need to error the whole transaction.
  --  !! ***********************************************************************************************************
  PROCEDURE Error_Whole_Transaction(cInternalErrorCode IN OUT NUMBER) IS
  BEGIN

    -- Flag Error
    gEngine_Error_Found := TRUE;  

    -- Set all transactions in group to error status
    gDDL_Command := 
        'UPDATE '||gInterface_Table_Name||' r
           SET r.vt_status = ''ERROR'', 
               r.vt_internal_Error_code = 12823
         WHERE r.vt_request_id           = :1
           AND r.vt_Source_Assignment_id = :2
           AND r.vt_parent_trx_id        = :3
           AND NOT r.vt_status IN (''TRASH'', ''IGNORED'')';
      
    Begin
      Execute Immediate gDDL_Command using xxcp_global.gCommon(1).Current_request_id,
                                              xxcp_global.gCommon(1).Current_assignment_id,
                                              xxcp_global.gCommon(1).Current_parent_Trx_id;
         
      Exception When Others Then
        xxcp_foundation.FndWriteError(150,'Error Whole Transaction-1: SQLERRM='||SQLERRM, gDDL_Command);
        Raise INVALID_CURSOR;       
    End;
    -- Update Record that actually caused the problem.
    gDDL_Command := 
        'UPDATE '||gInterface_Table_Name||' r
            SET r.vt_status = ''ERROR'',
                r.vt_internal_error_code = :1,
                r.vt_date_processed = sysdate
         WHERE r.ROWID         = :2
           AND r.vt_request_id = :3 ';

    Begin
      Execute Immediate gDDL_Command using cInternalErrorCode, 
                                           xxcp_global.gCommon(1).Current_source_rowid,
                                           xxcp_global.gCommon(1).Current_request_id;
       Exception When Others Then
            xxcp_foundation.FndWriteError(150,'Error Whole Transaction-2: SQLERRM='||SQLERRM, gDDL_Command);
            Raise INVALID_CURSOR;       
    END;
  
    IF xxcp_global.gCommon(1).Custom_events = 'Y' THEN
    
      xxcp_custom_events.ON_TRANSACTION_ERROR(xxcp_global.gCommon(1).Source_id,
                                              xxcp_global.gCommon(1).Current_assignment_id,
                                              xxcp_global.gCommon(1).Current_source_rowid,
                                              xxcp_global.gCommon(1).Current_Transaction_Table,
                                              xxcp_global.gCommon(1).Current_Parent_Trx_id,
                                              xxcp_global.gCommon(1).Current_transaction_id,
                                              cInternalErrorCode);
    END IF;
  
    ClearWorkingStorage;
  
  END Error_Whole_Transaction;

  --
  --   !! ***********************************************************************************************************
  --   !!                                     No_Action_Transaction
  --   !!                                If there are no records to process.
  --   !! ***********************************************************************************************************
  --
  PROCEDURE No_Action_Transaction(cSource_Rowid IN ROWID) IS
  BEGIN
    -- Update Record that actually had not action required
      gDDL_Command :=     
        'UPDATE '||gInterface_Table_Name||' r
           SET r.vt_status              = ''SUCCESSFUL'',
               r.vt_internal_error_code = NULL,
               r.vt_date_processed      = Sysdate,
               r.vt_status_code         = 7003
         WHERE r.ROWID = :1';
       
    Begin
       Execute Immediate gDDL_Command using cSource_Rowid;

       Exception When Others Then
          xxcp_foundation.FndWriteError(150,'No Action Transction: SQLERRM='||SQLERRM, gDDL_Command);
          Raise INVALID_CURSOR;              
    END;
  END No_Action_Transaction;
  
  --
  --   !! ***********************************************************************************************************
  --   !!                                     Record_Checks
  --   !!                       Ignore, Duplicates and Pass through checks
  --   !! ***********************************************************************************************************
  --
  Function Record_Checks(cSource_Rowid      in rowid, 
                         cTransaction_Table in varchar2,
                         cTransaction_Class in varchar2,
                         cDuplicate_check   in varchar2,
                         cDuplicate_found   in varchar2 
                         ) Return Number is

  Begin
    
      IF XXCP_TE_BASE.Ignore_Class(cTransaction_Table, cTransaction_Class) THEN
          gDDL_Command :=   
          'UPDATE '||gInterface_Table_Name||' r  
             SET r.VT_STATUS = ''IGNORED'',
                 r.VT_INTERNAL_ERROR_CODE = NULL,
                 r.VT_DATE_PROCESSED = sysdate
           WHERE r.ROWID = 1:';
                             
        Begin
           Execute Immediate gDDL_Command using cSource_Rowid;

           Exception When Others Then
             xxcp_foundation.FndWriteError(150,'Control-1: SQLERRM='||SQLERRM, gDDL_Command);
             Raise INVALID_CURSOR;       
        End;

      -- If it is a duplicate then it will be marked as DUPLICATE and not processed.
      ELSIF cDuplicate_check = 'Y' and cDuplicate_found = 'Y' then 
        gDDL_Command := 
           'UPDATE '||gInterface_Table_Name||' r
             Set r.VT_STATUS = ''DUPLICATE'',
                 r.VT_INTERNAL_ERROR_CODE = NULL,
                 r.VT_DATE_PROCESSED      = Sysdate
             Where r.ROWID = :1';
                           
         Begin
           Execute Immediate gDDL_Command using cSource_Rowid;

           Exception When Others Then
             xxcp_foundation.FndWriteError(150,'Control-2: SQLERRM='||SQLERRM, gDDL_Command);
             Raise INVALID_CURSOR;       
         End;

      -- First/Second Passthrough
      ELSIF xxcp_global.gPassThroughCode > 0 then
        gDDL_Command := 
           'Update '||gInterface_Table_Name||'  r
              set r.vt_status = decode(xxcp_global.gPassThroughCode,7022,''SUCCESSFUL'',
                                                                    7023,''SUCCESSFUL'',
                                                                    7026,''PASSTHROUGH'',
                                                                    7027,''PASSTHROUGH'')
                 ,r.vt_internal_Error_code  = Null
                 ,r.vt_status_code          = :1
                 ,r.vt_date_processed       = Sysdate
            where r.rowid                   = :2';

         Begin
           Execute Immediate gDDL_Command using xxcp_global.gPassThroughCode, cSource_Rowid;
                           
           Exception When Others Then
              xxcp_foundation.FndWriteError(150,'Control-3: SQLERRM='||SQLERRM, gDDL_Command);
              Raise INVALID_CURSOR;       
          End; 
       END IF;
       
    xxcp_global.Set_Unmatched_Flag('N');

    Return(0);
  End Record_Checks;                                           
 --   !! ***********************************************************************************************************
 --   !!                                     Pass_Through_Whole_Trx 
 --   !!                                If there are no records to process.
 --   !! ***********************************************************************************************************
 Procedure Pass_Through_Whole_Trx(cSource_Rowid in Rowid, cPassThroughCode in number) is

  vStatus xxcp_vt_interface.vt_status%type;
 
 Begin
 
   If cPassThroughCode between 7021 AND 7024 then
     vStatus := 'IGNORED';
   Else
     vStatus := 'PASSTHROUGH';
   End If;
 
   If cPassThroughCode in (7022, 7026) then -- Parent level   
     xxcp_global.gPassThroughCode := cPassThroughCode;
   End if;
      
   gDDL_Command :=   
     'Update '||gInterface_Table_Name ||' r
        set r.vt_status               = :1
           ,r.vt_internal_Error_code  = Null
           ,r.vt_status_code          = :2
           ,r.vt_date_processed       = sysdate
      where r.rowid         = :3
        and r.vt_request_id = :4 ';
      
      Execute Immediate gDDL_Command using vStatus, cPassThroughCode, cSource_Rowid,xxcp_global.gCommon(1).Current_request_id;
   
     Exception When Others Then
            xxcp_foundation.FndWriteError(150,'Pass Through Whole Trx: SQLERRM='||SQLERRM, gDDL_Command);
            Raise INVALID_CURSOR;       
  End Pass_Through_Whole_Trx;
  
  --  !! ***********************************************************************************************************
  --  !!                                   Transaction_Group_Stamp
  --  !!                 Stamp Incomming record with parent Trx to create groups of
  --  ||                    transactions that resemble on document transaction.
  --  !! ***********************************************************************************************************
  PROCEDURE Transaction_Group_Stamp(cSource_assignment_id IN NUMBER,
                                    cTable_Group_id       IN NUMBER, 
                                    cInternalErrorCode    OUT NUMBER) IS
  
    vInternalErrorCode NUMBER(8) := 0;
    vStatement         VARCHAR2(6000);

    -- Distinct list of Transaction Tables
    -- populated in Set_Running_Status
    CURSOR cTrxTbl is
      SELECT transaction_table,
             nvl(grouping_rule_id,0) grouping_rule_id,
             nvl(w.interface_filter_id,0) interface_filter_id
      FROM   xxcp_sys_source_tables w
      where  w.source_id = xxcp_global.gCommon(1).Source_id
      and    w.transaction_table in (select * from TABLE(CAST(xxcp_global.gTrans_Table_Array AS XXCP_DYNAMIC_ARRAY)));

    type tgs_type is REF CURSOR;
    type tgs_map_rec is record( 
           Parent_trx_Column_Def  xxcp_sys_source_tables.parent_trx_column%type,
           Transaction_Table      xxcp_sys_source_tables.transaction_table%type
         );
                                
    tgs     tgs_type;
    tgsRec  tgs_map_rec;

  BEGIN

    vTiming(vTimingCnt).Group_Stamp_Start := to_char(Sysdate, 'HH24:MI:SS');

    For cTblRec in cTrxTbl Loop  -- Loop for all Transaction Tables in Array within SA.
      If vInternalErrorCode = 0 then
        If cTblRec.Grouping_Rule_Id > 0 THEN  --02.06.02

          --
          -- Apply Grouping Rules
          --
          XXCP_DYNAMIC_SQL.Apply_Grouping_Rules(cNew_Status        => 'GROUPING', 
                                                cInternalErrorCode => vInternalErrorCode,
                                                cTransaction_Table => cTblRec.Transaction_Table);
           -- Error Checking for Null Parent Trx id
           gDDL_Command := 
            'UPDATE '||gInterface_Table_Name||' r
               SET r.vt_status = ''ERROR'',
                   r.vt_parent_trx_id = NULL,
                   r.vt_internal_error_code = 11090
             WHERE r.vt_request_id    = :1
               AND r.vt_transaction_table = any(
                         SELECT g.vt_transaction_table
                           FROM '||gInterface_Table_Name||' g
                          WHERE g.vt_request_id           = :2
                            AND g.vt_source_assignment_id = :3
                            AND g.vt_transaction_table    = :4
                            AND g.vt_status               = ''GROUPING''
                            AND g.vt_parent_trx_id IS NULL)';
                        
         Begin
            Execute Immediate gDDL_Command 
               Using xxcp_global.gCommon(1).Current_request_id,
                     xxcp_global.gCommon(1).Current_request_id,
                     cSource_Assignment_id,
                     cTblRec.Transaction_Table;    
          
            Exception When Others Then
               xxcp_foundation.FndWriteError(150,'Transaction Group Stamp-1: SQLERRM='||SQLERRM, gDDL_Command);
               Raise INVALID_CURSOR;       
          End;
        Else 

          --
          -- Mass update of Parent Trx id
          --
          Open TGS for
           'SELECT DISTINCT 
                   t.Parent_trx_column Parent_Trx_Column_Def,
                   t.Transaction_Table
              FROM '||gInterface_Table_Name||' g,
                   xxcp_sys_source_tables  t,
                   xxcp_source_assignments x
             WHERE g.vt_request_id           = :1
               AND g.vt_status               = ''GROUPING''
               AND g.vt_transaction_table    = t.transaction_table
               AND t.table_group_id          = :2
               AND t.source_id               = x.source_id
               AND x.source_assignment_id    = :3
               AND g.vt_source_assignment_id = x.source_assignment_id
               AND g.vt_transaction_table    = :4 '
              Using xxcp_global.gCommon(1).Current_request_id,
                    cTable_group_id,
                    cSource_assignment_id,
                    cTblRec.transaction_table;
          Loop

            Fetch TGS into TGSRec;
            Exit when TGS%notfound;
          
            IF TGSRec.Parent_trx_column_def IS NULL THEN
              vInternalErrorCode := 10665;
              xxcp_foundation.FndWriteError(vInternalErrorCode,'Transaction Table <' || TGSRec.Transaction_table || '>');
            ELSE
              
              vStatement := 
                'Update '|| gInterface_Table_Name ||
                 '  set vt_parent_trx_id = ' || TGSRec.Parent_trx_column_def || ' ' ||
                 'where vt_status = :1 ' ||
                 '  and vt_source_assignment_id = :2 ' ||
                 '  and vt_transaction_table = :3 ' ||
                 '  and vt_request_id = :4 ';
               
              BEGIN

                EXECUTE IMMEDIATE vStatement
                  USING 'GROUPING', cSource_Assignment_id, TGSRec.transaction_table, xxcp_global.gCommon(1).Current_request_id;
              
              EXCEPTION
                WHEN OTHERS THEN
                  vInternalErrorCode := 10667;
                  xxcp_foundation.FndWriteError(vInternalErrorCode, SQLERRM,vStatement);
              END;
            END IF;
          END LOOP;
          Close TGS;
        End If;
  
     End If;  --new
  
    End Loop;
  
    cInternalErrorCode := vInternalErrorCode;
  
  END Transaction_Group_Stamp;
  -- 
  -- DDL_Statement_Setup
  --
  Function DDL_Statement_Setup(cPreview_Ctl in number) return number is

    cursor c1(pPreview_Ctl in number) is
      select c.short_code 
        from xxcp_sys_engine_ctl c
       where c.preview_ctl = pPreview_Ctl
         and c.short_code is not null;

    vInternalErrorCode Number(8) := 167; -- Invalid setup

  Begin
      
     For Rec in c1(pPreview_Ctl => cPreview_Ctl) Loop
       
        vInternalErrorCode := 0;

        gDDL_Flush_DML :=
         'Begin
             XXCP_PROCESS_DML_'||rec.short_code||'.Process_Oracle_Insert(
                                                      cSource_Rowid        => :1,
                                                      cParent_Rowid        => :2,
                                                      cTarget_Table        => :3,
                                                      cTarget_Instance_id  => :4,
                                                      cProcess_History_id  => :5,
                                                      cRule_id             => :6,
                                                      cD1001_Array         => :7,
                                                      cD1002_Array         => :8,
                                                      cD1003_Array         => :9,
                                                      cD1004_Array         => :10,
                                                      cD1005_Array         => :11,
                                                      cD1006_Array         => :12,
                                                      cI1009_Array         => :13,
                                                      cErrorMessage        => :14,
                                                      cInternalErrorCode   => :15);'||chr(10)
         ||'End;';
                                                      
         gDDL_Flush_Hist :=                                           
         'Begin
             XXCP_PROCESS_HIST_'||rec.short_code||'.Process_History_Insert(
                                                              cSource_Rowid            => :1,
                                                              cParent_Rowid            => :2,
                                                              cTarget_Table            => :3,
                                                              cTarget_Instance_id      => :4,
                                                              cProcess_History_id      => :5,
                                                              cTarget_assignment_id    => :6,
                                                              cAttribute_id            => :7,
                                                              cRecord_type             => :8,
                                                              cRequest_id              => :9,
                                                              cStatus                  => :10,
                                                              cInterface_id            => :11,
                                                              cModel_ctl_id            => :12,
                                                              cRule_id                 => :13,
                                                              cTax_registration_id     => :14,
                                                              cEntered_rounding_amount => :15,
                                                              cAccount_rounding_amount => :16,
                                                              cD1001_Array             => :17,
                                                              cD1002_Array             => :18,
                                                              cD1003_Array             => :19,
                                                              cD1004_Array             => :20,
                                                              cD1005_Array             => :21,
                                                              cD1006_Array             => :22,
                                                              cI1009_Array             => :23,
                                                              cErrorMessage            => :24,
                                                              cInternalErrorCode       => :25);'||chr(10)
         ||'End;';
         
       -- 03.06.18
       gDDL_Flush_ICS_DML :=
         'Begin
             xxcp_Process_Dml_Ics_'||rec.short_code||'.Process_Oracle_Insert(
                                                      cSource_Rowid        => :1,
                                                      cParent_Rowid        => :2,
                                                      cTarget_Table        => :3,
                                                      cTarget_Instance_id  => :4,
                                                      cProcess_History_id  => :5,
                                                      cRule_id             => :6,
                                                      cD1001_Array         => :7,
                                                      cD1002_Array         => :8,
                                                      cD1003_Array         => :9,
                                                      cD1004_Array         => :10,
                                                      cD1005_Array         => :11,
                                                      cD1006_Array         => :12,
                                                      cI1009_Array         => :13,
                                                      cErrorMessage        => :14,
                                                      cInternalErrorCode   => :15);'||chr(10)
         ||'End;';         


       gDDL_Interface_Select := 
           ' Select 
                 ACCOUNTED_CR
                ,ACCOUNTED_DR
                ,ACCOUNTING_DATE
                ,ASG_EXPLOSION_ID
                ,AUX_DATA1
                ,AUX_DATA2
                ,AUX_DATA3
                ,AUX_DATA4
                ,AUX_DATA5
                ,BE2
                ,BE3
                ,BE4
                ,BE6
                ,BE7
                ,BE8
                ,CALC_LEGAL_EXCH_RATE
                ,CLASS_MAPPING_REQ
                ,CLASS_TRANSACTION_ID
                ,CODE_COMBINATION_ID
                ,CURRENCY_CODE
                ,ENTERED_CR
                ,ENTERED_DR
                ,GL_SL_LINK_ID
                ,GL_SL_LINK_TABLE
                ,0
                ,ORDER_SOURCE_ID
                ,PARENT_ENTERED_AMOUNT
                ,PARENT_ENTERED_CURRENCY
                ,SEGMENT1
                ,SEGMENT2
                ,SEGMENT3
                ,SEGMENT4
                ,SEGMENT5
                ,SEGMENT6
                ,SEGMENT7
                ,SEGMENT8
                ,SEGMENT9
                ,SEGMENT10
                ,SEGMENT11
                ,SEGMENT12
                ,SEGMENT13
                ,SEGMENT14
                ,SEGMENT15
                ,SET_OF_BOOKS_ID
                ,SOURCE_CLASS_ID
                ,SOURCE_CREATION_DATE
                ,SOURCE_ID
                ,SOURCE_ROWID
                ,SOURCE_SET_OF_BOOKS_ID
                ,SOURCE_TABLE_ID
                ,SOURCE_TYPE_ID
                ,TABLE_GROUP_ID
                ,TRANSACTION_COST
                ,TRANSACTION_SET_ID
                ,TRANSACTION_SET_NAME
                ,TRX_EXPLOSION_ID
                ,USER_JE_CATEGORY_NAME
                ,USER_JE_SOURCE_NAME
                ,VT_INSTANCE_ID
                ,VT_INTERFACE_ID
                ,VT_PARENT_TRX_ID
                ,VT_REQUEST_ID
                ,VT_SOURCE_ASSIGNMENT_ID
                ,VT_STATUS
                ,VT_TRANSACTION_CLASS
                ,VT_TRANSACTION_DATE
                ,VT_TRANSACTION_ID
                ,VT_TRANSACTION_REF
                ,VT_TRANSACTION_TABLE
                ,VT_TRANSACTION_TYPE
               from '||gInterface_View_Name ||
               ' Where vt_status = :1
                   and source_id = :2
                   and vt_source_assignment_id = :3
                   and vt_request_id  = :4                    
                   and Table_Group_id = :5 ';   
       End Loop;

     If vInternalErrorCode > 0 then
       xxcp_foundation.FndWriteError(167,'Flush Setup failed in Wrapper');
     End If;

     Return(vInternalErrorCode);    
  End DDL_Statement_Setup;

  -- !! ***********************************************************************************************************
  -- !!                                   FlushRecordsToTarget
  -- !!     Control The process of moving the Array elements throught to creating the new output records
  -- !! ***********************************************************************************************************
  FUNCTION FlushRecordsToTarget(cGlobal_Rounding_Tolerance IN NUMBER,
                                cGlobalPrecision           IN NUMBER,
                                cTransaction_Type          IN VARCHAR2,
                                cInternalErrorCode         IN OUT NUMBER)
    RETURN NUMBER IS
  
    vCnt           PLS_INTEGER := 0;
    c              PLS_INTEGER := 0;
    j              PLS_INTEGER := 0;
  
    fe             PLS_INTEGER := xxcp_wks.WORKING_CNT;  
    vRC            PLS_INTEGER := xxcp_wks.SOURCE_CNT;
    vRC_Records    PLS_INTEGER := 0;
    vRC_Error      PLS_INTEGER := 0;
    vEntered_DR    NUMBER := 0;
    vEntered_CR    NUMBER := 0;
  
    vStatus        xxcp_vt_interface.vt_status%type;
    vErrorMessage  xxcp_errors.error_message%type;
    vZero_Action   PLS_INTEGER := 0;  
    UDI_Cnt        PLS_INTEGER := 0;
    
    vNull_Value    varchar2(1);
    
    -- 03.06.18
    vICS_Cnt       PLS_INTEGER := 0;  

  BEGIN

    IF fe > 0 THEN
      xxcp_te_base.Account_Rounding(cGlobal_Rounding_Tolerance,
                                    cGlobalPrecision,
                                    cTransaction_Type,
                                    vEntered_DR,
                                    vEntered_CR,
                                    cInternalErrorCode);
    END IF;
  
    -- Check that for each attribute record we have a records for output.
    FOR c IN 1 .. vRC LOOP
      vRC_Error := 0;
      FOR j IN 1 .. fe LOOP
        IF xxcp_wks.Source_rowid(c) = xxcp_wks.WORKING_RCD(j).Source_rowid THEN
          xxcp_wks.WORKING_RCD(j).Source_Pos := c;
          xxcp_wks.SOURCE_STATUS(c) := 'FLUSH';
          vRC_Error   := 1;
          vRC_Records := vRC_Records + 1;
        END IF;
      END LOOP;
      IF vRC_Error = 0 THEN
        xxcp_wks.SOURCE_SUB_CODE(c) := 7003;
        xxcp_wks.SOURCE_STATUS(c) := 'NO RULES';
        No_Action_Transaction(xxcp_wks.Source_rowid(c));
      END IF;
    END LOOP;
   
    SAVEPOINT TARGET_INSERT;
  
    IF cInternalErrorCode = 0 AND vRC_Records > 0 THEN

      FOR j IN 1 .. fe LOOP
      
        EXIT WHEN cInternalErrorCode <> 0;

        vCnt := vCnt + 1;
      
        -- Custom Events 
        IF xxcp_global.gCommon(1).Custom_events = 'Y' THEN
          xxcp_te_base.Custom_Event_Insert_Row(j,cInternalErrorCode);
        END IF;

        -- Tag Zero Account Value rows
        IF cInternalErrorCode = 0 THEN

          xxcp_global.gCommon(1).Current_source_rowid := xxcp_wks.Source_rowid(xxcp_wks.WORKING_RCD(j).Source_Pos);

          -- Find the Action to take for Zero Accounted values
          vZero_Action := xxcp_te_base.Zero_Suppression_Rule(j);

        IF vZero_Action > 0 THEN

          SELECT xxcp_process_history_seq.NEXTVAL INTO xxcp_wks.WORKING_RCD(j) .Process_History_id FROM dual;
          -- don't post ZAV

          IF vZero_Action = 3 THEN
           Begin
               Execute Immediate gDDL_Flush_DML 
                 Using  in  xxcp_wks.WORKING_RCD(j).Source_rowid,
                            vNull_Value,
                            xxcp_wks.WORKING_RCD(j).Target_Table,
                            xxcp_wks.WORKING_RCD(j).Target_Instance_id,
                            xxcp_wks.WORKING_RCD(j).Process_History_id,
                            xxcp_wks.WORKING_RCD(j).Rule_id,
                            xxcp_wks.WORKING_RCD(j).D1001_Array,
                            xxcp_wks.WORKING_RCD(j).D1002_Array,
                            xxcp_wks.WORKING_RCD(j).D1003_Array,
                            xxcp_wks.WORKING_RCD(j).D1004_Array,
                            xxcp_wks.WORKING_RCD(j).D1005_Array,
                            xxcp_wks.WORKING_RCD(j).D1006_Array,
                            xxcp_wks.WORKING_RCD(j).I1009_Array,
                        out vErrorMessage,
                     in out cInternalErrorCode;
                                                
               Exception when others then                                   
                 xxcp_foundation.FndWriteError(150,'Flush DML: SQLERRM='||SQLERRM, gDDL_Flush_DML);
                 Raise INVALID_CURSOR;       
             End;                          
          End If;
                 
          IF cInternalErrorCode = 0 THEN
              ---
              --- XXCP_PROCESS_HISTORY
              ---
              Begin
                    Execute Immediate gDDL_Flush_Hist 
                           Using in xxcp_wks.WORKING_RCD(j).Source_rowid,
                                    vNull_Value,
                                    xxcp_wks.WORKING_RCD(j).Target_Table,
                                    xxcp_wks.WORKING_RCD(j).Target_Instance_id,
                                    xxcp_wks.WORKING_RCD(j).Process_History_id,
                                    xxcp_wks.WORKING_RCD(j).Target_Assignment_id,
                                    xxcp_wks.WORKING_RCD(j).Attribute_id,
                                    xxcp_wks.WORKING_RCD(j).Record_type,
                                    xxcp_global.gCommon(1).Current_request_id,
                                    xxcp_wks.WORKING_RCD(j).Record_Status,
                                    xxcp_wks.WORKING_RCD(j).Interface_id,
                                    xxcp_wks.WORKING_RCD(j).Model_ctl_id,
                                    xxcp_wks.WORKING_RCD(j).Rule_id,
                                    xxcp_wks.WORKING_RCD(j).Tax_Registration_id,
                                    xxcp_wks.WORKING_RCD(j).Entered_Rnd_Amount,
                                    xxcp_wks.WORKING_RCD(j).Accounted_Rnd_Amount,
                                    xxcp_wks.WORKING_RCD(j).D1001_Array,
                                    xxcp_wks.WORKING_RCD(j).D1002_Array,
                                    xxcp_wks.WORKING_RCD(j).D1003_Array,
                                    xxcp_wks.WORKING_RCD(j).D1004_Array,
                                    xxcp_wks.WORKING_RCD(j).D1005_Array,
                                    xxcp_wks.WORKING_RCD(j).D1006_Array,
                                    xxcp_wks.WORKING_RCD(j).I1009_Array,
                                out vErrorMessage,
                             in out cInternalErrorCode;
               Exception when Others then
                 xxcp_foundation.FndWriteError(150,'Flush History: SQLERRM='||SQLERRM, gDDL_Flush_Hist);
                 Raise INVALID_CURSOR;       
             End;                          

              IF cInternalErrorCode = 0 THEN
                vTiming(vTimingCnt).Flush_Records := vTiming(vTimingCnt).Flush_Records + 1;
              ELSE
                xxcp_foundation.FndWriteError(cInternalErrorCode, vErrorMessage);
              END IF;
          ELSE
            xxcp_foundation.FndWriteError(cInternalErrorCode, vErrorMessage);
          END IF;
          
          -- 03.06.18
          IF cInternalErrorCode = 0 THEN
          
            -- Check the report settlement rule.
            If xxcp_global.gCommon(1).ic_settlements = 'Y' and xxcp_wks.WORKING_RCD(j).I1009_Array(151) is not null then   -- If ICS Rule
            
                Begin
                   Execute Immediate gDDL_Flush_ICS_DML 
                     Using  in  xxcp_wks.WORKING_RCD(j).Source_rowid,
                                vNull_Value,
                                xxcp_wks.WORKING_RCD(j).Target_Table,
                                xxcp_wks.WORKING_RCD(j).Target_Instance_id,
                                xxcp_wks.WORKING_RCD(j).Process_History_id,
                                xxcp_wks.WORKING_RCD(j).Rule_id,
                                xxcp_wks.WORKING_RCD(j).D1001_Array,
                                xxcp_wks.WORKING_RCD(j).D1002_Array,
                                xxcp_wks.WORKING_RCD(j).D1003_Array,
                                xxcp_wks.WORKING_RCD(j).D1004_Array,
                                xxcp_wks.WORKING_RCD(j).D1005_Array,
                                xxcp_wks.WORKING_RCD(j).D1006_Array,
                                xxcp_wks.WORKING_RCD(j).I1009_Array,
                            out vErrorMessage,
                         in out cInternalErrorCode;
                                                    
                   Exception when others then                                   
                     xxcp_foundation.FndWriteError(150,'Flush DML: SQLERRM='||SQLERRM, gDDL_Flush_ICS_DML);
                     Raise INVALID_CURSOR;       
                 End;             
                                                          
                 vICS_Cnt := vICS_Cnt + 1;                                                         
            end if;          
          end if;               

          IF cInternalErrorCode = 0 THEN
             vStatus := 'SUCCESSFUL';
          ELSE
             vStatus := 'ERROR';
             xxcp_wks.SOURCE_SUB_CODE(xxcp_wks.WORKING_RCD(j).Source_Pos):= 7008;
          END IF;

          xxcp_wks.SOURCE_STATUS(xxcp_wks.WORKING_RCD(j).Source_Pos) := vStatus;
        ELSE
          -- ZAV
          xxcp_wks.SOURCE_SUB_CODE(xxcp_wks.WORKING_RCD(j).Source_Pos):= 7008;
          xxcp_wks.SOURCE_STATUS(xxcp_wks.WORKING_RCD(j).Source_Pos) := 'SUCCESSFUL';
        END IF;
        
        UDI_Cnt := UDI_Cnt + 1;     
      
      END IF;
   
      END LOOP;
      
      -- 03.06.18
      -- validate the records inserted into the interface table.                                           
      IF cInternalErrorCode = 0 and xxcp_global.gCommon(1).ic_settlements = 'Y' and vICS_Cnt > 0  then -- If ICS Rule and processed one or more settlement rules
        if xxcp_global.gCommon(1).current_Transaction_table != 'PAYMENTS' then  -- don't perform the integrity check for intercompany payments
          xxcp_ic_applications.integrity_check(cParent_Trx_id       => xxcp_global.gCommon(1).current_parent_trx_id,
                                               cRequest_id          => xxcp_global.gCommon(1).current_request_id,
                                               cErrorMessage        => vErrorMessage,
                                               cInternalErrorCode   => cInternalErrorCode);                                                          
        end if;                                               
      end if;    
      
      -- 03.06.24 Only call the approval_unrestricted ICS function for PO Receipt Matching Source
      if cInternalErrorCode = 0 and xxcp_global.gCommon(1).Source_id = 43 then 
        xxcp_ic_applications.approval_unrestricted(cApproved_Matched_Parents => xxcp_global.Get_Approved_Matched_Parents,
                                                   cRequest_id               => xxcp_global.gCommon(1).current_request_id,
                                                   cErrorMessage             => vErrorMessage,
                                                   cInternalErrorCode        => cInternalErrorCode);
      end if;
    
      IF cInternalErrorCode = 0 AND UDI_Cnt > 0 THEN
        Begin

          gDDL_Command :=         
            'UPDATE '||gInterface_Table_Name||' r
               SET r.vt_Status              = :1,
                   r.vt_internal_Error_code = NULL,
                   r.vt_date_processed      = sysdate,
                   r.vt_Status_Code         = :2
             WHERE r.ROWID = :3';

         FOR j IN 1 .. xxcp_wks.SOURCE_CNT Loop  -- check this one 
           Execute Immediate gDDL_Command using xxcp_wks.SOURCE_STATUS(j),xxcp_wks.SOURCE_SUB_CODE(j), xxcp_wks.Source_rowid(j);  
         End Loop;

         Exception When Others Then
            xxcp_foundation.FndWriteError(150,'Flush Records Update: SQLERRM='||SQLERRM, gDDL_Command);
            Raise INVALID_CURSOR;       
       End;

      END IF;    
    END IF;
  
   -- Source_Pos
    IF cInternalErrorCode <> 0 THEN
      -- Rollback
      IF vCnt > 0 THEN
        ROLLBACK TO TARGET_INSERT;
        xxcp_foundation.FndWriteError(cInternalErrorCode, vErrorMessage);
      END IF;
    
      Error_Whole_Transaction(cInternalErrorCode => cInternalErrorCode);
    
      IF cInternalErrorCode = 12800 THEN
        xxcp_foundation.FndWriteError(cInternalErrorCode,
          'Entered DR <' ||TO_CHAR(vEntered_DR) ||'> Entered CR <' ||TO_CHAR(vEntered_CR) || '>');
      END IF;
    
    END IF;
  
    -- Remove Array Elements
    ClearWorkingStorage;
  
    RETURN(vCnt);
  END FlushRecordsToTarget;

  --  !! ***********************************************************************************************************
  --  !!                                     Reset_Parent_Trx
  --  !!                      Reset Parent_Trx for non Successful Transactions
  --  !! ***********************************************************************************************************
  PROCEDURE Reset_Parent_Trx(cSource_assignment_id IN NUMBER,
                             cTransaction_Table    IN VARCHAR2) IS
  Begin
    gDDL_Command :=
      'Update '||gInterface_Table_Name||' r
         set r.vt_parent_trx_id = Null
       where r.vt_transaction_date between :1 and :2
         and r.vt_source_assignment_id = :3
         and r.vt_transaction_table    = :4
         and r.vt_status IN (''NEW'',''ERROR'',''WAITING'',''ASSIGNMENT'',''TRANSACTION'',''PASSTHROUGH'',''UN-MATCHED'')
         and not r.vt_request_id = any(select c.request_id from xxcp_activity_control c where c.active = ''Y'')';

       Execute Immediate gDDL_Command 
          Using gStart_Partition_Date,gEnd_Partition_Date,cSource_assignment_id,cTransaction_Table; 

       Exception When Others Then
          xxcp_foundation.FndWriteError(150,'Reset Parent Trx: SQLERRM='||SQLERRM, gDDL_Command);
          Raise INVALID_CURSOR;       

  End Reset_Parent_Trx;

  --  !! ***********************************************************************************************************
  --  !!                                     Reset_HAC_Tables
  --  !!                      Reset Headers, Attributes, Cache for Errored Transactions
  --  !! ***********************************************************************************************************
  PROCEDURE Reset_HAC_Tables(cSource_assignment_id IN NUMBER,
                             cTransaction_Table    IN VARCHAR2) IS
  BEGIN

      -- Clear HAC tables
      -- Header
      
      gDDL_Command := 
        'DELETE from xxcp_transaction_header h
         WHERE  h.header_id in (select a.header_id 
                             from   xxcp_transaction_attributes a
                             where  a.source_assignment_id = :1
                             and    a.interface_id in (select g.vt_interface_id
                                                       from '||gInterface_Table_Name||' g
                                                      where g.vt_transaction_date between :2 and :3 
                                                        and g.vt_source_assignment_id = :4
                                                        and g.vt_transaction_table    = :5 
                                                        and (g.vt_status = ''ERROR'' or g.vt_status = ''WAITING'' or g.vt_status = ''UN-MATCHED'')
                                                        and not g.vt_request_id = any(select c.request_id from xxcp_activity_control c where c.active = ''Y'')))';
                          
      Begin
        Execute Immediate gDDL_Command using cSource_assignment_id,
                                             gStart_Partition_Date,
                                             gEnd_Partition_Date,
                                             cSource_assignment_id,
                                             cTransaction_Table;                      
         Exception When Others Then
            xxcp_foundation.FndWriteError(150,'Reset HAC Tables-1: SQLERRM='||SQLERRM, gDDL_Command);
            Raise INVALID_CURSOR;       
      End;
      -- Cache
      gDDL_Command := 
        'DELETE from xxcp_transaction_cache c
         WHERE  c.attribute_id in (select a.attribute_id 
                                from   xxcp_transaction_attributes a
                                where  a.source_assignment_id = :1
                                and    a.interface_id in (select vt_interface_id
                                                        from  '||gInterface_Table_Name||' g
                                                        where vt_transaction_date between :2 and :3
                                                          and g.vt_source_assignment_id = :4
                                                          and g.vt_transaction_table    = :5 
                                                          and (g.vt_status = ''ERROR'' or g.vt_status = ''WAITING'' or g.vt_status = ''UN-MATCHED'')
                                                          and not g.vt_request_id = any(select c.request_id from xxcp_activity_control c where c.active = ''Y'')))'; 
      Begin         
          Execute Immediate gDDL_Command using cSource_assignment_id,
                                               gStart_Partition_Date,
                                               gEnd_Partition_Date,
                                               cSource_assignment_id,
                                               cTransaction_Table;                      
         Exception When Others Then
            xxcp_foundation.FndWriteError(150,'Reset HAC Tables-2: SQLERRM='||SQLERRM, gDDL_Command);
            Raise INVALID_CURSOR;       
      End;
      -- Attributes
      gDDL_Command := 
       'DELETE from xxcp_transaction_attributes a
        where  a.source_assignment_id = :1
        and    a.interface_id in (select vt_interface_id g
                                from  '||gInterface_Table_Name||' g
                                where g.vt_transaction_date between :2 and :3
                                  and g.vt_source_assignment_id = :4
                                  and g.vt_transaction_table    = :5 
                                  and (g.vt_status = ''ERROR'' or g.vt_status = ''WAITING'' or g.vt_status = ''UN-MATCHED'') 
                                  and not g.vt_request_id = any(select c.request_id from xxcp_activity_control c where c.active = ''Y''))';
      Begin    
        Execute Immediate gDDL_Command using cSource_assignment_id,
                                             gStart_Partition_Date,
                                             gEnd_Partition_Date,
                                             cSource_assignment_id,
                                             cTransaction_Table;                      
         Exception When Others Then
            xxcp_foundation.FndWriteError(150,'Reset HAC Tables-3: SQLERRM='||SQLERRM, gDDL_Command);
            Raise INVALID_CURSOR;       
      End; 
  END Reset_HAC_Tables;
  
  --  !! ***********************************************************************************************************
  --  !!                                     Direct_Reset_HAC_Tables
  --  !!                      Reset Headers, Attributes, Cache for Errored Transactions
  --  !! ***********************************************************************************************************
  PROCEDURE Direct_Reset_HAC_Tables(cSource_assignment_id IN NUMBER,
                                    cTransaction_Table    IN VARCHAR2,
                                    cParent_Trx_id        IN NUMBER) IS
  Begin

      -- Clear HAC tables      
      -- Header
      gDDL_Command := 
        'DELETE from xxcp_transaction_header h
         WHERE  h.header_id in (select a.header_id 
                             from   xxcp_transaction_attributes a
                             where  a.source_assignment_id = :1
                               and  a.parent_trx_id = :2                            
                               and  a.interface_id in (select g.vt_interface_id
                                                       from '||gInterface_Table_Name||' g
                                                      where g.vt_transaction_date between :3 and :4 
                                                        and g.vt_source_assignment_id = :5
                                                        and g.vt_transaction_table    = :6 
                                                        and (g.vt_status = ''ERROR'' or g.vt_status = ''WAITING'' or g.vt_status = ''UN-MATCHED'')
                                                        and not g.vt_request_id = any(select c.request_id from xxcp_activity_control c where c.active = ''Y'')))'; 
      Begin 
        
        Execute Immediate gDDL_Command using cSource_assignment_id,
                                             cParent_Trx_id,
                                             gStart_Partition_Date,
                                             gEnd_Partition_Date,
                                             cSource_assignment_id,
                                             cTransaction_Table;                      
         Exception When Others Then
            xxcp_foundation.FndWriteError(150,'Direct Reset HAC Tables-1: SQLERRM='||SQLERRM, gDDL_Command);
            Raise INVALID_CURSOR;       
      End;
      -- Cache
      gDDL_Command := 
        'DELETE from xxcp_transaction_cache c
         WHERE  c.attribute_id in (select a.attribute_id 
                                from   xxcp_transaction_attributes a
                                where  a.source_assignment_id = :1
                                  and  a.parent_trx_id        = :2
                                  and  a.interface_id in (select vt_interface_id
                                                        from  '||gInterface_Table_Name||' g
                                                        where g.vt_transaction_date between :3 and :4
                                                          and g.vt_source_assignment_id = :5
                                                          and g.vt_transaction_table    = :6 
                                                          and (g.vt_status = ''ERROR'' or g.vt_status = ''WAITING''  or g.vt_status = ''UN-MATCHED'')
                                                          and not g.vt_request_id = any(select c.request_id from xxcp_activity_control c where c.active = ''Y'')))'; 
      Begin  
        Execute Immediate gDDL_Command Using cSource_assignment_id,
                                             cParent_Trx_id,
                                             gStart_Partition_Date,
                                             gEnd_Partition_Date,
                                             cSource_assignment_id,
                                             cTransaction_Table;                      
         Exception When Others Then
            xxcp_foundation.FndWriteError(150,'Direct Reset HAC Tables-2: SQLERRM='||SQLERRM, gDDL_Command);
            Raise INVALID_CURSOR;       
      End;
      -- Attributes
      gDDL_Command := 
       'DELETE from xxcp_transaction_attributes a
        where  a.source_assignment_id = :1
          and  a.parent_trx_id        = :2
          and  a.interface_id in (select vt_interface_id
                                    from  '||gInterface_Table_Name||' g
                                   where g.vt_transaction_date between :3 and :4
                                     and g.vt_source_assignment_id = :5
                                     and g.vt_transaction_table    = :6 
                                     and (g.vt_status = ''ERROR'' or g.vt_status = ''WAITING'' or g.vt_status = ''UN-MATCHED'') 
                                     and not g.vt_request_id = any(select c.request_id from xxcp_activity_control c where c.active = ''Y''))';

      Begin  
  
        Execute Immediate gDDL_Command using cSource_assignment_id,
                                             cParent_Trx_id,
                                             gStart_Partition_Date,
                                             gEnd_Partition_Date,
                                             cSource_assignment_id,
                                             cTransaction_Table;                      
         Exception When Others Then
            xxcp_foundation.FndWriteError(150,'Direct Reset HAC Tables-3: SQLERRM='||SQLERRM, gDDL_Command);
            Raise INVALID_CURSOR;       
      End;
  END Direct_Reset_HAC_Tables;
  
  --  !! ***********************************************************************************************************
  --  !!                                   Grab_Records_To_Process
  --  !!                      Flag the records that are going to be processed.
  --  !! ***********************************************************************************************************
  PROCEDURE Grab_Records_To_Process(cSource_id            IN NUMBER,
                                    cSource_Group_id      IN NUMBER,
                                    cRequest_id           IN NUMBER,
                                    cTable_Group_id       IN NUMBER,
                                    cTrx_Group_id         IN NUMBER) IS

  Begin
    
    gDDL_Command :=         
         'Update '||gInterface_Table_Name||' r 
        set vt_request_id     = :1,
            vt_date_processed = sysdate
       Where r.vt_transaction_date between :2 and :3
         and r.vt_source_assignment_id = any(select v.source_assignment_id 
                                               from xxcp_source_assignments v 
                                              where v.source_group_id = :4 
                                                and v.source_id = :5)
         and r.vt_status             = ''NEW''
         and not nvl(r.vt_request_id,0) = any(select c.request_id from xxcp_activity_control c where c.active = ''Y'')
         and r.vt_transaction_table  = any(select t.transaction_table 
                                             from xxcp_sys_source_tables t
                                            where t.Table_Group_id  = :6)';

       Execute Immediate gDDL_Command 
          Using cRequest_id, gStart_Partition_Date, gEnd_Partition_Date, cSource_Group_id, cSource_id, cTable_Group_id;
        
     Commit;
     
     Exception when others then 
          xxcp_foundation.fndwriteerror(303,'Grab Records to Process:'||SQLERRM);
     
  End Grab_Records_To_Process;

  --  !! ***********************************************************************************************************
  --  !!                                   Reset_Errored_Transactions
  --  !!                              Reset Transactions from a previous run.
  --  !! ***********************************************************************************************************
  PROCEDURE Reset_Errored_Transactions(cSource_id            IN NUMBER, 
                                       cSource_assignment_id IN NUMBER,
                                       cRequest_id           IN NUMBER, 
                                       cTable_Group_id       IN NUMBER, 
                                       cTrx_Group_id         IN NUMBER,
                                       cTransaction_Table    IN VARCHAR2 default Null,
                                       cParent_Trx_id        IN NUMBER default 0
                                       ) IS

    -- Distinct list of Transaction Tables
    CURSOR c1 is
      SELECT distinct transaction_table,
                      clear_hac,
                      clear_parent_trx,
                      nvl(grouping_rule_id,0) grouping_rule_id
      FROM   xxcp_sys_source_tables st
      where  source_id      = cSource_id
      and    table_group_id = cTable_Group_id; 

    vPrevious_Request_id NUMBER;

  BEGIN

    vTiming(vTimingCnt).Reset_Start := to_char(Sysdate, 'HH24:MI:SS');

    If nvl(cParent_Trx_id,0) = 0 then
      
     -- Remove Errors
     BEGIN
      DELETE FROM xxcp_errors cx
       WHERE source_assignment_id = cSource_assignment_id
        AND table_group_id        = cTable_Group_id
        AND trx_group_id          = cTrx_Group_id;

      Exception When Others Then NULL;
     END;

     For c1_rec in c1 loop -- Loop for all distinct tables in SA


      -- Reset HAC
      If c1_rec.clear_hac = 'Y' then
        Reset_HAC_Tables(cSource_assignment_id => cSource_assignment_id,
                         cTransaction_Table    => c1_rec.Transaction_Table);
      end if;
      
      -- Reset Parent Trx
      If c1_rec.clear_parent_trx = 'Y' and c1_rec.grouping_rule_id <> 0 then
        Reset_Parent_Trx(cSource_assignment_id => cSource_assignment_id,
                         cTransaction_Table    => c1_rec.Transaction_Table);

       End if; 
     End Loop;
     
   Else -- Direct Call
     
     BEGIN
       
      DELETE FROM xxcp_errors cx
       WHERE source_assignment_id = cSource_assignment_id
        AND table_group_id        = cTable_Group_id
        AND trx_group_id          = cTrx_Group_id
        and cx.parent_trx_id      = cParent_Trx_id;

      Exception When Others Then NULL;
     END;

     For c1_rec in c1 loop -- Loop for all distinct tables in SA
      -- Reset HAC
      If c1_rec.clear_hac = 'Y' then
        Direct_Reset_HAC_Tables(cSource_assignment_id => cSource_assignment_id,
                                cTransaction_Table    => c1_rec.Transaction_Table,
                                cParent_Trx_id        => cParent_Trx_id);
      end if;
     End Loop;

   End If;
    
    -- Update Interface
    BEGIN

      -- Three updates are used rather than one so that we hit the vt_status
      -- index. This is important when you have 30million rows.

      vPrevious_Request_id := cRequest_id - gRequest_Id_Range;  -- 02.06.07

     If nvl(cParent_Trx_id,0) = 0 then
      gDDL_Command := 
        'UPDATE '||gInterface_Table_Name||' r
           SET r.vt_status              = ''NEW'',
               r.vt_Internal_Error_Code = NULL,
               r.vt_status_code         = NULL,
               r.vt_request_id          = :1
         WHERE r.vt_transaction_date between :2 and :3
           AND r.vt_source_assignment_id = :4
           AND r.vt_request_id          >= :5
           AND r.vt_status IN (''ASSIGNMENT'',''ERROR'',''GROUPING'',''TRANSACTION'',''WAITING'',''PASSTHROUGH'',''UN-MATCHED'')
           AND not r.vt_request_id = any(select c.request_id from xxcp_activity_control c where c.active = ''Y'')           
           AND r.vt_transaction_Table = ANY(Select w.transaction_table 
                                              from xxcp_sys_source_tables w
                                             where w.source_id      = :6
                                               and w.table_group_id = :7 )';

       Begin
         Execute Immediate gDDL_Command using cRequest_id,
                                              gStart_Partition_Date,
                                              gEnd_Partition_Date,
                                              cSource_assignment_id,
                                              vPrevious_Request_id,
                                              cSource_id,
                                              cTable_Group_id;

         Exception When Others Then
            xxcp_foundation.FndWriteError(150,'Reset Errored Transactions-1: SQLERRM='||SQLERRM, gDDL_Command);
            Raise INVALID_CURSOR;       

       End;
      Else -- Direct Call
      
        gDDL_Command := 
          'UPDATE '||gInterface_Table_Name||' r
               SET r.vt_status              = ''NEW'',
                   r.vt_Internal_Error_Code = NULL,
                   r.vt_status_code         = NULL,
                   r.vt_request_id          = :1
             WHERE r.vt_transaction_date between :2 and :3
               AND r.vt_source_assignment_id = :4
               AND r.vt_transaction_table    = :5
               AND r.vt_parent_trx_id        = :6
               AND r.vt_status IN (''ASSIGNMENT'',''NEW'',''ERROR'',''GROUPING'',''TRANSACTION'',''WAITING'',''PASSTHROUGH'',''UN-MATCHED'')
               AND not r.vt_request_id = any(select c.request_id from xxcp_activity_control c where c.active = ''Y'')
               AND r.vt_transaction_Table = ANY(Select w.transaction_table 
                                                  from xxcp_sys_source_tables w
                                                 where w.source_id      = :7
                                                   and w.table_group_id = :8 )';
         Begin
           Execute Immediate gDDL_Command using cRequest_id,
                                                gStart_Partition_Date,
                                                gEnd_Partition_Date,
                                                cSource_assignment_id,
                                                cTransaction_Table,
                                                cParent_Trx_id,
                                                cSource_id,
                                                cTable_Group_id;
            Exception When Others Then
               xxcp_foundation.FndWriteError(150,'Reset Errored Transactions-2: SQLERRM='||SQLERRM, gDDL_Command);
               Raise INVALID_CURSOR;       
          End;
      End If;
    END;

    COMMIT;
  END RESET_ERRORED_TRANSACTIONS;
  
  --  !! ***********************************************************************************************************
  --  !!                                  Set_Running_Status
  --  !!                      Flag the records that are going to be processed.
  --  !! ***********************************************************************************************************
  PROCEDURE Set_Running_Status(cSource_id            IN NUMBER,
                               cSource_assignment_id IN NUMBER,
                               cSource_group_id      IN NUMBER,
                               cRequest_id           IN NUMBER,
                               cTable_Group_id       IN NUMBER,
                               cTrx_Group_id         IN NUMBER,
                               cTransaction_Table    IN VARCHAR2 default Null,
                               cParent_Trx_id        IN NUMBER default 0) IS

    type  rx_type is REF CURSOR;
    type rx_map_rec is record( 
           vt_transaction_table   xxcp_vt_interface.vt_transaction_table%type,
           vt_transaction_type    xxcp_vt_interface.vt_transaction_type%type,
           vt_transaction_id      xxcp_vt_interface.vt_transaction_id%type,
           source_rowid           rowid,
           grouping_rule_id       number
         );
                                
    rx     rx_type;
    rxRec  rx_map_rec;

    type rx2_type is REF CURSOR;
    type rx2_map_rec is record( 
           vt_transaction_table   xxcp_vt_interface.vt_transaction_table%type,
           vt_transaction_type    xxcp_vt_interface.vt_transaction_type%type,
           vt_transaction_id      xxcp_vt_interface.vt_transaction_id%type,
           source_rowid           rowid,
           grouping_rule_id       number
         );
                                
    rx2     rx2_type;
    rx2Rec  rx2_map_rec;

    vGrouping_Rule_id xxcp_vt_interface.vt_grouping_rule_id%TYPE;

    type ErrCheck_type is REF CURSOR;
    type ErrCheck_map_rec is record( 
           vt_transaction_table   xxcp_vt_interface.vt_transaction_table%type
         );
                                
    ErrCheck     ErrCheck_type;
    ErrCheckRec  ErrCheck_map_rec;

    vAction_flag           VARCHAR2(1) := 'N';
    vGroupingRuleExists    BOOLEAN     := FALSE;
    vPrevTransaction_Table xxcp_sys_source_tables.transaction_table%TYPE := '~NULL~';
  
  BEGIN
  
    vTiming(vTimingCnt).Set_Running_Start := to_char(Sysdate, 'HH24:MI:SS');
    
    Grab_Records_To_Process(cSource_id            => cSource_id,
                            cSource_Group_id      => cSource_Group_id,
                            cRequest_id           => cRequest_id,
                            cTable_Group_id       => cTable_Group_id,
                            cTrx_Group_id         => cTrx_Group_id);
  
    If nvl(cParent_Trx_id,0) = 0 then

      Open rx for
       ' select 
           r.vt_transaction_table,
           r.vt_transaction_type,
           r.vt_transaction_id,
           r.ROWID source_rowid,
           nvl(t.grouping_rule_id,0) grouping_rule_id
         FROM '||gInterface_Table_Name||' r, 
              xxcp_sys_source_tables t
        WHERE r.vt_transaction_date between :1 and :2
          AND r.vt_source_assignment_id = :3
          AND r.vt_status             = ''NEW''
          AND r.vt_transaction_table  = t.transaction_table
          AND t.Table_Group_id        = :4
          AND t.source_id             = :5
          AND r.vt_request_id         = :6
        ORDER BY vt_transaction_table'
         Using gStart_Partition_Date, 
               gEnd_Partition_Date,
               cSource_assignment_id, 
               cTable_Group_id,
               cSource_id,
               cRequest_id;
       Loop

         Fetch rx into rxRec;
         Exit when rx%notfound;

         vGrouping_rule_id := Null;

         -- Populate Transaction_Table Array
         If vPrevTransaction_Table <> rxRec.vt_transaction_table then
           xxcp_global.gTrans_Table_Array.extend;
           xxcp_global.gTrans_Table_Array(xxcp_global.gTrans_Table_Array.count) := rxRec.vt_transaction_table;
         End if;
      
         vPrevTransaction_Table := rxRec.vt_transaction_table;
 
         IF rxRec.grouping_rule_id > 0 then  -- 02.06.02

            vGroupingRuleExists := TRUE;
                    
            xxcp_global.gCommon(1).Current_transaction_table := rxRec.vt_Transaction_Table;
            xxcp_global.gCommon(1).Current_transaction_id    := rxRec.vt_Transaction_id;

            vGrouping_rule_id := xxcp_custom_events.get_group_rule_id(cSource_id,
                                                                      cSource_assignment_id,
                                                                      rxRec.source_rowid,
                                                                      rxRec.vt_transaction_table,
                                                                      rxRec.vt_transaction_type);

            IF NVL(vGrouping_rule_id, 0) = 0 THEN
              vGrouping_rule_id := rxRec.Grouping_rule_id; -- Default;
            END IF;
         END IF;
      
          -- Set Processing Status   
         BEGIN
            
            gDDL_Command :=       
              'UPDATE '||gInterface_Table_Name||' r
                 SET r.vt_status           = ''GROUPING'',
                     r.vt_date_processed   = sysdate,
                     r.vt_status_code      = NULL,
                     r.vt_request_id       = :1,
                     r.vt_Grouping_rule_id = :2
               WHERE r.ROWID = :3';
             
            Begin
              Execute Immediate gDDL_Command Using cRequest_id, vGrouping_rule_id, rxRec.source_rowid;
          
              Exception When Others Then
                xxcp_foundation.FndWriteError(150,'Set Running Status-1: SQLERRM='||SQLERRM, gDDL_Command);
                Raise INVALID_CURSOR;       
            End; 
          END;
       End Loop;
       Close rx;
       
    Else
      
      Open rx2 for
       ' select 
           r.vt_transaction_table,
           r.vt_transaction_type,
           r.vt_transaction_id,
           r.ROWID source_rowid,
           nvl(t.grouping_rule_id,0) grouping_rule_id
         FROM '||gInterface_Table_Name||' r, 
               xxcp_sys_source_tables t
        WHERE r.vt_transaction_date between :1 and :2
          AND r.vt_transaction_table    = :3
          AND r.vt_parent_trx_id        = :4
          AND r.vt_source_assignment_id = :5
          AND r.vt_status             = ''NEW''
          AND r.vt_transaction_table  = t.transaction_table
          AND t.Table_Group_id        = :6
          AND t.source_id             = :7
        ORDER BY vt_transaction_table'
         Using gStart_Partition_Date, 
               gEnd_Partition_Date, 
               cTransaction_Table, 
               cParent_Trx_id,
               cSource_assignment_id, 
               cTable_Group_id, 
               cSource_id;
       Loop

         Fetch rx2 into rx2Rec;
         Exit when rx2%notfound;
      
         vGrouping_rule_id := Null;

         -- Populate Transaction_Table Array
         If vPrevTransaction_Table <> rx2Rec.vt_transaction_table then
           xxcp_global.gTrans_Table_Array.extend;
           xxcp_global.gTrans_Table_Array(xxcp_global.gTrans_Table_Array.count) := rx2Rec.vt_transaction_table;
         End if;
      
         vPrevTransaction_Table := rx2Rec.vt_transaction_table;
 
         IF rx2Rec.grouping_rule_id > 0 then  -- 02.06.02

            vGroupingRuleExists := TRUE;
                    
            xxcp_global.gCommon(1).Current_transaction_table := rx2Rec.vt_Transaction_Table;
            xxcp_global.gCommon(1).Current_transaction_id    := rx2Rec.vt_Transaction_id;

            vGrouping_rule_id := 
               xxcp_custom_events.get_group_rule_id(cSource_id,
                                                    cSource_assignment_id,
                                                    rx2Rec.source_rowid,
                                                    rx2Rec.vt_transaction_table,
                                                    rx2Rec.vt_transaction_type);

            IF NVL(vGrouping_rule_id, 0) = 0 THEN
              vGrouping_rule_id := rx2Rec.Grouping_rule_id; -- Default;
            END IF;
         END IF;
      
          -- Set Processing Status   
          gDDL_Command := 
              'UPDATE '||gInterface_Table_Name||' r
                 SET r.vt_status           = ''GROUPING'',
                     r.vt_date_processed   = sysdate,
                     r.vt_status_code      = NULL,
                     r.vt_request_id       = :1,
                     r.vt_Grouping_rule_id = :2
               WHERE r.ROWID = :3';

           Begin
             Execute Immediate gDDL_Command Using cRequest_id, vGrouping_Rule_id, rx2Rec.source_Rowid;
          
              Exception When Others Then
               xxcp_foundation.FndWriteError(150,'Set Running Status-2: SQLERRM='||SQLERRM, gDDL_Command);
               Raise INVALID_CURSOR;       
           End;
       END LOOP;       
       Close RX2;
      
    End If;
    -- Error Checking  

    IF vGroupingRuleExists then  -- 02.06.02 Don't Check If no Grouping Rule used.

      -- Find records with no vt_grouping_rule_id
      Open ErrCheck for
      'SELECT /*+ All_Rows */ DISTINCT r.vt_transaction_table
        FROM '||gInterface_Table_Name||' r, 
             xxcp_sys_source_tables t
       WHERE r.vt_request_id           = :1
         AND r.vt_source_assignment_id = :2
         AND r.vt_status               = ''GROUPING''
         AND r.vt_transaction_table = t.transaction_table
         AND t.Table_Group_id       = :3
         AND t.source_id            = :4
         AND t.grouping_rule_id IS NOT NULL
         AND r.vt_grouping_rule_id IS NULL'
         Using cRequest_id, 
               cSource_Assignment_id,
               cTable_Group_id, 
               cSource_id;         
       Loop

         Fetch ErrCheck into ErrCheckRec;
         Exit when ErrCheck%notfound;
        
        gDDL_Command :=         
          'UPDATE '||gInterface_Table_Name||' r
             SET r.vt_status  = ''ERROR'',
                 r.vt_date_processed = sysdate,
                 r.vt_internal_error_code = 11088
           WHERE r.vt_request_id           = :1
             AND r.vt_source_assignment_id = :2
             AND r.vt_transaction_table    = :3 
             AND r.vt_status = ''GROUPING''';

         Begin             
           Execute Immediate gDDL_Command Using cRequest_id, cSource_assignment_id, ErrCheckRec.vt_transaction_table;

           Exception When Others Then
               xxcp_foundation.FndWriteError(150,'Set Running Status-3: SQLERRM='||SQLERRM, gDDL_Command);
               Raise INVALID_CURSOR;       
         End;    
      END LOOP;
      Close ErrCheck;
      
      -- Find records with invalid Grouping rule       
      Open ErrCheck for
        'SELECT /*+ All_Rows */  DISTINCT g.vt_transaction_table
           FROM '||gInterface_Table_Name||' g, 
                xxcp_grouping_rules r, 
                xxcp_sys_source_tables t
          WHERE g.vt_request_id  = :1
           AND g.vt_source_assignment_id = :2
           AND g.vt_status  = ''GROUPING''
           AND g.vt_transaction_table = t.transaction_table
           AND t.Table_Group_id = :3
           AND t.Source_id  = :4
           AND g.vt_grouping_rule_id = r.grouping_rule_id(+)
           AND t.grouping_rule_id IS NOT NULL
           AND r.grouping_rule_id IS NULL'
           Using cRequest_id, 
                 cSource_Assignment_id, 
                 cTable_Group_id, 
                 cSource_id;         
        Loop

         Fetch ErrCheck into ErrCheckRec;
         Exit when ErrCheck%notfound;

         gDDL_Command :=         
          'UPDATE '||gInterface_Table_Name||' r
              SET r.vt_status = ''ERROR'',
                  r.vt_date_processed = sysdate,
                  r.vt_internal_error_code = 11089
            WHERE r.vt_request_id           = :1
              AND r.vt_source_assignment_id = :2
              AND r.vt_transaction_table    = :3
              AND r.vt_status = ''GROUPING''';

          Begin
            Execute Immediate gDDL_Command using cRequest_id,cSource_assignment_id, ErrCheckRec.vt_transaction_table;

            Exception When Others Then
               xxcp_foundation.FndWriteError(150,'Set Running Status-4: SQLERRM='||SQLERRM, gDDL_Command);
               Raise INVALID_CURSOR;       
          End;

        End Loop;
        Close ErrCheck;
    END IF;
    
    -- UNPOSTED TRANSACTIONS 
    --
    -- Check to see if we should test for Unposted Transactions.
    -- If the database trigger is being used, this is not necessary!! 
    --
    If xxcp_global.gCommon(1).Preview_ctl = 1 then
     BEGIN
      -- Check Lookup 
      SELECT UPPER(Lookup_Code) INTO vAction_flag 
        FROM xxcp_lookups
       WHERE numeric_code = 4
         AND lookup_type  = 'RECEIVABLE JOURNALS'
         AND lookup_type_subset = 'CHECK UNPOSTED'
         AND Enabled_flag = 'Y';
         
      IF vAction_flag = 'CHECK' THEN
         gDDL_Command := 
           'UPDATE '||gInterface_Table_Name||' r
             SET r.vt_status =  
               DECODE(XXCP_UNPOSTED_TRANSACTIONS.Check_Unposted_Rows(r.vt_transaction_table, r.vt_transaction_id
                                                                   , r.set_of_books_id),''Y'',''UNPOSTED'',r.vt_status)
             WHERE r.vt_request_id = :1 ';
          
            Begin 
              
              Execute Immediate gDDL_Command using cRequest_id;
         
              Exception When Others Then
                 xxcp_foundation.FndWriteError(150,'Set Running Status-5: SQLERRM='||SQLERRM, gDDL_Command);
                 Raise INVALID_CURSOR;       
            End; 
      END IF;
      
      EXCEPTION WHEN NO_DATA_FOUND THEN Null; 

     END;
    End If;
  
    COMMIT;
  
  END Set_Running_Status;
  
  --  !! ***********************************************************************************************************
  --  !!                                  Set_Assignment_Status
  --  !!                      Flag the records that are going to be processed.
  --  !! ***********************************************************************************************************
  PROCEDURE Set_Assignment_Status(cSource_id            IN NUMBER,
                                  cSource_assignment_id IN NUMBER,
                                  cRequest_id           IN NUMBER,
                                  cInternalErrorCode    IN OUT NUMBER) IS
 
    vDV_Statement       VARCHAR2(32000);
    vSQLERRM            VARCHAR2(512);

  BEGIN
    cInternalErrorCode := 0; 
  
    vDV_Statement := xxcp_dynamic_sql.Build_Interface_Selection(cSource_id, cSource_assignment_id, cRequest_id);
    
    IF vDV_Statement IS NOT NULL THEN
     EXECUTE IMMEDIATE vDV_Statement;
    END IF;  

    gDDL_Command :=       
        'UPDATE '||gInterface_Table_Name ||' r
           SET r.vt_status  = ''NEW'',
               r.vt_date_processed = sysdate,
               r.VT_STATUS_CODE = 7004
         WHERE r.vt_request_id           = :1
           AND r.vt_source_assignment_id = :2
           AND r.vt_status = ''GROUPING''';
       
    Begin
      Execute Immediate gDDL_Command using cRequest_id, cSource_assignment_id;     
           
      Exception When Others Then
        xxcp_foundation.FndWriteError(150,'Set Assignment Status-1: SQLERRM='||SQLERRM, gDDL_Command);
        Raise INVALID_CURSOR;       
    End;

    COMMIT;
                                
    EXCEPTION
      WHEN OTHERS THEN
        vSQLERRM := SQLERRM; -- New to trap Oracle's Reason for not working.
        xxcp_foundation.Set_InternalErrorCode(cInternalErrorCode, 10884);
        xxcp_foundation.FndWriteError(10884, vSQLERRM);

  END Set_Assignment_Status;

  --  !! ***********************************************************************************************************
  --  !!                                    CONTROL
  --  !!                  External Procedure (Entry Point) to start the AR Engine.
  --  !! ***********************************************************************************************************
  FUNCTION Control(cSource_Group_ID    IN NUMBER,
                   cConc_Request_Id    IN NUMBER,
                   cTable_Group_id     IN NUMBER   DEFAULT 0) RETURN NUMBER IS

    type  gli_type is REF CURSOR;

    -- Record Type of required gl_interface fields
    type gli_map_rec is record( 
              ACCOUNTED_CR                    xxcp_gl_eng_v.accounted_cr%type,
              ACCOUNTED_DR                    xxcp_gl_eng_v.accounted_dr%type,
              ACCOUNTING_DATE                 xxcp_gl_eng_v.accounting_date%type,
              ASG_EXPLOSION_ID                xxcp_gl_eng_v.asg_explosion_id%type,
              AUX_DATA1                       xxcp_gl_eng_v.aux_data1%type,
              AUX_DATA2                       xxcp_gl_eng_v.aux_data2%type,
              AUX_DATA3                       xxcp_gl_eng_v.aux_data3%type,
              AUX_DATA4                       xxcp_gl_eng_v.aux_data4%type,
              AUX_DATA5                       xxcp_gl_eng_v.aux_data5%type,
              BE2                             xxcp_gl_eng_v.be2%type,
              BE3                             xxcp_gl_eng_v.be3%type,
              BE4                             xxcp_gl_eng_v.be4%type,
              BE6                             xxcp_gl_eng_v.be6%type,
              BE7                             xxcp_gl_eng_v.be7%type,
              BE8                             xxcp_gl_eng_v.be8%type,
              CALC_LEGAL_EXCH_RATE            xxcp_gl_eng_v.calc_legal_exch_rate%type,
              CLASS_MAPPING_REQ               xxcp_gl_eng_v.class_mapping_req%type,
              CLASS_TRANSACTION_ID            xxcp_gl_eng_v.class_transaction_id%type,
              CODE_COMBINATION_ID             xxcp_gl_eng_v.code_combination_id%type,
              CURRENCY_CODE                   xxcp_gl_eng_v.currency_code%type,
              ENTERED_CR                      xxcp_gl_eng_v.entered_cr%type,
              ENTERED_DR                      xxcp_gl_eng_v.entered_dr%type,
              GL_SL_LINK_ID                   xxcp_gl_eng_v.gl_sl_link_id%type,
              GL_SL_LINK_TABLE                xxcp_gl_eng_v.gl_sl_link_table%type,
              GROUP_ID                        xxcp_gl_eng_v.group_id%type,
              ORDER_SOURCE_ID                 xxcp_gl_eng_v.Order_Source_id%type,
              PARENT_ENTERED_AMOUNT           xxcp_gl_eng_v.parent_entered_amount%type,
              PARENT_ENTERED_CURRENCY         xxcp_gl_eng_v.parent_entered_currency%type,
              SEGMENT1                        xxcp_gl_eng_v.segment1%type,
              SEGMENT2                        xxcp_gl_eng_v.segment2%type,
              SEGMENT3                        xxcp_gl_eng_v.segment3%type,
              SEGMENT4                        xxcp_gl_eng_v.segment4%type,
              SEGMENT5                        xxcp_gl_eng_v.segment5%type,
              SEGMENT6                        xxcp_gl_eng_v.segment6%type,
              SEGMENT7                        xxcp_gl_eng_v.segment7%type,
              SEGMENT8                        xxcp_gl_eng_v.segment8%type,
              SEGMENT9                        xxcp_gl_eng_v.segment9%type,
              SEGMENT10                       xxcp_gl_eng_v.segment10%type,
              SEGMENT11                       xxcp_gl_eng_v.segment11%type,
              SEGMENT12                       xxcp_gl_eng_v.segment12%type,
              SEGMENT13                       xxcp_gl_eng_v.segment13%type,
              SEGMENT14                       xxcp_gl_eng_v.segment14%type,
              SEGMENT15                       xxcp_gl_eng_v.segment15%type,
              SET_OF_BOOKS_ID                 xxcp_gl_eng_v.set_of_books_id%type,
              SOURCE_CLASS_ID                 xxcp_gl_eng_v.source_class_id%type,
              SOURCE_CREATION_DATE            xxcp_gl_eng_v.source_creation_date%type,
              SOURCE_ID                       xxcp_gl_eng_v.source_id%type,
              SOURCE_ROWID                    xxcp_gl_eng_v.source_rowid%type,
              SOURCE_SET_OF_BOOKS_ID          xxcp_gl_eng_v.source_set_of_books_id%type,
              SOURCE_TABLE_ID                 xxcp_gl_eng_v.source_table_id%type,
              SOURCE_TYPE_ID                  xxcp_gl_eng_v.source_type_id%type,
              TABLE_GROUP_ID                  xxcp_gl_eng_v.table_group_id%type,
              TRANSACTION_COST                xxcp_gl_eng_v.transaction_cost%type,
              TRANSACTION_SET_ID              xxcp_gl_eng_v.transaction_set_id%type,
              TRANSACTION_SET_NAME            xxcp_gl_eng_v.transaction_set_name%type,
              TRX_EXPLOSION_ID                xxcp_gl_eng_v.trx_explosion_id%type,
              USER_JE_CATEGORY_NAME           xxcp_gl_eng_v.user_je_category_name%type,
              USER_JE_SOURCE_NAME             xxcp_gl_eng_v.user_je_source_name%type,
              VT_INSTANCE_ID                  xxcp_gl_eng_v.vt_instance_id%type,
              VT_INTERFACE_ID                 xxcp_gl_eng_v.vt_interface_id%type,
              VT_PARENT_TRX_ID                xxcp_gl_eng_v.vt_parent_trx_id%type,
              VT_REQUEST_ID                   xxcp_gl_eng_v.vt_request_id%type,
              VT_SOURCE_ASSIGNMENT_ID         xxcp_gl_eng_v.vt_source_assignment_id%type,
              VT_STATUS                       xxcp_gl_eng_v.vt_status%type,
              VT_TRANSACTION_CLASS            xxcp_gl_eng_v.vt_transaction_class%type,
              VT_TRANSACTION_DATE             xxcp_gl_eng_v.vt_transaction_date%type,
              VT_TRANSACTION_ID               xxcp_gl_eng_v.vt_transaction_id%type,
              VT_TRANSACTION_REF              xxcp_gl_eng_v.vt_transaction_ref%type,
              VT_TRANSACTION_TABLE            xxcp_gl_eng_v.vt_transaction_table%type,
              VT_TRANSACTION_TYPE             xxcp_gl_eng_v.vt_transaction_type%type
         );
                          
    -- Assignment Cursor      
    cfg     gli_type;
    cfgRec  gli_map_rec;
    -- Transactions Cursor
    gli     gli_type;
    gliRec  gli_map_rec;

    type  ConfErr_type is REF CURSOR;

    -- Record Type of required gl_interface fields
    type ConfErr_map_rec is record( 
          VT_TRANSACTION_TABLE       xxcp_gl_eng_v.vt_transaction_table%type,
          VT_PARENT_TRX_ID           xxcp_gl_eng_v.vt_parent_trx_id%type
         );
                                
    ConfErr     ConfErr_type;
    ConfErrRec  ConfErr_map_rec;

    -- Source Assignments
    CURSOR sr1(pSource_Group_id IN NUMBER) IS
      SELECT s.set_of_books_id,
             s.Source_Assignment_id,
             s.instance_id source_instance_id,
             s.Source_id,
             s.stamp_parent_trx
        FROM xxcp_source_assignments s
       WHERE s.Source_Group_id = pSource_Group_id
         AND s.active          = 'Y';
  
    vInternalErrorCode         xxcp_errors.internal_error_code%TYPE := 0;
    vExchange_Rate_Type        xxcp_tax_registrations.exchange_rate_type%TYPE;
    vCommon_Exch_Curr          xxcp_tax_registrations.common_exch_curr%TYPE;
  
    i                          PLS_INTEGER := 0;
    j                          INTEGER := 0;
    k                          NUMBER;
    vTiming_Start              NUMBER;
    vDML_Compiled              VARCHAR2(1);
    vGlobal_Rounding_Tolerance NUMBER := 0;
    vTransaction_Error         BOOLEAN := FALSE;
    
    vParent_Rowid              Rowid;
  
    vGlobalPrecision           PLS_INTEGER := 2;
    CommitCnt                  PLS_INTEGER := 0;
    vJob_Status                PLS_INTEGER := 0;
    vSource_Activity           xxcp_sys_sources.source_activity%TYPE := 'GEN';
    vStaged_Records            xxcp_sys_sources.staged_records%TYPE;
  
    vTransaction_Class         xxcp_sys_source_classes.CLASS%TYPE;
    vRequest_ID                xxcp_process_history.request_id%TYPE;
  
    -- NEW PARAMETERS
    vCurrent_Parent_Trx_id     xxcp_transaction_attributes.parent_trx_id%TYPE := 0;
    vTransaction_Type          xxcp_sys_source_types.TYPE%TYPE;
    --
    -- Used for the REC Distribution
    vExtraLoop                 BOOLEAN := FALSE;
    vExtraClass                xxcp_sys_source_classes.CLASS%TYPE;
    --
    vSource_Assignment_id      xxcp_source_assignments.source_assignment_id%TYPE; 
    vSource_instance_id        xxcp_source_assignments.instance_id%TYPE;
    vSource_ID                 xxcp_sys_sources.source_id%TYPE; 
    vClass_Mapping_Name        xxcp_sys_source_classes.CLASS%TYPE;
    -- 02.05.02
    vDuplicate_check           xxcp_sys_sources.duplicate_check%type;
    vDuplicate_found           varchar2(1);
    
    vError_Completion_Status   xxcp_source_assignment_groups.error_completion_status%TYPE := 'N';                                     
     
    
    CURSOR SF(pSource_Group_id IN NUMBER) IS
      SELECT s.source_activity,
             s.Source_id,
             s.Preview_ctl,
             s.Timing,
             s.Custom_events,
             s.Cached,
             s.DML_Compiled,
             s.Staged_Records,
             g.instance_id source_instance_id,
             s.source_base_table Interface_Table_Name,
             nvl(s.duplicate_check,'N') duplicate_check,
             nvl(g.error_completion_status,'N') error_completion_status,
             -- Engine Ctl
             c.engine_interface_view,
             c.short_code Source_Short_Name,
             -- 03.06.18
             nvl(s.flat_file,'N') flat_file,
             nvl(s.ics_flag,'N') ic_settlements,
             -- 03.06.25
             nvl(s.multithread_engine,'N') multithread_engine
        FROM xxcp_source_assignment_groups g,
             xxcp_sys_sources s,
             xxcp_sys_engine_ctl c
       WHERE source_group_id = pSource_Group_id
         AND s.source_id     = g.source_id
         AND s.preview_ctl   = c.preview_ctl
         AND c.Short_Code is not null;
  
    CURSOR Tolx IS
      SELECT Numeric_Code Global_Rounding_Tolerance
        FROM xxcp_lookups
       WHERE lookup_type = 'ROUNDING TOLERANCE'
         AND lookup_code = 'GLOBAL';
  
     CURSOR CRL(pSource_id IN NUMBER) IS
     SELECT DISTINCT Transaction_table, source_id
       FROM xxcp_column_rules
      WHERE source_id = pSource_id; 
      
    CURSOR TGR(pSource_id IN NUMBER, pTrx_Group_id IN NUMBER) IS
      select g.trx_group_required, nvl(g.parent_commit_count,2000) parent_commit_count
      from xxcp_sys_table_groups g
      where g.source_id      = pSource_id
        and g.table_group_id = pTrx_Group_id;
        
    Cursor CHDR(pSource_Assignment_id in number, pTransaction_Table in varchar2, pTransaction_Ref in varchar2, pOrder_Source_id in number) is
     select hd.rowid parent_rowid
       from XXCP_oe_headers_iface       hd
        where hd.VT_SOURCE_ASSIGNMENT_ID = pSource_Assignment_id
          and hd.VT_TRANSACTION_TABLE    = pTransaction_Table
          and hd.VT_TRANSACTION_REF      = pTransaction_Ref
          and hd.ORDER_SOURCE_ID         = pOrder_Source_id;
        
    Cursor OE_HDR is
     select nvl(lookup_code,'N') code
      from XXCP_lookups jk
      where jk.lookup_code = 'USE OE HEADERS'
        and jk.numeric_code = 19;
   
    vTrx_Group_id            Number; 
    vErrorMessage            xxcp_errors.error_message%type;
    
    -- Dynamic Explosion 
    vExplosion_summary      NUMBER;
    vLast_source_type_id    xxcp_sys_source_types.source_type_id%TYPE := 0;
    vExplosion_sql          VARCHAR2(4000);
    vRowsfound              NUMBER;
    jx                      NUMBER;
    vExplosion_trx_id       NUMBER;
    vExplosion_source_rowid VARCHAR2(32);
    vExplosion_trx_type_id  NUMBER(15);
    vExplosion_trx_class_id NUMBER(15);
    
    -- 03.06.25
    vProcess_Run_Id           Number;    
    vDuplicate_Check_Days     Number := 0;
    vMin_Duplicate_Check_Date Date;
    
  BEGIN

    xxcp_global.Trace_on   := NVL(gEngine_Trace,'N');
    
    xxcp_global.Preview_on := 'N';
    xxcp_global.SystemDate := Sysdate;
    vTimingCnt             := 1;
    gEngine_Error_Found    := FALSE;
    gForce_Job_Error       := FALSE;
    gForce_Job_Warning     := FALSE;

    FOR SFRec IN SF(cSource_Group_id) LOOP
      
      gInterface_Table_Name := SFRec.Interface_Table_Name;
      vSource_Activity      := SFRec.Source_Activity;
      vSource_id            := SFRec.Source_id;
      vDML_Compiled         := SFRec.DML_Compiled;
      vStaged_Records       := SFRec.Staged_Records;
      vSource_Instance_id   := SFRec.Source_Instance_id;
      vDuplicate_check      := SFRec.Duplicate_Check;

      xxcp_global.gCommon(1).Source_id     := vSource_id;
      xxcp_global.gCommon(1).Preview_ctl   := SFRec.Preview_ctl;
      xxcp_global.gCommon(1).Preview_on    := xxcp_global.Preview_on;
      xxcp_global.gCommon(1).Custom_events := SFRec.Custom_events;
      xxcp_global.gCommon(1).Current_Source_activity := vSource_Activity;
      xxcp_global.gCommon(1).Cached_Attributes := SFRec.Cached; 
      xxcp_global.gCommon(1).Current_Table_Group_id := cTable_Group_id;
      xxcp_global.gCommon(1).Current_Trx_Group_id := xxcp_global.Get_Interface_Parameters('GPRM_JOURNAL_GROUP_ID');
      vTrx_Group_id := to_number(xxcp_global.gCommon(1).Current_Trx_Group_id);
      --
      vError_Completion_Status := SFRec.Error_Completion_Status;
      -- Engine Control      
      gInterface_View_Name := SFRec.Engine_Interface_View;
      Setup_Wrapper_Source_Group(cSource_Short_Name => gSource_Short_Name);
      
       -- 03.06.18
       xxcp_global.gCommon(1).flat_file := SFRec.flat_file;
       xxcp_global.gCommon(1).ic_settlements := SFRec.ic_settlements;    
       
       -- 03.06.25
       vProcess_Run_Id := 0; -- needs to be zero if not mutlitreading
       If SFRec.Multithread_engine = 'Y' Then
         -- Use the concurrent request id. This number may be recycled
         -- but as long a the number is not reused whilst the job
         -- is active then it will not matter.
         vProcess_Run_Id := cConc_Request_Id;
       End If;
             
    END LOOP;
    
    -- OE Headers off
    If vSource_id = gOE_SOURCE_id then
       For Rec in OE_HDR loop
         gUse_OE_Headers := rec.Code;
       End Loop;        
    End If;
    

    xxcp_global.set_source_id(cSource_id => vSource_id);

    -- Duplicate Check Days
    -- 02.06.03
    Begin
      Select lookup_code
        into vDuplicate_Check_Days
        from xxcp_lookups
       where lookup_type  = 'DUPLICATE CHECK DAYS'
         and numeric_code = vSource_id
         and enabled_flag = 'Y';

    Exception when OTHERS then
      vDuplicate_Check_Days := 0;
    End;

    If vDuplicate_Check_Days > 0 then
      vMin_Duplicate_Check_Date := sysdate - vDuplicate_Check_Days;
    Else
      vMin_Duplicate_Check_Date := to_date('01-JAN-2000','DD-MON-YYYY');
    End if;


    vTiming(vTimingCnt).CF_Records    := 0;
    vTiming(vTimingCnt).Flush_Records := 0;
    vTiming(vTimingCnt).Start_time    := to_char(Sysdate, 'HH24:MI:SS');
    vTiming(vTimingCnt).FLUSH         := 0;

    vTiming_Start  := xxcp_reporting.Get_Seconds;
  
    IF xxcp_global.gCommon(1).Custom_events = 'Y' THEN
      xxcp_custom_events.Set_system_mode(xxcp_global.gCommon);
    END IF;
  
    -- Setup Flush Commands
    If DDL_Statement_Setup(cPreview_Ctl => xxcp_global.gCommon(1).Preview_ctl) > 0 then
      vJob_Status := 1; -- fail
    End If;
    --
    -- ## *******************************************************************************************************
    -- ##
    -- ##                 Check to See Column Rules are compiled
    -- ##
    -- ## *******************************************************************************************************
    --
    IF vDML_Compiled = 'N' THEN
     -- Prevent Processing and the column rules have changed and
     -- they need compiling.
     vJob_Status := 4;
     xxcp_foundation.show('ERROR: Column Rules have changed, but not re-generated'); 
     xxcp_foundation.FndWriteError(10999,'Column Rules have changed, but not re-generated');

    END IF;

    xxcp_global.Set_Last_Request_id(0);

    --
    -- ## *******************************************************************************************************
    -- ##
    -- ##      Check the Journal Group against Table Groups
    -- ## If the transaction Group is required and the Transaction Group has not been provided
    -- ## The job will terminate with an error code of 8
    -- ##
    -- ## *******************************************************************************************************
    --
    FOR Rec IN TGR(vSource_id, cTable_Group_id) LOOP
      
      gParent_Commit_Count := Rec.Parent_Commit_Count;
    
      IF rec.Trx_Group_Required = 'Y' AND  vTrx_Group_id = 0 THEN
        vJob_Status := 8;
      END IF;
    END LOOP;

    --
    -- ## *******************************************************************************************************
    -- ##
    -- ##                            Check to See the process is already in use
    -- ##
    -- ## *******************************************************************************************************
    --
    IF NOT xxcp_management.Locked_Process(cSource_Activity => vSource_Activity,
                                          cSource_Group_id => cSource_Group_id,
                                          cTable_Group_id  => cTable_Group_id, 
                                          cTrx_Group_id    => nvl(vTrx_Group_id,0),
                                          cProcess_Run_Id  => vProcess_Run_Id
                                         ) AND vJob_Status = 0 THEN
                                         
      vRequest_id := xxcp_management.Activate_Lock_Process(cSource_Activity => vSource_Activity,
                                                           cSource_Group_id => cSource_Group_id,
                                                           cConc_Request_Id => cConc_Request_ID,
                                                           cTable_Group_id  => cTable_Group_id,
                                                           cTrx_Group_id    => nvl(vTrx_Group_id,0),
                                                           cProcess_Run_Id  => vProcess_Run_Id);

      xxcp_global.Set_Last_Request_id(vRequest_id);
      xxcp_foundation.show('Activity locked....' || TO_CHAR(vRequest_id));

      xxcp_global.User_id  := fnd_global.user_id;
      xxcp_global.Login_id := fnd_global.login_id;

      xxcp_global.gCommon(1).Current_request_id := vRequest_id;

      IF xxcp_global.gCommon(1).Custom_events = 'Y' THEN
        vInternalErrorCode := xxcp_custom_events.Before_Processing(vSource_id);
      END IF;
      
      --
      -- ## **************************************************************************************************
      -- ##
      -- ##                                Setup Ready for the Run
      -- ##
      -- ## **************************************************************************************************
      --
      FOR REC IN sr1(cSource_Group_id) LOOP
      
        EXIT WHEN vJob_Status <> 0;
      
        vSource_Instance_id   := rec.Source_instance_id;
        vSource_Assignment_id := rec.Source_Assignment_id; 
        
        vTiming(vTimingCnt).Init_Start := to_char(Sysdate, 'HH24:MI:SS');
        vTiming(vTimingCnt).Flush_Records := 0;

        IF vTimingCnt = 1 THEN
            xxcp_foundation.show('Initialization....');
            xxcp_te_base.Engine_Init(cSource_id         => vSource_id,
                                     cSource_Group_id   => cSource_Group_id,
                                     cInternalErrorCode => vInternalErrorCode);
        END IF;
        
        vTiming(vTimingCnt).Source_Assignment_id := vSource_Assignment_id;
      
        -- Setup Globals
        xxcp_global.gCommon(1).Current_process_name  := 'XXCP_'||gSource_Short_Name||'_ENG';
        xxcp_global.gCommon(1).Current_assignment_id := vSource_Assignment_ID;
        vCurrent_Parent_Trx_id := 0;
        xxcp_global.gTrans_Table_Array.delete;
        
        -- 03.06.24
        xxcp_global.Clear_Parent_Trx_Arrays('A');       

        --
        -- *********************************************************************************************************
        --                            Populate Column Rules and Initialize Cursors
        -- *********************************************************************************************************
        --
        vTiming(vTimingCnt).Memory_Start := to_char(Sysdate, 'HH24:MI:SS');

        FOR Rec IN CRL(pSource_id => vSource_id) LOOP
          -- Used for column Attributes and error messages 
          --
          xxcp_memory_Pack.LoadColumnRules(cSource_id          => vSource_id, 
                                           cSource_Table       => gInterface_Table_Name,
                                           cSource_Instance_id => vSource_instance_id,
                                           cTarget_Table       => Rec.Transaction_table,
                                           cRequest_id         => vRequest_id,
                                           cInternalErrorCode  => vInternalErrorCode);

          xxcp_te_base.Init_Cursors(cTarget_Table => Rec.Transaction_table);
        END LOOP;
      
        --
        -- *******************************************************************************************************
        --     Reset Previosly Errored Transactions and Assign Parent Trx Id to Incomming transactions
        -- *******************************************************************************************************
        --
        
        vInternalErrorCode := 0;
        
        Reset_Errored_Transactions(cSource_id            => vSource_id,
                                   cSource_assignment_id => vSource_Assignment_id, 
                                   cRequest_id           => vRequest_id,
                                   cTable_Group_id       => cTable_Group_id,
                                   cTrx_Group_id         => vTrx_Group_id);
                                   
        Set_Running_Status(        cSource_id            => vSource_id, 
                                   cSource_assignment_id => vSource_Assignment_id, 
                                   cSource_Group_id      => cSource_Group_id,
                                   cRequest_id           => vRequest_id,
                                   cTable_Group_id       => cTable_Group_id,
                                   cTrx_Group_id         => vTrx_Group_id);
                      
        If xxcp_dynamic_sql.Apply_Exclude_Rule(cSource_Assignment_id => vSource_assignment_id) = 0 then

           Transaction_Group_Stamp(cSource_Assignment_id => vSource_Assignment_id, 
                                   cTable_Group_id       => cTable_Group_id,
                                   cInternalErrorCode    => vInternalErrorCode);
        End If;
        
        -- Set Assignment Status -- (Not used in Preview)
        IF vInternalErrorCode = 0 THEN 
          Set_Assignment_Status(cSource_id            => vSource_id,
                                cSource_Assignment_id => vSource_assignment_id,
                                cRequest_id           => vRequest_id,
                                cInternalErrorCode    => vInternalErrorCode);
                                  
        END IF;
        
        -- Data Staging 
        IF NVL(vInternalErrorCode,0) = 0 THEN
          vInternalErrorCode :=
            xxcp_custom_events.Data_Staging(cSource_id            => vSource_id,
                                            cSource_Assignment_id => vSource_Assignment_id,
                                            cRequest_id           => vRequest_id, 
                                            cPreview_id           => NULL);
        END IF;
        
        vTiming(vTimingCnt).Init_End := to_char(Sysdate, 'HH24:MI:SS');
        --
        -- GLOBAL ROUNDING TOLERANCE
        --
        vGlobal_Rounding_Tolerance := 0;
        FOR Rec IN Tolx LOOP
          vGlobal_Rounding_Tolerance := Rec.Global_Rounding_Tolerance;
        END LOOP;

        IF vInternalErrorCode = 0 THEN
          --
          -- Get Standard Parameters
          --
          vInternalErrorCode := 
            xxcp_foundation.fnd_gl_lookup(cExchange_Rate_Type => vExchange_Rate_Type,
                                          cGlobalPrecision    => vGlobalPrecision,
                                          cCommon_Exch_Curr   => vCommon_Exch_Curr);
        END IF;

        COMMIT;

        IF vInternalErrorCode = 0 THEN

          --
          -- ## ***********************************************************************************************************
          -- ##                                Assignment
          -- ## ***********************************************************************************************************
          --
          i := 0;

          vTiming(vTimingCnt).CF_last     := xxcp_reporting.Get_Seconds;
          vTiming(vTimingCnt).CF          := 0;
          vTiming(vTimingCnt).CF_Records  := 0;
          vTiming(vTimingCnt).CF_Start_Date := Sysdate;
          xxcp_global.SystemDate := Sysdate;

          -- !!
          -- !! ASSIGNMENT CURSOR 
          -- !!
          Open CFG for gDDL_Interface_Select
                  Using 'ASSIGNMENT', vSource_id, vSource_Assignment_id, vRequest_id, cTable_Group_id;
          Loop

                Fetch CFG into CFGRec;
                Exit when CFG%notfound;
          
                i := i + 1;

                vTiming(vTimingCnt).CF_RECORDS := vTiming(vTimingCnt).CF_RECORDS + 1;

                xxcp_global.gCommon(1).Current_SOURCE_ROWID      := CFGRec.SOURCE_ROWID;
                xxcp_global.gCommon(1).Current_TRANSACTION_TABLE := CFGRec.VT_TRANSACTION_TABLE;
                xxcp_global.gCommon(1).Current_TRANSACTION_ID    := CFGRec.VT_TRANSACTION_ID;
                xxcp_global.gCommon(1).Calc_legal_exch_rate      := CFGRec.calc_legal_exch_rate;
                xxcp_global.gCommon(1).Current_PARENT_TRX_ID     := CFGRec.VT_PARENT_TRX_ID;
                xxcp_global.gCommon(1).Current_INTERFACE_ID      := CFGRec.VT_INTERFACE_ID;
                xxcp_global.gCommon(1).Current_TRANSACTION_DATE  := CFGRec.VT_TRANSACTION_DATE;
                xxcp_global.gCommon(1).Current_INTERFACE_REF     := CFGRec.VT_TRANSACTION_REF; 
                xxcp_global.gCommon(1).PREVIEW_PARENT_TRX_ID     := Null;
                xxcp_global.gCommon(1).PREVIEW_SOURCE_ROWID      := Null;
                
                -- 03.06.24 Only populate invoice_parent_trx_id for PO Receipt Matching Source                  
                if xxcp_global.gCommon(1).Source_id = 43 then 
                  xxcp_global.gInvoice_Matching_Common(1).invoice_parent_trx_id        := CFGRec.AUX_DATA1; 
                  xxcp_global.gInvoice_Matching_Common(1).invoice_source_assignment_id := CFGRec.AUX_DATA2; 
                  xxcp_global.gInvoice_Matching_Common(1).invoice_transaction_table    := CFGRec.AUX_DATA3;
                end if;  
                
                vTransaction_Class := CFGRec.VT_TRANSACTION_CLASS;
                                
                -- 02.05.02
                -- If the duplicate check is enabled for the source
                IF vDuplicate_check = 'Y' then 
                
                  vDuplicate_found := 'N';
                  
                  -- this cursor checks to see if the record already exists. 
                  gDDL_Command := 
                         'Select count(*) Cnt 
                            from '||gInterface_Table_Name||' r
                            where r.vt_source_assignment_id =  :1
                              and r.vt_transaction_table    =  :2
                              and r.vt_transaction_id       =  :3
                              and r.rowid                  !=  :4 
                              and r.vt_status              != ''DUPLICATE''
                              and vt_transaction_date       >  :5';
                   
                  Begin
                     Execute Immediate gDDL_Command 
                            INTO j 
                           Using vSource_assignment_id,
                                 CFGRec.VT_Transaction_Table,
                                 CFGRec.VT_Transaction_ID,
                                 CFGRec.Source_Rowid,
                                 vMin_Duplicate_Check_Date;
                                 
                      If j > 0 then
                        vDuplicate_found := 'Y';
                      End If;
                  
                     Exception when others then Null;
                  
                  End;         
                End if;
                
                If vCurrent_Parent_Trx_id <> CFGRec.vt_Parent_Trx_id then  -- New Parent Trx Id
                  xxcp_global.gPassThroughCode := 0;
                End if;
                
                vCurrent_Parent_Trx_id := CFGRec.vt_Parent_Trx_id;
                
                -- Branch for Dynamic Explosions 
                IF cfgrec.Asg_Explosion_id > 0 THEN
                   -- Explosion Processing
                   IF cfgrec.Source_Type_id != vLast_Source_Type_id THEN
                      vInternalErrorCode := xxcp_dynamic_sql.ReadExplosionDef(cExplosion_id    => cfgrec.Asg_Explosion_id, 
                                                                              cExplosion_sql   => vExplosion_Sql, 
                                                                              cSummary_columns => vExplosion_Summary);
                       
                      vLast_Source_Type_id := cfgrec.Source_Type_id;
                   END IF;
                   
                   If vInternalErrorCode = 0 then
                     -- Call Explosion
                     vInternalErrorCode := xxcp_dynamic_sql.ExplosionInitProcess(
                                                           cSource_type_id    => cfgrec.Source_type_id, 
                                                           cSource_rowid      => cfgrec.Source_rowid, 
                                                           cInterface_id      => cfgrec.vt_interface_id, 
                                                           cExplosion_sql     => vExplosion_Sql, 
                                                           cExplosion_summary => vExplosion_summary, 
                                                           cColumncount       => 2, 
                                                           cRowsfound         => vRowsfound, 
                                                           cTransaction_type  => cfgrec.vt_transaction_type, 
                                                           cTransaction_class => cfgrec.vt_transaction_class);
                   End If;
                                 
                   IF vRowsFound > 0 AND vInternalErrorCode = 0 THEN
                     
                        FOR jx IN 1 .. vRowsFound LOOP
                        
                           i := i + 1;
                        
                           vExplosion_Source_rowid := xxcp_dynamic_sql.gDataColumn1(jx);
                           vExplosion_Trx_id       := xxcp_dynamic_sql.gDataColumn2(jx);
                           vTransaction_Type       := xxcp_dynamic_sql.gDataColumn3(jx);
                           vTransaction_Class      := xxcp_dynamic_sql.gDataColumn4(jx);
                           vExplosion_Trx_type_id  := xxcp_dynamic_sql.gDataColumnTypeId(jx);
                           vExplosion_Trx_class_id := xxcp_dynamic_sql.gDataColumnClassId(jx);
                        
                           xxcp_global.gCommon(1).Explosion_Rowid          := vExplosion_Source_Rowid;
                           xxcp_global.gCommon(1).Explosion_Transaction_id := vExplosion_Trx_id;
                           xxcp_global.gCommon(1).Explosion_RowsFound      := vRowsFound;
                           
                           -- Record Checks must work with the orginial interface values
                           If Record_Checks(cSource_Rowid      => CFGRec.SOURCE_ROWID, 
                                            cTransaction_Table => CFGRec.VT_Transaction_Table,
                                            cTransaction_Class => CFGRec.VT_Transaction_Class,
                                            cDuplicate_check   => vDuplicate_Check,
                                            cDuplicate_found   => vDuplicate_Found) = 0 then
                        
                                  XXCP_TE_CONFIGURATOR.Control(
                                               cSource_Assignment_ID     => vSource_Assignment_id,
                                               cSet_of_books_id          => CFGRec.Source_Set_of_books_id,
                                               cTransaction_Date         => CFGRec.VT_Transaction_Date,
                                               cSource_id                => vSource_id,
                                               cSource_Table_id          => CFGRec.Source_table_id,
                                               cSource_Type_id           => vExplosion_Trx_Type_id,
                                               cSource_Class_id          => vExplosion_Trx_Class_id,
                                               cTransaction_id           => vExplosion_Trx_id,
                                               cSourceRowid              => CFGRec.Source_Rowid,
                                               cParent_Trx_id            => CFGRec.VT_Parent_Trx_id,
                                               cTransaction_Set_id       => CFGRec.Transaction_Set_id,
                                               cSpecial_Array            => 'N', 
                                               cKey                      => NULL, 
                                               cTransaction_Table        => CFGRec.VT_Transaction_Table,
                                               cTransaction_Type         => CFGRec.VT_Transaction_Type,
                                               cTransaction_Class        => vTransaction_Class,
                                               cInternalErrorCode        => vInternalErrorCode);
                             End If;     
                                                                       
                           EXIT WHEN vInternalErrorCode > 0;
                           
                        END LOOP;
                     
                     ELSIF vInternalErrorCode = 0 THEN
                        vInternalErrorCode := 3441;
                     END IF;
                  
                  ELSE                
                       -- Normal Processing   
                       If Record_Checks(cSource_Rowid   => CFGRec.Source_Rowid, 
                                     cTransaction_Table => CFGRec.VT_Transaction_Table,
                                     cTransaction_Class => vTransaction_Class,
                                     cDuplicate_check   => vDuplicate_Check,
                                     cDuplicate_found   => vDuplicate_Found) = 0 then
                                         
                                 XXCP_TE_CONFIGURATOR.Control(
                                                     cSource_Assignment_ID     => vSource_Assignment_id,
                                                     cSet_of_books_id          => CFGRec.Source_Set_of_books_id,
                                                     cTransaction_Date         => CFGRec.VT_Transaction_Date,
                                                     cSource_id                => vSource_id,
                                                     cSource_Table_id          => CFGRec.Source_table_id,
                                                     cSource_Type_id           => CFGRec.Source_type_id,
                                                     cSource_Class_id          => CFGRec.Source_Class_id,
                                                     cTransaction_id           => CFGRec.VT_Transaction_ID,
                                                     cSourceRowid              => CFGRec.Source_Rowid,
                                                     cParent_Trx_id            => CFGRec.VT_Parent_Trx_id,
                                                     cTransaction_Set_id       => CFGRec.Transaction_Set_id,
                                                     cSpecial_Array            => 'N', 
                                                     cKey                      => NULL, 
                                                     cTransaction_Table        => CFGRec.VT_Transaction_Table,
                                                     cTransaction_Type         => CFGRec.VT_Transaction_Type,
                                                     cTransaction_Class        => vTransaction_Class,
                                                     cInternalErrorCode        => vInternalErrorCode);
                        End If; -- End Explosion
                
                END IF; -- end configuration

                -- check status
                IF xxcp_global.Get_Release_Date IS NOT NULL THEN -- Release Date
                  Stop_Whole_Transaction; 
                        
                -- 03.06.23
                ELSIF xxcp_global.Get_Unmatched_flag = 'Y' then  
                         
                  gDDL_Command := 
                     'UPDATE '||gInterface_Table_Name||' r
                         SET r.vt_status = ''UN-MATCHED'',
                             r.vt_internal_error_code = NULL,
                             r.vt_date_processed = sysdate,
                             r.vt_parent_trx_id = null
                       WHERE r.ROWID = :1
                         AND r.vt_status NOT IN (''IGNORED'',''DUPLICATE'')';
                           
                   Begin   
                     Execute Immediate gDDL_Command using CFGRec.Source_Rowid;  

                     xxcp_global.Set_Unmatched_Flag('N');

                     Exception When Others Then
                       xxcp_foundation.FndWriteError(150,'Control-3: SQLERRM='||SQLERRM, gDDL_Command);
                       Raise INVALID_CURSOR;       
                   End;                          
                ELSIF NVL(vInternalErrorCode, 0) = 0 THEN
                          
                  gDDL_Command := 
                     'UPDATE '||gInterface_Table_Name||' r
                         SET r.vt_status = ''TRANSACTION'',
                             r.vt_internal_error_code = NULL,
                             r.vt_date_processed = sysdate
                       WHERE r.ROWID = :1
                         AND r.vt_status NOT IN (''IGNORED'',''DUPLICATE'')';
                           
                   Begin   
                     Execute Immediate gDDL_Command using CFGRec.Source_Rowid;  

                     Exception When Others Then
                       xxcp_foundation.FndWriteError(150,'Control-3: SQLERRM='||SQLERRM, gDDL_Command);
                       Raise INVALID_CURSOR;       
                   End;  
                ELSIF vInternalErrorCode = -1 THEN
                  gDDL_Command := 
                  'UPDATE '||gInterface_Table_Name||' r
                     SET r.vt_status  = ''SUCCESSFUL'',
                         r.vt_internal_error_code = NULL,
                         r.vt_date_processed  = sysdate,
                         r.vt_status_code = 7001
                   WHERE r.ROWID = :1';

                   Begin
                     Execute Immediate gDDL_Command using CFGRec.Source_Rowid;    

                     Exception When Others Then
                       xxcp_foundation.FndWriteError(150,'Control-4: SQLERRM='||SQLERRM, gDDL_Command);
                       Raise INVALID_CURSOR;       
                   End;                     
                ELSIF vInternalErrorCode BETWEEN 7021 and 7028 then
                  Pass_Through_Whole_Trx(CFGRec.Source_Rowid, vInternalErrorCode);
                ELSIF NVL(vInternalErrorCode, 0) <> 0 THEN

                  gDDL_Command := 
                    'UPDATE '||gInterface_Table_Name||' r
                        SET r.vt_status = ''ERROR'',
                            r.vt_internal_error_code = 12000,
                            r.vt_date_processed = sysdate
                     WHERE r.ROWID = :1';

                   Begin 
                     Execute Immediate gDDL_Command using CFGRec.Source_Rowid;   
       
                      Exception When Others Then
                       xxcp_foundation.FndWriteError(150,'Control-5: SQLERRM='||SQLERRM, gDDL_Command);
                       Raise INVALID_CURSOR;       
                   End;                    
                   -- Flag Error
                   gEngine_Error_Found := TRUE;
                           
                END IF;
                        
                -- emergency exit (out of disk space) 
                IF vInternalErrorCode BETWEEN 3550 AND 3559 THEN
                  ROLLBACK;
                  vJob_Status := 6;
                  EXIT; 
                END IF;

           
                -- ##
                -- ## COMMIT CONFIGURED ROWS
                -- ##
              
                IF i >= gParent_Commit_Count THEN
                  xxcp_global.systemdate := Sysdate;
                  COMMIT;
                  i := 0; 
                  
                  vJob_Status := 
                    xxcp_management.Has_Oracle_Been_Terminated(
                                         cRequest_id      => vRequest_id,
                                         cSource_Activity => vSource_activity,
                                         cSource_Group_id => cSource_group_id,
                                         cConc_Request_Id => cConc_request_id
                                         );
                  EXIT WHEN vJob_Status > 0; -- controlled exit
                END IF;

          END LOOP; 
          
          Close CFG;
          
          xxcp_dynamic_sql.Close_Session;
          -- #
          -- # Clear Common vars
          -- #
          xxcp_global.gCommon(1).Current_transaction_table := Null;
          xxcp_global.gCommon(1).Current_transaction_id    := Null;
          xxcp_global.gCommon(1).Current_parent_trx_id     := Null;
          xxcp_global.gCommon(1).Current_interface_ref     := Null;
          xxcp_global.Set_New_Trx_Flag('N');

          COMMIT; -- Final Configuration Commit.
        
          --
          IF xxcp_global.gCommon(1).Custom_events = 'Y' THEN
            xxcp_custom_events.After_assignment_processing(cSource_id            => vSource_id,
                                                           cSource_assignment_id => vSource_assignment_id);
          END IF;
        
          vTiming(vTimingCnt).CF     := xxcp_reporting.Get_Seconds_Diff(cSec1 => vTiming(vTimingCnt).CF_last, 
                                                                        cSec2 => xxcp_reporting.Get_Seconds);
          vTiming(vTimingCnt).CF_End_Date := Sysdate;
        
          -- ***********************************************************************************************************
          --
          --  Set associated Transactions to error if anyone of the Transactions is a set has errored in configuration
          --
          -- ***********************************************************************************************************
          --
          xxcp_foundation.show('Reporting Configuration errors....');
          xxcp_global.SystemDate := Sysdate;
          vInternalErrorCode := 0;
          
          Open ConfErr for
             ' Select Distinct VT_TRANSACTION_TABLE,VT_PARENT_TRX_ID
                  from '||gInterface_Table_Name ||' r
                 Where r.vt_request_id = :1
                   and r.vt_source_assignment_id   = :2
                   and r.vt_status = ''ERROR'''                   
                   using vRequest_id, vSource_Assignment_id;
             Loop

                  Fetch ConfErr into ConfErrRec;
                  Exit when ConfErr%notfound;

                  gDDL_Command := 
                     'UPDATE '||gInterface_Table_Name||' r
                       SET r.vt_status = ''ERROR'',
                           r.vt_internal_Error_code = 12824,
                           r.vt_date_processed = sysdate
                      WHERE r.vt_request_id = :1
                        AND r.vt_Source_Assignment_id = :2
                        AND r.vt_parent_trx_id        = :3
                        AND r.vt_transaction_table    = :4
                        AND r.vt_status IN (''ASSIGNMENT'', ''TRANSACTION'')';
                     
                  Begin  
                    
                     Execute Immediate gDDL_Command 
                       Using vRequest_id, vSource_Assignment_id,ConfErrRec.vt_parent_trx_id,ConfErrRec.vt_Transaction_table;

                      -- Flag Error
                      If SQL%ROWCOUNT > 0 then
                         gEngine_Error_Found := TRUE;  
                      End If;            

                      Exception When Others Then vInternalErrorCode := 12819; -- Update Failed.
                         xxcp_foundation.FndWriteError(150,'Control-7: SQLERRM='||SQLERRM, gDDL_Command);
                         Raise INVALID_CURSOR;       
                  End;
               END LOOP;
               Close ConfErr;

          --## ***********************************************************************************************************
          --##
          --##                                    Transaction Engine
          --##
          --## ***********************************************************************************************************
        
          vTiming(vTimingCnt).TE_Last    := xxcp_reporting.Get_Seconds;
          vTiming(vTimingCnt).TE_Records := 0;
          vTiming(vTimingCnt).TE_Start_Date := Sysdate;
        
          vExtraLoop             := FALSE;
          vExtraClass            := Null;
          xxcp_global.SystemDate := Sysdate;
          i := 0;
          
          IF vInternalErrorCode = 0 AND vJob_Status = 0 THEN
          
            xxcp_foundation.show('Transactions....');
            xxcp_wks.Reset_Source_Segments;
            
            gBalancing_Array       := xxcp_wks.Clear_Segments;
            ClearWorkingStorage;
            vCurrent_Parent_Trx_id := 0;
            -- 03.06.24
            xxcp_global.Clear_Parent_Trx_Arrays('T');
 
            -- !!
            -- !! TRANSACTION CURSOR 
            -- !!
            Open GLI for gDDL_Interface_Select
               Using 'TRANSACTION', vSource_id, vSource_Assignment_id, vRequest_id, cTable_Group_id;
             Loop

                  Fetch GLI into GLIRec;
                  Exit when GLI%notfound;
              
                  vInternalErrorCode := 0;
                  EXIT WHEN vJob_Status > 0;
                  vTiming(vTimingCnt).TE_Records := vTiming(vTimingCnt).TE_Records + 1;
                  
                  -- Dynamic Explosion at Transaction Level
                  xxcp_global.gCommon(1).Explosion_id     := glirec.Asg_Explosion_id;
                  xxcp_global.gCommon(1).Trx_Explosion_id := glirec.Trx_Explosion_id;

                  --
                  -- ***********************************************************************************************************
                  --        Set the balancing Setments to be written to the Column Rules Array
                  -- ***********************************************************************************************************
                  --
                  -- Poplulate the Balancing Array
                  xxcp_wks.Source_Balancing(1) := Null;
                  xxcp_wks.Source_Balancing(2) := GLIRec.be2; -- User_Je_Source_Name
                  xxcp_wks.Source_Balancing(3) := GLIRec.be3; -- Group Id
                  xxcp_wks.Source_Balancing(4) := GLIRec.be4; -- User Je Category Name
                  xxcp_wks.Source_Balancing(5) := Null;
                  xxcp_wks.Source_Balancing(6) := GLIRec.be6; -- Reference 1
                  xxcp_wks.Source_Balancing(7) := GLIRec.be7; -- Reference 2
                  xxcp_wks.Source_Balancing(8) := GLIRec.be8; -- Reference 3
                  xxcp_wks.Source_Balancing(9) := Null;
                  xxcp_wks.Source_Balancing(10):= Null;
                  
                  --
                  -- ***********************************************************************************************************
                  --         Move Accounting Codes from Master Record to Array
                  -- ***********************************************************************************************************
                  -- Populate Source Account Segments
                  xxcp_wks.Source_Segments(01) := GLIRec.Segment1;
                  xxcp_wks.Source_Segments(02) := GLIRec.Segment2;
                  xxcp_wks.Source_Segments(03) := GLIRec.Segment3;
                  xxcp_wks.Source_Segments(04) := GLIRec.Segment4;
                  xxcp_wks.Source_Segments(05) := GLIRec.Segment5;
                  xxcp_wks.Source_Segments(06) := GLIRec.Segment6;
                  xxcp_wks.Source_Segments(07) := GLIRec.Segment7;
                  xxcp_wks.Source_Segments(08) := GLIRec.Segment8;
                  xxcp_wks.Source_Segments(09) := GLIRec.Segment9;
                  xxcp_wks.Source_Segments(10) := GLIRec.Segment10;
                  xxcp_wks.Source_Segments(11) := GLIRec.Segment11;
                  xxcp_wks.Source_Segments(12) := GLIRec.Segment12;
                  xxcp_wks.Source_Segments(13) := GLIRec.Segment13;
                  xxcp_wks.Source_Segments(14) := GLIRec.Segment14;
                  xxcp_wks.Source_Segments(15) := GLIRec.Segment15;

                  xxcp_global.Set_New_Trx_Flag('N');
                  -- 03.06.24
                  xxcp_global.Set_Quantity_Flag('N');

                  --
                  -- ***********************************************************************************************************
                  --         When a New Transaction is detected then flush the current buffer
                  -- ***********************************************************************************************************
                  --
                  IF (vCurrent_Parent_Trx_id <> GLIRec.VT_Parent_Trx_id) THEN
                  
                    vTransaction_Error := FALSE;
                  
                    --
                    -- !! ******************************************************************************************************
                    --                             FLUSH TRANSACTION 
                    -- !! ******************************************************************************************************
                    --
                    IF vCurrent_Parent_Trx_id <> 0 THEN
                      CommitCnt := CommitCnt + 1; -- Number of records process since last commit.

                      k := FlushRecordsToTarget(cGlobal_Rounding_Tolerance => vGlobal_Rounding_Tolerance,
                                                cGlobalPrecision           => vGlobalPrecision,
                                                cTransaction_Type          => vTransaction_Type,
                                                cInternalErrorCode         => vInternalErrorCode);
                    END IF;
                   
                    xxcp_global.Set_New_Trx_Flag('Y');

                    --
                    -- ***********************************************************************************************************
                    --            Store Variables that are only set once per Parent Trx Id
                    -- ***********************************************************************************************************
                    --
                    xxcp_global.gCommon(1).Current_transaction_table := GLIRec.VT_Transaction_Table;
                    xxcp_global.gCommon(1).Current_parent_trx_id     := GLIRec.VT_Parent_Trx_id;
                    vCurrent_Parent_Trx_id := GLIRec.VT_Parent_Trx_id;
                    vTransaction_Type      := GLIRec.VT_Transaction_Type;
                    ClearWorkingStorage;
                    
                    -- 03.06.24
                    xxcp_global.Clear_Parent_Trx_Arrays('T');
                    --
                    -- ***********************************************************************************************************
                    --                         Extra Loops for different Classes
                    -- ***********************************************************************************************************
                    --                  
                    vExtraLoop  := FALSE;
                    vExtraClass := Null;
/*                    FOR j IN 1 .. xxcp_global.gSRE.COUNT LOOP

                      IF GLIRec.vt_Transaction_Table = xxcp_global.gSRE(j).Transaction_table THEN
                        vExtraClass := xxcp_global.gSRE(j).Extra_Record_type;
                        vExtraLoop  := TRUE;
                        EXIT;
                      END IF;
                    END LOOP;*/
                  
                  ELSE
                    vExtraLoop  := FALSE; -- !!!!
                    vExtraClass := Null;
                  END IF;
                
                  xxcp_global.gCommon(1).Current_transaction_id := GLIRec.vt_Transaction_id;
                  --
                  -- ***********************************************************************************************************
                  --                                   Store Working Variables
                  -- ***********************************************************************************************************
                  --
                  xxcp_global.gCommon(1).Current_source_rowid    := GLIRec.Source_Rowid;
                  xxcp_global.gCommon(1).Current_Interface_id    := GLIRec.VT_Interface_id;
                  xxcp_global.gCommon(1).Current_Accounting_date := GLIRec.Accounting_Date;
                  xxcp_global.gCommon(1).Current_Batch_number    := GLIRec.Group_id;
                  xxcp_global.gCommon(1).Current_Category_name   := GLIRec.User_JE_Category_Name;
                  xxcp_global.gCommon(1).Current_Source_name     := GLIRec.User_JE_Source_name;
                  xxcp_global.gCommon(1).Current_interface_ref   := GLIRec.VT_Transaction_ref; 
                  xxcp_global.gCommon(1).Current_creation_date   := GLIRec.Source_creation_date;
                  xxcp_global.gCommon(1).calc_legal_exch_rate    := GLIRec.Calc_legal_exch_rate;
                  
                  -- 03.06.24 Only populate invoice_parent_trx_id for PO Receipt Matching Source                  
                  if xxcp_global.gCommon(1).Source_id = 43 then 
                    xxcp_global.gInvoice_Matching_Common(1).invoice_parent_trx_id        := GLIRec.AUX_DATA1; 
                    xxcp_global.gInvoice_Matching_Common(1).invoice_source_assignment_id := GLIRec.AUX_DATA2; 
                    xxcp_global.gInvoice_Matching_Common(1).invoice_transaction_table    := GLIRec.AUX_DATA3;
                  end if;
                
                  -- ***********************************************************************************************************
                  --                         FETCH OE HEADER PARENT ROWID  
                  -- ***********************************************************************************************************
                  If gUse_OE_Headers = 'Y'  Then
                    vInternalErrorCode := '10721';
                    vParent_Rowid      := Null;
                    For HDRec in CHDR(pSource_Assignment_id  => vSource_Assignment_id,
                                      pTransaction_Table     => GLIRec.vt_Transaction_Table,
                                      pTransaction_Ref       => GLIRec.vt_Transaction_Ref,
                                      pOrder_Source_id       => GLIRec.Order_Source_id)
                    Loop
                      vParent_Rowid := HDRec.Parent_Rowid;
                      vInternalErrorCode := 0;
                    End Loop;
                  End If;        
                  
                  xxcp_global.gcommon(1).Current_Parent_Rowid := vParent_Rowid;
                           
                  --
                  -- ***********************************************************************************************************
                  --                                   Call the Transaction Engine
                  -- ***********************************************************************************************************
                  --
                  IF vTransaction_Error = FALSE THEN
                    vInternalErrorCode := 0; -- Reset for each row

                    i := i + 1; -- Count the number of rows processed.
                  
                    xxcp_wks.SOURCE_CNT  := xxcp_wks.SOURCE_CNT + 1;
                    xxcp_wks.SOURCE_ROWID(xxcp_wks.source_cnt)   := GLIRec.Source_Rowid;
                    xxcp_wks.SOURCE_SUB_CODE(xxcp_wks.source_cnt):= 0;
                    xxcp_wks.SOURCE_STATUS(xxcp_wks.source_cnt)  := '?';

                    -- *********************************************************************
                    -- Class Mapping 
                    -- *********************************************************************
                    IF GLIRec.Class_Mapping_Req = 'Y' THEN
                        vClass_Mapping_Name := xxcp_te_base.class_mapping(
                                                           cSource_id              => vSource_id,
                                                           cClass_Mapping_Required => GLIRec.Class_Mapping_Req,
                                                           cTransaction_Table      => GLIRec.VT_Transaction_table,
                                                           cTransaction_Type       => GLIRec.VT_Transaction_type,
                                                           cTransaction_Class      => GLIRec.VT_Transaction_class,
                                                           cTransaction_id         => GLIRec.Class_transaction_id,
                                                           cCode_Combination_id    => GLIRec.Code_Combination_id,
                                                           cAmount                 => GLIRec.Transaction_Cost,
                                                           cSystem_Link_id         => GLIRec.gl_sl_link_id);
                    END IF;                                                          
                                                           
                    -- *********************************************************************
                    -- End Inventory Logic
                    -- *********************************************************************

                    XXCP_TE_ENGINE.Control(cSource_assignment_id    => vSource_Assignment_id,
                                           cSource_Table_id         => GLIRec.Source_Table_id,
                                           cSource_Type_id          => GLIRec.Source_Type_id,
                                           cSource_Class_id         => GLIRec.Source_Class_id,
                                           cTransaction_id          => GLIRec.vt_Transaction_id,
                                           cParent_Trx_id           => GLIRec.vt_Parent_Trx_id,
                                           cSourceRowid             => GLIRec.Source_Rowid,
                                           cParent_Entered_Amount   => GLIRec.Parent_Entered_Amount,
                                           cParent_Entered_Currency => GLIRec.Parent_Entered_Currency,
                                           cClass_Mapping           => GLIRec.class_mapping_req,
                                           cClass_Mapping_Name      => vClass_Mapping_Name,
                                           cInterface_ID            => GLIRec.VT_Interface_id,
                                           cExtraLoop               => vExtraLoop,
                                           cExtraClass              => vExtraClass,
                                           cTransaction_set_id      => GLIRec.Transaction_Set_id,
                                           cTransaction_Table       => GLIRec.VT_Transaction_Table,
                                           cTransaction_Type        => GLIRec.VT_Transaction_Type,
                                           cTransaction_Class       => GLIRec.VT_Transaction_Class,
                                           cInternalErrorCode       => vInternalErrorCode);

                    xxcp_global.gCommon(1).Current_trading_set := Null;

                    -- Report Errors
                    IF vInternalErrorCode <> 0 THEN
                      Error_Whole_Transaction(cInternalErrorCode => vInternalErrorCode);
                      vTransaction_Error := TRUE;
                    END IF;

                  END IF;
                  --
                  -- ***********************************************************************************************************
                  --                     Test to See if Oracle or the User has terminated this process
                  --                                   And Commit after Each 1000 Records processed
                  -- ***********************************************************************************************************
                  --
                  IF CommitCnt >= gParent_Commit_Count THEN
                    CommitCnt := 0;
                    xxcp_global.SystemDate    := Sysdate;
                    COMMIT;

                    vJob_Status := 
                     xxcp_management.Has_Oracle_been_Terminated(cRequest_id      => vRequest_id,
                                                                cSource_Activity => vSource_activity,
                                                                cSource_Group_id => cSource_group_id,
                                                                cConc_Request_Id => cConc_request_id);
                                         
                    EXIT WHEN vJob_Status > 0; -- Controlled Exit
                  END IF;

            END LOOP;

            Close GLI;
            -- Pass Through (Always GL Interface)
            If vJob_Status = 0 and xxcp_te_base.GetPassThroughActive = 'Y' and xxcp_global.gCommon(1).Preview_ctl = 1 then
               Begin
           
                 XXCP_PROCESS_DML_GL.Process_Pass_Through(
                           cSource_table         => 'GL_INTERFACE'
                          ,cSource_assignment_id => vSource_Assignment_id
                          ,cSource_Instance_id   => vSource_Instance_id
                          ,cProcess_history_id   => 0 
                          ,cRequest_id           => xxcp_global.gCommon(1).Current_request_id
                          ,cErrorMessage         => vErrorMessage
                          ,cInternalErrorCode    => vInternalErrorCode);
                          
                 
                 If vInternalErrorCode = 0 then
                   
                   gDDL_Command := 
                     'Update '||gInterface_Table_Name||' r
                       set r.vt_status   = ''SUCCESSFUL'' 
                      where r.vt_Source_assignment_id = :1
                        and r.vt_request_id           = :2
                        and r.vt_source_assignment_id = :3
                        and r.vt_status      = ''PASSTHROUGH''
                        and r.vt_status_code between 7025 and 7028';
                    Begin
                      Execute Immediate gDDL_Command 
                       Using vSource_Assignment_id,xxcp_global.gCommon(1).Current_request_id,vSource_Assignment_id; 
                      
                      Exception When Others Then
                         xxcp_foundation.FndWriteError(150,'Control-8: SQLERRM='||SQLERRM, gDDL_Command);
                         Raise INVALID_CURSOR;       
                    End;
                      
                 Else
                   
                   gDDL_Command := 
                     'Update '||gInterface_Table_Name||' r
                       set r.vt_status  = ''ERROR''
                          ,r.vt_internal_Error_code = 12827
                      where r.vt_Source_assignment_id = :1
                        and r.vt_request_id           = :2
                        and r.vt_source_assignment_id = :3
                        and r.vt_status      = ''PASSTHROUGH''
                        and r.vt_status_code between 7025 and 7028';
                    Begin 
                      Execute Immediate gDDL_Command 
                        Using vSource_Assignment_id,xxcp_global.gCommon(1).Current_request_id,vSource_Assignment_id; 
                        
                      Exception When Others Then
                         xxcp_foundation.FndWriteError(150,'Control-9: SQLERRM='||SQLERRM, gDDL_Command);
                         Raise INVALID_CURSOR;       
                    End;
                 End If;     
                 
                 Commit;
                 
                 Exception When Others Then 
                     vInternalErrorCode := 12827;
                     xxcp_foundation.FndWriteError(vInternalErrorCode,'Pass Through Insert Failed');
                 
               End;
            End If;           

            xxcp_te_base.Close_Cursors;

            --
            --  !!***********************************************************************************************************
            --                     Flush the Buffer for a Final Time
            -- !! ***********************************************************************************************************
            --
            IF vCurrent_Parent_Trx_id <> 0 AND vInternalErrorCode = 0 THEN
              IF vInternalErrorCode = 0 THEN

                k := FlushRecordsToTarget(cGlobal_Rounding_Tolerance => vGlobal_Rounding_Tolerance,
                                          cGlobalPrecision           => vGlobalPrecision,
                                          cTransaction_Type          => vTransaction_type,
                                          cInternalErrorCode         => vInternalErrorCode);
              END IF;
            END IF;
          END IF; -- Internal Error Check GLI
        END IF; -- End Internal Error Check

        vTiming(vTimingCnt).TE := xxcp_reporting.Get_Seconds_Diff(cSec1 => vTiming(vTimingCnt).TE_last, cSec2 => xxcp_reporting.Get_Seconds);

        xxcp_foundation.show('Clear down...');
        xxcp_global.SystemDate := Sysdate;
        -- *******************************************************************
        -- Error out records remaining with a Processing status after the run
        -- *******************************************************************
        BEGIN
        
          gDDL_Command :=                     
           'UPDATE '||gInterface_Table_Name||' r 
             SET r.vt_status = ''ERROR'',
                 r.vt_internal_error_code = DECODE(vt_status,''TRANSACTION'',12999,''ASSIGNMENT'',12998,''GROUPING'',12996),
                 r.vt_date_processed = sysdate
             WHERE r.ROWID = ANY (SELECT g.ROWID Source_Rowid
                                    FROM '||gInterface_Table_Name||' g
                                   WHERE g.vt_request_id           = :1
                                     AND g.vt_source_assignment_id = :2
                                     AND g.vt_status IN (''ASSIGNMENT'', ''TRANSACTION'', ''GROUPING''))';

           
            Execute Immediate gDDL_Command Using xxcp_global.gCommon(1).Current_request_id,vSource_Assignment_id;

           -- Flag Error
           If SQL%ROWCOUNT > 0 then
             gEngine_Error_Found := TRUE;  
           End If;            

           Exception When Others Then 
              xxcp_foundation.FndWriteError(150,'Control-10: SQLERRM='||SQLERRM, gDDL_Command);
              Raise INVALID_CURSOR;       
        END;

        -- #
        -- # Clear Common vars
        -- #
        xxcp_global.gCommon(1).Current_transaction_table := Null;
        xxcp_global.gCommon(1).Current_transaction_id    := Null;
        xxcp_global.gCommon(1).Current_parent_trx_id     := Null;
        xxcp_global.gCommon(1).Current_interface_ref     := Null; 
        --
        -- ***********************************************************************************************************
        --                     Clear Down
        -- ***********************************************************************************************************
        --
        ClearWorkingStorage;
        --
        -- ***********************************************************************************************************
        --                     DeActive the Lock in XXCP_ACTIVITY_CONTROL
        -- ***********************************************************************************************************
        --
        COMMIT;
        vTiming(vTimingCnt).TE_End_Date := Sysdate;
        vTimingCnt := vTimingCnt + 1;
      END LOOP; -- SR1

      -- Error Completion Status
      If gEngine_Error_Found and vJob_Status = 0 then
        If vError_Completion_Status = 'W' then
          gForce_Job_Warning := TRUE;
        Elsif vError_Completion_Status = 'E' then
          gForce_Job_Error := TRUE;
        End if;
      End If;
      
      -- Distributer
      IF vJob_Status = 0 and not gForce_Job_Error THEN  -- Normal or Warning. Dont Distribute if Error
         vJob_Status := xxcp_data_distributor.Control(
                                cSource_id        => vSource_id,
                                cSource_Group_id  => cSource_Group_id,
                                cRequest_id       => Null, -- Leave as NULL 
                                cJournal_Group_id => xxcp_global.gCommon(1).Current_trx_group_id);
      END IF;     

      -- Export File         
      -- 02.06.09 Write record out to file only if Flat file is enabled. 
      IF vJob_Status = 0 and xxcp_global.gCommon(1).flat_file = 'Y' and not gForce_Job_Error THEN

        vJob_Status := xxcp_file_upload.exportfile(cSource_id  => vSource_id,
                                                   cDebug      => gEngine_Trace,
                                                   cRequest_id => vRequest_id);

      END IF; 
      
      IF xxcp_global.gCommon(1).Custom_events = 'Y' THEN
        
        xxcp_custom_events.After_Processing(cSource_id       => vSource_id,
                                            cSource_Group_id => cSource_Group_id,
                                            cRequest_id      => vRequest_id,
                                            cJob_Status      => vJob_Status);
      END IF;


      xxcp_management.DeActivate_lock_process(cRequest_id      => vRequest_id,
                                              cSource_Activity => vSource_Activity,
                                              cSource_Group_id => cSource_Group_id
                                              );
 
      xxcp_te_base.ClearDown;
      xxcp_memory_pack.ClearDown;
      xxcp_dynamic_sql.Close_Session;

      COMMIT;
      
    ELSIF NVL(vJob_Status,0) = 0 THEN
      --
      -- Report If this process was already in use
      --
      xxcp_foundation.show('Engine already in use');
      vJob_Status := 1;
    END IF;

    -- Force Warning/Error
    If gForce_Job_Warning then
      vJob_Status := 15;
    Elsif gForce_Job_Error then
      vJob_Status := 16;
    End if;

    xxcp_te_base.Write_Timings(vTiming, vTiming_Start);
    --
    -- ************************************************************************************************
    --                     Email Job Details to Users
    -- ************************************************************************************************
    --
    xxcp_comms.Send_job_details(cRequest_id => xxcp_global.gCommon(1).Current_Request_id,
                                cJob_Status => vJob_Status);
   --
    -- ************************************************************************************************
    --                     Show Time Statistics when called from SQL*Plus
    -- ************************************************************************************************
    --
    xxcp_global.gCommon(1).Current_Request_id := Null;
    --
    -- ***********************************************************************************************************
    --                     Return Job Status Code and Finish!!
    -- ***********************************************************************************************************
    --

    RETURN(vJob_Status);

  END Control;


END XXCP_GENERIC_ENGINE;
/
