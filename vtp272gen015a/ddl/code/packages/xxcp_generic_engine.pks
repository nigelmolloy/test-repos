CREATE OR REPLACE PACKAGE XXCP_GENERIC_ENGINE AS
  /******************************************************************************
                      V I R T U A L  T R A D E R  L I M I T E D
        (Copyright 2001-2012 Virtual Trader Ltd. All rights Reserved)

     NAME:       XXCP_GENERIC_ENGINE
     PURPOSE:    Generic Engine
     
     Version [03.06.27] Build Date [30-OCT-2012] Name [XXCP_GENERIC_ENGINE]

  ******************************************************************************/

  FUNCTION Software_Version RETURN VARCHAR2;

  FUNCTION Control(cSource_Group_id    IN NUMBER,
                   cConc_Request_Id    IN NUMBER,
                   cTable_Group_id     IN NUMBER   DEFAULT 0) RETURN NUMBER;
                   
END XXCP_GENERIC_ENGINE;
/
