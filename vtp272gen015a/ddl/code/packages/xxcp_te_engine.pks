CREATE OR REPLACE PACKAGE XXCP_TE_ENGINE AS
  /******************************************************************************
                          V I R T A L  T R A D E R  L I M I T E D
             (Copyright 2002-2012 Virtual Trader Ltd. All rights Reserved) 

     NAME:       XXCP_TE_ENGINE
     PURPOSE:    The Main Transaction Engine process.
     
     Version [03.06.28] Build Date [19-DEC-2012] Name [XXCP_TE_ENGINE]

  ******************************************************************************/

  FUNCTION Software_Version RETURN VARCHAR2;
  
  FUNCTION Current_Start_of_Year_id RETURN NUMBER;

  FUNCTION Check_combination_id(cChart_of_Accounts_id IN NUMBER, cMax_Segments IN NUMBER, cAccount_Segments IN xxcp_dynamic_array, cGl_Date IN DATE, cInstance_id IN NUMBER) RETURN NUMBER;

  PROCEDURE Control(cSource_assignment_id    IN NUMBER,
                    cSource_Table_id         IN NUMBER,
                    cSource_Type_id          IN NUMBER,
                    cSource_Class_id         IN VARCHAR2,
                    cTransaction_id          IN NUMBER,
                    cParent_Trx_id           IN NUMBER,
                    cSourceRowid             IN ROWID,
                    cParent_Entered_Amount   IN NUMBER,
                    cParent_Entered_Currency IN VARCHAR2,
                    cClass_Mapping           IN VARCHAR2,
                    cClass_Mapping_Name      IN VARCHAR2,
                    cInterface_ID            IN NUMBER,
                    cExtraLoop               IN BOOLEAN,
                    cExtraClass              IN VARCHAR2,
                    cTransaction_set_id      IN NUMBER,
                    --
                    cTransaction_Table       IN VARCHAR2,
                    cTransaction_Type        IN VARCHAR2,     
                    cTransaction_Class       IN VARCHAR2,           
                    cInternalErrorCode       OUT NOCOPY NUMBER);

END XXCP_TE_ENGINE;
/
