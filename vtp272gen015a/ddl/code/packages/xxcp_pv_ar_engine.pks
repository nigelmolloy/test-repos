CREATE OR REPLACE PACKAGE XXCP_PV_AR_ENGINE AS
/******************************************************************************
                    V I R T A L  T R A D E R  L I M I T E D
		  (Copyright 2005-2012 Virtual Trader Ltd. All rights Reserved)

   NAME:       XXCP_PV_AR_ENGINE
   PURPOSE:    AR Engine in Preview Mode

   Version [03.06.06] Build Date [21-NOV-2012] Name [XXCP_PV_AR_ENGINE]
 
******************************************************************************/

  Function Software_Version RETURN VARCHAR2;

  Function Control(cSource_Group_id         IN NUMBER,
	                 cSource_Assignment_id    IN NUMBER,
                   cPreview_id              IN NUMBER,
                   cParent_trx_id           IN NUMBER   DEFAULT 0,
                   cUser_id                 IN NUMBER,
                   cLogin_id                IN NUMBER,
									 cPV_Zero_Flag            IN VARCHAR2,
									 cSource_Rowid            IN Rowid default null) RETURN NUMBER;

END XXCP_PV_AR_ENGINE;
/
