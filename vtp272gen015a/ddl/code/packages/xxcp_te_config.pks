CREATE OR REPLACE PACKAGE XXCP_TE_CONFIGURATOR AS
  /******************************************************************************
                          V I R T A L  T R A D E R  L I M I T E D
       (Copyright 2005-2012 Virtual Trader Ltd. All rights Reserved)

     NAME:       XXCP_TE_CONFIGURATOR
     PURPOSE:    The Main Configuration process.

     Version [03.06.33] Build Date [15-OCT-2012] Name [XXCP_TE_CONFIGURATOR]

  ******************************************************************************/

  OutboundTab xxcp_global.gOutBoundTab%type;

  FUNCTION Software_Version RETURN VARCHAR2;

  PROCEDURE Set_SAE_Inbound(cInboundRec in xxcp_vt_interface%rowtype);

  PROCEDURE FindAdjustmentRates(cRate_Set_id        IN NUMBER,
                                cQualifier1         IN VARCHAR2,
                                cQualifier2         IN VARCHAR2,
                                cQualifier3         IN VARCHAR2,
                                cQualifier4         IN VARCHAR2,
                                cTransaction_Date   IN DATE,
                                cAdjustment_Rate_id OUT NUMBER,
                                cAdjustment_Rate    OUT VARCHAR2,
                                cInternalErrorCode  IN OUT NUMBER);

  PROCEDURE Get_Last_IC_Values(cLastICPrice    OUT NUMBER,
                               cLastICCurrency OUT VARCHAR2,
                               cLastICPMID     OUT NUMBER,
                               cLastDftPMused  OUT VARCHAR2
                               );

  PROCEDURE Clear_Last_IC_Values;

  PROCEDURE Main_Control(
                    cSource_Assignment_ID   IN NUMBER,
                    cSet_of_books_id        IN NUMBER,
										cTransaction_Date       IN DATE,
										cSource_id              IN NUMBER,
                    cSource_Table_id        IN NUMBER,
                    cSource_Type_id         IN NUMBER,
                    cSource_Class_id        IN NUMBER,
                    cTransaction_id         IN NUMBER,
                    cSourceRowid            IN ROWID,
                    cParent_Trx_id          IN NUMBER,
                    cTransaction_Set_id     IN NUMBER,
                    cSpecial_Array          IN VARCHAR2,
                    cKey                    IN VARCHAR2,
										--
										cTransaction_Table      IN VARCHAR2,
										cTransaction_Type       IN VARCHAR2,
										cTransaction_Class      IN VARCHAR2,
										--
                    cOutboundTab           OUT OutboundTab%type,
                    cInternalErrorCode      IN OUT NUMBER);

  PROCEDURE Control(cSource_Assignment_ID   IN NUMBER,
                    cSet_of_books_id        IN NUMBER,
										cTransaction_Date       IN DATE,
										cSource_id              IN NUMBER,
                    cSource_Table_id        IN NUMBER,
                    cSource_Type_id         IN NUMBER,
                    cSource_Class_id        IN NUMBER,
                    cTransaction_id         IN NUMBER,
                    cSourceRowid            IN ROWID,
                    cParent_Trx_id          IN NUMBER,
                    cTransaction_Set_id     IN NUMBER,
                    cSpecial_Array          IN VARCHAR2,
                    cKey                    IN VARCHAR2,
										--
										cTransaction_Table      IN VARCHAR2,
										cTransaction_Type       IN VARCHAR2,
										cTransaction_Class      IN VARCHAR2,
										--
                    cInternalErrorCode      IN OUT NUMBER);
  -- 02.05.08
  PROCEDURE reset_parent_trx_id;

END XXCP_TE_CONFIGURATOR;
/
