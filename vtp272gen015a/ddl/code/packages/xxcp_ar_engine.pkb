CREATE OR REPLACE PACKAGE BODY XXCP_AR_ENGINE AS
/******************************************************************************
                        V I R T A L  T R A D E R  L I M I T E D
            (Copyright 2005-2012 Virtual Trader Ltd. All rights Reserved)

   NAME:       XXCP_AR_ENGINE
   PURPOSE:    AR Engine

   REVISIONS:
   Ver        Date      Author  Description
   ---------  --------  ------- ------------------------------------
   02.01.00   28/06/02  Keith   First Coding
   02.01.01   29/11/03  Keith   Removed need for Temporary Tables
   02.01.02   03/12/03  Keith   ReStructured for VT 2.1
   02.01.03   18/05/04  Keith   Transaction Group Stamp
   02.01.04   20/08/04  Keith   Moved Dynamic Attribute numbers
   02.01.05   24/09/04  Keith   Set parent_trx_id if not Openuptime
   02.02.00   01/02/05  Keith   Timing Logic added to help develope quick transactions
   02.02.01   21/02/05  Keith   Changed Error Checking logic
   02.02.02   27/12/05  Keith   Exit and Rollback if out of disk space (3550)
   02.03.01   08/02/06  Keith   Moved .init upwards
   02.03.02   22/04/06  Keith   Changing timing report to work per source assignment,
                                not just at group level.
   02.03.03   25/08/06  Keith   Only put header on records with an amount
   02.03.04   11/09/06  Keith   Added Transaction Date to configuration
   02.03.05   14/09/06  Keith   Changed vExtra Loop count logic
   02.04.06   03/01/07  Keith   Added Table Group to Stamp logic
   02.04.07   17/01/07  Keith   Added History output logic
   02.04.08   10/05/07  Keith   Added vRequest_id to after_processing
   02.04.09   25/06/07  Keith   Removed Zero_Amount_Trx
   02.04.10   16/07/07  Keith   Improved Error message around Sabrix Tax
   02.04.11   07/07/08  Keith   xxcp_global.gcommon(1).current_creation_date
   02.06.01   19/10/09  Simon   Grouping now done by transaction_table not assignment_id..
   02.06.02   28/12/09  Keith   Added calc_legal_exch_rate
   02.06.03   02/11/10  Nigel   Fixed issue with get_grouping_rule_id custom event.
   02.06.04   04/11/10  Nigel   Fixed issue with Transaction_Group_Stamp setting error incorrectly.
   02.06.05   04/07/11  Keith   Added Pass Through at Assignment Level
   03.06.06   13/03/12  Simon   Add facility to optionally end in warning/error (OOD-33). 
   03.06.07   19/03/12  Mat     Pass_Through_Whole_Trx - replaced ref to XXCP_ra_interface_lines with xxcp_gl_interface (OOD-61)
   03.06.08   24/03/12  Keith   Added Exclude Rules
   03.06.09   05/07/12  Simon   gTrans_Table_Array moved to xxcp_global package.
   03.06.10   21/11/12  Keith   removed reference to xxcp_wks.gTaxCodes
***************************************************************************************/


  gEngine_Trace    varchar2(1);

  vTiming          xxcp_global.gTiming_rec;
  vTimingCnt       pls_integer;

  gBalancing_Array XXCP_DYNAMIC_ARRAY := XXCP_DYNAMIC_ARRAY(' ',' ',' ',' ',' ',' ',' ',' ',' ',' ');


  TYPE gCursors_Rec is RECORD(
    v_cursorId     pls_Integer := 0,
    v_Target_Table XXCP_column_rules.transaction_table%type,
    v_usage        varchar2(1),
    v_OpenCur      varchar2(1) := 'N',
    v_LastCur      pls_integer := -1);

  TYPE gCUR_REC is TABLE of gCursors_Rec INDEX by BINARY_INTEGER;
  gCursors gCUR_REC;

  gEngine_Error_Found         boolean := FALSE;
  gForce_Job_Error            boolean := FALSE;
  gForce_Job_Warning          boolean := FALSE;

  -- *************************************************************
  --
  --                     Software Version Control
  --
  -- This package can be checked with SQL to ensure it's installed
  -- select packagename.software_version from dual;
  -- *************************************************************
  Function Software_Version RETURN VARCHAR2 IS
  BEGIN
    RETURN('Version [03.06.09] Build Date [05-JUL-2012] Name [XXCP_AR_ENGINE]');
  END Software_Version;


  -- !! ***********************************************************************************************************
  -- !!
  -- !!                                             Open_Cursors
  -- !!
  -- !! ***********************************************************************************************************
  Function Open_Cursor(cUsage in varchar2, cTarget_Table in varchar2) Return pls_integer is

    i    pls_integer;
    vPos pls_integer := 0;

  Begin
    For i in 1 .. gCursors.count loop
      If (gCursors(i).v_Target_Table = cTarget_Table) AND (gCursors(i).v_usage = cUsage) then
        vPos := i;
        If gCursors(i).v_OpenCur = 'N' then
          gCursors(i).v_cursorId := DBMS_SQL.OPEN_CURSOR;
          gCursors(i).v_OpenCur := 'Y';
          gCursors(i).v_LastCur := -1;
          exit;
        End If;
      End If;
    End Loop;

    Return(vPos);
  End Open_Cursor;

  -- *************************************************************
  --
  --                  ClearWorkingStorage
  --
  -- *************************************************************
  Procedure ClearWorkingStorage IS

   j pls_integer;

  begin
    -- Processed Records
    xxcp_wks.WORKING_CNT := 0;
    xxcp_wks.WORKING_RCD.Delete;

    -- Current Records before processing
    xxcp_wks.SOURCE_CNT := 0;
    XXCP_WKS.SOURCE_ROWID.Delete;
    XXCP_WKS.SOURCE_STATUS.Delete;
    XXCP_WKS.SOURCE_SUB_CODE.Delete;
    -- Balancing
    xxcp_wks.BALANCING_CNT := 0;
    XXCP_WKS.BALANCING_RCD.Delete;

  End ClearWorkingStorage;

 --  !! ***********************************************************************************************************
 --  !!   Stop_Whole_Transaction
 --  !! ***********************************************************************************************************
 Procedure Stop_Whole_Transaction is

 vVT_Source_Rowid Rowid;

 Begin
   -- Set all transactions in group to error status
   Begin
    Update XXCP_ra_interface_lines
      set vt_status              = 'WAITING'
         ,vt_internal_Error_code = 0 -- Associated errors
         ,VT_DATE_PROCESSED  = xxcp_global.SystemDate
       WHERE vt_request_id = xxcp_global.gCommon(1).Current_request_id
         AND vt_Source_Assignment_id = xxcp_global.gCommon(1).current_assignment_id
         AND vt_parent_trx_id = xxcp_global.gCommon(1).current_parent_Trx_id
         AND NOT vt_status IN ('TRASH', 'IGNORED');
   End;
   -- Update Record that actually caused the problem.
   Begin
     Update XXCP_ra_interface_lines
        set vt_status          = 'WAITING'
           ,VT_DATE_PROCESSED  = xxcp_global.SystemDate
       WHERE ROWID = xxcp_global.gCommon(1).current_source_rowid
         AND vt_request_id = xxcp_global.gCommon(1).current_request_id;
   End;

   ClearWorkingStorage;

End Stop_Whole_Transaction;

  --  !! ***********************************************************************************************************
  --  !!
  --  !!                                   Error_Whole_Gl_Transaction
  --  !!                If one part of the wrapper finds a problem whilst processing a record,
  --  !!                          then we need to error the whole transaction.
  --  !!
  --  !! ***********************************************************************************************************
  Procedure Error_Whole_Transaction(cInternalErrorCode in out Number) IS
  BEGIN

    -- Flag Error
    gEngine_Error_Found := TRUE;  

    -- Set all transactions in group to error status
    Begin
      Update XXCP_ra_interface_lines
         set vt_status = 'ERROR', vt_internal_Error_code = 12823 -- Associated errors
       where vt_request_id = xxcp_global.gCommon(1).Current_request_id
         and vt_Source_Assignment_id = xxcp_global.gCommon(1).current_assignment_id
         and vt_parent_trx_id = xxcp_global.gCommon(1).current_parent_Trx_id
         and NOT vt_status in ('TRASH', 'IGNORED');
    End;
    -- Update Record that actually caused the problem.
    Begin
      update XXCP_ra_interface_lines
         set vt_status              = 'ERROR',
             vt_internal_error_code = cInternalErrorCode,
             vt_date_processed      = xxcp_global.SystemDate
       where rowid = xxcp_global.gCommon(1).current_source_rowid
         and vt_request_id = xxcp_global.gCommon(1).current_request_id;
    End;

    If xxcp_global.gCommon(1).Custom_events = 'Y' then

      xxcp_custom_events.ON_TRANSACTION_ERROR(xxcp_global.gCommon(1).Source_id,
                                              xxcp_global.gCommon(1).current_assignment_id,
                                              xxcp_global.gCommon(1).current_source_rowid,
                                              xxcp_global.gCommon(1).current_Transaction_Table,
                                              xxcp_global.gCommon(1).current_Parent_Trx_id,
                                              xxcp_global.gCommon(1).current_transaction_id,
                                              cInternalErrorCode);
    End If;

    ClearWorkingStorage;

  END Error_Whole_Transaction;

  --
  --   !! ***********************************************************************************************************
  --   !!
  --   !!                                     No_Action_GL_Transaction
  --   !!                                If there are no records to process.
  --   !!
  --   !! ***********************************************************************************************************
  --
  Procedure No_Action_GL_Transaction(cSource_Rowid in rowid) IS
  BEGIN
    -- Update Record that actually had not action required
    Begin
      update XXCP_ra_interface_lines
         set vt_status              = 'SUCCESSFUL',
             vt_internal_error_code = Null,
             vt_date_processed      = xxcp_global.SystemDate,
             vt_status_code         = 7003
       where rowid = csource_rowid;
    End;
  END NO_Action_GL_Transaction;

 --   !! ***********************************************************************************************************
 --   !!
 --   !!                                     Pass_Through_Whole_Trx
 --   !!                                If there are no records to process.
 --   !!
 --   !! ***********************************************************************************************************
 Procedure Pass_Through_Whole_Trx(cSource_Rowid in Rowid, cPassThroughCode in number) is

  vStatus XXCP_ra_interface_lines.vt_status%type;
 
 Begin
 
   If cPassThroughCode between 7021 AND 7024 then
	   vStatus := 'IGNORED';
	 Else
	   vStatus := 'PASSTHROUGH';
	 End If;
 
   If cPassThroughCode in (7022, 7026) then -- Parent level   
     xxcp_global.gPassThroughCode := cPassThroughCode;
	 End if;
      
   Update XXCP_ra_interface_lines p
      set p.vt_status               = vStatus
         ,p.vt_internal_Error_code  = Null
         ,p.vt_status_code          = cPassThroughCode
         ,p.vt_date_processed       = xxcp_global.SystemDate
    where p.rowid = cSource_Rowid
      and p.vt_request_id           = xxcp_global.gCommon(1).Current_request_id;
	 
 End Pass_Through_Whole_Trx;
  


  --  !! ***********************************************************************************************************
  --  !!
  --  !!                                   Transaction_Group_Stamp
  --  !!                 Stamp Incomming record with parent Trx to create groups of
  --  ||                    transactions that resemble on document transaction.
  --  !!
  --  !! ***********************************************************************************************************
  Procedure Transaction_Group_Stamp(cStamp_Parent_Trx     in varchar2,
                                    cSource_assignment_id in number,
                                    cTable_Group_id       in number,
                                    cInternalErrorCode    out number) IS


    vInternalErrorCode number(8) := 0;
    vStatement         varchar2(6000);

    -- Distinct list of Transaction Tables
    -- populated in Set_Running_Status
    -- 02.06.03 Changed to look at all the distinct grouping rules in the interface table.
    CURSOR cTrxTbl is
      select distinct vt_grouping_rule_id Grouping_Rule_Id
      from   XXCP_ra_interface_lines g
      where  g.vt_source_assignment_id = cSource_assignment_id
      and    g.vt_status               = 'GROUPING';

    Cursor c1(pSource_assignment_id in number,
              pTable_Group_id       in number) IS
      select distinct 'update XXCP_ra_interface_lines ' ||
                      '  set vt_parent_trx_id = ' || t.Parent_trx_column || ' ' ||
                      'where vt_status = :1 ' ||
                      '  and vt_source_assignment_id = :2 ' ||
                      '  and vt_transaction_table = :3 ' ||
                      '  and vt_request_id = :4 ' Statement_line,
                      t.Parent_trx_column,
                      t.Transaction_Table
        from XXCP_ra_interface_lines       g,
             XXCP_sys_source_tables  t,
             XXCP_source_assignments x
       where g.vt_request_id           = xxcp_global.gCommon(1).Current_request_id
         and g.vt_status               = 'GROUPING'
         and g.vt_transaction_Table    = t.Transaction_Table
         and t.table_group_id          = pTable_Group_id
         and t.source_id               = x.Source_id
         and x.source_assignment_id    = pSource_assignment_id
         and g.vt_source_assignment_id = x.source_assignment_id;

    -- 02.06.04 Now passing in Grouping_Rule_Id
    Cursor er1(pSource_assignment_id in number, cGrouping_rule_id in number) IS
      select g.vt_interface_id,
             g.rowid source_rowid,
             g.vt_transaction_table
        from XXCP_ra_interface_lines g
       where g.vt_request_id           = xxcp_global.gCommon(1).Current_request_id
         and g.vt_status               = 'GROUPING'
         and g.vt_source_assignment_id = pSource_Assignment_id
         and g.vt_parent_trx_id is null
         and g.vt_grouping_rule_id = cGrouping_rule_id;

  BEGIN

    vTiming(vTimingCnt).Group_Stamp_Start := to_char(sysdate, 'HH24:MI:SS');

    For cTblRec in cTrxTbl Loop  -- Loop for all grouping rules within this set of interface records.
      If cTblRec.Grouping_Rule_Id > 0 THEN  --02.06.01
       XXCP_DYNAMIC_SQL.Apply_Grouping_Rules(cNew_Status        => 'GROUPING',
                                              cInternalErrorCode => vInternalErrorCode, -- 02.06.03
                                              cGrouping_rule_id  => cTblRec.Grouping_Rule_Id);
                                              
        -- Error Checking for Null Parent Trx id
        -- 02.06.04 Now passing in Grouping_Rule_Id
        For er1Rec in er1(cSource_assignment_id,cTblRec.Grouping_Rule_Id) LOOP

          update XXCP_ra_interface_lines l
             set vt_status                = 'ERROR',
                 l.vt_internal_error_code = 11090
           where l.vt_request_id = xxcp_global.gCommon(1).Current_request_id
             and l.vt_transaction_table = er1rec.vt_transaction_table
             and l.vt_grouping_rule_id = cTblRec.Grouping_Rule_Id;

        End Loop;

      Else
        -- Mass update of Parent Trx id
        For rec in c1(cSource_Assignment_id,
                      cTable_Group_id) LOOP
          If rec.Parent_trx_column is null then
            vInternalErrorCode := 10665;
            xxcp_foundation.FndWriteError(vInternalErrorCode,'Transaction Table <' || Rec.Transaction_Table || '>');
          Else

            vStatement := rec.Statement_line;

            Begin

              Execute Immediate vStatement
                Using 'GROUPING', cSource_Assignment_id, rec.transaction_table, xxcp_global.gCommon(1).current_request_id;

              Exception
                when OTHERS then
                  vInternalErrorCode := 10667;
                  xxcp_foundation.FndWriteError(vInternalErrorCode, SQLERRM,vStatement);
            End;
          End If;
        End loop;
      End If;
    End Loop;

    cInternalErrorCode := vInternalErrorCode;

  END Transaction_Group_Stamp;



  -- !! ***********************************************************************************************************
  -- !!
  -- !!                                   FlushRecordsToTarget
  -- !!     Control The process of moving the Array elements throught to creating the new output records
  -- !!
  -- !! ***********************************************************************************************************
  Function FlushRecordsToTarget(cGlobal_Rounding_Tolerance in number,
                                cGlobalPrecision           in number,
                                cTransaction_Type          in varchar2,
                                cInternalErrorCode         in out number)
    return number is

    vCnt           pls_integer := 0;
    c              pls_integer := 0;

    vEntered_DR    Number := 0;
    vEntered_CR    Number := 0;
    j              pls_integer := 0;
    fe             pls_integer := xxcp_wks.WORKING_CNT;

    vRC            pls_integer := xxcp_wks.SOURCE_CNT;
    vRC_Records    pls_integer := 0;
    vRC_Error      pls_integer := 0;

    vStatus XXCP_ra_interface_lines.vt_status%type;

    vRecord_type       varchar2(4);

    vD1005_Array       xxcp_dynamic_array;
    vCustom_Attributes xxcp_dynamic_array;
    vNewTransaction    varchar2(1) := 'Y';
    vErrorMessage      varchar2(3000);

    UDI_Cnt pls_integer := 0;

    vZero_Action pls_integer := 0;

  BEGIN

    -- Rounding is Optional in AR
     If fe > 0  then
      xxcp_te_base.Account_Rounding(cGlobal_Rounding_Tolerance,
                                    cGlobalPrecision,
                                    cTransaction_Type,
                                    vEntered_DR,
                                    vEntered_CR,
                                    cInternalErrorCode);
    End If;

    -- Check that for each attribute record we have a records for output.
    For c in 1 .. vRC Loop
      vRC_Error := 0;
      FOR j in 1 .. fe LOOP
        If xxcp_wks.Source_rowid(c) = xxcp_wks.WORKING_RCD(j).Source_rowid then
          xxcp_wks.WORKING_RCD(j).Source_Pos := c;
          xxcp_wks.SOURCE_STATUS(c) := 'FLUSH';
          vRC_Error   := 1;
          vRC_Records := vRC_Records + 1;
        end if;
      End loop;
      IF vRC_Error = 0 THEN
        xxcp_wks.SOURCE_SUB_CODE(c) := 7003;
        xxcp_wks.SOURCE_STATUS(c) := 'NO RULES';
        No_Action_GL_Transaction(xxcp_wks.Source_rowid(c));
      END IF;
    END LOOP;

    Savepoint TARGET_INSERT;

    If cInternalErrorCode = 0 and vRC_Records > 0 then


      For j in 1 .. fe Loop

        Exit when cInternalErrorCode <> 0;

        vCnt := vCnt + 1;

        -- Custom Events
        If xxcp_global.gCommon(1).Custom_events = 'Y' then
           xxcp_te_base.Custom_Event_Insert_Row(j,cInternalErrorCode);
        End If;

        -- Tag Zero Account Value rows

        If cInternalErrorCode = 0 then

          xxcp_global.gCommon(1).current_source_rowid := xxcp_wks.Source_rowid(xxcp_wks.WORKING_RCD(j).Source_Pos);

           -- Find the Action to take for Zero Accounted values
          vZero_Action := xxcp_te_base.Zero_Suppression_Rule(j);

          IF vZero_Action > 0 THEN
                Select xxcp_process_history_seq.nextval into xxcp_wks.WORKING_RCD(j).Process_History_id from dual;
                 -- don't post ZAV

                  If vZero_Action = 3 then
                      XXCP_PROCESS_DML_AR.Process_Oracle_Insert(
                                                      cSource_Rowid        => xxcp_wks.WORKING_RCD(j).Source_rowid,
                                                      cParent_Rowid        => Null,
                                                      cTarget_Table        => xxcp_wks.WORKING_RCD(j).Target_Table,
                                                      cTarget_Instance_id  => xxcp_wks.WORKING_RCD(j).Target_Instance_id,
                                                      cProcess_History_id  => xxcp_wks.WORKING_RCD(j).Process_History_id,
                                                      cRule_id             => xxcp_wks.WORKING_RCD(j).Rule_id,
                                                      cD1001_Array         => xxcp_wks.WORKING_RCD(j).D1001_Array,
                                                      cD1002_Array         => xxcp_wks.WORKING_RCD(j).D1002_Array,
                                                      cD1003_Array         => xxcp_wks.WORKING_RCD(j).D1003_Array,
                                                      cD1004_Array         => xxcp_wks.WORKING_RCD(j).D1004_Array,
                                                      cD1005_Array         => xxcp_wks.WORKING_RCD(j).D1005_Array,
                                                      cD1006_Array         => xxcp_wks.WORKING_RCD(j).D1006_Array,
                                                      cI1009_Array         => xxcp_wks.WORKING_RCD(j).I1009_Array,
                                                      cErrorMessage        => vErrorMessage,
                                                      cInternalErrorCode   => cInternalErrorCode);
                  End If;

                  If cInternalErrorCode = 0 then

                    ---
                    --- XXCP_PROCESS_HISTORY
                    ---
                    XXCP_PROCESS_HIST_AR.Process_History_Insert(
                                                              cSource_Rowid            => xxcp_wks.WORKING_RCD(j).Source_rowid,
                                                              cParent_Rowid            => Null,
                                                              cTarget_Table            => xxcp_wks.WORKING_RCD(j).Target_Table,
                                                              cTarget_Instance_id      => xxcp_wks.WORKING_RCD(j).Target_Instance_id,
                                                              cProcess_History_id      => xxcp_wks.WORKING_RCD(j).Process_History_id,
                                                              cTarget_assignment_id    => xxcp_wks.WORKING_RCD(j).Target_Assignment_id,
                                                              cAttribute_id            => xxcp_wks.WORKING_RCD(j).Attribute_id,
                                                              cRecord_type             => xxcp_wks.WORKING_RCD(j).Record_type,
                                                              cRequest_id              => xxcp_global.gCommon(1).current_request_id,
                                                              cStatus                  => xxcp_wks.WORKING_RCD(j).Record_Status,
                                                              cInterface_id            => xxcp_wks.WORKING_RCD(j).Interface_id,
                                                              cModel_ctl_id            => xxcp_wks.WORKING_RCD(j).Model_ctl_id,
                                                              cRule_id                 => xxcp_wks.WORKING_RCD(j).Rule_id,
                                                              cTax_registration_id     => xxcp_wks.WORKING_RCD(j).Tax_Registration_id,
                                                              cEntered_rounding_amount => xxcp_wks.WORKING_RCD(j).Entered_Rnd_Amount,
                                                              cAccount_rounding_amount => xxcp_wks.WORKING_RCD(j).Accounted_Rnd_Amount,
                                                              cD1001_Array             => xxcp_wks.WORKING_RCD(j).D1001_Array,
                                                              cD1002_Array             => xxcp_wks.WORKING_RCD(j).D1002_Array,
                                                              cD1003_Array             => xxcp_wks.WORKING_RCD(j).D1003_Array,
                                                              cD1004_Array             => xxcp_wks.WORKING_RCD(j).D1004_Array,
                                                              cD1005_Array             => xxcp_wks.WORKING_RCD(j).D1005_Array,
                                                              cD1006_Array             => xxcp_wks.WORKING_RCD(j).D1006_Array,
                                                              cI1009_Array             => xxcp_wks.WORKING_RCD(j).I1009_Array,
                                                              cErrorMessage            => vErrorMessage,
                                                              cInternalErrorCode       => cInternalErrorCode);

                     If cInternalErrorCode = 0 then
                       vTiming(vTimingCnt).Flush_Records := vTiming(vTimingCnt).Flush_Records + 1;
                     Else
                       xxcp_foundation.FndWriteError(cInternalErrorCode, vErrorMessage);
                     End If;
                  Else
                    xxcp_foundation.FndWriteError(cInternalErrorCode, vErrorMessage);
                  End If;

                  If cInternalErrorCode = 0 then
                     vStatus := 'SUCCESSFUL';
                  Else
                     vStatus := 'ERROR';
                     xxcp_wks.SOURCE_SUB_CODE(xxcp_wks.WORKING_RCD(j).Source_Pos):= 7008;
                  End If;

              xxcp_wks.SOURCE_STATUS(xxcp_wks.WORKING_RCD(j).Source_Pos) := vStatus;
            Else
              -- ZAV
              xxcp_wks.SOURCE_SUB_CODE(xxcp_wks.WORKING_RCD(j).Source_Pos):= 7008;
              xxcp_wks.SOURCE_STATUS(xxcp_wks.WORKING_RCD(j).Source_Pos) := 'SUCCESSFUL';
            End If;

            UDI_Cnt := UDI_Cnt + 1;


          END IF;

      END LOOP;

      If cInternalErrorCode = 0 and UDI_Cnt > 0 then
        FORALL j in 1 .. xxcp_wks.SOURCE_CNT
          Update XXCP_ra_interface_lines
             Set vt_Status              = xxcp_wks.SOURCE_STATUS(j),
                 vt_internal_Error_code = Null,
                 vt_date_processed      = xxcp_global.SystemDate,
                 vt_Status_Code         = xxcp_wks.SOURCE_SUB_CODE(j)
           where rowid = xxcp_wks.Source_rowid(j);
      End If;

    END IF;

   -- Source_Pos

    If cInternalErrorCode <> 0 then
      -- Rollback
      If vCnt > 0 then
        Rollback To TARGET_INSERT;
        xxcp_foundation.FndWriteError(cInternalErrorCode, vErrorMessage);
      End If;

      Error_Whole_Transaction(cInternalErrorCode);

      If cInternalErrorCode = 12800 then
        xxcp_foundation.FndWriteError(cInternalErrorCode,
          'Entered DR <' ||to_char(vEntered_DR) ||'> Entered CR <' ||to_char(vEntered_CR) || '>');
      End If;

    End If;

     -- Remove Array Elements
     ClearWorkingStorage;

    Return(vCnt);
  End FlushRecordsToTarget;

  --  !! ***********************************************************************************************************
  --  !!
  --  !!                                   Reset_Errored_Transactions
  --  !!                              Reset Transactions from a previous run.
  --  !!
  --  !! ***********************************************************************************************************
  Procedure Reset_Errored_Transactions(cSource_assignment_id in number,
                                       cRequest_id           in number) is

  begin

    vTiming(vTimingCnt).Reset_Start := to_char(sysdate, 'HH24:MI:SS');

    -- Remove Errors
    Begin
      Delete from XXCP_errors cx
       Where source_assignment_id = cSource_assignment_id;

      Exception when OTHERS then null;
    End;
        -- Update Interface
    Begin

      Update XXCP_ra_interface_lines r
         set vt_status              = 'NEW',
             vt_Internal_Error_Code = Null,
             vt_status_code         = Null,
             vt_request_id          = cRequest_id
       where vt_source_assignment_id = cSource_assignment_id
         and vt_status IN ('ASSIGNMENT','GROUPING','ERROR','TRANSACTION','WAITING');

      Exception when OTHERS then Null;
    End;

    Commit;
  End RESET_ERRORED_TRANSACTIONS;

--  !! ***********************************************************************************************************
  --  !!
  --  !!                                  Set_Running_Status
  --  !!                      Flag the records that are going to be processed.
  --  !!
  --  !! ***********************************************************************************************************
  Procedure Set_Running_Status(cSource_id            in number,
                               cSource_assignment_id in number,
                               cRequest_id           in number,
                               cTable_Group_id       in number) is

    cursor rx(pSource_id in number, pSource_Assignment_id in number, pRequest_id in number, pTable_Group_id in number) is
      select r.vt_transaction_table,
             r.vt_transaction_id,
             r.rowid source_rowid,
             r.vt_transaction_type,
             nvl(t.grouping_rule_id,0) grouping_rule_id
        from XXCP_ra_interface_lines r, XXCP_sys_source_tables t
       where vt_source_assignment_id = pSource_assignment_id
         and vt_status               = 'NEW'
         and r.vt_transaction_table  = t.transaction_table
         and t.Table_group_id        = pTable_Group_id
         and t.source_id             = pSource_id
    order by t.transaction_table;

    vGrouping_rule_id XXCP_ra_interface_lines.vt_grouping_rule_id%type;

    -- Find records with no vt_grouping_rule_id
    cursor er2(pSource_assignment_id in number) is
      select Distinct g.vt_transaction_table
        from XXCP_ra_interface_lines g, XXCP_sys_source_tables t
       where g.vt_request_id = cRequest_id
         and g.vt_status = 'GROUPING'
         AND g.vt_transaction_table  = t.transaction_table
         and g.vt_source_assignment_id = pSource_assignment_id
         AND t.grouping_rule_id IS NOT NULL  -- 02.06.01
         and g.vt_grouping_rule_id is null;

    -- Find records with invalid Grouping rule
    cursor er3(pSource_assignment_id in number) is
      select Distinct g.vt_transaction_table
        from XXCP_ra_interface_lines g, XXCP_grouping_rules r, XXCP_sys_source_tables t
       where g.vt_request_id = cRequest_id
         and g.vt_status = 'GROUPING'
         and g.vt_source_assignment_id = pSource_assignment_id
         and g.vt_grouping_rule_id = r.grouping_rule_id(+)
         and r.grouping_rule_id is null;

    vCurrent_request_id number;
    vGroupingRuleExists    BOOLEAN     := FALSE;
    vPrevTransaction_Table XXCP_sys_source_tables.transaction_table%TYPE := '~NULL~';

  begin

    vTiming(vTimingCnt).Set_Running_Start := to_char(sysdate, 'HH24:MI:SS');

    vCurrent_Request_id := nvl(cRequest_id, 0);

    For rec in rx(cSource_id, cSource_assignment_id, vCurrent_Request_id, cTable_Group_id) Loop
      vGrouping_rule_id := Null;

      -- Populate Transaction_Table Array
      if vPrevTransaction_Table <> rec.vt_transaction_table then
        xxcp_global.gTrans_Table_Array.extend;
        xxcp_global.gTrans_Table_Array(xxcp_global.gTrans_Table_Array.count) := rec.vt_transaction_table;
      End if;

      vPrevTransaction_Table := rec.vt_transaction_table;

      IF Rec.grouping_rule_id > 0 then  -- 02.06.02

        vGroupingRuleExists := TRUE;

        xxcp_global.gCommon(1).current_transaction_table := Rec.vt_Transaction_Table;
        xxcp_global.gCommon(1).current_transaction_id    := Rec.vt_Transaction_id;

        vGrouping_rule_id := xxcp_custom_events.get_group_rule_id(cSource_id,
                                                                  cSource_assignment_id,
                                                                  rec.source_rowid,
                                                                  rec.vt_transaction_table,
                                                                  rec.vt_transaction_type);

        If nvl(vGrouping_rule_id, 0) = 0 then
          vGrouping_rule_id := Rec.grouping_rule_id; -- Default Rule
        End If;
      End If;
      -- Set Processing Status
      begin
        update XXCP_ra_interface_lines r
           set vt_status           = 'GROUPING',
               vt_date_processed   = xxcp_global.SystemDate,
               vt_status_code      = Null,
               vt_request_id       = cRequest_id,
               vt_Grouping_rule_id = vGrouping_rule_id
         where r.rowid = rec.source_rowid;

      Exception
        when OTHERS then null;
      End;
    End Loop;

    -- Error Checking
    IF vGroupingRuleExists then  -- 02.06.01 Don't Check If no Grouping Rule used.
      -- Find records with no vt_grouping_rule_id
      For Rec in er2(cSource_assignment_id) loop
        update XXCP_ra_interface_lines g
           set g.vt_status              = 'ERROR',
               g.vt_date_processed      = xxcp_global.SystemDate,
               g.vt_internal_error_code = 11088
         where g.vt_request_id = cRequest_id
           and g.vt_status     = 'GROUPING'
           and g.vt_source_assignment_id = cSource_assignment_id
           and g.vt_transaction_table = rec.vt_transaction_table;
      End Loop;
      -- Find records with invalid Grouping rule
      For Rec in er3(cSource_assignment_id) loop
        update XXCP_ra_interface_lines g
           set g.vt_status              = 'ERROR',
               g.vt_date_processed      = xxcp_global.SystemDate,
               g.vt_internal_error_code = 11089
         where g.vt_request_id           = cRequest_id
           and g.vt_status               = 'GROUPING'
           and g.vt_source_assignment_id = cSource_assignment_id
           and g.vt_transaction_table = rec.vt_transaction_table;
      End Loop;
    End If;

    Commit;

  End Set_Running_Status;


  --  !! ***********************************************************************************************************
  --  !!
  --  !!                                  Set_Assignment_Status
  --  !!                      Flag the records that are going to be processed.
  --  !!
  --  !! ***********************************************************************************************************
  Procedure Set_Assignment_Status(cSource_id            in number,
                                  cSource_assignment_id in number,
                                  cRequest_id           in number,
                                  cInternalErrorCode    in out number) is

    vCurrent_request_id number;
    vDV_Statement       varchar2(32000);
    vSQLERRM            varchar2(512);

  begin
    cInternalErrorCode := 0;


    vCurrent_Request_id := nvl(cRequest_id, 0);

    vDV_Statement := xxcp_dynamic_sql.Build_Interface_Selection(cSource_id, cSource_assignment_id, cRequest_id);

    If vDV_Statement is not null then
     Execute Immediate vDV_Statement;
    End If;

    Begin
        update XXCP_ra_interface_lines g
           set g.vt_status              = 'NEW',
               g.vt_date_processed      = xxcp_global.SystemDate,
               g.VT_STATUS_CODE = 7004
         where g.vt_request_id = cRequest_id
           and g.vt_status     = 'GROUPING'
           and g.vt_source_assignment_id = cSource_assignment_id;
    End;

    Commit;


  --  Exception
    --  when OTHERS then
     --   vSQLERRM := SQLERRM; -- New to trap Oracle's Reason for not working.
     --   xxcp_foundation.Set_InternalErrorCode(cInternalErrorCode, 10884);
  --      xxcp_foundation.FndWriteError(10884, vSQLERRM);
     --

  End Set_Assignment_Status;



  --  !! ***********************************************************************************************************
  --  !!
  --  !!                                    CONTROL
  --  !!                  External Procedure (Entry Point) to start the AR Engine.
  --  !!
  --  !! ***********************************************************************************************************
  Function Control(cSource_Group_ID    IN NUMBER,
                   cConc_Request_Id    IN NUMBER,
                   cTable_Group_id     IN NUMBER   DEFAULT 0,
                   cClearDownFrequency IN NUMBER   DEFAULT 0,
                   cDebug_on           IN VARCHAR2 DEFAULT 'N'
                   ) return number is

    -- Assignment
    cursor cfg(pSource_id in number, pSource_Assignment_id in Number, pRequest_id in number, pTable_Group_id in number) is
                select  vt_transaction_table
                       ,vt_transaction_type
                       ,vt_transaction_class
                       ,vt_transaction_id
                       ,vt_parent_trx_id
                       ,vt_transaction_ref
                       ,s.set_of_books_id
                       ,vt_source_assignment_id
                       ,g.rowid            Source_rowid
                       ,currency_code
                       ,0 entered_dr
                       ,0 entered_cr
                       ,s.instance_id source_instance_id
                       ,s.source_assignment_id
                       ,s.source_id
                       ,s.set_of_books_id source_set_of_books_id
                       ,g.vt_interface_id
                       ,w.transaction_set_name
                       ,nvl(w.transaction_set_id,0) transaction_set_id
                       ,g.vt_transaction_date
                       --
                       ,y.source_table_id
                       ,y.source_type_id
                       ,cx.source_class_id
                       ,t.calc_legal_exch_rate
                    from XXCP_ra_interface_lines g,
                         XXCP_source_assignments s,
                         -- New
                         XXCP_sys_source_tables       t,
                         XXCP_sys_source_types        y,
                         XXCP_sys_source_classes      cx,
                         XXCP_source_transaction_sets w
                   WHERE G.VT_STATUS                 = 'ASSIGNMENT'
                     AND G.VT_SOURCE_ASSIGNMENT_ID   = PSOURCE_ASSIGNMENT_ID
                     AND G.VT_SOURCE_ASSIGNMENT_ID   = S.SOURCE_ASSIGNMENT_ID
                     -- TABLE
                     AND G.VT_TRANSACTION_TABLE    = T.TRANSACTION_TABLE
                     AND T.SOURCE_ID               = pSOURCE_ID
                     and T.table_group_id          = pTable_Group_id
                     -- TYPE
                     AND G.VT_TRANSACTION_TYPE     = Y.TYPE
                     and Y.latch_only             = 'N'
                     AND Y.SOURCE_TABLE_ID         = T.SOURCE_TABLE_ID
                     -- CLASS
                     AND G.VT_TRANSACTION_CLASS    = CX.CLASS
                     and CX.latch_only             = 'N'
                     AND Y.SOURCE_TABLE_ID         = CX.SOURCE_TABLE_ID
                     -- TRANSACTION_SET_ID
                     AND Y.TRANSACTION_SET_ID      = W.TRANSACTION_SET_ID
                     --
                     ORDER BY VT_TRANSACTION_TABLE, W.SEQUENCE, G.VT_PARENT_TRX_ID, CX.SEQUENCE, VT_TRANSACTION_ID;


    -- Transactions
    cursor GLI(pSource_id in number, pSource_Assignment_id in Number, pRequest_id in number, pTable_Group_id in number) is
    select
     vt_transaction_table
    ,vt_transaction_type
    ,vt_Interface_id
    ,vt_transaction_class
    ,vt_transaction_id
    ,vt_transaction_ref
    ,s.instance_id            vt_instance_id
    --
    ,g.Cust_trx_type_name     Category
    ,g.interface_line_context Source
    ,g.gl_date                Accounting_Date
    --
    ,g.Currency_Code
    ,vt_parent_trx_id
    ,g.ROWID                  Source_Rowid
    ,s.set_of_books_id
    ,g.amount                 Parent_Entered_Amount
    ,g.Currency_Code          Parent_Entered_Currency
    ,s.source_assignment_id
    ,s.source_id
    ,w.transaction_set_name
    ,w.Transaction_Set_id
    ,g.creation_date  source_creation_date
    --
    ,y.source_table_id
    ,y.source_type_id
    ,cx.source_class_id
    ,t.CLASS_MAPPING_REQ
    ,0 Transaction_cost
    ,0 code_combination_id
    from XXCP_ra_interface_lines      g,
         XXCP_source_assignments      s,
         -- New
         XXCP_sys_source_tables       t,
         XXCP_sys_source_types        y,
         XXCP_sys_source_classes      cx,
         XXCP_source_transaction_sets w
    where g.vt_status               = 'TRANSACTION'
      and g.vt_source_assignment_id = pSource_Assignment_ID
      and g.vt_source_assignment_id = s.source_assignment_id
      -- Table
      and g.vt_transaction_table    = t.transaction_table
      and t.source_id               = pSource_id
      and t.table_group_id          = pTable_Group_id
      -- Type
      and g.vt_transaction_type     = y.type
      and y.latch_only             = 'N'
      and t.source_table_id         = y.source_table_id
      -- Class
      and g.vt_transaction_class    = cx.class
      and cx.latch_only             = 'N'
      and y.source_table_id         = cx.source_table_id
      -- Transaction_Set_id
      and y.Transaction_Set_id      = w.Transaction_Set_id
      --
    ORDER BY vt_transaction_table DESC, w.sequence, g.vt_parent_trx_id, cx.sequence, vt_transaction_id, vt_transaction_line;

    Cursor ConfErr(pSource_Assignment_id in number, pRequest_id in number) is
      select Distinct k.vt_parent_trx_id, k.vt_transaction_table
        from XXCP_ra_interface_lines k
       where k.vt_request_id           = pRequest_id
         and k.vt_source_assignment_id = pSource_Assignment_id
         and k.vt_status               = 'ERROR';

    -- Assignments
    Cursor sr1(pSource_Group_id in number) is
      select set_of_books_id,
             Source_Assignment_id,
             instance_id source_instance_id,
             Stamp_Parent_Trx,
             Source_id
        from XXCP_source_assignments g
       where g.Source_Group_id = pSource_Group_id
         and g.active = 'Y';


    i                       pls_integer := 0;
    k                       number;
    j                       integer := 0;
    xw                      number := 0;
    g                       number;
    gx                      number := 0;
    vTiming_Start           number;

    vInternalErrorCode      XXCP_errors.INTERNAL_ERROR_CODE%type := 0;
    vExchange_Rate_Type     XXCP_tax_registrations.EXCHANGE_RATE_TYPE%type;
    vCommon_Exch_Curr       XXCP_tax_registrations.COMMON_EXCH_CURR%type;

    vGlobalPrecision        pls_integer := 2;
    CommitCnt               pls_Integer := 0;
    vSource_Activity        varchar2(4) := 'GL';
    vJob_Status             pls_Integer := 0;

    vTransaction_Class      XXCP_sys_source_classes.CLASS%type;
    vRequest_ID             XXCP_process_history.request_id%type;

    -- NEW PARAMETERS
    vCurrent_Parent_Trx_id  XXCP_transaction_attributes.parent_trx_id%type := 0;
    vTransaction_Type       XXCP_sys_source_types.Type%type;

    vErrorMessage              Varchar2(4000);
    vGlobal_Rounding_Tolerance Number := 0;
    vTransaction_Error         Boolean := False;

    -- Used for the Header Records
    vExtraLoop                 boolean := False;
    vISExtraClass              varchar2(30);
    vExtraClass                varchar2(30);
    vExtraCount                pls_integer := 0;
    --
    vSource_Assignment_id XXCP_source_assignments.source_assignment_id%type;
    vStamp_Parent_Trx     XXCP_source_assignments.stamp_parent_trx%type := 'N';
    vSource_instance_id   XXCP_source_assignments.instance_id%type;
    vSource_ID            XXCP_sys_sources.source_id%type;

    vClass_Mapping_Name    XXCP_sys_source_classes.class%type;

    vDML_Compiled         varchar2(1);
    vDebugParentTrx       number;

    vTrx_Ref_From         XXCP_ra_interface_lines.vt_transaction_ref%type;
    vTrx_Ref_To           XXCP_ra_interface_lines.vt_transaction_ref%type;

    vError_Completion_Status   xxcp_source_assignment_groups.error_completion_status%TYPE := 'N';                                     


    Cursor SF(pSource_Group_id in number) is
      select s.source_activity,
             s.Source_id,
             Preview_ctl,
             Timing,
             Custom_events,
             s.Cached,
             s.dml_compiled,
             nvl(g.error_completion_status,'N') error_completion_status 
        from XXCP_source_assignment_groups g, XXCP_sys_sources s
       where source_group_id = pSource_Group_id
         and s.source_id = g.source_id;

    Cursor Tolx is
      Select Numeric_Code Global_Rounding_Tolerance
        from XXCP_lookups
       where lookup_type = 'ROUNDING TOLERANCE'
         and lookup_code = 'GLOBAL';

    Cursor CRL(pSource_id in number) is
     select Distinct Transaction_table, source_id
       from XXCP_column_rules
      where source_id = pSource_id;

  BEGIN

    xxcp_global.Trace_on   := nvl(gEngine_Trace,'N');
    xxcp_global.Preview_on := 'N';
    xxcp_global.SystemDate := Sysdate;
    vTimingCnt             := 1;
    gEngine_Error_Found    := FALSE;
    gForce_Job_Error       := FALSE;
    gForce_Job_Warning     := FALSE;

    For SFRec in SF(cSource_Group_id) Loop
      xxcp_global.gCommon(1).Source_id   := SFRec.Source_id;
      xxcp_global.gCommon(1).Preview_ctl := SFRec.Preview_ctl;
      xxcp_global.gCommon(1).Preview_on  := xxcp_global.Preview_on;
      xxcp_global.gCommon(1).Custom_events           := SFRec.Custom_events;
      xxcp_global.gCommon(1).current_Source_activity := SFRec.Source_Activity;
      xxcp_global.gCommon(1).Cached_Attributes := SFRec.Cached;
       vDML_Compiled := SFRec.DML_Compiled;

      vSource_Activity := SFRec.Source_Activity;
      vSource_id       := SFRec.Source_id;
      --
      vError_Completion_Status := SFRec.Error_Completion_Status;
    End Loop;


    xxcp_global.SET_SOURCE_ID(cSource_id => vSource_id);
    --     SYS.DBMS_SUPPORT.START_TRACE( waits=>true, binds=>true );

    vTiming(vTimingCnt).CF_Records    := 0;
    vTiming(vTimingCnt).Flush_Records := 0;
    vTiming(vTimingCnt).Start_time    := to_char(sysdate, 'HH24:MI:SS');
    vTiming(vTimingCnt).Flush         := 0;
    vTiming_Start     := xxcp_reporting.Get_Seconds;

    If xxcp_global.gCommon(1).Custom_events = 'Y' then
      xxcp_custom_events.Set_system_mode(xxcp_global.gCommon);
    End If;

    If vDML_Compiled = 'N' then
      -- Prevent Processing and the column rules have changed and
      -- they need compiling.
      vJob_Status := 4;
      xxcp_foundation.show('ERROR: Column Rules have changed, but not re-generated');
      xxcp_foundation.FndWriteError(10999,'Column Rules have changed, but not re-generated');

    End If;


    --
    -- ## *******************************************************************************************************
    -- ##
    -- ##                            Check to See the process is already in use
    -- ##
    -- ## *******************************************************************************************************
    --

    If NOT xxcp_management.Locked_Process(cSource_Activity => vSource_Activity,  cSource_Group_id => cSource_Group_id) and vJob_Status = 0 then

      vRequest_id := xxcp_management.Activate_Lock_Process(vSource_Activity,cSource_Group_id,cConc_Request_Id);

      xxcp_foundation.show('Activity locked....' || to_char(vRequest_id));

      xxcp_global.User_id  := fnd_global.user_id;
      xxcp_global.Login_id := fnd_global.login_id;
      xxcp_global.gCommon(1).current_process_name  := 'XXCP_ARENG';
      xxcp_global.gCommon(1).Current_request_id := vRequest_id;

      If xxcp_global.gCommon(1).Custom_events = 'Y' then
        vInternalErrorCode := xxcp_custom_events.Before_Processing(cSource_id => vSource_id);
      End If;
      --
      -- ## **************************************************************************************************
      -- ##
      -- ##                                Setup Ready for the Run
      -- ##
      -- ## **************************************************************************************************
      --
      For REC in sr1(pSource_Group_id => cSource_Group_id) Loop

        Exit when vJob_Status <> 0;

        vSource_instance_id   := rec.source_instance_id;
        vSource_Assignment_id := rec.Source_Assignment_id;
        vStamp_Parent_Trx     := rec.Stamp_Parent_Trx;
        xxcp_global.gCommon(1).Grouping_Rule := vStamp_Parent_Trx;

        vTiming(vTimingCnt).Init_Start := to_char(sysdate, 'HH24:MI:SS');
        vTiming(vTimingCnt).Flush_Records := 0;

        If vTimingCnt = 1 then
          -- First time and once only
          xxcp_foundation.show('Initialization....');
          xxcp_te_base.Engine_Init(cSource_id            => vSource_id,
                                   cSource_Group_id      => cSource_Group_id,
                                   cInternalErrorCode    => vInternalErrorCode);
        End If;

        vTiming(vTimingCnt).Source_Assignment_id := vSource_Assignment_id;
        -- Setup Globals
        xxcp_global.gCommon(1).current_assignment_id := vSource_Assignment_ID;
        -- 02.06.01
        xxcp_global.gTrans_Table_Array.delete;

        --
        -- *********************************************************************************************************
        --                            Populate Column Rules and Initialize Cursors
        -- *********************************************************************************************************
        --
        vTiming(vTimingCnt).Memory_Start := to_char(sysdate, 'HH24:MI:SS');

        For Rec in CRL(vSource_id) Loop
          -- Used for column Attributes and error messages
          xxcp_memory_Pack.LoadColumnRules(vSource_id, 'XXCP_RA_INTERFACE_LINES',vSource_instance_id,Rec.Transaction_table,vRequest_id,vInternalErrorCode);
          xxcp_te_base.Init_Cursors(Rec.Transaction_table);
        End Loop;
        
        vInternalErrorCode := 0;

        --
        -- *******************************************************************************************************
        --     Reset Previosly Errored Transactions and Assign Parent Trx Id to Incomming transactions
        -- *******************************************************************************************************
        --
        Reset_Errored_Transactions(cSource_assignment_id => vSource_Assignment_id,
                                   cRequest_id           => vRequest_id);

        Set_Running_Status(        cSource_id            => vSource_id,
                                   cSource_assignment_id => vSource_Assignment_id,
                                   cRequest_id           => vRequest_id,
                                   cTable_Group_id       => cTable_Group_id);
                                   
        vInternalErrorCode := xxcp_dynamic_sql.Apply_Exclude_Rule(cSource_assignment_id => vSource_Assignment_id);                           
                                   

        If vInternalErrorCode = 0 then

           Transaction_Group_Stamp(  cStamp_Parent_Trx     => vStamp_Parent_Trx,
                                     cSource_Assignment_id => vSource_Assignment_id,
                                     cTable_Group_id       => cTable_Group_id,
                                      cInternalErrorCode    => vInternalErrorCode);

           --
           -- GLOBAL ROUNDING TOLERANCE
           --
           vGlobal_Rounding_Tolerance := 0;
           For Rec in Tolx loop
             vGlobal_Rounding_Tolerance := Rec.Global_Rounding_Tolerance;
           End loop;

           If vInternalErrorCode = 0 then
             --
             -- Get Standard Parameters
             --
             vInternalErrorCode := xxcp_foundation.fnd_gl_lookup(cExchange_Rate_Type => vExchange_Rate_Type,
                                                                 cGlobalPrecision    => vGlobalPrecision,
                                                                 cCommon_Exch_Curr   => vCommon_Exch_Curr);
           End If;

         End If; -- Set Running Status

        -- Set Assignment Status
        If vInternalErrorCode = 0 then
          Set_Assignment_Status(vSource_id, vSource_Assignment_id, vRequest_id,vInternalErrorCode);
        End If;

        -- Data Staging
        If nvl(vInternalErrorCode,0) = 0 then
          vInternalErrorCode := xxcp_custom_events.Data_Staging(vSource_id,vSource_Assignment_id,vRequest_id, Null);
        End If;

        vTiming(vTimingCnt).Init_End := to_char(sysdate, 'HH24:MI:SS');
        Commit;

        If vInternalErrorCode = 0 then
          --
          -- ## ***********************************************************************************************************
          -- ##
          -- ##                                Assignment
          -- ##
          -- ## ***********************************************************************************************************
          --
          i := 0;

          vTiming(vTimingCnt).CF_last     := xxcp_reporting.Get_Seconds;
          vTiming(vTimingCnt).CF          := 0;
          vTiming(vTimingCnt).CF_Records  := 0;
          vTiming(vTimingCnt).CF_Start_Date := Sysdate;
          xxcp_global.SystemDate := sysdate;

          For CFGRec in CFG(vSource_id, vSource_Assignment_id, vRequest_id, cTable_Group_id) Loop

                i := i + 1;

                vTiming(vTimingCnt).CF_Records := vTiming(vTimingCnt).CF_Records + 1;

                xxcp_global.gCommon(1).current_source_rowid      := CFGRec.Source_Rowid;
                xxcp_global.gCommon(1).current_transaction_date  := CFGRec.vt_Transaction_Date;
                xxcp_global.gCommon(1).current_transaction_table := CFGRec.vt_Transaction_Table;
                xxcp_global.gCommon(1).current_transaction_id    := CFGRec.vt_Transaction_id;
                xxcp_global.gCommon(1).current_parent_trx_id     := CFGRec.vt_Parent_Trx_id;
                xxcp_global.gCommon(1).current_Interface_id      := CFGRec.vt_interface_id;
                xxcp_global.gCommon(1).current_Interface_Ref     := CFGRec.vt_Transaction_Ref;
                xxcp_global.gCommon(1).preview_parent_trx_id     := Null;
                xxcp_global.gCommon(1).preview_source_rowid      := Null;
                xxcp_global.gCommon(1).calc_legal_exch_rate      := CFGRec.calc_legal_exch_rate;

                vDebugParentTrx := CFGRec.vt_Parent_Trx_id;

                vTransaction_Class := CFGRec.VT_Transaction_Class;

                If XXCP_TE_BASE.Ignore_Class(CFGRec.VT_Transaction_Table, vTransaction_Class) then
                  Begin
                    update XXCP_ra_interface_lines
                       set vt_status              = 'IGNORED',
                           vt_internal_error_code = Null,
                           vt_date_processed      = xxcp_global.SystemDate
                     where rowid = CFGRec.Source_Rowid;

                  Exception
                    when others then
                      null;
                  End;
                Else

                  XXCP_TE_CONFIGURATOR.Control(
                                               cSource_Assignment_ID     => vSource_assignment_id,
                                               cSet_of_books_id          => CFGRec.Source_Set_of_books_id,
                                               cTransaction_Date         => CFGRec.VT_Transaction_Date,
                                               cSource_id                => vSource_id,
                                               cSource_Table_id          => CFGRec.Source_table_id,
                                               cSource_Type_id           => CFGRec.Source_type_id,
                                               cSource_Class_id          => CFGRec.Source_class_id,
                                               cTransaction_id           => CFGRec.VT_Transaction_ID,
                                               cSourceRowid              => CFGRec.Source_Rowid,
                                               cParent_Trx_id            => CFGRec.VT_Parent_Trx_id,
                                               cTransaction_Set_id       => CFGRec.Transaction_Set_id,
                                               cSpecial_Array            => 'N',
                                               cKey                      => Null,
                                               cTransaction_Table        => CFGRec.VT_Transaction_Table,
                                               cTransaction_Type         => CFGRec.VT_Transaction_Type,
                                               cTransaction_Class        => vTransaction_Class,
                                               cInternalErrorCode        => vInternalErrorCode);

                  -- check status
                  IF xxcp_global.Get_Release_Date IS NOT NULL THEN -- Release Date
                    Stop_Whole_Transaction; 
                  ELSIF NVL(vInternalErrorCode, 0) = 0 THEN
                    UPDATE XXCP_ra_interface_lines
                       SET vt_status              = 'TRANSACTION',
                           vt_internal_error_code = NULL,
                           vt_date_processed      = xxcp_global.systemdate
                     WHERE ROWID = cfgrec.source_rowid;
                  ELSIF vInternalerrorcode = -1 THEN
                    UPDATE XXCP_ra_interface_lines
                       SET vt_status              = 'SUCCESSFUL',
                           vt_internal_error_code = NULL,
                           vt_date_processed      = xxcp_global.systemdate,
                           vt_status_code         = 7001
                     WHERE ROWID = cfgrec.source_rowid;
									ELSIF vInternalErrorCode BETWEEN 7021 and 7028 then
									  Pass_Through_Whole_Trx(CFGRec.Source_Rowid, vInternalErrorCode);
                  ELSIF NVL(vInternalErrorCode, 0) <> 0 THEN
                    UPDATE XXCP_ra_interface_lines
                       SET vt_status              = 'ERROR',
                           vt_internal_error_code = 12000,
                           vt_date_processed      = xxcp_global.systemdate
                     WHERE ROWID = cfgrec.source_rowid;

                     -- Flag Error
                     gEngine_Error_Found := TRUE;

                  END IF;                  

                  -- Emergency Exit (out of disk space)
                  If vInternalErrorCode between 3550 and 3559 then
                    Rollback;
                    vJob_Status := 6;
                    Exit;
                  End If;

                End If; -- End configuration

                -- ##
                -- ## Commit Configured rows
                -- ##

                If i >= 2000 then
                  xxcp_global.SystemDate    := Sysdate;
                  Commit;
                  i           := 0;
                  vJob_Status := xxcp_management.Has_Oracle_been_Terminated(vRequest_id,
                                                                            vSource_Activity,
                                                                            cSource_Group_id,
                                                                            cConc_Request_Id);
                  Exit when vJob_Status > 0; -- Controlled Exit
                End If;

         END Loop;

          xxcp_dynamic_sql.Close_Session;
          -- #
          -- # Clear Common vars
          -- #
          xxcp_global.gCommon(1).current_transaction_table := Null;
          xxcp_global.gCommon(1).current_transaction_id    := Null;
          xxcp_global.gCommon(1).current_parent_trx_id     := Null;
          xxcp_global.gCommon(1).current_Interface_Ref     := Null;

          Commit; -- Final Configuration Commit.

          --
          If xxcp_global.gCommon(1).Custom_events = 'Y' then
            xxcp_custom_events.after_assignment_processing(vSource_id,vSource_assignment_id);
          End If;

          vTiming(vTimingCnt).CF := xxcp_reporting.Get_Seconds_Diff(vTiming(vTimingCnt).CF_last,xxcp_reporting.Get_Seconds);
          vTiming(vTimingCnt).CF_End_Date := Sysdate;

          -- ***********************************************************************************************************
          --
          --  Set associated Transactions to error if anyone of the Transactions is a set has errored in configuration
          --
          -- ***********************************************************************************************************
          --
          xxcp_foundation.show('Reporting Configuration errors....');
          xxcp_global.SystemDate := Sysdate;
          vInternalErrorCode := 0;
          For ConfErrRec in ConfErr(vSource_Assignment_id, vRequest_id) loop

            Begin
              update XXCP_ra_interface_lines
                 set vt_status              = 'ERROR',
                     vt_internal_Error_code = 12824, -- Associated errors
                     vt_date_processed      = xxcp_global.SystemDate
               where vt_request_id = vRequest_id
                 and vt_Source_Assignment_id = vSource_Assignment_id
                 and vt_status IN ('ASSIGNMENT', 'TRANSACTION')
                 and vt_parent_trx_id = ConfErrRec.vt_parent_trx_id
                 and vt_transaction_table = ConfErrRec.vt_Transaction_table;

               -- Flag Error
               If SQL%ROWCOUNT > 0 then
                 gEngine_Error_Found := TRUE;  
               End If;            

            Exception
              when OTHERS then
                vInternalErrorCode := 12819; -- Update Failed.

            End;
          End Loop;

          --## ***********************************************************************************************************
          --##
          --##                                    Transaction Engine
          --##
          --## ***********************************************************************************************************

          vTiming(vTimingCnt).TE_Last    := xxcp_reporting.Get_Seconds;
          vTiming(vTimingCnt).TE_Records := 0;
          vTiming(vTimingCnt).TE_Start_Date := Sysdate;

          vExtraLoop             := False;
          vExtraClass            := Null;
          vISExtraClass          := Null;
          vExtraCount            := 0;
          xxcp_global.SystemDate := Sysdate;
          i                := 0;

          If vInternalErrorCode = 0 and vJob_Status = 0 then

            xxcp_foundation.show('Transactions....');
            xxcp_wks.Reset_Source_Segments;
            gBalancing_Array      := xxcp_wks.Clear_Segments;
            ClearWorkingStorage;
            xxcp_global.New_Transaction := 'Y';

            vDebugParentTrx := Null;

            For  GLIRec in  GLI(vSource_id, vSource_assignment_id, vRequest_id, cTable_Group_id) Loop

                  vInternalErrorCode := 0;
                  Exit when vJob_Status > 0;
                  vTiming(vTimingCnt).TE_Records := vTiming(vTimingCnt).TE_Records + 1;
                  xxcp_global.New_Transaction := 'N';
                  --
                  --
                  -- ***********************************************************************************************************
                  --         When a New Transaction is detected then flush the current buffer
                  -- ***********************************************************************************************************
                  --
                  If (vCurrent_Parent_Trx_id <> GLIRec.vt_parent_Trx_id) then

                    vTransaction_Error := False;

                    --
                    -- !! ******************************************************************************************************
                    --                             FLUSH TRANSACTION
                    -- !! ******************************************************************************************************
                    --
                    If vCurrent_Parent_Trx_id <> 0 then
                      CommitCnt := CommitCnt + 1; -- Number of records process since last commit.

                      k := FlushRecordsToTarget(vGlobal_Rounding_Tolerance,
                                                vGlobalPrecision,
                                                vTransaction_Type,
                                                vInternalErrorCode);
                    End If;

                    xxcp_global.New_Transaction := 'Y';

                    --
                    -- ***********************************************************************************************************
                    --            Store Variables that are only set once per Parent Trx Id
                    -- ***********************************************************************************************************
                    --

                    xxcp_global.gCommon(1).current_transaction_table := GLIRec.vt_Transaction_Table;
                    xxcp_global.gCommon(1).current_parent_trx_id     := GLIRec.vt_Parent_Trx_id;
                    xxcp_global.gCommon(1).current_transaction_id    := GLIRec.vt_Transaction_id;
                    xxcp_global.gCommon(1).current_source_rowid      := GLIRec.Source_Rowid;


                    xxcp_wks.Trace_Log     := Null;

                    vCurrent_Parent_Trx_id := GLIRec.vt_Parent_Trx_id;
                    vTransaction_Type      := GLIRec.vt_Transaction_Type;
                    ClearWorkingStorage;
                    --
                    -- ***********************************************************************************************************
                    --                         Extra Loops for different Classes
                    -- ***********************************************************************************************************
                    --
                    vExtraLoop    := False;
                    vExtraClass   := Null;
                    vISExtraClass := Null;
                    vExtraCount   := 0;

                    --
                    -- !! ******************************************************************************************************
                    --                             TAX TRANSACTION
                    -- !! ******************************************************************************************************
                    --
                    If xxcp_global.gCommon(1).Custom_events = 'Y' then
/*                      If xxcp_te_base.gSabrix_Engine = 'Y' then
                        xxcp_wks.gTaxCodes.delete;
                      End If;
*/
                      vInternalErrorCode := nvl(xxcp_custom_events.TRANSACTION_ENGINE_HEADER(vSource_id, vSource_assignment_id, GLIRec.vt_Transaction_table,GLIRec.vt_Transaction_Type, GLIRec.Source_Rowid, Null, xxcp_te_base.gMasterTradingSetId),0);
                      If vInternalErrorCode != 0 then
                        vTransaction_Error := True;
                        Error_Whole_Transaction(vInternalErrorCode);
                      End If;
                    End If;


                     -- Check to See if an HDR Class is required
                     For j in 1 .. xxcp_global.gSRE.Count loop
                      If GLIRec.vt_Transaction_Table = xxcp_global.gSRE(j).Transaction_table then
                        vISExtraClass := xxcp_global.gSRE(j).Extra_Record_type;
                        Exit;
                      End If;
                     End Loop;

                     -- Phase 1 Check
                     If nvl(GLIRec.Parent_Entered_Amount,0) = 0 then
                       -- Check to see if any of the records are ! zero
                        vExtraClass := vISExtraClass;
                        vExtraLoop  := TRUE;
                        vExtraCount := 1; -- Dont do it again for this Trx
                     End If;

                  Else
                    vExtraLoop  := FALSE; -- !!!!
                    vExtraClass := Null;
                  End If;

                  -- New Extra HDR Control
                  -- Phase 2 Check
                  If vExtraCount = 0 then
                    If nvl(GLIRec.Parent_Entered_Amount,0) != 0 then
                      If vISExtraClass is not null then
                        vExtraClass := vISExtraClass;
                        vExtraLoop  := TRUE;
                        vExtraCount := 1; -- Dont do it again for this Trx
                      End If;
                    End If;
                  End If;

                  vDebugParentTrx := GLIRec.vt_Parent_Trx_id;

                  --
                  -- ***********************************************************************************************************
                  --                                   Store Working Variables
                  -- ***********************************************************************************************************
                  --
                  xxcp_global.gCommon(1).current_Interface_id    := GLIRec.vt_interface_id;
                  xxcp_global.gCommon(1).current_Accounting_date := GLIRec.Accounting_Date;
                  xxcp_global.gCommon(1).current_Interface_Ref   := GLIRec.VT_Transaction_Ref;
                  xxcp_global.gcommon(1).current_creation_date   := GLIRec.source_creation_date;
                  --
                  -- ***********************************************************************************************************
                  --                                   Call the Transaction Engine
                  -- ***********************************************************************************************************
                  --
                  If vTransaction_Error = False then
                    vInternalErrorCode := 0; -- Reset for each row

                    i := i + 1; -- Count the number of rows processed.

                    xxcp_wks.SOURCE_CNT  := xxcp_wks.SOURCE_CNT + 1;
                    xxcp_wks.Source_rowid(xxcp_wks.source_cnt)   := GLIRec.Source_Rowid;
                    xxcp_wks.SOURCE_SUB_CODE(xxcp_wks.source_cnt):= 0;
                    xxcp_wks.SOURCE_STATUS(xxcp_wks.source_cnt)  := '?';

                    -- *********************************************************************
                    -- Class Mapping
                    -- *********************************************************************
                    If GLIRec.class_mapping_req = 'Y' then
                        vClass_Mapping_Name := xxcp_te_base.class_mapping(
                                                           cSource_id              => vSource_id,
                                                           cClass_Mapping_Required => GLIRec.class_mapping_req,
                                                           cTransaction_Table      => GLIRec.vt_transaction_table,
                                                           cTransaction_Type       => GLIRec.vt_transaction_type,
                                                           cTransaction_Class      => GLIRec.vt_transaction_class,
                                                           cTransaction_id         => GLIRec.vt_transaction_id,
                                                           cCode_Combination_id    => GLIRec.code_combination_id,
                                                           cAmount                 => GLIRec.Transaction_Cost);
                    End If;
                    -- *********************************************************************
                    -- End Class Mapping
                    -- *********************************************************************

                    xxcp_global.gCommon(1).current_Source_rowid := GLIRec.Source_Rowid;

                    XXCP_TE_ENGINE.Control(cSource_assignment_id    => vSource_Assignment_id,
                                           cSource_Table_id         => GLIRec.Source_Table_id,
                                           cSource_Type_id          => GLIRec.Source_Type_id,
                                           cSource_Class_id         => GLIRec.Source_Class_id,
                                           cTransaction_id          => GLIRec.vt_Transaction_id,
                                           cParent_Trx_id           => GLIRec.vt_Parent_Trx_id,
                                           cSourceRowid             => GLIRec.Source_Rowid,
                                           cParent_Entered_Amount   => GLIRec.Parent_Entered_Amount,
                                           cParent_Entered_Currency => GLIRec.Parent_Entered_Currency,
                                           cClass_Mapping           => GLIRec.class_mapping_req,
                                           cClass_Mapping_Name      => vClass_Mapping_Name,
                                           --
                                           cInterface_ID            => GLIRec.VT_Interface_id,
                                           cExtraLoop               => vExtraLoop,
                                           cExtraClass              => vExtraClass,
                                           cTransaction_set_id      => GLIRec.Transaction_Set_id,
                                           --
                                           cTransaction_Table       => GLIRec.VT_Transaction_Table,
                                           cTransaction_Type        => GLIRec.VT_Transaction_Type,
                                           cTransaction_Class       => GLIRec.vt_Transaction_Class,
                                           cInternalErrorCode       => vInternalErrorCode);


                    xxcp_global.gCommon(1).current_trading_set := Null;

                    -- Report Errors
                    If vInternalErrorCode <> 0 then
                      Error_Whole_Transaction(vInternalErrorCode);
                      vTransaction_Error := True;
                    End If;

                  End If;
                  --
                  -- ***********************************************************************************************************
                  --                     Test to See if Oracle or the User has terminated this process
                  --                                   And Commit after Each 1000 Records processed
                  -- ***********************************************************************************************************
                  --
                  IF CommitCnt >= 2000 THEN
                    CommitCnt := 0;
                    xxcp_global.SystemDate    := sysdate;
                    Commit;
                     vJob_Status := xxcp_management.Has_Oracle_been_Terminated(vRequest_id,
                                                                               vSource_Activity,
                                                                               cSource_Group_id,
                                                                               cConc_Request_Id);
                    Exit when vJob_Status > 0; -- Controlled Exit
                  END IF;

            END LOOP;

            xxcp_te_base.Close_Cursors;

            --
            --  !!***********************************************************************************************************
            --                     Flush the Buffer for a Final Time
            -- !! ***********************************************************************************************************
            --
            IF vCurrent_Parent_Trx_id <> 0 and vInternalErrorCode = 0 then
              If vInternalErrorCode = 0 then
                k := FlushRecordsToTarget(vGlobal_Rounding_Tolerance,
                                          vGlobalPrecision,
                                          vTransaction_type,
                                          vInternalErrorCode);
              End If;
            End If;
          End If; -- Internal Error Check GLI
        End If; -- End Internal Error Check

        vTiming(vTimingCnt).TE := xxcp_reporting.Get_Seconds_Diff(vTiming(vTimingCnt).TE_last,xxcp_reporting.Get_Seconds);

        xxcp_foundation.show('Clear down...');
        xxcp_global.SystemDate := Sysdate;
        -- *******************************************************************
        -- Error out records remaining with a Processing status after the run
        -- *******************************************************************
        Begin
          update XXCP_ra_interface_lines
             set vt_status              = 'ERROR',
                 vt_internal_error_code = Decode(vt_status,'TRANSACTION',12999,'ASSIGNMENT',12998,'GROUPING',12996),
                 vt_date_processed      = xxcp_global.SystemDate
           where Rowid = any
           (select g.rowid Source_Rowid
                    from XXCP_ra_interface_lines g
                   where g.vt_request_id = xxcp_global.gCommon(1).current_request_id
                     and g.vt_source_assignment_id = vSource_Assignment_id
                     and g.vt_status in ('ASSIGNMENT', 'TRANSACTION','GROUPING'));

           -- Flag Error
           If SQL%ROWCOUNT > 0 then
             gEngine_Error_Found := TRUE;  
           End If;            

          Exception when OTHERS then Null;
        End;

        -- #
        -- # Clear Common vars
        -- #
        xxcp_global.gCommon(1).current_transaction_table := Null;
        xxcp_global.gCommon(1).current_transaction_id    := Null;
        xxcp_global.gCommon(1).current_parent_trx_id     := Null;
        xxcp_global.gCommon(1).current_Interface_Ref     := Null;
        --
        -- ***********************************************************************************************************
        --                     Clear Down
        -- ***********************************************************************************************************
        --
        ClearWorkingStorage;
        Commit;
        vTiming(vTimingCnt).TE_End_Date := Sysdate;
        vTimingCnt := vTimingCnt + 1;
      End loop; -- SR1

      -- Error Completion Status
      If gEngine_Error_Found and vJob_Status = 0 then
        If vError_Completion_Status  = 'W' then
          gForce_Job_Warning := TRUE;
        Elsif vError_Completion_Status  = 'E' then
          gForce_Job_Error   := TRUE;
        End if;
      End If;

      xxcp_te_base.ClearDown;
      xxcp_memory_pack.ClearDown;
      xxcp_dynamic_sql.Close_Session;

      --
      -- ***********************************************************************************************************
      --                     DeActive the Lock in XXCP_ACTIVITY_CONTROL
      -- ***********************************************************************************************************
      --
      xxcp_management.DeActivate_lock_process(vRequest_id,vSource_Activity,cSource_Group_id);

      If xxcp_global.gCommon(1).Custom_events = 'Y' then
        xxcp_custom_events.After_Processing(cSource_id       => vSource_id,
                                            cSource_Group_id => cSource_Group_id,
                                            cRequest_id      => vRequest_id);
      End If;
      Commit;

    Else
      --
      -- Report If this process was already in use
      --
      xxcp_foundation.show('Engine already in use');
      vJob_Status := 1;
    End If;

    -- Force Warning/Error
    If gForce_Job_Warning then
      vJob_Status := 15;
    Elsif gForce_Job_Error then
      vJob_Status := 16;
    End if;

    xxcp_te_base.Write_Timings(vTiming, vTiming_Start);
    --
    -- ************************************************************************************************
    --                     Email Job Details to Users
    -- ************************************************************************************************
    --
    xxcp_comms.Send_job_details(xxcp_global.gCommon(1).Current_Request_id,vJob_Status);
    --
    -- ************************************************************************************************
    --                     Show Time Statistics when called from SQL*Plus
    -- ************************************************************************************************
    --
    xxcp_global.gCommon(1).Current_Request_id := Null;
    --
    -- ***********************************************************************************************************
    --                     Return Job Status Code and Finish!!
    -- ***********************************************************************************************************
    --
    Return(vJob_Status);

  End Control;

Begin

  Begin
    Select Substr(Lookup_code, 1, 1)
      into gEngine_Trace
      from XXCP_lookups
     where lookup_type = 'TRACE AR ENGINE'
       and enabled_flag = 'Y';

    Exception when OTHERS then
          gEngine_Trace := 'N';
  End;


END XXCP_AR_ENGINE;
/
