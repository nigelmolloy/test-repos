CREATE OR REPLACE PACKAGE BODY XXCP_RT_ENGINE AS
/***********************************************************************************
                        V I R T A L  T R A D E R  L I M I T E D
  		      (Copyright 2005-2012 Virtual Trader Ltd. All rights Reserved)

   NAME:       XXCP_RT_ENGINE
   PURPOSE:    RT Engine

   REVISIONS:
   Ver        Date      Author  Description
   ---------  --------  ------- ------------------------------------
   02.01.00   28/06/02  Keith   First Coding
	 02.04.00   02/01/07  Keith   Added Table_Group_id
   02.04.01   07/02/08  Keith   Changed RT Column names
   02.04.02   19/02/08  Keith   Assignment Only does not create Cache Records
   02.04.03   17/03/08  Keith   Added Frozen Flag Logic.
   02.04.04   20/03/08  Keith   Revised Remove_Assignment_Records
   02.04.05   24/03/08  Keith   Added Remove_Prior_Records logic
   02.04.06   0gDDL_Interface_Select5/04/08  Keith   Added Investigate and IC Price check
   02.04.07   09/05/08  Keith   Not Remove, not use UnFreeze_Attribute_Record
   02.05.00   20/05/08  Keith   Removed .show function as requested by Cisco.
   02.05.01   07/07/08  Keith   xxcp_global.gcommon(1).current_creation_date
   02.05.02   15/07/08  Keith   gRealTime_Results populate new fields in interface
   02.05.03   10/10/08  Keith   Removed unwanted reset function.
   02.05.04   09/01/09  Keith   Added suggested Optimizer hint to delete of errors
   02.05.05   14/01/09  Keith   Added Assignment_Engine_Init
   02.05.06   29/01/09  Keith   Added Optimizer hint to all deletion of XXCP_errors
   02.05.07   29/01/09  Keith   Removed Optimizer hint to all deletion of XXCP_errors
                                and moved delete of old records to alert queue.
   02.05.08   10/03/09  Nigel   Added vt_results_attributes11..30 to control
   02.04.09   13/03/09  Nigel   Added diagnostics
   02.04.10   22/07/09  Nigel   Changed UnFreeze_Attribute_Record to include SOURCE_TABLE_ID
                                to improve performance.
   02.04.10a  14/09/09  Simon   Removed call to LoadPricingMethods.
   02.06.01   20/10/09  Simon   Grouping now done by transaction_table not assignment_id..
   02.06.02   21/12/09  Simon   Added vt_results_attributes31..50 to control
   02.06.03   28/12/09  Keith   Added Calc_legal_exch_rate
   02.06.04   03/06/10  Keith   Added logic to stop whole batch if one record fails.
   02.06.05   16/11/10  Keith   Added Timing Adv Mode to help isolate performance issues
   03.06.06   15/06/12  Keith   Updated for Cisco to change order by clause in CFG cursor
   03.06.07   02/01/12  Mat     OOD-597 Init - gDDL_Interface_Select - ref to CP_ changed to XXCP_
**********************************************************************************************/

  gEngine_Trace       varchar2(1) := 'N';
  gErrorWholeRequest  varchar2(1) := 'Y';
	-- New 
	gPackage_Called     varchar2(1) := 'N'; 
	gSource_Group_id    number; 

  -- New Dynamic SQL
  gDDL_Interface_Select    varchar2(12000);
  gDDL_Interface_Order_By  varchar2(2000);
   
	vTimingCnt          pls_integer; 
  gTrans_Table_Array  XXCP_DYNAMIC_ARRAY := XXCP_DYNAMIC_ARRAY();
  
  TYPE gCursors_Rec is RECORD(
    v_cursorId     pls_Integer := 0,
    v_Target_Table XXCP_column_rules.transaction_table%type,
    v_usage        varchar2(1),
    v_OpenCur      varchar2(1) := 'N',
    v_LastCur      pls_integer := -1);

  TYPE gCUR_REC is TABLE of gCursors_Rec INDEX by BINARY_INTEGER;
  gCursors gCUR_REC;

  -- *************************************************************
  --
  --                     Software Version Control
  --
  -- This package can be checked with SQL to ensure it's installed
  -- select packagename.software_version from dual;
  -- *************************************************************
  Function Software_Version RETURN VARCHAR2 IS
  BEGIN
    RETURN('Version [03.06.07] Build Date [02-JAN-2013] Name [XXCP_RT_ENGINE]');
  END Software_Version;
	

  -- *************************************************************
  --                     NextSequenceId  
  --                   ------------------ 
	-- This sequence is for General/Public Use and just provides a
	-- unque number 
  -- *************************************************************
	Function NextSequenceId Return Number is
	  vSeq number;
	Begin
    Select XXCP_PUBLIC_SEQ.nextval into vSeq from dual;
		Return(vSeq);
	End NextSequenceId; 

  -- *************************************************************
  --                             SHOW 
  --                   --------------------------
  -- *************************************************************	
	Procedure Show(cMessage in Varchar2) is
	  Begin
		   xxcp_foundation.show(cMessage => cMessage);
		End Show;
		
  -- *************************************************************
  --                     Report_Error_Detail 
  --                   --------------------------
  -- *************************************************************
  Procedure Report_Error_Details(cInternalErrorCode in number, cError_Message in varchar2, cLong_Message in long) is
	Begin
		 xxcp_foundation.FndWriteError(cInternalErrorCode => cInternalErrorCode,
		                               cErrorMessage      => cError_Message,
																	 cLong_Message      => cLong_Message);
	End Report_Error_Details;
	
  -- *************************************************************
  --                       SummarizeInterface 
  --                   --------------------------
  -- *************************************************************
	Procedure SummarizeInterface(cSource_Assignment_id in number, cRequest_id in number) is
	
	 pragma autonomous_transaction;
	
	 Begin
	 
	   Update XXCP_rt_interface
		  set vt_request_id = cRequest_id
			where vt_request_id <= cRequest_id
			  and vt_source_assignment_id = cSource_Assignment_id
				and vt_status = 'SUCCESSFUL';
				
		  Commit;
			
	 End SummarizeInterface;
   
  --
  -- OLS_Trx_By_Source_Group
  -- =======================
  -- This function sets the OLS date for Transactions that have
  -- a unpredicatable ols date.
  -- 
  Function OLS_Trx_By_Source_Group( cSource_Group_id      in number,
                                    cTransaction_Table    in varchar2,
                                    cParent_Trx_id        in number) Return number is
                                   
   Cursor STI(pTransaction_Table in varchar2, pSource_Group_id in number) is
     select s.source_table_id,
            g.source_assignment_id,
            t.trading_set_id
      from XXCP_sys_source_tables s,
           XXCP_source_assignments g,
           XXCP_trading_sets t
      where s.transaction_Table = pTransaction_Table        
        and g.source_id         = t.Source_id     
        and g.source_group_id   = pSource_Group_id
        and g.source_id         = t.source_id;
                     
   vInternal_Error_Code number(8) := 12054;   
                    
  Begin
    For Rec in STI(cTransaction_Table, cSource_Group_id) Loop 
      Begin
       Update XXCP_transaction_attributes x
         set x.ols_date = x.transaction_date,
             x.frozen   = 'F'
         where Source_Assignment_id = rec.Source_Assignment_id
           and Parent_Trx_id        = cParent_Trx_id
           and Source_Table_id      = Rec.source_table_id
           and Trading_Set_id       = Rec.Trading_Set_id
           and x.Frozen != 'N';
       
       vInternal_Error_Code := 0;
       
       Exception when Others then Null;
       
      End;
    End Loop;
  
    Return(vInternal_Error_Code);
  
  End OLS_Trx_By_Source_Group;
                        
	
  -- *************************************************************
  --                     Get_Source_Assignment_id 
  --                   --------------------------
  -- *************************************************************
  Function Get_source_assignment_id( cSource_assignment_name in varchar2) return number is

    Cursor c1(pSource_id in number, pSource_Assignment_name in varchar2) is
     Select source_assignment_id
       from XXCP_source_assignments
     where source_id              = pSource_id
       and source_assignment_name = pSource_Assignment_name
       and nvl(active,'Y')        = 'Y';

   vSource_Assignment_id XXCP_source_assignments.source_assignment_id%type := 0;

  Begin
    For Rec in c1(17, cSource_Assignment_Name) loop
      vSource_Assignment_id := Rec.source_assignment_id;
    End loop;
    vSource_Assignment_id := vSource_Assignment_id;
    Return(vSource_Assignment_id);
  End Get_source_assignment_id;
  
  --
  -- Get_Source_Table_id
  --
  Function Get_Source_Table_id(cTransaction_Table in varchar2, cSource_id in number default 17) return number is
     Cursor st(pTransaction_Table in varchar2, pSource_id in number) is
       select t.source_table_id
       from XXCP_sys_source_tables t
       where t.source_id = pSource_id
         and t.transaction_table = pTransaction_Table;
         
      vSource_Table_id XXCP_sys_source_tables.source_table_id%type := 0;
       
    Begin
    
      For Rec in st(pTransaction_Table => cTransaction_Table, pSource_id => cSource_id) Loop
        vSource_Table_id := rec.Source_Table_Id;
      End Loop;
    
      Return(vSource_Table_id);
    End Get_Source_Table_id;
  
    Function Get_Trading_Set_id(cTrading_Set in varchar2, cSource_id in number default 17) return number is
  
       Cursor st(pTrading_Set in varchar2, pSource_id in number) is
       select t.trading_set_id
       from XXCP_trading_sets t
       where t.source_id = pSource_id
         and t.trading_set = pTrading_Set;
         
      vTrading_Set_id XXCP_trading_sets.trading_set_id%type := 0;
       
    Begin
    
      For Rec in st(pTrading_Set => cTrading_Set, pSource_id => cSource_id) Loop
        vTrading_Set_id := rec.Trading_Set_id;
      End Loop;
    
      Return(vTrading_Set_id);
    End Get_Trading_Set_id;
    
  --
  -- Set_Frozen_Flag/OLS Date
  --
  Function Set_Frozen_Flag(cSource_assignment_id in number,
                           cSource_table_id      in number,
                           cTrading_Set_id       in number,
                           cParent_trx_id        in number,
                           cTransaction_Date     in date)
                           return boolean is
                           
    vFrozen boolean := False;                           
                           
  Begin
  
    Begin
    
      Update XXCP_transaction_attributes t
       set t.frozen = 'F',
           t.ols_date =  nvl(cTransaction_Date, xxcp_global.SystemDate)
       where source_assignment_id = cSource_assignment_id
         and source_table_id      = cSource_table_id
         and parent_trx_id        = cParent_trx_id
         and trading_set_id       = cTrading_Set_id
         and t.frozen            != 'N';
         
      vFrozen := True;
      
      Exception when NO_DATA_FOUND THEN NULL;
    
    End;
    
    Return(vFrozen);
  
  End Set_Frozen_Flag;
	
  -- *************************************************************
  --                        Get_Trading_Set 
  --                   --------------------------
  --
  -- The Trading Set is needed for the Index when reading the
  -- Gateway views.
  -- *************************************************************
  Function Get_Master_Trading_Set(cTrading_Set_id out Number , cSource_id in number default 17) return varchar2 is
  
   Cursor c1(pSource_id in number) is
		 select Trading_set, trading_set_id
		  from XXCP_trading_sets w
		 where w.source_id = pSource_id
       and w.MASTER = 'Y'
		 order by w.sequence;
	
   vTrading_Set XXCP_trading_sets.trading_set%type := -1;	
	   
  Begin
    For Rec in c1(cSource_id) loop
      vTrading_Set    := Rec.Trading_Set;
      cTrading_Set_id := Rec.Trading_Set_id;
  	End Loop;
  	Return(vTrading_Set);
  End Get_Master_Trading_Set;	
  
  
 -- !! ***********************************************************************************************************
 -- !!
 -- !!                                   Assignment_Engine_Init
 -- !!                 This procedure reads data into memory read for use by the Engines
 -- !!
 -- !! ***********************************************************************************************************
  PROCEDURE Assignment_Engine_Init (  
                           cSource_id               in number
	                        ,cSource_Group_id         in number
                          ,cInternalErrorCode   in out NOCOPY number) is

   Begin

        xxcp_global.set_source_id(cSource_id); -- Set the source
        xxcp_te_base.Reset_Package;
        -- Set up Working Storage
	      xxcp_wks.init;
        -- Preload memory tables
        xxcp_global.gCR_CNT := 1;

        If xxcp_global.Preview_on = 'Y' then
          xxcp_memory_pack.LoadSQLBuildStatements(cSource_id,'N','N');
        Else
          xxcp_memory_pack.LoadSQLBuildStatements(cSource_id,'N','Y');
        End If;
				
        xxcp_memory_pack.LoadTaxRegInfo('N');
        xxcp_memory_pack.LoadTaxRegOnlyInfo('N');
        xxcp_memory_pack.LoadExtraRecordType(cSource_id,'N');
        xxcp_memory_pack.LoadCurrencies('N');
        xxcp_memory_pack.LoadCurrTolerences(cSource_id, 'N');
        xxcp_memory_pack.LoadIgnoreClass('N',cSource_id);

	      xxcp_dynamic_sql.Open_Session;

        -- New Session
				xxcp_global.Set_New_Trx_Flag('N'); 

				xxcp_global.gCommon(1).Current_Trx_Group_id := 0;
        xxcp_global.gCommon(1).Direct_Currencies       := 'Y';

  End Assignment_Engine_Init;
  
 
  -- *************************************************************
  --                         Open_Session 
  --                     -------------------
  -- Caution: Using this will clear down the memory stand and
  -- force vt to re-initialise it. This will slow down the processing
  -- if it is callsed for every record being processed.
  -- *************************************************************
  Procedure Open_Session(cSource_Assignment_Name in varchar2,
                         cSource_Assignment_Id  out number,
                         cSource_id              in number default 17
                         ) is
   Begin
     xxcp_global.SET_SOURCE_ID(cSource_id => cSource_id);
     cSource_Assignment_id :=  Get_source_assignment_id(cSource_assignment_name => cSource_Assignment_Name);
	   xxcp_dynamic_sql.Open_Session;      
   End Open_Session;
	 
  -- *************************************************************
  --                         Close_Session 
  --                     -------------------
  -- *************************************************************
  Procedure Close_Session is
   Begin
     xxcp_dynamic_sql.Close_Session;
   End Close_Session;
	 
  -- *************************************************************
  --                      Remove_Transaction
  --                     -------------------
  -- *************************************************************
	Function Remove_Transaction_Set(cSource_Assignment_id   IN NUMBER,
                                  cUnique_Request_id      IN NUMBER
									               ) return number is
									 
		
	Cursor c1(pSource_Assignment_id in number, pRequest_id in number) is
	 select vt_interface_id, vt_transaction_table, vt_transaction_id, vt_parent_trx_id
	 from XXCP_rt_interface t
	 where t.vt_source_assignment_id = pSource_Assignment_id
	   and t.vt_request_id           = pRequest_id
		 and t.vt_status               in ('NEW','ERROR');
		 
	Cursor c2(pSource_Assignment_id in number, pTransaction_table in varchar2, pParent_trx_id in number) is	 	
	  select header_id 
		from xxcp_transaction_header_v
		where source_assignment_id = pSource_Assignment_id
		  and transaction_table    = pTransaction_table
			and parent_trx_id        = pParent_trx_id;
									 
	Cursor c3(pSource_Assignment_id in number, pTransaction_table in varchar2, pParent_trx_id in number, pTransaction_id in number) is	 	
	  select header_id, attribute_id
		from xxcp_transaction_attributes_v
		where source_assignment_id = pSource_Assignment_id
		  and transaction_table    = pTransaction_table
			and parent_trx_id        = pParent_trx_id
			and transaction_id       = pTransaction_id;

	 vCnt               Number;
	 vRecCnt            Number := 0;
	 vInternalErrorCode Number := 0;		
			
	Begin

     -- Check that no Process History exists. Source id is very important
     For Rec in c1(cSource_Assignment_id, cUnique_Request_Id) Loop
		   Select count(*) cnt into vCnt
			   from XXCP_process_history
				 where source_id    = 17
				   and interface_id = rec.vt_interface_id;
					 
			 If nvl(vCnt,0) > 0 then
			   vInternalErrorCode := 12770; -- Process History Exists
			   Exit;
			 End If;
		 End Loop;
		 
	   -- Remove Configuration
     For Rec in c1(cSource_Assignment_id, cUnique_Request_Id) Loop
		    
				vRecCnt := vRecCnt +1;
				
		    -- Remove Header
		    For Rec2 in c2(cSource_Assignment_id, rec.vt_transaction_table, rec.vt_parent_trx_id) Loop
				  -- Header
					Begin  
		        Delete from XXCP_transaction_header h
				     where h.header_id            = rec2.header_id
						   and h.source_assignment_id = cSource_Assignment_id;

						Exception when OTHERS then null;
					 ---
					End;
				End loop;

		    -- Remove Header
		    For Rec3 in c3(cSource_Assignment_id, rec.vt_transaction_table, rec.vt_parent_trx_id, rec.vt_transaction_id) Loop
				  -- Attributes cache 
				  Begin 
		        Delete from XXCP_transaction_cache a where a.attribute_id = rec3.attribute_id;
						Exception when OTHERS then null;
          End;
				  -- Attributes cache 
				  Begin 
		        Delete from XXCP_transaction_attributes a  where a.attribute_id = rec3.attribute_id;
						Exception when OTHERS then null;
           End;
				End loop;
		 
		    -- Remove Interface Record 
		    Delete from XXCP_rt_interface where vt_interface_id = rec.vt_interface_id;
		 
		 End Loop; 	  
		 
	
	   Return(vInternalErrorCode);
	
	End Remove_Transaction_Set;
	-- *************************************************************
  --                       UnFreeze_Attribute_Record
  --                     ----------------------------
	-- *************************************************************
  Procedure UnFreeze_Attribute_Record(
	                 cSource_Assignment_Id   IN NUMBER,
	                 cSource_Table_Id        IN NUMBER,
                   cParent_Trx_id          IN NUMBER,
                   cTransaction_id         IN NUMBER,
                   cSource_Type_id         IN NUMBER,
                   cRemove_Cache           IN VARCHAR2 DEFAULT 'N'
									 ) is
  Begin
  
      -- UnFreeze_Attribute_Record rather than delete.
      -- this save on database fragmenation.
  
      update XXCP_transaction_attributes a
        set frozen = 'N'
        where a.source_assignment_id = cSource_Assignment_id
          and a.parent_trx_id  = cParent_Trx_id
          and a.transaction_id = cTransaction_id
          and a.source_type_id = cSource_type_id
          -- 02.04.10
          and a.source_table_id = cSource_Table_Id;
      
      Exception when Others then Null;
    
  End UnFreeze_Attribute_Record; 
 	
	-- *************************************************************
  --                         Delete_Old_Errors
  --                     ----------------------------
	-- *************************************************************
  Procedure Delete_Old_Errors(cSource_assignment_id in number, 
				                      cRequest_id           in number) is
                             
  Begin
  
    Delete from XXCP_errors cx
      where cx.source_assignment_id = cSource_Assignment_id
        and (cx.source_assignment_id,cx.parent_trx_id, cx.transaction_id) = any(
              select vt_source_assignment_id, vt_parent_trx_id, vt_transaction_id
                from XXCP_rt_interface r
               where r.vt_request_id = cRequest_id);
          
    Exception when Others then Null;
  
  End Delete_Old_Errors;

  
	-- *************************************************************
  --                      Remove_Transaction_in_Error
  --                     ----------------------------
  -- *************************************************************
	Function Remove_Transactions_In_Error(cSource_Assignment_id   IN NUMBER,
                                        cUnique_Request_id      IN NUMBER
									                    ) return number is
									 
		
	Cursor c1(pSource_Assignment_id in number, pRequest_id in number) is
	 select vt_interface_id, vt_transaction_table, vt_transaction_id, vt_parent_trx_id
	 from XXCP_rt_interface t
	 where t.vt_source_assignment_id = pSource_Assignment_id
	   and t.vt_request_id           = pRequest_id
		 and t.vt_status               = 'ERROR';
		 
	Cursor c2(pSource_Assignment_id in number, pTransaction_table in varchar2, pParent_trx_id in number) is	 	
	  select header_id 
		from xxcp_transaction_header_v
		where source_assignment_id = pSource_Assignment_id
		  and transaction_table    = pTransaction_table
			and parent_trx_id        = pParent_trx_id;
									 
	Cursor c3(pSource_Assignment_id in number, pTransaction_table in varchar2, pParent_trx_id in number, pTransaction_id in number) is	 	
	  select header_id, attribute_id
		from xxcp_transaction_attributes_v
		where source_assignment_id = pSource_Assignment_id
		  and transaction_table    = pTransaction_table
			and parent_trx_id        = pParent_trx_id
			and transaction_id       = pTransaction_id;

	  
	 vRecCnt            Number := 0;
	 vInternalErrorCode Number := 0;		
			
	Begin
		 
	   -- Remove Configuration
     For Rec in c1(cSource_Assignment_id, cUnique_Request_Id) Loop
		    
				vRecCnt := vRecCnt +1;
				
				Delete from XXCP_errors 
				where source_assignment_id = cSource_Assignment_id
				  and transaction_id       = Rec.vt_transaction_id; 
				
		    -- Remove Header
		    For Rec2 in c2(cSource_Assignment_id, rec.vt_transaction_table, rec.vt_parent_trx_id) Loop
				  -- Header
					Begin  
		        Delete from XXCP_transaction_header h
				     where h.header_id            = rec2.header_id
						   and h.source_assignment_id = cSource_Assignment_id;

						Exception when OTHERS then null;
					 ---
					End;
				End loop;
		 
		    -- Remove Header
		    For Rec3 in c3(cSource_Assignment_id, rec.vt_transaction_table, rec.vt_parent_trx_id, rec.vt_transaction_id) Loop
				  -- Attributes cache 
				  Begin 
		        Delete from XXCP_transaction_cache a where a.attribute_id = rec3.attribute_id;
						Exception when OTHERS then null;
          End;
				  -- Attributes cache 
				  Begin 
		        Delete from XXCP_transaction_attributes a  where a.attribute_id = rec3.attribute_id;
						Exception when OTHERS then null;
           End;
				End loop;
		 
		    -- Remove Interface Record 
		    Delete from XXCP_rt_interface where vt_interface_id = rec.vt_interface_id;
		 
		 End Loop; 	   
	
	   Return(vInternalErrorCode);
	
	End Remove_Transactions_In_Error;
 	 
  -- !! ***********************************************************************************
  -- !!
  -- !!                            Open_Cursors
  -- !!
  -- !! ***********************************************************************************
  Function Open_Cursor(cUsage in varchar2, cTarget_Table in varchar2) Return pls_integer is
  
    i    pls_integer;
    vPos pls_integer := 0;
  
  Begin
    For i in 1 .. gCursors.count loop
      If (gCursors(i).v_Target_Table = cTarget_Table) AND (gCursors(i).v_usage = cUsage) then
        vPos := i;
        If gCursors(i).v_OpenCur = 'N' then
          gCursors(i).v_cursorId := DBMS_SQL.OPEN_CURSOR;
          gCursors(i).v_OpenCur := 'Y';
          gCursors(i).v_LastCur := -1;
          exit;
        End If;
      End If;
    End Loop;
  
    Return(vPos);
  End Open_Cursor;

  -- *************************************************************
  --
  --                  ClearWorkingStorage
  --
  -- *************************************************************
  Procedure ClearWorkingStorage IS
  begin
    -- Processed Records
    xxcp_wks.WORKING_CNT := 0;
    xxcp_wks.WORKING_RCD.Delete;
    
    -- Current Records before processing
    xxcp_wks.SOURCE_CNT := 0;
    XXCP_WKS.SOURCE_ROWID.Delete;
    XXCP_WKS.SOURCE_STATUS.Delete;
    XXCP_WKS.SOURCE_SUB_CODE.Delete;
    -- Balancing
    xxcp_wks.BALANCING_CNT := 0;
    XXCP_WKS.BALANCING_RCD.Delete;

  End ClearWorkingStorage;
  
  
  -- !! ***************************************************************************************
  --  !!
  --  !!                                   Error_Whole_Request
  --  !!                If one part of the wrapper finds a problem whilst processing a record,
  --  !!                          then we need to error the whole transaction.
  --  !!
  -- !! ***************************************************************************************
  Procedure Error_Whole_Request( cInternalErrorCode in out Number) IS
  BEGIN
   
    If gErrorWholeRequest = 'Y' then
     -- Set all transactions in group to error status
     Begin
      Update XXCP_rt_interface r
         set vt_status = 'ERROR', vt_internal_Error_code = 12823 -- Associated errors
       where vt_request_id           = xxcp_global.gCommon(1).Current_request_id
         and vt_Source_Assignment_id = xxcp_global.gCommon(1).current_assignment_id 
         and NOT vt_status in ('TRASH', 'IGNORED');
     End;  
        
    End If;
     
  
  END Error_Whole_Request;  

  -- !! ***************************************************************************************
  --  !!
  --  !!                                   Error_Whole_Gl_Transaction
  --  !!                If one part of the wrapper finds a problem whilst processing a record,
  --  !!                          then we need to error the whole transaction.
  --  !!
  -- !! ***************************************************************************************
  Procedure Error_Whole_Transaction(cInternalErrorCode in out Number) IS
  BEGIN
  
     -- Set all transactions in group to error status
     Begin
       Update XXCP_rt_interface r
          set vt_status = 'ERROR', vt_internal_Error_code = 12823 -- Associated errors
        where vt_request_id = xxcp_global.gCommon(1).Current_request_id
          and vt_Source_Assignment_id = xxcp_global.gCommon(1).current_assignment_id
          and vt_parent_trx_id = xxcp_global.gCommon(1).current_parent_Trx_id
          and NOT vt_status in ('TRASH', 'IGNORED');
     End; 

     -- Update Record that actually caused the problem.
     Begin
      update XXCP_rt_interface
         set vt_status              = 'ERROR',
             vt_internal_error_code = cInternalErrorCode,
             vt_date_processed      = xxcp_global.SystemDate
       where rowid = xxcp_global.gCommon(1).current_source_rowid
         and vt_request_id = xxcp_global.gCommon(1).current_request_id;
     End;

    If xxcp_global.gCommon(1).Custom_events = 'Y' then
    
      xxcp_custom_events.ON_TRANSACTION_ERROR(xxcp_global.gCommon(1).Source_id,
                                              xxcp_global.gCommon(1).current_assignment_id,
                                              xxcp_global.gCommon(1).current_source_rowid,
                                              xxcp_global.gCommon(1).current_Transaction_Table,
                                              xxcp_global.gCommon(1).current_Parent_Trx_id,
                                              xxcp_global.gCommon(1).current_transaction_id,
                                              cInternalErrorCode);
    End If;
  
    ClearWorkingStorage;
  
  END Error_Whole_Transaction;

  --
  --   !! ***********************************************************************************************************
  --   !!
  --   !!                                     No_Action_GL_Transaction
  --   !!                                If there are no records to process.
  --   !!
  --   !! ***********************************************************************************************************
  --
  Procedure No_Action_Transaction(cSource_Rowid in rowid) IS
  BEGIN
    -- Update Record that actually had not action required
    Begin
      update XXCP_rt_interface
         set vt_status              = 'SUCCESSFUL',
             vt_internal_error_code = Null,
             vt_date_processed      = xxcp_global.SystemDate,
             vt_status_code         = 7003
       where rowid = csource_rowid;

    End;
  END NO_Action_Transaction;

  --  !! ***********************************************************************************************************
  --  !!
  --  !!                                   Transaction_Group_Stamp
  --  !!                 Stamp Incomming record with parent Trx to create groups of
  --  ||                    transactions that resemble on document transaction.
  --  !!
  --  !! ***********************************************************************************************************
  Procedure Transaction_Group_Stamp(cStamp_Parent_Trx     in varchar2,
                                    cSource_assignment_id in number,
																		cTable_Group_id       in number,
                                    cInternalErrorCode    out number) IS


    vInternalErrorCode number(8) := 0;
    vStatement         varchar2(6000);

    -- Distinct list of Transaction Tables
    -- populated in Set_Running_Status
    CURSOR cTrxTbl is
      SELECT transaction_table,
             nvl(grouping_rule_id,0) grouping_rule_id
      FROM   XXCP_sys_source_tables
      where  source_id = xxcp_global.gCommon(1).Source_id
      and    transaction_table in (select * from TABLE(CAST(gTrans_Table_Array AS XXCP_DYNAMIC_ARRAY)));

    Cursor c1(pSource_assignment_id in number, 
              pTable_Group_id       in number,
              pTransaction_Table    IN VARCHAR2) IS
      select distinct 'update XXCP_rt_interface ' ||
                      '  set vt_parent_trx_id = ' || t.Parent_trx_column || ' ' ||
                      'where vt_status = :1 ' ||
                      '  and vt_source_assignment_id = :2 ' ||
                      '  and vt_transaction_table = :3 ' ||
                      '  and vt_request_id = :4 ' Statement_line,
                      t.Parent_trx_column,
                      t.Transaction_Table
        from XXCP_rt_interface       g,
             XXCP_sys_source_tables  t,
             XXCP_source_assignments x
       where g.vt_request_id           = xxcp_global.gCommon(1).Current_request_id
         and g.vt_status               = 'ASSIGNMENT'
         and g.vt_transaction_Table    = t.TRANSACTION_TABLE
         and t.source_id               = x.Source_id
				 and t.TABLE_GROUP_ID          = pTable_Group_id
         and x.source_assignment_id    = pSource_assignment_id
         and g.vt_source_assignment_id = x.source_assignment_id
         and g.vt_transaction_table    = pTransaction_Table;     -- 02.06.01

    Cursor er1(pSource_assignment_id in number,
               pTransaction_Table    IN VARCHAR2) IS
      select g.vt_interface_id,
             g.rowid source_rowid,
             g.vt_transaction_table
        from XXCP_rt_interface g
       where g.vt_request_id           = xxcp_global.gCommon(1).Current_request_id
         and g.vt_status               = 'ASSIGNMENT'
         and g.vt_source_assignment_id = psource_assignment_id
         and g.vt_transaction_table    = pTransaction_Table     -- 02.06.01
         and g.vt_parent_trx_id is null;

  BEGIN


    For cTblRec in cTrxTbl Loop  -- Loop for all Transaction Tables in Array within SA.
      
      If cTblRec.Grouping_Rule_Id > 0 THEN  --02.06.01
        XXCP_DYNAMIC_SQL.Apply_Grouping_Rules(cInternalErrorCode => vInternalErrorCode,
                                              cTransaction_Table => cTblRec.Transaction_Table);

        -- Error Checking for Null Parent Trx id
        For er1Rec in er1(cSource_assignment_id,
                          cTblRec.transaction_table) LOOP

          update XXCP_rt_interface l
             set vt_status                = 'ERROR',
                 l.vt_internal_error_code = 11090
           where l.vt_request_id = xxcp_global.gCommon(1).Current_request_id
             and l.vt_transaction_table = er1rec.vt_transaction_table;

        End Loop;

      Else
        -- Mass update of Parent Trx id
        For rec in c1(cSource_assignment_id, 
                      cTable_Group_id,
                      cTblRec.transaction_table) LOOP
          If rec.Parent_trx_column is null then
            vInternalErrorCode := 10665;
            xxcp_foundation.FndWriteError(vInternalErrorCode,'Transaction Table <' || Rec.Transaction_Table || '>');
          Else
            vStatement := rec.Statement_line;
            Begin

              Execute Immediate vStatement
                Using 'ASSIGNMENT', cSource_Assignment_id, rec.transaction_table, xxcp_global.gCommon(1).current_request_id;

            Exception
              when OTHERS then
                vInternalErrorCode := 10667;
                xxcp_foundation.FndWriteError(vInternalErrorCode, SQLERRM,vStatement);
            End;
          End If;
        End loop;
      End If;
    End Loop;
    
    cInternalErrorCode := vInternalErrorCode;
  
  END Transaction_Group_Stamp;

  --  !! ***********************************************************************************************************
  --  !!
  --  !!                                   Reset_Errored_Transactions                                                                                                                                                                                                                                                                                                                                                                                                                                                   
  --  !!                              Reset Transactions from a previous run.                                                                                                                                                                                                                                                                                                                                                                                                                                           
  --  !!                                      * externally called *                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
  --  !! ***********************************************************************************************************                                                                                                                                                                                                                                                                                                                                                                                                    
  Procedure Reset_Errored_Transactions(cSource_assignment_id in number,
                                       cRequest_id           in number) is
  
  begin
  
    -- Remove Errors
    Begin
      Delete from XXCP_errors cx
       Where cx.source_assignment_id = cSource_assignment_id
         and cx.request_id           = cRequest_id;

      Exception When OTHERS then null;
    End;
		-- Update Interface
    Begin

      Update XXCP_rt_interface r
         set vt_status              = 'NEW',
             vt_Internal_Error_Code = Null,
             vt_status_code         = Null
       where vt_source_assignment_id = cSource_assignment_id
			   and vt_request_id           = cRequest_id
         and (vt_status in ('ERROR', 'PROCESSING', 'TRANSACTION', 'ASSIGNMENT'));

      Exception when OTHERS then Null;
    End;

    Commit;
  End RESET_ERRORED_TRANSACTIONS;
 
  --  !! ***********************************************************************************************************
  --  !!
  --  !!                                  Set_Running_Status
  --  !!                      Flag the records that are going to be processed.
  --  !!
  --  !! ***********************************************************************************************************
  Procedure Set_Running_Status(cSource_id            in number,
                               cSource_assignment_id in number,
                               cRequest_id           in number,
															 cTable_Group_id       in number) is
  
    cursor rx(pSource_id in number, pSource_Assignment_id in number, pRequest_id in number, pTable_Group_id in number) is
      select r.vt_transaction_table,
             r.vt_transaction_id,
             r.rowid source_rowid,
             r.vt_transaction_type,
             nvl(t.grouping_rule_id,0) grouping_rule_id
        from XXCP_rt_interface r, XXCP_sys_source_tables t
       where vt_request_id = xxcp_global.gcommon(1).unique_request_id
			   and vt_source_assignment_id = pSource_assignment_id
         and vt_status               = 'NEW'
         and r.vt_transaction_table  = t.transaction_table
				 and t.Table_Group_id        = pTable_Group_id
         and t.source_id             = pSource_id
    order by t.transaction_table;
  
    vGrouping_rule_id XXCP_rt_interface.vt_grouping_rule_id%type;
  
    -- Find records with no vt_grouping_rule_id
    cursor er2(pSource_assignment_id in number) is
      select Distinct g.vt_transaction_table
        from XXCP_rt_interface g, XXCP_sys_source_tables t
       where g.vt_request_id = cRequest_id
         and g.vt_status = 'ASSIGNMENT'
         AND g.vt_transaction_table  = t.transaction_table
         and g.vt_source_assignment_id = pSource_assignment_id
         AND t.grouping_rule_id IS NOT NULL  -- 02.06.01
         and g.vt_grouping_rule_id is null;
  
    -- Find records with invalid Grouping rule 
    cursor er3(pSource_assignment_id in number) is
      select Distinct g.vt_transaction_table
        from XXCP_rt_interface g, XXCP_grouping_rules r, XXCP_sys_source_tables t
       where g.vt_request_id = cRequest_id
         and g.vt_status = 'ASSIGNMENT'
         AND g.vt_transaction_table  = t.transaction_table
         and g.vt_source_assignment_id = pSource_assignment_id
         and g.vt_grouping_rule_id = r.grouping_rule_id(+)
         AND t.grouping_rule_id IS NOT NULL  -- 02.06.01
         and r.grouping_rule_id is null;
  
    vCurrent_request_id    number;
    vGroupingRuleExists    BOOLEAN     := FALSE;
    vPrevTransaction_Table XXCP_sys_source_tables.transaction_table%TYPE := '~NULL~';
  
  begin
  
    vCurrent_Request_id := nvl(cRequest_id, 0);
  
    For rec in rx(cSource_id, cSource_assignment_id, vCurrent_Request_id, cTable_Group_id) Loop
      vGrouping_rule_id := Null;
      -- Populate Transaction_Table Array
      if vPrevTransaction_Table <> rec.vt_transaction_table then
        gTrans_Table_Array.extend;
        gTrans_Table_Array(gTrans_Table_Array.count) := rec.vt_transaction_table;
      End if;
      
      vPrevTransaction_Table := rec.vt_transaction_table;
			
   --   IF xxcp_global.gCommon(1).Grouping_Rule = 'Y' THEN
      IF Rec.grouping_rule_id > 0 then  -- 02.06.02

        vGroupingRuleExists := TRUE;
      
				xxcp_global.gCommon(1).current_transaction_table := Rec.vt_Transaction_Table;
        xxcp_global.gCommon(1).current_transaction_id    := Rec.vt_Transaction_id;

        vGrouping_rule_id := xxcp_custom_events.get_group_rule_id(cSource_id,
                                                                  cSource_assignment_id,
                                                                  rec.source_rowid,
                                                                  rec.vt_transaction_table,
                                                                  rec.vt_transaction_type);

        If nvl(vGrouping_rule_id, 0) = 0 then
          vGrouping_rule_id := xxcp_foundation.Fetch_Default_Group_Rule(cSource_assignment_id,
                                                                        rec.vt_transaction_table);
        End If;
      End If;
      -- Set Processing Status   
      begin
        update XXCP_rt_interface r
           set vt_status           = 'ASSIGNMENT',
               vt_date_processed   = xxcp_global.SystemDate,
               vt_status_code      = Null,
               vt_request_id       = cRequest_id,
               vt_Grouping_rule_id = vGrouping_rule_id
         where r.rowid = rec.source_rowid;
      
         Exception   when OTHERS then null;
      End;
    End Loop;
    -- Error Checking 
    IF vGroupingRuleExists then  -- 02.06.01 Don't Check If no Grouping Rule used.
      -- Find records with no vt_grouping_rule_id 
      For Rec in er2(cSource_assignment_id) loop
        update XXCP_rt_interface g
           set g.vt_status              = 'ERROR',
               g.vt_date_processed      = xxcp_global.SystemDate,
               g.vt_internal_error_code = 11088
         where g.vt_request_id = cRequest_id
           and g.vt_status     = 'ASSIGNMENT'
           and g.vt_source_assignment_id = cSource_assignment_id
           and g.vt_transaction_table = rec.vt_transaction_table;
      End Loop;
      -- Find records with invalid Grouping rule 
      For Rec in er3(cSource_assignment_id) loop
        update XXCP_rt_interface g
           set g.vt_status              = 'ERROR',
               g.vt_date_processed      = xxcp_global.SystemDate,
               g.vt_internal_error_code = 11089
         where g.vt_request_id           = cRequest_id
           and g.vt_status               = 'ASSIGNMENT'
           and g.vt_source_assignment_id = cSource_assignment_id
           and g.vt_transaction_table = rec.vt_transaction_table;
      End Loop;
    End If;
  
    Commit;
  
  End Set_Running_Status;

 
 --
 -- Investigate 
 --
 Function Investigate(cSource_Rowid in rowid, cInvestigate_Errors in varchar2) return number is
 
   cursor c1 (pSource_Rowid in rowid) is
   select f.vt_transaction_id, f.vt_transaction_table, f.vt_transaction_type, f.vt_transaction_class, f.vt_source_assignment_id,
          f.vt_parent_trx_id, f.vt_transaction_ref, f.vt_interface_id, f.vt_transaction_date
   from XXCP_rt_interface       f 
   where f.rowid  = pSource_Rowid;
     
   cursor c2 (pSource_Assignment_id in number, pTransaction_Table in varchar2) is
   select t.source_table_id
   from XXCP_sys_source_tables  t,
        XXCP_source_assignments s
   where t.source_id            = s.source_id
     and s.source_assignment_id = pSource_Assignment_id
     and t.transaction_table    = pTransaction_Table; 
 
  cursor c3 (pSource_table_id in number, pType in varchar2) is
   select c.transaction_set_id
   from XXCP_sys_source_types c,
        XXCP_sys_source_tables  t
   where c.source_table_id     = pSource_table_id
     and c.type                = pType;

  cursor c4 (pSource_table_id in number, pClass in varchar2) is
   select c.class
   from XXCP_sys_source_classes c,
        XXCP_sys_source_tables  t
   where c.source_table_id      = pSource_table_id
     and c.class                = pClass;
     
  cursor c5 (pSource_Assignment_id in number) is
   select 'Y' Found
   from XXCP_source_assignments c 
   where c.Source_Assignment_id = pSource_Assignment_id
     and c.Source_id            = 17;
     
   vMsg      XXCP_errors.error_message%type;
   vErrCode  XXCP_errors.internal_error_code%type := 0;
     
 Begin 
 
   If (cSource_Rowid is not Null) and (cInvestigate_Errors = 'Y') then 
  
     For Rec1 in c1(pSource_Rowid => cSource_Rowid ) loop
      
        XXCP_GLOBAL.GCOMMON(1).CURRENT_TRANSACTION_TABLE := REC1.VT_TRANSACTION_TABLE;
        XXCP_GLOBAL.GCOMMON(1).CURRENT_TRANSACTION_ID    := REC1.VT_TRANSACTION_ID;
        XXCP_GLOBAL.GCOMMON(1).CURRENT_PARENT_TRX_ID     := REC1.VT_PARENT_TRX_ID;
        XXCP_GLOBAL.GCOMMON(1).CURRENT_INTERFACE_ID      := REC1.VT_INTERFACE_ID; 
				XXCP_GLOBAL.GCOMMON(1).CURRENT_INTERFACE_REF     := REC1.VT_TRANSACTION_REF; 
             
        vErrCode := 201;
        vMsg     := 'Transaction Table <'||REC1.VT_TRANSACTION_TABLE||'>';
        
        For Rec2 in c2(Rec1.vt_source_assignment_id, rec1.vt_transaction_table) Loop
          vErrCode := 202;
          vMsg     := 'Transaction Type <'||Rec1.vt_transaction_type||'>';
          For Rec3 in c3(Rec2.Source_table_id, Rec1.vt_transaction_type) Loop
            vErrCode := 203;
            vMsg     := 'Transaction Class <'||Rec1.vt_transaction_class||'>';
            For Rec4 in c4(Rec2.Source_table_id, Rec1.vt_transaction_class) Loop 
              vErrCode := 0;
            End Loop;
            vErrCode := 209;
            vMsg     := 'Source Assignment <'||to_char(Rec1.vt_source_assignment_id)||'>';
            For Rec5 in c5(Rec1.vt_source_assignment_id) Loop
              vErrCode := 0;
            End Loop;
          End Loop;
        End Loop;  
        
        If vErrCode = 0 then         
          If nvl(REC1.VT_PARENT_TRX_ID,0) = 0 then
           vErrCode := 204;
           vMsg := 'Invalid Parent Trx Id';
          ElsIf nvl(Rec1.VT_TRANSACTION_ID,0) = 0 then
           vErrCode := 205;
           vMsg := 'Invalid Transaction Id';
          ElsIf Rec1.VT_TRANSACTION_DATE is null then
           vErrCode := 206;
           vMsg := 'Invalid Transaction Date';
          ElsIf Rec1.VT_TRANSACTION_REF is null then
           vErrCode := 207;
           vMsg := 'Invalid Transaction Ref';
          ElsIf Rec1.VT_SOURCE_ASSIGNMENT_ID is null then
           vErrCode := 208;
           vMsg := 'Invalid Source Assignment_id';
          End If;
        End If;
        
        If vErrCode != 0 then
          xxcp_foundation.FndWriteError(vErrCode,vMsg);
        End If;
     End Loop;
   End If; 
   
   Return(vErrCode);
   
 End Investigate;
  
  --
  -- InterfaceFailureCheck
  --
  Procedure InterfaceFailureCheck(cSource_Assignment_id  in number,
                                  cRequest_id            in number,
                                  cInvestigate_Errors    in varchar2) is
                                  
   Cursor FailureCheck(pRequest_id in number) is
      select  f.vt_status              
             ,f.vt_interface_id        
             ,f.vt_source_assignment_id
             ,f.vt_transaction_table   
             ,f.vt_transaction_type    
             ,f.vt_transaction_class   
             ,f.vt_transaction_ref     
             ,f.vt_transaction_id      
             ,f.vt_parent_trx_id       
             ,f.vt_transaction_date
             ,f.Rowid Source_Rowid
        from XXCP_rt_interface f
       where f.vt_request_id = pRequest_id 
         and NOT f.vt_status in ('ERROR','SUCCESSFUL') for Update;
             
     vInternalErrorCode XXCP_rt_interface.vt_internal_error_code%type;
             
   Begin
   
    
     For Rec in FailureCheck(pRequest_id => cRequest_id) Loop 
       xxcp_global.gCommon(1).current_transaction_table := Rec.vt_transaction_table;
       xxcp_global.gCommon(1).current_transaction_id    := Rec.vt_transaction_id;
			 xxcp_global.gcommon(1).current_interface_ref     := Rec.vt_transaction_ref; 
       xxcp_global.gCommon(1).current_parent_trx_id     := Rec.vt_parent_trx_id;
       xxcp_global.gCommon(1).current_source_activity   := 'PV';
       xxcp_global.gCommon(1).current_process_name      := 'Preview Engine';
       xxcp_global.gCommon(1).current_source_table_id   := 0;
       xxcp_global.gCommon(1).current_assignment_id     := cSource_Assignment_id;
       xxcp_global.gCommon(1).current_table_group_id    := 0;
       xxcp_global.gCommon(1).current_trx_group_id      := 0;
       xxcp_global.gCommon(1).current_source_rowid      := Rec.Source_Rowid;
       xxcp_global.gCommon(1).current_request_id        := cRequest_id; 

       vInternalErrorCode := Investigate(cSource_Rowid => Rec.Source_Rowid, cInvestigate_Errors => cInvestigate_Errors );
       
       If vInternalErrorCode = 0 then
         vInternalErrorCode := 299;
         xxcp_foundation.FndWriteError(vInternalErrorCode, 'Unable to Process Record. Invalid Data.');
       End If;     
                              
       update XXCP_rt_interface x
          set x.vt_status = 'ERROR',
              x.vt_internal_error_code = vInternalErrorCode
          where x.rowid = Rec.Source_Rowid;
     End Loop;
     
     Commit;
     
   End InterfaceFailureCheck;
   
  --  !! ***********************************************************************************************************
  --  !!
  --  !!                                    CONTROL
  --  !!                  External Procedure (Entry Point) to start the AR Engine.
  --  !!
  --  !! ***********************************************************************************************************
  Function Control(cSource_Assignment_Id   IN NUMBER,
                   cUnique_Request_Id      IN NUMBER,
									 cTable_Group_id         IN NUMBER   Default 0,
                   cRemove_Prior_Records   IN VARCHAR2 Default 'N',
                   cInvestigate_Errors     IN VARCHAR2 Default 'Y',
                   cIC_Price_Mandatory     IN VARCHAR2 Default 'N'
                   ) return number IS
                   
  type cfg_type is REF CURSOR;

  -- Record Type of required cp_rt_interface fields
  type cfg_map_rec is record( 
          vt_transaction_table        xxcp_rt_interface.vt_transaction_table%type
         ,vt_transaction_type         xxcp_rt_interface.vt_transaction_type%type
         ,vt_transaction_class        xxcp_rt_interface.vt_transaction_class%type
         ,vt_transaction_id           xxcp_rt_interface.vt_transaction_id%type
         ,vt_transaction_ref          xxcp_rt_interface.vt_transaction_ref%type
         ,vt_parent_trx_id            xxcp_rt_interface.vt_parent_trx_id%type
         ,vt_source_assignment_id     xxcp_rt_interface.vt_source_assignment_id%type
         ,vt_transaction_date         xxcp_rt_interface.vt_transaction_date%type
         ,vt_interface_id             xxcp_rt_interface.vt_interface_id%type
         ,set_of_books_id             xxcp_source_assignments.set_of_books_id%type
         ,source_rowid                Rowid
         ,currency_code               xxcp_rt_interface.currency_code%type
         ,entered_dr                  xxcp_rt_interface.entered_dr%type
         ,entered_cr                  xxcp_rt_interface.entered_cr%type
         ,source_instance_id          xxcp_source_assignments.instance_id%type
         ,source_assignment_id        xxcp_source_assignments.source_assignment_id%type
         ,source_id                   xxcp_source_assignments.source_id%type
         ,source_set_of_books_id      xxcp_source_assignments.set_of_books_id%type
         ,transaction_set_name        xxcp_source_transaction_sets.transaction_set_name%type
         ,transaction_set_id          xxcp_source_transaction_sets.transaction_set_id%type
         ,source_table_id             xxcp_sys_source_tables.source_table_id%type
         ,source_type_id              xxcp_sys_source_types.source_type_id%type
         ,source_class_id             xxcp_sys_source_classes.source_class_id%type
         ,assignment_only             xxcp_sys_source_tables.assignment_only%type
         ,calc_legal_exch_rate        xxcp_sys_source_tables.calc_legal_exch_rate%type
         );

    -- Assignment Cursor      
   cfg     cfg_type;
   cfgRec  cfg_map_rec;
                   
 
   
    -- Assignments
    Cursor SR1(pSource_Assignment_id in number) is
      select set_of_books_id,
             Source_Assignment_id,
             instance_id source_instance_id,
             Stamp_Parent_Trx,
             Source_id
        from XXCP_source_assignments g
       where g.source_assignment_id = pSource_assignment_id
         and g.active = 'Y';
  
    vInternalErrorCode        XXCP_errors.internal_error_code%type := 0;
    vExchange_Rate_Type       XXCP_tax_registrations.EXCHANGE_RATE_TYPE%type;
    vCommon_Exch_Curr         XXCP_tax_registrations.COMMON_EXCH_CURR%type;

    vGlobalPrecision          pls_integer := 2; 
    vSource_Activity          varchar2(4) := 'RT';
    vJob_Status               Number(1)   := 0;
  
    vTransaction_Class        XXCP_sys_source_classes.Class%type;
    vRequest_ID               XXCP_process_history.request_id%type;
  
    -- NEW PARAMETERS
    vCurrent_Parent_Trx_id    XXCP_transaction_attributes.parent_trx_id%type := 0;
    i                         pls_integer := 0;  
    g                         number;
    w                         number := 0;
	 
    vSource_Assignment_id XXCP_source_assignments.source_assignment_id%type;
    vStamp_Parent_Trx     XXCP_source_assignments.stamp_parent_trx%type := 'N';
    vSource_instance_id   XXCP_source_assignments.instance_id%type;
    vSource_ID            XXCP_sys_sources.source_id%type; 
  
    Cursor SF(pSource_Assignment_id in number) is
      select s.source_activity,
             s.Source_id,
             s.Preview_ctl,
             s.Timing,
             s.Custom_events,
             s.Cached, 
						 a.Source_Group_Id,
             nvl(s.timing_advanced_mode,'N') Timing_Adv_mode
        from XXCP_source_assignments a,
				     XXCP_sys_sources s
       where a.source_assignment_id = pSource_Assignment_id
			   and a.source_id            = s.source_id;
   
    vLastICPrice            XXCP_rt_interface.vt_ic_price%type;
    vLastICCurrency         XXCP_rt_interface.vt_ic_currency_code%type;
    vLastICPMID             XXCP_rt_interface.vt_ic_pricing_method_id%type;
    vLastDftPMused          varchar2(1) := 'N';
  
  BEGIN
  
    xxcp_global.Trace_on   := nvl(gEngine_Trace,'N');
    xxcp_global.Preview_on := 'N';
    xxcp_global.SystemDate := Sysdate;

		vTimingCnt := 1;
		xxcp_global.gCommon(1).Unique_Request_Id := cUnique_Request_Id;

    -- Source Information
    For SFRec in SF(cSource_Assignment_id) loop
      xxcp_global.gCommon(1).Source_id     := SFRec.Source_id;
      xxcp_global.gCommon(1).Preview_ctl   := SFRec.Preview_ctl;
      xxcp_global.gCommon(1).Preview_on    := xxcp_global.Preview_on;
      xxcp_global.gCommon(1).Custom_events := SFRec.Custom_events;
      xxcp_global.gCommon(1).current_Source_activity := SFRec.Source_Activity;
      xxcp_global.gCommon(1).Cached_Attributes := SFRec.Cached;
      xxcp_global.gCommon(1).Grouping_Rule := vStamp_Parent_Trx; 
      vSource_Activity := SFRec.Source_Activity;
      vSource_id       := SFRec.Source_id;
			gSource_Group_id := SFRec.Source_group_id;       
      xxcp_global.Timing_Adv_Mode := SFRec.Timing_Adv_mode;
    End Loop;
    
    xxcp_global.SET_SOURCE_ID(cSource_id => vSource_id);
    xxcp_global.gCommon(1).current_process_name  := 'XXCP_RTENG';  
    --     SYS.DBMS_SUPPORT.START_TRACE( waits=>true, binds=>true );
  
    If xxcp_global.gCommon(1).Custom_events = 'Y' then
      xxcp_custom_events.Set_system_mode(xxcp_global.gCommon);
    End If;
  
    --
    -- ## *******************************************************************************************************
    -- ##
    -- ##                            Check to See the process is already in use
    -- ##
    -- ## *******************************************************************************************************
    --

    If cUnique_Request_Id > 0 then

      vRequest_id := cUnique_Request_Id;

      xxcp_global.gCommon(1).Current_request_id := vRequest_id;
    
      If xxcp_global.gCommon(1).Custom_events = 'Y' then
        vInternalErrorCode := xxcp_custom_events.BEFORE_PROCESSING(vSource_id);
      End If;
      --
      -- ## **************************************************************************************************
      -- ##
      -- ##                                Setup Ready for the Run
      -- ##
      -- ## **************************************************************************************************
      --
      For REC in sr1(cSource_Assignment_id) Loop -- Once only
      
        Exit when vJob_Status <> 0;
      
        vSource_Instance_id   := rec.source_instance_id;
        vSource_Assignment_id := rec.Source_Assignment_id;
        --vStamp_Parent_Trx     := rec.Stamp_Parent_Trx;
     
			  --
        -- Startup Only 
        --
			  If gPackage_Called = 'N' then
           Assignment_Engine_Init(  cSource_id         => vSource_id,
				                            cSource_Group_id   => gSource_Group_id,
																    cInternalErrorCode => vInternalErrorCode);
	
          --
          -- Get Standard Parameters
          --
					vInternalErrorCode := xxcp_foundation.fnd_gl_lookup(cExchange_Rate_Type => vExchange_Rate_Type,
                                                              cGlobalPrecision    => vGlobalPrecision,
                                                              cCommon_Exch_Curr   => vCommon_Exch_Curr);
     	  End If;
																			 
        -- Setup Globals
        xxcp_global.gCommon(1).current_assignment_id := vSource_Assignment_ID;

        -- 02.06.01
        gTrans_Table_Array.delete;

        --
        -- *********************************************************************************************************
        --                            Populate Column Rules and Initialize Cursors
        -- *********************************************************************************************************
        --

    	  xxcp_te_base.init_Cursors(cTarget_Table => 'XXCP_RT_INTERFACE');

        --
        -- *******************************************************************************************************
        --     Reset Previosly Errored Transactions and Assign Parent Trx Id to Incomming transactions
        -- *******************************************************************************************************
        --																	 
        Set_Running_Status(        cSource_id            => vSource_id, 
				                           cSource_assignment_id => vSource_Assignment_id, 
													         cRequest_id           => vRequest_id,
																	 cTable_Group_id       => cTable_Group_id);
											
        Transaction_Group_Stamp(  cStamp_Parent_Trx     => vStamp_Parent_Trx, 
                                  cSource_Assignment_id => vSource_Assignment_id, 
																	cTable_Group_id       => cTable_Group_id,
																  cInternalErrorCode    => vInternalErrorCode);
      
        Commit;

        If vInternalErrorCode = 0 then
        
          --
          -- ## ***********************************************************************************************************
          -- ##
          -- ##                                Assignment 
          -- ##
          -- ## ***********************************************************************************************************
          --
          i := 0;
        
          xxcp_global.SystemDate := sysdate;
          vCurrent_Parent_Trx_id := 0;
          
          Open CFG for gDDL_Interface_Select
                   Using vRequest_id, vSource_Assignment_id, vSource_id,  cTable_Group_id;
          Loop

                Fetch CFG into CFGRec;
                Exit when CFG%notfound;

                i := i + 1;
              
                xxcp_global.gCommon(1).current_source_rowid      := CFGRec.Source_Rowid;
                xxcp_global.gCommon(1).current_transaction_date  := CFGRec.vt_Transaction_Date;
                xxcp_global.gCommon(1).current_transaction_table := CFGRec.vt_Transaction_Table;
                xxcp_global.gCommon(1).current_transaction_id    := CFGRec.vt_Transaction_id;
                xxcp_global.gCommon(1).calc_legal_exch_rate      := CFGRec.calc_legal_exch_rate;
			          xxcp_global.gcommon(1).current_interface_ref     := CFGRec.vt_Transaction_Ref;
                xxcp_global.gCommon(1).current_parent_trx_id     := CFGRec.vt_Parent_Trx_id;
                xxcp_global.gCommon(1).current_Interface_id      := CFGRec.vt_interface_id;
                xxcp_global.gCommon(1).preview_parent_trx_id     := Null;
                xxcp_global.gCommon(1).preview_source_rowid      := Null;
              
                vTransaction_Class := CFGRec.VT_Transaction_Class;
              
                If 1=1 Then
 
                  -- Transaction Cache is not required
                  xxcp_global.gCommon(1).Cached_Attributes := 'N';
                
                  -- Replan 
                  If cRemove_Prior_Records = 'Y' then
                   UnFreeze_Attribute_Record(
                          CFGRec.vt_Source_Assignment_id, 
                          CFGRec.source_Table_id, 
                          CFGRec.vt_parent_trx_id,
                          CFGRec.vt_transaction_id, 
                          CFGRec.source_type_id, 
                          xxcp_global.gCommon(1).Cached_Attributes );
                  End If;
                  
                  xxcp_te_configurator.Clear_Last_IC_Values;   -- Cisco 
                  xxcp_wks.gRealTime_Results := xxcp_wks.gEmpty_RealTime_Results; -- Cisco 
                  
                  vCurrent_Parent_Trx_id := CFGRec.VT_Parent_Trx_id;         
                
                  XXCP_TE_CONFIGURATOR.Control(
                                               cSource_Assignment_ID     => vSource_assignment_id,
                                               cSet_of_books_id          => CFGRec.Source_Set_of_books_id,
																							 cTransaction_Date         => CFGRec.VT_Transaction_Date,
																							 cSource_id                => vSource_id,
                                               cSource_Table_id          => CFGRec.Source_table_id,
                                               cSource_Type_id           => CFGRec.Source_type_id,
																							 cSource_Class_id          => CFGRec.Source_Class_id,
                                               cTransaction_id           => CFGRec.VT_Transaction_ID,
                                               cSourceRowid              => CFGRec.Source_Rowid,
                                               cParent_Trx_id            => CFGRec.VT_Parent_Trx_id,
                                               cTransaction_Set_id       => CFGRec.Transaction_Set_id,
                                               cSpecial_Array            => 'N', 
                                               cKey                      => Null, 
																							 cTransaction_Table        => CFGRec.VT_Transaction_Table,
																							 cTransaction_Type         => CFGRec.VT_Transaction_Type,
                                               cTransaction_Class        => vTransaction_Class,
                                               cInternalErrorCode        => vInternalErrorCode);
 
                  -- Check Status
                  If vInternalErrorCode = -1 then                  
                    Update XXCP_rt_interface
                       set vt_status              = 'SUCCESSFUL',
                           vt_internal_error_code = Null,
                           vt_date_processed      = xxcp_global.SystemDate,
                           vt_status_code         = 7001,
                           -- 02.05.09
                           vt_diagnostics1        = xxcp_global.Add_Diagnostics(null,1),
                           vt_diagnostics2        = xxcp_global.Add_Diagnostics(null,2)
                     where rowid = CFGRec.Source_Rowid;
                  ElsIf nvl(vInternalErrorCode, 0) <> 0 then 

                    Rollback;
                    
                    Error_Whole_Request(cInternalErrorCode => vInternalErrorCode);

                    Update XXCP_rt_interface
                       set vt_status              = 'ERROR',
                           vt_internal_error_code = vInternalErrorCode,
                           vt_date_processed      = xxcp_global.SystemDate,
                           vt_results_attribute1   = xxcp_wks.gRealTime_Results.Attribute1, -- Cisco
                           vt_results_attribute2   = xxcp_wks.gRealTime_Results.Attribute2,
                           vt_results_attribute3   = xxcp_wks.gRealTime_Results.Attribute3,
                           vt_results_attribute4   = xxcp_wks.gRealTime_Results.Attribute4,
                           vt_results_attribute5   = xxcp_wks.gRealTime_Results.Attribute5,
                           vt_results_attribute6   = xxcp_wks.gRealTime_Results.Attribute6,
                           vt_results_attribute7   = xxcp_wks.gRealTime_Results.Attribute7,
                           vt_results_attribute8   = xxcp_wks.gRealTime_Results.Attribute8,
                           vt_results_attribute9   = xxcp_wks.gRealTime_Results.Attribute9,
                           vt_results_attribute10  = xxcp_wks.gRealTime_Results.Attribute10,
                           -- 10.03.09 NCM
                           vt_results_attribute11   = xxcp_wks.gRealTime_Results.Attribute11, -- Cisco
                           vt_results_attribute12   = xxcp_wks.gRealTime_Results.Attribute12,
                           vt_results_attribute13   = xxcp_wks.gRealTime_Results.Attribute13,
                           vt_results_attribute14   = xxcp_wks.gRealTime_Results.Attribute14,
                           vt_results_attribute15   = xxcp_wks.gRealTime_Results.Attribute15,
                           vt_results_attribute16   = xxcp_wks.gRealTime_Results.Attribute16,
                           vt_results_attribute17   = xxcp_wks.gRealTime_Results.Attribute17,
                           vt_results_attribute18   = xxcp_wks.gRealTime_Results.Attribute18,
                           vt_results_attribute19   = xxcp_wks.gRealTime_Results.Attribute19,
                           vt_results_attribute20   = xxcp_wks.gRealTime_Results.Attribute20,
                           vt_results_attribute21   = xxcp_wks.gRealTime_Results.Attribute21, -- Cisco
                           vt_results_attribute22   = xxcp_wks.gRealTime_Results.Attribute22,
                           vt_results_attribute23   = xxcp_wks.gRealTime_Results.Attribute23,
                           vt_results_attribute24   = xxcp_wks.gRealTime_Results.Attribute24,
                           vt_results_attribute25   = xxcp_wks.gRealTime_Results.Attribute25,
                           vt_results_attribute26   = xxcp_wks.gRealTime_Results.Attribute26,
                           vt_results_attribute27   = xxcp_wks.gRealTime_Results.Attribute27,
                           vt_results_attribute28   = xxcp_wks.gRealTime_Results.Attribute28,
                           vt_results_attribute29   = xxcp_wks.gRealTime_Results.Attribute29,
                           vt_results_attribute30   = xxcp_wks.gRealTime_Results.Attribute30,
                           -- 02.06.02
                           vt_results_attribute31   = xxcp_wks.gRealTime_Results.Attribute31,
                           vt_results_attribute32   = xxcp_wks.gRealTime_Results.Attribute32,
                           vt_results_attribute33   = xxcp_wks.gRealTime_Results.Attribute33,
                           vt_results_attribute34   = xxcp_wks.gRealTime_Results.Attribute34,
                           vt_results_attribute35   = xxcp_wks.gRealTime_Results.Attribute35,
                           vt_results_attribute36   = xxcp_wks.gRealTime_Results.Attribute36,
                           vt_results_attribute37   = xxcp_wks.gRealTime_Results.Attribute37,
                           vt_results_attribute38   = xxcp_wks.gRealTime_Results.Attribute38,
                           vt_results_attribute39   = xxcp_wks.gRealTime_Results.Attribute39,
                           vt_results_attribute40   = xxcp_wks.gRealTime_Results.Attribute40,
                           vt_results_attribute41   = xxcp_wks.gRealTime_Results.Attribute41,
                           vt_results_attribute42   = xxcp_wks.gRealTime_Results.Attribute42,
                           vt_results_attribute43   = xxcp_wks.gRealTime_Results.Attribute43,
                           vt_results_attribute44   = xxcp_wks.gRealTime_Results.Attribute44,
                           vt_results_attribute45   = xxcp_wks.gRealTime_Results.Attribute45,
                           vt_results_attribute46   = xxcp_wks.gRealTime_Results.Attribute46,
                           vt_results_attribute47   = xxcp_wks.gRealTime_Results.Attribute47,
                           vt_results_attribute48   = xxcp_wks.gRealTime_Results.Attribute48,
                           vt_results_attribute49   = xxcp_wks.gRealTime_Results.Attribute49,
                           vt_results_attribute50   = xxcp_wks.gRealTime_Results.Attribute50,                           
                           -- 02.05.09
                           vt_diagnostics1        = xxcp_global.Add_Diagnostics(null,1),
                           vt_diagnostics2        = xxcp_global.Add_Diagnostics(null,2)
                      where rowid = CFGRec.Source_Rowid;
                      
                      
                      If gErrorWholeRequest = 'Y' then
                        Exit;
                      End If;

                  ElsIf nvl(vInternalErrorCode, 0) = 0 then

                       xxcp_te_configurator.Get_Last_IC_Values(cLastICPrice    => vLastICPrice,
                                                               cLastICCurrency => vLastICCurrency,
                                                               cLastICPMID     => vLastICPMID,
                                                               cLastDftPMused  => vLastDftPMused
                                                               );

                       If nvl(vLastICPrice,0) = 0 And cIC_Price_Mandatory = 'Y' Then

                         Rollback;
                       
                         Error_Whole_Request(cInternalErrorCode => vInternalErrorCode);

                         Update XXCP_rt_interface
                            set vt_status               = 'ERROR',
                                vt_internal_error_code  = 12055,
                                vt_date_processed       = xxcp_global.SystemDate,
                                vt_results_attribute1   = xxcp_wks.gRealTime_Results.Attribute1, -- Cisco
                                vt_results_attribute2   = xxcp_wks.gRealTime_Results.Attribute2,
                                vt_results_attribute3   = xxcp_wks.gRealTime_Results.Attribute3,
                                vt_results_attribute4   = xxcp_wks.gRealTime_Results.Attribute4,
                                vt_results_attribute5   = xxcp_wks.gRealTime_Results.Attribute5,
                                vt_results_attribute6   = xxcp_wks.gRealTime_Results.Attribute6,
                                vt_results_attribute7   = xxcp_wks.gRealTime_Results.Attribute7,
                                vt_results_attribute8   = xxcp_wks.gRealTime_Results.Attribute8,
                                vt_results_attribute9   = xxcp_wks.gRealTime_Results.Attribute9,
                                vt_results_attribute10  = xxcp_wks.gRealTime_Results.Attribute10,
                                 -- 10.03.09 NCM
                                 vt_results_attribute11   = xxcp_wks.gRealTime_Results.Attribute11, -- Cisco
                                 vt_results_attribute12   = xxcp_wks.gRealTime_Results.Attribute12,
                                 vt_results_attribute13   = xxcp_wks.gRealTime_Results.Attribute13,
                                 vt_results_attribute14   = xxcp_wks.gRealTime_Results.Attribute14,
                                 vt_results_attribute15   = xxcp_wks.gRealTime_Results.Attribute15,
                                 vt_results_attribute16   = xxcp_wks.gRealTime_Results.Attribute16,
                                 vt_results_attribute17   = xxcp_wks.gRealTime_Results.Attribute17,
                                 vt_results_attribute18   = xxcp_wks.gRealTime_Results.Attribute18,
                                 vt_results_attribute19   = xxcp_wks.gRealTime_Results.Attribute19,
                                 vt_results_attribute20   = xxcp_wks.gRealTime_Results.Attribute20,
                                 vt_results_attribute21   = xxcp_wks.gRealTime_Results.Attribute21, -- Cisco
                                 vt_results_attribute22   = xxcp_wks.gRealTime_Results.Attribute22,
                                 vt_results_attribute23   = xxcp_wks.gRealTime_Results.Attribute23,
                                 vt_results_attribute24   = xxcp_wks.gRealTime_Results.Attribute24,
                                 vt_results_attribute25   = xxcp_wks.gRealTime_Results.Attribute25,
                                 vt_results_attribute26   = xxcp_wks.gRealTime_Results.Attribute26,
                                 vt_results_attribute27   = xxcp_wks.gRealTime_Results.Attribute27,
                                 vt_results_attribute28   = xxcp_wks.gRealTime_Results.Attribute28,
                                 vt_results_attribute29   = xxcp_wks.gRealTime_Results.Attribute29,
                                 vt_results_attribute30   = xxcp_wks.gRealTime_Results.Attribute30,
                                 -- 02.06.02
                                 vt_results_attribute31   = xxcp_wks.gRealTime_Results.Attribute31,
                                 vt_results_attribute32   = xxcp_wks.gRealTime_Results.Attribute32,
                                 vt_results_attribute33   = xxcp_wks.gRealTime_Results.Attribute33,
                                 vt_results_attribute34   = xxcp_wks.gRealTime_Results.Attribute34,
                                 vt_results_attribute35   = xxcp_wks.gRealTime_Results.Attribute35,
                                 vt_results_attribute36   = xxcp_wks.gRealTime_Results.Attribute36,
                                 vt_results_attribute37   = xxcp_wks.gRealTime_Results.Attribute37,
                                 vt_results_attribute38   = xxcp_wks.gRealTime_Results.Attribute38,
                                 vt_results_attribute39   = xxcp_wks.gRealTime_Results.Attribute39,
                                 vt_results_attribute40   = xxcp_wks.gRealTime_Results.Attribute40,
                                 vt_results_attribute41   = xxcp_wks.gRealTime_Results.Attribute41,
                                 vt_results_attribute42   = xxcp_wks.gRealTime_Results.Attribute42,
                                 vt_results_attribute43   = xxcp_wks.gRealTime_Results.Attribute43,
                                 vt_results_attribute44   = xxcp_wks.gRealTime_Results.Attribute44,
                                 vt_results_attribute45   = xxcp_wks.gRealTime_Results.Attribute45,
                                 vt_results_attribute46   = xxcp_wks.gRealTime_Results.Attribute46,
                                 vt_results_attribute47   = xxcp_wks.gRealTime_Results.Attribute47,
                                 vt_results_attribute48   = xxcp_wks.gRealTime_Results.Attribute48,
                                 vt_results_attribute49   = xxcp_wks.gRealTime_Results.Attribute49,
                                 vt_results_attribute50   = xxcp_wks.gRealTime_Results.Attribute50,                           
                                 -- 02.05.09
                                 vt_diagnostics1        = xxcp_global.Add_Diagnostics(null,1),
                                 vt_diagnostics2        = xxcp_global.Add_Diagnostics(null,2)
                          where rowid = CFGRec.Source_Rowid;
                          
                         If gErrorWholeRequest = 'Y' then
                           Exit;
                         End If;


                       Else
                         Update XXCP_rt_interface
                         set vt_status             = Decode(CFGRec.Assignment_only,'Y','SUCCESSFUL','TRANSACTION'),
                           vt_internal_error_code  = Null,
                           vt_date_processed       = xxcp_global.SystemDate,
                           vt_ic_price             = nvl(vLastICPrice,0),
                           vt_ic_currency_code     = vLastICCurrency,
                           vt_ic_pricing_method_id = vLastICPMID,
                           vt_status_code          = Decode(vLastDftPMused,'Y',7550,Null),
                           vt_results_attribute1   = xxcp_wks.gRealTime_Results.Attribute1, -- Cisco
                           vt_results_attribute2   = xxcp_wks.gRealTime_Results.Attribute2,
                           vt_results_attribute3   = xxcp_wks.gRealTime_Results.Attribute3,
                           vt_results_attribute4   = xxcp_wks.gRealTime_Results.Attribute4,
                           vt_results_attribute5   = xxcp_wks.gRealTime_Results.Attribute5,
                           vt_results_attribute6   = xxcp_wks.gRealTime_Results.Attribute6,
                           vt_results_attribute7   = xxcp_wks.gRealTime_Results.Attribute7,
                           vt_results_attribute8   = xxcp_wks.gRealTime_Results.Attribute8,
                           vt_results_attribute9   = xxcp_wks.gRealTime_Results.Attribute9,
                           vt_results_attribute10  = xxcp_wks.gRealTime_Results.Attribute10,
                           -- 10.03.09 NCM
                           vt_results_attribute11   = xxcp_wks.gRealTime_Results.Attribute11, -- Cisco
                           vt_results_attribute12   = xxcp_wks.gRealTime_Results.Attribute12,
                           vt_results_attribute13   = xxcp_wks.gRealTime_Results.Attribute13,
                           vt_results_attribute14   = xxcp_wks.gRealTime_Results.Attribute14,
                           vt_results_attribute15   = xxcp_wks.gRealTime_Results.Attribute15,
                           vt_results_attribute16   = xxcp_wks.gRealTime_Results.Attribute16,
                           vt_results_attribute17   = xxcp_wks.gRealTime_Results.Attribute17,
                           vt_results_attribute18   = xxcp_wks.gRealTime_Results.Attribute18,
                           vt_results_attribute19   = xxcp_wks.gRealTime_Results.Attribute19,
                           vt_results_attribute20   = xxcp_wks.gRealTime_Results.Attribute20,
                           vt_results_attribute21   = xxcp_wks.gRealTime_Results.Attribute21, -- Cisco
                           vt_results_attribute22   = xxcp_wks.gRealTime_Results.Attribute22,
                           vt_results_attribute23   = xxcp_wks.gRealTime_Results.Attribute23,
                           vt_results_attribute24   = xxcp_wks.gRealTime_Results.Attribute24,
                           vt_results_attribute25   = xxcp_wks.gRealTime_Results.Attribute25,
                           vt_results_attribute26   = xxcp_wks.gRealTime_Results.Attribute26,
                           vt_results_attribute27   = xxcp_wks.gRealTime_Results.Attribute27,
                           vt_results_attribute28   = xxcp_wks.gRealTime_Results.Attribute28,
                           vt_results_attribute29   = xxcp_wks.gRealTime_Results.Attribute29,
                           vt_results_attribute30   = xxcp_wks.gRealTime_Results.Attribute30,
                           -- 02.06.02
                           vt_results_attribute31   = xxcp_wks.gRealTime_Results.Attribute31,
                           vt_results_attribute32   = xxcp_wks.gRealTime_Results.Attribute32,
                           vt_results_attribute33   = xxcp_wks.gRealTime_Results.Attribute33,
                           vt_results_attribute34   = xxcp_wks.gRealTime_Results.Attribute34,
                           vt_results_attribute35   = xxcp_wks.gRealTime_Results.Attribute35,
                           vt_results_attribute36   = xxcp_wks.gRealTime_Results.Attribute36,
                           vt_results_attribute37   = xxcp_wks.gRealTime_Results.Attribute37,
                           vt_results_attribute38   = xxcp_wks.gRealTime_Results.Attribute38,
                           vt_results_attribute39   = xxcp_wks.gRealTime_Results.Attribute39,
                           vt_results_attribute40   = xxcp_wks.gRealTime_Results.Attribute40,
                           vt_results_attribute41   = xxcp_wks.gRealTime_Results.Attribute41,
                           vt_results_attribute42   = xxcp_wks.gRealTime_Results.Attribute42,
                           vt_results_attribute43   = xxcp_wks.gRealTime_Results.Attribute43,
                           vt_results_attribute44   = xxcp_wks.gRealTime_Results.Attribute44,
                           vt_results_attribute45   = xxcp_wks.gRealTime_Results.Attribute45,
                           vt_results_attribute46   = xxcp_wks.gRealTime_Results.Attribute46,
                           vt_results_attribute47   = xxcp_wks.gRealTime_Results.Attribute47,
                           vt_results_attribute48   = xxcp_wks.gRealTime_Results.Attribute48,
                           vt_results_attribute49   = xxcp_wks.gRealTime_Results.Attribute49,
                           vt_results_attribute50   = xxcp_wks.gRealTime_Results.Attribute50,                                                    
                           -- 02.05.09
                           vt_diagnostics1        = xxcp_global.Add_Diagnostics(null,1),
                           vt_diagnostics2        = xxcp_global.Add_Diagnostics(null,2)
                         where rowid = CFGRec.Source_Rowid;
                       End If;
                  
                  End If;

									-- Emergency Exit (out of disk space) 
									If vInternalErrorCode between 3550 and 3559 then
									  Rollback;
										vJob_Status := 6;
										Exit; 
									End If;								
								
                End If; -- End configuration
              
                -- ##
                -- ## Commit Configured rows
                -- ##
              
                If i >= 8000 then
                  xxcp_global.SystemDate    := Sysdate;
                  Commit;
                  i := 0;
                End If;
    			
    		  END Loop;
          Close CFG;
          -- #
          -- # Clear Common vars
          -- #
          xxcp_global.gCommon(1).current_transaction_table := Null;
          xxcp_global.gCommon(1).current_transaction_id    := Null;
			    xxcp_global.gcommon(1).current_interface_ref     := Null; 
          xxcp_global.gCommon(1).current_parent_trx_id     := Null;
        
          Commit; -- Final Configuration Commit.
        
          --
          If xxcp_global.gCommon(1).Custom_events = 'Y' then
            xxcp_custom_events.AFTER_ASSIGNMENT_PROCESSING(cSource_id => vSource_id, cSource_assignment_id => vSource_assignment_id);
          End If;       

          vInternalErrorCode := 0;         
          
			    End If; -- Assignment Only
		 
        xxcp_global.SystemDate := Sysdate;
        -- *******************************************************************
        -- Error out records remaining with a Processing status after the run 
        -- ******************************************************************* 
      

        --
        -- ***********************************************************************************************************
        --                     Clear Down
        -- ***********************************************************************************************************
        --
        ClearWorkingStorage;
        --
        -- ***********************************************************************************************************
        --                     Add Alert to Queue
        -- ***********************************************************************************************************
        xxcp_te_base.Add_Alert_to_Queue (cSource_id            => vSource_id,
                                         cSource_Group_id      => 0,
                                         cSource_Assignment_id => cSource_Assignment_id,
                                         cRequest_id           => xxcp_global.gCommon(1).Current_Request_id);
        --
        -- ***********************************************************************************************************
        --                     DeActive the Lock in XXCP_ACTIVITY_CONTROL
        -- ***********************************************************************************************************
        --
        Commit;
      End loop; -- SR1
    
      xxcp_te_base.ClearDown;
      xxcp_memory_pack.ClearDown;
      xxcp_dynamic_sql.Close_Session;
     
      If xxcp_global.gCommon(1).Custom_events = 'Y' then
        xxcp_custom_events.after_processing(cSource_id => vSource_id, cSource_Group_id => gSource_Group_id);
      End If;
      Commit;

    End If; 
  
 
 
    --
    -- ***********************************************************************************************************
    --                     Return Job Status Code and Finish!!
    -- ***********************************************************************************************************
    --
    Return(vJob_Status);
  
  End Control;
  
  --
  -- Init
  --  
  Procedure Init is
  
   Cursor pf2(pProfile_Name in varchar2, pCategory in varchar2) is
      select Profile_Value
        from XXCP_SYS_PROFILE 
       where profile_category = pCategory
         and profile_name = pProfile_Name;
    
   Begin
      -- User Details      
      xxcp_global.User_id  := fnd_global.user_id;
      xxcp_global.Login_id := fnd_global.login_id;
      -- Trace Mode
      Begin
        Select Substr(Lookup_code, 1, 1)
          into gEngine_Trace
          from xxcp_lookups
         where lookup_type = 'TRACE RT ENGINE'
           and enabled_flag = 'Y';
   
        Exception when OTHERS then gEngine_Trace := 'N';
      End;
      
      For Rec in pf2('REAL TIME ERROR WHOLE REQUEST','EV') Loop
        gErrorWholeRequest := rec.profile_value;
      End Loop;
      
      gDDL_Interface_Order_By := 'int.vt_transaction_id ';
      For Rec in pf2('INTERFACE ADDITIONAL ORDER BY','RT') Loop
        gDDL_Interface_Order_By := rec.profile_value;
      End Loop;

     gDDL_Interface_Select  := 
              'Select   int.vt_transaction_table
                       ,int.vt_transaction_type
                       ,int.vt_transaction_class
                       ,int.vt_transaction_id
											 ,int.vt_transaction_ref
                       ,int.vt_parent_trx_id
                       ,int.vt_source_assignment_id
											 ,int.vt_transaction_date 
                       ,int.vt_interface_id
                       ,s.set_of_books_id
                       ,int.rowid source_rowid
                       ,int.currency_code
                       ,int.entered_dr
                       ,int.entered_cr
                       ,s.instance_id source_instance_id
                       ,s.source_assignment_id
                       ,s.source_id
                       ,s.set_of_books_id source_set_of_books_id
                       ,w.transaction_set_name
                       ,nvl(w.transaction_set_id,0) transaction_set_id
											 ,y.source_table_id
											 ,y.source_type_id
											 ,cx.source_class_id
											 ,t.assignment_only
                       ,t.calc_legal_exch_rate
                    from xxcp_rt_interface          int,
                         xxcp_source_assignments      s,
                         xxcp_sys_source_tables       t,
                         xxcp_sys_source_types        y,
												 xxcp_sys_source_classes      cx,
                         xxcp_source_transaction_sets w
                   where int.vt_request_id             = :1
									   and int.vt_status                 = ''ASSIGNMENT''
                     and int.vt_source_assignment_id   = :2
                     and int.vt_source_assignment_id   = s.source_assignment_id
                     and int.vt_transaction_table      = t.transaction_table
                     and t.source_id            = :3
				             and t.table_group_id       = :4
                     and int.vt_transaction_type  = y.type
									   and y.latch_only           = ''N''
                     and y.source_table_id      = t.source_table_id
                     and int.vt_transaction_class = cx.class
									   and cx.latch_only          = ''N''
                     and cx.source_table_id     = t.source_table_id
                     and y.Transaction_Set_id   = w.Transaction_Set_id
                order by int.vt_transaction_table, 
                         w.sequence, 
                         int.vt_parent_trx_id';
                         
       If gDDL_Interface_Order_By is not null then
         gDDL_Interface_Select := gDDL_Interface_Select || ','||chr(10)||gDDL_Interface_Order_By;
       End If;                  

   End Init; 
 
 
 Begin

  Init; 
  
END XXCP_RT_ENGINE;
/
