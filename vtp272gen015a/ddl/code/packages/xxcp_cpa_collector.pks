create or replace package XXCP_CPA_COLLECTOR is

  -- Author  : SIMON SWALLOW
  -- Created : 04/12/2008 15:10:15
  -- Purpose :

  -- Version [03.06.02] Build Date [12-DEC-2012] Name [XXCP_CPA_COLLECTOR]

  Function Software_Version RETURN VARCHAR2;

  -- Public function and procedure declarations
  Function Control(cSource_Group_ID          IN NUMBER,
                   cCalendar_id              IN NUMBER,
                   cForecast_Period_Name     IN VARCHAR2,
                   cConc_Request_id          IN NUMBER,
                   cCurrent_Period_Only      IN VARCHAR2 default 'N',
                   cCost_Plus_Set_Id         IN NUMBER default null) return Number;

end XXCP_CPA_COLLECTOR;
/
