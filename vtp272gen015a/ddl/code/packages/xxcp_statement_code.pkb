CREATE OR REPLACE PACKAGE BODY XXCP_STATEMENT_CODE AS
  /*****************************************************************************************
                          V I R T A L  T R A D E R  L I M I T E D
             (Copyright 2005-2012 Virtual Trader Ltd. All rights Reserved)

     NAME:       XXCP_STATEMENT_CODE
     PURPOSE:    Statement Code Generator. Creates the DML Insert/Update packages

     REVISIONS:
     Ver        Date      Author  Description
     ---------  --------  ------- ------------------------------------
     02.02.00   21/02/05  Keith   First Release
     02.03.00   21/06/06  Keith   Added Realtime
     02.03.01   06/11/06  Keith   Added Validate_Column_Rules to check
                                  that extened and none extended Dynamic Attributes
                                  are being mixed.
     02.03.02   31/07/07  Keith   Changed TG cursor in Preview history DML
     02.03.03   28/01/08  Keith   Added 3 new sources.
     02.03.04   04/12/08  Simon   Added 1 new source - Cost Plus.
     02.03.05   12/12/08  Simon   Added Forecast to gPN.
     02.03.06   05/05/09  Simon   Fixed Target Table hard coding.
     02.03.07   01/07/10  Keith   Added Get_Package_Names for external use (column rules)
     02.03.08   28/02/11  Simon   Added POH Source (36)
     02.03.09   07/07/11  Keith   Added SO Source (38)
     02.03.10   17/07/11  Keith   Moved Package Names into new table to be read to save this 
                                  package from being modified for each new source
     02.03.11   03/09/11  Keith   Added JournalEntry package. Added Target_Suppress_Rule
     02.03.12   06/01/12  Simon   Added API Column Rules changes.
     03.03.13   27/06/12  Nigel   ICS interface changes.
     03.03.14   03/07/12  Nigel   Split out ICS DML packages to one per perview_ctl
     03.03.15   16/08/12  Simon   Generate now also checks status of Settlement packages.
     03.03.16   10/12/12  Nigel   Stopped insert statement being generated for XXCP_IC_INVOICE_INTERFACE
  *****************************************************************************************/

  gTAB      VARCHAR2(50) := '     ';
  gTAB_LESS VARCHAR2(50) := '  ';

  gPN_Cnt            NUMBER:= 0;
  TYPE gPN_TYPE IS RECORD(
     TARGET           varchar2(30),
     HISTORY          varchar2(30),
     PREVIEW          varchar2(30),
     PASSTHROUGH      varchar2(30),
     FORECAST_HISTORY varchar2(30),
     FORECAST_TARGET  varchar2(30),
     SETTLEMENT_TARGET  varchar2(30),
     SETTLEMENT_PREVIEW varchar2(30)
     );

  TYPE gPN_REC    IS TABLE OF gPN_TYPE    INDEX BY BINARY_INTEGER;
  gPN gPN_REC;

  Function Software_Version RETURN VARCHAR2 IS
  Begin
   RETURN('Version [03.03.16] Build Date [10-DEC-2012] Name [XXCP_STATEMENT_CODE]');
  End Software_Version;

  --! ***********************************************************************
  --!                       Load_Package_Names
  --! ***********************************************************************
  Procedure Load_Package_Names is
    
   Cursor c1 is 
      select c.preview_ctl, c.short_code, c.preview_interface_view,
             c.target_tables, c.pass_through, c.forcast, c.settlement
      from xxcp_sys_engine_ctl c
      order by c.preview_ctl;
  
  Begin
     gPN_Cnt := 0;
     gPN.Delete;
     
     For Rec in C1 Loop
       gPN_Cnt := gPN_Cnt + 1;
       If rec.target_tables = 'Y' then
          gPN(rec.preview_ctl).Target  := 'XXCP_PROCESS_DML_'||rec.short_code;  
          gPN(rec.preview_ctl).History := 'XXCP_PROCESS_HIST_'||rec.short_code;  
          gPN(rec.preview_ctl).Preview := 'XXCP_PV_PROCESS_HIST_'||rec.short_code;
       Else
          gPN(rec.preview_ctl).Target  := 'NULL';  
          gPN(rec.preview_ctl).History := 'NULL';  
          gPN(rec.preview_ctl).Preview := 'NULL';
       End If;
       
       If rec.Pass_Through = 'Y' then
         gPN(rec.preview_ctl).PassThrough := 'XXCP_PROCESS_PTR_'||rec.short_code;
       Else
         gPN(rec.preview_ctl).PassThrough := 'NULL';
       End If;
  
       If rec.forcast = 'Y' then
          gPN(rec.preview_ctl).Forecast_Target  := 'XXCP_FC_PROCESS_DML_CPA'; 
          gPN(rec.preview_ctl).Forecast_History := 'XXCP_FC_PROCESS_HIST_CPA';
       Else
          gPN(rec.preview_ctl).Forecast_Target  := 'NULL'; 
          gPN(rec.preview_ctl).Forecast_History := 'NULL';
       End If;
       
       -- 03.03.13
       If rec.settlement = 'Y' then
          gPN(rec.preview_ctl).settlement_target   := 'XXCP_PROCESS_DML_ICS_'||rec.short_code; 
          gPN(rec.preview_ctl).settlement_preview  := 'XXCP_PV_PROCESS_DML_ICS_'||rec.short_code;
       else 
          gPN(rec.preview_ctl).settlement_target   := 'NULL';
          gPN(rec.preview_ctl).settlement_preview  := 'NULL';
       End If;
     End Loop;   

     gPN_Cnt := gPN.Count;

  End Load_Package_Names;
  
  --! ***********************************************************************
  --! Get_Package_Names
  --! ***********************************************************************
  Procedure Get_Package_Names(cPreview_Ctl_id in number, 
                              cTarget_Name out varchar2, 
                              cHistory_Name out varchar2, 
                              cPreview_Name out varchar2,
                              cICS_Preview_Name out varchar2,
                              cICS_Target_Name out varchar2) is
    
  Begin
    
    Load_Package_Names;
    
    if cPreview_Ctl_id between 1 and gPN_cnt then
       cTarget_Name      := gPN(cPreview_Ctl_id).Target;
       cHistory_Name     := gPN(cPreview_Ctl_id).History;
       cPreview_Name     := gPN(cPreview_Ctl_id).Preview;
       cICS_Preview_Name := gPN(cPreview_Ctl_id).settlement_preview;
       cICS_Target_Name  := gPN(cPreview_Ctl_id).settlement_target;
    End If;
  
  End Get_Package_Names;
  

  --! ***********************************************************************
  --!                       Column_Validation
  --! ***********************************************************************

  FUNCTION Column_Validation(cPrevew_ctl in number, cErr_Message out varchar2) Return Boolean is

    Cursor tabIns(pCtl in number) is
     Select distinct transaction_table Table_Name
          , Instance_id
          , nvl(t.rule_category_1,'*')
          , nvl(t.target_object,'TABLE') target_object
       from xxcp_column_rules_tables t,
            xxcp_sys_sources s
      where t.source_id = s.source_id
        and s.preview_ctl = pCtl
        order by 1,2, 3 desc;

    Cursor ColCheck(pTable_Name in varchar2, pInstance_id in number) is
     select Count(*) Cnt
       from xxcp_table_columns t
      where t.TABLE_NAME    = pTable_Name
        and   t.INSTANCE_ID = pInstance_id;

    Cursor DBName(pInstance_id in number) is
     select Instance_name
       from XXCP_INSTANCE_DB_LINKS_V
      where instance_id = pInstance_id
      Union 
      select '*'  -- Wild card
       from dual
       where -1 = pInstance_id;

    vBoolean boolean := TRUE;
    x        integer := 0;
    vMsg     varchar2(250);

    Begin

      For tsrec in tabIns(cPrevew_ctl) Loop
        If tsrec.target_object = 'TABLE' then  -- Only Check Columns for Tables 
          For CKRec in ColCheck( tsrec.Table_Name, tsrec.instance_id) Loop
            If CKRec.Cnt > 0 then
               x := 1;
            End If;
          End Loop;

          If x = 0 then
            vBoolean := FALSE;
            For DBRec in DBName(tsrec.instance_id) Loop
              vMsg := 'Table <'||tsrec.Table_Name||'> Instance <'||DBRec.Instance_name||'> '||
                      'is missing. '||chr(10)||'Run the Column Reset Concurrent program';
              xxcp_foundation.FndWriteError(100,'Statement Code >'||vMsg);
            End Loop;
            Exit;
          End If;
        End if;
      End Loop;

      cErr_Message := vMsg;

      RETURN(vBoolean);
  End Column_Validation;

  --! ***********************************************************************
  --!                       SET_IF_THEN_ELSE_CONSTRUCT
  --!
  --! This procedure works out how to construct the IF THEN ELSE logice
  --! inside the target instance code.
  --! ***********************************************************************
  PROCEDURE Set_If_Then_Else_Construct is

  w            pls_integer;
  q            Integer      := 0;
  vLastRuleKey Varchar2(30);

  BEGIN

   If xxcp_global.gCR.Count > 0 then
     FOR w IN 1..xxcp_global.gCR.Count LOOP

       xxcp_global.gCR(w).Rule_Key := 'X'||xxcp_global.gCR(w).Rule_Key;

       IF nvl(vLastRuleKey,'~null~') !=  nvl(xxcp_global.gCR(w).Rule_Key,'~null~') THEN
          --
          q := 1;
          vLastRuleKey :=  xxcp_global.gCR(w).Rule_Key;
          xxcp_global.gCR(w).Rule_Key := 'F'||Substr(xxcp_global.gCR(w).Rule_Key,2,30);
          xxcp_global.gCR(w).Rule_id  := 1;
          IF w != 1 THEN
            xxcp_global.gCR(w-1).Rule_Key := 'L'||Substr(xxcp_global.gCR(w-1).Rule_Key,2,30);
          END IF;
       ELSE
          q := q + 1;
          xxcp_global.gCR(w).Rule_id  := q;
          xxcp_global.gCR(w).Rule_Key := 'M'||Substr(xxcp_global.gCR(w).Rule_Key,2,30);
       END IF;
     End Loop;

     w := xxcp_global.gCR.Count;
     xxcp_global.gCR(w).Rule_Key := 'L'||Substr(xxcp_global.gCR(w).Rule_Key,2,30);

     FOR w in 1..xxcp_global.gCR.Count LOOP
       IF xxcp_global.gCR(w).Rule_id > 1 THEN
         FOR x in 1..xxcp_global.gCR.Count LOOP
           IF substr(xxcp_global.gCR(x).Rule_Key,2,30) = substr(xxcp_global.gCR(w).Rule_Key,2,30) THEN
             xxcp_global.gCR(x).Rule_id := xxcp_global.gCR(w).Rule_id;
           END IF;
         END LOOP;
       END IF;
     END LOOP;

   End If;

  END Set_if_then_else_construct;

  --! ***********************************************************************
  --! Forecast_History
  --! ***********************************************************************
  FUNCTION Forecast_History(cPreview_Ctl   in number,
                            cSQLERRM      out varchar2) Return Boolean is

    g                       pls_integer ;
    vSource_Table           xxcp_sys_source_tables.transaction_table%type;
    vSource_Instance_id     xxcp_source_assignments.instance_id%type := 0;
    vSource_id              xxcp_sys_sources.source_id%type;
    vTarget_Table           xxcp_sys_target_tables.transaction_table%type;
    vProcess_History_Column xxcp_sys_target_tables.history_id_column%type;
    vDebug_Source_Table     xxcp_sys_source_tables.transaction_table%type;
    vCategory_Str           varchar2(1000);
    vFirstRow               varchar2(1);

    vInternalErrorCode      Integer;

    vPackage_Header   long;
    vPackage_Body     clob;
    vPackage_Middle   clob;
    vPackage          long;
    vInsert_Statement long;
    vPackage_Name     varchar2(30);

    Cursor c1 is
      select header, c.definition_block, c.return_block, c.code_name
        from xxcp_statement_code_b c
       where c.code_name = 'PROCESS_HISTORY_INSERT';

    Cursor SR is
      Select s.Source_activity, s.source_id, s.name, s.source_base_table
        from xxcp_sys_sources s, xxcp_column_rules_tables p1
       Where s.preview_ctl = cPreview_Ctl
         and s.source_id = p1.source_id
         and s.active = 'Y';

    vMaxSource number := 0;
    vSourceCnt number := 0;
    vSQLERRM   varchar2(512);

    Cursor TG(pSource_id in number) is
      select t.source_id,
             t.transaction_table target_table,
             nvl(t.history_id_column, 'VT_HISTORY_ID') process_history_column
        from xxcp_sys_target_tables t, xxcp_sys_sources s
       where t.source_id = s.source_id
         and s.active = 'Y'
         and s.preview_ctl = cpreview_ctl
         and s.source_id = psource_id
       order by s.source_id, t.transaction_table;

    vSuccessful boolean := False;

  Begin

    xxcp_global.Forecast_on := 'Y';     -- Forecast On
    xxcp_global.Preview_on  := 'N';
    -- 03.03.13
    xxcp_global.Settlement_on := 'N';    
    
    if xxcp_global.User_id is null then
    xxcp_global.User_id := 0;
    end if;
    xxcp_global.gCommon(1).Current_Request_id := 0;

    Select count(*)
      into vMaxSource
      from xxcp_sys_target_tables t, xxcp_sys_sources s, xxcp_column_rules_tables p1
     where t.source_id = s.source_id
       and s.active = 'Y'
       and s.source_id = p1.source_id
       and s.preview_ctl = cPreview_Ctl;

    vPackage_Name := gPN(cPreview_Ctl).Forecast_History;

    For PKRec in C1 loop

      vPackage_Header := 'CREATE OR REPLACE PACKAGE ' || vPackage_Name ||
                         ' AS ' || CHR(10) || CHR(10) || pkrec.header ||
                         CHR(10) || 'END ' || vPackage_Name || ';';

      For SRRec in SR Loop

        vSource_Table := SRRec.source_base_table;

        For TGRec in TG(SRRec.Source_id) loop

          vTarget_Table           := TGRec.Target_Table;
          --vTarget_Table           := 'XXCP_FC_PROCESS_HISTORY';
          vProcess_History_Column := TGRec.Process_History_Column;   -- 02.03.06

          vSource_id := SRRec.Source_id;
          xxcp_global.gCommon(1).current_Source_activity := SRRec.Source_Activity;
          xxcp_global.gCommon(1).Source_id := SRRec.Source_id;

          xxcp_global.gCR.delete;
          xxcp_global.gCR_CNT := 0;

          XXCP_MEMORY_PACK.LoadColumnRules(vSource_id,
                                           vSource_Table,
                                           vSource_Instance_id,
                                           vTarget_Table,
                                           0,
                                           vInternalErrorCode);
          Set_if_then_else_construct;

          vSourceCnt := vSourceCnt + 1;

          If vSourceCnt = 1 then
            vPackage_Middle := vPackage_Middle ||
                               '  If xxcp_global.gCommon(1).Source_id = ' ||
                               to_char(vSource_id) ||
                               ' AND cTarget_Table = ''' ||
                               upper(vTarget_Table) || ''' then ' ||
                               CHR(10);
          Else
            vPackage_Middle := vPackage_Middle ||
                               ' xxcp_global.gCommon(1).Source_id = ' ||
                               to_char(vSource_id) ||
                               ' AND cTarget_Table = ''' ||
                               upper(vTarget_Table) || ''' then ' ||
                               CHR(10);
          End If;

          vPackage_Middle := vPackage_Middle || '    -- ' || SRRec.Name ||CHR(10);

          If xxcp_global.gCR.Count > 0 then

            vFirstRow := 'Y';

            For g in 1 .. xxcp_global.gCR.Count Loop

              vInsert_Statement := substr(xxcp_global.gCR(G).Direct_History_statement,1,32600);

              vInsert_Statement := Replace(vInsert_Statement,':M0','cSource_Rowid');
              vInsert_Statement := Replace(vInsert_Statement,':J0','cParent_Rowid');
              vInsert_Statement := Replace(vInsert_Statement,':M15', vProcess_History_Column);
              vInsert_Statement := Replace(vInsert_Statement,':H01','cProcess_history_id');
              vInsert_Statement := Replace(vInsert_Statement,':H02','cTarget_Assignment_id');
              vInsert_Statement := Replace(vInsert_Statement,':H03','cAttribute_id');
              vInsert_Statement := Replace(vInsert_Statement,':H05','cRecord_type');
              vInsert_Statement := Replace(vInsert_Statement,':H06','cRequest_id');
              vInsert_Statement := Replace(vInsert_Statement,':H07','cStatus');
              vInsert_Statement := Replace(vInsert_Statement,':H08','cInterface_id');
              vInsert_Statement := Replace(vInsert_Statement,':H09','cRule_id');
              vInsert_Statement := Replace(vInsert_Statement,':H13','cTax_registration_id');
              vInsert_Statement := Replace(vInsert_Statement,':H14','cAccount_Rounding_amount');
              vInsert_Statement := Replace(vInsert_Statement,':H15','cEntered_Rounding_amount');
              vInsert_Statement := Replace(vInsert_Statement,':H16','cTarget_Instance_id');
              vInsert_Statement := Replace(vInsert_Statement,':H17','cModel_Ctl_id');

                If xxcp_global.gCR(G).Rule_id = 1 Then

                  vPackage_Middle := vPackage_Middle || CHR(10) ||
                  gTAB||'If cTarget_Instance_id = ' ||
                   to_char(xxcp_global.gCR(G).Target_Instance_id) || ' then ' || CHR(10);
                  vPackage_Middle := vPackage_Middle || gTAB_LESS||vInsert_Statement || ';' || chr(10);
                  vPackage_Middle := vPackage_Middle || CHR(10) ||gTAB||'End If;';
                  vFirstRow := 'N';

                Else
                   vCategory_Str := ''''||xxcp_global.gCR(G).Rule_Category||''' = cI1009_Array(62) then'||chr(10);

                   If vFirstRow = 'N' then
                     vFirstRow := 'Y';
                     xxcp_global.gCR(G).Rule_Key := 'F'||substr(xxcp_global.gCR(G).Rule_Key,2,30);
                   End If;

                   CASE Substr(xxcp_global.gCR(G).Rule_Key,1,1)
                    WHEN 'F' then
                        vPackage_Middle := vPackage_Middle || CHR(10) ||
                        gTAB||'If cTarget_Instance_id = ' ||
                        to_char(xxcp_global.gCR(G).Target_Instance_id) || ' then ' || CHR(10) || CHR(10);
                        vPackage_Middle := vPackage_Middle||gTAB||'If '||vCategory_Str;
                        vPackage_Middle := vPackage_Middle||gTAB_LESS||vInsert_Statement || ';' || chr(10);
                    WHEN 'M' then
                        vPackage_Middle := vPackage_Middle||gTAB||'ElsIf '||vCategory_Str;
                        vPackage_Middle := vPackage_Middle||gTAB_LESS||vInsert_Statement || ';' || chr(10);
                    WHEN 'L' then
                        If instr(vCategory_str,'*') > 0 then
                          vPackage_Middle := vPackage_Middle||gTAB||'-- Default '||chr(10)||gTAB||'Else '||chr(10);
                        Else
                          vPackage_Middle := vPackage_Middle||gTAB||'ElsIf '||vCategory_Str;
                        End If;
                        vPackage_Middle := vPackage_Middle||gTAB_LESS||vInsert_Statement || ';' || chr(10);
                        vPackage_Middle := vPackage_Middle ||gTAB||  CHR(10) ||gTAB||'End If;' ||Chr(10);
                        vPackage_Middle := vPackage_Middle || CHR(10) ||gTAB||'End If;' || chr(10);
                        vFirstRow := 'N';
                   END CASE;

                End If;

            End Loop;
          End If;

          If xxcp_global.gCR.Count = 0 then
            vPackage_Middle := vPackage_Middle ||gTAB||'cInternalErrorCode := 12801; -- No statement' ||chr(10);
          End If;

          If vMaxSource = vSourceCnt then
            vPackage_Middle := vPackage_Middle || chr(10)||'End If;' || chr(10);
          Else
            vPackage_Middle := vPackage_Middle || chr(10)||'  ElsIf ';
          End If;

        End Loop;
      End Loop;

      If  vPackage_Middle is null then
        vPackage_Middle := '    cInternalErrorCode := 12801; -- No statement';
      End If;

      vPackage_Body := 'CREATE OR REPLACE PACKAGE BODY ' || vPackage_Name ||
                       ' AS ' || CHR(10) || CHR(10) ||
                       pkrec.definition_block || CHR(10) || CHR(10) ||
                       'Begin' || CHR(10) || CHR(10) || vPackage_Middle ||
                       CHR(10) || CHR(10) || pkrec.return_block || CHR(10) ||
                       CHR(10) || 'End ' || pkrec.code_name || ';' ||
                       CHR(10) || CHR(10) || 'END ' || vPackage_Name || ';';

      --
      -- Compile Packages!!
      --

      Begin

        If xxcp_forms.Execute_immediate_clob(vPackage_Header, 'N', vSQLERRM) then
          If xxcp_forms.Execute_immediate_clob(vPackage_Body, 'N', vSQLERRM) then

            vSuccessful := True;
          Else
            cSQLERRM := vSQLERRM;
          End If;
        Else
          cSQLERRM := vSQLERRM;
        End If;
      Exception
        when OTHERS then
          vSuccessful := False;
          cSQLERRM    := vSQLERRM;
      End;

    End Loop;

    Return(vSuccessful);

  End Forecast_History;

  --! ***********************************************************************
  --! Process_History
  --! ***********************************************************************
  FUNCTION Process_History(cPreview_Ctl   in number,
                           cPreview_mode  in varchar2 Default 'N',
                           cSQLERRM      out varchar2) Return Boolean is

    g                       pls_integer ;
    vSource_Table           xxcp_sys_source_tables.transaction_table%type;
    vSource_Instance_id     xxcp_source_assignments.instance_id%type := 0;
    vSource_id              xxcp_sys_sources.source_id%type;
    vTarget_Table           xxcp_sys_target_tables.transaction_table%type;
    vProcess_History_Column xxcp_sys_target_tables.history_id_column%type;
    vDebug_Source_Table     xxcp_sys_source_tables.transaction_table%type;
    vCategory_Str           varchar2(1000);
    vFirstRow               varchar2(1);

    vInternalErrorCode      Integer;

    vPackage_Header   long;
    vPackage_Body     clob;
    vPackage_Middle   clob;
    vPackage          long;
    vInsert_Statement long;
    vPackage_Name     varchar2(30);

    Cursor c1 is
      select header, c.definition_block, c.return_block, c.code_name
        from xxcp_statement_code_b c
       where c.code_name = 'PROCESS_HISTORY_INSERT';

    Cursor SR is
      Select s.Source_activity, s.source_id, s.name, s.source_base_table
        from xxcp_sys_sources s, xxcp_column_rules_tables p1
       Where s.preview_ctl = cPreview_Ctl
         and s.source_id = p1.source_id
         and s.active = 'Y';

    vMaxSource number := 0;
    vSourceCnt number := 0;
    vSQLERRM   varchar2(512);

    Cursor TG(pSource_id in number) is
      select t.source_id,
             t.transaction_table target_table,
             nvl(t.history_id_column, 'VT_HISTORY_ID') process_history_column
        from xxcp_sys_target_tables t, xxcp_sys_sources s
       where t.source_id = s.source_id
         and s.active = 'Y'
         and s.preview_ctl = cpreview_ctl
         and s.source_id = psource_id
       order by s.source_id, t.transaction_table;

    vSuccessful   boolean := False;
    vMake_Package boolean := True;

  Begin

    If cPreview_Mode in ('Y','J') then
       xxcp_global.Preview_on  := 'Y';
    Else
       xxcp_global.Preview_on  := 'N';
    End If;

    -- 03.03.13
    xxcp_global.Settlement_on := 'N';    
    
    xxcp_global.Forecast_on := 'N';

    if xxcp_global.User_id is null then
      xxcp_global.User_id := 0;
    end if;

    xxcp_global.gCommon(1).Current_Request_id := 0;

    Select count(*)
      into vMaxSource
      from xxcp_sys_target_tables t, xxcp_sys_sources s, xxcp_column_rules_tables p1
     where t.source_id = s.source_id
       and s.active = 'Y'
       and s.source_id = p1.source_id
       and s.preview_ctl = cPreview_Ctl;

    If cPreview_mode = 'Y'  then
       vPackage_Name := gPN(cPreview_Ctl).Preview;
       vMake_Package := True;
    Elsif cPreview_mode = 'N' then
       vPackage_Name := gPN(cPreview_Ctl).History;
       vMake_Package := True;
    ElsIf cPreview_mode = 'J' then
       vPackage_Name := gPN(cPreview_Ctl).Preview;
       vPackage_Name := Replace(vPackage_Name,'XXCP_PV','XXCP_PJ');
       vMake_Package := True;
    End If;

    If vMake_Package then

     For PKRec in C1 loop

      vPackage_Header := 'CREATE OR REPLACE PACKAGE ' || vPackage_Name ||
                         ' AS ' || CHR(10) || CHR(10) || pkrec.header ||
                         CHR(10) || 'END ' || vPackage_Name || ';';

      For SRRec in SR Loop

        vSource_Table := SRRec.source_base_table;
        
        For TGRec in TG(pSource_id => SRRec.Source_id) loop

          vTarget_Table           := TGRec.Target_Table;
          vProcess_History_Column := TGRec.Process_History_Column;

          vSource_id := SRRec.Source_id;
          xxcp_global.gCommon(1).current_Source_activity := SRRec.Source_Activity;
          xxcp_global.gCommon(1).Source_id := SRRec.Source_id;

          xxcp_global.gCR.delete;
          xxcp_global.gCR_CNT := 0;
          
          XXCP_MEMORY_PACK.LoadColumnRules(cSource_id          => vSource_id,  
                                           cSource_Table       => vSource_Table,
                                           cSource_Instance_id => vSource_Instance_id,
                                           cTarget_Table       => vTarget_Table,
                                           cRequest_id         => 0,
                                           cInternalErrorCode  => vInternalErrorCode);  
                                           
          Set_if_then_else_construct;

          vSourceCnt := vSourceCnt + 1;

          If vSourceCnt = 1 then
            vPackage_Middle := vPackage_Middle ||
                               '  If xxcp_global.gCommon(1).Source_id = ' ||
                               to_char(vSource_id) ||
                               ' AND cI1009_Array(146) != ''Y'''||
                               ' AND cTarget_Table = ''' ||
                               upper(vTarget_Table) || ''' then ' ||
                               CHR(10);
          Else
            vPackage_Middle := vPackage_Middle ||
                               ' xxcp_global.gCommon(1).Source_id = ' ||
                               to_char(vSource_id) ||
                               ' AND cI1009_Array(146) != ''Y'''||
                               ' AND cTarget_Table = ''' ||
                               upper(vTarget_Table) || ''' then ' ||
                               CHR(10);
          End If;

          vPackage_Middle := vPackage_Middle || '    -- ' || SRRec.Name ||CHR(10);

          If xxcp_global.gCR.Count > 0 then

            vFirstRow := 'Y';

            For g in 1 .. xxcp_global.gCR.Count Loop

              vInsert_Statement := substr(xxcp_global.gCR(G).Direct_History_statement,1,32600);
              
              If cPreview_mode = 'J' then
                vInsert_Statement := Replace(vInsert_Statement,'from XXCP_GL_INTERFACE','from XXCP_PV_GL_INTERFACE');
              End If;

              vInsert_Statement := Replace(vInsert_Statement,':M0','cSource_Rowid');
              vInsert_Statement := Replace(vInsert_Statement,':J0','cParent_Rowid');
              vInsert_Statement := Replace(vInsert_Statement,':M15', vProcess_History_Column);
              vInsert_Statement := Replace(vInsert_Statement,':H01','cProcess_history_id');
              vInsert_Statement := Replace(vInsert_Statement,':H02','cTarget_Assignment_id');
              vInsert_Statement := Replace(vInsert_Statement,':H03','cAttribute_id');
              vInsert_Statement := Replace(vInsert_Statement,':H05','cRecord_type');
              vInsert_Statement := Replace(vInsert_Statement,':H06','cRequest_id');
              vInsert_Statement := Replace(vInsert_Statement,':H07','cStatus');
              vInsert_Statement := Replace(vInsert_Statement,':H08','cInterface_id');
              vInsert_Statement := Replace(vInsert_Statement,':H09','cRule_id');
              vInsert_Statement := Replace(vInsert_Statement,':H13','cTax_registration_id');
              vInsert_Statement := Replace(vInsert_Statement,':H14','cAccount_Rounding_amount');
              vInsert_Statement := Replace(vInsert_Statement,':H15','cEntered_Rounding_amount');
              vInsert_Statement := Replace(vInsert_Statement,':H16','cTarget_Instance_id');
              vInsert_Statement := Replace(vInsert_Statement,':H17','cModel_Ctl_id');

                If xxcp_global.gCR(G).Rule_id = 1 Then

                  If xxcp_global.gCR(G).Target_Instance_id = -1 then
                    vPackage_Middle := vPackage_Middle || CHR(10) ||
                    gTAB||'If vFnd = ''N'' then ' || CHR(10);
                  Else
                   vPackage_Middle := vPackage_Middle || CHR(10) ||
                    gTAB||'If cTarget_Instance_id = ' ||
                     to_char(xxcp_global.gCR(G).Target_Instance_id) || ' then ' || CHR(10);
                  End If; 

                  vPackage_Middle := vPackage_Middle || gTAB_LESS||vInsert_Statement || ';' || chr(10);
                  vPackage_Middle := vPackage_Middle || CHR(10) ||gTAB||'End If;';
                  vFirstRow := 'N';

                Else
                   vCategory_Str := ''''||xxcp_global.gCR(G).Rule_Category||''' = cI1009_Array(62) then'||chr(10);

                   If vFirstRow = 'N' then
                     vFirstRow := 'Y';
                     xxcp_global.gCR(G).Rule_Key := 'F'||substr(xxcp_global.gCR(G).Rule_Key,2,30);
                   End If;

                   CASE Substr(xxcp_global.gCR(G).Rule_Key,1,1)
                    WHEN 'F' then
                    
                       If xxcp_global.gCR(G).Target_Instance_id = -1 then
                        vPackage_Middle := vPackage_Middle || CHR(10) ||
                        gTAB||'If vFnd = ''N'' then ' || CHR(10) || CHR(10);
                       Else  
                        vPackage_Middle := vPackage_Middle || CHR(10) ||
                        gTAB||'If cTarget_Instance_id = ' ||
                        to_char(xxcp_global.gCR(G).Target_Instance_id) || ' then ' || CHR(10) || CHR(10);
                       End If;
                        vPackage_Middle := vPackage_Middle||gTAB||'If '||vCategory_Str;
                        vPackage_Middle := vPackage_Middle||gTAB_LESS||vInsert_Statement || ';' || chr(10);
                    WHEN 'M' then
                        vPackage_Middle := vPackage_Middle||gTAB||'ElsIf '||vCategory_Str;
                        vPackage_Middle := vPackage_Middle||gTAB_LESS||vInsert_Statement || ';' || chr(10);
                    WHEN 'L' then
                        If instr(vCategory_str,'*') > 0 then
                          vPackage_Middle := vPackage_Middle||gTAB||'-- Default '||chr(10)||gTAB||'Else '||chr(10);
                        Else
                          vPackage_Middle := vPackage_Middle||gTAB||'ElsIf '||vCategory_Str;
                        End If;
                        vPackage_Middle := vPackage_Middle||gTAB_LESS||vInsert_Statement || ';' || chr(10);
                        vPackage_Middle := vPackage_Middle ||gTAB||  CHR(10) ||gTAB||'End If;' ||Chr(10);
                        vPackage_Middle := vPackage_Middle || CHR(10) ||gTAB||'End If;' || chr(10);
                        vFirstRow := 'N';
                   END CASE;

                End If;

            End Loop;
          End If;

          If xxcp_global.gCR.Count = 0 then
            vPackage_Middle := vPackage_Middle ||gTAB||'cInternalErrorCode := 12801; -- No statement' ||chr(10);
          End If;

          If vMaxSource = vSourceCnt then
            vPackage_Middle := vPackage_Middle || chr(10)||'End If;' || chr(10);
          Else
            vPackage_Middle := vPackage_Middle || chr(10)||'  ElsIf ';
          End If;

        End Loop;
      End Loop;

      If  vPackage_Middle is null then
        vPackage_Middle := '    cInternalErrorCode := 12801; -- No statement';
      End If;

      vPackage_Body := 'CREATE OR REPLACE PACKAGE BODY ' || vPackage_Name ||
                       ' AS ' || CHR(10) || 
                       pkrec.definition_block || CHR(10) || ' vFnd varchar2(1) := ''N'';'|| CHR(10) ||
                       'Begin' || CHR(10) || CHR(10) || vPackage_Middle ||
                       CHR(10) || CHR(10) || pkrec.return_block || CHR(10) ||
                       CHR(10) || 'End ' || pkrec.code_name || ';' ||
                       CHR(10) || CHR(10) || 'END ' || vPackage_Name || ';';


      --
      -- Compile Packages!!
      --


      Begin

        If xxcp_forms.Execute_immediate_clob(vPackage_Header, 'N', vSQLERRM) then
          If xxcp_forms.Execute_immediate_clob(vPackage_Body, 'N', vSQLERRM) then

            vSuccessful := True;
          Else
            cSQLERRM := vSQLERRM;
          End If;
        Else
          cSQLERRM := vSQLERRM;
        End If;
      Exception
        when OTHERS then
          vSuccessful := False;
          cSQLERRM    := vSQLERRM;
      End;

    End Loop;

     -- Create Forecast History Package if applicable
     If (vSuccessful and gPN(cPreview_Ctl).Forecast_History != 'NULL') then
       vSuccessful := Forecast_History(cPreview_Ctl, vSQLERRM);
     End if;

    End If;

    Return(vSuccessful);

  End Process_History;

  --! ***********************************************************************
  --! Forecast_DML
  --! ***********************************************************************
  Function Forecast_DML(cPreview_Ctl  in number,
                        cSQLERRM      out varchar2) Return Boolean is

    g                       Integer;
    vSource_Table           xxcp_sys_source_tables.transaction_table%type;
    vSource_Instance_id     number;
    vSource_id              number;
    vTarget_Table           xxcp_sys_target_tables.transaction_table%type;
    vProcess_History_Column xxcp_sys_target_tables.history_id_column%type;
    vDebug_Source_Table     xxcp_sys_target_tables.transaction_table%type;

    vInternalErrorCode      xxcp_errors.internal_error_code%type;
    vMaxSource              number := 0;
    vSourceCnt              number := 0;
    vSQLERRM                varchar2(512);

    vSuccessful             boolean := False;
    vCategory_Str           varchar2(1000);
    vFirstRow               varchar2(1);

    vPackage_Header         long;
    vPackage_Body           clob;
    vPackage_Middle         clob;
    vPackage                long;
    vInsert_Statement       long;
    vPackage_Name           varchar2(30);

    Cursor POI is
      select header, c.definition_block, c.return_block, c.code_name
        from xxcp_statement_code_b c
       where c.code_name = 'PROCESS_ORACLE_INSERT'
          or c.code_name = 'PROCESS_PASS_THROUGH'
          order by c.code_name;

    Cursor SR is
      Select s.Source_activity, s.source_id, s.name, s.source_base_table
        from xxcp_sys_sources s
       Where s.preview_ctl = cPreview_Ctl
         and s.active = 'Y';

    Cursor TG(pSource_id in number) is
      select t.source_id,
             t.transaction_table target_table,
             nvl(t.history_id_column, 'VT_HISTORY_ID') process_history_column
        from xxcp_sys_target_tables t, xxcp_sys_sources s
       where t.source_id = s.source_id
         and s.active = 'Y'
         and s.preview_ctl = cpreview_ctl
         and s.source_id = psource_id
       order by s.source_id, t.transaction_table;

    vTable_Tag    varchar2(30);
    vInstance_Tag varchar2(30);

  Begin

    xxcp_global.Forecast_on := 'Y';     -- Forecast On
    xxcp_global.Preview_on  := 'N';
    if xxcp_global.User_id is null then
      xxcp_global.User_id    := 0;
    end if;
    
    -- 03.03.13
    xxcp_global.Settlement_on := 'N';    

    xxcp_global.gCommon(1).Current_Request_id := 0;

    If xxcp_global.Preview_on = 'N' then
      -- Never create one for Preview Mode!!!

      Select count(*)
        into vMaxSource
        from xxcp_sys_target_tables t, xxcp_sys_sources s
       where t.source_id = s.source_id
         and s.active = 'Y'
         and s.preview_ctl = cPreview_Ctl;

      vPackage_Name := gPN(cPreview_Ctl).Forecast_Target;

     vPackage_Header := 'CREATE OR REPLACE PACKAGE ' || vPackage_Name || ' AS ' ;
     For PKRec in POI loop
        vPackage_Header := vPackage_Header || CHR(10) || CHR(10) || pkrec.header;
     End Loop;
     vPackage_Header := vPackage_Header ||CHR(10) || 'END ' || vPackage_Name || ';';

     For PKRec in POI loop

        vPackage_Body :=   CHR(10)||vPackage_Body|| CHR(10) ||
                          pkrec.definition_block || CHR(10) || CHR(10);

        vSourceCnt := 0;
        xxcp_global.gCR.delete;
        xxcp_global.gCR_CNT := 0;
        vPackage_Middle := Null;


        For SRRec in SR Loop

          vSource_Table := SRRec.source_base_table;

          For TGRec in TG(SRRec.Source_id) loop

            vTarget_Table           := TGRec.Target_Table;
            vProcess_History_Column := TGRec.Process_History_Column;

            vSource_id := SRRec.Source_id;
            xxcp_global.gCommon(1).current_Source_activity := SRRec.Source_Activity;
            xxcp_global.gCommon(1).Source_id := SRRec.Source_id;

            xxcp_global.gCR.delete;
            xxcp_global.gCR_CNT := 0;

            If pkRec.Code_Name = 'PROCESS_ORACLE_INSERT' then

               xxcp_memory_pack.LoadColumnRules(
                                             cSource_id           => vSource_id,
                                             cSource_Table        => vSource_Table,
                                             cSource_Instance_id  => vSource_Instance_id,
                                             cTarget_Table        => vTarget_Table,
                                             cRequest_id          => 0,
                                             cInternalErrorCode   => vInternalErrorCode);

                   vTable_Tag    := 'cTarget_Table';
                   vInstance_Tag := 'cTarget_Instance_id';
            Else
                  xxcp_memory_pack.LoadPassThroughRules(
                                             cSource_id           => vSource_id,
                                             cSource_Table        => vSource_Table,
                                             cSource_Instance_id  => vSource_Instance_id,
                                             cTarget_Table        => vTarget_Table,
                                             cRequest_id          => 0,
                                             cPassThroughRules    => 'Y',
                                             cInternalErrorCode   => vInternalErrorCode);

                   vTable_Tag    := 'cSource_Table';
                   vInstance_Tag := 'cSource_Instance_id';


            End If;

            SET_IF_THEN_ELSE_CONSTRUCT;

            vSourceCnt := vSourceCnt + 1;

            If vSourceCnt = 1 then
              vPackage_Middle := vPackage_Middle ||
                                 '  If xxcp_global.gCommon(1).Source_id = ' ||
                                 to_char(vSource_id) ||
                                 ' AND '||vTable_Tag||' = ''' ||
                                 upper(vTarget_Table) || ''' then ' ||
                                 CHR(10);
            Else
              vPackage_Middle := vPackage_Middle ||
                                 ' xxcp_global.gCommon(1).Source_id = ' ||
                                 to_char(vSource_id) ||
                                 ' AND '||vTable_Tag||' = ''' ||
                                 upper(vTarget_Table) || ''' then ' ||
                                 CHR(10);
            End If;

            vPackage_Middle := vPackage_Middle || '    -- ' || SRRec.Name || CHR(10);

            If xxcp_global.gCR.Count > 0 then

              vFirstRow := 'Y';

              For G in 1 .. xxcp_global.gCR.Count Loop

                vInsert_Statement := substr(xxcp_global.gCR(G).Direct_DML_statement,1,32600);
                vInsert_Statement := Replace(vInsert_Statement, ':M0', 'cSource_Rowid');
                vInsert_Statement := Replace(vInsert_Statement, ':J0', 'cParent_Rowid');
                vInsert_Statement := Replace(vInsert_Statement, ':M15', 'cProcess_History_id');

                If xxcp_global.gCR(G).Rule_id = 1 Then
                  vCategory_Str := ''''||xxcp_global.gCR(G).Rule_Category||''' = cI1009_Array(62) then'||chr(10);
                  vPackage_Middle := vPackage_Middle || CHR(10) ||
                  gTAB||'If '||vInstance_Tag||' = ' ||
                   to_char(xxcp_global.gCR(G).Target_Instance_id) || ' THEN ' || CHR(10);
                  vPackage_Middle := vPackage_Middle || gTAB_LESS||vInsert_Statement || ';' || chr(10);
                  vPackage_Middle := vPackage_Middle || CHR(10) ||gTAB||'End If;';
                  vFirstRow := 'N';
                Else
                   vCategory_Str := ''''||xxcp_global.gCR(G).Rule_Category||''' = cI1009_Array(62) then'||chr(10);

                   If vFirstRow = 'N' then
                     vFirstRow := 'Y';
                     xxcp_global.gCR(G).Rule_Key := 'F'||substr(xxcp_global.gCR(G).Rule_Key,2,30);
                   End If;

                   CASE Substr(xxcp_global.gCR(G).Rule_Key,1,1)
                    WHEN 'F' then
                        vPackage_Middle := vPackage_Middle || CHR(10) ||
                        gTAB||'If '||vInstance_Tag||' = ' ||
                        to_char(xxcp_global.gCR(G).Target_Instance_id) || ' THEN ' || CHR(10) || CHR(10);
                        vPackage_Middle := vPackage_Middle||gTAB||'If '||vCategory_Str;
                        vPackage_Middle := vPackage_Middle||gTAB_LESS||vInsert_Statement || ';' || chr(10);
                    WHEN 'M' then
                        vPackage_Middle := vPackage_Middle||gTAB||'ElsIf '||vCategory_Str;
                        vPackage_Middle := vPackage_Middle||gTAB_LESS||vInsert_Statement || ';' || chr(10);
                    WHEN 'L' then                    
                        If instr(vCategory_str,'*') > 0 then
                          vPackage_Middle := vPackage_Middle||gTAB||'-- Default '||chr(10)||gTAB||'Else '||chr(10);
                        Else
                          vPackage_Middle := vPackage_Middle||gTAB||'ElsIf '||vCategory_Str;
                        End If;
                        vPackage_Middle := vPackage_Middle||gTAB_LESS||vInsert_Statement || ';' || chr(10);
                        vPackage_Middle := vPackage_Middle ||gTAB||  CHR(10) ||gTAB||'End If;' ||Chr(10);
                        vPackage_Middle := vPackage_Middle || CHR(10) ||gTAB||'End If;' || chr(10);
                        vFirstRow := 'N';
                   END CASE;

                End If;

            End Loop;
          End If;

          If xxcp_global.gCR.Count = 0 then
            vPackage_Middle := vPackage_Middle ||gTAB||'cInternalErrorCode := 12801; -- No statement' ||chr(10);
          End If;

          If vMaxSource = vSourceCnt then
            vPackage_Middle := vPackage_Middle || chr(10)||'End If;' || chr(10);
          Else
            vPackage_Middle := vPackage_Middle || chr(10)||'  ElsIf ';
          End If;


          End Loop;
        End Loop;

        If  vPackage_Middle is null then
          vPackage_Middle := '    cInternalErrorCode := 12801; -- No statement';
        End If;


        vPackage_Body :=  vPackage_Body|| CHR(10) ||
                          'Begin' || CHR(10) || CHR(10) || vPackage_Middle ||
                         CHR(10) || CHR(10) || pkrec.return_block ||
                         CHR(10) || CHR(10) || 'End ' || pkrec.code_name || ';'|| CHR(10) ;
     End Loop;

        vPackage_Body := 'CREATE OR REPLACE PACKAGE BODY ' || vPackage_Name ||
                         ' AS ' || CHR(10) || CHR(10) || vPackage_Body ||
                         CHR(10) || CHR(10) || 'END ' || vPackage_Name || ';';

        --
        -- Compile Packages!!
        --
        Begin

          If xxcp_forms.Execute_Immediate_Clob(vPackage_Header,'N',vSQLERRM) then
            If xxcp_forms.Execute_Immediate_Clob(vPackage_Body,'N',vSQLERRM) then
              vSuccessful := True;
            Else
              cSQLERRM := vSQLERRM;
            End If;
          Else
            cSQLERRM := vSQLERRM;
          End If;

        Exception
          when OTHERS then
            vSuccessful := False;
            cSQLERRM    := vSQLERRM;
        End;

     -- End Loop;

    End If;

    Return(vSuccessful);

  End Forecast_DML;
  
  --! ***********************************************************************
  --! Settlement_DML
  --! ***********************************************************************
  Function Settlement_DML(cPreview_Ctl  in number,
                          cPreview_mode in varchar2 Default 'N',
                          cSQLERRM      out varchar2) Return Boolean is

    g                       Integer;
    vSource_Table           xxcp_sys_source_tables.transaction_table%type;
    vSource_Instance_id     number;
    vSource_id              number;
    vTarget_Table           xxcp_sys_target_tables.transaction_table%type;
    vProcess_History_Column xxcp_sys_target_tables.history_id_column%type;
    vDebug_Source_Table     xxcp_sys_target_tables.transaction_table%type;

    vInternalErrorCode      xxcp_errors.internal_error_code%type;
    vMaxSource              number := 0;
    vSourceCnt              number := 0;
    vSQLERRM                varchar2(512);

    vSuccessful             boolean := False;
    vCategory_Str           varchar2(1000);
    vFirstRow               varchar2(1);

    vPackage_Header         long;
    vPackage_Body           clob;
    vPackage_Middle         clob;
    vPackage                long;
    vInsert_Statement       long;
    vPackage_Name           varchar2(30);

    Cursor POI is
      select header, c.definition_block, c.return_block, c.code_name
        from xxcp_statement_code_b c
       where c.code_name = 'PROCESS_ORACLE_INSERT'
          order by c.code_name;

    Cursor SR is
      Select s.Source_activity, s.source_id, s.name, s.source_base_table
        from xxcp_sys_sources s
       Where s.preview_ctl = cPreview_Ctl
         and s.active = 'Y';

    Cursor TG(pSource_id in number) is
      select t.source_id,
             t.transaction_table target_table,
             nvl(t.history_id_column, 'VT_HISTORY_ID') process_history_column
        from xxcp_sys_target_tables t, xxcp_sys_sources s
       where t.source_id = s.source_id
         and s.active = 'Y'
         and s.preview_ctl = cpreview_ctl
         and s.source_id = psource_id
       order by s.source_id, t.transaction_table;

    vTable_Tag    varchar2(30);
    vInstance_Tag varchar2(30);

  Begin

    xxcp_global.Preview_on  := 'N';
    xxcp_global.Forecast_on := 'N';
    -- ICS V4
    xxcp_global.Settlement_on := 'Y';
    
    if xxcp_global.User_id is null then
      xxcp_global.User_id    := 0;
    end if;
    xxcp_global.gCommon(1).Current_Request_id := 0;

      -- Never create one for Preview Mode!!!

      Select count(*)
        into vMaxSource
        from xxcp_sys_target_tables t, xxcp_sys_sources s
       where t.source_id = s.source_id
         and s.active = 'Y'
         and s.preview_ctl = cPreview_Ctl;

     If cPreview_mode = 'N' then
       vPackage_Name := gPN(cPreview_Ctl).settlement_target;
       xxcp_global.Preview_on  := 'N';
     else 
       vPackage_Name := gPN(cPreview_Ctl).settlement_preview;
       xxcp_global.Preview_on  := 'Y';
     End if;

     vPackage_Header := 'CREATE OR REPLACE PACKAGE ' || vPackage_Name || ' AS ' ;
     For PKRec in POI loop
        vPackage_Header := vPackage_Header || CHR(10) || CHR(10) || pkrec.header;
     End Loop;
     vPackage_Header := vPackage_Header ||CHR(10) || 'END ' || vPackage_Name || ';';


     For PKRec in POI loop

        vPackage_Body :=   CHR(10)||vPackage_Body|| CHR(10) ||
                          pkrec.definition_block || CHR(10) || CHR(10);

        vSourceCnt := 0;
        xxcp_global.gCR.delete;
        xxcp_global.gCR_CNT := 0;
        vPackage_Middle := Null;


        For SRRec in SR Loop

          vSource_Table := SRRec.source_base_table;

          For TGRec in TG(SRRec.Source_id) loop

            vTarget_Table           := TGRec.Target_Table;
            vProcess_History_Column := TGRec.Process_History_Column;

            vSource_id := SRRec.Source_id;
            xxcp_global.gCommon(1).current_Source_activity := SRRec.Source_Activity;
            xxcp_global.gCommon(1).Source_id := SRRec.Source_id;

            xxcp_global.gCR.delete;
            xxcp_global.gCR_CNT := 0;

            If pkRec.Code_Name = 'PROCESS_ORACLE_INSERT' then

              -- 03.03.16 Never generate ICS column rules for XXCP_IC_INVOICE_INTERFACE
              if vTarget_Table != 'XXCP_IC_INVOICE_INTERFACE' then
                xxcp_memory_pack.LoadColumnRules(
                                              cSource_id           => vSource_id,
                                              cSource_Table        => vSource_Table,
                                              cSource_Instance_id  => vSource_Instance_id,
                                              cTarget_Table        => vTarget_Table,
                                              cRequest_id          => 0,
                                              cInternalErrorCode   => vInternalErrorCode);

                   vTable_Tag    := 'cTarget_Table';
                   vInstance_Tag := 'cTarget_Instance_id';
              End If;                   

            End If;

            SET_IF_THEN_ELSE_CONSTRUCT;

            vSourceCnt := vSourceCnt + 1;

            If vSourceCnt = 1 then
              vPackage_Middle := vPackage_Middle ||
                                 '  If xxcp_global.gCommon(1).Source_id = ' ||
                                 to_char(vSource_id) ||
                                 ' AND '||vTable_Tag||' = ''' ||
                                 upper(vTarget_Table) || ''' then ' ||
                                 CHR(10);
            Else
              vPackage_Middle := vPackage_Middle ||
                                 ' xxcp_global.gCommon(1).Source_id = ' ||
                                 to_char(vSource_id) ||
                                 ' AND '||vTable_Tag||' = ''' ||
                                 upper(vTarget_Table) || ''' then ' ||
                                 CHR(10);
            End If;

            vPackage_Middle := vPackage_Middle || '    -- ' || SRRec.Name || CHR(10);

            If xxcp_global.gCR.Count > 0 then

              vFirstRow := 'Y';

              For G in 1 .. xxcp_global.gCR.Count Loop

                vInsert_Statement := substr(xxcp_global.gCR(G).Direct_DML_statement,1,32600);
                vInsert_Statement := Replace(vInsert_Statement, ':M0', 'cSource_Rowid');
                vInsert_Statement := Replace(vInsert_Statement, ':J0', 'cParent_Rowid');
                vInsert_Statement := Replace(vInsert_Statement, ':M15', 'cProcess_History_id');

                If xxcp_global.gCR(G).Rule_id = 1 Then
                  vCategory_Str := ''''||xxcp_global.gCR(G).Rule_Category||''' = cI1009_Array(62) then'||chr(10);
                  vPackage_Middle := vPackage_Middle || CHR(10) ||
                  gTAB||'If '||vInstance_Tag||' = ' ||
                   to_char(xxcp_global.gCR(G).Target_Instance_id) || ' THEN ' || CHR(10);
                  vPackage_Middle := vPackage_Middle || gTAB_LESS||vInsert_Statement || ';' || chr(10);
                  vPackage_Middle := vPackage_Middle || CHR(10) ||gTAB||'End If;';
                  vFirstRow := 'N';
                Else
                   vCategory_Str := ''''||xxcp_global.gCR(G).Rule_Category||''' = cI1009_Array(62) then'||chr(10);

                   If vFirstRow = 'N' then
                     vFirstRow := 'Y';
                     xxcp_global.gCR(G).Rule_Key := 'F'||substr(xxcp_global.gCR(G).Rule_Key,2,30);
                   End If;

                   CASE Substr(xxcp_global.gCR(G).Rule_Key,1,1)
                    WHEN 'F' then
                        vPackage_Middle := vPackage_Middle || CHR(10) ||
                        gTAB||'If '||vInstance_Tag||' = ' ||
                        to_char(xxcp_global.gCR(G).Target_Instance_id) || ' THEN ' || CHR(10) || CHR(10);
                        vPackage_Middle := vPackage_Middle||gTAB||'If '||vCategory_Str;
                        vPackage_Middle := vPackage_Middle||gTAB_LESS||vInsert_Statement || ';' || chr(10);
                    WHEN 'M' then
                        vPackage_Middle := vPackage_Middle||gTAB||'ElsIf '||vCategory_Str;
                        vPackage_Middle := vPackage_Middle||gTAB_LESS||vInsert_Statement || ';' || chr(10);
                    WHEN 'L' then
                        If instr(vCategory_str,'*') > 0 then
                          vPackage_Middle := vPackage_Middle||gTAB||'-- Default '||chr(10)||gTAB||'Else '||chr(10);
                        Else
                          vPackage_Middle := vPackage_Middle||gTAB||'ElsIf '||vCategory_Str;
                        End If;
                        vPackage_Middle := vPackage_Middle||gTAB_LESS||vInsert_Statement || ';' || chr(10);
                        vPackage_Middle := vPackage_Middle ||gTAB||  CHR(10) ||gTAB||'End If;' ||Chr(10);
                        vPackage_Middle := vPackage_Middle || CHR(10) ||gTAB||'End If;' || chr(10);
                        vFirstRow := 'N';
                   END CASE;

                End If;

            End Loop;
          End If;

          If xxcp_global.gCR.Count = 0 then
            vPackage_Middle := vPackage_Middle ||gTAB||'cInternalErrorCode := 12801; -- No statement' ||chr(10);
          End If;

          If vMaxSource = vSourceCnt then
            vPackage_Middle := vPackage_Middle || chr(10)||'End If;' || chr(10);
          Else
            vPackage_Middle := vPackage_Middle || chr(10)||'  ElsIf ';
          End If;


          End Loop;
        End Loop;

        If  vPackage_Middle is null then
          vPackage_Middle := '    cInternalErrorCode := 12801; -- No statement';
        End If;


        vPackage_Body :=  vPackage_Body|| CHR(10) ||
                          'Begin' || CHR(10) || CHR(10) || vPackage_Middle ||
                         CHR(10) || CHR(10) || pkrec.return_block ||
                         CHR(10) || CHR(10) || 'End ' || pkrec.code_name || ';'|| CHR(10) ;
     End Loop;

        vPackage_Body := 'CREATE OR REPLACE PACKAGE BODY ' || vPackage_Name ||
                         ' AS ' || CHR(10) || CHR(10) || vPackage_Body ||
                         CHR(10) || CHR(10) || 'END ' || vPackage_Name || ';';

        --
        -- Compile Packages!!
        --
        Begin

          If xxcp_forms.Execute_Immediate_Clob(vPackage_Header,'N',vSQLERRM) then
            If xxcp_forms.Execute_Immediate_Clob(vPackage_Body,'N',vSQLERRM) then
              vSuccessful := True;
            Else
              cSQLERRM := vSQLERRM;
            End If;
          Else
            cSQLERRM := vSQLERRM;
          End If;

        Exception
          when OTHERS then
            vSuccessful := False;
            cSQLERRM    := vSQLERRM;
        End;

     -- End Loop;

--    End If;

    Return(vSuccessful);

  End Settlement_DML;  

  --! ***********************************************************************
  --! Process_DML
  --! ***********************************************************************
  Function Process_DML(cPreview_Ctl  in number,
                       cPreview_mode in varchar2 Default 'N',
                       cSQLERRM      out varchar2) Return Boolean is

    g                       Integer;
    vSource_Table           xxcp_sys_source_tables.transaction_table%type;
    vSource_Instance_id     number;
    vSource_id              number;
    vTarget_Table           xxcp_sys_target_tables.transaction_table%type;
    vProcess_History_Column xxcp_sys_target_tables.history_id_column%type;
    vDebug_Source_Table     xxcp_sys_target_tables.transaction_table%type;

    vInternalErrorCode      xxcp_errors.internal_error_code%type;
    vMaxSource              number := 0;
    vSourceCnt              number := 0;
    vSQLERRM                varchar2(512);

    vSuccessful             boolean := False;
    vCategory_Str           varchar2(1000);
    vFirstRow               varchar2(1);

    vPackage_Header         long;
    vPackage_Body           clob;
    vPackage_Middle         clob;
    vPackage                long;
    vInsert_Statement       long;
    vPackage_Name           varchar2(30);

    Cursor POI is
      select header, c.definition_block, c.return_block, c.code_name
        from xxcp_statement_code_b c
       where c.code_name = 'PROCESS_ORACLE_INSERT'
          or c.code_name = 'PROCESS_PASS_THROUGH'
          order by c.code_name;

    Cursor SR is
      Select s.Source_activity, s.source_id, s.name, s.source_base_table
        from xxcp_sys_sources s
       Where s.preview_ctl = cPreview_Ctl
         and s.active = 'Y';

    Cursor TG(pSource_id in number) is
      select t.source_id,
             t.transaction_table target_table,
             nvl(t.history_id_column, 'VT_HISTORY_ID') process_history_column
        from xxcp_sys_target_tables t, xxcp_sys_sources s
       where t.source_id = s.source_id
         and s.active = 'Y'
         and s.preview_ctl = cpreview_ctl
         and s.source_id = psource_id
       order by s.source_id, t.transaction_table;

    vTable_Tag    varchar2(30);
    vInstance_Tag varchar2(30);
    
    vPassThroughRule varchar2(1) := 'N';

  Begin

    xxcp_global.Preview_on := cPreview_mode;
    xxcp_global.Forecast_on := 'N';
    if xxcp_global.User_id is null then
      xxcp_global.User_id    := 0;
    end if;
    xxcp_global.gCommon(1).Current_Request_id := 0;
    
    -- 03.03.13
    xxcp_global.Settlement_on := 'N';  

    If xxcp_global.Preview_on = 'N' then
      -- Never create one for Preview Mode!!!

      Select count(*)
        into vMaxSource
        from xxcp_sys_target_tables t, xxcp_sys_sources s
       where t.source_id = s.source_id
         and s.active = 'Y'
         and s.preview_ctl = cPreview_Ctl;

      vPackage_Name := gPN(cPreview_Ctl).Target;

     vPackage_Header := 'CREATE OR REPLACE PACKAGE ' || vPackage_Name || ' AS ' ;
     For PKRec in POI loop
        vPackage_Header := vPackage_Header || CHR(10) || CHR(10) || pkrec.header;
     End Loop;
     vPackage_Header := vPackage_Header ||CHR(10) || 'END ' || vPackage_Name || ';';


     For PKRec in POI loop

        vPackage_Body :=   CHR(10)||vPackage_Body|| chr(10)|| pkrec.definition_block ||Chr(10)|| Chr(10)|| ' vFnd varchar2(1) := ''N'';'|| CHR(10) || CHR(10);

        vSourceCnt := 0;
        xxcp_global.gCR.delete;
        xxcp_global.gCR_CNT := 0;
        vPackage_Middle := Null;

        For SRRec in SR Loop

          vSource_Table := SRRec.source_base_table;

          For TGRec in TG(SRRec.Source_id) loop

            vTarget_Table           := TGRec.Target_Table;
            vProcess_History_Column := TGRec.Process_History_Column;

            vSource_id := SRRec.Source_id;
            xxcp_global.gCommon(1).current_Source_activity := SRRec.Source_Activity;
            xxcp_global.gCommon(1).Source_id := SRRec.Source_id;

            xxcp_global.gCR.delete;
            xxcp_global.gCR_CNT := 0;

            If pkRec.Code_Name = 'PROCESS_ORACLE_INSERT' then
               vPassThroughRule := 'N';
               xxcp_memory_pack.LoadColumnRules(
                                             cSource_id           => vSource_id,
                                             cSource_Table        => vSource_Table,
                                             cSource_Instance_id  => vSource_Instance_id,
                                             cTarget_Table        => vTarget_Table,
                                             cRequest_id          => 0,
                                             cInternalErrorCode   => vInternalErrorCode);

                   vTable_Tag    := 'cTarget_Table';
                   vInstance_Tag := 'cTarget_Instance_id';

            Else

                  vPassThroughRule := 'Y';
                  xxcp_memory_pack.LoadPassThroughRules(
                                             cSource_id           => vSource_id,
                                             cSource_Table        => vSource_Table,
                                             cSource_Instance_id  => vSource_Instance_id,
                                             cTarget_Table        => vTarget_Table,
                                             cRequest_id          => 0,
                                             cPassThroughRules    => 'Y',
                                             cInternalErrorCode   => vInternalErrorCode);

                   vTable_Tag    := 'cSource_Table';
                   vInstance_Tag := 'cSource_Instance_id';

            End If;

            SET_IF_THEN_ELSE_CONSTRUCT;

            vSourceCnt := vSourceCnt + 1;

            If vSourceCnt = 1 then
              vPackage_Middle := vPackage_Middle ||
                                 '  If xxcp_global.gCommon(1).Source_id = ' ||to_char(vSource_id);
                                 
                                 If vPassThroughRule = 'N' then
                                   vPackage_Middle := vPackage_Middle ||
                                     ' AND cI1009_Array(145) != ''Y''';
                                 End If;
                                 
                                 vPackage_Middle := vPackage_Middle ||
                                   ' AND '||vTable_Tag||' = ''' ||
                                 upper(vTarget_Table) || ''' then ' ||
                                 CHR(10);
            Else
              vPackage_Middle := vPackage_Middle ||
                                 ' xxcp_global.gCommon(1).Source_id = ' || to_char(vSource_id);
                                 
                                 If vPassThroughRule = 'N' then
                                   vPackage_Middle := vPackage_Middle ||
                                     ' AND cI1009_Array(145) != ''Y''' ;
                                 End If;
                                 
                                 vPackage_Middle := vPackage_Middle ||
                                 ' AND '||vTable_Tag||' = ''' ||
                                 upper(vTarget_Table) || ''' then ' ||
                                 CHR(10);
            End If;

            vPackage_Middle := vPackage_Middle || '    -- ' || SRRec.Name || CHR(10);

            If xxcp_global.gCR.Count > 0 then

              vFirstRow := 'Y';

              For G in 1 .. xxcp_global.gCR.Count Loop

                vInsert_Statement := substr(xxcp_global.gCR(G).Direct_DML_statement,1,32600);
                vInsert_Statement := Replace(vInsert_Statement, ':M0', 'cSource_Rowid');
                vInsert_Statement := Replace(vInsert_Statement, ':J0', 'cParent_Rowid');
                vInsert_Statement := Replace(vInsert_Statement, ':M15', 'cProcess_History_id');

                If xxcp_global.gCR(G).Rule_id = 1 Then
                  
                  If xxcp_global.gCR(G).Target_Instance_id = -1 then                    
                    vPackage_Middle := vPackage_Middle || CHR(10) || gTAB||'If vFnd = ''N'' then ' || CHR(10);
                  Else                  
                    vPackage_Middle := vPackage_Middle || CHR(10) ||
                    gTAB||'If '||vInstance_Tag||' = ' ||  to_char(xxcp_global.gCR(G).Target_Instance_id)|| ' then ' || CHR(10);
                  End If; 
                   
                  vPackage_Middle := vPackage_Middle || gTAB_LESS||vInsert_Statement || ';' || chr(10);
                  vPackage_Middle := vPackage_Middle || CHR(10) ||gTAB|| '   vFnd := ''Y''; '||gTAB ||CHR(10)||gTAB||'End If;';
                  
                  vFirstRow := 'N';
                Else
                   vCategory_Str := ''''||xxcp_global.gCR(G).Rule_Category||''' = cI1009_Array(62) then'||chr(10);

                   If vFirstRow = 'N' then
                     vFirstRow := 'Y';
                     xxcp_global.gCR(G).Rule_Key := 'F'||substr(xxcp_global.gCR(G).Rule_Key,2,30);
                   End If;

                   CASE Substr(xxcp_global.gCR(G).Rule_Key,1,1)
                    WHEN 'F' then
                       
                        If xxcp_global.gCR(G).Target_Instance_id = -1 then   
                          vPackage_Middle := vPackage_Middle ||
                           'If vFnd = ''N'' then ' || CHR(10) || CHR(10);                                                  
                        Else                   
                          vPackage_Middle := vPackage_Middle ||
                           'If '||vInstance_Tag||' = ' ||to_char(xxcp_global.gCR(G).Target_Instance_id) || ' then ' || CHR(10) || CHR(10);                        
                        End If;
                        
                        vPackage_Middle := vPackage_Middle||gTAB||'If '||vCategory_Str;
                        vPackage_Middle := vPackage_Middle||gTAB_LESS||vInsert_Statement || ';' || chr(10);
                        vPackage_Middle := vPackage_Middle||gTAB_LESS||'   vFnd := ''Y''; ' || chr(10);
                    WHEN 'M' then
                        vPackage_Middle := vPackage_Middle||gTAB||'ElsIf '||vCategory_Str;
                        vPackage_Middle := vPackage_Middle||gTAB_LESS||vInsert_Statement || ';' || chr(10);
                    WHEN 'L' then
                        If instr(vCategory_str,'*') > 0 then
                          vPackage_Middle := vPackage_Middle||gTAB||'-- Default '||chr(10)||gTAB||'Else '||chr(10);
                        Else
                          vPackage_Middle := vPackage_Middle||gTAB||'ElsIf '||vCategory_Str;
                        End If;
                        vPackage_Middle := vPackage_Middle||gTAB_LESS||vInsert_Statement || ';' || chr(10);

                        vPackage_Middle := vPackage_Middle ||gTAB||  CHR(10) ||gTAB||'End If;' ||Chr(10);
                        vPackage_Middle := vPackage_Middle||gTAB_LESS||'   vFnd := ''Y''; ' || chr(10);

                        vPackage_Middle := vPackage_Middle || CHR(10) ||gTAB||'End If;' || chr(10);
                        vFirstRow := 'N';
                   END CASE;
                End If;
            End Loop;
          End If;

          If xxcp_global.gCR.Count = 0 then
            vPackage_Middle := vPackage_Middle ||gTAB||'cInternalErrorCode := 12801; -- No statement' ||chr(10);
          End If;

          If vMaxSource = vSourceCnt then
            vPackage_Middle := vPackage_Middle || chr(10)||'End If;' || chr(10);
          Else
            vPackage_Middle := vPackage_Middle || chr(10)||'  ElsIf ';
          End If;

          End Loop;
        End Loop;

        If  vPackage_Middle is null then
          vPackage_Middle := '    cInternalErrorCode := 12801; -- No statement';
        End If;

        vPackage_Body :=  vPackage_Body|| CHR(10) ||
                          'Begin' || CHR(10) || CHR(10) || vPackage_Middle ||
                         CHR(10) || CHR(10) || pkrec.return_block ||
                         CHR(10) || CHR(10) || 'End ' || pkrec.code_name || ';'|| CHR(10) ;
     End Loop;

        vPackage_Body := 'CREATE OR REPLACE PACKAGE BODY ' || vPackage_Name ||
                         ' AS ' || CHR(10) || CHR(10) || vPackage_Body ||
                         CHR(10) || CHR(10) || 'END ' || vPackage_Name || ';';

        --
        -- Compile Packages!!
        --
        Begin

          If xxcp_forms.Execute_Immediate_Clob(vPackage_Header,'N',vSQLERRM) then
            If xxcp_forms.Execute_Immediate_Clob(vPackage_Body,'N',vSQLERRM) then
              vSuccessful := True;
            Else
              cSQLERRM := vSQLERRM;
            End If;
          Else
            cSQLERRM := vSQLERRM;
          End If;

        Exception
          when OTHERS then
            vSuccessful := False;
            cSQLERRM    := vSQLERRM;
        End;

    End If;
     
    -- Create Forecast DML Package if applicable
    If (vSuccessful and gPN(cPreview_Ctl).Forecast_Target != 'NULL') then
       vSuccessful := Forecast_DML(cPreview_Ctl, vSQLERRM);
    End if;
    
    -- Create Settlement DML Package if applicable
    if (vSuccessful and gPN(cPreview_Ctl).settlement_target != 'NULL') then
       vSuccessful := Settlement_DML(cPreview_Ctl, 
                                     'N',
                                     vSQLERRM);
    end if;    

    -- Create Settlement Preview DML Package if applicable
    if (vSuccessful and gPN(cPreview_Ctl).settlement_preview != 'NULL') then
       vSuccessful := Settlement_DML(cPreview_Ctl, 
                                     'Y',
                                     vSQLERRM);
    end if;       

    Return(vSuccessful);

  End Process_DML;

  --! ***********************************************************************
  --! Process_PTR Pass Through
  --! ***********************************************************************
  Function Process_PTR(cPreview_Ctl  in number,
                       cPreview_mode in varchar2 Default 'N',
                       cSQLERRM      out varchar2) Return Boolean is

    g                       Integer;
    vSource_Table           xxcp_sys_source_tables.transaction_table%type;
    vSource_Instance_id     number;
    vSource_id              number;
    vTarget_Table           xxcp_sys_target_tables.transaction_table%type;
    vProcess_History_Column xxcp_sys_target_tables.history_id_column%type;
    vDebug_Source_Table     xxcp_sys_target_tables.transaction_table%type;

    vInternalErrorCode      xxcp_errors.internal_error_code%type;
    vMaxSource              number := 0;
    vSourceCnt              number := 0;
    vSQLERRM                varchar2(512);

    vSuccessful             boolean := False;
    vCategory_Str           varchar2(1000);
    vFirstRow               varchar2(1);

    vPackage_Header         long;
    vPackage_Body           clob;
    vPackage_Middle         clob;
    vPackage                long;
    vInsert_Statement       long;
    vPackage_Name           varchar2(30);

    Cursor POI is
      select header, c.definition_block, c.return_block, c.code_name
        from xxcp_statement_code_b c
       where c.code_name = 'PROCESS_PASS_THROUGH';

    Cursor SR is
      Select s.Source_activity, s.source_id, s.name, s.source_base_table
        from xxcp_sys_sources s
       Where s.preview_ctl = cPreview_Ctl
         and s.active = 'Y';

    Cursor TG(pSource_id in number) is
      select t.source_id,
             t.transaction_table target_table,
             nvl(t.history_id_column, 'VT_HISTORY_ID') process_history_column
        from xxcp_sys_target_tables t, xxcp_sys_sources s
       where t.source_id = s.source_id
         and s.active = 'Y'
         and s.preview_ctl = cpreview_ctl
         and s.source_id = psource_id
       order by s.source_id, t.transaction_table;

  Begin

    xxcp_global.Preview_on := cPreview_mode;
    if xxcp_global.User_id is null then
      xxcp_global.User_id    := 0;
    end if;
    xxcp_global.gCommon(1).Current_Request_id := 0;

    If xxcp_global.Preview_on = 'N' then
      -- Never create one for Preview Mode!!!

      Select count(*)
        into vMaxSource
        from xxcp_sys_target_tables t, xxcp_sys_sources s
       where t.source_id = s.source_id
         and s.active = 'Y'
         and s.preview_ctl = cPreview_Ctl;

      vPackage_Name := gPN(cPreview_Ctl).PassThrough;

      For PKRec in POI loop

        vPackage_Header := 'CREATE OR REPLACE PACKAGE ' || vPackage_Name ||
                           ' AS ' || CHR(10) || CHR(10) || pkrec.header ||
                           CHR(10) || 'END ' || vPackage_Name || ';';

        For SRRec in SR Loop

          vSource_Table := SRRec.source_base_table;

          For TGRec in TG(SRRec.Source_id) loop

            vTarget_Table           := TGRec.Target_Table;
            vProcess_History_Column := TGRec.Process_History_Column;

            vSource_id := SRRec.Source_id;
            xxcp_global.gCommon(1).current_Source_activity := SRRec.Source_Activity;
            xxcp_global.gCommon(1).Source_id := SRRec.Source_id;

            xxcp_global.gCR.delete;
            xxcp_global.gCR_CNT := 0;

            xxcp_memory_pack.LoadPassThroughRules(
                                             cSource_id           => vSource_id,
                                             cSource_Table        => vSource_Table,
                                             cSource_Instance_id  => vSource_Instance_id,
                                             cTarget_Table        => vTarget_Table,
                                             cRequest_id          => 0,
                                             cPassThroughRules    => 'Y',
                                             cInternalErrorCode   => vInternalErrorCode);

            SET_IF_THEN_ELSE_CONSTRUCT ;

            vSourceCnt := vSourceCnt + 1;

            If vSourceCnt = 1 then
              vPackage_Middle := vPackage_Middle ||
                                 '  If xxcp_global.gCommon(1).Source_id = ' ||
                                 to_char(vSource_id) ||
                                 ' AND cTarget_Table = ''' ||
                                 upper(vTarget_Table) || ''' then ' ||
                                 CHR(10);
            Else
              vPackage_Middle := vPackage_Middle ||
                                 ' xxcp_global.gCommon(1).Source_id = ' ||
                                 to_char(vSource_id) ||
                                 ' AND cTarget_Table = ''' ||
                                 upper(vTarget_Table) || ''' then ' ||
                                 CHR(10);
            End If;

            vPackage_Middle := vPackage_Middle || '    -- ' || SRRec.Name || CHR(10);

            If xxcp_global.gCR.Count > 0 then

              vFirstRow := 'Y';

              For G in 1 .. xxcp_global.gCR.Count Loop

                vInsert_Statement := substr(xxcp_global.gCR(G).Direct_DML_statement,1,32600);
                vInsert_Statement := Replace(vInsert_Statement, ':M15', 'cProcess_History_id');

                If xxcp_global.gCR(G).Rule_id = 1 Then
                  vPackage_Middle := vPackage_Middle || CHR(10) ||
                  gTAB||'If cTarget_Instance_id = ' ||
                   to_char(xxcp_global.gCR(G).Target_Instance_id) || ' then ' || CHR(10);
                  vPackage_Middle := vPackage_Middle || gTAB_LESS||vInsert_Statement || ';' || chr(10);
                  vPackage_Middle := vPackage_Middle || CHR(10) ||gTAB||'End If;';
                  vFirstRow := 'N';
                Else
                   vCategory_Str := ''''||xxcp_global.gCR(G).Rule_Category||''' = cI1009_Array(62) then'||chr(10);

                   If vFirstRow = 'N' then
                     vFirstRow := 'Y';
                     xxcp_global.gCR(G).Rule_Key := 'F'||substr(xxcp_global.gCR(G).Rule_Key,2,30);
                   End If;

                   CASE Substr(xxcp_global.gCR(G).Rule_Key,1,1)
                    WHEN 'F' then
                        vPackage_Middle := vPackage_Middle || CHR(10) ||
                        gTAB||'If cTarget_Instance_id = ' ||
                        to_char(xxcp_global.gCR(G).Target_Instance_id) || ' then ' || CHR(10) || CHR(10);
                        vPackage_Middle := vPackage_Middle||gTAB||'If '||vCategory_Str;
                        vPackage_Middle := vPackage_Middle||gTAB_LESS||vInsert_Statement || ';' || chr(10);
                    WHEN 'M' then
                        vPackage_Middle := vPackage_Middle||gTAB||'ElsIf '||vCategory_Str;
                        vPackage_Middle := vPackage_Middle||gTAB_LESS||vInsert_Statement || ';' || chr(10);
                    WHEN 'L' then
                        vPackage_Middle := vPackage_Middle||gTAB||'-- Default '||chr(10)||gTAB||'Else '||chr(10);
                        vPackage_Middle := vPackage_Middle||gTAB_LESS||vInsert_Statement || ';' || chr(10);
                        vPackage_Middle := vPackage_Middle ||gTAB||  CHR(10) ||gTAB||'End If;' ||Chr(10);
                        vPackage_Middle := vPackage_Middle || CHR(10) ||gTAB||'End If;' || chr(10);
                        vFirstRow := 'N';
                   END CASE;

                End If;

            End Loop;
          End If;

          If xxcp_global.gCR.Count = 0 then
            vPackage_Middle := vPackage_Middle ||gTAB||'cInternalErrorCode := 12801; -- No statement' ||chr(10);
          End If;

          If vMaxSource = vSourceCnt then
            vPackage_Middle := vPackage_Middle || chr(10)||'End If;' || chr(10);
          Else
            vPackage_Middle := vPackage_Middle || chr(10)||'  ElsIf ';
          End If;


          End Loop;
        End Loop;

        If  vPackage_Middle is null then
          vPackage_Middle := '    cInternalErrorCode := 12801; -- No statement';
        End If;

        vPackage_Body := 'CREATE OR REPLACE PACKAGE BODY ' || vPackage_Name ||
                         ' AS ' || CHR(10) || CHR(10) ||
                         pkrec.definition_block || CHR(10) || CHR(10) ||
                         'Begin' || CHR(10) || CHR(10) || vPackage_Middle ||
                         CHR(10) || CHR(10) || pkrec.return_block ||
                         CHR(10) || CHR(10) || 'End ' || pkrec.code_name || ';' ||
                         CHR(10) || CHR(10) || 'END ' || vPackage_Name || ';';

        --
        -- Compile Packages!!
        --
        Begin

          If xxcp_forms.Execute_Immediate_Clob(vPackage_Header,'N',vSQLERRM) then
            If xxcp_forms.Execute_Immediate_Clob(vPackage_Body,'N',vSQLERRM) then
              vSuccessful := True;
            Else
              cSQLERRM := vSQLERRM;
            End If;
          Else
            cSQLERRM := vSQLERRM;
          End If;

        Exception
          when OTHERS then
            vSuccessful := False;
            cSQLERRM    := vSQLERRM;
        End;

      End Loop;

    End If;

    Return(vSuccessful);

  End Process_PTR;

  --! ***********************************************************************
  -- Validate_Column_Rules
  --
  -- The column rules should not use Extened values for one Accounted or Entered fields
  -- and use none extended for others in the same source.
  --! ***********************************************************************
  Function Validate_Column_Rules(cPreview_ctl  in number,
                                 cMessage     out varchar2) return varchar2 is


  Cursor wr1(pPreview_ctl in number) is
   select sc.source_name, source_id
    from xxcp_sys_sources sc
      where sc.preview_ctl        = pPreview_ctl
        and substr(sc.active,1,1) = 'Y';

  Cursor wr2(pSource_id in number) is
   select sc.source_id, t.instance_id, r.transaction_table, t.rule_id, decode(instr(column_definition,'{P9023}'),0,'N','Y') Acct_Exted_Bal, sc.Source_name
       from xxcp_column_rules r,
            xxcp_column_rules_tables t,
            xxcp_sys_sources sc
      where sc.source_id          = pSource_id
        and t.source_id           = sc.source_id
        and substr(sc.active,1,1) = 'Y'
        and t.rule_id             = r.rule_id
        and t.transaction_table   = r.transaction_table
        and r.column_name         in ('ACCOUNTED_CR')
        and t.transaction_table   = any(select x.transaction_table from xxcp_sys_target_tables x where x.target_rounding = 'Y' and x.source_id = t.source_id)
        order by sc.source_id, r.transaction_table, instance_id;

    Cursor wr3(pRule_id in number, pExtended in varchar2) is
        select count(*) cnt
        from xxcp_column_rules r
        where r.rule_id    = pRule_id
         and r.transaction_table   = any(select x.transaction_table from xxcp_sys_target_tables x where x.target_rounding = 'Y' and x.source_id = r.source_id)
         and r.column_name in ('ACCOUNTED_CR','ACCOUNTED_DR','ENTERED_CR','ENTERED_DR')
         and instr(column_definition,
               Decode(r.column_name,'ACCOUNTED_CR',Decode(pExtended,'Y','{P9023}','{P9083}'),
                                    'ACCOUNTED_DR',Decode(pExtended,'Y','{P9023}','{P9083}'),
                                    'ENTERED_CR'  ,Decode(pExtended,'Y','{P9021}','{P9081}'),
                                    'ENTERED_DR'  ,Decode(pExtended,'Y','{P9021}','{P9081}'))) > 0;

    Cursor wr4(pSource_id in number, pExtended in varchar2) is
        select count(*) cnt
        from xxcp_column_rules r
        where r.Source_id    = pSource_id
         and r.transaction_table   = any(select x.transaction_table from xxcp_sys_target_tables x where x.target_rounding = 'Y' and x.source_id = r.source_id)
         and r.column_name in ('ACCOUNTED_CR','ACCOUNTED_DR','ENTERED_CR','ENTERED_DR')
         and instr(column_definition,
               Decode(r.column_name,'ACCOUNTED_CR',Decode(pExtended,'Y','{P9023}','{P9083}'),
                                    'ACCOUNTED_DR',Decode(pExtended,'Y','{P9023}','{P9083}'),
                                    'ENTERED_CR'  ,Decode(pExtended,'Y','{P9021}','{P9081}'),
                                    'ENTERED_DR'  ,Decode(pExtended,'Y','{P9021}','{P9081}'))) > 0;

  vPass           varchar2(1) := 'Y';
  vMsg            varchar2(500);
  vCN             Number;
  vInvertExtended Varchar2(1);

  Begin
    -- Open Source Information
    For rec1 in wr1(cPreview_ctl) loop

      For rec2 in wr2(rec1.source_id) loop

        If rec2.Acct_Exted_Bal = 'Y' then
         vInvertExtended:= 'N';
        Else
         vInvertExtended := 'Y';
        End If;

        vCN := 0;
        -- Test Accounted Amount
        -- At Rule Level
        For rec3 in wr3(rec2.rule_id,vInvertExtended ) loop
          vCN := rec3.cnt;
        End Loop;

        -- If we have found records that dont match the first record we should stop processing
        If vCN > 0 then
          vPass := 'N';
          vMsg  := ' at Rule Level';
        End If;

        -- Test Accounted Amount
        -- At Source Level
        If vPass = 'Y' then
          For rec4 in wr4(rec1.source_id,vInvertExtended ) loop
            vCN := rec4.cnt;
          End Loop;
          -- If we have found records that dont match the first record we should stop processing
          If vCN > 0 then
            vPass := 'N';
            vMsg  := ' at Source Level';
          End If;

        End If;

      End Loop; -- WR2

      If vPass = 'N' then
       vMsg := Rec1.source_name||' failed balancing check'||vMsg||'. You are mixing extended and none extended internal dynamic attributes in your Accounted or Entered values.';
       exit; -- exit loop;
      End If;

    End Loop; -- WR1

    cMessage := vMsg;

    Return(vPass);

  End Validate_Column_Rules;

  --! ***********************************************************************
  --! Generate Column Rules
  --! ***********************************************************************
  FUNCTION Generate_Column_Rules(cPreview_Ctl in number, cMessage out varchar2) return varchar2 is

  vSQLERRM varchar2(2000);
  vPass    varchar2(1) := 'N';
  vMsg     varchar2(2000);

  Cursor PACKAGE_STATUS(pDML_Name in varchar2, pHist_Name in varchar2, pPV_Name in varchar2) is
  select Object_Name, Object_Type, 1 Type
    from All_objects
   where object_name in (pDML_Name, pHist_Name,pPV_Name,gPN(cPreview_Ctl).settlement_target,gPN(cPreview_Ctl).settlement_preview)
     and object_type in ('PACKAGE','PACKAGE BODY')
     and status = 'INVALID';

Begin

    If xxcp_statement_code.Column_Validation(cPreview_ctl,vSQLERRM) then
      
      If cPreview_Ctl = 1 then 
        xxcp_global.Set_Journal_Preview('Y');
        If xxcp_statement_code.Process_History(cPreview_ctl,'J',vSQLERRM) then
           vPass := vPass;
        End If;
      End If;
      xxcp_global.Set_Journal_Preview('N');
    
      If xxcp_statement_code.Process_History(cPreview_ctl,'Y',vSQLERRM) then
       If xxcp_statement_code.Process_History(cPreview_ctl,'N',vSQLERRM) then
         If xxcp_statement_code.Process_DML(cPreview_ctl,'N',vSQLERRM) then
             vPass := 'Y';
         End If;
       End If;
     End If;
    End If;

    If vMsg is null then
      vPass := 'N';

      If cPreview_Ctl between 1 and gPN_Cnt then
        -- Check Package failures
         For PKRec in PACKAGE_STATUS(gPN(cPreview_Ctl).Target,gPN(cPreview_Ctl).History,gPN(cPreview_Ctl).Preview) loop
              vMsg  := 'Invalid Package: ';
             vMsg  := vMsg  ||CHR(10)||PKRec.Object_Name||chr(10)||PKRec.Object_Type||chr(10)||vSQLERRM;
        End Loop;
      End If;
    End If;

    Begin
       update xxcp_sys_sources s set s.DML_COMPILED = vPass
      where s.preview_ctl = cPreview_Ctl;
      Commit;
    End;

    cMessage := vMsg;

    Return(vPass);
End Generate_Column_Rules;

  --! ***********************************************************************
  --! Init
  --! ***********************************************************************
  Procedure Init is
   Begin
    Load_Package_Names;
   End Init;

Begin
   Init;
END XXCP_STATEMENT_CODE;
/
