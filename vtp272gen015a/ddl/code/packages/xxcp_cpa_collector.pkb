CREATE OR REPLACE Package Body XXCP_CPA_COLLECTOR is
  /******************************************************************************
                          V I R T A L  T R A D E R  L I M I T E D
              (Copyright 2005-2009 Virtual Trader Ltd. All rights Reserved)

     NAME:       XXCP_CPA_COLLECTOR
     PURPOSE:    Cost-Plus Collector

     REVISIONS:
     Ver        Date      Author  Description
     ---------  --------  ------- ------------------------------------
     02.05.00   04/12/08  Simon   First Release
     02.05.01   20/02/09  Keith   Added Period Dates maker
     02.05.02   26/02/09  Simon   Fix "FORALL INDICES" issue - not supported in 9i
     02.05.03   27/02/09  Simon   Prevent insert of un-initialized collections.
     02.05.04   31/03/09  Simon   Fix Doubling of amounts on extract (cc.instance_id).
     02.05.05   01/04/09  Simon   Only Target Acct Currency to be collected per Set of Books.
     02.05.06   01/05/09  Simon   Add Category Data Source.
                                  Fix Incorrect Source Assignments link in collector.
     02.05.07   19/05/09  Simon   Collector only to run for Active records.
     02.05.08   21/05/09  Simon   True Up Interface to set vt_transaction_date as vTrue_Up_End_Date.
     02.05.09   10/06/09  Simon   Modified to make 1 pass of gl_balances.
                                  Added Trace mode.
     02.05.10   11/06/09  Simon   Modify Transaction Type = STD.
     02.05.11   06/07/09  Simon   Remove Cost_Category from Actuals.
     02.05.12   19/08/09  Simon   Add Leading Hint to cGl_Balances cursor.
                                  Only Create TU Interface records where effective collector
                                  records exist. ie take into account the offset.
     02.05.13   24/08/09  Simon   Add Summarize functionality.
     02.05.14   27/08/09  Simon   Fix Inverting of Account for prior summarized records.
     02.05.15   02/09/09  Simon   Fix CTL cursor only looking at period_set_name_id=1.
                                  Modify cGl_Balances to add collector_id desc to order clause.
     02.05.16   18/05/10  Simon   Fix AZC-247062. nvl incorrect on effective_to_date.
     02.05.17   30/11/11  Simon   Fix OVX-831445. Interface records not created if no actuals.
     02.05.18   29/06/12  Simon   JRA-993-20882 Truncate temp table to increase performance.
     03.06.01   17/09/12  Simon   Cost_Plus_Set_Id changes (OOD-293).
     03.06.02   12/12/12  Simon   Sym Changes.
  ******************************************************************************/

  bulk_errors          exception;
  unique_error         exception;
  interface_exists     exception;
  PRAGMA EXCEPTION_INIT (bulk_errors,  -24381);     -- Catch Bulk Binding Errors
  PRAGMA EXCEPTION_INIT (unique_error, -00001);     -- Catch Unique constraint Errors
  PRAGMA EXCEPTION_INIT (interface_exists, -20001); -- Catch Interface Exists Errors

  gCompany_Seg    Number(15) := null;
  gDepartment_Seg Number(15) := null;
  gAccount_Seg    Number(15) := null;
  gExtra_Seg1     Number(15) := null;
  gExtra_Seg2     Number(15) := null;
  gExtra_Seg3     Number(15) := null;

  gTrace_On          Boolean := FALSE;
  gRejected_Int      number  := 0;
  gInterface_Warning boolean := false;
  
  gAssign_Category    Boolean := FALSE;
  gTax_Agreement_Mode Boolean := FALSE;
  gSource_Assignment_id Number;
  
  TYPE tab_cost_plus_int is TABLE of XXCP_cost_plus_interface%ROWTYPE
                            INDEX BY BINARY_INTEGER;

  TYPE tab_actual_costs is TABLE of XXCP_actual_costs%ROWTYPE
                           INDEX BY BINARY_INTEGER;

  TYPE rec_Actual_Interface is RECORD (Forecast_Indx number,
                                       True_Up_Indx  number);

  TYPE tab_Actual_Interface is TABLE of rec_Actual_Interface
                               INDEX BY BINARY_INTEGER;


  TYPE tab_1d_var IS TABLE OF VARCHAR2(1) INDEX BY varchar2(25);
  TYPE tab_2d_var IS TABLE OF tab_1d_var INDEX BY varchar2(25);
  TYPE tab_3d_var IS TABLE OF tab_2d_var INDEX BY varchar2(25);

  TYPE tab_1d_num IS TABLE OF number INDEX BY varchar2(25);
  TYPE tab_2d_num IS TABLE OF tab_1d_num INDEX BY varchar2(25);
  TYPE tab_3d_num IS TABLE OF tab_2d_num INDEX BY varchar2(25);

  t_tu_interface tab_3d_var;
  t_fc_interface tab_3d_var;
  t_ac_costs     tab_3d_num;

  t_cost_plus_int    tab_cost_plus_int;
  t_actual_costs     tab_actual_costs;
  t_Actual_Interface tab_Actual_Interface;

  TYPE rec_gl_balances is RECORD (Period_Name          varchar2(15),
                                  Period_id            Number,
                                  Period_end_date      Date,
                                  Source_Assignment_id Number,
                                  Currency_code        Varchar2(15),
                                  Company              Varchar2(30),
                                  Department           Varchar2(30),
                                  Account              Varchar2(30),
                                  Period_Net_DR        Number,
                                  Period_Net_CR        Number,
                                  Forecast             Varchar2(1),
                                  True_Up              Varchar2(1),
                                  Summarize_Account    Varchar2(1),
                                  Account_To           Varchar2(30),
                                  Cost_Plus_Set_Id     Number,
                                  Cost_Category        Varchar2(50),
                                  Extra_Segment1       Varchar2(30),
                                  Extra_Segment2       Varchar2(30),
                                  Extra_Segment3       Varchar2(30)
                                  );

  TYPE tab_gl_balances is TABLE of rec_gl_balances INDEX BY BINARY_INTEGER;

  TYPE rec_collector_tmp is RECORD (CODE_COMBINATION_ID  NUMBER(15),
                                    PERIOD_NAME          VARCHAR2(15),
                                    SET_OF_BOOKS_ID      NUMBER(15),
                                    SOURCE_ASSIGNMENT_ID NUMBER(10),
                                    COMPANY              VARCHAR2(25),
                                    DEPARTMENT           VARCHAR2(25),
                                    ACCOUNT              VARCHAR2(25),
                                    CHART_OF_ACCOUNTS_ID NUMBER,
                                    INSTANCE_ID          NUMBER,
                                    FORECAST             VARCHAR2(1),
                                    TRUE_UP              VARCHAR2(1),
                                    CURRENCY_CODE        VARCHAR2(15),
                                    PERIOD_SET_NAME_ID   NUMBER(2),
                                    PERIOD_ID            NUMBER,
                                    PERIOD_END_DATE      DATE,
                                    SUMMARIZE_ACCOUNT    VARCHAR2(1),
                                    ACCOUNT_FROM         VARCHAR2(25),
                                    ACCOUNT_TO           VARCHAR2(25),
                                    COST_PLUS_SET_ID     NUMBER,
                                    COMPANY_LIST         VARCHAR2(30),
                                    DEPARTMENT_LIST      VARCHAR2(30),
                                    ACCOUNT_LIST         VARCHAR2(30),
                                    SUMMARIZE_DEPARTMENT VARCHAR2(1),
                                    DEPARTMENT_FROM      VARCHAR2(25),
                                    DEPARTMENT_TO        VARCHAR2(25),
                                    COST_CATEGORY        VARCHAR2(50),
                                    EXTRA_SEGMENT1       VARCHAR2(30),
                                    EXTRA_SEGMENT2       VARCHAR2(30),
                                    EXTRA_SEGMENT3       VARCHAR2(30),
                                    SUMMARIZE_EXTRA_SEGMENT1 VARCHAR2(1),
                                    SUMMARIZE_EXTRA_SEGMENT2 VARCHAR2(1),
                                    SUMMARIZE_EXTRA_SEGMENT3 VARCHAR2(1)
);

  TYPE tab_collector_tmp is TABLE of rec_collector_tmp INDEX BY BINARY_INTEGER;

  t_gl_balances     tab_gl_balances;
  t_collector_tmp   tab_collector_tmp;

  -- *************************************************************
  --
  --                     Software Version Control
  --
  -- This package can be checked with SQL to ensure it's installed
  -- select packagename.software_version from dual;
  -- *************************************************************
  Function Software_Version RETURN VARCHAR2 IS
  BEGIN
    RETURN('Version [03.06.02] Build Date [12-DEC-2012] Name [XXCP_CPA_COLLECTOR]');
  END Software_Version;

  --
  -- Retrieve Chart of Account Segments
  --
  Function Retrieve_COA_Segments (cChart_of_accounts_id IN number) return number is

  begin
    -- Get Segments
    select company,department,account,
           extra_segment1,extra_segment2,extra_segment3
    into   gCompany_Seg,gDepartment_Seg,gAccount_Seg,
           gExtra_Seg1,gExtra_Seg2,gExtra_Seg3
    from   XXCP_cost_plus_mapping
    where  chart_of_accounts_id = cChart_of_accounts_id
    and    active =  'Y';

    return(0);

  Exception
    when too_many_rows then
      xxcp_foundation.show('Error retrieving COA segment - Too many Rows. <'||cChart_of_accounts_id||'>');
      return(3);
    when no_data_found then
      xxcp_foundation.show('Error retrieving COA segment - No Data Found. <'||cChart_of_accounts_id||'>');
      return(3);
    when others then
      xxcp_foundation.show('Error retrieving COA segment - '||SQLERRM||' <'||cChart_of_accounts_id||'>');
      return(3);

  End Retrieve_COA_Segments;

  --
  -- Actual Exists Function
  --
  Function Actual_Exists (cCollection_Period_id number,
                          cPeriod_Set_Name_id   number,
                          cCompany              varchar2,
                          cDepartment           varchar2,
                          cAccount              varchar2,
                          cCurrency_Code        varchar2,
                          cSource_Assignment_id number) return boolean is

    cursor c1 is
                 select 'X'
                 from   XXCP_actual_costs f
                 where  f.collection_period_id = cCollection_Period_id
                 and    f.company              = cCompany
                 and    f.department           = cDepartment
                 and    f.account              = cAccount
                 and    f.source_assignment_id = cSource_Assignment_id
                 and    f.currency_code        = cCurrency_Code
                 and    f.period_set_name_id = cPeriod_Set_Name_id
                 ;

--
-- NOTE!!! INDEX REQUIRED
--

    vResult Varchar2(1) := null;

  Begin

    open  c1;
    fetch c1 into vResult;
    close c1;

    if vResult is not null then
      return TRUE;
    else
      return FALSE;
    end if;

  End Actual_Exists;

  --
  -- Populate Actual Costs record Procedure
  --
  Procedure Populate_CP_Actual_Costs(cRequest_id           IN  number,
                                     cCollection_Period_ID IN  number,
                                     cPeriod_set_id        IN  number,
                                     cCompany              IN  varchar2,
                                     cDepartment           IN  varchar2,
                                     cAccount              IN  varchar2,
                                     cPeriod_Net_DR        IN  number,
                                     cPeriod_Net_CR        IN  number,
                                     cCurrency_Code        IN  varchar2,
                                     cSource_Assignment_id IN  varchar2,
                                     cPeriod_End_Date      IN  date,
                                     cCurrent_Period       IN  varchar2,
                                     cSummarize_Account    IN  varchar2,
                                     cAccount_To           IN  varchar2,
                                     cCost_Plus_Set_Id     IN  number,
                                     cCost_Category        IN  varchar2,
                                     cExtra_segment1       IN  varchar2,
                                     cExtra_segment2       IN  varchar2,
                                     cExtra_segment3       IN  varchar2,
                                     cTransaction_ID      OUT  number) is
    vIndx          number;
    vActual_Exists boolean := FALSE;
  begin

    If cCurrent_Period = 'Y' and NOT gTax_Agreement_Mode then
      -- Has duplicate record already been inserted into array?
      BEGIN
        cTransaction_Id := t_ac_costs (cCompany)(cDepartment)(cAccount);
        vActual_Exists  := TRUE;
      EXCEPTION
        WHEN NO_DATA_FOUND then
          -- Take next number off stack
          select xxcp_cost_plus_trx_id.nextval
          into   cTransaction_ID
          from   dual;
      END;
      t_ac_costs (cCompany)(cDepartment)(cAccount) := cTransaction_ID;
    Else
      select xxcp_cost_plus_trx_id.nextval
      into   cTransaction_ID
      from   dual;    
    End if;


    If not vActual_Exists then
      vIndx := t_actual_costs.count + 1;

    -- Construct Record
      t_actual_costs(vIndx).Transaction_ID       := cTransaction_ID;
      t_actual_costs(vIndx).Request_id           := cRequest_id;
      t_actual_costs(vIndx).Collection_Period_id := cCollection_Period_id;
      t_actual_costs(vIndx).Period_set_name_id   := cPeriod_set_id;

      t_actual_costs(vIndx).period_end_date      := cPeriod_End_Date;

      t_actual_costs(vIndx).Company              := cCompany;
      t_actual_costs(vIndx).Department           := cDepartment;
      t_actual_costs(vIndx).Account              := cAccount;
      t_actual_costs(vIndx).Account_To           := cAccount_To;
      t_actual_costs(vIndx).Period_Net_DR        := cPeriod_Net_DR;
      t_actual_costs(vIndx).Period_Net_CR        := cPeriod_Net_CR;
      t_actual_costs(vIndx).Currency_Code        := cCurrency_Code;
      t_actual_costs(vIndx).Source_Assignment_id := cSource_Assignment_id;
      t_actual_costs(vIndx).Summarize_Account    := cSummarize_Account;
      t_actual_costs(vIndx).Creation_Date        := xxcp_global.Systemdate;
      t_actual_costs(vIndx).Created_by           := xxcp_global.user_id;
      t_actual_costs(vIndx).Last_Update_login    := xxcp_global.Login_id;
      
      t_actual_costs(vIndx).Cost_Plus_Set_Id     := cCost_Plus_Set_Id;

      t_actual_costs(vIndx).Extra_Segment1       := cExtra_Segment1;
      t_actual_costs(vIndx).Extra_Segment2       := cExtra_Segment2;
      t_actual_costs(vIndx).Extra_Segment3       := cExtra_Segment3;

      If gAssign_Category then
        t_actual_costs(vIndx).Cost_Category      := cCost_Category;  
      End if;

    End if;
  End Populate_CP_Actual_Costs;

  --
  -- Populate Interface Procedure
  --
  Procedure Populate_CP_Cost_Plus_Int(cVT_transaction_table    varchar2,
                                      cVT_transaction_type     varchar2,
                                      cVT_transaction_class    varchar2,
                                      cCompany                 varchar2,
                                      cDepartment              varchar2,
                                      cAccount                 varchar2,
                                      cCollection_Period_id    number,
                                      cCurrency_Code           varchar2,
                                      cVT_source_assignment_ID number,
                                      cVT_transaction_id       number,
                                      cVT_transaction_date     date,
                                      cAct_Indx                number,
                                      cPeriod_Set_Name_id      number,
                                      cPeriod_id               number,
                                      cCost_Plus_Set_Id        number,
                                      cTax_Agreement_Number    varchar2 default null
                                      ) is
    vIndx   number;
    vResult varchar2(1) := '';
  begin
     
     --
     -- Check If Interface Record has already been populated.
     --
     If NOT gTax_Agreement_Mode then

       BEGIN
         If cVT_transaction_table='FORECAST' then 
           vResult := t_fc_interface (cCompany)(cDepartment)(cAccount);
         ElsIf cVT_transaction_table='TRUE-UP' then
           vResult := t_tu_interface (cCompany)(cDepartment)(cAccount);
         End if;
         Raise interface_exists;
       EXCEPTION
         WHEN NO_DATA_FOUND THEN
           -- No Interface Record so continue.
           null;
       END;
            
     End if;
     
     -- Insert into Array before bulk Insert
     vIndx := t_cost_plus_int.count + 1;
     t_cost_plus_int(vIndx).vt_status := 'NEW';
     t_cost_plus_int(vIndx).vt_source_assignment_id := cVT_source_assignment_id;
     t_cost_plus_int(vIndx).vt_transaction_table    := cVT_transaction_table;
     t_cost_plus_int(vIndx).vt_transaction_type     := cVT_transaction_type;
     t_cost_plus_int(vIndx).vt_transaction_class    := cVT_transaction_class;

     If gTax_Agreement_Mode then
       -- TA Mode
       t_cost_plus_int(vIndx).vt_transaction_ref      := cCompany||'-'||cTax_Agreement_Number;
       t_cost_plus_int(vIndx).Tax_Agreement_Number    := cTax_Agreement_Number;
     Else
       -- Standard
       t_cost_plus_int(vIndx).vt_transaction_ref      := cCompany||'-'||cDepartment||'-'||cAccount||'-'||cCurrency_Code;
     End if;
     
     t_cost_plus_int(vIndx).vt_transaction_id       := cVT_transaction_id;
     t_cost_plus_int(vIndx).vt_transaction_date     := cVT_transaction_date;
     t_cost_plus_int(vIndx).vt_request_id           := 0;
     t_cost_plus_int(vIndx).Creation_Date           := xxcp_global.Systemdate;
     t_cost_plus_int(vIndx).Created_by              := xxcp_global.user_id;
     t_cost_plus_int(vIndx).Last_Update_login       := xxcp_global.Login_id;
     t_cost_plus_int(vIndx).Period_Set_Name_id      := cPeriod_Set_Name_id;
     t_cost_plus_int(vIndx).posting_period_id       := cPeriod_id;
     t_cost_plus_int(vIndx).Collection_period_id    := cCollection_Period_id;
     t_cost_plus_int(vIndx).Cost_Plus_Set_id        := cCost_Plus_Set_id;


     If NOT gTax_Agreement_Mode then
       -- Hold Linking Record for Actual and Interface (Required if Actual Rejected)
       if cVT_transaction_table = 'FORECAST' then
         t_Actual_Interface(cAct_Indx).Forecast_Indx := vIndx;
         t_fc_interface (cCompany)(cDepartment)(cAccount) := 'Y';

       else

         t_Actual_Interface(cAct_Indx).True_Up_Indx := vIndx;
         t_tu_interface (cCompany)(cDepartment)(cAccount) := 'Y';
       end if;
     End if;

     --t_Interface_Actual(vIndx) := cAct_Indx;
  Exception
    When Interface_Exists then
      -- Skip processing
      null;
  End populate_cp_cost_plus_int;

  --
  -- Bulk Insert Actual Costs
  --
  Procedure Insert_Bulk_Actual_Costs (cRejected_count out number) is

    vError_indx number;
    vFC_indx    number;
    vTU_indx    number;
    vRejected   number := 0;

  BEGIN

    -- BULK INSERT
    if t_actual_costs.COUNT > 0 then                            -- 02.05.03
      FORALL i IN 1..t_actual_costs.COUNT SAVE EXCEPTIONS
      INSERT INTO XXCP_actual_costs VALUES t_actual_costs(i);
    end if;
--    RETURNING transaction_id bulk collect into t_transaction_id;  -- Return Transaction_ID into array
    -- Note
    -- This is using a consecutive indice for transaction_id
    -- ie if a row is rejected the indice is not skipped. Any way to prevent this? Workaround?
    
    cRejected_count := vRejected;

  EXCEPTION
    WHEN bulk_errors then
      --xxcp_foundation.show(' Actuals Rejected (Records Already Exist): '||SQL%BULK_EXCEPTIONS.COUNT);
      cRejected_count := SQL%BULK_EXCEPTIONS.COUNT; 
     FOR indx in 1..SQL%BULK_EXCEPTIONS.COUNT
      LOOP

        -- Delete Actual
        t_actual_costs.delete(SQL%BULK_EXCEPTIONS(indx).ERROR_INDEX);

        --
        -- 02.05.17 Do not create
        --
        
        -- Reset indices
        vError_indx := null;
        vFC_indx    := null;
        vTU_indx    := null;

        vError_indx := SQL%BULK_EXCEPTIONS(indx).ERROR_INDEX;

        -- If actual has a linked interface record
        if t_actual_interface.exists(vError_indx) then 
          vFC_indx := t_actual_interface(vError_indx).forecast_indx;
          vTU_indx := t_actual_interface(vError_indx).true_up_indx;
          
          -- Delete Forecast interface array record
          if vFC_indx is not null then
            t_cost_plus_int.delete(vFC_indx);
            gRejected_Int := gRejected_Int + 1;
          end if;

          -- Delete True Up interface array record
          if vTU_indx is not null then
            t_cost_plus_int.delete(vTU_indx);
            gRejected_Int := gRejected_Int + 1;
          end if;
        end if;
      END LOOP;
  END Insert_Bulk_Actual_Costs;

  --
  -- Bulk Insert Interface
  --
  Procedure Insert_Bulk_Interface (cRejected_count out number)  is
    vRejected number := 0;
  BEGIN
    --Assign transaction_id
--    FORALL i IN INDICES of t_cost_plus_int SAVE EXCEPTIONS

    if t_cost_plus_int.count > 0 then                                             -- 02.05.03
      FORALL i IN t_cost_plus_int.first..t_cost_plus_int.last SAVE EXCEPTIONS     -- 02.05.02
      INSERT INTO XXCP_cost_plus_interface VALUES t_cost_plus_int(i);
    end if;

    vRejected := gRejected_Int;
    --xxcp_foundation.show(' Interface Rejected : '||vRejected);
    cRejected_count := vRejected;
  EXCEPTION
    WHEN bulk_errors then
      --02.05.17
      vRejected := nvl(gRejected_Int,0) + SQL%BULK_EXCEPTIONS.COUNT;

      --xxcp_foundation.show('Interface Rejected : '||vRejected);
      cRejected_count := vRejected;


      FOR indx in 1..SQL%BULK_EXCEPTIONS.COUNT
      LOOP

          --xxcp_foundation.fndwriteerror(004,'Error Bulk Inserting Interface <'||indx||'> - '||SQLERRM(-1 * SQL%BULK_EXCEPTIONS(indx).error_code));



        if SQL%BULK_EXCEPTIONS(indx).error_code <> 22160 then -- Element at index does not exist (Expected).
          xxcp_foundation.show('Error Bulk Inserting Interface <'||indx||'> - '||SQLERRM(-1 * SQL%BULK_EXCEPTIONS(indx).error_code)||
                               ' for iteration '||SQL%BULK_EXCEPTIONS(indx).ERROR_INDEX||' : '||
                               t_gl_balances(SQL%BULK_EXCEPTIONS(indx).ERROR_INDEX).company||'/'||
                               t_gl_balances(SQL%BULK_EXCEPTIONS(indx).ERROR_INDEX).department||'/'||
                               t_gl_balances(SQL%BULK_EXCEPTIONS(indx).ERROR_INDEX).account
                               );
        end if;


        -- Delete Rejected record
      END LOOP;
  END Insert_Bulk_Interface;

  -- Period_Marker
  Procedure Period_Marker(cSource_Group_id in number, cPeriod_Name in varchar2, cPeriod_Set_Name_id in number, cPeriod_id in number) is

    vResult boolean := FALSE;

  Begin

    Begin

       Insert into XXCP_cost_plus_periods (
          PERIOD_SET_NAME_ID,
          SOURCE_GROUP_ID,
          PERIOD_ID,
          PERIOD_NAME,
          COLLECTED,
          CREATION_DATE,
          CREATED_BY
          )
        Values (cPeriod_Set_Name_id, cSource_Group_id, cPeriod_id, cPeriod_Name, 'Y', sysdate, fnd_global.USER_ID);

     Exception when others then
         vResult := False;

     End;

  End Period_Marker;

  --
  -- Main Entry
  --
  Function Control(cSource_Group_ID      IN NUMBER,
                   cCalendar_id          IN NUMBER,
                   cForecast_Period_Name IN VARCHAR2, -- Forecast Period Name
                   cConc_Request_id      IN NUMBER,
                   cCurrent_Period_Only  IN VARCHAR2 default 'N',
                   cCost_Plus_Set_Id     IN NUMBER default null) return Number is

   -- Collector Control
   cursor CTL(pCalendar_id              in number, 
              pForecast_Period_End_Date in date, 
              pTrue_Up_Period_End_Date  in date,
              pCost_Plus_Set_Id         in number) is
              select  max(collector_id) collector_id,
                      chart_of_accounts_id,
                      company_from,
                      company_to,
                      case when company_from is not null then
                        'RANGE'
                      else
                        'LIST'
                      end comp_range_list,
                      department_from,
                      department_to,
                      case when department_from is not null then
                        'RANGE'
                      else
                        'LIST'
                      end dept_range_list,
                      account_from,
                      account_to,
                      case when account_from is not null then
                        'RANGE'
                      else
                        'LIST'
                      end acct_range_list,
                      max(forecast)     forecast,
                      max(true_up)      true_up,
                      period_set_name_id,           
                      instance_id,
                      max(summarize_account) summarize_account,  
                      max(data_source) data_source,
                      cost_plus_set_id,
                      company_list,
                      department_list,
                      account_list,
                      extra_segment1_from,
                      extra_segment1_to,
                      case 
                        when extra_segment1_from is not null then
                          extra_segment1_from||'-'||extra_segment1_to
                        when extra_segment1_list is not null then
                          extra_segment1_list
                      end extra_segment1,
                      case 
                        when extra_segment2_from is not null then
                          extra_segment2_from||'-'||extra_segment2_to
                        when extra_segment2_list is not null then
                          extra_segment2_list
                      end extra_segment2,                      
                      case 
                        when extra_segment3_from is not null then
                          extra_segment3_from||'-'||extra_segment3_to
                        when extra_segment3_list is not null then
                          extra_segment3_list
                      end extra_segment3,
                      extra_segment1_list,
                      extra_segment2_from,
                      extra_segment2_to,
                      extra_segment2_list,
                      extra_segment3_from,
                      extra_segment3_to,
                      extra_segment3_list,
                      max(summarize_department) summarize_department,
                      max(summarize_extra_segment1) summarize_extra_segment1,
                      max(summarize_extra_segment2) summarize_extra_segment2,
                      max(summarize_extra_segment3) summarize_extra_segment3,
                      max(cost_category) cost_category,  
                      nvl(lead(company_from) over ( order by cost_plus_set_id,chart_of_accounts_id, company_from, max(summarize_account),
                                          decode(max(data_source),'VT','0',max(data_source)), 
                                          max(collector_id) desc),-999) next_company_from,
                      nvl(lead(cost_plus_set_id) over ( order by cost_plus_set_id,chart_of_accounts_id, company_from, max(summarize_account),
                                          decode(max(data_source),'VT','0',max(data_source)), 
                                          max(collector_id) desc),-999) next_cost_plus_set_id
              from (
                    --FORECAST
                    select  c.collector_id,
                            c.chart_of_accounts_id,
                            c.company_from,
                            c.company_to,
                            c.department_from,
                            c.department_to,
                            c.account_from,
                            c.account_to,
                            c.forecast,
                            'N' true_up,
                            c.period_set_name_id,           -- Calendar
                            c.instance_id,
                            --c.effective_from_date,          -- 02.5.12
                            nvl(c.summarize_account,'N') summarize_account,  -- 02.5.13
                            c.data_source,
                            c.cost_plus_set_id,
                            c.company_list,
                            c.department_list,
                            c.account_list,
                            c.extra_segment1_from,
                            c.extra_segment1_to,
                            c.extra_segment1_list,
                            c.extra_segment2_from,
                            c.extra_segment2_to,
                            c.extra_segment2_list,
                            c.extra_segment3_from,
                            c.extra_segment3_to,
                            c.extra_segment3_list,
                            nvl(c.summarize_department,'N') summarize_department,
                            nvl(c.summarize_extra_segment1,'N') summarize_extra_segment1,
                            nvl(c.summarize_extra_segment2,'N') summarize_extra_segment2,
                            nvl(c.summarize_extra_segment3,'N') summarize_extra_segment3,
                            c.cost_category
                     from   xxcp_instance_cpa_collector_v c
                     where  pForecast_Period_End_Date >= effective_from_date
                       and  pForecast_Period_End_Date <= nvl(effective_to_date,to_date('31-DEC-2999','DD-MON-YYYY')) -- 02.05.16
                       and  c.period_set_name_id = pCalendar_id -- 02.05.15
                       and  c.active = 'Y'
                       and  c.cost_plus_set_id = nvl(pCost_Plus_Set_id,c.cost_plus_set_id) 
                     UNION ALL
                     --TRUE UP
                    select  c.collector_id,
                            c.chart_of_accounts_id,
                            c.company_from,
                            c.company_to,
                            c.department_from,
                            c.department_to,
                            c.account_from,
                            c.account_to,
                            'N' forecast,
                            c.true_up,
                            c.period_set_name_id,           -- Calendar
                            c.instance_id,
                            --c.effective_from_date,          -- 02.5.12
                            nvl(c.summarize_account,'N') summarize_account,  -- 02.5.13
                            c.data_source,
                            c.cost_plus_set_id,
                            c.company_list,
                            c.department_list,
                            c.account_list,
                            c.extra_segment1_from,
                            c.extra_segment1_to,
                            c.extra_segment1_list,
                            c.extra_segment2_from,
                            c.extra_segment2_to,
                            c.extra_segment2_list,
                            c.extra_segment3_from,
                            c.extra_segment3_to,
                            c.extra_segment3_list,
                            nvl(c.summarize_department,'N') summarize_department,
                            nvl(c.summarize_extra_segment1,'N') summarize_extra_segment1,
                            nvl(c.summarize_extra_segment2,'N') summarize_extra_segment2,
                            nvl(c.summarize_extra_segment3,'N') summarize_extra_segment3,
                            c.cost_category
                     from   xxcp_instance_cpa_collector_v c
                     where  pTrue_Up_Period_End_Date >= effective_from_date
                       and  pTrue_Up_Period_End_Date <= nvl(effective_to_date,to_date('31-DEC-2999','DD-MON-YYYY')) -- 02.05.16
                       and  c.period_set_name_id = pCalendar_id -- 02.05.15
                       and  c.active = 'Y'
                       and  c.cost_plus_set_id = nvl(pCost_Plus_Set_id,c.cost_plus_set_id) 
                    )
              group by chart_of_accounts_id,
                       company_from,
                       company_to,
                       department_from,
                       department_to,
                       account_from,
                       account_to,
                       period_set_name_id,           
                       instance_id,
                       cost_plus_set_id,
                       company_list,
                       department_list,
                       account_list,
                       extra_segment1_from,
                       extra_segment1_to,
                       extra_segment1_list,
                       extra_segment2_from,
                       extra_segment2_to,
                       extra_segment2_list,
                       extra_segment3_from,
                       extra_segment3_to,
                       extra_segment3_list
              order by cost_plus_set_id,chart_of_accounts_id, company_from, summarize_account,
                       decode(data_source,'VT','0',data_source), -- Order by VT first then other data_sources
                       collector_id desc;

   -- gl period details
   cursor c_gl_periods (pCalendar_id in number, pMin_Period_id in number, pMax_Period_id in number) is
                  select period_name, (w.period_year*100)+w.period_num Period_id, w.End_Date
                   from XXCP_INSTANCE_GL_PERIODS_v w
                  where (w.period_year*100)+w.period_num between pMin_Period_id and pMax_Period_id
                    and w.period_set_name_id = pCalendar_id
                  order by period_year desc,period_num desc;

   vPeriod_set_id XXCP_lookups.numeric_code%type := 1;  -- default

   TYPE rec_gl_periods is RECORD (period_name          Varchar2(15),
                                  period_id            Number,
                                  end_date             date
                                  );

   TYPE tab_gl_periods is table of rec_gl_periods INDEX by BINARY_INTEGER;

   t_gl_periods               tab_gl_periods;

    -- Forecast Accouting Offset
    Cursor TUPO is
      select Profile_Value Offset
        from XXCP_SYS_PROFILE
       where profile_category = 'CP'
       and   profile_name = 'TRUE-UP PERIOD OFFSET';

    -- Collector min periods default
    Cursor CLD is
      select Profile_Value Collector_Periods
        from XXCP_SYS_PROFILE
       where profile_category = 'CP'
       and   profile_name = 'COLLECTOR MIN PERIODS DEFAULT';

    -- Collector min periods default
    Cursor TRC is
      select Profile_Value Collector_Periods
        from XXCP_SYS_PROFILE
       where profile_category = 'CP'
       and   profile_name = 'TRACE'
       and   upper(profile_value) = 'ON';

    -- Assign Category to Actual Costs
    Cursor CAT is
      select Profile_Value Collector_Periods
        from XXCP_SYS_PROFILE
       where profile_category = 'CP'
       and   profile_name = 'COLLECTOR ASSIGN CATEGORY'
       and   profile_value = 'Y';

    -- Assign Category to Actual Costs
    Cursor TAM is
      select Profile_Value Tax_Agreement_Mode
        from XXCP_SYS_PROFILE
       where profile_category = 'CP'
       and   profile_name = 'TAX AGREEMENT MODE'
       and   profile_value = 'Y';

    -- Assign Category to Actual Costs
    Cursor SA is
      select Profile_Value Source_Assignment_id
        from XXCP_SYS_PROFILE
       where profile_category = 'CP'
       and   profile_name = 'SOURCE ASSIGNMENT ID';
              
    Cursor cInt is
      select attribute1 tax_agreement_number,
             attribute2 payee
      from   xxcp_cust_data
      where  category_name = 'TAX AGREEMENT DETAILS'
      and    trunc(sysdate) between nvl(effective_from_date,'01-JAN-1900') and nvl(effective_to_date,'01-JAN-2900')
      order  by attribute7;  -- Seq


             
   -- Period
   cursor pd(pSource_group_id in number, pPeriod_Name in varchar2, pInstance_id in number, pPeriod_Set_Name_id in number) is
     select  (f.period_year*100)+f.period_num Period_id,
             f.end_date
     from xxcp_instance_gl_periods_v f,
          XXCP_source_assignment_groups t
     where f.period_name        = pPeriod_Name
       and f.Instance_id        = t.instance_id
       and t.source_group_id    = pSource_group_id
       and t.instance_id        = pInstance_id
       and f.Period_Set_Name_id = pPeriod_Set_Name_id;

   cursor cGl_Balances is
     select /*+ leading(t) */
            t.period_name,t.period_id,t.period_end_date,t.source_assignment_id, b.currency_code, t.company 
           ,decode(max(t.summarize_department),'N',min(t.department)
                                                  ,nvl(max(t.department_list),min(t.department_from)||'-'||max(t.department_to))) department   -- Hold From and To in Department column.
           ,decode(max(t.summarize_account),'N',min(t.account)||'-'||min(t.account)
                                               ,nvl(max(t.account_list),min(t.account_from)||'-'||max(t.account_to))) account   -- Hold From and To in Account column.
           ,sum(b.Period_net_dr),sum(b.Period_net_cr), t.Forecast, t.True_Up, max(t.summarize_account)
           ,decode(max(t.summarize_account),'N',min(t.account),max(t.account_to)) account_to
           ,t.cost_plus_set_id
           ,max(t.cost_category) cost_category
           ,max(t.extra_segment1) extra_segment1
           ,max(t.extra_segment2) extra_segment2
           ,max(t.extra_segment3) extra_segment3           
     from xxcp_instance_cpa_balances_v b,
          XXCP_cost_plus_collector_tmp t
     where b.code_combination_id = t.code_combination_id
     and b.period_name           = t.period_name
     and b.set_of_books_id       = t.set_of_books_id
     and b.instance_id           = t.instance_id
     and b.actual_flag           = 'A'
     and length(b.currency_code) = 3
     and b.currency_code = t.currency_code
     group by t.period_name,t.period_id,t.period_end_date,t.source_assignment_id, b.currency_code, t.company,
              t.Forecast,t.True_Up,
              decode(t.summarize_department,'Y',nvl(t.department_list,t.department_from||'-'||t.department_to),t.department),
              decode(t.summarize_account,'Y',nvl(t.account_list,t.account_from||'-'||t.account_to),t.account),
              t.extra_segment1,
              t.extra_segment2,
              t.extra_segment3,
              t.cost_plus_set_id;

   vJob_Status                pls_integer := 0;

   vTrue_Up_Period_Off        number(3);

   vSource_Activity           XXCP_sys_sources.source_activity%type;
   vSource_Group_id           XXCP_source_assignment_groups.source_group_id%type;

   vRequest_id                Number;
   vUser_id                   Number := fnd_global.user_id;
   vSysdate                   Date   := sysdate;
   vLogin_id                  Number := fnd_global.login_id;
   vSession_id                Number := fnd_global.session_id;

   vPrev_COA                  Number(15) := -1;
   vSQL                       Varchar2(4000);
   vPrv_Min_Periods           Number := 0;

   vTransaction_ID            Number;

   TYPE cur_typ IS REF CURSOR;

   c           cur_typ;
   rc                         pls_integer := 0;

   vStart_Date date;
   vEnd_Date   date;

   vInstance_id               XXCP_source_assignment_groups.instance_id%type := 0;
   vTmp_Period_Year           gl_periods.period_year%type;
   vTmp_Period_Num            gl_periods.period_num%type := 0;
   vTmp_Start_Date            date;
   vTmp_End_Date              date;
   vTmp_Period_Type           gl_periods.period_type%type;
   vTmp_Period_Name           gl_periods.period_name%type;

   vCollector_Periods         Number := 13; -- Default to 13 periods

   vTrue_UP_Period_id         number := 0;
   vMin_Cal_Period_id         number := 0;
   vForecast_Period_id        number := 0;
   vForecast_End_Date         date;
   vTrue_Up_End_Date          date;

   vCalendar_Name             varchar2(30);
   vCounter                   number := 0;

   cursor cal( pCalendar_id in number) is
     select *
     from xxcp_gl_period_sets_v w
     where w.PERIOD_SET_NAME_ID = pCalendar_id;

   vTrue_up_Period_Name     XXCP_cost_plus_periods.period_name%type;
   vTrue_up_period_end_date date;


   vPeriod_From_id number;
   vPeriod_To_id   number;
   
   vPrv_Cost_Plus_Set_id number        := -999;


   
   --
   -- INSERT_ENTITY PROCEDURE
   --
   Procedure Insert_Entity (cEntity           IN VARCHAR2,
                            cCost_Plus_Set_id IN NUMBER) is

     cursor cSet (pCost_Plus_Set_id number) is 
                    select cost_plus_set_description
                    from   xxcp_cost_plus_sets_v
                    where  cost_plus_set_id = pCost_Plus_Set_id;

     vCost_Plus_Set_Description varchar2(100);


     vCurrent_Period            varchar2(1) := 'N';
     vRejected_Act_Count        number;
     vRejected_Int_Count        number;


   Begin


      if cCurrent_Period_Only = 'N' then
        insert into XXCP_cost_plus_collector_tmp
                        (CODE_COMBINATION_ID,
                         PERIOD_NAME,
                         SET_OF_BOOKS_ID,
                         SOURCE_ASSIGNMENT_ID,
                         COMPANY,
                         DEPARTMENT,
                         ACCOUNT,
                         CHART_OF_ACCOUNTS_ID,
                         INSTANCE_ID,
                         COLLECTOR_ID,
                         FORECAST,
                         TRUE_UP,
                         CURRENCY_CODE,
                         COST_CATEGORY_ID,
                         CATEGORY_DATA_SOURCE,
                         DATA_SOURCE,
                         PERIOD_SET_NAME_ID,
                         PERIOD_ID,
                         PERIOD_END_DATE,
                         SUMMARIZE_ACCOUNT,
                         ACCOUNT_FROM,
                         ACCOUNT_TO,
                         COST_PLUS_SET_ID,
                         COMPANY_LIST,
                         DEPARTMENT_LIST,
                         ACCOUNT_LIST,
                         SUMMARIZE_DEPARTMENT,
                         DEPARTMENT_FROM,
                         DEPARTMENT_TO,
                         COST_CATEGORY,
                         EXTRA_SEGMENT1,
                         EXTRA_SEGMENT2,
                         EXTRA_SEGMENT3)
                  select a.CODE_COMBINATION_ID,
                         p.period_name, --PERIOD_NAME,
                         a.SET_OF_BOOKS_ID,
                         a.SOURCE_ASSIGNMENT_ID,
                         a.COMPANY,
                         a.DEPARTMENT,
                         a.ACCOUNT,
                         a.CHART_OF_ACCOUNTS_ID,
                         a.INSTANCE_ID,
                         a.COLLECTOR_ID,
                         a.FORECAST,
                         a.TRUE_UP,
                         a.CURRENCY_CODE,
                         a.COST_CATEGORY_ID,
                         a.CATEGORY_DATA_SOURCE,
                         a.DATA_SOURCE,
                         a.PERIOD_SET_NAME_ID,
                         (p.period_year*100)+p.period_num, --PERIOD_ID
                         p.END_DATE,
                         a.SUMMARIZE_ACCOUNT,
                         a.ACCOUNT_FROM,
                         a.ACCOUNT_TO,
                         a.COST_PLUS_SET_ID,
                         a.COMPANY_LIST,
                         a.DEPARTMENT_LIST,
                         a.ACCOUNT_LIST,
                         a.SUMMARIZE_DEPARTMENT,
                         a.DEPARTMENT_FROM,
                         a.DEPARTMENT_TO,
                         a.COST_CATEGORY,
                         a.EXTRA_SEGMENT1,
                         a.EXTRA_SEGMENT2,
                         a.EXTRA_SEGMENT3
                  from   XXCP_cost_plus_collector_tmp a,
                         xxcp_instance_gl_periods_v p
                   where p.period_set_name_id   = cCalendar_id
                         and   p.instance_id          = vInstance_id
                         and  (p.period_year*100)+p.period_num between vPeriod_From_id and vPeriod_To_id;
      end if;

      if gTrace_On then
        xxcp_foundation.fndwriteerror(100,'CPA Trace|  <'||cEntity||'> Before GL_BALANCES '||to_char(sysdate,'HH24:MI:SS'));
      end if;
      
      --
      -- NEW CODE
      -- Now join onto GL_BALANCES for single pass
      --
      t_gl_balances.delete;    -- Clear Array

      OPEN  cGl_Balances;
      FETCH cGl_Balances BULK COLLECT INTO t_gl_balances;
      CLOSE cGl_Balances;

      if gTrace_On then
        xxcp_foundation.fndwriteerror(100,'CPA Trace|  <'||cEntity||'> Before pop act costs '||to_char(sysdate,'HH24:MI:SS'));
      end if;
      
      For k in 1..t_gl_balances.count Loop
        If t_gl_balances(k).Period_Name = vTrue_up_Period_Name then
          vCurrent_Period := 'Y';
        Else
          vCurrent_Period := 'N';
        End if;

        --
        -- Unique Index will prevent duplicates
        --
        populate_cp_actual_costs(  cRequest_id           => vRequest_id,
                                   cCollection_Period_ID => t_gl_balances(k).Period_id, --vTrue_UP_Period_id,
                                   cPeriod_set_id        => vPeriod_set_id,
                                   cCompany              => t_gl_balances(k).Company,
                                   cDepartment           => t_gl_balances(k).Department,
                                   cAccount              => t_gl_balances(k).Account,
                                   cPeriod_Net_DR        => t_gl_balances(k).Period_Net_DR,
                                   cPeriod_Net_CR        => t_gl_balances(k).Period_Net_CR,
                                   cCurrency_Code        => t_gl_balances(k).Currency_Code,
                                   cSource_Assignment_id => t_gl_balances(k).Source_Assignment_id,
                                   cPeriod_End_Date      => t_gl_balances(k).Period_End_Date, --vTrue_Up_End_Date
                                   cCurrent_Period       => vCurrent_Period,
                                   cSummarize_Account    => t_gl_balances(k).Summarize_Account,
                                   cAccount_To           => t_gl_balances(k).Account_To,
                                   cCost_Plus_Set_id     => t_gl_balances(k).Cost_Plus_Set_id,
                                   cCost_Category        => t_gl_balances(k).Cost_Category,
                                   cExtra_segment1       => t_gl_balances(k).Extra_Segment1,
                                   cExtra_segment2       => t_gl_balances(k).Extra_Segment2,
                                   cExtra_segment3       => t_gl_balances(k).Extra_Segment3,
                                   cTransaction_ID       => vTransaction_ID                   -- OUT
                                   );


          If vCurrent_Period = 'Y' Then  -- Only Create Interface for current Period
            If NOT gTax_Agreement_Mode then
              If t_gl_balances(k).Forecast = 'Y' Then
                -- Table = FORECAST
                populate_cp_cost_plus_int(cVT_Transaction_Table    => 'FORECAST',
                                          cVT_Transaction_Type     => 'STD', --'ALL',
                                          cVT_Transaction_Class    => 'ALL',
                                          cCompany                 => t_gl_balances(k).Company,
                                          cDepartment              => t_gl_balances(k).Department,
                                          cAccount                 => t_gl_balances(k).Account,
                                          cCollection_Period_id    => vTrue_Up_Period_id,
                                          cCurrency_Code           => t_gl_balances(k).Currency_Code,
                                          cVT_Source_Assignment_id => t_gl_balances(k).Source_Assignment_id,
                                          cVT_Transaction_id       => vTransaction_ID,  -- transaction_id from XXCP_actual_costs
                                          cVT_Transaction_Date     => vForecast_End_Date,
                                          cAct_Indx                => t_actual_costs.count,
                                          cPeriod_Set_Name_id      => vPeriod_set_id,
                                          cPeriod_id               => vForecast_Period_id,
                                          cCost_Plus_Set_Id        => t_gl_balances(k).Cost_Plus_Set_id
                                         );
              End if;

              If t_gl_balances(k).True_Up = 'Y' then 
                -- Table = TRUE-UP
                populate_cp_cost_plus_int(cVT_Transaction_Table    => 'TRUE-UP',
                                          cVT_Transaction_Type     => 'STD', --'ALL',
                                          cVT_Transaction_Class    => 'ALL',
                                          cCompany                 => t_gl_balances(k).Company,
                                          cDepartment              => t_gl_balances(k).Department,
                                          cAccount                 => t_gl_balances(k).Account,
                                          cCollection_Period_id    => vTrue_Up_Period_id,
                                          cCurrency_Code           => t_gl_balances(k).Currency_Code,
                                          cVT_Source_Assignment_id => t_gl_balances(k).Source_Assignment_id,
                                          cVT_Transaction_id       => vTransaction_ID,  -- transaction_id from XXCP_actual_costs
                                          cVT_Transaction_Date     => vTrue_Up_End_Date,-- vForecast_End_Date,
                                          cAct_Indx                => t_actual_costs.count,
                                          cPeriod_Set_Name_id      => vPeriod_set_id,
                                          cPeriod_id               => vForecast_Period_id,
                                          cCost_Plus_Set_Id        => t_gl_balances(k).Cost_Plus_Set_id
                                          );
              End if;
            End if; -- Tax Agreement Mode
          End If;


      End loop;  -- t_gl_balances

      if gTrace_On then
        xxcp_foundation.fndwriteerror(100,'CPA Trace|  <'||cEntity||'> Before Bulk '||to_char(sysdate,'HH24:MI:SS'));
      end if;

      -- New Cost Plus Set?
      If cCost_Plus_Set_id <> vPrv_Cost_Plus_Set_id then
        -- Get Description
        Open cSet(cCost_Plus_Set_id);
        Fetch cSet into vCost_Plus_Set_Description;
        Close cSet;
        
        xxcp_foundation.show('Cost Plus Set <'||vCost_Plus_Set_Description||'>');
      End if;
      
      vPrv_Cost_Plus_Set_id := cCost_Plus_Set_id;
      
      --
      -- Bulk Inserts
      --
      insert_bulk_actual_costs(vRejected_Act_Count);
      xxcp_foundation.show(' <'||cEntity||'> Actuals Count   : '||t_actual_costs.count||
                                                  ' Rejected : '||vRejected_Act_Count);

      insert_bulk_interface(vRejected_Int_Count);
      xxcp_foundation.show(' <'||cEntity||'> Interface Count : '||t_cost_plus_int.count||
                                                  ' Rejected : '||vRejected_Int_count);

      if gTrace_On then
        xxcp_foundation.fndwriteerror(100,'CPA Trace|  <'||cEntity||'> After Bulk '||to_char(sysdate,'HH24:MI:SS'));
      end if;

      -- Clean Up
      --delete from XXCP_cost_plus_collector_tmp;
      Execute Immediate ('truncate table xxcp.xxcp_cost_plus_collector_tmp');
      t_actual_costs.delete;
      t_gl_balances.delete;
      t_cost_plus_int.delete;
      t_Actual_Interface.delete;
      t_collector_tmp.delete;
      t_tu_interface.delete;
      t_fc_interface.delete;
      t_ac_costs.delete;
      gRejected_Int := 0;
      
   End Insert_Entity;

  Begin

    xxcp_foundation.show('Start');

    For Rec in Cal(cCalendar_id) Loop
     vCalendar_Name := rec.period_set_name;
    End Loop;

    -- Identify the source activity
    select s.source_activity, instance_id
    into   vSource_Activity, vInstance_id
    from   XXCP_source_assignment_groups g,
           XXCP_sys_sources s
    where  source_group_id = cSource_Group_id
    and    s.source_id = g.source_id;


    -- Read System Profile for Forecast Period Offset
    For TUPORec in TUPO Loop
      vTrue_Up_Period_Off  := TUPORec.Offset;
    End Loop;

    -- Read System Profile for Default collection periods
    For Rec in CLD Loop
      vCollector_Periods := Rec.Collector_Periods;
    End Loop;

    -- Read System Profile to see if Trace is On.
    gTrace_On := FALSE;
    For Rec in TRC Loop
      gTrace_On := TRUE;
    End Loop;
    
    -- Read System Profile. Assign Category Actual Costs?
    For Rec in CAT Loop
      gAssign_Category := TRUE;
    End Loop;

    -- Tax Agreement Mode
    For Rec in TAM Loop
      gTax_Agreement_Mode := TRUE;
    End Loop;

    -- Source Assignment (Used in Tax Agreement Mode)
    For Rec in SA Loop
      gSource_Assignment_id := Rec.Source_Assignment_Id;
    End Loop;
    
    -- Check profile results
    If nvl(vTrue_Up_Period_Off,0) = 0 or nvl(vCollector_Periods,0) = 0 then
      vJob_Status := 3; -- Report Setup Issue
    ElsIf  nvl(vTrue_Up_Period_Off,0) > nvl(vCollector_Periods,0) then
     vCollector_Periods := abs(nvl(vTrue_Up_Period_Off,0));
    Elsif gTax_Agreement_Mode and gSource_Assignment_id is null then
      xxcp_foundation.show('ERROR: System Profile <SOURCE ASSIGNMENT ID> not found.' );
      vJob_Status := 3; -- Report Setup Issue
    End If;

    --
    -- Concurrent Job locking process to be added here.
    --
    IF NOT xxcp_management.Locked_Process(cSource_Activity => vSource_Activity,
                                          cSource_Group_id => vSource_Group_id,
                                          cTable_Group_id  => 0,
                                          cTrx_Group_id    => 0) AND vJob_Status = 0 THEN

      vRequest_id := xxcp_management.Activate_Lock_Process(cSource_Activity => vSource_Activity,
                                                           cSource_Group_id => cSource_Group_id,
                                                           cConc_Request_Id => cConc_Request_id,
                                                           cTable_Group_id  => 0,
                                                           cTrx_Group_id    => 0);

      xxcp_global.Set_Last_Request_id(vRequest_id);
      xxcp_foundation.show('Activity locked....' || TO_CHAR(vRequest_id));

      xxcp_global.User_id  := fnd_global.user_id;
      xxcp_global.Login_id := fnd_global.login_id;

      xxcp_global.gCommon(1).Current_request_id := vRequest_id;

      -- Get Period / Calendar Details
      vPeriod_set_id := cCalendar_id;

      -- Get Period Id for Forecast
      For pdRec in Pd(pSource_group_id      => cSource_Group_id,
                      pPeriod_Name          => cForecast_Period_Name,
                      pInstance_id          => vInstance_id,
                      pPeriod_Set_Name_id   => cCalendar_id) Loop
        vForecast_Period_id := pdRec.Period_id;
      End Loop;

      xxcp_reporting.period_date_range(cForecast_Period_Name, vStart_Date, vEnd_Date, vCalendar_Name);


      vTrue_up_Period_Name := xxcp_reporting.Get_Period_Offset_Name(cPeriod_id          => vForecast_Period_id,
                                                                    cPeriod_Set_Name_id => cCalendar_id,
                                                                    cPeriods_Offset     => vTrue_Up_Period_Off*-1,
                                                                    cInstance_id        => vInstance_id);
      -- Get Period Id for True-Up
      For pdRec in Pd(pSource_group_id      => cSource_Group_id,
                      pPeriod_Name          => vTrue_up_Period_Name,
                      pInstance_id          => vInstance_id,
                      pPeriod_Set_Name_id   => cCalendar_id) Loop
        vTrue_Up_Period_id       := pdRec.Period_id;
        vTrue_up_period_end_date := pdRec.End_Date;
      End Loop;

      vMin_Cal_Period_id := xxcp_reporting.Get_Period_id_details
                                     (cPeriod_id          => vTrue_Up_Period_id,
                                      cPeriod_Set_Name_id => vPeriod_set_id,
                                      cPeriods_Offset     => vCollector_Periods*-1,
                                      cInstance_id        => vInstance_id,
                                      cPeriod_Name        => vTmp_Period_Name,
                                      cPeriod_Year        => vTmp_Period_Year,
                                      cPeriod_Num         => vTmp_Period_Num,
                                      cStart_Date         => vTmp_Start_Date,
                                      cEnd_Date           => vTmp_End_Date,
                                      cPeriod_Type        => vTmp_Period_Type
                                      );


      xxcp_foundation.Show('True-Up Period : '||vTrue_up_Period_Name||' Calendar Name : '||vCalendar_Name);
      xxcp_foundation.show('Period Start Date : '||vStart_Date||' Min Periods Required :'||vCollector_Periods);

      -- Clear Temporary Table
  --    delete from XXCP_cost_plus_collector_tmp;

      --
      -- Get Prior Period ids
      --

      vPeriod_From_id := xxcp_reporting.Get_Period_Offset(cPeriod_id          => vTrue_Up_Period_id,
                                                          cPeriod_Set_Name_id => cCalendar_id,
                                                          cPeriods_Offset     => vCollector_Periods * -1,
                                                          cInstance_id        => vInstance_id);

      vPeriod_To_id := xxcp_reporting.Get_Period_Offset(cPeriod_id          => vTrue_Up_Period_id,
                                                        cPeriod_Set_Name_id => cCalendar_id,
                                                        cPeriods_Offset     => -1,
                                                        cInstance_id        => vInstance_id);


      --
      -- COMPANY / DEPARTMENT / ACCOUNT
      --
      For CTL_rec in CTL(pCalendar_id              => cCalendar_id, 
                         pForecast_Period_End_Date => vEnd_Date, 
                         pTrue_Up_Period_End_Date  => vTrue_up_period_end_date,
                         pCost_Plus_Set_Id         => cCost_Plus_Set_Id) loop

        vCounter              := vCounter+1;

        if gTrace_On then
          if mod(vCounter,100) = 0 then --every 100 records
            xxcp_foundation.fndwriteerror(100,'CPA Trace|  '||vCounter||' '||to_char(sysdate,'HH24:MI:SS'));
          end if;
        end if;
 
        vJob_Status := 0;
        If vPrev_COA <> ctl_rec.chart_of_accounts_id then
         If Retrieve_COA_Segments(ctl_rec.chart_of_accounts_id) <> 0 Then
           xxcp_foundation.show('Error - Unable to retrieve COA segments <'||ctl_rec.chart_of_accounts_id||'>');
           vJob_Status := 3;
         End if;
        End if;

        vPrev_COA := ctl_rec.chart_of_accounts_id;

        If vJob_Status = 0 then

         -- Populate Prev Periods Array
         If vPrv_Min_Periods <> vCollector_Periods Then

          t_gl_periods.delete;

          --
          -- This gets together the period details for the last 'X' periods based on the min periods required.
          --

          rc := 1;

          For c_gl_periods_rec in c_gl_periods(cCalendar_id, vMin_Cal_Period_id, vTrue_Up_Period_id) loop

            t_gl_periods(rc).period_name := c_gl_periods_rec.period_Name;
            t_gl_periods(rc).period_id   := c_gl_periods_rec.period_id;
            t_gl_periods(rc).end_date    := c_gl_periods_rec.end_Date;

            Period_Marker(cSource_Group_id    => cSource_Group_ID,
                          cPeriod_Name        => t_gl_periods(rc).period_name,
                          cPeriod_Set_Name_id => cCalendar_id ,
                          cPeriod_id          => t_gl_periods(rc).period_id);

            If rc = 1 then

              vTrue_Up_End_Date := c_gl_periods_rec.End_Date;
              vForecast_Period_id := xxcp_reporting.Get_Period_id_details
                                     (cPeriod_id          => vForecast_Period_id,
                                      cPeriod_Set_Name_id => vPeriod_set_id,
                                      cPeriods_Offset     => 0,
                                      cInstance_id        => vInstance_id,
                                      cPeriod_Name        => vTmp_Period_Name,
                                      cPeriod_Year        => vTmp_Period_Year,
                                      cPeriod_Num         => vTmp_Period_Num,
                                      cStart_Date         => vTmp_Start_Date,
                                      cEnd_Date           => vTmp_End_Date,
                                      cPeriod_Type        => vTmp_Period_Type
                                      );
               vForecast_End_Date := vTmp_End_Date;
             End If;

            rc := rc + 1;

          End loop;
         End if;

        vPrv_Min_Periods := vCollector_Periods;
       

      vSQL := '         select cc.code_combination_id,';
      vSQL := vSQL || '       '''||vTrue_up_Period_Name||''','; -- Period Name
      vSQL := vSQL || '       ww.set_of_books_id,';
      vSQL := vSQL || '       ww.source_assignment_id,';
      vSQL := vSQL || '       cc.segment'||gCompany_Seg||',';
      vSQL := vSQL || '       cc.segment'||gDepartment_Seg||',';
      vSQL := vSQL || '       cc.segment'||gAccount_Seg||',';
      vSQL := vSQL || '       '''||ctl_rec.chart_of_accounts_id||''',';     -- Chart of Accounts
      vSQL := vSQL || '       '''||ctl_rec.instance_id||''',';     --ww.instance_id,';
      vSQL := vSQL || '       '''||ctl_rec.forecast||''',';
      vSQL := vSQL || '       '''||ctl_rec.true_up||''',';
      vSQL := vSQL || '       curr.currency_code ,';
      vSQL := vSQL || '       '''||ctl_rec.period_set_name_id||''', ';
      vSQL := vSQL || '       '''||vTrue_UP_Period_Id||''', ';
      vSQL := vSQL || '       '''||vTrue_UP_Period_End_Date||''', ';
      vSQL := vSQL || '       '''||ctl_rec.summarize_account||''', ';
      vSQL := vSQL || '       '''||ctl_rec.account_from||''', ';
      vSQL := vSQL || '       '''||ctl_rec.account_to||''', ';
      vSQL := vSQL || '       '''||ctl_rec.cost_plus_set_id||''', ';
      vSQL := vSQL || '       '''||ctl_rec.company_list||''', ';
      vSQL := vSQL || '       '''||ctl_rec.department_list||''', ';
      vSQL := vSQL || '       '''||ctl_rec.account_list||''', ';
      vSQL := vSQL || '       '''||ctl_rec.summarize_department||''', ';
      vSQL := vSQL || '       '''||ctl_rec.department_from||''', ';
      vSQL := vSQL || '       '''||ctl_rec.department_to||''', ';
      vSQL := vSQL || '       '''||ctl_rec.cost_category||''', ';

      If ctl_rec.summarize_extra_segment1 = 'N' 
      and gExtra_Seg1 is not null 
      and ctl_rec.extra_segment1 is not null then
        vSQL := vSQL || '       cc.segment'||gExtra_Seg1||',';
      Else
        vSQL := vSQL || '       '''||ctl_rec.extra_segment1||''', ';
      End if;

      If ctl_rec.summarize_extra_segment2 = 'N' 
      and gExtra_Seg2 is not null 
      and ctl_rec.extra_segment2 is not null then
        vSQL := vSQL || '       cc.segment'||gExtra_Seg2||',';
      Else
        vSQL := vSQL || '       '''||ctl_rec.extra_segment2||''', ';
      End if;
  
      If ctl_rec.summarize_extra_segment3 = 'N' 
      and gExtra_Seg3 is not null 
      and ctl_rec.extra_segment3 is not null then
        vSQL := vSQL || '       cc.segment'||gExtra_Seg3||',';
      Else
        vSQL := vSQL || '       '''||ctl_rec.extra_segment3||''', ';
      End if;

      vSQL := vSQL || '       '''||ctl_rec.summarize_extra_segment1||''', ';
      vSQL := vSQL || '       '''||ctl_rec.summarize_extra_segment2||''', ';
      vSQL := vSQL || '       '''||ctl_rec.summarize_extra_segment3||''' ';
      vSQL := vSQL || 'from  xxcp_instance_code_comb_v cc,';
      vSQL := vSQL || '      XXCP_source_assignments ww,';
      vSQL := vSQL || '      (select distinct h.target_acct_currency currency_code, r.company_number';
      vSQL := vSQL || '       from  XXCP_sys_target_types t,';
      vSQL := vSQL || '       XXCP_sys_profile p,';
      vSQL := vSQL || '       XXCP_target_assignments h,';
      vSQL := vSQL || '       XXCP_tax_registrations r';
      vSQL := vSQL || '       where t.target_type      = p.profile_value';
      vSQL := vSQL || '       and p.profile_name     = ''TARGET TYPE NAME''';
      vSQL := vSQL || '       and p.profile_category = ''CP''';
      vSQL := vSQL || '       and t.target_type_id   = h.target_type_id';
      vSQL := vSQL || '       and h.reg_id           = r.reg_id';
      vSQL := vSQL || '       and r.cost_plus        = ''Y''';
      vSQL := vSQL || '       and h.active           = ''Y'') curr ';
      vSQL := vSQL || 'where ww.instance_id = cc.instance_id ';
      vSQL := vSQL || 'and   ww.source_group_id = :1 ';
      vSQL := vSQL || 'and   ww.active != ''N'' ';
      vSQL := vSQL || 'and   cc.chart_of_accounts_id=:2 ';
      vSQL := vSQL || 'and   ((cc.segment'||gCompany_Seg||'    between :3 and :4  AND ''RANGE'' = :5)';
      vSQL := vSQL || '       or';
      vSQL := vSQL || '       (cc.segment'||gCompany_Seg||' in (select value from xxcp_cost_plus_lists where list_name = :6)  AND ''LIST'' = :7))';
      vSQL := vSQL || 'and   ((cc.segment'||gDepartment_Seg||' between :8 and :9  AND ''RANGE'' = :10)';
      vSQL := vSQL || '       or';
      vSQL := vSQL || '       (cc.segment'||gDepartment_Seg||' in (select value from xxcp_cost_plus_lists where list_name = :11)  AND ''LIST'' = :12))';
      vSQL := vSQL || 'and   ((cc.segment'||gAccount_Seg||' between :13 and :14  AND ''RANGE'' = :15)';
      vSQL := vSQL || '       or';
      vSQL := vSQL || '       (cc.segment'||gAccount_Seg||' in (select value from xxcp_cost_plus_lists where list_name = :16)  AND ''LIST'' = :17))';
      vSQL := vSQL || 'and   cc.segment'||gCompany_Seg||' = any (select company_number from XXCP_tax_registrations where cost_plus = ''Y'') ';
      vSQL := vSQL || 'and   curr.company_number = cc.segment'||gCompany_Seg||' ';
      vSQL := vSQL || 'and   ww.instance_id = :18 ';

      --
      -- Extra Segments 
      --
      If gExtra_Seg1 is not null then
        If ctl_rec.extra_segment1_from is not null then
          vSQL := vSQL || 'and cc.segment'||gExtra_Seg1||'    between '''||ctl_rec.extra_segment1_from||''' and '''||ctl_rec.extra_segment1_to||''' ';
        Elsif ctl_rec.extra_segment1_list is not null then 
          vSQL := vSQL || 'and cc.segment'||gExtra_Seg1||' in (select value from xxcp_cost_plus_lists where list_name = '''||ctl_rec.extra_segment1_list||''') ';
        End if;
      End if;

      If gExtra_Seg2 is not null then
        If ctl_rec.extra_segment2_from is not null then
          vSQL := vSQL || 'and cc.segment'||gExtra_Seg2||'    between '''||ctl_rec.extra_segment2_from||''' and '''||ctl_rec.extra_segment2_to||''' ';
        Elsif ctl_rec.extra_segment2_list is not null then 
          vSQL := vSQL || 'and cc.segment'||gExtra_Seg2||' in (select value from xxcp_cost_plus_lists where list_name = '''||ctl_rec.extra_segment2_list||''') ';
        End if;
      End if;

      If gExtra_Seg3 is not null then
        If ctl_rec.extra_segment3_from is not null then
          vSQL := vSQL || 'and cc.segment'||gExtra_Seg3||'    between '''||ctl_rec.extra_segment3_from||''' and '''||ctl_rec.extra_segment3_to||''' ';
        Elsif ctl_rec.extra_segment3_list is not null then 
          vSQL := vSQL || 'and cc.segment'||gExtra_Seg3||' in (select value from xxcp_cost_plus_lists where list_name = '''||ctl_rec.extra_segment3_list||''') ';
        End if;
      End if;

      If gTrace_On then
        xxcp_foundation.fndwriteerror(100,'Collector SQL',vSQL);
      End if;

        t_collector_tmp.delete;    -- Clear Array

        -- XXCP_INSTANCE_BALANCES
        OPEN c FOR vSQL USING   cSource_Group_ID,               -- :1
                                ctl_rec.chart_of_accounts_id,
                                ctl_rec.company_from,
                                ctl_rec.company_to,
                                ctl_rec.Comp_Range_List,        -- :5
                                ctl_rec.company_list,
                                ctl_rec.Comp_Range_List,
                                ctl_rec.department_from,
                                ctl_rec.department_to,
                                ctl_rec.Dept_Range_List,        -- :10
                                ctl_rec.department_list,
                                ctl_rec.Dept_Range_List,
                                ctl_rec.account_from,
                                ctl_rec.account_to,
                                ctl_rec.Acct_Range_List,        -- :15
                                ctl_rec.Account_list,
                                ctl_rec.Acct_Range_List,
                                ctl_rec.instance_id             -- :18
                                
                               ;

          FETCH c BULK COLLECT into t_collector_tmp;
        CLOSE c;

        for j in 1..t_collector_tmp.count loop
          begin
            insert into XXCP_cost_plus_collector_tmp(code_combination_id,
                                                   period_name,
                                                   set_of_books_id,
                                                   source_assignment_id,
                                                   company,
                                                   department,
                                                   account,
                                                   chart_of_accounts_id,
                                                   instance_id,
                                                   forecast,
                                                   true_up,
                                                   currency_code,
                                                   period_set_name_id,
                                                   period_id,
                                                   period_end_date,
                                                   summarize_account,
                                                   account_from,
                                                   account_to,
                                                   cost_plus_set_id,
                                                   company_list,
                                                   department_list,
                                                   account_list,
                                                   summarize_department,
                                                   department_from,
                                                   department_to,
                                                   cost_category,
                                                   extra_segment1,
                                                   extra_segment2,
                                                   extra_segment3,
                                                   summarize_extra_segment1,
                                                   summarize_extra_segment2,
                                                   summarize_extra_segment3)
                                            values(t_collector_tmp(j).code_combination_id,
                                                   t_collector_tmp(j).period_name,
                                                   t_collector_tmp(j).set_of_books_id,
                                                   t_collector_tmp(j).source_assignment_id,
                                                   t_collector_tmp(j).company,
                                                   t_collector_tmp(j).department,
                                                   t_collector_tmp(j).account,
                                                   t_collector_tmp(j).chart_of_accounts_id,
                                                   t_collector_tmp(j).instance_id,
                                                   t_collector_tmp(j).forecast,
                                                   t_collector_tmp(j).true_up,
                                                   t_collector_tmp(j).currency_code,
                                                   t_collector_tmp(j).period_set_name_id,
                                                   t_collector_tmp(j).period_id,
                                                   t_collector_tmp(j).period_end_date,
                                                   t_collector_tmp(j).summarize_account,
                                                   t_collector_tmp(j).account_from,
                                                   t_collector_tmp(j).account_to,
                                                   t_collector_tmp(j).cost_plus_set_id,
                                                   t_collector_tmp(j).company_list,
                                                   t_collector_tmp(j).department_list,
                                                   t_collector_tmp(j).account_list,
                                                   t_collector_tmp(j).summarize_department,
                                                   t_collector_tmp(j).department_from,
                                                   t_collector_tmp(j).department_to,
                                                   t_collector_tmp(j).cost_category,
                                                   t_collector_tmp(j).extra_segment1,
                                                   t_collector_tmp(j).extra_segment2,
                                                   t_collector_tmp(j).extra_segment3,
                                                   t_collector_tmp(j).summarize_extra_segment1,
                                                   t_collector_tmp(j).summarize_extra_segment2,
                                                   t_collector_tmp(j).summarize_extra_segment3);
          exception
            when unique_error then
              null;
          end;
        end loop;



        End if;    -- Check vError_code = 0

        If vJob_Status <> 0 then
         Exit;
        End If;

        --
        -- Entity or Cost_Plus_Set_id change. Query Balances and Insert.
        --
        If (CTL_rec.company_from <> CTL_rec.next_company_from
            or
            CTL_rec.cost_plus_set_id <> CTL_rec.next_cost_plus_set_id) then
                  
          Insert_Entity(cEntity           => CTL_rec.company_from,
                        cCost_Plus_Set_id => CTL_rec.cost_plus_set_id);
        end if;


      End Loop;    -- CTL End Loop

      --
      -- Tax Agreement Mode
      --
      If gTax_Agreement_Mode Then


        t_Actual_Interface.delete;
        t_cost_plus_int.delete;

        -- Create Interface records from Seeded Custom Data
        For cInt_rec in cInt loop

          -- Trx Id
          select xxcp_cost_plus_trx_id.nextval
          into   vTransaction_ID
          from   dual;

          -- Dummy Actual Costs
/*          populate_cp_actual_costs(cRequest_id           => vRequest_id,
                                   cCollection_Period_ID => vTrue_Up_Period_id, --vTrue_UP_Period_id,
                                   cPeriod_set_id        => cCalendar_id,
                                   cCompany              => cInt_rec.payee,
                                   cDepartment           => 'X',
                                   cAccount              => 'X',
                                   cPeriod_Net_DR        => 0,
                                   cPeriod_Net_CR        => 0,
                                   cCurrency_Code        => 'X',
                                   cSource_Assignment_id => gSource_Assignment_id,
                                   cPeriod_End_Date      => vTrue_Up_End_Date, --vTrue_Up_End_Date
                                   cCurrent_Period       => vForecast_Period_id,
                                   cSummarize_Account    => 'N',
                                   cAccount_To           => 'X',
                                   cCost_Plus_Set_id     => 0,
                                   cCost_Category        => Null,
                                   cExtra_segment1       => Null,
                                   cExtra_segment2       => Null,
                                   cExtra_segment3       => Null,
                                   cTransaction_ID       => vTransaction_ID                   -- OUT
                                   );*/
          
                    
          -- Forecast 
          Populate_CP_Cost_Plus_Int(cVT_transaction_table    => 'FORECAST',
                                    cVT_transaction_type     => 'STD',
                                    cVT_transaction_class    => 'ALL',
                                    cCompany                 => cInt_rec.payee,
                                    cDepartment              => null,
                                    cAccount                 => null,
                                    cCollection_Period_id    => vTrue_Up_Period_id,
                                    cCurrency_Code           => null,
                                    cVT_source_assignment_ID => gSource_Assignment_id,
                                    cVT_transaction_id       => vTransaction_ID,
                                    cVT_transaction_date     => vForecast_End_Date,
                                    cAct_Indx                => null,
                                    cPeriod_Set_Name_id      => cCalendar_id,
                                    cPeriod_id               => vForecast_Period_id,
                                    cCost_Plus_Set_Id        => 0,
                                    cTax_Agreement_Number    => cInt_rec.tax_agreement_number);          

          -- True Up 
          Populate_CP_Cost_Plus_Int(cVT_transaction_table    => 'TRUE-UP',
                                    cVT_transaction_type     => 'STD',
                                    cVT_transaction_class    => 'ALL',
                                    cCompany                 => cInt_rec.payee,
                                    cDepartment              => null,
                                    cAccount                 => null,
                                    cCollection_Period_id    => vTrue_Up_Period_id,
                                    cCurrency_Code           => null,
                                    cVT_source_assignment_ID => gSource_Assignment_id,
                                    cVT_transaction_id       => vTransaction_ID,
                                    cVT_transaction_date     => vTrue_up_End_Date,
                                    cAct_Indx                => null,
                                    cPeriod_Set_Name_id      => cCalendar_id,
                                    cPeriod_id               => vForecast_Period_id,
                                    cCost_Plus_Set_Id        => 0,
                                    cTax_Agreement_Number    => cInt_rec.tax_agreement_number);          
  
        End Loop;

        dbms_output.put_line('Interface Records : '||t_cost_plus_int.count);

        -- Insert TA Dummy Actual records
        --Insert_bulk_actual_costs(vCounter);

        -- Insert TA Interface records
        Insert_bulk_interface(vCounter);

      End if;

      xxcp_management.DeActivate_lock_process(cRequest_id      => vRequest_id,
                                              cSource_Activity => vSource_Activity,
                                              cSource_Group_id => cSource_Group_id
                                              );

    Else
      vJob_Status := 1;
    END IF;

    -- Clean Up Arrays
    t_actual_costs.delete;
    t_gl_balances.delete;
    t_cost_plus_int.delete;
    t_cost_plus_int.delete;
    t_Actual_Interface.delete;
    t_collector_tmp.delete;
    t_tu_interface.delete;
    t_fc_interface.delete;
    t_ac_costs.delete;
    
    commit;
    return(vJob_Status);
  End control;

End XXCP_CPA_COLLECTOR;
/
