create or replace package xxcp_vertex_ws is
  /******************************************************************************
                          V I R T A L  T R A D E R  L I M I T E D
              (Copyright 2005-2012 Virtual Trader Ltd. All rights Reserved)

     NAME:       XXCP_VERTEX_WS 
     PURPOSE:    The controlling processes for single Dynamic SQL selects 

     Version [03.06.01] Build Date [05-NOV-2012] Name [XXCP_VERTEX_WS]
     
  ******************************************************************************/

  FUNCTION Software_version RETURN VARCHAR2;

  -- Public function and procedure declarations
  function Control (cTaxable_Amount   number,
                    cTaxable_Currency varchar2,
                    cTransaction_Date date,
                    cSeller_PostCode  varchar2,
                    cSeller_Country   varchar2,
                    cCust_Postcode    varchar2,
                    cCust_Country     varchar2,
                    cPreview_Mode     varchar2 default 'N') return number;

end xxcp_vertex_ws;
/
