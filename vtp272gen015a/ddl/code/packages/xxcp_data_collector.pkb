Create Or Replace Package Body XXCP_DATA_COLLECTOR As
/******************************************************************************
                       V I R T A L  T R A D E R  L I M I T E D 
           (Copyright 2005-2012 Virtual Trader Ltd. All rights Reserved)
   
  NAME:       XXCP_DATA_COLLECTOR 
  PURPOSE:    The Package is used to process Data Collector configuration and 
              populate the VT source tables
   
  REVISIONS:
  Ver        Date      Author  Description
  ---------  --------  ------- ------------------------------------
  03.02.00   22/06/12  Mat     First Release
******************************************************************************/

   TYPE t_1 IS TABLE OF VARCHAR2(500) INDEX BY PLS_INTEGER;  -- associative arrays
   TYPE t_2 IS TABLE OF t_1 INDEX BY PLS_INTEGER;            -- nested table of associative arrays

   gDatCol t_2 ;
   gDatCol_Rec t_1;      
  
   gEngine_Trace        Varchar2(1) := 'N';   
   gCollector_Table     Varchar2(30);
   gCollector_Key       Varchar2(30);      
   gStart_id            Number := 0;  
   gCollector_Name      Varchar2(100);     
   gInsert_add_cols     Number := 0; 
   gSelect_Statement    Clob;
   gInsert_Statement    Clob;       
   gColumn_Count        Number := 0;     
   gInsertLimit         NUMBER := 1000; -- 1000 Default Limit
 
   gDataResults        xxcp_dynamic_array := xxcp_dynamic_array(' ');
  
   g_exception_message Varchar(300);

   e_setup             Exception;
   e_process           EXCEPTION;
   e_insert            EXCEPTION;
   e_lock              Exception;             

   gInsertRecCount     PLS_INTEGER := 0;

   -- *************************************************************
   --                     Software Version Control
   --                   -----------------------------
   -- This package can be checked with SQL to ensure it's installed
   -- select packagename.software_version from dual;
   -- *************************************************************
   Function Software_Version Return Varchar2 Is
   Begin
      Return('Version [03.02.01] Build Date [30-JUN-2012] NAME [XXCP_DATA_COLLECTOR]');
   End Software_Version;

   -- *************************************************************
   -- write_log
   -- *************************************************************
   PROCEDURE Write_Log ( cOutputType   IN VARCHAR2 ,
                         cMessage      IN VARCHAR2 ) IS
     
   BEGIN

      IF cOutputType = 'O' THEN
         FND_FILE.PUT_LINE( FND_FILE.OUTPUT, cMessage);
      ELSIF cOutputType = 'L' THEN
         IF gEngine_Trace = 'Y' THEN
            FND_FILE.PUT_LINE( FND_FILE.LOG, cMessage);
         END IF;
      END IF;      

   END Write_Log;

   -- *************************************************************
   -- Get_Value
   -- *************************************************************
   FUNCTION Get_Value(cPosition in number) Return varchar2 is
   Begin
     RETURN  XXCP_DATA_COLLECTOR.gDatCol_Rec (cPosition); 
   End Get_Value;

   -- *************************************************************
   -- get_Coll_ctl
   -- *************************************************************
   PROCEDURE Get_Coll_Ctl(cSource_id in number, cCol_Ctl_id in number) Is

    Cursor gColl_ctl(pSource_id number, pCol_Ctl_id in number) Is
      Select dcc.start_id,
             dcc.end_id,
             dcc.collector_name,
             dcc.collector_table,
             dcc.collector_key,
             ss.source_base_table,
             dcc.Trace_Flag
        From xxcp_data_collector_ctl dcc,
             xxcp_sys_sources ss
       Where dcc.col_ctl_id = pCol_Ctl_id
         AND ss.source_id = pSource_id
         AND dcc.source_id  = ss.source_id;

     gr_coll_ctl         gColl_ctl%Rowtype;

   Begin
      -- Get Data Collector Control
      Open gColl_ctl(pSource_id => cSource_id, pCol_Ctl_id => cCol_Ctl_id);
      Fetch gColl_ctl Into gr_coll_ctl;
   
      gEngine_Trace    := gr_coll_ctl.trace_flag;
      gCollector_Table := gr_coll_ctl.collector_table;
      gCollector_Key   := gr_coll_ctl.collector_key;
      gCollector_Name  := gr_coll_ctl.collector_name;

      gStart_id        := NVL(gr_coll_ctl.start_id, 0);

      If gColl_ctl%Notfound Then
         g_exception_message := 'Data Collection Control does not exist for Id: ' || cCol_ctl_id;

         Close gColl_ctl;
         Raise e_setup;
      End If;
   
      Close gColl_ctl;
   
   End Get_Coll_ctl;

   -- *************************************************************
   -- Get_Que_Def
   -- *************************************************************
   Procedure Get_Que_Def(cSource_id in number, cCol_Ctl_id in number) is

   Cursor Que_def( pSource_id number, pCol_ctl_id in number) Is
      Select select_statement,
             insert_statement,
             generated,
             column_count,
             (SELECT COUNT(*) 
                FROM xxcp_queue_def_cols qdc 
               WHERE qdc.sql_build_id = dcd.sql_build_id ) insert_add_cols
        From xxcp_queue_definitions dcd
       Where col_ctl_id   = pCol_ctl_id
         And source_id    = pSource_id
         And active       = 'Y'
         And trunc(Sysdate) Between trunc(dcd.effective_start_date) AND nvl(dcd.effective_end_date, trunc(Sysdate));
  
      vCount NUMBER(1) := 0;

   Begin

      FOR rec IN Que_def(pSource_id => cSource_id, pCol_Ctl_id => cCol_Ctl_id) LOOP
   
         gInsert_Add_Cols   := rec.insert_add_cols;
         gSelect_Statement  := rec.select_statement;
         gColumn_Count      := rec.column_count;
         gInsert_Statement  := rec.insert_statement;
     
         If rec.generated = 'N' Then
            g_exception_message := 'Data Collector has not been generated since the last modification, please generate.';
            Raise e_setup;
         End If;

         vCount := vCount + 1;

      END LOOP;

      If vCount = 0 Then
         g_exception_message := 'Queue (Data Collection) Definition does not exist for Id: ' || cCol_Ctl_id;
         Raise e_setup;
      ELSIF vCount > 1 THEN
         g_exception_message := 'Multiple Definition Controls exist for Id: ' || cCol_Ctl_id;
         Raise e_setup;
      End If;
   
   End Get_Que_Def;

   -- *************************************************************
   -- get_new_max_id
   -- *************************************************************
   FUNCTION Get_New_Max_id RETURN VARCHAR2 IS

      vEndId           VARCHAR2(30);
      vEndIdSql        Varchar2(200); 
      vSQLERRM         Varchar2(512);    
   
   BEGIN
     
      -- Compose End Id restriction
      vEndIdSql := 'SELECT MAX(' || gCollector_key || ') FROM ' || gCollector_Table;

    	If NOT xxcp_foundation.Dynamic_SQL_TEST(vEndIdSql, vSQLERRM,'N') then
         g_exception_message := 'Cannot get new Collection End Id: '||vSQLERRM;
         RAISE e_setup;
      END IF;

      Execute Immediate vEndIdSql Into vEndId;

      write_log( 'L', 'New Max Id is: '||vEndId );
      
      RETURN vEndId;
    
   END Get_new_max_id;

   -- *************************************************************
   -- insert_interface_records
   -- *************************************************************
  Function Insert_Interface_Records return number IS
     
   vJob_Status Number(2) := 0;
   vSQLERRM    Varchar2(512);
      
   BEGIN
      Begin     
    
         FOR x IN gDatCol.FIRST..gDatCol.LAST LOOP
            -- Turn into Single Record
            gDatCol_Rec:= gDatCol(x);
               
            If xxcp_forms.Execute_immediate_clob(cStatement => gInsert_Statement ,cParse_Only => 'N' ,cSqlErrm => vSQLERRM ) then
               Null;
            End If;
         END LOOP;
      
         -- Cleardown Array
         gDatCol.delete;

     EXCEPTION
        WHEN OTHERS THEN
           write_log('O', 'Cannot insert into VT Interface table: '||vSQLERRM);
           g_exception_message := 'Cannot insert into VT Interface table: '||substr(vSQLERRM, 1, 100);
           vJob_Status         := 3;
           RAISE e_insert;
       End;
     
     Return(vJob_Status);
   END Insert_Interface_Records;

   -- *************************************************************
   -- update_start_end_id
   -- *************************************************************
   Procedure Update_Start_End_id (cColCtlId  IN NUMBER,
                                  cLastEndId IN NUMBER)IS
                                  
     vLog varchar2(250);                             
     
   BEGIN
            
      UPDATE xxcp_data_collector_ctl c
         SET c.start_id   = cLastEndId + 1,
             c.end_id     = cLastEndId,
             c.last_update_date = sysdate
       WHERE c.col_ctl_id = cColCtlId;

      vLog := 'Collector Start id <'|| to_char(gStart_id) || '>  End id <'|| to_char(cLastEndId)||'>';
 
      write_log( 'L', vLog );

   EXCEPTION
      WHEN OTHERS THEN
         g_exception_message := 'Error Updating Start and End Ids : '||SQLERRM;
         RAISE e_setup;
   
   END Update_Start_End_id;

   -- *************************************************************
   -- append_interface_record
   -- *************************************************************
   Function Append_Interface_Record return number is
     
     vJob_Status number(2) := 0;
     
   BEGIN

     Begin            
      gInsertRecCount    := gInsertRecCount + 1;

      -- Assign (First 8) Mandatory Column Values to Collection
      FOR rMandCol IN 1 .. 7 LOOP 
         gDatCol(gInsertRecCount)(rMandCol) := gDataResults(rMandCol);         
      END LOOP;
      
      -- Assign Additional Columns Values to Collection - positioned from 18 onwards
      IF gInsert_Add_Cols > 0 THEN
        FOR rAddCol IN 18 .. gInsert_add_cols + 17 LOOP 
           gDatCol(gInsertRecCount)(rAddCol) := gDataResults(rAddCol);         
        END LOOP;
      END IF;
      
/*   EXCEPTION
      WHEN OTHERS THEN
         write_log('O', 'Cannot Append to Collection: '||SQLERRM);
         vJob_Status := 3;
         RAISE e_insert;*/
         
     End;
     
     Return(vJob_Status);    
   
   END Append_Interface_Record;

   -- *************************************************************
   -- process_interface_record
   -- *************************************************************
   Function Process_Interface_Record(  cCollectorName      IN VARCHAR2,
                                       cTransaction_Table  In OUT Varchar2,
                                       cTransaction_Type   In Out Varchar2,
                                       cTransaction_Class  In Out Varchar2,
                                       cTransaction_Ref    IN OUT varchar2,
                                       cTransaction_Id     IN OUT varchar2,
                                       cSetOfBooksOrgId    In VARCHAR2,
                                       cSetOfBooksOrgFlag  IN VARCHAR2,
                                       cUserSourceName     IN VARCHAR2,
                                       cUserSourceCategory IN VARCHAR2,
                                       cGL_SL_link_id      IN VARCHAR2,
                                       cGL_SL_link_table   IN VARCHAR2,                                             
                                       cRef1               IN VARCHAR2,
                                       cRef2               IN VARCHAR2,
                                       cRef3               IN VARCHAR2,
                                       cRef4               IN VARCHAR2,
                                       cRef5               IN VARCHAR2 ) Return Number is

   vInstance_id          xxcp_source_assignments.instance_id%type := 0;
   vDisable_Source       xxcp_source_assignments.disable_source%Type := 'N';
   vCustom_Ovr_Flag      VARCHAR2(1);   
   vSourceAssignmentId   NUMBER(15);
   vSetOfBooksId         NUMBER(15); 
   vOrganizationId       NUMBER(15);  
   vJob_Status           NUMBER(2) := 0;
   
   vTrx_Number           Number;
  
   BEGIN
     
    Begin

      -- Set Organization Id and Set of Books Id
      IF cSetOfBooksOrgFlag = 'S' THEN    -- Set of Books Id / Ledger Id
         vSetOfBooksId := cSetOfBooksOrgId;
         vOrganizationId := NULL;    
      ELSIF cSetOfBooksOrgFlag = 'O' THEN -- Organization Id 
         vSetOfBooksId := NULL;
         vOrganizationId := cSetOfBooksOrgId;    
      END IF;
     
      -- Get Table, Type and Class
      xxcp_trigger_code.GetTableTypeClass(
                          cTrigger_Name          => cCollectorName,
                          cInstance_id           => vInstance_id,
                          cUser_je_source_name   => cUserSourceName,
                          cUser_je_category_name => cUserSourceCategory,
                          cTransaction_Table     => cTransaction_Table,
                          cTransaction_Type      => cTransaction_Type,
                          cTransaction_Class     => cTransaction_Class,
                          cTransaction_id        => cTransaction_id,
                          cTransaction_Ref       => cTransaction_Ref,
                          cSet_of_books_id       => vSetOfBooksId,
                          cOrg_id                => Null,
                          cOrganization_id       => vOrganizationId,
                          cGL_SL_Link_Id         => cGL_SL_link_id,
                          cGL_SL_Link_Table      => cGL_SL_link_table,
                          cReference1            => cRef1,
                          cReference2            => cRef2,
                          cReference3            => cRef3,
                          cReference4            => cRef4,
                          cReference5            => cRef5,
                          cReference6            => Null,
                          cReference7            => Null,
                          cReference8            => Null,
                          cReference9            => Null,
                          cReference10           => Null,
                          cReference11           => Null,
                          cReference12           => Null,
                          cReference13           => Null,
                          cReference14           => Null,
                          cReference15           => Null,
                          cReference16           => Null,
                          cReference17           => Null,
                          cReference18           => Null,
                          cReference19           => Null,
                          cReference20           => Null,
                          cReference21           => Null,
                          cReference22           => Null,
                          cReference23           => Null,
                          cReference24           => Null,
                          cReference25           => Null,
                          cReference26           => Null,
                          cReference27           => Null,
                          cReference28           => Null,
                          cReference29           => Null,
                          cReference30           => Null,
                          cCustom_Ovr_flag       => vCustom_Ovr_Flag );
   
      IF cSetOfBooksOrgFlag = 'O' THEN -- Organization Id  
         -- Check with VT to see if VT requires this record to be inserted.
         If XXCP_TRIGGER_CODE.Assignment_Control_Org( cTrigger_Name         => cCollectorName
                                                    , cInstance_id          => vInstance_id
                                                    , cOrg_id               => vOrganizationId
                                                    , cUser_Source_Name     => cUserSourceName
                                                    , cUser_Category_Name   => cUserSourceCategory
                                                    , cTransaction_Table    => cTransaction_Table
                                                    , cTransaction_Type     => cTransaction_Type
                                                    , cTransaction_Class    => cTransaction_Class
                                                    , cTransaction_id       => cTransaction_id
                                                    , cAux_Data1            => cRef1
                                                    , cAux_Data2            => cRef2
                                                    , cAux_Data3            => cRef3
                                                    , cAux_Data4            => cRef4
                                                    , cAux_Data5            => cRef5
                                                    , cSource_Assignment_id => vSourceAssignmentId
                                                    , cDisable_Source       => vDisable_Source) Then

             -- Assign New Source Assignment Id
             gDataResults(7) := vSourceAssignmentId;

             -- Add Record to Collection for insert
             vJob_Status := append_interface_record;

         ELSE
           write_log('L',
                     'Record not inserted: Assignment Control Organization not found - Trigger Name <'||cCollectorName||'>'||chr(10)|| 'Ledger <'||to_char(vSetOfBooksId)||'>'||chr(10)|| 
                     'Table <'||cTransaction_Table||'>'||chr(10)||'Type <'||cTransaction_Type||'>'||chr(10)|| 'Class <'||cTransaction_Class||'>'||chr(10)||
                     'Source Name <'||cUserSourceName||'>'||chr(10)||'Category Name <'||cUserSourceCategory||'>'||chr(10)|| 'Transaction ID <'||to_char(cTransaction_id)||'>'|| chr(10));

         END IF;

      ELSIF cSetOfBooksOrgFlag = 'S' THEN -- Set of Books Id / Ledger Id
         -- Check with VT to see if VT requires this record to be inserted.               
         IF XXCP_TRIGGER_CODE.Assignment_Control_Active( cTrigger_Name         => cCollectorName
                                                       , cInstance_id          => vInstance_id
                                                       , cSet_of_books_id      => vSetOfBooksId
                                                       , cUser_Source_Name     => cUserSourceName
                                                       , cUser_Category_Name   => cUserSourceCategory
                                                       , cTransaction_Table    => cTransaction_Table
                                                       , cTransaction_Type     => cTransaction_Type
                                                       , cTransaction_Class    => cTransaction_Class
                                                       , cTransaction_id       => cTransaction_id
                                                       , cAux_Data1            => cRef1
                                                       , cAux_Data2            => cRef2
                                                       , cAux_Data3            => cRef3
                                                       , cAux_Data4            => cRef4
                                                       , cAux_Data5            => cRef5
                                                       , cSource_Assignment_id => vSourceAssignmentId
                                                       , cDisable_Source       => vDisable_Source ) THEN

             -- Assign New Source Assignment Id
             gDataResults(7) := vSourceAssignmentId;
             
             If nvl(gDataResults(6),'0') = '0' then
               select xxcp_transaction_id_seq.nextval into vTrx_Number from dual;
               gDataResults(6) := to_char(vTrx_Number);
             End If;

             -- Add Record to Collection for insert
             vJob_Status := append_interface_record ;             

         ELSE
            write_log ('L',
                       'Record not inserted: Assignment Control Active not found - Trigger Name <'||cCollectorName||'>'||chr(10)|| 'Ledger <'||to_char(vSetOfBooksId)||'>'||chr(10)|| 
                       'Table <'||cTransaction_Table||'>'||chr(10)||'Type <'||cTransaction_Type||'>'||chr(10)|| 'Class <'||cTransaction_Class||'>'||chr(10)||
                       'Source Name <'||cUserSourceName||'>'||chr(10)||'Category Name <'||cUserSourceCategory||'>'||chr(10)|| 'Transaction ID <'||to_char(cTransaction_id)||'>'|| chr(10));
         End If;
      END IF;
      
   EXCEPTION
      WHEN OTHERS THEN
         write_log('O', 'Error processing Record: '||SQLERRM);
         write_log('L', 'Error processing Record - Trigger Name <'||cCollectorName||'>'||chr(10)|| 'Ledger <'||to_char(vSetOfBooksId)||'>'||chr(10)|| 
                   'Table <'||cTransaction_Table||'>'||chr(10)||'Type <'||cTransaction_Type||'>'||chr(10)|| 'Class <'||cTransaction_Class||'>'||chr(10)||
                   'Source Name <'||cUserSourceName||'>'||chr(10)||'Category Name <'||cUserSourceCategory||'>'||chr(10)|| 'Transaction ID <'||to_char(cTransaction_id)||'>'|| chr(10));
         g_exception_message := 'Error processing Record: '|| SQLERRM;
         RAISE e_process;      
     End;
     return(vJob_Status);
   End Process_Interface_Record;
                                   
   -- *************************************************************
   -- ReadTable
   -- *************************************************************
   Function ReadTable( cCollectorName IN VARCHAR2,
                       cSQL           In Varchar2,
                       cColumn_Count  In Number,
                       cStart_id      In Number,
                       cEnd_id        in Number,
                       cRows         Out Number) Return Number Is
   
      vRowsFetched       Integer := 0;
      vInternalErrorCode Number := 0;
      v_dummy            Pls_Integer;
      vJob_Status        Number(2) := 0;
      v_cursorId         Integer;   
          
   Begin
     
      gDataResults.delete;
      gDataResults.extend(200); -- Allow for Number of Columns returned in Select Statement
   
      Begin

         write_log( 'L', 'Reading data for Collector: '||cCollectorName || ' No of Columns: '||cColumn_Count );
         write_log( 'L', 'SQL: '|| cSQL);
      
         v_cursorId   := DBMS_SQL.Open_cursor;
         dbms_sql.Parse(v_cursorId, cSQL, DBMS_SQL.native);
      
         For T In 1 .. cColumn_Count Loop
            DBMS_SQL.DEFINE_COLUMN(v_cursorId, t, gDataResults(t), 100);
         End Loop;
         
         dbms_sql.bind_variable(v_cursorid, ':Start_Number', cStart_id);   
         dbms_sql.bind_variable(v_cursorid, ':End_Number', cEnd_id);   
         
         v_dummy      := DBMS_SQL.EXECUTE(v_cursorId);

         vRowsFetched := 0;
         gInsertRecCount := 0;
         
         Loop
            If DBMS_SQL.FETCH_ROWS(v_cursorId) > 0 Then
               vRowsFetched       := vRowsFetched + 1;
               
               For t In 1 .. cColumn_Count LOOP
                  DBMS_SQL.COLUMN_VALUE(v_cursorId, t, gDataResults(t));

                  If t <= 7 And gDataResults(t) Is Null Then
                     vInternalErrorCode := t;
                     Exit;
                  End If;

               End Loop;
               
               IF vInternalErrorCode = 0 THEN -- All Mandatory fields found
                  vJob_Status :=
                    Process_Interface_Record(cCollectorName      => cCollectorName,
                                             cTransaction_Table  => gDataResults(1), 
                                             cTransaction_Type   => gDataResults(2), 
                                             cTransaction_Class  => gDataResults(3), 
                                             cTransaction_Ref    => gDataResults(5),
                                             cTransaction_Id     => gDataResults(6),                                                
                                             cSetOfBooksOrgId    => gDataResults(7),
                                             cSetofbooksOrgFlag  => gDataResults(8),
                                             cUserSourceName     => gDataResults(9),
                                             cUserSourceCategory => gDataResults(10),
                                             cGL_SL_link_id      => gDataResults(11),
                                             cGL_SL_link_table   => gDataResults(12),
                                             cRef1               => gDataResults(13),
                                             cRef2               => gDataResults(14),
                                             cRef3               => gDataResults(15),
                                             cRef4               => gDataResults(16),
                                             cRef5               => gDataResults(17) );
               
               ELSE
                   vInternalErrorCode := 0;
                   write_log('L',
                             'Record not inserted: Mandatory fields not found not found - Trigger Name <'||cCollectorName||'>'||chr(10)|| 'Ledger <'||to_char(gDataResults(7))||'>'||chr(10)|| 
                             'Table <'||gDataResults(1)||'>'||chr(10)||
                             'Type <'||gDataResults(2)||'>'||chr(10)|| 
                             'Class <'||gDataResults(3)||'>'||chr(10)||
                             'Transaction Date <'||gDataResults(4)||'>'||chr(10)||                             
                             'Transaction Ref <'||gDataResults(5)||'>'||chr(10)||
                             'Transaction Id <'||gDataResults(6)||'>'||chr(10)||
                             'Source Assignment <'||gDataResults(7)||'>'||chr(10));
               
               
               END IF;
                                                          
            Else
               -- Insert remaining records before exiting
               If gDatCol.count > 0 then  -- Records have been appended for Insert
                  vJob_Status := insert_interface_records;            
               END IF;
               
               Exit;
            End If;
            
            -- Insert Every x (default=1000) rows
            If mod( vRowsFetched, gInsertLimit ) = 0 THEN
              If gDatCol.count > 0 then  -- Records have been appended for Insert
                 -- Insert into VT Interface                                       
                 vJob_Status := insert_interface_records;
              END IF;
            End if;
            
         End Loop;     
      End;
   
      cRows := vRowsFetched;
      dbms_sql.close_cursor(v_cursorId);  
      Return(vInternalErrorCode);

   EXCEPTION
     WHEN OTHERS THEN
        IF dbms_sql.is_open(v_cursorId) THEN
           dbms_sql.close_cursor(v_cursorId);
        END IF;
        write_log('O', 'Error Reading Table: '||SQLERRM);
        g_exception_message := 'Error Reading table: '|| SQLERRM;     
        RAISE e_process;

   End ReadTable;
   
   -- *************************************************************
   -- Lock_Collector
   -- *************************************************************   
   Function Lock_Collector(cSource_id         IN Number,
                           cCol_ctl_id        IN Number,
                           cLock_Flag         IN Varchar2,
                           cOracle_Request_id IN Number,
                           cRun_Log           IN Varchar2) return boolean is
                           
     vLocked            Varchar2(1);
     vResult            Boolean := False;
     vOracle_Request_id Number;                      
     
     
   Begin 
     
     select locked, nvl(b.conc_request_id,0) Oracle_Request_id 
       into vLocked, vOracle_Request_id
       from xxcp_data_collector_ctl b
      where b.source_id  = cSource_id
        and b.col_ctl_id = cCol_ctl_id;
       
     If cLock_Flag = 'Y' and vLocked = 'N' then
       vResult := True;

       update xxcp_data_collector_ctl tb
          set last_run_date = sysdate,
              locked = 'Y',
              last_run_log = cRun_Log,
              conc_request_id = cOracle_Request_id
        where tb.source_id  = cSource_id
          and tb.col_ctl_id = cCol_ctl_id;
       
       Commit;
       
     ElsIf cLock_Flag = 'Y' and vLocked = 'Y' then
       vResult := False;
   
     ElsIf cLock_Flag = 'N' and vLocked = 'Y' then
       vResult := True;

       update xxcp_data_collector_ctl tb
          set last_run_date = sysdate,
              locked = 'N',
              last_run_log = cRun_Log
        where tb.source_id       = cSource_id
          and tb.col_ctl_id      = cCol_ctl_id
          and tb.conc_request_id = cOracle_Request_id ;
       
       Commit;

     End If;     
     
     Return(vResult);   
   End Lock_Collector;
   
   -- *************************************************************
   -- CONTROL
   -- *************************************************************
   Function Control( cSource_id         IN Number,
                     cCol_ctl_id        IN Number,
                     cOracle_Request_id IN Number,
                     cRecsProcessed    OUT NUMBER,
                     cRecsInserted     OUT NUMBER,                     
                     cException        Out Varchar) Return NUMBER Is

      vEnd_id            Number := 0;
      vProcessCount      Number := 0;
      vInternalErrorCode Number := 0;
      vSQLERRM           Varchar2(512);
      vSelect_SQL        Varchar2(32000);
      vRun_Log           xxcp_data_collector_ctl.last_run_log%type := Null;
   
   Begin
   
      -- timing
      write_log('L', 'Start Time: '|| to_char(sysdate,'DD-MON-YYYY HH24:MI:SS'));
      
      gDatCol.delete;
      gDatCol_Rec.Delete;
      
      vRun_Log := 'Oracle Request : '||to_char(cOracle_Request_id)||chr(10)||
                  'Start Time     : '|| to_char(sysdate,'DD-MON-YYYY HH24:MI:SS')||chr(10);
            
      If Lock_Collector(cSource_id         => cSource_id,
                        cCol_ctl_id        => cCol_Ctl_id,
                        cLock_Flag         => 'Y',
                        cOracle_Request_id => cOracle_Request_id,
                        cRun_Log           => vRun_Log) Then
             
          -- Get Data Collector Setup data
          Get_Coll_ctl(cSource_id => cSource_id, cCol_Ctl_id => cCol_Ctl_id);
          Get_Que_def(cSource_id => cSource_id, cCol_Ctl_id => cCol_Ctl_id);
       
          -- Get Collector New End Id
          vEnd_id := get_new_max_id;
          
          vSelect_Sql := DBMS_LOB.SUBSTR(gSelect_statement,32000,1 ) ;

          xxcp_global.gCommon(1).current_source_activity := 'DCOL';
          xxcp_global.gCommon(1).current_request_id      := cOracle_Request_id;
          xxcp_global.gCommon(1).current_Process_name    := substr(gCollector_Name,1,30);
          xxcp_global.gCommon(1).current_table_group_id  := cCol_Ctl_id;
       
          -- Remove old error codes for this job
          Begin       
             Delete from xxcp_errors xe
               where xe.source_activity = 'DCOL'
                 and xe.table_group_id = cCol_Ctl_id;      
             Commit;       
          End;
                    
          If NOT xxcp_foundation.Dynamic_SQL_TEST(vSelect_sql, vSQLERRM,'N') then
             xxcp_foundation.fndwriteerror(9200,vSQLERRM,vSelect_sql);
             g_exception_message := 'Cannot get records from Collector Table: '||vSQLERRM;
             RAISE e_setup;
          END IF;

          xxcp_foundation.fndwriteerror(100,'Data Collector:'||gCollector_name||' Start <'||to_char(gStart_id)||'> End id <'||to_char(vEnd_id)||'>');

          vRun_Log := vRun_Log||'Start Number   : '||to_char(gStart_id)||chr(10)||
                                'End Number     : '||to_char(vEnd_id)||chr(10);

          -- Update Start Id for subsequent run
          
          update_start_end_id (cCol_Ctl_id, vEnd_id) ;
          xxcp_foundation.fndwriteerror(100,'Data Collector: Read data for Collector id: '||to_char(cCol_Ctl_id),vSelect_sql );
          vRun_Log := vRun_Log||'Read Data      : '||to_char(sysdate,'DD-MON-YYYY HH24:MI:SS')||chr(10);

          vInternalErrorCode := ReadTable( cCollectorName => gCollector_name,
                                           cSQL           => vSelect_sql, 
                                           cColumn_Count  => gColumn_Count,
                                           cStart_id      => gStart_id,
                                           cEnd_id        => vEnd_id, 
                                           cRows          => vProcessCount);
       
          xxcp_foundation.fndwriteerror(100,'Data Collector: Insert Records');
          vRun_Log := vRun_Log||'Insert Records : '||to_char(sysdate,'DD-MON-YYYY HH24:MI:SS')||chr(10);

          write_log('L', 'End Time: '|| to_char(sysdate,'DD-MON-YYYY HH24:MI:SS'));

          cRecsProcessed := vProcessCount;
          cRecsInserted  := gInsertRecCount;
          
          xxcp_foundation.fndwriteerror(100,'Data Collector:'||gCollector_name||' Processed <'||to_char(cRecsProcessed)||'> Recs Inserted <'||to_char(cRecsInserted)||'>');

          vRun_Log := vRun_Log||'Processed      : '||to_char(cRecsProcessed)||chr(10)||
                                'Recs Inserted  : '||to_char(cRecsInserted)||chr(10);
          vRun_Log := vRun_Log||'End Date       : '||to_char(sysdate,'DD-MON-YYYY HH24:MI:SS')||chr(10);

          -- Unlock Collector
          If Lock_Collector(cSource_id         => cSource_id,
                            cCol_ctl_id        => cCol_Ctl_id,
                            cLock_Flag         => 'N',
                            cOracle_Request_id => cOracle_Request_id,
                            cRun_Log           => vRun_Log) Then
            NULL;
          End If;

      Else
        write_log('L', 'Collector already in use');
        RAISE e_lock;
      End If;
      
      gDatCol.delete;
      gDatCol_Rec.Delete;

      COMMIT;
        
      Return 0;
   
   Exception
      When e_setup Then         
         cException := g_exception_message;
         write_log( 'O','Error with Setup: '||g_exception_message );
         ROLLBACK;
         Return 1;
      WHEN e_process THEN
         cException := g_exception_message;
         ROLLBACK;
         Return 2;
      WHEN e_lock THEN
         cException := 'Collector already in use';
         ROLLBACK;
         Return 3;
      WHEN OTHERS THEN   
         cException := 'UNKNOWN ERROR: '|| SQLERRM;
         write_log( 'O','Unknown Error: '||SQLERRM );
         ROLLBACK;
         Return 2;

   End Control;

  --
  -- Init
  --
  Procedure Init is

   Cursor SP(pProfile_Category in varchar2, pProfile_Name in varchar2) is
    select p.profile_value
      from xxcp_sys_profile p
     where p.profile_name = pProfile_Name
       and p.profile_category = pProfile_Category;

  Begin

    -- Partition Months Start
    For Rec in SP('EV','DATALOADER INSERT LIMIT') loop
       gInsertLimit := rec.profile_value;
    End Loop;

    write_log( 'L','Insert Limit: '||gInsertLimit );

  End Init;
  
  


BEGIN

  Init;

End XXCP_DATA_COLLECTOR;
/
