CREATE OR REPLACE PACKAGE BODY APPS.XXCP_TE_ENGINE AS
  /******************************************************************************
                          V I R T A L  T R A D E R  L I M I T E D
           (Copyright 2002-2012 Virtual Trader Ltd. All rights Reserved)

     NAME:       XXCP_TE_ENGINE
     PURPOSE:    The Main Transaction Engine process.

     REVISIONS:
     Ver        Date      Author  Description
     ---------  --------  ------- ------------------------------------
     02.00.00   02/08/02  Keith   First Coding
     02.04.00   04/01/07  Keith   Rouding issue resolved and balancing element 11
     02.04.01   23/01/07  Keith   Internal Attribute 89 - Ed Trx Type Name
     02.04.02   07/02/07  Keith   Added Sabrix Logic.
     02.04.03   12/03/07  Keith   Added Preview_id to header select
     02.04.04   24/03/07  Keith   Altered how cache summaries works.
     02.04.05   14/05/07  Keith   Changed Cashe Trace size.
     02.04.06   25/06/07  Keith   Zero logic change with Once Rule
     02.04.07   03/10/07  Keith   Increased Cache Summary 25 elements
     02.04.08   10/10/07  Keith   Transaction Trace for Account Segments
     02.05.00   18/05/08  Keith   Added cWhen_DI_Not_found, cWhen_DI_Too_Many_Found
     02.05.01   20/05/08  Keith   Added Balancing 11 (Companny Segment 1)
     02.05.02   22/05/08  Keith   Added new Accounted Conversions
     02.05.02   11/06/08  Keith   Added dynamic Accounted Exhcnage Rate Type
     02.05.03   07/08/08  Nigel   Changed TA Cursor in control so that it would use
                                  XXCP_TRANSACTION_ATTRIB_U1 index and remove the leading hint..
     02.05.05   02/09/08  Keith   Improved Tax Rule logic.
     02.05.06   14/11/08  Nigel   Made changes for Gain and Loss Once rule type.
     02.05.07   19/11/08  Nigel   Added default trading relationship.
     02.05.08   01/12/08  Nigel   Made changes for owner/partner account set control.
     02.05.09   15/12/08  Simon   Added Forecast cursor changes for Header and Attributes.
                                  Added Forecast to Get_Transaction_Cache,Cache_Summary,Cache_Summary_Totals
     02.05.10   19/02/09  Keith   Added Forecast and Trueup calculations
     02.05.11   11/03/09  Keith   Added Cost Category Change to true up logic
     02.05.12   21/04/09  Simon   Added Data Source Change to forecast and true up logic.
                                  Added Active to XXCP_FORECAST_HISTORY and XXCP_TRUE_UP_HISTORY.
                                  Added check for xxcp_global.replan_on.
                                  Changed check for multi-tier to use
                                  xxcp_global.gCommon(1).Previous_Attribute_Id = 0 (Reset to 0 in Reset_Working_Storage).
     02.05.13   01/05/09  Simon   Added Category_Data_Source.
     02.05.14   05/05/09  Simon   Fix CPA Rates Cursor (order desc).
     02.05.15   13/05/09  Simon   Get_Tax_Reg_Info now called from True_Up_Rates.
     02.05.16   18/05/09  Simon   Add Instance_id to FC and TU rates procedures.
                                  Added Populate_Replan.
     02.05.17   31/05/09  Simon   Added True_Up_On. CPA Rates now orders by
     02.05.18   23/06/09  Simon   Added FC_True_Up_Rates (Used during Replan Activity).
     02.05.19   24/06/09  Simon   Add Trading_Set_Id to Column Rules working storage.
     02.05.20   01/07/09  Simon   Change rates to work off id rather than description.
                                  Populate Internal(129) with Territory.
                                  Pass vCurr_Category_Data_Source into TU Rates calculation.
     02.05.20a  20/07/09  Nigel   Fixed bug D1003 array being incorrectly set to D1001.
     02.05.21   13/07/09  Simon   Populate_Replan to use "VT_REPLAN_" columns instead of attributes.
     02.05.22   13/07/09  Simon   Modify Get_Payer_Details.
     02.05.23   13/08/09  Simon   Forecast_Rates bug fix to return first Rates record only.
     02.05.24   28/08/09  Keith   Added Error message to not finding Rate in True-up.
     02.05.25   09/09/09  Simon   Add Payer_Details check into Populate_replan.
     02.05.26   14/09/09  Simon   FC_True_Up_Rates to look at RF and RT history records.
                                  Do not error when no cat or payee found when processing CPA TU subsequent period.
     02.05.27   18/09/09  Simon   True_up_rates - If no cat is found for a prior period then default it to the current cost cat.
     02.06.00   04/08/09  Keith   Added Accounted Currency and value to account rules
     02.06.01   10/08/09  Keith   Added Pricing Ladder
     02.06.02   11/08/09  Simon   Moved gReplan_Reprocess from xxcp_cpa_true_up_engine.
                                  Add IC_PRICE (102) to Cache_Summary_Totals.
     02.06.03   13/08/09  Simon   Forecast_Rates bug fix to return first Rates record only (Patch14).
     02.06.04   15/09/09  Keith   IC Unit Extended and IC Price Extended
     02.06.05   02/10/09  Keith   Added formual sets
     02.06.06   15/12/09  Nigel   Now passing transaction_class, trading_Set to xxcp_dynamic_sql.non_configurator
     02.06.07   03/02/10  Simon   Added Value Sign Internal Attribute(136).
     02.06.08   17/02/10  Simon   Check xxcp_global.gBalancing_Segment11_Override.
     02.06.09   19/05/10  Keith   Formula Sets now take rule type as a parameter and Once Rule
     02.06.10   21/05/10  Keith   Selective Cross Validation
     02.06.11   28/02/11  Keith   Dynamic Explosion
                                  Inceased the Cache Summaries array to hold 25 attributes.
     02.06.12   22/09/11  Nigel   Disable class mapping in the engine      
     02.06.13   26/09/11  Keith   Added Suppress Target Output      
     02.06.14   14/11/11  Mark    Use nvl's around History_Suppression_Rule and Target_Suppression_Rule
     02.06.15   23/11/11  Keith   Mutliple Owners
     02.06.16   18/01/12  Keith   Account Set change and ExplosionTrxProcess
     02.06.17   19/01/12  Nigel   Added Rule Run Sequence population (9147)
     03.06.18   24/01/12  Keith   Increased gMaxCacheRows to 5     
     03.06.19   12/03/12  Nigel   Change order by on taxreg cursor.
     03.06.20   28/06/12  Nigel   ICS interface changes and Standard Rules Required fix
     03.06.21   11/07/12  Simon   Fix gDo_Not_Do_Rule Gain and Loss issue (OOD-205).
                                  Gain and Loss rules now populate history_suppression correctly.
     03.06.22   16/07/12  Simon   Local rules now populate history_suppression correctly (OOD-213).
     03.06.23   16/07/12  Nigel   Moved ICS Classification Attribute population from init to xxcp_te_base.engine_init
     03.06.24   14/09/12  Simon   Added Cost_Plus_Set_Id (OOD-293).
                                  Added abilty to disable CPA calculations.
     03.06.25   10/05/12  Simon   Increase Dyn Att length from 100 to 250 (OOD-195).
     03.06.26   28/09/12  Nigel   Added the population of Approve Restricted dynamic attribute (OOD-314)
     03.07.27   26/10/12  Nigel   Added the population of Target Type Id dynamic attribute for ICS (OOD-362)
     03.07.28   19/12/12  Simon   Added TAForecastEXP to enable Dyn Explosion in Cost Plus.
  ******************************************************************************************************/

   gPrevCrossValOvr      VARCHAR2(1) := 'N';
   gCacheSumArray        XXCP_DYNAMIC_ARRAY;
   gCacheSummaries       VARCHAR2(1) := 'N';
/*   gTP_TAX_Class_Name    VARCHAR2(30);*/

   gNo_Target_Type_Match Number(5) := 10782;   
   gDo_Not_Do_Rule       Number(6) := 10789;

   ----------------------------------------------------------------------
   -- Constants
   ----------------------------------------------------------------------
   gCacheSumMaxQty    CONSTANT INTEGER(3)  := 25;
   gMaxCacheRows      CONSTANT INTEGER(2)  := 5;
   gTrue_up_Offset    NUMBER(2) := 1;
   gStart_of_Year_id  NUMBER(8) := 0;

   gTierOneOwner      xxcp_tax_registrations.short_code%type;
   gTierOneTerr       xxcp_tax_registrations.territory%type;
   gTierOne_Control   varchar2(1) := 'N';
   gDisable_CPA_Calc  varchar2(1) := 'N';

   gPayerAttribute         number(4) := 0;
   gPayerRetProAttribute   number(4) := 0;
   gTargetTypeName         varchar2(100);
   gFormula_Pointers_1003  xxcp_formula_gen.build_attributes%type;

   gValidateColumnAttributes varchar2(1) := 'Y';
   

  -- *************************************************************
  --                     Software Version Control
  --                   -----------------------------
  -- This package can be checked with SQL to ensure it's installed
  -- select packagename.software_version from dual;
  -- *************************************************************
  FUNCTION Software_Version RETURN VARCHAR2 IS
  BEGIN
    RETURN('Version [03.06.28] Build Date [19-DEC-2012] Name [XXCP_TE_ENGINE]');
  END Software_Version;

  -- *************************************************************
  --                     Current_Start_of_Year_id (CPA)
  -- *************************************************************
  FUNCTION Current_Start_of_Year_id RETURN NUMBER is
  BEGIN
    RETURN(gStart_of_Year_id);
  End Current_Start_of_Year_id;

  -- *************************************************************
  --                     Check_combination_id
  --                   -----------------------------
  -- *************************************************************
  FUNCTION Check_Combination_id(cChart_of_Accounts_id IN NUMBER,
                                cMax_Segments         IN NUMBER,
                                cAccount_Segments     IN xxcp_dynamic_array,
                                cGl_Date              IN DATE,
                                cInstance_id          IN NUMBER) RETURN NUMBER IS

    CURSOR C_Seven(
      pChart_of_Accounts_id IN NUMBER,
      pSegment1             IN VARCHAR2,
      pSegment2             IN VARCHAR2,
      pSegment3             IN VARCHAR2,
      pSegment4             IN VARCHAR2,
      pSegment5             IN VARCHAR2,
      pSegment6             IN VARCHAR2,
       pSegment7             IN VARCHAR2,
      pInstance_id          IN NUMBER
      ) IS
      SELECT m.start_date_active, m.end_date_active, m.code_combination_id
        FROM xxcp_instance_code_comb_v m
        WHERE m.chart_of_accounts_id = pchart_of_accounts_id
          AND m.segment1 = psegment1
          AND m.segment2 = psegment2
          AND m.segment3 = psegment3
          AND m.segment4 = psegment4
          AND m.segment5 = psegment5
          AND m.segment6 = psegment6
          AND m.segment7 = psegment7
          AND m.instance_id = pinstance_id
          AND m.enabled_flag = 'Y';

    CURSOR C_Fifteen(
      pChart_of_Accounts_id  IN NUMBER,
      pSegment1              IN VARCHAR2,
      pSegment2              IN VARCHAR2,
      pSegment3              IN VARCHAR2,
      pSegment4              IN VARCHAR2,
      pSegment5              IN VARCHAR2,
      pSegment6              IN VARCHAR2,
      pSegment7              IN VARCHAR2,
      pSegment8              IN VARCHAR2,
      pSegment9              IN VARCHAR2,
      pSegment10             IN VARCHAR2,
      pSegment11             IN VARCHAR2,
      pSegment12             IN VARCHAR2,
      pSegment13             IN VARCHAR2,
      pSegment14             IN VARCHAR2,
      pSegment15             IN VARCHAR2,
      pInstance_id           IN NUMBER
      ) IS
      SELECT m.START_DATE_ACTIVE, m.END_DATE_ACTIVE, m.CODE_COMBINATION_ID
        FROM xxcp_instance_code_comb_v m
        WHERE m.CHART_OF_ACCOUNTS_ID = pChart_of_Accounts_id
          AND m.SEGMENT1  = pSegment1
          AND m.SEGMENT2  = pSegment2
          AND m.SEGMENT3  = pSegment3
          AND m.SEGMENT4  = pSegment4
          AND m.SEGMENT5  = pSegment5
          AND m.SEGMENT6  = pSegment6
          AND m.SEGMENT7  = pSegment7
          AND m.SEGMENT8  = pSegment8
          AND m.SEGMENT9  = pSegment9
          AND m.SEGMENT10 = pSegment10
          AND m.SEGMENT11 = pSegment11
          AND m.SEGMENT12 = pSegment12
          AND m.SEGMENT13 = pSegment13
          AND m.SEGMENT14 = pSegment14
          AND m.SEGMENT15 = pSegment15
          AND M.INSTANCE_ID = pInstance_id
          AND m.ENABLED_FLAG = 'Y';

   vGLCOM_Found BOOLEAN := FALSE;

   vCCID        NUMBER  := 0;

  BEGIN

    IF cMax_Segments <= 7 THEN
      FOR Rec IN C_Seven(
        cChart_of_Accounts_id,
        cAccount_Segments(1),
        cAccount_Segments(2),
        cAccount_Segments(3),
        cAccount_Segments(4),
        cAccount_Segments(5),
        cAccount_Segments(6),
        cAccount_Segments(7),
        cInstance_id
       ) LOOP

          IF cGL_Date >= nvl(Rec.START_DATE_ACTIVE,cGL_Date) AND cGL_Date <= nvl(Rec.End_DATE_ACTIVE,cGL_Date) THEN
            vGLCOM_Found := TRUE;
           vCCID        := Rec.Code_Combination_id;
         END IF;
        EXIT;
      END LOOP;
    ELSE
      FOR Rec IN C_Fifteen(
        cChart_of_Accounts_id,
        cAccount_Segments(1),
        cAccount_Segments(2),
        cAccount_Segments(3),
        cAccount_Segments(4),
        cAccount_Segments(5),
        cAccount_Segments(6),
        cAccount_Segments(7),
        cAccount_Segments(8),
        cAccount_Segments(9),
        cAccount_Segments(10),
        cAccount_Segments(11),
        cAccount_Segments(12),
        cAccount_Segments(13),
        cAccount_Segments(14),
        cAccount_Segments(15),
        cInstance_id
        ) LOOP

          IF cGL_Date >= nvl(Rec.START_DATE_ACTIVE,cGL_Date) AND cGL_Date <= nvl(Rec.End_DATE_ACTIVE,cGL_Date) THEN
           vGLCOM_Found := TRUE;
           vCCID := Rec.Code_Combination_id;
         END IF;
        EXIT;
      END LOOP;
    END IF;

   RETURN(vCCID);
  END Check_combination_id;

  --
  --## ***********************************************************************************************************
  --##
  --##                            Cache_Summary_Get
  --##
  --##       Put the Summary information into the right memory positions
  --##
  --## ***********************************************************************************************************
  --
  PROCEDURE Cache_Summary_Get(cAction IN VARCHAR2) IS
    r  PLS_INTEGER;
    f  PLS_INTEGER := 0;

    sx PLS_INTEGER := 0;

   BEGIN

    If gCacheSummaries = 'Y' then -- Cache Summaries must be active

      sx := nvl(xxcp_wks.BRK_SET_RCD(1).sum_qty,0);

      FOR r IN 1..sx LOOP
        IF xxcp_wks.BRK_SET_RCD(1).NUMERIC_CACHE(r) BETWEEN 3001 AND 3999 THEN
             f := xxcp_wks.BRK_SET_RCD(1).NUMERIC_CACHE(r)-3000;
             IF cAction = 'L' THEN
                xxcp_wks.D1003_ArrayLC(f) := gCacheSumArray(f);
             ELSIF cAction = 'S' THEN
                xxcp_wks.D1003_ArrayCH(f) := gCacheSumArray(f);
             ELSIF cAction = 'G' THEN
                xxcp_wks.D1003_ArrayGL(f) := gCacheSumArray(f);
             ELSIF cAction = 'B' THEN
                xxcp_wks.D1003_Array(f) := gCacheSumArray(f);
             END IF;
        END IF;
        IF xxcp_wks.BRK_SET_RCD(1).COUNT_CACHE BETWEEN 3001 AND 3999 THEN
             f := xxcp_wks.BRK_SET_RCD(1).COUNT_CACHE-3000;
             IF cAction = 'L' THEN
                xxcp_wks.D1003_ArrayLC(f) := gCacheSumArray(f);
             ELSIF cAction = 'S' THEN
                xxcp_wks.D1003_ArrayCH(f) := gCacheSumArray(f);
             ELSIF cAction = 'G' THEN
                xxcp_wks.D1003_ArrayGL(f) := gCacheSumArray(f);
             ELSIF cAction = 'B' THEN
                xxcp_wks.D1003_Array(f) := gCacheSumArray(f);
             END IF;
        END IF;
        -- Trading Set
        IF xxcp_wks.BRK_SET_RCD(1).TS_CACHE(r) BETWEEN 3001 AND 3999 THEN
             f := xxcp_wks.BRK_SET_RCD(1).TS_CACHE(r)-3000;
             IF cAction = 'L' THEN
                xxcp_wks.D1003_ArrayLC(f) := gCacheSumArray(f);
             ELSIF cAction = 'S' THEN
                xxcp_wks.D1003_ArrayCH(f) := gCacheSumArray(f);
             ELSIF cAction = 'G' THEN
                xxcp_wks.D1003_ArrayGL(f) := gCacheSumArray(f);
             ELSIF cAction = 'B' THEN
                xxcp_wks.D1003_Array(f)   := gCacheSumArray(f);
             END IF;
        END IF;
      END LOOP;
    End If;

  END Cache_Summary_Get;


  --## ***********************************************************************************************************
  --##
  --##                            Cache_Summary_Qualifiers
  --##
  --## Perform the Trx Qualifier Calculation
  --##
  --## ***********************************************************************************************************
  --
  Function Cache_Summary_Qualifiers( cTrx_Qualifier1 IN OUT VARCHAR2, cTrx_Qualifier2 IN OUT VARCHAR2 ) return number IS

    vInternalErrorCode Number := 0;

   BEGIN

     If gCacheSummaries = 'Y' then

       IF xxcp_wks.BRK_SET_RCD(1).TRX_QUALIFIER1_EXPR IS NOT NULL
         OR xxcp_wks.BRK_SET_RCD(1).TRX_QUALIFIER2_EXPR IS NOT NULL THEN

         xxcp_dynamic_sql.Cache_Summary_Qualifiers(xxcp_global.source_id,
                                                   xxcp_wks.BRK_SET_RCD(1).break_point_id,
                                                   gCacheSumArray,
                                                   cTrx_Qualifier1,
                                                   cTrx_Qualifier2,
                                                   vInternalErrorCode);
       End if;

    End If;

    Return(vInternalErrorCode);

  END Cache_Summary_Qualifiers;

  --## ***********************************************************************************************************
  --##
  --##                            Cache_Summary_Find
  --##
  --## Find the Right Cache Summar Information
  --##
  --## ***********************************************************************************************************
  --
  PROCEDURE Cache_Summary_Find(cTrading_Set IN VARCHAR2) IS
    r  PLS_INTEGER;
    f  PLS_INTEGER := 0;
    tf PLS_INTEGER := 0;

    sx PLS_INTEGER := 0;

    Q1 VARCHAR2(250);
    Q2 VARCHAR2(250);
    Q3 VARCHAR2(250);
    Q4 VARCHAR2(250);
    Q5 VARCHAR2(250);


   BEGIN

    If gCacheSummaries = 'Y' then

      Q1 := xxcp_foundation.Find_DynamicAttribute(xxcp_wks.BRK_SET_RCD(1).BREAK_CACHE(1));
      Q2 := xxcp_foundation.Find_DynamicAttribute(xxcp_wks.BRK_SET_RCD(1).BREAK_CACHE(2));
      Q3 := xxcp_foundation.Find_DynamicAttribute(xxcp_wks.BRK_SET_RCD(1).BREAK_CACHE(3));
      Q4 := xxcp_foundation.Find_DynamicAttribute(xxcp_wks.BRK_SET_RCD(1).BREAK_CACHE(4));
      Q5 := xxcp_foundation.Find_DynamicAttribute(xxcp_wks.BRK_SET_RCD(1).BREAK_CACHE(5));

      sx := nvl(xxcp_wks.BRK_SET_RCD(1).sum_qty,0);

      FOR r IN 1..xxcp_wks.SUM_GRP_RCD.COUNT LOOP
        IF  cTrading_Set = xxcp_wks.SUM_GRP_RCD(r).Trading_Set
            AND nvl(Q1,'~#~') = nvl(xxcp_wks.SUM_GRP_RCD(r).BREAK_CACHE_ATTRIBUTES(1),'~#~')
            AND nvl(Q2,'~#~') = nvl(xxcp_wks.SUM_GRP_RCD(r).BREAK_CACHE_ATTRIBUTES(2),'~#~')
            AND nvl(Q3,'~#~') = nvl(xxcp_wks.SUM_GRP_RCD(r).BREAK_CACHE_ATTRIBUTES(3),'~#~')
            AND nvl(Q4,'~#~') = nvl(xxcp_wks.SUM_GRP_RCD(r).BREAK_CACHE_ATTRIBUTES(4),'~#~')
            AND nvl(Q5,'~#~') = nvl(xxcp_wks.SUM_GRP_RCD(r).BREAK_CACHE_ATTRIBUTES(5),'~#~')
          THEN
            f := r; -- Found!!
            EXIT;
          END IF;
      END LOOP;

      -- Summary
      IF f = 0 THEN
        FOR r IN 1..sx LOOP
           IF xxcp_wks.BRK_SET_RCD(1).NUMERIC_CACHE(r) BETWEEN 3001 AND 3999 THEN
              gCacheSumArray(xxcp_wks.BRK_SET_RCD(1).NUMERIC_CACHE(r)-3000) := 0;
           END IF;
           IF xxcp_wks.BRK_SET_RCD(1).COUNT_CACHE BETWEEN 3001 AND 3999 THEN
              gCacheSumArray(xxcp_wks.BRK_SET_RCD(1).Count_CACHE-3000) := 0;
           END IF;
        END LOOP;
      ELSE
        FOR r IN 1..sx LOOP -- across
           IF xxcp_wks.BRK_SET_RCD(1).NUMERIC_CACHE(r) BETWEEN 3001 AND 3999 THEN
             gCacheSumArray(xxcp_wks.BRK_SET_RCD(1).NUMERIC_CACHE(r)-3000) := xxcp_wks.SUM_GRP_RCD(f).CACHE_VALUE(r);
           END IF;
           IF xxcp_wks.BRK_SET_RCD(1).COUNT_CACHE BETWEEN 3001 AND 3999 THEN
              gCacheSumArray(xxcp_wks.BRK_SET_RCD(1).Count_CACHE-3000)  := xxcp_wks.SUM_GRP_RCD(f).Cnt;
           END IF;
        END LOOP;
      END IF;

      tf := 0;
      FOR r IN 1..xxcp_wks.TST_GRP_RCD.COUNT LOOP
        IF  cTrading_Set = xxcp_wks.TST_GRP_RCD(r).Trading_Set
          THEN
            tf := r; -- Found!!
            EXIT;
          END IF;
      END LOOP;

      -- Trading Set
      IF tf = 0 THEN
        FOR r IN 1..sx LOOP
           IF xxcp_wks.BRK_SET_RCD(1).TS_CACHE(r) BETWEEN 3001 AND 3999 THEN
             gCacheSumArray(xxcp_wks.BRK_SET_RCD(1).TS_CACHE(r)-3000) := 0;
           END IF;
        END LOOP;
      ELSE
        FOR r IN 1..sx LOOP
           IF xxcp_wks.BRK_SET_RCD(1).TS_CACHE(r) BETWEEN 3001 AND 3999 THEN
             gCacheSumArray(xxcp_wks.BRK_SET_RCD(1).TS_CACHE(r)-3000) := xxcp_wks.TST_GRP_RCD(tf).CACHE_VALUE(r);
           END IF;
        END LOOP;
      END IF;
    End If;
  END Cache_Summary_Find;

  --
  -- ## ***********************************************************************************************************
  -- ##
  -- ##                                      Get_Segment_Rules
  -- ##                    Work out the Account Segments from the Account Rules
  -- ##
  -- ## ***********************************************************************************************************
  --
  PROCEDURE Get_Segment_Rules(cAction                  IN VARCHAR2,
                              cRule_id                 IN NUMBER,
                              cRule_Name               IN VARCHAR2,
                              cChart_of_accounts_id    IN NUMBER,
                              cInstance_id             IN NUMBER,
                              cEnforce_Accounting      IN VARCHAR2,
                              cTarget_Cross_Validation IN VARCHAR2,
                              cEffective_Date          IN DATE,
                              -- NCM 02.05.08
                              cEntity_type             IN VARCHAR2,
                              cInternalErrorCode       IN OUT NOCOPY NUMBER) IS

    vACC_SEGMENT_NUMBER      XXCP_ACCOUNT_SETS.segment_number%TYPE;
    vMaster_Account_Set_id   XXCP_ACCOUNT_SETS.Account_Set_Id%Type; -- KP31DEC2011
    vMaster_Source_id        Number(4); -- KP31DEC2011
    vXCO_Found               Boolean := False; -- KP31DEC2011
    vXCO_Text                Varchar2(100); -- KP31DEC2011
    vASQ1_Attribute_Id       xxcp_dynamic_attributes.column_id%type;
    vASQ2_Attribute_Id       xxcp_dynamic_attributes.column_id%type;
    vACC_ACCOUNT_SET_ID      XXCP_ACCOUNT_SETS.account_set_id%TYPE;
    vACC_ACCOUNT_NXT_SET     XXCP_ACCOUNT_SETS.next_acct_set_id%TYPE;
    vACC_ASQ1                XXCP_ACCOUNT_SETS.AS1_Qualifier%TYPE;
    vACC_ASQ2                XXCP_ACCOUNT_SETS.AS2_Qualifier%TYPE;

    v1 VARCHAR2(250);
    v2 VARCHAR2(250);
    v3 VARCHAR2(250);
    v4 VARCHAR2(250);


    vAppl_short_name CONSTANT VARCHAR2(5) := 'SQLGL';
    vKey_flex_code   CONSTANT VARCHAR2(3) := 'GL#';

    CURSOR ASCTL(pAcc_Segment_Number IN NUMBER, pAcc_Account_Set_id IN NUMBER, pChart_of_Accounts_id IN NUMBER, pEntity_type IN VARCHAR2) IS
      SELECT nvl(ast.Account_Set_id, 0)    Account_set_id,
             nvl(ast.asq1_attribute_id, 0) ASQ1_Attribute_Id,
             nvl(ast.asq2_attribute_id, 0) ASQ2_Attribute_Id,
             nvl(ast.account_set_override_id,0) Override_id,
             nvl(ast.account_set_override_source_id,0) Override_source_id,
             ast.source_id
        FROM xxcp_account_set_ctl ast
       WHERE ast.segment_number       = pAcc_Segment_number
         AND ast.Account_set_id       = pAcc_Account_set_id
--         AND ast.chart_of_accounts_id = pChart_of_Accounts_id
         -- NCM 02.05.08
         AND ast.entity_type          = pEntity_type
         and ast.entity_type in ('Owner','Partner');
--         AND ast.source_id            = xxcp_global.gCommon(1).Source_id;

    CURSOR XCO(pAcc_Segment_Number IN NUMBER, pAcc_Account_Set_id IN NUMBER, pSource_id IN NUMBER, pEntity_type IN VARCHAR2) IS
      SELECT nvl(ast.Account_Set_id, 0)    Account_set_id,
             nvl(ast.asq1_attribute_id, 0) ASQ1_Attribute_Id,
             nvl(ast.asq2_attribute_id, 0) ASQ2_Attribute_Id
        FROM xxcp_account_set_ctl ast
       WHERE ast.segment_number       = pAcc_Segment_number
         AND ast.Account_set_id       = pAcc_Account_set_id
         and ast.source_id            = pSource_id
         -- NCM 02.05.08
         AND ast.entity_type          = Decode(pEntity_Type,'Owner', 'x-Owner', 'x-Partner');

    CURSOR ACCSET(pAcc_account_set_id IN NUMBER, pAcc_segment_number IN NUMBER, pACC_ASQ1 IN VARCHAR2, pACC_ASQ2 IN VARCHAR2,pEffective_Date IN Date) IS
      SELECT a.Segment_value,
             nvl(a.Segment_attribute_id, 0) Segment_attribute_id,
             nvl(a.Next_acct_set_id, 0) Next_acct_set_id,
             t.set_name                     Next_account_set_name,
             a.source_id
        FROM XXCP_ACCOUNT_SETS a,
             XXCP_TABLE_SET_CTL t
       WHERE Account_Set_id = pAcc_account_set_id
         AND Segment_Number = pAcc_segment_number
--         AND a.source_id    = xxcp_global.gCommon(1).Source_id
         AND nvl(a.AS1_Qualifier, '~null~') =
             DECODE(a.AS1_Qualifier,'*',nvl(a.AS1_Qualifier, '~null~'),nvl(pACC_ASQ1, '~null~'))
         AND nvl(a.AS2_Qualifier, '~null~') =
             DECODE(a.AS2_Qualifier,'*',nvl(a.AS2_Qualifier, '~null~'),nvl(pACC_ASQ2, '~null~'))
         AND nvl(a.next_acct_set_id, 0)       = t.set_id(+)
         AND xxcp_global.gCommon(1).Source_id = t.Source_id(+)
         AND 'ACCOUNT SET' = t.set_type(+)
         AND to_char(pEffective_Date,'DD-MON-YYYY') BETWEEN nvl(effective_from_date, '01-JAN-2000') AND nvl(effective_to_date, '31-DEC-2999')
       ORDER BY (DECODE(AS1_Qualifier, NULL, 0, 1) * 10) + DECODE(AS2_Qualifier, NULL, 0, 1) DESC;

    vMaxLoops              PLS_INTEGER := 15;
    vSegmentMessage        LONG;
    i                      PLS_INTEGER := 0;
    x                      PLS_INTEGER := 0;
    k                      PLS_INTEGER := 0;
    q                      PLS_INTEGER := 0;
    es                     INTEGER := xxcp_global.gEnSegs.COUNT;
    w                      INTEGER := 0;
    sr                     INTEGER := xxcp_global.gSR_Cnt;
    vSR_Loop_Cnt           INTEGER := 0; -- new
    --

    vASCTL_Found           BOOLEAN;
    vACCSET_Found          BOOLEAN;
    vSegment_Rule_Found    BOOLEAN := FALSE;

    vACC_Account_set_name  xxcp_table_set_ctl.set_name%type;
    vNext_Account_set_name xxcp_table_set_ctl.set_name%type;
    vMax_Segments          NUMBER(2) := 0;
    vError_Message         xxcp_errors.error_message%type;
    v_KeyFlexField         Fnd_Flex_Ext.SegmentArray;
    vNew_ccid              NUMBER := 0;
    vStoredValue           VARCHAR2(250);

    vCrossVal              VARCHAR2(1) := 'N';
    vAccountString         VARCHAR2(2000);
    vCheckNotNull          VARCHAR2(1) := 'N';
    vCOA_Cross_Validation  xxcp_segment_rules_coa.cross_validation%type := 'N';
    vSegDelimiter          VARCHAR2(1) := '-';
    vKeyFlexSegValue       xxcp_gl_interface.segment1%type;


    vCOA_Name              varchar2(100);
    vInstance_Name         xxcp_instance_db_links.instance_name%type;

    --
    -- Get Account Set Name
    --
    Function Get_Account_Set_Name(cAccount_Set_id in number, cCOA_Name out varchar2, cInstance_Name out varchar2) return varchar2 IS
    
     Cursor c1 (pAccount_Set_id in number) is
       select t.set_name||' ('||to_char(t.set_id)||')' account_set_name, 
              t.attribute1 coa_name, t.attribute2 Instance_name
        from xxcp_table_set_ctl t
        where t.set_id = pAccount_Set_id
          and t.set_base_table = 'CP_ACCOUNT_SETS';
  
      vAccount_Set_Name varchar2(100);
  
      Begin
        For rec in c1(pAccount_Set_id => cAccount_Set_id) Loop
         vAccount_Set_Name := rec.account_set_name;
         cCOA_Name         := rec.coa_name;
         cInstance_Name    := rec.instance_Name;
        End Loop;     
        Return(vAccount_Set_Name);
      End Get_Account_Set_Name;


  BEGIN

    --
    -- Find Active Account Segments if
    -- Chart of Accounts <> Global Chart of accounts id
    --

    xxcp_wks.Reset_Account_Segments;
    xxcp_wks.Reset_Enabled_Segments;
    vCOA_Cross_Validation := 'N';

    IF (cInternalErrorCode = 0) AND (cEnforce_Accounting IN ('O', 'Y')) THEN -- Opt/Yes

      FOR w IN 1 .. es LOOP
        IF xxcp_global.gEnSegs(w).coa_id = cChart_of_accounts_id AND xxcp_global.gEnSegs(w).instance_id = cInstance_id THEN

          vMax_Segments  := xxcp_global.gEnSegs(w).Max_Segments;
          vSegDelimiter  := fnd_flex_ext.get_delimiter(application_short_name => vAppl_short_name ,key_flex_code => vKey_flex_code,structure_number => cChart_of_accounts_id );

          FOR sl IN 1 .. vMax_Segments LOOP
            xxcp_wks.Enabled_Segments(sl) := SUBSTR(xxcp_global.gEnSegs(w).enabled,sl,1);
            IF k <= xxcp_wks.Source_Segments.COUNT AND
              xxcp_wks.Enabled_Segments(sl) = 'Y' THEN
              xxcp_wks.Account_Segments(sl) := xxcp_wks.Source_Segments(sl);
              -- Store Original value only for valid segments
              IF sl <= 10 THEN
                xxcp_wks.I1009_ArrayTEMP(10 + sl) := xxcp_wks.Source_Segments(sl);
              ELSIF sl BETWEEN 11 AND 15 THEN
                xxcp_wks.I1009_ArrayTEMP(30 + sl) := xxcp_wks.Source_Segments(sl);
              END IF;
            END IF;
          END LOOP;
          EXIT;
        END IF;
      END LOOP;

      -- No segment rules found and enforced is on
      IF vMax_Segments = 0 AND cEnforce_Accounting = 'Y' THEN
         cInternalErrorCode :=  6782;
         xxcp_foundation.FndWriteError(cInternalErrorCode,'Chart of Accounts <'||TO_CHAR(cChart_of_accounts_id)||'> Instance ID <'||TO_CHAR(cInstance_id)||
                                       '> Rule Name <'||cRule_Name||'> Rule id <'||TO_CHAR(cRule_id)||'>');
      END IF;

     vASCTL_FOUND := TRUE;

     IF cInternalErrorCode = 0 THEN
      FOR k IN 1 .. sr LOOP

        vStoredValue := NULL;

        IF (xxcp_global.gSR(k).Rule_id = cRule_id AND xxcp_global.gSR(k).Chart_of_Accounts_id = cChart_of_accounts_id AND xxcp_global.gSR(k).Instance_id = cInstance_id) THEN

          cInternalErrorCode     := 0;
          vSegment_Rule_Found    := TRUE;
          vAcc_Asq1              := NULL;
          vAcc_Asq2              := NULL;
          vStoredValue           := NULL;
          vAcc_segment_number    := xxcp_global.gSR(k).segment_number;
          vAcc_account_set_id    := xxcp_global.gSR(k).account_set_id;
          
          vSR_Loop_Cnt := vSR_Loop_Cnt + 1;
          
          If vSR_Loop_Cnt = 1 then
             vMaster_Account_Set_id := vAcc_Account_Set_id;
             vMaster_Source_id      := xxcp_global.gCommon(1).Source_id;
          End If;
          
          vACC_Account_set_name  := xxcp_global.gSR(k).Set_Name;
          vCOA_Cross_Validation  := xxcp_global.gSR(k).Cross_Validation;
          vACC_ACCOUNT_NXT_SET   := 0;

          IF xxcp_wks.Enabled_Segments(vAcc_segment_number) = 'Y' THEN
            -- only if segment is enabled

            v3 := xxcp_foundation.Find_DynamicAttribute(xxcp_global.gSR(k).segment_attribute_id, cAction);
            v4 := xxcp_global.gSR(k).segment_value;

            vSegmentMessage := vSegmentMessage || '    Segment [' || TO_CHAR(vAcc_segment_number) ||
                            '] Account Set <'  || get_Account_Set_Name(vAcc_Account_Set_id, vCOA_Name, vInstance_Name) || 
                            '> Segment Attribute <' || v3 ||
                            '> Segment Value <' || v4 ||'> COA <'||vCOA_Name||'> Instance <'||vInstance_Name||'>' ||CHR(10);              

            vStoredValue := nvl(v3, v4);

            IF nvl(xxcp_global.gSR(k).Account_set_id, 0) <> 0 THEN

              vASCTL_found := TRUE;

              FOR i IN 1 .. vMaxLoops + 1 LOOP
                EXIT WHEN vACC_Account_set_id = 0;
                EXIT WHEN vASCTL_found = FALSE;
                EXIT WHEN i =(vMaxloops + 1); -- this is done so we can report the error.
                w := 0; -- Make sure loop once only

                vSegmentMessage := vSegmentMessage || '    Segment [' ||
                                TO_CHAR(vACC_Segment_Number) || '] Step <' ||LPAD(TO_CHAR(i), 2, '0') ||
                                '> ACCOUNT SET CTL: Account Set <' ||Get_Account_Set_Name(vACC_ACCOUNT_SET_ID, vCOA_Name, vInstance_Name) ||'> Owner/Partner <'||cEntity_type||'> COA <' ||vCOA_Name|| '>' ||CHR(10);

                vASCTL_Found := FALSE;

                -- Account Set Ctl to get qualifer pointers


                FOR ASCTLRec IN ASCTL(pAcc_Segment_Number   => vAcc_Segment_Number,
                                      pAcc_Account_Set_id   => vAcc_Account_Set_id,
                                      pChart_of_Accounts_id => cChart_of_Accounts_id,
                                      pEntity_type          => cEntity_type) LOOP
                  EXIT WHEN w = 1;

                  vASCTL_Found := TRUE;
                  vXCO_Text    := Null;
                  w            := w + 1;
                  -- Work out Qualifiers for XXCP_ACCOUNT_SETS                  
                  If (vAcc_Account_Set_id = vMaster_Account_Set_id) or (ASCTLRec.Source_id = xxcp_global.gCommon(1).Source_id ) then
                     vASQ1_Attribute_Id := ASCTLRec.Asq1_Attribute_Id;
                     vASQ2_Attribute_Id := ASCTLRec.Asq1_Attribute_Id;
                  vACC_ASQ1 := xxcp_foundation.Find_DynamicAttribute(ASCTLRec.ASQ1_Attribute_Id,cAction);
                  vACC_ASQ2 := xxcp_foundation.Find_DynamicAttribute(ASCTLRec.ASQ2_Attribute_Id,cAction);
                     vXCO_Found := True;
                  Else -- its a different source
                     vXCO_Found := False; -- No translation
                     vXCO_Text := ' X-Ovr Source <'||to_char(ASCTLRec.Source_id)||'>';
                     For XCORec in XCO(pAcc_Segment_Number => vAcc_Segment_Number, pAcc_Account_Set_id => vAcc_Account_Set_id, pSource_id => vMaster_Source_id , pEntity_type => cEntity_type) Loop
                        vASQ1_Attribute_Id := XCORec.Asq1_Attribute_Id;
                        vASQ2_Attribute_Id := XCORec.Asq2_Attribute_Id;
                        
                        vACC_ASQ1 := xxcp_foundation.Find_DynamicAttribute(XCORec.Asq1_Attribute_Id,cAction);
                        vACC_ASQ2 := xxcp_foundation.Find_DynamicAttribute(XCORec.Asq2_Attribute_Id,cAction);
                        vXCO_Found := True;
                     End Loop;
                     
                     If vXCO_Found = False then
                         cInternalErrorCode := 6734;
                         Exit;
                     End If;                     
                  End If;
                  
                  vACC_ACCOUNT_NXT_SET := 0;

                  v1 := NULL;
                  v2 := NULL;
                  vSegmentMessage := vSegmentMessage || 'Segment [' ||TO_CHAR(vACC_Segment_number) ||'] Step <' || LPAD(TO_CHAR(i), 2, '0') ||
                                  '> id <' ||TO_CHAR(vASQ1_Attribute_Id) ||'> Qualifier 1 <' || vACC_ASQ1 ||
                                  '>  id <' ||TO_CHAR(vASQ2_Attribute_Id) ||'> Qualifier 2 <' || vACC_ASQ2 ||'> '||vXCO_Text||chr(10);
                                  
                  If nvl(ASCTLRec.Override_id,0) > 0 then 
                  vSegmentMessage := vSegmentMessage || '    Segment [' ||
                                  TO_CHAR(vACC_Segment_number) ||'] Step <' || LPAD(TO_CHAR(i), 2, '0') ||
                                         '> Override Set <'||get_Account_Set_Name(ASCTLRec.Override_id,vCOA_Name, vInstance_Name)||
                                         '> Ovr id <' ||ASCTLRec.Override_id||'> Instance <'||vInstance_Name||'>' ||CHR(10);
                 
                  End If;
                  If nvl(ASCTLRec.Override_id,0) > 0 then
                    vAcc_account_set_id := ASCTLRec.Override_id;
                  End If;                 

                  x := 0; -- Make sure loop once only

                  FOR ACCSETRec IN ACCSET(pAcc_account_set_id => vAcc_account_set_id,
                                          pAcc_segment_number => vAcc_segment_number,
                                          pACC_ASQ1           => vACC_ASQ1,
                                          pACC_ASQ2           => vACC_ASQ2,
                                          pEffective_Date     => cEffective_Date) LOOP
                    EXIT WHEN x = 1;
                    vACCSET_Found            := TRUE;
                    x  := x + 1;
                    vACC_ACCOUNT_NXT_SET   := ACCSETRec.Next_Acct_Set_Id;
                    vNext_Account_Set_Name := ACCSETRec.Next_Account_Set_Name;

                    IF nvl(ACCSETREC.Segment_Attribute_id, 0) > 0 THEN
                      v1 := xxcp_foundation.Find_DynamicAttribute(ACCSETREC.segment_Attribute_id,cAction);
                    END IF;
                    v2 := ACCSETRec.segment_value;

                    vStoredValue := nvl(nvl(v1, v2), vStoredValue);

                    vSegmentMessage := vSegmentMessage || '    Segment [' ||to_char(vACC_Segment_number) ||
                                    '] Step <' || LPAD(TO_CHAR(i), 2, '0') ||
                                    '> Account Set Values : Attribute <' || v1 ||'> Segment <' || v2 ||
                                    '> Next Account Set <' ||vNEXT_ACCOUNT_SET_NAME || '>' ||CHR(10);
                  END LOOP;
                  vACC_Account_set_id := vACC_ACCOUNT_NXT_SET;
                END LOOP; -- End Account Set Ctl loop

                If vXCO_Found = FALSE then
                  Exit; -- vXCO_Found
                End If;
                
              END LOOP; -- End Account Set Ctl loop
            END IF; -- for end loop;
            -- Default in Source Segment is the calculated segment is null
            -- This will then be tested in the next section of code.
            --
            vSegmentMessage := vSegmentMessage || '    Segment [' || TO_CHAR(vACC_Segment_number) ||'] New Segment Value <' || vStoredValue || '>' ||CHR(10);

            IF xxcp_global.gSR(k).Validation_Type = 0 THEN
              xxcp_wks.Account_Segments(vAcc_segment_number) :=
                 nvl(vStoredValue,xxcp_wks.Source_Segments(vAcc_segment_number));
            ELSE
              -- Check for Null value at this point. Do not default to base value
               xxcp_wks.Account_Segments(vAcc_segment_number) := vStoredValue;
            END IF;

            IF xxcp_wks.Account_Segments(vAcc_segment_number) IS NULL THEN

              -- No Segment Value Found
              cInternalErrorCode := 6700 + vACC_Segment_number;
              IF xxcp_global.gSR(k).Validation_Type = 0 THEN
                vCheckNotNull := 'N';
              ELSE
                vCheckNotNull := 'Y';
              END IF;
              xxcp_foundation.FndWriteError(cInternalErrorCode,'Segment is null','    Segment Rule Not Null Check = '|| vCheckNotNull||CHR(10)||vSegmentMessage);
              IF i > vMaxLoops THEN
                xxcp_foundation.FndWriteError(cInternalErrorCode,'Maximum Account Set Control Loops Hit <' ||TO_CHAR(vMaxLoops) || '>');
              END IF;
            ELSE

              IF xxcp_global.gSR(k).segment_number <= 10 THEN
                xxcp_wks.I1009_ArrayTEMP(10 + xxcp_global.gSR(k).segment_number) := xxcp_wks.Account_Segments(vAcc_segment_number);
              ELSIF xxcp_global.gSR(k).segment_number BETWEEN 11 AND 15 THEN
                xxcp_wks.I1009_ArrayTEMP(30 + xxcp_global.gSR(k).segment_number) := xxcp_wks.Account_Segments(vAcc_segment_number);
              END IF;
            END IF;

          END IF;

          EXIT WHEN cInternalErrorCode <> 0;
        END IF;
      END LOOP; -- vAcc_segment_number
     END IF; -- Error before segments (6782)

      --
      -- Check the Account Segments
      --

     IF cInternalErrorCode = 0 AND (cEnforce_Accounting = 'Y') THEN

        q := 0;
        vAccountString := Null;
        FOR i IN 1 .. vMax_Segments LOOP

          IF i = vMax_Segments THEN
            vAccountString := vAccountString || xxcp_wks.Account_Segments(i);
          ELSE
            vAccountString := vAccountString ||nvl(xxcp_wks.Account_Segments(i),' ')||vSegDelimiter;
          END IF;

          IF ((xxcp_wks.Enabled_Segments(i) = 'Y') AND (RTRIM(xxcp_wks.Enabled_Segments(i)) IS NULL)) THEN
            cInternalErrorCode := 6800 + i;
            vSegmentMessage    := vSegmentMessage || '    Segment [' || TO_CHAR(i) ||'] Enabled Account Segment required' || CHR(10);
          END IF;
          IF ((xxcp_wks.Enabled_Segments(i) <> 'Y') AND (RTRIM(xxcp_wks.Account_Segments(i)) IS NOT NULL)) THEN
            cInternalErrorCode := 6900 + i;
            vSegmentMessage    := vSegmentMessage || '    Segment [' || TO_CHAR(i) ||'] Account Segment not enabled. New Segment Value <' || RTRIM(xxcp_wks.Account_Segments(i)) || '>' ||CHR(10);
          END IF;

          -- Cross Validation
          IF xxcp_wks.Enabled_Segments(i) = 'Y' Then
            IF cTarget_Cross_Validation = 'Y' or vCOA_Cross_Validation = 'Y' THEN
              vKeyFlexSegValue  := rtrim(xxcp_wks.Account_Segments(i));
              v_KeyFlexField(i) := vKeyFlexSegValue;
            END IF;
          END IF;
          q := i;
          EXIT WHEN cInternalErrorCode > 0;
        END LOOP;
        --
        -- Cross Validations Logic,
        -- This can be called, but there are various options because Oracle has a flag that if used will create whatever
        -- account code that is passed. You many not want this to happen.
        --
        IF (cTarget_Cross_Validation = 'Y' OR vCOA_Cross_Validation = 'Y') THEN
          vCrossVal := 'F';

          vNew_ccid := Check_Combination_id(cChart_of_Accounts_id   => cChart_of_Accounts_id,
                                            cMax_Segments           => vMax_Segments,
                                            cAccount_Segments       => xxcp_wks.Account_Segments,
                                            cGl_Date                => xxcp_global.gCommon(1).current_Accounting_date,
                                            cInstance_id            => cInstance_id);

          IF nvl(vNew_ccid,0) > 0  THEN
            vCrossVal := 'P';
          ELSE
              -- Cross Validation
              IF cInstance_id = 0 THEN
                vAccountString := Fnd_Flex_Ext.concatenate_segments(n_segments =>   vMax_Segments ,segments =>  v_keyflexfield,delimiter => vSegDelimiter);

                IF xxcp_global.Preview_on = 'N' THEN

                   IF Fnd_Flex_Ext.get_combination_id(application_short_name => 'SQLGL',
                                                      key_flex_code          => 'GL#',
                                                      structure_number       => cChart_of_accounts_id, -- ID Flex Num
                                                      validation_date        => xxcp_global.gCommon(1).current_Accounting_date,
                                                      n_segments             => vMax_Segments,         --  No of Segments,
                                                      segments               => v_KeyFlexField,        -- Segments 1-15
                                                      combination_id         => vNew_ccid)             -- New Code Combination_id
                  THEN
                    vCrossVal := 'P';
                  ELSE
                    vError_Message := Fnd_Message.GET;
                 END IF;

              ELSIF xxcp_global.Preview_on = 'Y' THEN

                IF fnd_flex_keyval.validate_segs(    Operation                => 'CHECK_COMBINATION',
                                                     Appl_short_name          => 'SQLGL',
                                                     Key_Flex_Code            => 'GL#',
                                                     Structure_number         => cChart_of_accounts_id,
                                                     Concat_Segments          => vAccountString,
                                                     Validation_date          => xxcp_global.gCOMMON(1).current_Accounting_date) THEN
                  vCrossVal := 'P';
                Else
                  vError_Message := Fnd_Flex_Keyval.error_message;
                End If;
              End If;

               ELSE
                  -- Target Instances:
                  -- Calling Cross Validations on diferent instance will require additional
                  -- code in the custom event API.
                  IF xxcp_Custom_Events.remote_cross_validation(
                                                           cSource_id            => xxcp_global.gCommon(1).Source_id,
                                                           cSource_Assignment_id => xxcp_global.gCommon(1).current_Assignment_id,
                                                           cInstance_id          => cInstance_id,
                                                           cChart_of_accounts_id => cChart_of_accounts_id,
                                                           cAccounting_date      => xxcp_global.gCommon(1).current_Accounting_date,
                                                           cMax_Segments         => vMax_Segments,
                                                           cKeyFlexField         => v_KeyFlexField,
                                                           cCode_Combination_id  => vNew_ccid)
                  THEN
                    vCrossVal := 'P';
                 END IF;
               END IF;
               IF vCrossVal = 'F' THEN
                   cInternalErrorCode := 6781; -- Update table with error code.
                   xxcp_foundation.FndWriteError(cInternalErrorCode,'Code Combination <'||vAccountString||'>',vError_Message);
               END IF;
            END IF;
        END IF;

        xxcp_wks.I1009_ArrayTEMP(84) := vNew_ccid;
        --
        -- End of Cross Validation Logic
        --
        IF cInternalErrorCode > 0 and cInternalErrorCode != 6781 THEN
          xxcp_foundation.FndWriteError(cInternalErrorCode,
                                          'Rule id <' || to_char(cRule_id) ||
                                        '> Chart of Accounts <' ||TO_CHAR(cChart_of_accounts_id) ||
                                        '> Instance <' ||to_char(cInstance_id) ||
                                        '> Segment <' || to_char(q) ||
                                        '> Segment Value <' ||rtrim(xxcp_wks.Account_Segments(q)) ||
                                        '> Segment Required <' ||xxcp_wks.Enabled_Segments(q) || '>',
                                        vSegmentMessage);
        END IF;
      END IF;
    END IF;

    -- Trace
    xxcp_trace.Transaction_Trace(cTrace_id            => 7,
                                 cRule_id             => cRule_id,
                                 cRule_Name           => cRule_Name,
                                 cTarget_COA_id       => cChart_of_accounts_id,
                                 cTarget_Instance_id  => cInstance_id,
                                 cAccount_String      => vAccountString,
                                 cCode_Combination_id => vNew_ccid,
                                 cCross_Validation    => vCrossVal,
                                 cLongMessage         => vSegmentMessage);


  END Get_Segment_Rules;

  --
  -- ## ***********************************************************************************************************
  -- ##
  -- ##                                      Get_Column_Attributes
  -- ##                                   Work out the Column Attributes
  -- ##
  -- ## ***********************************************************************************************************
  PROCEDURE Get_Column_Attributes(cAction             IN VARCHAR2,
                                  cTarget_Table       IN VARCHAR2,
                                  cTarget_Instance_id IN NUMBER,
                                  cModel_Name_id      IN NUMBER,
                                  cInternalErrorCode  IN OUT NOCOPY NUMBER) IS

    vCS_DYNAMIC_ATTRIBUTE_ID XXCP_COLUMN_SET_RULES.DYNAMIC_ATTRIBUTE_ID%TYPE;
    vCS_COLUMN_SET_ID        XXCP_COLUMN_SETS.COLUMN_SET_ID%TYPE;
    vCS_COLUMN_NEXT_SET_ID   XXCP_COLUMN_SETS.NEXT_COLUMN_SET_ID%TYPE;
    vCS_Q1                   XXCP_COLUMN_SETS.CS1_QUALIFIER%TYPE;
    vCS_Q2                   XXCP_COLUMN_SETS.CS1_QUALIFIER%TYPE;

    v1 VARCHAR2(250);
    v2 VARCHAR2(250);
    v3 VARCHAR2(250);
    v4 VARCHAR2(250);

    --
    cursor cs_ctl(pcolumn_set_id in number ) is
      select nvl(ast.attribute3, 0) csq1_attribute_id,
             nvl(ast.attribute4, 0) csq2_attribute_id,
             set_name
        from xxcp_table_set_ctl     ast
       where ast.set_id  = pcolumn_set_id
         and ast.set_base_table = 'CP_COLUMN_SET_CTL'
         and ast.source_id = xxcp_global.gcommon(1).source_id;

    cursor cs_set(pcolumn_set_id in number, pcsq1 in varchar2, pcsq2 in varchar2) is
      select a.column_value,
             nvl(a.column_attribute_id, 0) column_attribute_id,
             nvl(a.next_column_set_id, 0)  next_column_set_id,
             t.set_name                    next_column_set_name
        from xxcp_column_sets a, xxcp_table_set_ctl t
       where a.column_set_id = pcolumn_set_id
         and a.source_id     = xxcp_global.gcommon(1).source_id
         and (nvl(a.cs1_qualifier, nvl(pcsq1, '~Null~')) =  nvl(pcsq1, '~Null~')
         and nvl(a.cs2_qualifier, nvl(pcsq2, '~Null~'))  = nvl(pcsq2, '~Null~'))
         and nvl(a.next_column_set_id, 0) = t.set_id(+)
         and a.source_id                  = t.source_id(+)
         and 'CP_COLUMN_SET_CTL' = t.set_base_table(+)
       order by (decode(a.cs1_qualifier, null, 0, 1) * 10) + decode(a.cs2_qualifier, null, 0, 1) desc;

    vMaxLoops        pls_integer := 15;
    vLongMessage     long;

    i pls_integer := 0;
    x pls_integer := 0;
    k pls_integer := 0;
    w pls_integer := 0;
    --
    vCsset_found boolean;
    vCsctl_found boolean := false;

    vCurrent_attribute    number(8);
    vCs_column_name       xxcp_dynamic_attributes.column_name%type;
    vCs_column_set_name   xxcp_table_set_ctl.set_name%type;
    vNext_column_set_name xxcp_table_set_ctl.set_name%type;
    vValidation_type      pls_integer := 0;

    vStoredvalue          varchar2(250);
    vCSA_cnt              pls_integer := 0;
    v                     pls_integer := 0;


  -- Get a distinct list of Attributes to find
  Cursor ca(pModel_Name_id in number) is
   select distinct dynamic_attribute_id
    from xxcp_column_set_rules r
   where source_id     = xxcp_global.gcommon(1).source_id
     and model_name_id in (0,pModel_Name_id);

  Cursor cs(pmodel_name_id in number, pcurrent_attribute in number) is
   select c.model_name_id,
          nvl(t.set_name,'None') column_set_name,
          c.dynamic_attribute_id,
          x.attribute_name,
          c.column_value,
          c.column_attribute_id,
          c.column_set_id,
          c.validation_type
    from xxcp_column_set_rules c
        ,xxcp_table_set_ctl t
        ,xxcp_dynamic_attributes_v x
   where c.source_id             = xxcp_global.gcommon(1).source_id
     and c.dynamic_attribute_id  = x.id
     and c.dynamic_attribute_id  = pCurrent_Attribute
     and c.column_set_id         = t.set_id(+)
     and t.set_base_table(+)     = 'CP_COLUMN_SET_CTL'
     and (c.model_name_id = pmodel_name_id or model_name_id = 0)
     -- only bring back one. the one with a model name id in preference
     and (c.dynamic_attribute_id, c.model_name_id) = any(select max(w.dynamic_attribute_id), max(w.model_name_id)
                                                           from xxcp_column_set_rules w
                                                          where w.dynamic_attribute_id = c.dynamic_attribute_id
                                                            and w.source_id = xxcp_global.gcommon(1).source_id
                                                            and (w.model_name_id        = pmodel_name_id
                                                                or w.model_name_id      = 0)
                                                            and w.dynamic_attribute_id  = pcurrent_attribute
                                                            and w.source_id             = c.source_id);

   vColAttUsed Boolean := FALSE;

  BEGIN

    xxcp_wks.Reset_Column_Segments;

    If nvl(gValidateColumnAttributes,'Y') = 'Y' then 
      --
      --
      -- Column Attributes are only used if they are defined in the Column Rules.
      -- That is why the gCSA records are checked.
      --
      vCSA_Cnt := xxcp_global.gCSA_Cnt;

      -- Find out if Column Attributes are used for this target
      For v in 1 .. vCSA_cnt loop --
        If (((xxcp_global.gcsa(v).Target_Table       = cTarget_Table) and
             (xxcp_global.gcsa(v).Target_Instance_id = cTarget_Instance_id))) then
          vColAttUsed := true;
          exit;
        End if;
      End loop;
    Else 
      vColAttUsed := true;
    End if; 

    -- Column Rule Found
    IF vColAttUsed THEN

      vLongMessage := '  COLUMN ATTRIBUTES..'||CHR(10);

      FOR CARec IN CA(cModel_Name_id) LOOP
        vCurrent_Attribute := CARec.dynamic_attribute_id;

        vCS_Q1       := NULL;
        vCS_Q2       := NULL;
        vStoredValue := NULL;
        k            := k + 1;

        FOR csRec IN cs(cModel_Name_id, vCurrent_Attribute) LOOP

            -- #####  Entry Point ######
            cInternalErrorCode := 0;
            vCSCTL_found       := FALSE;

            vCS_Column_Name          := csRec.Attribute_Name;
            vCS_Dynamic_attribute_id := csRec.Dynamic_attribute_id;
            vCS_Column_set_id        := csRec.Column_set_id;
            vCS_Column_set_name      := csRec.Column_set_name;
            vValidation_type         := csRec.Validation_type;
            vCS_Column_next_set_id   := 0;

            v3 := xxcp_foundation.Find_DynamicAttribute(csRec.column_attribute_id, cAction);
            v4 := csRec.COLUMN_VALUE;

            vLongMessage := vLongMessage || '    [' || vCurrent_Attribute ||
                            ']: Column Attribute <' || vCS_Column_Name ||
                            '> Value <' || v4 || '> Attribute <' || v3 ||
                            '> Column Set <' || csRec.Column_set_name|| '>' || CHR(10);

            vStoredValue := nvl(v3, v4);

            -- #####  Entry Point ######
            IF nvl(csRec.column_set_id, 0) = 0 THEN
              vStoredValue := nvl(v3, v4);
            ELSE
              vCSCTL_found := FALSE;
              For i in 1 .. vMaxLoops + 1 LOOP
                exit when vcs_column_set_id = 0;
                exit when i =(vmaxloops + 1); -- this is done so we can report the error.
                w := 0; -- Make sure loop once only

                vLongMessage := vLongMessage || '    [' || vCurrent_Attribute ||
                                ']: Column Set <' || TO_CHAR(vCS_Column_Set_id) || '>' || CHR(10);

                vCSCTL_found := FALSE;
                -- Account Set Ctl to get qualifer pointers
                FOR CS_CTLRec IN CS_CTL(vCS_Column_Set_id) LOOP
                  EXIT WHEN w = 1;

                  vCSCTL_found := TRUE;

                  w := w + 1;
                  -- Work out Qualifiers for XXCP_ACCOUNT_SET
                  vCS_Q1 := xxcp_foundation.Find_DynamicAttribute(CS_CTLRec.CSQ1_Attribute_Id, cAction);
                  vCS_Q2 := xxcp_foundation.Find_DynamicAttribute(CS_CTLRec.CSQ2_Attribute_Id, cAction);
                  vCS_Column_next_set_id := 0;

                  v1 := NULL;
                  v2 := NULL;

                  x := 0; -- Make sure loop once only
                  FOR CSSETRec IN CS_SET(vCS_Column_set_id,vCS_Q1,vCS_Q2) LOOP
                    EXIT WHEN x = 1;
                    x                      := x + 1;
                    vCSSet_Found           := TRUE;
                    vCS_Column_Next_Set_id := nvl(CSSETRec.Next_Column_Set_Id,0);
                    vNext_Column_Set_Name  := CSSETRec.Next_Column_Set_Name;

                    IF nvl(CSSETREC.Column_Attribute_id, 0) > 0 THEN
                      v1 := xxcp_foundation.Find_DynamicAttribute(CSSETREC.Column_Attribute_id, cAction);
                    END IF;

                    v2           := CSSetRec.COLUMN_VALUE;
                    vStoredValue := nvl(nvl(v1, v2), vStoredValue);

                  END LOOP;
                  vCS_Column_set_id := vCS_Column_next_set_id;
                END LOOP; -- End Column Set Ctl loop

                IF vCSCTL_found = FALSE THEN
                  vCS_Column_set_id := 0;
                END IF;

              END LOOP; -- End Column Set Ctl loop
            END IF; -- for end loop;
          --  Exit;
          -- **************** END ATTRIBUTE LOGIC ******************************

          EXIT WHEN cInternalErrorCode <> 0;

          IF cInternalErrorCode = 0 THEN
            IF vValidation_type IN (1, 3) AND vStoredValue IS NULL THEN
              cInternalErrorCode := 8823;
              xxcp_foundation.FndWriteError(cInternalErrorCode,
                                            'Column Attribute <' ||vCS_Column_Name || '> ID <' ||
                                            TO_CHAR(vCS_Dynamic_attribute_id) || '>', vLongMessage);
              EXIT;
            ELSIF vValidation_type IN (2, 3) AND vStoredValue = '0' THEN
              cInternalErrorCode := 8824;
              xxcp_foundation.FndWriteError(cInternalErrorCode,
                                            'Column Attribute <' ||vCS_Column_Name || '> ID <' || TO_CHAR(vCS_Dynamic_attribute_id) || '>',
                                            vLongMessage);
              EXIT;
            ELSE
              IF vCS_Dynamic_attribute_id >= 6000 THEN
                xxcp_wks.D1006_Array(vCurrent_Attribute - 6000) := vStoredValue;
              END IF;
            END IF;
          END IF;

          vCurrent_Attribute := NULL; -- Only let the value be calculated once with Model Name id first.

        END LOOP; -- Column Set Rules

        EXIT WHEN cInternalErrorCode <> 0;

      END LOOP;

      xxcp_trace.Transaction_Trace(cTrace_id => 8, cLongMessage => vLongMessage);

    END IF;

  END Get_Column_Attributes;
  --
  -- ## ***********************************************************************************************************
  -- ##
  -- ##                                      Column_Rules
  -- ##                    Store Working Storage for Columns Rules to apply
  -- ## ***********************************************************************************************************
  --
  PROCEDURE Column_Rules(cRule_type              IN VARCHAR2,
                         cRule_id                IN NUMBER,
                         cModel_Ctl_id           IN NUMBER,
                         cTarget_Table           IN VARCHAR2,
                         cAttribute_id           IN NUMBER,
                         cInterface_ID           IN NUMBER,
                         cTarget_Set_of_books_id IN NUMBER,
                         cTransaction_Class      IN VARCHAR2,
                         cTarget_Type_id         IN NUMBER,
                         cTarget_Record_Status   IN VARCHAR2,
                         cTarget_Rounding        IN VARCHAR2,
                         cRound_Suppressed_Rules IN VARCHAR2,
                         cTarget_Reg_id          IN NUMBER,
                         cNo_Rounding_Rule       IN VARCHAR2,
                         cBalancing_Segment      IN NUMBER,
                         cRounding_Group         IN VARCHAR2,
                         cRule_Entity_Type       IN VARCHAR2,
                         cTarget_Assignment_id   IN NUMBER,
                         cTarget_Zero_Flag       IN VARCHAR2,
                         cTax_Registration_id    IN NUMBER,
                         cTarget_Instance_id     IN NUMBER,
                         cTarget_COA_ID          IN NUMBER,
                         cZero_Suppression_Rule  IN VARCHAR2,
                         cCost_Plus_Rule         IN VARCHAR2,
                         cExtended_Values        IN VARCHAR2,
                         cEntered_Amounts        IN VARCHAR2,
                         cAccounted_Amounts      IN VARCHAR2,
                         cSettlement_rule        IN VARCHAR2,
                         cReporting_field1       IN VARCHAR2,
                         cReporting_field2       IN VARCHAR2,
                         cReporting_field3       IN VARCHAR2,
                         cReporting_field4       IN VARCHAR2,
                         cReporting_field5       IN VARCHAR2) IS

    -- #
    -- # This procedure is responsible for holding the results of each account rule
    -- # in memory and passing them back to the outter wrapper.
    -- #

    fe PLS_INTEGER := xxcp_wks.WORKING_CNT;

    vEnt_Pointer       number := 21;
    vAcc_Pointer       number := 23;
    vEntered_Value     number := 0;
    vAccountd_Value    number := 0;
    vAccounted_Amounts varchar2(1);

  BEGIN

    fe := fe + 1;
    xxcp_wks.WORKING_CNT := fe;

    vAccounted_Amounts := cAccounted_Amounts;

    xxcp_trace.Transaction_Trace( cTrace_id => 9,cRule_Type => cRule_Type, cTarget_Record_Status => cTarget_Record_Status);

    If cExtended_Values = 'Y' then
      vEnt_Pointer   := 21;
      vAcc_Pointer   := 23;
    Else
      vEnt_Pointer   := 81;
      vAcc_Pointer   := 83;
    End If;

    IF cRule_Type IN ('S','T','O') THEN
      xxcp_wks.WORKING_RCD(fe).Accounted_Currency    := xxcp_wks.I1009_ArrayCH(56); -- Accounted Currency_code
      xxcp_wks.WORKING_RCD(fe).Entered_Currency      := xxcp_wks.I1009_ArrayCH(22);

      xxcp_wks.I1009_ArrayCH(49) := xxcp_global.gCommon(1).current_Batch_number;
      xxcp_wks.I1009_ArrayCH(50) := xxcp_global.gCommon(1).current_category_name;
      xxcp_wks.I1009_ArrayCH(51) := xxcp_global.gCommon(1).current_Source_name;
      xxcp_wks.I1009_ArrayCH(52) := xxcp_global.gCommon(1).current_Accounting_date;
      xxcp_wks.I1009_ArrayCH(58) := cRule_id;
      xxcp_wks.I1009_ArrayCH(66) := cRule_type;
      xxcp_wks.I1009_ArrayCH(82) := sign(nvl(xxcp_wks.I1009_ArrayCH(vEnt_Pointer),0)+nvl(xxcp_wks.I1009_ArrayCH(vAcc_Pointer),0));
      xxcp_wks.WORKING_RCD(fe).Entered_Amount   := xxcp_wks.I1009_ArrayCH(vEnt_Pointer); -- Entered Sum
      xxcp_wks.WORKING_RCD(fe).Accounted_Amount := xxcp_wks.I1009_ArrayCH(vAcc_Pointer); -- Accounted Sum
      xxcp_wks.WORKING_RCD(fe).I1009_Array      := xxcp_wks.I1009_ArrayCH; -- Internal
      xxcp_wks.WORKING_RCD(fe).D1001_Array      := xxcp_wks.D1001_ArrayCH; -- Configuration
      xxcp_wks.WORKING_RCD(fe).D1002_Array      := xxcp_wks.D1002_ArrayCH; -- Configuration Pricing
      xxcp_wks.WORKING_RCD(fe).D1003_Array      := xxcp_wks.D1003_ArrayCH; -- Accounting
      xxcp_wks.WORKING_RCD(fe).D1004_Array      := xxcp_wks.D1004_ArrayCH; -- Accounting Pricing
      xxcp_wks.WORKING_RCD(fe).D1006_Array      := xxcp_wks.D1006_ArrayCH; -- Custom Attributes
      
      -- 03.06.20 Copy the user defined classification attribute to the internal attribute for column rules
      xxcp_wks.WORKING_RCD(fe).I1009_Array(152) := xxcp_foundation.Find_DynamicAttribute(xxcp_te_base.Get_Classification_Attribute, 'S' );      
      -- 03.06.26
      xxcp_wks.WORKING_RCD(fe).I1009_Array(159) := xxcp_foundation.Find_DynamicAttribute(xxcp_te_base.Get_Approve_Restricted_Att, 'S' );            
      
   ELSIF cRule_Type = 'L' THEN
      xxcp_wks.WORKING_RCD(fe).Accounted_Currency := xxcp_wks.I1009_ArrayLC(56); -- Accounted Currency_code
      xxcp_wks.WORKING_RCD(fe).Entered_Currency   := xxcp_wks.I1009_ArrayLC(22);
      xxcp_wks.I1009_ArrayLC(49) := xxcp_global.gCommon(1).current_Batch_number;
      xxcp_wks.I1009_ArrayLC(50) := xxcp_global.gCommon(1).current_category_name;
      xxcp_wks.I1009_ArrayLC(51) := xxcp_global.gCommon(1).current_Source_name;
      xxcp_wks.I1009_ArrayLC(52) := xxcp_global.gCommon(1).current_Accounting_date;
      xxcp_wks.I1009_ArrayLC(58) := cRule_id;
      xxcp_wks.I1009_ArrayLC(66) := 'L';
      xxcp_wks.I1009_ArrayLC(82) := SIGN(nvl(xxcp_wks.I1009_ArrayLC(vEnt_Pointer),0)+nvl(xxcp_wks.I1009_ArrayLC(vAcc_Pointer),0));

      xxcp_wks.WORKING_RCD(fe).Entered_Amount   := xxcp_wks.I1009_ArrayLC(vEnt_Pointer); -- Entered Sum
      xxcp_wks.WORKING_RCD(fe).Accounted_Amount := xxcp_wks.I1009_ArrayLC(vAcc_Pointer); -- Accounted Sum
      xxcp_wks.WORKING_RCD(fe).I1009_Array      := xxcp_wks.I1009_ArrayLC; -- Internal

      xxcp_wks.WORKING_RCD(fe).D1001_Array := xxcp_wks.D1001_ArrayLC; -- Configuration
      xxcp_wks.WORKING_RCD(fe).D1002_Array := xxcp_wks.D1002_ArrayLC; -- Configuration Pricing
      xxcp_wks.WORKING_RCD(fe).D1003_Array := xxcp_wks.D1003_ArrayLC; -- Accounting
      xxcp_wks.WORKING_RCD(fe).D1004_Array := xxcp_wks.D1004_ArrayLC; -- Accounting Pricing
      xxcp_wks.WORKING_RCD(fe).D1006_Array := xxcp_wks.D1006_ArrayLC; -- Custom Attributes
      
      -- 03.06.20 Copy the user defined classification attribute to the internal attribute for column rules
      xxcp_wks.WORKING_RCD(fe).I1009_Array(152) := xxcp_foundation.Find_DynamicAttribute(xxcp_te_base.Get_Classification_Attribute, 'L' );            
      -- 03.06.26
      xxcp_wks.WORKING_RCD(fe).I1009_Array(159) := xxcp_foundation.Find_DynamicAttribute(xxcp_te_base.Get_Approve_Restricted_Att, 'L' );            
      
   -- NCM 18/11/2008 Added Gain and Loss Wonce rule.
   ELSIF cRule_Type IN ('G','W') THEN
      xxcp_wks.WORKING_RCD(fe).Accounted_Currency := xxcp_wks.I1009_ArrayGL(56); -- Accounted Currency_code
      xxcp_wks.WORKING_RCD(fe).Entered_Currency   := xxcp_wks.I1009_ArrayGL(22);
      xxcp_wks.I1009_ArrayGL(49) := xxcp_global.gCommon(1).current_Batch_number;
      xxcp_wks.I1009_ArrayGL(50) := xxcp_global.gCommon(1).current_category_name;
      xxcp_wks.I1009_ArrayGL(51) := xxcp_global.gCommon(1).current_Source_name;
      xxcp_wks.I1009_ArrayGL(52) := xxcp_global.gCommon(1).current_Accounting_date;
      xxcp_wks.I1009_ArrayGL(58) := cRule_id;
      xxcp_wks.I1009_ArrayGL(66) := 'G';
      xxcp_wks.I1009_ArrayGL(82) := SIGN(nvl(xxcp_wks.I1009_ArrayGL(vEnt_Pointer),0)+nvl(xxcp_wks.I1009_ArrayGL(vAcc_Pointer),0));
      xxcp_wks.WORKING_RCD(fe).Entered_Amount   := xxcp_wks.I1009_ArrayGL(vEnt_Pointer); -- Entered Sum
      xxcp_wks.WORKING_RCD(fe).Accounted_Amount := xxcp_wks.I1009_ArrayGL(vAcc_Pointer); -- Accounted Sum
      xxcp_wks.WORKING_RCD(fe).I1009_Array      := xxcp_wks.I1009_ArrayGL; -- Internal

      xxcp_wks.WORKING_RCD(fe).D1001_Array := xxcp_wks.D1001_ArrayGL; -- Configuration
      xxcp_wks.WORKING_RCD(fe).D1002_Array := xxcp_wks.D1002_ArrayGL; -- Configuration Pricing
      xxcp_wks.WORKING_RCD(fe).D1003_Array := xxcp_wks.D1003_ArrayGL; -- Accounting
      xxcp_wks.WORKING_RCD(fe).D1004_Array := xxcp_wks.D1004_ArrayGL; -- Accounting Pricing
      xxcp_wks.WORKING_RCD(fe).D1006_Array := xxcp_wks.D1006_ArrayGL; -- Custom Attributes

      -- 03.06.20 Copy the user defined classification attribute to the internal attribute for column rules
      xxcp_wks.WORKING_RCD(fe).I1009_Array(152) := xxcp_foundation.Find_DynamicAttribute(xxcp_te_base.Get_Classification_Attribute, 'G' );            
      -- 03.06.26
      xxcp_wks.WORKING_RCD(fe).I1009_Array(159) := xxcp_foundation.Find_DynamicAttribute(xxcp_te_base.Get_Approve_Restricted_Att, 'G' );            

   END IF;

    xxcp_wks.WORKING_RCD(fe).No_Rounding_Rule       := cNo_Rounding_Rule;
    xxcp_wks.WORKING_RCD(fe).Cost_Plus_Rule         := cCost_Plus_Rule;
    
    -- 03.06.20
    xxcp_wks.WORKING_RCD(fe).I1009_Array(151)     := cSettlement_rule;
    xxcp_wks.WORKING_RCD(fe).I1009_Array(153)     := cReporting_field1;
    xxcp_wks.WORKING_RCD(fe).I1009_Array(154)     := cReporting_field2;
    xxcp_wks.WORKING_RCD(fe).I1009_Array(155)     := cReporting_field3;
    xxcp_wks.WORKING_RCD(fe).I1009_Array(156)     := cReporting_field4;
    xxcp_wks.WORKING_RCD(fe).I1009_Array(157)     := cReporting_field5;
    
    -- 03.06.23b
    -- STAT Currency blancing Override 
    If xxcp_wks.WORKING_RCD(fe).Entered_Currency = 'STAT' then
      xxcp_wks.WORKING_RCD(fe).Target_Rounding  := 'N';
      vAccounted_Amounts := 'N';
    Else
      xxcp_wks.WORKING_RCD(fe).Target_Rounding  := nvl(cTarget_Rounding,'Y');
    End If;
    -- 
    
    xxcp_wks.WORKING_RCD(fe).I1009_Array(158)       := xxcp_global.gCommon(1).current_trading_set;    
    xxcp_wks.WORKING_RCD(fe).Extended_Values        := cExtended_Values;
    xxcp_wks.WORKING_RCD(fe).Entered_Amounts        := cEntered_Amounts;
    xxcp_wks.WORKING_RCD(fe).Accounted_Amounts      := cAccounted_Amounts;

    xxcp_wks.WORKING_RCD(fe).Rule_id                := cRule_id;
    xxcp_wks.WORKING_RCD(fe).Model_Ctl_id           := cModel_Ctl_id;
    xxcp_wks.WORKING_RCD(fe).Rule_Entity_Type       := Substr(cRule_Entity_Type,1,1);
    xxcp_wks.WORKING_RCD(fe).I1009_Array(96)        := xxcp_wks.WORKING_RCD(fe).Rule_Entity_Type;
    xxcp_wks.WORKING_RCD(fe).Target_reg_id          := cTarget_reg_id;
    xxcp_wks.WORKING_RCD(fe).Balancing_segment      := cBalancing_segment;
    xxcp_wks.WORKING_RCD(fe).Source_rowid           := xxcp_global.gCommon(1).current_Source_rowid;
    xxcp_wks.WORKING_RCD(fe).Parent_rowid           := xxcp_global.gCommon(1).current_Parent_rowid;
    xxcp_wks.WORKING_RCD(fe).Attribute_id           := cAttribute_id;
    xxcp_wks.WORKING_RCD(fe).Record_type            := cRule_type;
    xxcp_wks.WORKING_RCD(fe).Record_Status          := nvl(cTarget_Record_Status,'?');
--    xxcp_wks.WORKING_RCD(fe).Target_Rounding        := nvl(cTarget_Rounding,'Y');
    xxcp_wks.WORKING_RCD(fe).Round_Suppressed_Rules := nvl(cRound_Suppressed_Rules,'Y');
    xxcp_wks.WORKING_RCD(fe).Interface_id           := nvl(cInterface_id, 0);
    xxcp_wks.WORKING_RCD(fe).Target_Table           := cTarget_Table;
    xxcp_wks.WORKING_RCD(fe).Target_Instance_id     := cTarget_Instance_id;
    --
    xxcp_wks.WORKING_RCD(fe).Transaction_Ref1       := xxcp_wks.D1001_Array(3);
    --
    xxcp_wks.WORKING_RCD(fe).Accounted_Rnd_Amount := NULL;
    xxcp_wks.WORKING_RCD(fe).Entered_Rnd_Amount   := NULL;
    xxcp_wks.WORKING_RCD(fe).Transaction_Table    := xxcp_global.gCommon(1).current_Transaction_Table;
    xxcp_wks.WORKING_RCD(fe).Trading_Set          := xxcp_global.gCommon(1).current_Trading_Set;
    xxcp_wks.WORKING_RCD(fe).Trading_Set_Id       := xxcp_global.gCommon(1).current_Trading_Set_Id;
    xxcp_wks.WORKING_RCD(fe).I1009_Array(149)     := xxcp_global.gCommon(1).current_assignment_id;
    xxcp_wks.WORKING_RCD(fe).I1009_Array(150)     := xxcp_global.gCommon(1).Source_id;
    xxcp_wks.WORKING_RCD(fe).Set_of_books_id      := cTarget_Set_of_books_id;
    xxcp_wks.WORKING_RCD(fe).Tax_Registration_id  := cTax_Registration_id;
    xxcp_wks.WORKING_RCD(fe).Target_Assignment_id := cTarget_Assignment_id;
    xxcp_wks.WORKING_RCD(fe).Target_Zero_Flag     := nvl(cTarget_Zero_Flag,'N');
    xxcp_wks.WORKING_RCD(fe).Transaction_Class    := cTransaction_Class;
    xxcp_wks.WORKING_RCD(fe).Target_Type_id       := cTarget_Type_id;

    xxcp_wks.WORKING_RCD(fe).Record_Number := fe; -- new
    xxcp_wks.WORKING_RCD(fe).Zero_Suppression_rule := cZero_Suppression_rule; -- help

    xxcp_wks.WORKING_RCD(fe).Zero_Suppression_rule := cZero_Suppression_rule;
    xxcp_wks.WORKING_RCD(fe).Trading_Set_Seq       := xxcp_global.gCommon(1).current_Trading_Set_seq;
    xxcp_wks.WORKING_RCD(fe).Transaction_id        := xxcp_global.gCommon(1).current_Transaction_id;
    --
    xxcp_wks.WORKING_RCD(fe).balancing_elements(1)  := cTarget_Set_of_books_id; -- cTarget_Assignment_id;
    xxcp_wks.WORKING_RCD(fe).balancing_elements(2)  := xxcp_wks.Source_Balancing(2);
    xxcp_wks.WORKING_RCD(fe).balancing_elements(3)  := xxcp_wks.Source_Balancing(3);
    xxcp_wks.WORKING_RCD(fe).balancing_elements(4)  := xxcp_wks.Source_Balancing(4);
    xxcp_wks.WORKING_RCD(fe).balancing_elements(5)  := xxcp_wks.working_rcd(fe).Accounted_Currency;
    xxcp_wks.WORKING_RCD(fe).balancing_elements(6)  := xxcp_wks.Source_Balancing(6);
    xxcp_wks.WORKING_RCD(fe).balancing_elements(7)  := xxcp_wks.Source_Balancing(7);
    xxcp_wks.WORKING_RCD(fe).balancing_elements(8)  := xxcp_wks.Source_Balancing(8);
    xxcp_wks.WORKING_RCD(fe).balancing_elements(9)  := cTarget_Instance_id;
    xxcp_wks.WORKING_RCD(fe).balancing_elements(10) := xxcp_wks.working_rcd(fe).Entered_Currency;
    -- New Code for company balancing Segment (UPS 20/05/08)
    if xxcp_global.gBalancing_Segment11_Override = 'N' then -- 02.06.08
      xxcp_wks.WORKING_RCD(fe).balancing_elements(11) := xxcp_wks.WORKING_RCD(fe).I1009_Array(11);
    end if;
    xxcp_wks.WORKING_RCD(fe).balancing_elements(12) := cRounding_Group;

    -- 03.06.27
    xxcp_wks.WORKING_RCD(fe).I1009_Array(162) := cTarget_Assignment_id;
    xxcp_wks.WORKING_RCD(fe).I1009_Array(97)  := nvl(cTarget_COA_ID,0);
    -- Summarization
    xxcp_wks.WORKING_RCD(fe).I1009_Array(104) := xxcp_global.gCOMMON(1).current_creation_date;

    If cEntered_Amounts = 'N' then
      xxcp_wks.WORKING_RCD(fe).Entered_Amount  := '0';
      xxcp_wks.WORKING_RCD(fe).I1009_Array(21) := '0';
      xxcp_wks.WORKING_RCD(fe).I1009_Array(81) := '0';
    End If;
    
    xxcp_wks.WORKING_RCD(fe).Target_Suppression_Rule := xxcp_wks.WORKING_RCD(fe).I1009_Array(145);
    xxcp_wks.WORKING_RCD(fe).History_Suppression_Rule := xxcp_wks.WORKING_RCD(fe).I1009_Array(146);
    If cAccounted_Amounts = 'N' then
      xxcp_wks.WORKING_RCD(fe).Accounted_Amount := Null;
      xxcp_wks.WORKING_RCD(fe).I1009_Array(23)  := Null;
      xxcp_wks.WORKING_RCD(fe).I1009_Array(83)  := Null;
      xxcp_wks.WORKING_RCD(fe).I1009_Array(54)  := Null; -- Accounted Exchange Rate
      xxcp_wks.WORKING_RCD(fe).I1009_Array(56)  := Null; -- Target Currency
    Else
      -- Implied Accounted Exchange Rate (Accounted Amount/Entered Amount)   
      xxcp_wks.WORKING_RCD(fe).I1009_Array(148) := 1; 
      If nvl(to_number(xxcp_wks.WORKING_RCD(fe).Entered_Amount),0) != 0 And nvl(to_number(xxcp_wks.WORKING_RCD(fe).Accounted_Amount),0) != 0 Then
        xxcp_wks.WORKING_RCD(fe).I1009_Array(148) := to_number(xxcp_wks.WORKING_RCD(fe).Accounted_Amount)/to_number(xxcp_wks.WORKING_RCD(fe).Entered_Amount);
      End If;
    End If;    
  END Column_Rules;

  --
  -- ## ***********************************************************************************************************
  -- ##
  -- ##                                Get_Entity_Document
  -- ##
  -- ## ***********************************************************************************************************
  --
  PROCEDURE Get_Entity_Document(cTaxRegId             IN NUMBER,
                                cTarget_Type_id       IN NUMBER,
                                cModel_Name_id        IN NUMBER,
                                cED_Qualifier1        IN VARCHAR2,
                                cED_Qualifier2        IN VARCHAR2,
                                cED_Qualifier3        IN VARCHAR2,
                                cED_Qualifier4        IN VARCHAR2,
                                cEffective_Date       IN DATE,
                                cBatch_Source_Name    OUT NOCOPY VARCHAR2,
                                cCust_Trx_Type_id     OUT NOCOPY NUMBER,
                                cReplace_Trx_Date     OUT NOCOPY VARCHAR2,
                                cED_Trx_Type_Name     OUT NOCOPY VARCHAR2,
                                cInternalErrorCode    IN OUT NUMBER) IS

    CURSOR BC(pTaxRegId IN NUMBER, pTarget_Type_id IN NUMBER, pModel_Name_id IN NUMBER
            , pED_Qualifier1 IN VARCHAR2, pED_Qualifier2 IN VARCHAR2, pED_Qualifier3 IN VARCHAR2, pED_Qualifier4 IN VARCHAR2
            , pEffective_Date  IN DATE) IS
      select distinct e.trx_number_source  batch_source_name,
                      e.trx_number_type_id cust_trx_type_id,
                      e.replace_trx_date,
                      e.model_name_id,
                      e.TRX_NUMBER_TYPE ED_Trx_Type_Name
        from xxcp_entity_documents e,
             xxcp_tax_registrations r,
             xxcp_target_assignments tx
       WHERE e.reg_id = r.reg_id
         AND r.tax_registration_id  = pTaxRegId
         AND e.target_type_id       = pTarget_Type_id
         AND e.model_name_id        in (0, pModel_Name_id)
         AND e.source_id            = xxcp_global.gCommon(1).Source_id
         --
         and e.reg_id               = tx.reg_id
         and e.target_type_id       = tx.target_type_id
         and e.instance_id          = tx.target_instance_id
         AND nvl(tx.active,'Y')     = 'Y'
         --
         AND r.active               = 'Y'
         AND nvl(ed_qualifier1, '~null~') = nvl(pED_Qualifier1, '~null~')
         AND nvl(ed_qualifier2, '~null~') = nvl(pED_Qualifier2, '~null~')
         AND nvl(ed_qualifier3, '~null~') = nvl(pED_Qualifier3, '~null~')
         AND nvl(ed_qualifier4, '~null~') = nvl(pED_Qualifier4, '~null~')
         AND to_char(pEffective_Date,'DD-MON-YYYY') BETWEEN nvl(effective_from, '01-JAN-1980') AND nvl(effective_to, '31-DEC-2999')
         order by e.model_name_id desc;


  BEGIN

    cInternalErrorCode := 10877; -- Not Found

    FOR BCRec IN BC(cTaxRegId,cTarget_Type_id, cModel_Name_id, cED_Qualifier1, cED_Qualifier2,cED_Qualifier3,cED_Qualifier4,cEffective_Date) LOOP
      cBatch_Source_Name := BCRec.Batch_Source_Name;
      cCust_Trx_Type_id  := BCRec.Cust_Trx_Type_Id;
      cReplace_Trx_Date  := BCRec.Replace_Trx_Date;
      cED_Trx_Type_Name  := BCRec.ED_Trx_Type_Name;
      cInternalErrorCode := 0; -- Found
      Exit;
    END LOOP;


    IF cInternalErrorCode != 0 THEN
      xxcp_foundation.FndWriteError(cInternalErrorCode,
                                    'Tax Reg <' ||to_char(cTaxRegid) ||
                                    '> Model Name <'||xxcp_translations.Model_Name(cModel_Name_id) ||
                                    '> Target Type <' ||xxcp_translations.Target_Types(cTarget_Type_id) ||
                                    '> Effective Date <'||to_char(cEffective_Date,'DD-MON-YYYY')||
                                    '> Qualifier 1 <' || cED_Qualifier1 ||'> Qualifier 2 <' || cED_Qualifier2 ||
                                    '> Qualifier 3 <' || cED_Qualifier3 ||'> Qualifier 4 <' || cED_Qualifier4 || '> '
                                    );
    END IF;

    xxcp_trace.Transaction_Trace( cTrace_id => 10, cBatch_Source_Name => cBatch_Source_Name);

  END Get_Entity_Document;

  -- ## ***********************************************************************************************************
  -- ##
  -- ##                                Get_Tax_Reg_Information
  -- ##             Get the information from tax registrations for the Internal Paramters
  -- ## ***********************************************************************************************************
  PROCEDURE Get_Tax_Reg_Information(cTarget_Type_id           IN NUMBER,
                                    cOwner_Tax_Reg_id         IN NUMBER,
                                    cPartner_Tax_Reg_id       IN NUMBER,
                                    cEntity_Type              IN VARCHAR2,
                                    cTarget_Type_Match        IN VARCHAR2,
                                    -- Returns
                                    cTarget_Assignment_id OUT NOCOPY NUMBER,
                                    cTarget_Zero_Flag     OUT NOCOPY VARCHAR2,
                                    cReg_id               OUT NOCOPY NUMBER,
                                    cTax_Reg_id           OUT NOCOPY NUMBER,
                                    cBal_Segment          OUT NOCOPY NUMBER,
                                    cExchange_Rate_Type   OUT NOCOPY VARCHAR2,
                                    cCOA_ID               OUT NOCOPY NUMBER,
                                    cInstance_ID          OUT NOCOPY NUMBER,
                                    cSOB_ID               OUT NOCOPY NUMBER,
                                    cCurrency_Code        OUT NOCOPY VARCHAR2,
                                    cCommon_Exch_Curr     OUT NOCOPY VARCHAR2,
                                    cTrade_currency       OUT NOCOPY VARCHAR2,
                                    cInternalErrorCode        IN OUT NOCOPY NUMBER) IS



  Cursor TaxReg(pTax_Registration_id in Number
               ,pTarget_Type_id      in Number
               ,pTarget_Type_Match   in varchar2
               ) is
       Select  
             r.Tax_registration_id
            ,r.Legal_currency
            ,r.Reg_id
            ,r.Exchange_Rate_Type
            ,r.Trade_Currency
            ,r.Common_Exch_Curr
            ,r.Adjustment_Rate_Group
            -- Target Assignments
            ,t.Acc_segment_value
            ,t.Target_Type_id
            ,t.Target_Set_of_books_id
            ,t.Target_Org_id
            ,t.Balancing_Segment
            ,t.Target_assignment_id
            ,t.Zero_Flag target_zero_flag
            ,t.Target_ACCT_Currency  currency_code
            ,t.Target_COA_id
            ,t.Target_instance_id
            ,t.Target_Instance_id instance_id
            ,t.Target_ACCT_Currency sob_currency_code
            ,t.Master Master
            ,r.Short_code
            ,r.Tax_Reg_Group_id 
        from xxcp_tax_registrations  r,
             xxcp_target_assignments t,
             xxcp_sys_target_types   x
       where r.reg_id       = t.reg_id
         and r.tax_registration_id = pTax_Registration_id
         and r.Active       = 'Y'
         and t.Active       = 'Y'
         and t.target_type_id           = decode(pTarget_Type_Match,'N',pTarget_Type_id, t.target_type_id)
         and t.target_type_id           = x.target_type_id
         and x.target_id                = any(select distinct tt.target_id
                                                from xxcp_sys_target_tables tt
                                               where tt.source_id = xxcp_global.gCommon(1).Source_id
                                              Union
                                              select 5 target_id -- No Target
                                                from xxcp_sys_sources
                                               where interfaced = 'N'
                                                 and source_id != 14
                                                 and source_id =  xxcp_global.gCommon(1).Source_id
                                             )
      order by decode(t.target_type_id,pTarget_Type_id,0,99999) asc;

    vError_TaxReg                 XXCP_TAX_REGISTRATIONS.tax_registration_id%TYPE;
    vOwner_Set_of_books           XXCP_TARGET_ASSIGNMENTS.target_set_of_books_id%TYPE := 0;
    vOwner_Org_id                 XXCP_TARGET_ASSIGNMENTS.target_org_id%TYPE;
    vOwner_Segment_Val            XXCP_TARGET_ASSIGNMENTS.acc_segment_value%TYPE;
    vOwner_Currency               XXCP_TAX_REGISTRATIONS.legal_currency%TYPE;
    vOwner_Target_Assignment_id   XXCP_TARGET_ASSIGNMENTS.Target_assignment_id%TYPE := 0;

    vOwner_Exchange_Rate_Type     XXCP_TAX_REGISTRATIONS.exchange_rate_type%TYPE;
    vOwner_common_exch_curr       XXCP_TAX_REGISTRATIONS.legal_currency%TYPE;
    vOwner_Legal_Currency         XXCP_TAX_REGISTRATIONS.legal_currency%TYPE;
    vOwner_Reg_id                 XXCP_TAX_REGISTRATIONS.reg_id%TYPE := 0;
    vOwner_Trade_currency         XXCP_TAX_REGISTRATIONS.legal_currency%TYPE;
    vOwner_Target_coa_id          XXCP_TARGET_ASSIGNMENTS.target_coa_id%TYPE;
    vOwner_Target_instance_id     XXCP_TARGET_ASSIGNMENTS.target_instance_id%TYPE;
    vOwner_Bal_Segment            XXCP_TARGET_ASSIGNMENTS.Balancing_Segment%TYPE;
    vOwner_Adj_group              XXCP_TAX_REGISTRATIONS.adjustment_rate_group%TYPE;

    vOwner_Zero_Flag              XXCP_TARGET_ASSIGNMENTS.zero_flag%TYPE;

    vTarget_Org_id                XXCP_TARGET_ASSIGNMENTS.target_org_id%TYPE;

    vPartner_Set_of_books         XXCP_TARGET_ASSIGNMENTS.target_set_of_books_id%TYPE := 0;
    vPartner_Org_id               XXCP_TARGET_ASSIGNMENTS.target_org_id%TYPE := 0;
    vPartner_Segment_Val          XXCP_TARGET_ASSIGNMENTS.acc_segment_value%TYPE;
    vPartner_Currency             XXCP_TAX_REGISTRATIONS.legal_currency%TYPE;
    vPartner_Reg_id               XXCP_TAX_REGISTRATIONS.reg_id%TYPE := 0;
    vPartner_Zero_Flag            XXCP_TARGET_ASSIGNMENTS.zero_flag%TYPE;
    vPartner_Adj_group            XXCP_TAX_REGISTRATIONS.adjustment_rate_group%TYPE;
    vPartner_Target_Assignment_id XXCP_TARGET_ASSIGNMENTS.target_assignment_id%TYPE := 0;
    vPartner_Exchange_Rate_Type   XXCP_TAX_REGISTRATIONS.exchange_rate_type%TYPE;
    vPartner_Target_coa_id        XXCP_TARGET_ASSIGNMENTS.target_coa_id%TYPE := 0;
    vPartner_Target_instance_id   XXCP_TARGET_ASSIGNMENTS.target_instance_id%TYPE := 0;
    vPartner_common_exch_curr     XXCP_TAX_REGISTRATIONS.legal_currency%TYPE;
    vPartner_Bal_Segment          XXCP_TARGET_ASSIGNMENTS.Balancing_Segment%TYPE;
    vPartner_Legal_Currency       XXCP_TAX_REGISTRATIONS.legal_currency%TYPE;
    vPartner_Trade_currency       XXCP_TAX_REGISTRATIONS.legal_currency%TYPE;

    t PLS_INTEGER := xxcp_global.gTR_Cnt;

    vOwnerFound    boolean := False;
    vPartnerFound  boolean := False;

    vPrime_Match_Error varchar2(1) := 'Y';

  BEGIN

    cInternalErrorCode := nvl(cInternalErrorCode, 0);

    -- Owner Tax Reg
    cInternalErrorCode := 10782;
    
    vError_TaxReg      := cOwner_Tax_Reg_id;

    For OwnerRec in TaxReg(pTax_Registration_id => cOwner_Tax_Reg_id
                         , pTarget_Type_id      => cTarget_type_id
                         , pTarget_Type_Match   => cTarget_Type_Match ) 
      Loop
        
        vOwnerFound := True;

        vOwner_Set_of_books         := OwnerRec.Target_set_of_books_id;
        vOwner_Org_id               := OwnerRec.Target_org_id;
        vOwner_Legal_Currency       := OwnerRec.Legal_currency;
        vOwner_Reg_id               := OwnerRec.Reg_id;
        vOwner_Segment_Val          := OwnerRec.Acc_segment_value;
        vOwner_Currency             := OwnerRec.Currency_code;
        vOwner_Bal_Segment          := OwnerRec.Balancing_segment;
        vOwner_Exchange_Rate_Type   := OwnerRec.Exchange_Rate_Type;
        vOwner_Target_coa_id        := OwnerRec.Target_coa_id;
        vOwner_Target_instance_id   := OwnerRec.Target_instance_id;
        vOwner_common_exch_curr     := OwnerRec.Common_Exch_Curr;
        vOwner_Target_Assignment_id := OwnerRec.Target_Assignment_id;
        vOwner_Trade_currency       := OwnerRec.Trade_currency;
        vOwner_Adj_group            := OwnerRec.Adjustment_Rate_Group;
        vOwner_Zero_Flag            := OwnerRec.Target_zero_flag;

        If cEntity_Type = 'Owner' and OwnerRec.Target_Type_Id != cTarget_Type_id then
          vPrime_Match_Error := 'Y'; 
        Else
        cInternalErrorCode := 0;
           vPrime_Match_Error := 'N';
        vError_TaxReg      := NULL;
        End If;   

        xxcp_wks.I1009_ArrayTEMP(03) := vOwner_Legal_Currency;
        xxcp_wks.I1009_ArrayTEMP(05) := vOwner_Currency;
        xxcp_wks.I1009_ArrayTEMP(08) := vOwner_Segment_Val;
        xxcp_wks.I1009_ArrayTEMP(06) := vOwner_Org_id;
        xxcp_wks.I1009_ArrayTEMP(24) := vOwner_Set_of_books;

    IF xxcp_wks.I1009_ArrayTEMP(08) IS NULL THEN
      cInternalErrorCode := 10784;
      xxcp_foundation.FndWriteError(cInternalErrorCode,
                                    ' Target Type Id <' ||TO_CHAR(cTarget_Type_Id) || '>' ||
                                    ' Target Type <'|| xxcp_translations.Target_Types(cTarget_Type_id => cTarget_Type_Id)||
                                    '> Tax Registration <' ||TO_CHAR(vError_TaxReg) || '>');
    END IF;

        EXIT;

    END LOOP;


    IF cInternalErrorCode = 0 AND (cPartner_Tax_Reg_id = cOwner_Tax_Reg_id) THEN
      xxcp_wks.I1009_ArrayTEMP(04) := xxcp_wks.I1009_ArrayTEMP(03);
      xxcp_wks.I1009_ArrayTEMP(07) := xxcp_wks.I1009_ArrayTEMP(05);
      xxcp_wks.I1009_ArrayTEMP(09) := xxcp_wks.I1009_ArrayTEMP(08);
      xxcp_wks.I1009_ArrayTEMP(10) := xxcp_wks.I1009_ArrayTEMP(06);
      xxcp_wks.I1009_ArrayTEMP(25) := xxcp_wks.I1009_ArrayTEMP(24);
      xxcp_wks.I1009_ArrayTEMP(60) := xxcp_wks.I1009_ArrayTEMP(61);
    END IF;
    
    --
    -- Partner
    --
    IF cInternalErrorCode = 0 Then
      -- AND (cPartner_Tax_Reg_id <> cOwner_Tax_Reg_id) THEN

      cInternalErrorCode := 10783;
      vError_TaxReg      := cPartner_Tax_Reg_id;

      For PartnerRec in TaxReg(pTax_Registration_id => cPartner_Tax_Reg_id
                             , pTarget_Type_id      => cTarget_type_id
                             , pTarget_Type_Match   => cTarget_Type_Match
                             ) 
      Loop

          vPartnerFound := True;

          vPartner_Set_of_books         := PartnerRec.Target_set_of_books_id;
          vPartner_Org_id               := PartnerRec.Target_org_id;
          vPartner_Legal_Currency       := PartnerRec.Legal_currency;
          vPartner_Reg_id               := PartnerRec.Reg_id;
          vPartner_Segment_Val          := PartnerRec.Acc_segment_value;
          vPartner_Currency             := PartnerRec.Currency_code;
          vPartner_Bal_Segment          := PartnerRec.balancing_segment;
          vPartner_Exchange_Rate_Type   := PartnerRec.Exchange_Rate_Type;
          vPartner_Target_coa_id        := PartnerRec.Target_coa_id;
          
          vPartner_Target_instance_id   := PartnerRec.Target_instance_id;
          vPartner_common_exch_curr     := PartnerRec.Common_Exch_Curr;
          vPartner_Target_Assignment_id := PartnerRec.Target_Assignment_id;
          vPartner_Trade_currency       := PartnerRec.Trade_currency;
          vPartner_Adj_group            := PartnerRec.Adjustment_Rate_Group;
          vPartner_Zero_Flag            := PartnerRec.Target_zero_flag;

          If cEntity_Type = 'Partner' and PartnerRec.Target_Type_Id != cTarget_Type_id then
            vPrime_Match_Error := 'Y'; 
          Else
             cInternalErrorCode := 0;
             vPrime_Match_Error := 'N';
          vError_TaxReg      := NULL;
          End If;   

          xxcp_wks.I1009_ArrayTEMP(04) := vPartner_Legal_Currency;
          xxcp_wks.I1009_ArrayTEMP(07) := vPartner_Currency;
          xxcp_wks.I1009_ArrayTEMP(09) := vPartner_Segment_Val;
          xxcp_wks.I1009_ArrayTEMP(10) := vPartner_Org_id;
          xxcp_wks.I1009_ArrayTEMP(25) := vPartner_Set_of_books;

      IF xxcp_wks.I1009_ArrayTEMP(09) IS NULL AND cInternalErrorCode = 0 THEN
        cInternalErrorCode := 10785;
        xxcp_foundation.FndWriteError(cInternalErrorCode,
                                      '{' || vPartner_Segment_Val ||
                                      '} Rule Id <' ||TO_CHAR(xxcp_global.gCommon(1).current_rule_id) ||
                                      '> Target Type Id <' || TO_CHAR(cTarget_Type_Id) ||
                                      '> Tax Registration <' || TO_CHAR(vError_TaxReg) || '>');
      END IF;

          EXIT;
      END LOOP;

      
    ELSE
          
          vPartner_Set_of_books         := vOwner_Set_of_books;
          vPartner_Org_id               := vOwner_Org_id  ;
          vPartner_Legal_Currency       := vOwner_Legal_Currency;
          vPartner_Reg_id               := vOwner_Reg_id;
          vPartner_Segment_Val          := vOwner_Segment_Val;
          vPartner_Currency             := vOwner_Currency;
          vPartner_Bal_Segment          := vOwner_Bal_Segment;
          vPartner_Exchange_Rate_Type   := vOwner_Exchange_Rate_Type;
          vPartner_Target_coa_id        := vOwner_Target_coa_id;
          
          vPartner_Target_instance_id   := vOwner_Target_instance_id;
          vPartner_common_exch_curr     := vOwner_common_exch_curr;
          vPartner_Target_Assignment_id := vOwner_Target_Assignment_id;
          vPartner_Trade_currency       := vOwner_Trade_currency;
          vPartner_Adj_group            := vOwner_Adj_group;
          vPartner_Zero_Flag            := vOwner_Zero_Flag; 

          xxcp_wks.I1009_ArrayTEMP(04) := vOwner_Legal_Currency;
          xxcp_wks.I1009_ArrayTEMP(07) := vOwner_Currency;
          xxcp_wks.I1009_ArrayTEMP(09) := vOwner_Segment_Val;
          xxcp_wks.I1009_ArrayTEMP(10) := vOwner_Org_id;
          xxcp_wks.I1009_ArrayTEMP(25) := vOwner_Set_of_books;

    END IF;


    If cTarget_Type_Match = 'Y' and cInternalErrorCode > 0 then      
      If vPrime_Match_Error = 'Y' then      
        cInternalErrorCode := gDo_Not_Do_Rule;
      End If;
    END IF;

    IF cInternalErrorCode IN (10782, 10783) THEN
      xxcp_foundation.FndWriteError(cInternalErrorCode,
                                    'Rule Id <' || TO_CHAR(xxcp_global.gCommon(1).current_rule_id) ||
                                    '> Target Type Id <' ||TO_CHAR(cTarget_Type_Id) ||
                                    '> Tax Registration <' ||TO_CHAR(vError_TaxReg) || '>');
    END IF;

    IF cInternalErrorCode = 0 THEN
      -- Return Target only information.
      IF cEntity_Type = 'Owner' OR (cPartner_Tax_Reg_id = cOwner_Tax_Reg_id) THEN
        cTarget_Assignment_id := vOwner_Target_Assignment_id;
        cReg_id               := vOwner_Reg_id;
        cTax_Reg_id           := cOwner_Tax_Reg_id;
        cBal_Segment          := vOwner_Bal_Segment;
        cExchange_Rate_Type   := vOwner_Exchange_Rate_Type;
        cCurrency_Code        := vOwner_Currency;
        cCOA_ID               := vOwner_Target_coa_id;
        cSOB_ID               := vOwner_Set_of_books;
        cInstance_ID          := vOwner_Target_instance_id;
        cCommon_Exch_Curr     := vOwner_common_exch_curr;
        cTrade_currency       := vOwner_Trade_currency;
        vTarget_Org_id        := vOwner_Org_id;
        cTarget_Zero_Flag     := vOwner_Zero_Flag;

        xxcp_wks.I1009_ArrayTEMP(32) := vTarget_Org_id;
        xxcp_wks.I1009_ArrayTEMP(33) := cSOB_ID;
        xxcp_wks.I1009_ArrayTEMP(34) := cInstance_ID;
        xxcp_wks.I1009_ArrayTEMP(99) := xxcp_wks.I1009_ArrayTEMP(100);
      ELSE
        cTarget_Assignment_id := vPartner_Target_Assignment_id;
        cReg_id               := vPartner_Reg_id;
        cTax_Reg_id           := cPartner_Tax_Reg_id;
        cBal_Segment          := vPartner_Bal_Segment;
        cExchange_Rate_Type   := vPartner_Exchange_Rate_Type;
        cCurrency_Code        := vPartner_Currency;
        cCOA_ID               := vPartner_Target_coa_id;
        cSOB_ID               := vPartner_Set_of_books;
        cInstance_ID          := vPartner_Target_instance_id;
        cCommon_Exch_Curr     := vPartner_common_exch_curr;
        cTrade_currency       := vPartner_Trade_currency;
        vTarget_Org_id        := vPartner_Org_id;
        cTarget_Zero_Flag     := vPartner_Zero_Flag;

        xxcp_wks.I1009_ArrayTEMP(32) := vTarget_Org_id;
        xxcp_wks.I1009_ArrayTEMP(33) := cSOB_ID;
        xxcp_wks.I1009_ArrayTEMP(34) := cInstance_ID;
        xxcp_wks.I1009_ArrayTEMP(99) := xxcp_wks.I1009_ArrayTEMP(101);
      END IF;
    END IF;

    If vPrime_Match_Error = 'Y' and cInternalErrorCode = 0 then
       cInternalErrorCode := gDo_Not_Do_Rule;
    End If;

  END Get_Tax_Reg_Information;

  --  ## ***********************************************************************************************************
  --  ##
  --  ##                                    Get_Attribute_Name
  --  ##     Returns the Text Name for the Dynamic Attribute Number. This is used for debug error reporting
  --  ##
  --  ## ***********************************************************************************************************
  FUNCTION Get_Attribute_Name(cColumn_id IN NUMBER) RETURN VARCHAR2 IS

    CURSOR AttName(pAtt_id IN NUMBER) IS
      SELECT Attribute_name
        FROM xxcp_dynamic_attributes_v
       WHERE ID = pAtt_id;

    vResult XXCP_DYNAMIC_ATTRIBUTES.column_name%TYPE;

  BEGIN

    FOR AttRec IN AttName(cColumn_id) LOOP
      vResult := AttRec.Attribute_Name;
    END LOOP;

    RETURN(vResult);
  END Get_Attribute_Name;

  --
  --## ***********************************************************************************************************
  --##
  --##                            Populate_Replan
  --##
  --## ***********************************************************************************************************
  --
  Procedure Populate_Replan (cTransaction_Date in Date,
                             cOwner            in Varchar2,
                             cTerritory        in Varchar2) is
   -- cstcat
   /*Cursor cstcat(pCost_Category_id in number,
                 pCategory_Data_Source in varchar2) is
    select  w.description cost_category,
            w.data_source
     from xxcp_instance_cpa_categories_v  w
    where w.cost_category_id = pCost_Category_id
    and   w.data_source      = pCategory_Data_Source;
    */

    -- Growth and Uplift Rates
/*    Cursor Rates(pTransaction_Date in date, pOwner in varchar2,
                 pTerritory in varchar2, pCost_Category_id in number, pData_Source in varchar2) is
     select c.owner, c.territory, c.cost_category,
            c.uplift_rate*0.01 uplift_rate, c.growth_rate*0.01 growth_rate, c.periods_required,
            c.attribute1 associated_data1, c.attribute2 associated_data2,
            c.ENTRY_TYPE
       from XXCP_COST_PLUS_RATES c
           ,XXCP_TABLE_SET_CTL t
     where c.period_set_name_id      = xxcp_wks.gActual_Costs_Rec(1).period_set_name_id
       and t.set_id                  = c.rate_set_id
       and t.set_base_table          = 'CP_COST_PLUS_RATES'
       and t.set_type                = 'COST PLUS RATES'
       and ((nvl(c.owner,'*')        = '*' or c.owner         = pOwner)
       and (nvl(c.territory,'*')     = '*' or c.territory     = pTerritory)
       and (c.Cost_Category_id = 0 or (c.cost_category_id = pCost_Category_id and c.data_source = pData_source)))  -- 02.05.20
       and pTransaction_Date between nvl(c.effective_from_date,'01-JAN-1980') and nvl(c.effective_to_date,'31-DEC-2999')
     Order by to_number(t.attribute1) desc, decode(c.owner ,'*',0,10) desc, -- 02.05.14
              decode(c.territory,'*',0,10) desc, decode(c.cost_category_id,0,0,10) desc,
              c.rates_id desc;
*/
    vInternalErrorCode number := 0;
  Begin
    --
    -- Get Cost Category
    --
    vInternalErrorCode := xxcp_te_base.Get_Cost_Category(xxcp_wks.gActual_Costs_Rec(1).vt_replan_company,    --xxcp_wks.gActual_Costs_Rec(1).Attribute2,
                                                         xxcp_wks.gActual_Costs_Rec(1).vt_replan_department, --xxcp_wks.gActual_Costs_Rec(1).Attribute3,
                                                         xxcp_wks.gActual_Costs_Rec(1).vt_replan_account,    --xxcp_wks.gActual_Costs_Rec(1).Attribute4,
                                                         cTransaction_date,
                                                         xxcp_wks.gActual_Costs_Rec(1).tu_Cost_Category_id,
                                                         xxcp_wks.gActual_Costs_Rec(1).tu_Category_Data_Source,
                                                         xxcp_wks.I1009_Array(115),
                                                         xxcp_wks.gActual_Costs_Rec(1).cost_plus_set_id);

    --
    -- Get Payer Details
    --
    vInternalErrorCode := xxcp_te_base.Get_Payer_Details ( cPayee            => cOwner,
                                                           cTerritory        => cTerritory,
                                                           cCost_Category_id => xxcp_wks.gActual_Costs_Rec(1).tu_Cost_Category_id, --vCost_Category,
                                                           cData_Source      => xxcp_wks.gActual_Costs_Rec(1).tu_Category_Data_Source,
                                                           cTransaction_Date => cTransaction_date,
                                                           cPayer1           => xxcp_wks.I1009_Array(123),
                                                           cPayer1_Percent   => xxcp_wks.I1009_Array(124),
                                                           cPayer2           => xxcp_wks.I1009_Array(125),
                                                           cPayer2_Percent   => xxcp_wks.I1009_Array(126),
                                                           cInter_Payer      => xxcp_wks.I1009_Array(127),
                                                           cAccount_Type     => xxcp_wks.I1009_Array(128));

    -- Error Codes
    If vInternalErrorCode = 7014 then
         xxcp_foundation.FndWriteError(vInternalErrorCode,'Owner <'||cOwner||'> Territory <'||cTerritory||'> Category <'||xxcp_wks.I1009_Array(115)||'> Date <'||to_char(cTransaction_Date)||'>');
    End if;

  End Populate_Replan;

  --
  --## ***********************************************************************************************************
  --##
  --##                            Forecast_Rates
  --##
  --## ***********************************************************************************************************
  --
  Function Forecast_Rates(cTransaction_Date in date, cOwner_Short_Name in varchar2, cOwner_Territory in varchar2) return Number is

   -- cstcat
/*   Cursor cstcat(pCost_Category_id in number,
                 pData_Source      in varchar2) is
    select  w.description cost_category,
            w.data_source
     from xxcp_instance_cpa_categories_v  w
    where w.cost_category_id = pCost_Category_id
      and w.data_source      = pData_source;*/

  -- Sum Period Net
  Cursor ACS(pSource_assignment_id in number,
             pPeriod_set_name_id   in number,
             pCurrency_Code        in varchar2,
             pCompany              in varchar2,
             pDepartment           in varchar2,
             pAccount              in varchar2,
             pOffSet_Period_id     in number,
             pCurrent_Period_id    in number,
             pCost_Plus_Set_Id     in number
             ) is
   select min(d.collection_period_id) min_period_id,
          max(d.collection_period_id) max_period_id,
          sum(nvl(d.period_net_dr,0)-(nvl(d.period_net_cr,0))) Total_Cost,
          Count(*) Cnt
   from xxcp_actual_costs d
   where d.source_assignment_id = pSource_assignment_id
     and d.period_set_name_id   = pPeriod_set_name_id
     and d.company              = pCompany
     and d.department           = pDepartment
     and d.account              = pAccount
     and d.currency_code        = pCurrency_Code
     and d.collection_period_id between pOffSet_Period_id and pCurrent_Period_id
     and d.cost_plus_set_id     = pCost_Plus_Set_id;

    -- Cost Plus Rates
    Cursor Rates(pPeriod_Set_Name_id in number,  pTransaction_Date in date, pOwner in varchar2, pTerritory in varchar2, pCost_Category_id in number,pData_Source in varchar2) is
     select c.owner, c.territory, c.cost_category, c.uplift_rate*0.01 uplift_rate, c.growth_rate*0.01 growth_rate , nvl(c.periods_required,0) periods_required,
            c.attribute1 associated_data1, c.attribute2 associated_data2
       from XXCP_COST_PLUS_RATES  c
         ,  XXCP_TABLE_SET_CTL    t
     where c.period_set_name_id      = pPeriod_Set_Name_id
       and t.set_id                  = c.rate_set_id
       and t.set_base_table          = 'CP_COST_PLUS_RATES'
       and t.set_type                = 'COST PLUS RATES'
       and ((nvl(c.Owner,'*')        = '*' or c.Owner         = pOwner)
       and (nvl(c.territory,'*')     = '*' or c.territory     = pTerritory)
       and (c.Cost_Category_id = 0 or (c.cost_category_id = pCost_Category_id and c.data_source = pData_source)))  -- 02.05.12
       and pTransaction_Date between nvl(c.effective_from_date,'01-JAN-1980') and nvl(c.effective_to_date,'31-DEC-2999')
--     order by to_number(t.attribute1), decode(c.Owner,'*',0,10) desc,
     order by to_number(t.attribute1) desc, decode(c.Owner,'*',0,10) desc,  -- 02.05.14
                 decode(c.territory,'*',0,10) desc,  decode(c.cost_category_id,0,0,10) desc, c.rates_id desc; --c.effective_from_date;

   vMinus_Period_id    number := 0;
   vCurrent_Period_id  number := 0;

   vTotal_Cost         xxcp_forecast_history.total_cost%type;
   vGrowth_Rate        xxcp_forecast_history.growth_rate%type;
   vUplift_Rate        xxcp_forecast_history.uplift_rate%type;
   vUplift_Rate_calc   xxcp_forecast_history.uplift_rate%type;
   vGrowth_Rate_calc   xxcp_forecast_history.growth_rate%type;
   vAvg_Period_Cost    xxcp_forecast_history.avg_period_cost%type;
   vUplifted_Amount    xxcp_forecast_history.uplifted_amount%type;
   vConsidered_Periods xxcp_forecast_history.considered_periods%type;
   vCurrency_Code      xxcp_forecast_history.currency_code%type;

   vCurr_Cost_Category_id     xxcp_cost_plus_categories.cost_category_id%type;
   vCurr_Category_Data_Source varchar2(10);
   vCurr_Cost_Category        varchar2(100);
   vCurr_Payer1               varchar2(15);
   vCurr_Payer1_percent       number;
   vCurr_Payer2               varchar2(15);
   vCurr_Payer2_percent       number;
   vCurr_Inter_payer          varchar2(15);
   vCurr_Account_Type         varchar2(100);
   vInternalErrorCode         xxcp_errors.internal_error_code%type := 0;
   vError_Message             xxcp_errors.long_message%type;

  Begin

   vCurrent_Period_id := xxcp_wks.gActual_Costs_Rec(1).collection_period_id;

    -- Get Cost Category
    vInternalErrorCode:= xxcp_te_base.Get_Cost_Category (cCompany              => xxcp_wks.gActual_Costs_Rec(1).Company,
                                                         cDepartment           => xxcp_wks.gActual_Costs_Rec(1).Department,
                                                         cAccount              => xxcp_wks.gActual_Costs_Rec(1).Account,
                                                         cPeriod_End_Date      => cTransaction_date,
                                                         cCost_Category_Id     => vCurr_Cost_Category_id,     -- out
                                                         cCategory_Data_Source => vCurr_Category_Data_Source, -- out
                                                         cCost_Category        => vCurr_Cost_Category,        -- out
                                                         cCost_Plus_Set_Id     => xxcp_wks.gActual_Costs_Rec(1).cost_plus_set_id);

     If vInternalErrorCode = 0 then
    -- Get Payer Details
      vInternalErrorCode:= xxcp_te_base.Get_Payer_Details (
                                                         cPayee            => xxcp_wks.gActual_Costs_Rec(1).Company,
                                                         cTerritory        => cOwner_Territory,
                                                         cCost_Category_id => vCurr_Cost_Category_id, --vCurr_Cost_Category,
                                                         cData_Source      => vCurr_Category_Data_Source,
                                                         cTransaction_Date => cTransaction_date,
                                                         cPayer1           => vCurr_Payer1,        --out
                                                         cPayer1_Percent   => vCurr_Payer1_percent,--out
                                                         cPayer2           => vCurr_Payer2,        --out
                                                         cPayer2_Percent   => vCurr_Payer2_percent,--out
                                                         cInter_Payer      => vCurr_Inter_payer,   --out
                                                         cAccount_Type     => vCurr_Account_Type); --out
      End If;

      -- Clear vars
      vTotal_Cost         := 0;
      vGrowth_Rate        := 0;
      vUplift_Rate        := 0;
      vAvg_Period_Cost    := 0;
      vConsidered_Periods := 0;

      If vInternalErrorCode = 0 then
        vInternalErrorCode  := 7805; -- No Rate found

      vCurrency_Code    := xxcp_wks.gActual_Costs_Rec(1).Currency_Code;
      xxcp_wks.I1009_Array(115) := vCurr_Cost_Category;

      -- Search
      For ratesRec in Rates(xxcp_wks.gActual_Costs_Rec(1).period_set_name_id,
                            cTransaction_Date,
                            cOwner_Short_Name,
                            cOwner_Territory,
                            vCurr_Cost_Category_id,     -- 02.05.20
                            vCurr_Category_Data_Source) -- 02.05.12
      Loop

         -- Get offset period id
         If ratesRec.Periods_Required > 1 then
           vMinus_Period_id :=
            xxcp_reporting.Get_Period_Offset(xxcp_wks.gActual_Costs_Rec(1).collection_period_id,
                                             xxcp_wks.gActual_Costs_Rec(1).period_set_name_id,
                                              ((ratesRec.Periods_Required-1)*-1),
                                             xxcp_wks.gActual_Costs_Rec(1).instance_id);

         Else
           vMinus_Period_id := xxcp_wks.gActual_Costs_Rec(1).collection_period_id;
         End If;

         vTotal_Cost := 0;
         vInternalErrorCode := 7803; -- Not enough values in xxcp_actual_costs table
         vUplift_Rate := ratesRec.uplift_rate;
         vGrowth_Rate := ratesRec.growth_rate;

         vConsidered_Periods := 0;
         xxcp_wks.I1009_Array(122) := ratesRec.Periods_Required;

         For AcsRec in ACS(pSource_assignment_id => xxcp_global.gCommon(1).current_assignment_id,
                           pPeriod_set_name_id   => xxcp_wks.gActual_Costs_Rec(1).period_set_name_id,
                           pCurrency_Code        => xxcp_wks.gActual_Costs_Rec(1).Currency_Code,
                           pCompany              => xxcp_wks.gActual_Costs_Rec(1).Company,
                           pDepartment           => xxcp_wks.gActual_Costs_Rec(1).Department,
                           pAccount              => xxcp_wks.gActual_Costs_Rec(1).Account,
                           pOffSet_Period_id     => vMinus_Period_id,
                           pCurrent_Period_id    => vCurrent_Period_id,
                           pCost_Plus_Set_Id     => xxcp_wks.gActual_Costs_Rec(1).cost_plus_set_id
                          ) Loop

            If AcsRec.cnt > 0 then -- = ratesRec.Periods_Required then
              vConsidered_Periods := AcsRec.cnt;

              vTotal_Cost := AcsRec.Total_Cost;

              If (nvl(vTotal_Cost,0) <> 0 and nvl(vConsidered_Periods,0) <> 0) then
                vAvg_Period_Cost := vTotal_Cost / vConsidered_Periods;
              End If;

              vGrowth_Rate_calc := 1+ratesRec.Growth_Rate;
              vUplift_Rate_calc := 1+ratesRec.Uplift_Rate;

              vUplifted_Amount := ((vAvg_Period_Cost * vGrowth_Rate_calc) * vUplift_Rate_calc);

              vInternalErrorCode := 0;

              xxcp_wks.I1009_Array(106) := ratesRec.Uplift_Rate;
              xxcp_wks.I1009_Array(107) := ratesRec.Growth_Rate ;
              xxcp_wks.I1009_Array(108) := vAvg_Period_Cost ;
              xxcp_wks.I1009_Array(109) := vTotal_Cost ;
              xxcp_wks.I1009_Array(110) := vConsidered_Periods ;
              xxcp_wks.I1009_Array(111) := vCurrency_Code;
              xxcp_wks.I1009_Array(114) := vUplifted_Amount;
              xxcp_wks.I1009_Array(112) := vCurr_Cost_Category_id;
              xxcp_wks.I1009_Array(119) := ratesRec.Associated_Data1;
              xxcp_wks.I1009_Array(120) := ratesRec.Associated_Data2;
              -- Payer Details
              xxcp_wks.I1009_Array(123) := vCurr_Payer1;
              xxcp_wks.I1009_Array(124) := vCurr_Payer1_Percent;
              xxcp_wks.I1009_Array(125) := vCurr_Payer2;
              xxcp_wks.I1009_Array(126) := vCurr_Payer2_Percent;
              xxcp_wks.I1009_Array(127) := vCurr_Inter_payer;
              xxcp_wks.I1009_Array(128) := vCurr_Account_Type;

            Else
               vInternalErrorCode := 7804;

               vError_Message := 'Parameters:'||chr(10)||
                ' Source Assignment ID <'||xxcp_global.gCommon(1).current_assignment_id||'>'||chr(10)||
                ' Period Set id <'||to_char(xxcp_wks.gActual_Costs_Rec(1).period_set_name_id)||'>'||chr(10)||
                ' Currency <'||vCurrency_Code||'>'||chr(10)||
                ' Company <'||xxcp_wks.gActual_Costs_Rec(1).Company||'>'||chr(10)||
                ' Department <'||xxcp_wks.gActual_Costs_Rec(1).Department ||'>'||chr(10)||
                ' Account <'||xxcp_wks.gActual_Costs_Rec(1).Account||'>'||chr(10)||
                ' Period from <'||to_char(vMinus_Period_id)||'>'||
                ' Period to <'||to_char(xxcp_wks.gActual_Costs_Rec(1).collection_period_id)||'>'||chr(10)||
                ' Cost Plus Set Id <'||to_char(xxcp_wks.gActual_Costs_Rec(1).cost_plus_set_id)||'>';

               xxcp_foundation.FndWriteError(vInternalErrorCode,
                'Period Required <'||to_char(ratesRec.Periods_Required)||'> Periods Found <'||to_char(AcsRec.cnt)||'>', vError_Message);
            End If;

          End Loop;
      EXIT;          -- 02.06.03
    End Loop;

      End If;
    -- Error Codes
    If vInternalErrorCode = 7805 then
      xxcp_foundation.FndWriteError(vInternalErrorCode,'Owner <'||cOwner_Short_Name||'> Territory <'||cOwner_Territory||'> Category <'||vCurr_Cost_Category||'> Category Data Source <'||vCurr_Category_Data_Source||'> Date <'||to_char(cTransaction_Date)||'>');
    ElsIf vInternalErrorCode = 7803 then
      xxcp_foundation.FndWriteError(vInternalErrorCode,'Growth Rate <'||to_char(vGrowth_Rate)||
          '> vUplift_Rate <'||to_char(vUplift_Rate)||'> Period from <'||to_char(vMinus_Period_id)||'>'||' Period to <'||to_char(xxcp_wks.gActual_Costs_Rec(1).collection_period_id)||'>');
    End If;

    Return (vInternalErrorCode);

  End Forecast_Rates;

  --
  --## ***********************************************************************************************************
  --##
  --##                            True-Up Rates
  --##
  --## ***********************************************************************************************************
  --
  Function True_Up_Rates(cTransaction_Date   in date,
                         cOwner_Short_Name   in varchar2,
                         cOwner_Territory    in varchar2,
                         cOwner_Tax_Reg_Id   in number,
                         cPartner_Tax_Reg_id in number) return Number is

  --  Period Net
  Cursor ACS(pSource_assignment_id in number,
             pPeriod_set_name_id   in number,
             pCurrency_Code        in varchar2,
             pCompany              in varchar2,
             pDepartment           in varchar2,
             pAccount              in varchar2,
             pPeriod_from_id       in number,
             pPeriod_to_id         in number,
             pCost_Plus_Set_Id     in number) is
   select d.period_end_date,
          d.collection_period_id,
          (nvl(d.period_net_dr,0)-(nvl(d.period_net_cr,0))) Actual_Cost
   from xxcp_actual_costs d
   where d.source_assignment_id = pSource_assignment_id
     and d.period_set_name_id   = pPeriod_set_name_id
     and d.company              = pCompany
     and d.department           = pDepartment
     and d.account              = pAccount
     and d.currency_code        = pCurrency_Code
     and d.collection_period_id between pPeriod_from_id and pPeriod_to_id
     and d.cost_plus_set_id = pCost_Plus_Set_id
     order by d.collection_period_id desc;

    -- Growth and Uplift Rates
    Cursor Rates(pPeriod_Set_Name_id in number, pTransaction_Date in date, pOwner in varchar2,
                 pTerritory in varchar2, pCost_Category_id in number, pData_Source in varchar2) is
     select c.owner, c.territory, c.cost_category,
            c.uplift_rate*0.01 uplift_rate, c.growth_rate*0.01 growth_rate, c.periods_required,
            c.attribute1 associated_data1, c.attribute2 associated_data2,
            c.ENTRY_TYPE
       from XXCP_COST_PLUS_RATES  c
           ,XXCP_TABLE_SET_CTL t
     where c.period_set_name_id      = pPeriod_Set_Name_id
       and t.set_id                  = c.rate_set_id
       and t.set_base_table          = 'CP_COST_PLUS_RATES'
       and t.set_type                = 'COST PLUS RATES'
       and ((nvl(c.owner,'*')        = '*' or c.owner         = pOwner)
       and (nvl(c.territory,'*')     = '*' or c.territory     = pTerritory)
       and (c.Cost_Category_id = 0 or (c.cost_category_id = pCost_Category_id and c.data_source = pData_source)))  -- 02.05.20
       and pTransaction_Date between nvl(c.effective_from_date,'01-JAN-1980') and nvl(c.effective_to_date,'31-DEC-2999')
     Order by to_number(t.attribute1) desc, decode(c.owner ,'*',0,10) desc, -- 02.05.14
              decode(c.territory,'*',0,10) desc, decode(c.cost_category_id,0,0,10) desc,
              c.rates_id desc; --c.effective_from_date;

   -- Forecast History
   Cursor fch(pPeriod_Set_Name_id in number,
              pPeriod_id          in number,
              pCompany            in varchar2,
              pDepartment         in varchar2,
              pAccount            in varchar2,
              pCurrency_Code      in varchar2,
              pTrading_Set_id     in varchar2,
              pCost_Plus_Set_id   in number) is
        select total_uplifted_amount/trading_set_count Uplifted_Amount,
               Cost_Category_id,
               Category_Data_Source,
               Payer1,
               Payer1_Percent,
               Payer2,
               Payer2_Percent,
               Inter_Payer,
               Account_Type,
               Growth_Rate,
               Uplift_Rate,
               b.trading_set_id
        from (select sum(h.Uplifted_Amount) over() total_uplifted_amount,
                     count(h.trading_Set_id) over() trading_set_count,
                     h.trading_set_id
            from xxcp_forecast_history h
            where h.period_id          = pPeriod_id
              and h.company            = pCompany
              and h.department         = pDepartment
              and h.account            = pAccount
              and h.currency_code      = pCurrency_Code
              and h.period_set_name_id = pPeriod_Set_Name_id
              and h.cost_plus_set_id   = pCost_Plus_Set_id
              and nvl(h.active,'Y')    = 'Y') a,
             (select nvl(h.cost_category_id,0)  Cost_Category_id,
                  nvl(data_source,0)         Category_Data_Source,
                  nvl(payer1,0)              Payer1,
                  nvl(payer1_percent,0)      Payer1_Percent,
                  nvl(payer2,0)              Payer2,
                  nvl(payer2_percent,0)      Payer2_Percent,
                  nvl(inter_payer,0)         Inter_Payer,
                  nvl(account_type,'~NULL~') Account_Type,
                  nvl(growth_rate,0)         Growth_Rate,
                  nvl(uplift_rate,0)         Uplift_Rate,
                  trading_set_id
            from xxcp_forecast_history h
            where h.period_id          = pPeriod_id
              and h.company            = pCompany
              and h.department         = pDepartment
              and h.account            = pAccount
              and h.currency_code      = pCurrency_Code
              and h.period_set_name_id = pPeriod_Set_Name_id
              and h.cost_plus_set_id   = pCost_Plus_Set_id
              and nvl(h.active,'Y')    = 'Y'
              and h.trading_set_id     = pTrading_Set_id) b
        where a.trading_set_id = b.trading_set_id (+)
        order by b.trading_set_id;


   -- True Up History
   Cursor tuh(pPeriod_Set_Name_id in number,
              pPeriod_id          in number,
              pCompany            in varchar2,
              pDepartment         in varchar2,
              pAccount            in varchar2,
              pCurrency_Code      in varchar2,
              pTrading_Set_id     in number,
              pCost_Plus_Set_id   in number) is
        select total_true_amount/trading_set_count True_Up_Amount,
               Cost_Category_id,
               Category_Data_Source,
               Payer1,
               Payer1_Percent,
               Payer2,
               Payer2_Percent,
               Inter_Payer,
               Account_Type,
               Growth_Rate,
               Uplift_Rate,
               b.trading_set_id
        from (select sum(h.True_up_Amount) over() total_true_amount,
                     count(h.trading_Set_id) over() trading_set_count,
                     h.trading_set_id
            from xxcp_true_up_history h
            where h.period_id          = pPeriod_id
              and h.company            = pCompany
              and h.department         = pDepartment
              and h.account            = pAccount
              and h.currency_code      = pCurrency_Code
              and h.period_set_name_id = pPeriod_Set_Name_id
              and h.cost_plus_set_id   = pCost_Plus_Set_id
              and nvl(h.active,'Y')    = 'Y') a,
             (select nvl(h.cost_category_id,0)  Cost_Category_id,
                  nvl(data_source,0)         Category_Data_Source,
                  nvl(payer1,0)              Payer1,
                  nvl(payer1_percent,0)      Payer1_Percent,
                  nvl(payer2,0)              Payer2,
                  nvl(payer2_percent,0)      Payer2_Percent,
                  nvl(inter_payer,0)         Inter_Payer,
                  nvl(account_type,'~NULL~') Account_Type,
                  nvl(growth_rate,0)         Growth_Rate,
                  nvl(uplift_rate,0)         Uplift_Rate,
                  trading_set_id
            from xxcp_true_up_history h
            where h.period_id          = pPeriod_id
              and h.company            = pCompany
              and h.department         = pDepartment
              and h.account            = pAccount
              and h.currency_code      = pCurrency_Code
              and h.period_set_name_id = pPeriod_Set_Name_id
              and h.cost_plus_set_id   = pCost_Plus_Set_id
              and nvl(h.active,'Y')    = 'Y'
              and h.trading_set_id     = pTrading_Set_id) b
        where a.trading_set_id = b.trading_set_id (+)
        order by b.trading_set_id;

    Cursor cTargetType(pTarget_Type in varchar2) is
     Select Target_Type_id
       from xxcp_sys_target_types t
     where Target_Type = pTarget_Type;


    Cursor FCPM(pPeriod_Set_Name_id in number,
                pPeriod_id          in number,
                pInstance_id        in number) is
      select w.Start_Date, w.End_Date, (w.period_year*100)+w.period_num period_id
      from xxcp_instance_gl_periods_v w
      where w.period_set_name_id             = pPeriod_Set_Name_id
        and (w.period_year*100)+w.period_num = pPeriod_id
        and w.Instance_id = pInstance_id;

   vGrowth_Rate         xxcp_forecast_history.growth_rate%type;
   vUplift_Rate         xxcp_forecast_history.uplift_rate%type;
   vAvg_Period_Cost     xxcp_forecast_history.avg_period_cost%type;
   vUplifted_Amount     xxcp_forecast_history.uplifted_amount%type;
   vConsidered_Periods  xxcp_forecast_history.considered_periods%type := 0;
   vInternalErrorCode   xxcp_errors.internal_error_code%type := 0;

   vTrueUpAmount        number := 0;
   vTotalTrueUpAmount   number := 0;
   vTotalForecastAmount number := 0;
   vRunning_Line_Cost   number := 0;
   vAssociated_data1    xxcp_cost_plus_rates.attribute1%type;
   vAssociated_data2    xxcp_cost_plus_rates.attribute1%type;
   vEntry_Type          varchar2(1);

   vTarget_Type_id            NUMBER(4);
   vTarget_Exch_Rate_Type     XXCP_TAX_REGISTRATIONS.exchange_rate_type%TYPE;
   vTarget_Reg_id             XXCP_TARGET_ASSIGNMENTS.reg_id%TYPE;
   vTarget_Set_of_books_id    XXCP_TARGET_ASSIGNMENTS.target_set_of_books_id%TYPE;
   vTarget_COA_ID             XXCP_TARGET_ASSIGNMENTS.target_coa_id%TYPE;
   vTarget_Instance_id        XXCP_TARGET_ASSIGNMENTS.target_instance_id%TYPE;
   vTarget_Currency_code      XXCP_TARGET_ASSIGNMENTS.target_acct_currency%TYPE;
   vCommon_Exch_Curr          XXCP_TAX_REGISTRATIONS.common_exch_curr%TYPE;
   vTrade_currency            XXCP_TAX_REGISTRATIONS.trade_currency%TYPE;
   vTarget_Zero_Flag          XXCP_TARGET_ASSIGNMENTS.zero_flag%TYPE;
   vTax_registration_id       NUMBER(10);
   vBalancing_Segment         NUMBER(4);
   vTarget_Assignment_id      INTEGER(10) := 0;

   vCurr_Cost_Category_id     number;
   vCurr_Category_Data_Source varchar2(10);
   vCurr_Cost_Category        varchar2(100);
   vCurr_Payer1               varchar2(15);
   vCurr_Payer1_percent       number;
   vCurr_Payer2               varchar2(15);
   vCurr_Payer2_percent       number;
   vCurr_Inter_payer          varchar2(15);
   vCurr_Account_Type         varchar2(100);
   vDef_Cost_Category_id      number;
   vDef_Category_Data_Source  varchar2(10);
   vDef_Cost_Category         varchar2(100);

   -- True_Up_Period  - 1
   vTrue_Up_Minus_Period_id  number := xxcp_reporting.Get_Period_Offset(xxcp_wks.gActual_Costs_Rec(1).Collection_Period_id,
                                                                        xxcp_wks.gActual_Costs_Rec(1).period_set_name_id,
                                                                        -1,
                                                                        xxcp_wks.gActual_Costs_Rec(1).instance_id);

   -- True_Up_Period  - 1
   vForecast_Minus_Period_id  number := xxcp_reporting.Get_Period_Offset(xxcp_wks.gActual_Costs_Rec(1).Period_id,
                                                                         xxcp_wks.gActual_Costs_Rec(1).period_set_name_id,
                                                                         -1,
                                                                         xxcp_wks.gActual_Costs_Rec(1).instance_id);

  Begin

    -- 02.05.15 (Start)
    open  cTargetType(gTargetTypeName);
    fetch cTargetType into vTarget_Type_Id;
    close cTargetType;

    xxcp_wks.I1009_ArrayTEMP := xxcp_wks.I1009_Array;

    Get_Tax_Reg_Information(cTarget_Type_id        => vTarget_Type_id,   -- System Profile
                            cOwner_Tax_Reg_id      => cOwner_Tax_Reg_id,
                            cPartner_Tax_Reg_id    => cPartner_Tax_Reg_id,
                            cEntity_Type           => 'Owner',
                            cTarget_Type_Match     => 'N',
                            -- Returned
                            cTarget_Assignment_id  => vTarget_Assignment_id,
                            cTarget_Zero_Flag      => vTarget_Zero_Flag,
                            cReg_id                => vTarget_Reg_id,
                            cTax_Reg_id            => vTax_registration_id,
                            cBal_Segment           => vBalancing_Segment,
                            cExchange_Rate_Type    => vTarget_Exch_Rate_Type,
                            cCOA_ID                => vTarget_COA_id,
                            cInstance_ID           => vTarget_Instance_id,
                            cSOB_ID                => vTarget_Set_of_books_id,
                            cCurrency_Code         => vTarget_Currency_code,
                            cCommon_Exch_Curr      => vCommon_Exch_Curr,
                            cTrade_currency        => vTrade_currency,
                            cInternalErrorCode     => vInternalErrorCode);

    xxcp_wks.I1009_Array := xxcp_wks.I1009_ArrayTEMP;
    -- 02.05.15 (Finish)


    gStart_of_Year_id := (to_number(substr(xxcp_wks.gActual_Costs_Rec(1).collection_period_id,1,4))*100)+1;

    xxcp_wks.gActual_Costs_Rec(1).Payer             := xxcp_foundation.Find_DynamicAttribute(gPayerAttribute);
    xxcp_wks.gActual_Costs_Rec(1).Payer_ret_pro_ind := xxcp_foundation.Find_DynamicAttribute(gPayerRetProAttribute);


    --
    -- First Check FC Period - 1 for Replan
    --
    for FCPMRec in FCPM (xxcp_wks.gActual_Costs_Rec(1).period_set_name_id,
                         vForecast_Minus_Period_id,
                         xxcp_wks.gActual_Costs_Rec(1).instance_id) Loop

      -- Get Cost Category
      vInternalErrorCode:= xxcp_te_base.Get_Cost_Category (cCompany              => xxcp_wks.gActual_Costs_Rec(1).Company,
                                                           cDepartment           => xxcp_wks.gActual_Costs_Rec(1).Department,
                                                           cAccount              => xxcp_wks.gActual_Costs_Rec(1).Account,
                                                           cPeriod_End_Date      => FCPMRec.End_Date,
                                                           cCost_Category_Id     => vCurr_Cost_Category_id,     -- out
                                                           cCategory_Data_Source => vCurr_Category_Data_Source, -- out
                                                           cCost_Category        => vCurr_Cost_Category,        -- out
                                                           cCost_Plus_Set_Id     => xxcp_wks.gActual_Costs_Rec(1).cost_plus_set_id);

      If vInternalErrorCode != 0 then
        Exit;
      End If;
      -- Get Payer Details
      vInternalErrorCode:= xxcp_te_base.Get_Payer_Details (cPayee            => xxcp_wks.gActual_Costs_Rec(1).Company,
                                                           cTerritory        => cOwner_Territory,
                                                           cCost_Category_id => vCurr_Cost_Category_id, --vCurr_Cost_Category,
                                                           cData_Source      => vCurr_Category_Data_Source,
                                                           cTransaction_Date => FCPMRec.End_Date,
                                                           cPayer1           => vCurr_Payer1,        --out
                                                           cPayer1_Percent   => vCurr_Payer1_percent,--out
                                                           cPayer2           => vCurr_Payer2,        --out
                                                           cPayer2_Percent   => vCurr_Payer2_percent,--out
                                                           cInter_Payer      => vCurr_Inter_payer,   --out
                                                           cAccount_Type     => vCurr_Account_Type);      --out

      If vInternalErrorCode != 0 then
        Exit;
      End If;

      vInternalErrorCode  := 7802; -- Rate Not Found

      -- Get Rates for Minus Period
      For ratesRec in Rates(xxcp_wks.gActual_Costs_Rec(1).period_set_name_id,
                            FCPMRec.End_Date,
                            cOwner_Short_Name,
                            cOwner_Territory,
                            vCurr_Cost_Category_id,           -- 02.05.20
                            vCurr_Category_Data_Source) Loop  -- 02.05.20
         vUplift_Rate       := nvl(ratesRec.uplift_rate,0);
         vGrowth_Rate       := nvl(ratesRec.growth_rate,0);
         vInternalErrorCode  := 0;
         Exit;
      End Loop;  -- ratesRec

      If vInternalErrorCode != 0 then
        Exit;
      End If;

      For fchRec in fch(xxcp_wks.gActual_Costs_Rec(1).Period_Set_Name_id,
                        FCPMRec.period_id,
                        xxcp_wks.gActual_Costs_Rec(1).Company,
                        xxcp_wks.gActual_Costs_Rec(1).Department,
                        xxcp_wks.gActual_Costs_Rec(1).Account,
                        xxcp_wks.gActual_Costs_Rec(1).Currency_Code,
                        xxcp_global.gCommon(1).current_Trading_Set_id,
                        xxcp_wks.gActual_Costs_Rec(1).cost_plus_set_id) Loop

        -- Check if replan required
        if fchRec.cost_category_id        <> nvl(vCurr_Cost_Category_id,0)
           or fchRec.Category_Data_Source <> nvl(vCurr_Category_Data_Source,0)
           or fchRec.Payer1               <> nvl(vCurr_Payer1,0)
           or fchRec.Payer1_Percent       <> nvl(vCurr_Payer1_Percent,0)
           or fchRec.Payer2               <> nvl(vCurr_Payer2,0)
           or fchRec.Payer2_Percent       <> nvl(vCurr_Payer2_Percent,0)
           or fchRec.Inter_Payer          <> nvl(vCurr_Inter_Payer,0)
           or fchRec.Account_Type         <> nvl(vCurr_Account_Type,'~NULL~') then
              -- Replan Required
              xxcp_wks.gActual_Costs_Rec(1).Replan := 'Y';
              vInternalErrorCode := 7015; -- CPA Replan Required.
              xxcp_foundation.FndWriteError(vInternalErrorCode,'Owner <'||cOwner_Short_Name||'> Territory <'||cOwner_Territory||'> Category <'||vCurr_Cost_Category||'> Date <'||to_char(cTransaction_Date)||'>');
        end if;
      End Loop;

      If vInternalErrorCode != 0 then
        Exit;
      End If;

   End loop;

    vInternalErrorCode := 0; -- Continue with TU even if no Category or Payee Found

    if vInternalErrorCode = 0 then

      -- Now continue with usual logic
    For AcsRec in ACS(pSource_assignment_id => xxcp_global.gCommon(1).current_assignment_id,
                      pPeriod_set_name_id   => xxcp_wks.gActual_Costs_Rec(1).period_set_name_id,
                      pCurrency_Code        => xxcp_wks.gActual_Costs_Rec(1).Currency_Code,
                      pCompany              => xxcp_wks.gActual_Costs_Rec(1).Company,
                      pDepartment           => xxcp_wks.gActual_Costs_Rec(1).Department,
                      pAccount              => xxcp_wks.gActual_Costs_Rec(1).Account,
                      pPeriod_from_id       => gStart_of_Year_id,
                      pPeriod_to_id         => xxcp_wks.gActual_Costs_Rec(1).collection_period_id,
                      pCost_plus_Set_id     => xxcp_wks.gActual_Costs_Rec(1).Cost_Plus_Set_id
                          )
    Loop

      -- Clear vars
      vConsidered_Periods        := vConsidered_Periods + 1;
      vGrowth_Rate               := 0;
      vUplift_Rate               := 0;
      vAvg_Period_Cost           := 0;
      vCurr_Cost_Category_id     := null;
      vCurr_Category_Data_Source := null;
      vCurr_Cost_Category        := null;
      vCurr_Payer1               := null;
      vCurr_Payer1_percent       := null;
      vCurr_Payer2               := null;
      vCurr_Payer2_percent       := null;
      vCurr_Inter_payer          := null;
      vCurr_Account_Type         := null;
      --vCurr_Territory            := null;

      -- Get Cost Category
      vInternalErrorCode:= xxcp_te_base.Get_Cost_Category (cCompany              => xxcp_wks.gActual_Costs_Rec(1).Company,
                                                           cDepartment           => xxcp_wks.gActual_Costs_Rec(1).Department,
                                                           cAccount              => xxcp_wks.gActual_Costs_Rec(1).Account,
                                                           cPeriod_End_Date      => AcsRec.Period_End_Date,
                                                           cCost_Category_Id     => vCurr_Cost_Category_id,     -- out
                                                           cCategory_Data_Source => vCurr_Category_Data_Source, -- out
                                                           cCost_Category        => vCurr_Cost_Category,        -- out
                                                           cCost_Plus_Set_Id     => xxcp_wks.gActual_Costs_Rec(1).cost_plus_set_id);

        If vInternalErrorCode = 0 then
          If vConsidered_Periods = 1 then
            --
            -- Set Default Cost Category (Used if any prior period does not return a Cost Cat).
            --
            vDef_Cost_Category_id     := vCurr_Cost_Category_id;
            vDef_Category_Data_Source := vCurr_Category_Data_Source;
            vDef_Cost_Category        := vCurr_Cost_Category;

          End if;

          --
      -- Get Payer Details
      vInternalErrorCode:= xxcp_te_base.Get_Payer_Details (cPayee            => xxcp_wks.gActual_Costs_Rec(1).Company,
                                                           cTerritory        => cOwner_Territory,
                                                             cCost_Category_id => vCurr_Cost_Category_id, --vCurr_Cost_Category,
                                                             cData_Source      => vCurr_Category_Data_Source,
                                                           cTransaction_Date => AcsRec.Period_End_Date,
                                                           cPayer1           => vCurr_Payer1,        --out
                                                           cPayer1_Percent   => vCurr_Payer1_percent,--out
                                                           cPayer2           => vCurr_Payer2,        --out
                                                           cPayer2_Percent   => vCurr_Payer2_percent,--out
                                                           cInter_Payer      => vCurr_Inter_payer,   --out
                                                           cAccount_Type     => vCurr_Account_Type);      --out

        Else
          If vConsidered_Periods > 1 and vDef_Cost_Category_id is not null and vDef_Category_Data_Source is not null then

            vCurr_Cost_Category_id     := vDef_Cost_Category_id;
            vCurr_Category_Data_Source := vDef_Category_Data_Source;
            vCurr_Cost_Category        := vDef_Cost_Category;

            vInternalErrorCode          := 0;
            xxcp_global.gDefCostCatUsed := 'Y';

            -- Message Default Category used
            xxcp_foundation.FndWriteError(100,'Defaulting Cost Category for Date <'||to_char(AcsRec.Period_End_Date)||'> to Cost Category <'||vCurr_Cost_Category||'> Cost Category Id <'||vCurr_Cost_Category_id||'> Data Source <'||vCurr_Category_Data_Source||'>');

          End if;
        End If;

        If vInternalErrorCode = 0 then
          vInternalErrorCode := 7802; -- No Rate found
      For ratesRec in Rates(xxcp_wks.gActual_Costs_Rec(1).period_set_name_id,
                            AcsRec.Period_End_Date,
                            cOwner_Short_Name,
                            cOwner_Territory,
                              vCurr_Cost_Category_id,           -- 02.05.20
                              vCurr_Category_Data_Source) Loop  -- 02.05.20
         vInternalErrorCode := 0;
         vUplift_Rate       := nvl(ratesRec.uplift_rate,0);
         vGrowth_Rate       := nvl(ratesRec.growth_rate,0);
         vRunning_Line_Cost := vRunning_Line_Cost + nvl(AcsRec.Actual_Cost,0) + (nvl(AcsRec.Actual_Cost,0) * nvl(ratesRec.uplift_rate,0));
         vAssociated_data1  := ratesRec.Associated_Data1;
         vAssociated_data2  := ratesRec.Associated_Data2;
         vEntry_Type        := ratesRec.Entry_Type;
             Exit;
          End Loop;  -- ratesRec

          If vInternalErrorCode != 0 then
            xxcp_foundation.FndWriteError(vInternalErrorCode,
               'Period Set Name Id <'||xxcp_wks.gActual_Costs_Rec(1).period_set_name_id||'> '||chr(10)||
               'Period End Date <'||to_char(AcsRec.Period_End_Date)||'> '||chr(10)||
               'Owner <'||cOwner_Short_Name||'> '||chr(10)||
               'Territory '||cOwner_Territory||'> '||chr(10)||
               'Cost Category id '||vCurr_Cost_Category_id||'> '||chr(10)||
               'Data Source <'||vCurr_Category_Data_Source||'>');
         Exit;
          End If;


        End If;

        If vConsidered_Periods = 1 and acsRec.Collection_Period_id <= xxcp_wks.gActual_Costs_Rec(1).collection_period_id Then -- Current Period
        xxcp_wks.I1009_Array(115) := vCurr_Cost_Category;
        xxcp_wks.I1009_Array(106) := vUplift_Rate;
        xxcp_wks.I1009_Array(107) := vGrowth_Rate;
        xxcp_wks.I1009_Array(108) := 0 ;
        xxcp_wks.I1009_Array(111) := xxcp_wks.gActual_Costs_Rec(1).Currency_Code;
        xxcp_wks.I1009_Array(114) := vUplifted_Amount;
        xxcp_wks.I1009_Array(112) := vCurr_Cost_Category_id;
        xxcp_wks.I1009_Array(119) := vAssociated_data1;
        xxcp_wks.I1009_Array(120) := vAssociated_data2;
        -- Payer Details
        xxcp_wks.I1009_Array(123) := vCurr_Payer1;
        xxcp_wks.I1009_Array(124) := vCurr_Payer1_Percent;
        xxcp_wks.I1009_Array(125) := vCurr_Payer2;
        xxcp_wks.I1009_Array(126) := vCurr_Payer2_Percent;
        xxcp_wks.I1009_Array(127) := vCurr_Inter_payer;
        xxcp_wks.I1009_Array(128) := vCurr_Account_Type;
      End If;



        If vInternalErrorCode = 0 then

      -- Forecast
      For fchRec in fch(xxcp_wks.gActual_Costs_Rec(1).Period_Set_Name_id,
                        AcsRec.collection_period_id,
                        xxcp_wks.gActual_Costs_Rec(1).Company,
                        xxcp_wks.gActual_Costs_Rec(1).Department,
                        xxcp_wks.gActual_Costs_Rec(1).Account,
                        xxcp_wks.gActual_Costs_Rec(1).Currency_Code,
                        xxcp_global.gCommon(1).current_Trading_Set_id,
                        xxcp_wks.gActual_Costs_Rec(1).cost_plus_set_id) Loop

        vTotalForecastAmount := nvl(vTotalForecastAmount,0) + nvl(fchRec.Uplifted_Amount,0);

        -- Check if replan required
            If (fchRec.cost_category_id        <> nvl(vCurr_Cost_Category_id,0)
             or fchRec.Category_Data_Source <> nvl(vCurr_Category_Data_Source,0)
             or fchRec.Payer1               <> nvl(vCurr_Payer1,0)
             or fchRec.Payer1_Percent       <> nvl(vCurr_Payer1_Percent,0)
             or fchRec.Payer2               <> nvl(vCurr_Payer2,0)
             or fchRec.Payer2_Percent       <> nvl(vCurr_Payer2_Percent,0)
             or fchRec.Inter_Payer          <> nvl(vCurr_Inter_Payer,0)
             or fchRec.Account_Type         <> nvl(vCurr_Account_Type,'~NULL~'))
             and xxcp_global.gDefCostCatUsed <> 'Y' then
               -- Replan Required
               xxcp_wks.gActual_Costs_Rec(1).Replan := 'Y';
               vInternalErrorCode := 7015; -- CPA Replan Required.
               xxcp_foundation.FndWriteError(vInternalErrorCode,'Owner <'||cOwner_Short_Name||'> Territory <'||cOwner_Territory||'> Category <'||vCurr_Cost_Category||'> Date <'||to_char(cTransaction_Date)||'>');
        end if;

            exit;  -- Exit After First FC History record

      End Loop;
        End If;

      -- True Up
        If vInternalErrorCode = 0 then
      If AcsRec.collection_period_id <= vTrue_Up_Minus_Period_id then
        For tuhRec in tuh(xxcp_wks.gActual_Costs_Rec(1).Period_Set_Name_id,
                          AcsRec.collection_period_id,
                          xxcp_wks.gActual_Costs_Rec(1).Company,
                          xxcp_wks.gActual_Costs_Rec(1).Department,
                          xxcp_wks.gActual_Costs_Rec(1).Account,
                          xxcp_wks.gActual_Costs_Rec(1).Currency_Code,
                          xxcp_global.gCommon(1).current_Trading_Set_id,
                          xxcp_wks.gActual_Costs_Rec(1).cost_plus_set_id) Loop

          vTotalTrueUpAmount := nvl(vTotalTrueUpAmount,0) + nvl(tuhRec.True_up_Amount,0);   -- 02.05.12

          -- Check if replan required
              if (tuhRec.cost_category_id        <> nvl(vCurr_Cost_Category_id,0)
               or tuhRec.Category_Data_Source <> nvl(vCurr_Category_Data_Source,0)
               or tuhRec.Payer1               <> nvl(vCurr_Payer1,0)
               or tuhRec.Payer1_Percent       <> nvl(vCurr_Payer1_Percent,0)
               or tuhRec.Payer2               <> nvl(vCurr_Payer2,0)
               or tuhRec.Payer2_Percent       <> nvl(vCurr_Payer2_Percent,0)
               or tuhRec.Inter_Payer          <> nvl(vCurr_Inter_Payer,0)
               or tuhRec.Account_Type         <> nvl(vCurr_Account_Type,'~NULL~'))
               and xxcp_global.gDefCostCatUsed <> 'Y' then
                 -- Replan Required
                 xxcp_wks.gActual_Costs_Rec(1).Replan := 'Y';
                 vInternalErrorCode := 7015; -- CPA Replan Required.
                 xxcp_foundation.FndWriteError(vInternalErrorCode,'Owner <'||cOwner_Short_Name||'> Territory <'||cOwner_Territory||'> Category <'||vCurr_Cost_Category||'> Date <'||to_char(cTransaction_Date)||'>');
          end if;

              exit;  -- Exit After First TU History record

        End loop;
      End if;
      End If;

      -- Exit clause
      If vInternalErrorCode <> 0 then
        Exit;
      End If;

    End Loop;  -- AcsRec
    End if;

    -- Error Codes
    If vInternalErrorCode = 7802 then
         xxcp_foundation.FndWriteError(vInternalErrorCode,'Owner <'||cOwner_Short_Name||'> Territory <'||cOwner_Territory||'> Category <'||vCurr_Cost_Category||'> Date <'||to_char(cTransaction_Date)||'>');
    ElsIf vInternalErrorCode = 7803 then
         xxcp_foundation.FndWriteError(vInternalErrorCode,'Growth Rate <'||to_char(vGrowth_Rate)||
          '> vUplift_Rate <'||to_char(vUplift_Rate)||'> Period from <'||to_char(gStart_of_Year_id)||'>'||' Period to <'||to_char(xxcp_wks.gActual_Costs_Rec(1).collection_period_id)||'>');
    End If;

    if vInternalErrorCode = 0 then
      vTrueUpAmount := vRunning_Line_Cost - (vTotalTrueUpAmount +  vTotalForecastAmount);
      xxcp_wks.I1009_Array(109) := (vTotalTrueUpAmount -  vTotalForecastAmount) ;
      xxcp_wks.I1009_Array(110) := vConsidered_Periods;
      xxcp_wks.I1009_Array(116) := vTrueUpAmount;
      xxcp_wks.I1009_Array(117) := vTotalTrueUpAmount;
      xxcp_wks.I1009_Array(118) := vTotalForecastAmount;
      xxcp_wks.I1009_Array(121) := nvl(vRunning_Line_Cost,0); -- fee
    End if;

    Return (vInternalErrorCode);

  End True_Up_Rates;


  --
  --## ***********************************************************************************************************
  --##
  --##                            FC True-Up Rates
  --##
  --##         Called on second pass of True-Up during REPLAN activity.
  --##     Function looks at the Trial History Tables which have been replanned.
  --##
  --## ***********************************************************************************************************
  --
  Function FC_True_Up_Rates(cTransaction_Date   in date,
                            cOwner_Short_Name   in varchar2,
                            cOwner_Territory    in varchar2,
                            cOwner_Tax_Reg_Id   in number,
                            cPartner_Tax_Reg_id in number) return Number is

  --  Period Net
  Cursor ACS(pSource_assignment_id in number,
             pPeriod_set_name_id   in number,
             pCurrency_Code        in varchar2,
             pCompany              in varchar2,
             pDepartment           in varchar2,
             pAccount              in varchar2,
             pPeriod_from_id       in number,
             pPeriod_to_id         in number,
             pCost_Plus_Set_id     in number) is
   select d.period_end_date,
          d.collection_period_id,
          (nvl(d.period_net_dr,0)-(nvl(d.period_net_cr,0))) Actual_Cost
   from xxcp_actual_costs d
   where d.source_assignment_id = pSource_assignment_id
     and d.period_set_name_id   = pPeriod_set_name_id
     and d.company              = pCompany
     and d.department           = pDepartment
     and d.account              = pAccount
     and d.currency_code        = pCurrency_Code
     and d.collection_period_id between pPeriod_from_id and pPeriod_to_id
     and d.cost_plus_set_id     = pCost_plus_set_id
     order by d.collection_period_id desc;

    -- Growth and Uplift Rates
    Cursor Rates(pPeriod_Set_Name_id in number, pTransaction_Date in date, pOwner in varchar2,
                 pTerritory in varchar2, pCost_Category_id in number, pData_Source in varchar2) is
     select c.owner, c.territory, c.cost_category,
            c.uplift_rate*0.01 uplift_rate, c.growth_rate*0.01 growth_rate, c.periods_required,
            c.attribute1 associated_data1, c.attribute2 associated_data2,
            c.ENTRY_TYPE
       from XXCP_COST_PLUS_RATES  c
           ,XXCP_TABLE_SET_CTL t
     where c.period_set_name_id      = pPeriod_Set_Name_id
       and t.set_id                  = c.rate_set_id
       and t.set_base_table          = 'CP_COST_PLUS_RATES'
       and t.set_type                = 'COST PLUS RATES'
       and ((nvl(c.owner,'*')        = '*' or c.owner         = pOwner)
       and (nvl(c.territory,'*')     = '*' or c.territory     = pTerritory)
       and (c.Cost_Category_id = 0 or (c.cost_category_id = pCost_Category_id and c.data_source = pData_source)))  -- 02.05.20
       and pTransaction_Date between nvl(c.effective_from_date,'01-JAN-1980') and nvl(c.effective_to_date,'31-DEC-2999')
     Order by to_number(t.attribute1) desc, decode(c.owner ,'*',0,10) desc, -- 02.05.14
              decode(c.territory,'*',0,10) desc, decode(c.cost_category_id,0,0,10) desc,
              c.rates_id desc; --c.effective_from_date;

   -- Forecast History
 Cursor fch(pPeriod_Set_Name_id in number,
              pPeriod_id          in number,
              pCompany            in varchar2,
              pDepartment         in varchar2,
              pAccount            in varchar2,
              pCurrency_Code      in varchar2,
              pTrading_Set_id     in varchar2,
              pCost_plus_set_id   in number) is
    select total_uplifted_amount/trading_set_count Uplifted_Amount,
           b.trading_set_id
    from (select sum(h.Uplifted_Amount) over() total_uplifted_amount,
                 count(h.trading_Set_id) over() trading_set_count,
                 h.trading_set_id
        from xxcp_fc_forecast_history h
        where h.period_id          = pPeriod_id
          and h.company            = pCompany
          and h.department         = pDepartment
          and h.account            = pAccount
          and h.currency_code      = pCurrency_Code
          and h.period_set_name_id = pPeriod_Set_Name_id
          and h.cost_plus_set_id   = pCost_Plus_Set_Id
          and h.cpa_type           = 'RF') a,
         (select nvl(h.cost_category_id,0)  Cost_Category_id,
              nvl(data_source,0)         Category_Data_Source,
              nvl(payer1,0)              Payer1,
              nvl(payer1_percent,0)      Payer1_Percent,
              nvl(payer2,0)              Payer2,
              nvl(payer2_percent,0)      Payer2_Percent,
              nvl(inter_payer,0)         Inter_Payer,
              nvl(account_type,'~NULL~') Account_Type,
              nvl(growth_rate,0)         Growth_Rate,
              nvl(uplift_rate,0)         Uplift_Rate,
              trading_set_id
        from xxcp_fc_forecast_history h
        where h.period_id          = pPeriod_id
          and h.company            = pCompany
          and h.department         = pDepartment
          and h.account            = pAccount
          and h.currency_code      = pCurrency_Code
          and h.period_set_name_id = pPeriod_Set_Name_id
          and h.cost_plus_set_id   = pCost_Plus_Set_Id
          and h.cpa_type           = 'RF'
          and h.trading_set_id     = pTrading_Set_id) b
    where a.trading_set_id = b.trading_set_id (+)
    order by  b.trading_set_id;

   -- True Up History
   Cursor tuh(pPeriod_Set_Name_id in number,
              pPeriod_id          in number,
              pCompany            in varchar2,
              pDepartment         in varchar2,
              pAccount            in varchar2,
              pCurrency_Code      in varchar2,
              pTrading_Set_id     in number,
              pCost_Plus_Set_id   in number) is
    select total_true_amount/trading_set_count True_Up_Amount,
           Cost_Category_id,
           Category_Data_Source,
           Payer1,
           Payer1_Percent,
           Payer2,
           Payer2_Percent,
           Inter_Payer,
           Account_Type,
           b.trading_set_id
    from (select sum(h.True_up_Amount) over() total_true_amount,
                 count(h.trading_Set_id) over() trading_set_count,
                 h.trading_set_id
        from xxcp_fc_forecast_history h
        where h.period_id          = pPeriod_id
          and h.company            = pCompany
          and h.department         = pDepartment
          and h.account            = pAccount
          and h.currency_code      = pCurrency_Code
          and h.period_set_name_id = pPeriod_Set_Name_id
          and h.cost_plus_set_id   = pCost_Plus_Set_Id
          and h.cpa_type           = 'RT') a,
         (select nvl(h.cost_category_id,0)  Cost_Category_id,
              nvl(data_source,0)         Category_Data_Source,
              nvl(payer1,0)              Payer1,
              nvl(payer1_percent,0)      Payer1_Percent,
              nvl(payer2,0)              Payer2,
              nvl(payer2_percent,0)      Payer2_Percent,
              nvl(inter_payer,0)         Inter_Payer,
              nvl(account_type,'~NULL~') Account_Type,
              nvl(growth_rate,0)         Growth_Rate,
              nvl(uplift_rate,0)         Uplift_Rate,
              trading_set_id
        from xxcp_fc_forecast_history h
        where h.period_id          = pPeriod_id
          and h.company            = pCompany
          and h.department         = pDepartment
          and h.account            = pAccount
          and h.currency_code      = pCurrency_Code
          and h.period_set_name_id = pPeriod_Set_Name_id
          and h.cost_plus_set_id   = pCost_Plus_Set_Id
          and h.trading_set_id     = pTrading_Set_id
          and h.cpa_type           = 'RT') b
    where a.trading_set_id = b.trading_set_id (+)
    order by  b.trading_set_id;

    Cursor cTargetType(pTarget_Type in varchar2) is
     Select Target_Type_id
       from xxcp_sys_target_types t
     where Target_Type = pTarget_Type;

   vGrowth_Rate               xxcp_forecast_history.growth_rate%type;
   vUplift_Rate               xxcp_forecast_history.uplift_rate%type;
   vAvg_Period_Cost           xxcp_forecast_history.avg_period_cost%type;
   vUplifted_Amount           xxcp_forecast_history.uplifted_amount%type;
   vConsidered_Periods        xxcp_forecast_history.considered_periods%type := 0;

   vInternalErrorCode         xxcp_errors.internal_error_code%type := 0;

   vTrueUpAmount              number := 0;
   vTotalTrueUpAmount         number := 0;
   vTotalForecastAmount       number := 0;
   vRunning_Line_Cost         number := 0;
   vAssociated_data1          xxcp_cost_plus_rates.attribute1%type;
   vAssociated_data2          xxcp_cost_plus_rates.attribute1%type;
   vEntry_Type                varchar2(1);

   vTarget_Type_id            NUMBER(4);
   vTarget_Exch_Rate_Type     XXCP_TAX_REGISTRATIONS.exchange_rate_type%TYPE;
   vTarget_Reg_id             XXCP_TARGET_ASSIGNMENTS.reg_id%TYPE;
   vTarget_Set_of_books_id    XXCP_TARGET_ASSIGNMENTS.target_set_of_books_id%TYPE;
   vTarget_COA_ID             XXCP_TARGET_ASSIGNMENTS.target_coa_id%TYPE;
   vTarget_Instance_id        XXCP_TARGET_ASSIGNMENTS.target_instance_id%TYPE;
   vTarget_Currency_code      XXCP_TARGET_ASSIGNMENTS.target_acct_currency%TYPE;
   vCommon_Exch_Curr          XXCP_TAX_REGISTRATIONS.common_exch_curr%TYPE;
   vTrade_currency            XXCP_TAX_REGISTRATIONS.trade_currency%TYPE;
   vTarget_Zero_Flag          XXCP_TARGET_ASSIGNMENTS.zero_flag%TYPE;
   vTax_registration_id       NUMBER(10);
   vBalancing_Segment         NUMBER(4);
   vTarget_Assignment_id      INTEGER(10) := 0;

   vCurr_Cost_Category_id     number;
   vCurr_Category_Data_Source varchar2(10);
   vCurr_Cost_Category        varchar2(100);
   vCurr_Payer1               varchar2(15);
   vCurr_Payer1_percent       number;
   vCurr_Payer2               varchar2(15);
   vCurr_Payer2_percent       number;
   vCurr_Inter_payer          varchar2(15);
   vCurr_Account_Type         varchar2(100);
   vDef_Cost_Category_id      number;
   vDef_Category_Data_Source  varchar2(10);
   vDef_Cost_Category         varchar2(100);

   -- True_Up_Period  - 1
   vTrue_Up_Minus_Period_id  number := xxcp_reporting.Get_Period_Offset(xxcp_wks.gActual_Costs_Rec(1).Collection_Period_id,
                                                                        xxcp_wks.gActual_Costs_Rec(1).period_set_name_id,
                                                                        -1,
                                                                        xxcp_wks.gActual_Costs_Rec(1).instance_id);

  Begin

    -- 02.05.15 (Start)
    open  cTargetType(gTargetTypeName);
    fetch cTargetType into vTarget_Type_Id;
    close cTargetType;

    xxcp_wks.I1009_ArrayTEMP := xxcp_wks.I1009_Array;

    Get_Tax_Reg_Information(cTarget_Type_id        => vTarget_Type_id,   -- System Profile
                            cOwner_Tax_Reg_id      => cOwner_Tax_Reg_id,
                            cPartner_Tax_Reg_id    => cPartner_Tax_Reg_id,
                            cEntity_Type           => 'Owner',
                            cTarget_Type_Match     => 'N',
                            -- Returned
                            cTarget_Assignment_id  => vTarget_Assignment_id,
                            cTarget_Zero_Flag      => vTarget_Zero_Flag,
                            cReg_id                => vTarget_Reg_id,
                            cTax_Reg_id            => vTax_registration_id,
                            cBal_Segment           => vBalancing_Segment,
                            cExchange_Rate_Type    => vTarget_Exch_Rate_Type,
                            cCOA_ID                => vTarget_COA_id,
                            cInstance_ID           => vTarget_Instance_id,
                            cSOB_ID                => vTarget_Set_of_books_id,
                            cCurrency_Code         => vTarget_Currency_code,
                            cCommon_Exch_Curr      => vCommon_Exch_Curr,
                            cTrade_currency        => vTrade_currency,
                            cInternalErrorCode     => vInternalErrorCode);

    xxcp_wks.I1009_Array := xxcp_wks.I1009_ArrayTEMP;
    -- 02.05.15 (Finish)


    gStart_of_Year_id := (to_number(substr(xxcp_wks.gActual_Costs_Rec(1).collection_period_id,1,4))*100)+1;

    xxcp_wks.gActual_Costs_Rec(1).Payer             := xxcp_foundation.Find_DynamicAttribute(gPayerAttribute);
    xxcp_wks.gActual_Costs_Rec(1).Payer_ret_pro_ind := xxcp_foundation.Find_DynamicAttribute(gPayerRetProAttribute);

    vInternalErrorCode  := 7803; -- Not enough values in xxcp_actual_costs table

    For AcsRec in ACS(pSource_assignment_id => xxcp_global.gCommon(1).current_assignment_id,
                      pPeriod_set_name_id   => xxcp_wks.gActual_Costs_Rec(1).period_set_name_id,
                      pCurrency_Code        => xxcp_wks.gActual_Costs_Rec(1).Currency_Code,
                      pCompany              => xxcp_wks.gActual_Costs_Rec(1).Company,
                      pDepartment           => xxcp_wks.gActual_Costs_Rec(1).Department,
                      pAccount              => xxcp_wks.gActual_Costs_Rec(1).Account,
                      pPeriod_from_id       => gStart_of_Year_id,
                      pPeriod_to_id         => xxcp_wks.gActual_Costs_Rec(1).collection_period_id,
                      pCost_plus_set_id     => xxcp_wks.gActual_Costs_Rec(1).Cost_Plus_set_id
                          )
    Loop

      -- Clear vars
      vConsidered_Periods        := vConsidered_Periods + 1;
      vGrowth_Rate               := 0;
      vUplift_Rate               := 0;
      vAvg_Period_Cost           := 0;
      vCurr_Cost_Category_id     := null;
      vCurr_Category_Data_Source := null;
      vCurr_Cost_Category        := null;
      vCurr_Payer1               := null;
      vCurr_Payer1_percent       := null;
      vCurr_Payer2               := null;
      vCurr_Payer2_percent       := null;
      vCurr_Inter_payer          := null;
      vCurr_Account_Type         := null;
      --vCurr_Territory            := null;

      -- Get Cost Category
      vInternalErrorCode:= xxcp_te_base.Get_Cost_Category (cCompany              => xxcp_wks.gActual_Costs_Rec(1).Company,
                                                           cDepartment           => xxcp_wks.gActual_Costs_Rec(1).Department,
                                                           cAccount              => xxcp_wks.gActual_Costs_Rec(1).Account,
                                                           cPeriod_End_Date      => AcsRec.Period_End_Date,
                                                           cCost_Category_Id     => vCurr_Cost_Category_id,     -- out
                                                           cCategory_Data_Source => vCurr_Category_Data_Source, -- out
                                                           cCost_Category        => vCurr_Cost_Category,        -- out
                                                           cCost_Plus_Set_Id     => xxcp_wks.gActual_Costs_Rec(1).cost_plus_set_id);

      If vInternalErrorCode = 0 then
        If vConsidered_Periods = 1 then
          --
          -- Set Default Cost Category (Used if any prior period does not return a Cost Cat).
          --
          vDef_Cost_Category_id     := vCurr_Cost_Category_id;
          vDef_Category_Data_Source := vCurr_Category_Data_Source;
          vDef_Cost_Category        := vCurr_Cost_Category;
        End if;

      -- Get Payer Details
      vInternalErrorCode:= xxcp_te_base.Get_Payer_Details (cPayee            => xxcp_wks.gActual_Costs_Rec(1).Company,
                                                           cTerritory        => cOwner_Territory,
                                                           cCost_Category_id => vCurr_Cost_Category_id, --vCurr_Cost_Category,
                                                           cData_Source      => vCurr_Category_Data_Source,
                                                           cTransaction_Date => AcsRec.Period_End_Date,
                                                           cPayer1           => vCurr_Payer1,        --out
                                                           cPayer1_Percent   => vCurr_Payer1_percent,--out
                                                           cPayer2           => vCurr_Payer2,        --out
                                                           cPayer2_Percent   => vCurr_Payer2_percent,--out
                                                           cInter_Payer      => vCurr_Inter_payer,   --out
                                                           cAccount_Type     => vCurr_Account_Type);      --out


        Else
          If vConsidered_Periods > 1 and vDef_Cost_Category_id is not null and vDef_Category_Data_Source is not null then

            vCurr_Cost_Category_id     := vDef_Cost_Category_id;
            vCurr_Category_Data_Source := vDef_Category_Data_Source;
            vCurr_Cost_Category        := vDef_Cost_Category;

            vInternalErrorCode          := 0;
            xxcp_global.gDefCostCatUsed := 'Y';

            -- Message Default Category used
            xxcp_foundation.FndWriteError(100,'Defaulting Cost Category for Date <'||to_char(AcsRec.Period_End_Date)||'> to Cost Category <'||vCurr_Cost_Category||'> Cost Category Id <'||vCurr_Cost_Category_id||'> Data Source <'||vCurr_Category_Data_Source||'>');

          End if;
        End If;


      -- Search
      vInternalErrorCode  := 7802; -- No Growth Rate found

      For ratesRec in Rates(xxcp_wks.gActual_Costs_Rec(1).period_set_name_id,
                            AcsRec.Period_End_Date,
                            cOwner_Short_Name,
                            cOwner_Territory,
                    --        vCurr_Cost_Category,
                            vCurr_Cost_Category_id,
                            vCurr_Category_Data_Source) Loop  -- 02.05.20
                    --        vCurrent_Data_Source) Loop -- 02.05.12
         vInternalErrorCode := 0;
         vUplift_Rate       := nvl(ratesRec.uplift_rate,0);
         vGrowth_Rate       := nvl(ratesRec.growth_rate,0);
         vRunning_Line_Cost := vRunning_Line_Cost + nvl(AcsRec.Actual_Cost,0) + (nvl(AcsRec.Actual_Cost,0) * nvl(ratesRec.uplift_rate,0));
         vAssociated_data1  := ratesRec.Associated_Data1;
         vAssociated_data2  := ratesRec.Associated_Data2;
         vEntry_Type        := ratesRec.Entry_Type;

         Exit;
      End Loop;  -- ratesRec

      If vConsidered_Periods = 1 Then -- Current Period
        xxcp_wks.I1009_Array(115) := vCurr_Cost_Category;
        xxcp_wks.I1009_Array(106) := vUplift_Rate;
        xxcp_wks.I1009_Array(107) := vGrowth_Rate;
        xxcp_wks.I1009_Array(108) := 0 ;
        xxcp_wks.I1009_Array(111) := xxcp_wks.gActual_Costs_Rec(1).Currency_Code;
        xxcp_wks.I1009_Array(114) := vUplifted_Amount;
        xxcp_wks.I1009_Array(112) := vCurr_Cost_Category_id;
        xxcp_wks.I1009_Array(119) := vAssociated_data1;
        xxcp_wks.I1009_Array(120) := vAssociated_data2;
        -- Payer Details
        xxcp_wks.I1009_Array(123) := vCurr_Payer1;
        xxcp_wks.I1009_Array(124) := vCurr_Payer1_Percent;
        xxcp_wks.I1009_Array(125) := vCurr_Payer2;
        xxcp_wks.I1009_Array(126) := vCurr_Payer2_Percent;
        xxcp_wks.I1009_Array(127) := vCurr_Inter_payer;
        xxcp_wks.I1009_Array(128) := vCurr_Account_Type;
      End If;

      -- Forecast
      For fchRec in fch(xxcp_wks.gActual_Costs_Rec(1).Period_Set_Name_id,
                        AcsRec.collection_period_id,
                        xxcp_wks.gActual_Costs_Rec(1).Company,
                        xxcp_wks.gActual_Costs_Rec(1).Department,
                        xxcp_wks.gActual_Costs_Rec(1).Account,
                        xxcp_wks.gActual_Costs_Rec(1).Currency_Code,
                        xxcp_global.gCommon(1).current_Trading_Set_id,
                        xxcp_wks.gActual_Costs_Rec(1).Cost_Plus_set_id) Loop

        vTotalForecastAmount := nvl(vTotalForecastAmount,0) + nvl(fchRec.Uplifted_Amount,0);

        Exit; -- Exit after first FC History record

      End Loop;

      -- True Up
      If AcsRec.collection_period_id <= vTrue_Up_Minus_Period_id then
        For tuhRec in tuh(xxcp_wks.gActual_Costs_Rec(1).Period_Set_Name_id,
                          AcsRec.collection_period_id,
                          xxcp_wks.gActual_Costs_Rec(1).Company,
                          xxcp_wks.gActual_Costs_Rec(1).Department,
                          xxcp_wks.gActual_Costs_Rec(1).Account,
                          xxcp_wks.gActual_Costs_Rec(1).Currency_Code,
                          xxcp_global.gCommon(1).current_Trading_Set_id,
                          xxcp_wks.gActual_Costs_Rec(1).Cost_Plus_set_id) Loop

          vTotalTrueUpAmount := nvl(vTotalTrueUpAmount,0) + nvl(tuhRec.True_up_Amount,0);   -- 02.05.12

          -- Check if replan required
          if (tuhRec.cost_category_id        <> vCurr_Cost_Category_id
             or tuhRec.Category_Data_Source <> vCurr_Category_Data_Source
             or tuhRec.Payer1               <> vCurr_Payer1
             or tuhRec.Payer1_Percent       <> vCurr_Payer1_Percent
             or tuhRec.Payer2               <> vCurr_Payer2
             or tuhRec.Payer2_Percent       <> vCurr_Payer2_Percent
             or tuhRec.Inter_Payer          <> vCurr_Inter_Payer
             or tuhRec.Account_Type         <> vCurr_Account_Type)
             and xxcp_global.gDefCostCatUsed <> 'Y' then
                -- Replan Required
                xxcp_wks.gActual_Costs_Rec(1).Replan := 'Y';
                vInternalErrorCode := 7015; -- CPA Replan Required.
                xxcp_foundation.FndWriteError(vInternalErrorCode,'Owner <'||cOwner_Short_Name||'> Territory <'||cOwner_Territory||'> Category <'||vCurr_Cost_Category||'> Date <'||to_char(cTransaction_Date)||'>');
          end if;

          exit; -- Exit After first TU History record

        End loop;
      End if;

      -- Error Codes
      If vInternalErrorCode = 7802 then
           xxcp_foundation.FndWriteError(vInternalErrorCode,'Owner <'||cOwner_Short_Name||'> Territory <'||cOwner_Territory||'> Category <'||vCurr_Cost_Category||'> Date <'||to_char(cTransaction_Date)||'>');
      ElsIf vInternalErrorCode = 7803 then
           xxcp_foundation.FndWriteError(vInternalErrorCode,'Growth Rate <'||to_char(vGrowth_Rate)||
            '> vUplift_Rate <'||to_char(vUplift_Rate)||'> Period from <'||to_char(gStart_of_Year_id)||'>'||' Period to <'||to_char(xxcp_wks.gActual_Costs_Rec(1).collection_period_id)||'>');
      End If;

      -- Exit clause
      If vInternalErrorCode <> 0 then
        Exit;
      End If;

    End Loop;  -- AcsRec

    if vInternalErrorCode = 0 then
      vTrueUpAmount := vRunning_Line_Cost - (vTotalTrueUpAmount +  vTotalForecastAmount);

      xxcp_wks.I1009_Array(109) := (vTotalTrueUpAmount -  vTotalForecastAmount) ;
      xxcp_wks.I1009_Array(110) := vConsidered_Periods;
      xxcp_wks.I1009_Array(116) := vTrueUpAmount;
      xxcp_wks.I1009_Array(117) := vTotalTrueUpAmount;
      xxcp_wks.I1009_Array(118) := vTotalForecastAmount;
      xxcp_wks.I1009_Array(121) := nvl(vRunning_Line_Cost,0); -- fee
    End if;

    Return (vInternalErrorCode);

  End FC_True_Up_Rates;

  -- ## ***********************************************************************************************************
  -- ##
  -- ##                                Process_Gain_Loss
  -- ##
  -- ## ***********************************************************************************************************
  PROCEDURE Process_Gain_Loss(cAction                 IN VARCHAR2,
                              cSet_of_books_id        IN NUMBER,
                              cSourceRowid            IN ROWID,
                              cTransaction_id         IN NUMBER,
                              cTransaction_Table      IN VARCHAR2,
                              cTransaction_Type       IN VARCHAR2,
                              cTransaction_Class      IN VARCHAR2,
                              cQuantity               IN NUMBER,
                              --
                              cEntity_Set_id          IN NUMBER,
                              cTransaction_set_id     IN NUMBER,
                              --
                              cTRX_Qualifier1         IN VARCHAR2,
                              cTRX_Qualifier2         IN VARCHAR2,
                              cTransaction_Currency   IN VARCHAR2,
                              cEffective_Date         IN DATE,
                              cOwner_Tax_Reg_id       IN NUMBER,
                              cPartner_Tax_Reg_id     IN NUMBER,
                              cAttribute_id           IN NUMBER,
                              cInterface_ID           IN NUMBER,
                              cOwner_Common_Exch_Curr IN VARCHAR2,
                              cBase_SQL_Build_ID      IN NUMBER,
                              -- OUT
                              cInternalErrorCode IN OUT NOCOPY NUMBER) IS

    -- Model Control

    CURSOR MC(pEntity_Set_Id IN NUMBER
            , pTRX_Qualifier1 IN VARCHAR2, pTRX_Qualifier2 IN VARCHAR2
            , pTransaction_Set_id IN NUMBER, pTransaction_Table IN VARCHAR2, pTransaction_Type IN VARCHAR2
            , pTransaction_Class IN VARCHAR2, pEffective_Date IN DATE, pTrading_Set_id IN NUMBER) IS
      SELECT /*+ Index(smn) */
       mc.sql_build_id,
       smn.MODEL_NAME,
       mc.model_name_id,
       mc.MC_QUALIFIER1,
       mc.MC_QUALIFIER2,
       mc.Account_Price_method_id AC_Pricing_Id,
       mc.Target_type_id,
       mc.model_ctl_id,
       mc.exchange_rate_type,
       smn.Extended_Precision,
       mc.formula_set_id,
       nvl(smn.target_type_match_ignore,'N') Target_Type_Match
        FROM xxcp_model_ctl mc,
             xxcp_sys_model_names smn,
             xxcp_sys_qualifier_groups mq
       WHERE mq.source_id          = xxcp_global.gCommon(1).Source_id
         AND mc.model_ctl_group_id = mq.qualifier_group_id
         AND mq.qualifier_usage    = 'M'
         AND mc.model_name_id      = smn.model_name_id
         AND mc.source_id          = smn.source_id
         AND nvl(mc.entity_set_id, pEntity_Set_id) = pEntity_Set_id
         AND mc.transaction_set_id = pTransaction_set_id
         AND nvl(mc.MC_Qualifier1, '~null~') =
             DECODE(mc.MC_Qualifier1,
                    '*',
                    nvl(mc.MC_Qualifier1, '~null~'),
                    nvl(xxcp_foundation.MC_DynamicAttribute(mq.Qualifier1_id), '~null~'))
         AND nvl(mc.MC_Qualifier2, '~null~') =
             DECODE(mc.MC_Qualifier2,
                    '*',
                    nvl(mc.MC_Qualifier2, '~null~'),
                    nvl(xxcp_foundation.MC_DynamicAttribute(mq.Qualifier2_id), '~null~'))
         AND nvl(mc.MC_Qualifier3, '~null~') =
             DECODE(mc.MC_Qualifier3,
                    '*',
                    nvl(mc.MC_Qualifier3, '~null~'),
                    nvl(xxcp_foundation.MC_DynamicAttribute(mq.Qualifier3_id), '~null~'))
         AND nvl(mc.MC_Qualifier4, '~null~') =
             DECODE(mc.MC_Qualifier4,
                    '*',
                    nvl(mc.MC_Qualifier4, '~null~'),
                    nvl(xxcp_foundation.MC_DynamicAttribute(mq.Qualifier4_id), '~null~'))
         -- Trx Qualifiers
         AND nvl(mc.TRX_Qualifier1, '~null~') =
             DECODE(mc.TRX_Qualifier1,
                    '*',
                    nvl(mc.TRX_Qualifier1, '~null~'),
                    nvl(pTRX_Qualifier1, '~null~'))
         AND nvl(mc.TRX_Qualifier2, '~null~') =
             DECODE(mc.TRX_Qualifier2,
                    '*',
                    nvl(mc.TRX_Qualifier2, '~null~'),
                    nvl(pTRX_Qualifier2, '~null~'))
         --
         AND mc.Trading_Set_id   = pTrading_Set_id
         AND mc.Active           = 'Y'
         AND mc.model_name_id    = ANY
       (SELECT /*+ Index(XXCP_ACCOUNT_RULES_A1) */ ar.Model_Name_id
                FROM XXCP_ACCOUNT_RULES ar
               WHERE ar.source_id = mc.source_id
                 AND DECODE(ar.transaction_table,'*',pTRANSACTION_TABLE, ar.transaction_table) = pTRANSACTION_TABLE
                 AND DECODE(ar.transaction_Type,'*',pTRANSACTION_TYPE,ar.transaction_Type)   = pTRANSACTION_TYPE
                 AND DECODE(ar.transaction_class,'*',pTRANSACTION_CLASS,ar.transaction_class) = pTRANSACTION_CLASS
                 AND ar.rule_type in ('G','W')
                 AND ar.active    = 'Y'
                 )
                 AND pEffective_Date BETWEEN
                   nvl(mc.Effective_From_Date, TO_DATE('01-JAN-1980','DD-MON-YYYY')) AND
                   nvl(mc.Effective_To_Date, TO_DATE('31-DEC-2999','DD-MON-YYYY'))
          order by smn.model_sequence;

    vModel_Name     XXCP_SYS_MODEL_NAMES.model_name%TYPE;
    vModel_Name_ID  XXCP_SYS_MODEL_NAMES.model_name_id%TYPE;
    vTarget_Type_ID XXCP_SYS_TARGET_TYPES.target_type_id%TYPE;

    -- Transaction Rules

    CURSOR AR(pTarget_Type_id IN NUMBER, pModel_Name_id IN NUMBER, pTransaction_Table IN VARCHAR2, pTransaction_Type IN VARCHAR2, pTransaction_Class IN VARCHAR2) IS
      SELECT ar.rule_name,
             ar.entity_type,
             ar.rule_value_name,
             ar.change_sign,
             ar.rule_id,
             ar.transaction_class,
             ar.target_table,
             ar.rule_type,
             -- Pair A
             ar.pair_a_value_attribute_id,
             ar.pair_a_curr_attribute_id,
             ar.pair_a_exch_date_attribute_id,
             ar.pair_a_exch_type_attribute_id,
             ar.pair_a_exch_rate_attribute_id,
             -- Pair B
             ar.pair_b_value_attribute_id,
             ar.pair_b_curr_attribute_id,
             ar.pair_b_exch_date_attribute_id,
             ar.pair_b_exch_type_attribute_id,
             ar.pair_b_exch_rate_attribute_id,
             --
             t.short_description Target_Record_Status,
             decode(ar.EXCLUDE_FROM_BALANCING,'Y','N',t.target_rounding) Target_Rounding,
             t.round_suppressed_rules,
             nvl(ar.target_type_id, ptarget_type_id) target_type_id,
             nvl(substr(t.enforce_accounting,1,1), 'Y') enforce_accounting,
             nvl(t.cross_validation,'N') cross_validation,
             ar.rule_category_1, ar.rule_category_2,
             ar.no_rounding no_rounding_rule,
             ar.transaction_class_replacement,
             ar.Rounding_Group,
             nvl(ar.Zero_Suppression_rule,'T') Zero_Suppression_rule,
             ar.cost_plus_rule,
             -- From Target Table
             t.Extended_Values,
             t.Entered_Amt_Bal Entered_Amounts,
             t.Accounted_Amt_Bal Accounted_Amounts,
             nvl(ar.Target_Suppression_Rule,'N') Target_Suppression_Rule,
             nvl(ar.History_Suppression_Rule,'N') History_Suppression_Rule,
             nvl(ar.ONCE_BRK_ATTRIBUTE1_ID,0) ONCE_BRK_ATTRIBUTE1_ID,
             nvl(ar.ONCE_BRK_ATTRIBUTE2_ID,0) ONCE_BRK_ATTRIBUTE2_ID,
             nvl(ar.ONCE_BRK_ATTRIBUTE3_ID,0) ONCE_BRK_ATTRIBUTE3_ID,
             nvl(ar.ONCE_BRK_ATTRIBUTE4_ID,0) ONCE_BRK_ATTRIBUTE4_ID,
             -- 03.06.20
             ar.report_settlement_rule,
             ar.attribute1 reporting_field1,
             ar.attribute2 reporting_field2,
             ar.attribute3 reporting_field3,
             ar.attribute4 reporting_field4,
             ar.attribute5 reporting_field5
       FROM XXCP_ACCOUNT_RULES ar,
            XXCP_SYS_TARGET_TABLES t
       WHERE ar.model_name_id = pModel_name_id
         AND ar.target_table  = t.transaction_table
         AND ar.source_id     = t.source_id
         AND DECODE(ar.transaction_table,'*',pTransaction_Table, ar.transaction_table) = pTransaction_table
         AND DECODE(ar.transaction_type,'*',pTransaction_Type,ar.transaction_type)     = pTransaction_type
         AND DECODE(ar.transaction_class,'*',pTransaction_Class,ar.transaction_class)  = pTransaction_class
         AND ar.active    = 'Y'
         AND ar.rule_type in ('G','W')
       Order by nvl(ar.rule_sequence,99999);


    vAccounted_Value NUMBER := 0;
    -- NEW
    vReturn_Common_Curr   XXCP_TAX_REGISTRATIONS.common_exch_curr%TYPE;
    vTo_Entered_Currency  XXCP_TAX_REGISTRATIONS.common_exch_curr%TYPE;
    --
    vPAIR_A_VALUE NUMBER;
    vPAIR_A_CURR  VARCHAR2(30);
    vPAIR_A_DATE  VARCHAR2(30);
    vPAIR_A_TYPE  VARCHAR2(30);

    vPAIR_B_VALUE NUMBER;
    vPAIR_B_CURR  VARCHAR2(30);
    vPAIR_B_DATE  VARCHAR2(30);
    vPAIR_B_TYPE  VARCHAR2(30);

    vPAIR_A_Rate NUMBER;
    vPAIR_B_Rate NUMBER;

    vPAIR_A_EXCH_Rate NUMBER;
    vPAIR_B_EXCH_Rate NUMBER;

    vError_Message XXCP_ERRORS.error_message%TYPE;

    vTarget_Exch_Rate_Type  XXCP_TAX_REGISTRATIONS.Exchange_Rate_type%TYPE;
    vTarget_Currency_code   XXCP_TAX_REGISTRATIONS.common_exch_curr%TYPE;
    vCommon_Exch_Curr       XXCP_TAX_REGISTRATIONS.common_exch_curr%TYPE;
    vTrade_currency         XXCP_TAX_REGISTRATIONS.common_exch_curr%TYPE;
    vTax_registration_id    XXCP_TAX_REGISTRATIONS.tax_registration_id%TYPE;
    vBalancing_Segment      XXCP_TARGET_ASSIGNMENTS.Balancing_segment%TYPE;
    vTarget_Assignment_id   XXCP_TARGET_ASSIGNMENTS.Target_assignment_id%TYPE := 0;
    vTarget_Reg_id          XXCP_TARGET_ASSIGNMENTS.reg_id%TYPE;
    vTarget_Set_of_books_id XXCP_TARGET_ASSIGNMENTS.target_set_of_books_id%TYPE;
    vTarget_COA_ID          XXCP_TARGET_ASSIGNMENTS.target_coa_id%TYPE;
    vTarget_Instance_id     XXCP_TARGET_ASSIGNMENTS.target_instance_id%TYPE;
    vTarget_Zero_Flag       XXCP_TARGET_ASSIGNMENTS.zero_flag%TYPE;
    vD1003_Pointers         VARCHAR2(500);

    vExch_A_Error Boolean;
    vExch_B_Error Boolean;

    -- NCM 14/11/2008 Made changes for Gain and Loss Once rule type.
    vOnce_cnt           NUMBER  := 0;
    vOnce_found         BOOLEAN := FALSE;
    vRule_type          varchar2(2);
    vOnce_Break_Value1  varchar2(250);
    vOnce_Break_Value2  varchar2(250);
    vOnce_Break_Value3  varchar2(250);
    vOnce_Break_Value4  varchar2(250);

    vTarget_Rounding    varchar2(1);

  BEGIN

    xxcp_wks.Reset_Gain_Loss_Arrays(cAction);
    xxcp_wks.I1009_ArrayTEMP:= xxcp_wks.I1009_ArrayGL;

    cInternalErrorCode  := 0;

    xxcp_trace.Transaction_Trace( cTrace_id => 11);

    FOR MCRec IN MC(pEntity_Set_Id      => cEntity_Set_Id,
                    pTRX_Qualifier1     => cTRX_Qualifier1,
                    pTRX_Qualifier2     => cTRX_Qualifier2,
                    pTransaction_Set_id => cTransaction_Set_id,
                    pTransaction_Table  => cTransaction_Table,
                    pTransaction_Type   => cTransaction_Type,
                    pTransaction_Class  => cTransaction_Class,
                    pEffective_Date     => cEffective_Date,
                    pTrading_Set_id     => xxcp_global.gCommon(1).current_Trading_Set_id)
     LOOP

        EXIT WHEN cInternalErrorCode <> 0;

         xxcp_trace.Transaction_Trace(cTrace_id     => 12,
                                      cModel_Name   => MCREC.Model_Name,
                                      cModel_Ctl_id => mcrec.Model_ctl_id);

        vTo_Entered_Currency  := cTransaction_Currency;
        vTarget_Type_id       := MCRec.Target_Type_id;

        xxcp_wks.vBase_ArrayGL(26) := MCRec.model_name;
        xxcp_wks.vBase_ArrayGL(57) := MCRec.model_Ctl_id;


        IF ((cBase_SQL_BUILD_ID = mcrec.SQL_Build_id) OR (mcrec.SQL_Build_id = 0)) THEN
          IF cAction IN ('S','O') THEN
            -- 02.05.20a
            xxcp_wks.D1003_ArrayGL := xxcp_wks.D1003_ArrayCH;
            xxcp_wks.D1003_CountGL := xxcp_wks.D1003_CountCH;
          ELSE
            -- 02.05.20a
            xxcp_wks.D1003_ArrayGL := xxcp_wks.D1003_Array;
            xxcp_wks.D1003_CountGL := xxcp_wks.D1003_Count;
          END IF;
        ELSE

          xxcp_wks.D1003_ArrayGL := xxcp_wks.Clear_1003_Dynamic;
          xxcp_wks.D1003_CountGL := 0;

          -- ACCOUNT DYNAMIC SQL
          xxcp_Dynamic_Sql.Non_Configurator(cActivity_Name         => 'Transaction',
                                            cRule_Type             => 'G', -- Rule Type
                                            cSet_of_books_id       => cSet_of_books_id,
                                            cSourceRowid           => cSourceRowid,
                                            cTransaction_id        => cTransaction_Id,
                                            cTransaction_Table     => cTransaction_Table,
                                            -- 02.06.06
                                            cTransaction_class      => cTransaction_class,
                                            cTrading_Set            => xxcp_global.gCommon(1).current_Trading_Set,
                                            cActivity_id           => 1003,
                                            cDynamic_Interface_id  => MCRec.SQL_Build_id,
                                            cOwner                 => cOwner_Tax_Reg_id,
                                            cPartner               => cPartner_Tax_Reg_id,
                                            cKey                   => NULL,
                                            cPrevious_Attribute_id => xxcp_global.gCommon(1).Previous_Attribute_id,
                                            cResults_Pointer       => vD1003_Pointers,
                                            cInternalErrorCode     => cInternalErrorCode);

          xxcp_wks.D1003_ArrayGL := xxcp_wks.DYNAMIC_RESULTS;
          xxcp_wks.D1003_CountGL := xxcp_wks.DYNAMIC_RESULTS_CNT;
          xxcp_wks.Dynamic_Results := xxcp_wks.Clear_Dynamic;

        END IF;

        IF cInternalErrorCode = 0 THEN
          -- ############## Cache Summary Find ################## --
          IF xxcp_wks.BRK_SET_RCD(1).sum_qty > 0 THEN
             Cache_Summary_Get('GL');
          END IF;
          -- ##

          IF xxcp_wks.Max_Accounting > 1 THEN

            vModel_Name    := MCRec.Model_Name;
            vModel_Name_id := MCRec.Model_Name_id;

            --
            -- Formulas
             If cInternalErrorCode = 0 and nvl(mcRec.Formula_Set_Id,0) > 0 then
               gFormula_Pointers_1003 := Null;
               cInternalErrorCode := xxcp_te_formulas.Call_Formula( cSource_id        => xxcp_global.gCommon(1).Source_id,
                                                                    cRule_Type        => 'G',
                                                                    cFormula_Set_id   => mcRec.Formula_Set_Id,
                                                                    cFormula_Pointers => gFormula_Pointers_1003,
                                                                    cDynamic_Array    => xxcp_wks.D1003_ArrayGL);

               Exit when cInternalErrorCode <> 0;
            End If;

            -- NCM 14/11/2008 Made changes for Gain and Loss Once rule type.
            vOnce_found := FALSE;

            FOR ARRec IN AR(pTarget_Type_id     => vTarget_Type_id,
                            pModel_Name_id      => vModel_Name_id,
                            pTransaction_Table  => cTransaction_Table,
                            pTransaction_Type   => cTransaction_Type,
                            pTransaction_Class  => cTransaction_Class) LOOP

              EXIT WHEN cInternalErrorCode <> 0;

              -- NCM 14/11/2008 Set the local rule type for the trace output.
              vRule_type := ARRec.Rule_Type;
              -- Balancing
              vTarget_Rounding := ARRec.Target_Rounding;

              -- Break Points
              vOnce_Break_Value1       := Null;
              vOnce_Break_Value2       := Null;
              vOnce_Break_Value3       := Null;
              vOnce_Break_Value4       := Null;
            
              If ARRec.Rule_Type = 'W' then
                -- New Once Break Values                           
                If ARRec.ONCE_BRK_ATTRIBUTE1_ID > 0 then
                  vOnce_Break_Value1 := xxcp_foundation.Find_DynamicAttribute(ARRec.ONCE_BRK_ATTRIBUTE1_ID,'S');
                End If;
                If ARRec.ONCE_BRK_ATTRIBUTE2_ID > 0 then
                  vOnce_Break_Value2 := xxcp_foundation.Find_DynamicAttribute(ARRec.ONCE_BRK_ATTRIBUTE2_ID,'S');
                End If;
                If ARRec.ONCE_BRK_ATTRIBUTE3_ID > 0 then
                  vOnce_Break_Value3 := xxcp_foundation.Find_DynamicAttribute(ARRec.ONCE_BRK_ATTRIBUTE3_ID,'S');
                End If;
                If ARRec.ONCE_BRK_ATTRIBUTE4_ID > 0 then
                  vOnce_Break_Value4 := xxcp_foundation.Find_DynamicAttribute(ARRec.ONCE_BRK_ATTRIBUTE4_ID,'S');
                End If;
              End If;

              -- NCM 14/11/2008 Made changes for Gain and Loss Once rule type.
              IF ((vRule_type in ('W')) AND (NOT vOnce_found)) THEN
                vOnce_Cnt := nvl(xxcp_wks.ONCE_RCD.COUNT,0);
                FOR k IN 1..vOnce_Cnt LOOP
                 IF xxcp_wks.ONCE_RCD(k).Model_name_id = vModel_Name_id
                   AND xxcp_wks.ONCE_RCD(k).Transaction_Table = cTransaction_Table
                   AND xxcp_wks.ONCE_RCD(k).Transaction_Type = cTransaction_Type
                   AND xxcp_wks.ONCE_RCD(k).Rule_id = ARRec.Rule_id
                   AND nvl(xxcp_wks.ONCE_RCD(k).Break_value1,'*') = nvl(vOnce_Break_Value1,'*')
                   AND nvl(xxcp_wks.ONCE_RCD(k).Break_value2,'*') = nvl(vOnce_Break_Value2,'*')
                   AND nvl(xxcp_wks.ONCE_RCD(k).Break_value3,'*') = nvl(vOnce_Break_Value3,'*')
                   AND nvl(xxcp_wks.ONCE_RCD(k).Break_value4,'*') = nvl(vOnce_Break_Value4,'*')
                   AND xxcp_wks.ONCE_RCD(k).Trading_Set_id    = xxcp_global.gCommon(1).current_Trading_Set_id
                   THEN
                     vOnce_found := TRUE;
                     EXIT;
                 END IF;
                END LOOP;
              END IF;

              -- NCM 14/11/2008 Made changes for Gain and Loss Once rule type.
              IF ((vRule_type = 'W' AND vOnce_found = FALSE) OR (vRule_type != 'W')) THEN

                xxcp_wks.I1009_ArrayGL := xxcp_wks.vBase_ArrayGL;

                vTarget_Type_id          := ARRec.Target_Type_id;
                xxcp_wks.I1009_ArrayTEMP := xxcp_wks.I1009_ArrayGL;

                Get_Tax_Reg_Information(cTarget_Type_id        => vTarget_Type_id,
                                        cOwner_Tax_Reg_id      => cOwner_Tax_Reg_id,
                                        cPartner_Tax_Reg_id    => cPartner_Tax_Reg_id,
                                        cEntity_Type           => ARRec.Entity_type,
                                        cTarget_Type_Match     => McRec.Target_Type_Match,
                                        -- Returned
                                        cTarget_Assignment_id  => vTarget_Assignment_id,
                                        cTarget_Zero_Flag      => vTarget_Zero_Flag,
                                        cReg_id                => vTarget_Reg_id,
                                        cTax_Reg_id            => vTax_registration_id,
                                        cBal_Segment           => vBalancing_Segment,
                                        cExchange_Rate_Type    => vTarget_Exch_Rate_Type,
                                        cCOA_ID                => vTarget_COA_id,
                                        cInstance_ID           => vTarget_Instance_id,
                                        cSOB_ID                => vTarget_Set_of_books_id,
                                        cCurrency_Code         => vTarget_Currency_code,
                                        cCommon_Exch_Curr      => vCommon_Exch_Curr,
                                        cTrade_currency        => vTrade_currency,
                                        cInternalErrorCode     => cInternalErrorCode);

                If cInternalErrorCode = gDo_Not_Do_Rule then
                 cInternalErrorCode := 0;
                Else
        
                   xxcp_trace.Transaction_Trace(cTrace_id           => 21,
                                               cRule_id             => ARRec.Rule_id,
                                               cRule_Name           => ARRec.rule_name,
                                               cTarget_COA_id       => vTarget_COA_id,
                                               cTarget_Instance_id  => vTarget_Instance_id,
                                               cTarget_Suppression  => ARRec.Target_Suppression_Rule,
                                               cHistory_Suppression => ARRec.History_Suppression_Rule
                                              );

                xxcp_wks.I1009_ArrayGL := xxcp_wks.I1009_ArrayTEMP;

                EXIT WHEN cInternalErrorCode <> 0;

                xxcp_wks.I1009_ArrayGL(38) := cTransaction_Table;
                xxcp_wks.I1009_ArrayGL(39) := cTransaction_Type;

                IF ARRec.transaction_class_replacement IS NOT NULL THEN
                  xxcp_wks.I1009_ArrayGL(40) := ARRec.Transaction_Class_Replacement;
                ELSE
                  xxcp_wks.I1009_ArrayGL(40) := cTransaction_class;
                END IF;

                xxcp_wks.I1009_ArrayGL(62) := arrec.rule_category_1;
                xxcp_wks.I1009_ArrayGL(63) := arrec.rule_category_2;
                xxcp_wks.I1009_ArrayGL(96) := arrec.entity_type;
                xxcp_wks.I1009_ArrayGL(145):= arrec.target_suppression_rule; 
                xxcp_wks.I1009_ArrayGL(146):= arrec.history_suppression_rule; 
                
                vPair_A_Value := xxcp_foundation.find_dynamicattribute(arrec.Pair_A_value_attribute_id,'G');
                vPair_A_Curr  := xxcp_foundation.find_dynamicattribute(arrec.Pair_A_curr_attribute_id,'G');
                vPair_A_Date  := xxcp_foundation.find_dynamicattribute(arrec.Pair_A_exch_date_attribute_id,'G');
                vPair_A_Type  := xxcp_foundation.find_dynamicattribute(arrec.Pair_A_exch_type_attribute_id,'G');
                vPair_B_Value := xxcp_foundation.find_dynamicattribute(arrec.Pair_B_value_attribute_id,'G');
                vPair_B_Curr  := xxcp_foundation.find_dynamicattribute(arrec.Pair_B_curr_attribute_id,'G');
                vPair_B_Date  := xxcp_foundation.find_dynamicattribute(arrec.Pair_B_exch_date_attribute_id,'G');
                vPair_B_Type  := xxcp_foundation.find_dynamicattribute(arrec.Pair_B_exch_type_attribute_id,'G');

                vPair_A_Rate  := xxcp_foundation.find_dynamicattribute(arrec.pair_a_exch_rate_attribute_id,'G');
                vPair_B_Rate  := xxcp_foundation.find_dynamicattribute(arrec.pair_b_exch_rate_attribute_id,'G');

                IF ((vTarget_Currency_code = vPAIR_A_CURR) and (vPAIR_A_CURR = vPAIR_B_CURR)) THEN
                  -- There is no gain and loss on records
                  -- where the 1st Currency, the 2nd current and target currencies
                  -- are the same!. JRE 27FEB2006

                  xxcp_trace.Transaction_Trace(cTrace_id             => 15,
                                               cTransaction_Currency => cTransaction_Currency,
                                               cTarget_Currency      => vTarget_Currency_code);

                  EXIT WHEN cTransaction_Currency = vTarget_Currency_code;
                END IF;


                IF ((vPair_A_Date IS NULL OR vPair_A_Type IS NULL) AND (vPair_A_Rate is null)) THEN
                  vExch_A_Error := TRUE;
                ELSE
                  vExch_A_Error := FALSE;
                END IF;

                -- Check that Gain and Loss pair all have values
                IF ((vPair_A_Date IS NULL OR vPair_A_Type IS NULL) OR (vExch_A_Error = TRUE)) THEN
                      cInternalErrorCode := 10531;
                      xxcp_foundation.FndWriteError(cInternalErrorCode,
                                '1st Trx = '||
                                'Value <'||vPAIR_A_VALUE||'> '||
                                'Currency <'||vPAIR_A_CURR||'> '||
                                'Date <'||vPAIR_A_DATE||'> '||
                                'Type <'||vPAIR_A_TYPE||'> '||
                                'Rate <'||vPAIR_A_RATE||'>');
                      EXIT;
                END IF;

                IF ((vPair_B_Date IS NULL OR vPair_B_Type IS NULL) AND (vPair_B_Rate is null)) THEN
                  vExch_B_Error := TRUE;
                ELSE
                  vExch_B_Error := FALSE;
                END IF;

                IF (vPair_B_Value IS NULL OR vPair_B_CURR IS NULL OR vExch_B_Error = TRUE) THEN
                      cInternalErrorCode := 10532;
                      xxcp_foundation.FndWriteError(cInternalErrorCode,
                                '2nd Trx = '||
                                'Value <'||vPAIR_B_VALUE||'> '||
                                'Currency <'||vPAIR_B_CURR||'> '||
                                'Date <'||vPAIR_B_DATE||'> '||
                                'Type <'||vPAIR_B_TYPE||'> '||
                                'Rate <'||vPAIR_B_RATE||'>');
                      EXIT;
                END IF;

                -- End Gain and Lost value check

                IF (vPAIR_A_CURR <> vTarget_Currency_code) OR (vPAIR_B_CURR <> vTarget_Currency_code) THEN
                  --
                  -- If the PAIR A does not equal the Set of Books Currency
                  -- then work out the exchange Rate.
                  --
                  IF (vPAIR_A_CURR <> vTarget_Currency_code) THEN

                    IF nvl(vPAIR_A_RATE,0) <> 0 then -- Use Given Exchange Rate
                      vPAIR_A_EXCH_Rate := vPAIR_A_RATE;
                    Else
                      -- PAIR A SIDE
                      vPAIR_A_EXCH_Rate := xxcp_foundation.Currency_Conversions(vPAIR_A_CURR -- From
                                                                             ,'1st Trx'
                                                                             ,vTarget_Currency_code -- To
                                                                             ,cOwner_Common_Exch_Curr
                                                                             ,vPAIR_A_DATE
                                                                             ,vPAIR_A_TYPE
                                                                             ,10526
                                                                             ,vReturn_Common_Curr -- Common Exch.
                                                                             ,cInternalErrorCode);
                    End If;
                  ELSE
                    --
                    -- If The PAIR A Currency is the same as the Set Of Books Currency
                    vPAIR_A_EXCH_Rate := 1;
                  END IF;

                  EXIT WHEN cInternalErrorCode <> 0;
                  --
                  -- If the PAIR A does not equal the Set of Books Currency
                  -- then work out the exchange Rate.
                  --
                  IF vPAIR_B_CURR <> vTarget_Currency_code THEN
                    -- PAIR B SIDE
                    IF nvl(vPAIR_B_RATE,0) <> 0 then -- Use Given Exchange Rate
                      vPAIR_B_EXCH_Rate := vPAIR_B_RATE;
                    Else
                      vPAIR_B_EXCH_Rate := xxcp_foundation.Currency_Conversions(vPAIR_B_CURR -- From
                                                                             ,'2nd Trx'
                                                                             ,vTarget_Currency_code -- To
                                                                             ,cOwner_Common_Exch_Curr
                                                                             ,vPAIR_B_DATE
                                                                             ,vPAIR_B_TYPE
                                                                             ,10527
                                                                             ,vReturn_Common_Curr -- Common Exch.
                                                                             ,cInternalErrorCode);
                    End If;
                  ELSE
                    --
                    -- If The PAIR A Currency is the same as the Set Of Books Currency
                    vPAIR_B_EXCH_Rate := 1;
                  END IF;

                END IF;

                IF cInternalErrorCode = 0 THEN

                  vAccounted_Value := ROUND(nvl(vPAIR_A_VALUE, 0) *
                                            VPAIR_A_EXCH_RATE,
                                            xxcp_foundation.Fnd_CurrencyPrecision(vTarget_Currency_code,2,MCREC.Extended_Precision) ) -
                                      ROUND(nvl(vPAIR_B_VALUE, 0) *
                                            vPAIR_B_EXCH_Rate,
                                            xxcp_foundation.Fnd_CurrencyPrecision(vTarget_Currency_code,2, MCREC.Extended_Precision));

                  -- Apply the change of sign
                  IF ARRec.Change_Sign = 'Y' AND vAccounted_Value IS NOT NULL THEN
                    vAccounted_Value := vAccounted_Value * -1;
                  END IF;
                END IF;

                -- For Gain and loss transactions Entered values MUST be zero!!!
                xxcp_wks.I1009_ArrayGL(21) := 0;
                xxcp_wks.I1009_ArrayGL(81) := 0;
                xxcp_wks.I1009_ArrayGL(22) := cTransaction_Currency; -- Entered Currency

                -- Entered Currency check
                 If vPAIR_A_CURR <> vTarget_Currency_code then
                   xxcp_wks.I1009_ArrayGL(22) := vPAIR_A_CURR;
                 ElsIf vPAIR_B_CURR <> vTarget_Currency_code then
                   xxcp_wks.I1009_ArrayGL(22) := vPAIR_B_CURR;
                 Else
                   cInternalErrorCode := 12442; -- Error. Entered Currency can not be the same as the Target Currency
                 End If;

                xxcp_wks.I1009_ArrayGL(23) := nvl(to_char(vAccounted_Value),0);

                xxcp_wks.I1009_ArrayGL(27) := ARRec.Rule_name; -- Rule Name
                xxcp_wks.I1009_ArrayGL(29) := cTransaction_Currency;
                xxcp_wks.I1009_ArrayGL(33) := to_char(vTarget_Set_of_books_id);

                xxcp_wks.I1009_ArrayGL(37) := NULL;

                xxcp_wks.I1009_ArrayGL(53) := NULL; -- Entered Exchange Rate
                xxcp_wks.I1009_ArrayGL(54) := NULL; -- Accounted Exchange Rate
                xxcp_wks.I1009_ArrayGL(56) := vTarget_Currency_code;

                EXIT WHEN cInternalErrorCode <> 0;

                xxcp_wks.I1009_ArrayTEMP := xxcp_wks.I1009_ArrayGL;

                Get_Segment_Rules(cAction                => 'G',
                                  cRule_id               => ARRec.Rule_id,
                                  cRule_Name             => ARRec.Rule_Name,
                                  cChart_of_accounts_id  => vTarget_COA_id,
                                  cInstance_id           => vTarget_Instance_id,
                                  cEnforce_Accounting    => ARRec.Enforce_Accounting,
                                  cTarget_Cross_Validation => ARRec.Cross_Validation,
                                  cEffective_Date        => cEffective_Date,
                                  -- NCM 02.05.08
                                  cEntity_type           => ARRec.Entity_Type,
                                  cInternalErrorCode     => cInternalErrorCode);

                xxcp_wks.I1009_ArrayGL := xxcp_wks.I1009_ArrayTEMP;

                EXIT WHEN cInternalErrorCode <> 0;
                
                -- Make Row
                Column_Rules(cRule_type              => vRule_type,
                             cRule_id                => ARRec.Rule_id,
                             cModel_Ctl_id           => mcrec.Model_Ctl_id,
                             cTarget_Table           => ARRec.Target_Table,
                             cAttribute_id           => cAttribute_id,
                             cInterface_ID           => cInterface_id,
                             cTarget_Set_of_books_id => vTarget_Set_of_books_id,
                             cTransaction_Class      => cTransaction_Class,
                             cTarget_Type_id         => vTarget_Type_id,
                             cTarget_Record_Status   => ARRec.Target_Record_Status,
                             cTarget_Rounding        => vTarget_rounding,
                             cRound_Suppressed_Rules => ARRec.Round_Suppressed_Rules,
                             cTarget_Reg_id          => vTarget_Reg_id,
                             cNo_Rounding_Rule       => ARRec.No_Rounding_Rule,
                             cBalancing_Segment      => vBalancing_Segment,
                             cRounding_Group         => ARRec.Rounding_Group,
                             cRule_Entity_Type       => ARRec.Entity_type,
                             cTarget_Assignment_id   => vTarget_Assignment_id,
                             cTarget_Zero_Flag       => vTarget_Zero_Flag,
                             cTax_Registration_id    => vTax_registration_id,
                             cTarget_Instance_id     => vTarget_Instance_id,
                             cTarget_COA_id          => vTarget_COA_id,
                             cZero_Suppression_Rule  => ARRec.Zero_Suppression_Rule,
                             cCost_Plus_Rule         => ARRec.Cost_Plus_Rule,
                             cExtended_Values        => ARRec.Extended_Values,
                             cEntered_Amounts        => ARRec.Entered_Amounts,
                             cAccounted_Amounts      => ARRec.Accounted_Amounts,
                             -- 03.06.20
                             cSettlement_rule        => ARRec.Report_Settlement_Rule,
                             cReporting_field1       => ARRec.Reporting_Field1,
                             cReporting_field2       => ARRec.Reporting_Field2,
                             cReporting_field3       => ARRec.Reporting_Field3,
                             cReporting_field4       => ARRec.Reporting_Field4,
                             cReporting_field5       => ARRec.Reporting_Field5 
                             );

                IF cInternalErrorCode <> 0 THEN
                  xxcp_foundation.FndWriteError(cInternalErrorCode,vError_Message);
                END IF;

                 --! ************************************************************************************
                 --!        ONCE RULE
                 --! ************************************************************************************
                 -- NCM 14/11/2008 Made changes for Gain and Loss Once rule type.
                 If vRule_type = 'W' then
                   xxcp_wks.ONCE_RCD(vOnce_Cnt+1).Model_Name_id     := vModel_Name_id;
                   xxcp_wks.ONCE_RCD(vOnce_Cnt+1).Transaction_Table := cTransaction_Table;
                   xxcp_wks.ONCE_RCD(vOnce_Cnt+1).Transaction_Type  := cTransaction_Type;
                   xxcp_wks.ONCE_RCD(vOnce_Cnt+1).Rule_id           := ARRec.Rule_id;
                   xxcp_wks.ONCE_RCD(vOnce_Cnt+1).Trading_Set_id    := xxcp_global.gCommon(1).current_Trading_Set_id;
                         xxcp_wks.ONCE_RCD(vOnce_Cnt+1).Break_Value1      := vOnce_Break_Value1;
                         xxcp_wks.ONCE_RCD(vOnce_Cnt+1).Break_Value2      := vOnce_Break_Value2;
                         xxcp_wks.ONCE_RCD(vOnce_Cnt+1).Break_Value3      := vOnce_Break_Value3;
                         xxcp_wks.ONCE_RCD(vOnce_Cnt+1).Break_Value4      := vOnce_Break_Value4;

                 End If;
                End If; --- Do not Do rule
              end if; -- end of once rule.

              EXIT WHEN cInternalErrorCode <> 0;
            END LOOP; -- End Account Rules Loop

          ELSE
            IF cAction = 'B' THEN
             xxcp_foundation.Set_InternalErrorCode(cInternalErrorCode, 10528);
            ELSE
             xxcp_foundation.Set_InternalErrorCode(cInternalErrorCode, 10529);
            END IF;
          END IF;
        END IF;

        -- NCM 14/11/2008 Made changes for Gain and Loss Once rule type.
        IF ((vRule_type = 'W' AND vOnce_found = FALSE) OR (vRule_type != 'W')) THEN
          -- Gain Loss output
          IF xxcp_global.Trace_on = 'Y' THEN
              xxcp_trace.Transaction_Trace(cTrace_id               => 16,
                                           cPAIR_A_VALUE           => vPAIR_A_VALUE,
                                           cPAIR_A_CURR            => vPAIR_A_CURR,
                                           cPAIR_A_DATE            => vPAIR_A_DATE,
                                           cPAIR_A_TYPE            => vPAIR_A_TYPE,
                                           cPAIR_A_RATE            => vPAIR_A_RATE,
                                           cPAIR_A_EXCH_RATE       => vPAIR_A_EXCH_Rate,
                                           cTarget_Currency        => vTarget_Currency_code,
                                           cOwner_Common_Exch_Curr => cOwner_Common_Exch_Curr,
                                           cPAIR_B_VALUE           => vPAIR_B_VALUE,
                                           cPAIR_B_CURR            => vPAIR_B_CURR,
                                           cPAIR_B_DATE            => vPAIR_B_DATE,
                                           cPAIR_B_TYPE            => vPAIR_B_TYPE,
                                           cPAIR_B_RATE            => vPAIR_B_RATE,
                                           cPAIR_B_EXCH_RATE       => vPAIR_B_EXCH_Rate
                                           );

          END IF;
        end if;

        -- End Create Interface Row
      END LOOP;

  EXCEPTION WHEN OTHERS THEN
    cInternalErrorCode := 12492;
    xxcp_foundation.FndWriteError(cInternalErrorCode,
                              ' 1st = From Curr. <'||vPAIR_A_CURR||'> '||
                              'To Curr. <'||vTarget_Currency_code||'> '||
                              'Common Curr. <'||cOwner_Common_Exch_Curr||'> '||
                              'Date <'||vPAIR_A_DATE||'>'||
                              'Type <'||vPAIR_A_TYPE||'> Rate <'||vPAIR_A_RATE||'>');

    xxcp_foundation.FndWriteError(cInternalErrorCode,
                              ' 2nd = From Curr. <'||vPAIR_B_CURR||'> '||
                              'To Curr. <'||vTarget_Currency_code||'> '||
                              'Common Curr. <'||cOwner_Common_Exch_Curr||'> '||
                              'Date <'||vPAIR_B_DATE||'>'||
                              'Type <'||vPAIR_B_TYPE||'> Rate <'||vPAIR_B_RATE||'>');



  END Process_Gain_Loss;

  -- !! ***********************************************************************************************************
  -- !!
  -- !!                                Process_Local
  -- !!
  -- !! ***********************************************************************************************************
  PROCEDURE Process_Local(cSet_of_books_id         IN NUMBER,
                          cSourceRowid             IN ROWID,
                          cCreate_Local_Trx        IN VARCHAR2,
                          cTransaction_id          IN NUMBER,
                          cTransaction_Table       IN VARCHAR2,
                          cTransaction_Type        IN VARCHAR2,
                          cTransaction_Class       IN VARCHAR2,
                          cQuantity                IN NUMBER,
                          --
                          cEntity_Set_id           IN NUMBER,
                          cTransaction_set_id      IN NUMBER,
                          --
                          cTRX_Qualifier1          IN VARCHAR2,
                          cTRX_Qualifier2          IN VARCHAR2,
                          --
                          cED_QUALIFIER1           IN VARCHAR2,
                          cED_QUALIFIER2           IN VARCHAR2,
                          cED_QUALIFIER3           IN VARCHAR2,
                          cED_QUALIFIER4           IN VARCHAR2,
                          --
                          cTransaction_Currency    IN VARCHAR2,
                          cTransaction_Quantity    IN NUMBER,
                          cTransaction_Date        IN DATE,
                          cOwner_Tax_Reg_id        IN NUMBER,
                          cPartner_Tax_Reg_id      IN NUMBER,
                          cOwner_Common_Exch_Curr  IN VARCHAR2,
                          cExchange_Date           IN DATE,
                          cOwner_Legal_Currency    IN VARCHAR2,
                          cPartner_Legal_Currency  IN VARCHAR2,
                          cAttribute_id            IN NUMBER,
                          cInterface_ID            IN NUMBER,
                          cIC_Tax_Rate             IN NUMBER,
                          cIC_Adjustment_Rate      IN NUMBER,
                          --
                          cParent_Entered_Amount   IN NUMBER,
                          cParent_Entered_Currency IN VARCHAR2,
                          cInternalErrorCode       IN OUT NOCOPY NUMBER) IS

    -- Model Control

    CURSOR MC(pTransaction_Table IN VARCHAR2, pTransaction_Type IN VARCHAR2
            , pTRX_Qualifier1 IN VARCHAR2, pTRX_Qualifier2 IN VARCHAR2
            , pTransaction_Set_id IN NUMBER, pTransaction_Date IN DATE, pTrading_Set_id IN NUMBER) IS
      SELECT /*+ Index(smn) */
             mc.SQL_build_Id,
             smn.Model_Name,
             mc.Model_name_id,
             mc.MC_QUALIFIER1,
             mc.MC_QUALIFIER2,
             mc.Account_PRICE_METHOD_ID AC_Pricing_id,
             mc.Account_PRICE_METHOD_Type AC_Pricing_Method_Type,
             mc.Target_type_id,
             Decode(mc.ed_enabled,'Y','O',mc.ed_enabled) ed_enabled,
             mc.model_ctl_id,
             mc.Acc_Conversions,
             mc.exchange_rate_type,
             smn.extended_precision,
             nvl(mc.acc_exchange_rate_type_id,0) acc_exchange_rate_type_id,
             nvl(mc.acc_exchange_date_id,0)      acc_exchange_date_id,
             nvl(mc.acc_owner_exch_rate_id,0)    acc_owner_exch_rate_id,
             nvl(mc.acc_partner_exch_rate_id,0)  acc_partner_exch_rate_id,
             mc.formula_set_id,
             mc.trade_curr_indicator
        FROM xxcp_model_ctl mc,
             xxcp_sys_model_names smn,
             xxcp_sys_qualifier_groups mq
       WHERE mq.source_id          = xxcp_global.gCommon(1).Source_id
         AND mc.model_ctl_group_id = mq.qualifier_group_id
         AND mq.qualifier_usage    = 'M'
         AND mc.model_name_id      = smn.model_name_id
         AND mc.Source_id          = smn.source_id
         AND mc.Entity_set_id      = 0
         AND nvl(mc.Transaction_set_id, pTransaction_set_id) = pTransaction_set_id
         AND nvl(mc.MC_Qualifier1, '~null~') =
             DECODE(mc.MC_Qualifier1,
                    '*',
                    nvl(mc.MC_Qualifier1, '~null~'),
                    nvl(xxcp_foundation.MC_DynamicAttribute(mq.Qualifier1_id), '~null~'))
         AND nvl(mc.MC_Qualifier2, '~null~') =
             DECODE(mc.MC_Qualifier2,
                    '*',
                    nvl(mc.MC_Qualifier2, '~null~'),
                    nvl(xxcp_foundation.MC_DynamicAttribute(mq.Qualifier2_id), '~null~'))
         AND nvl(mc.MC_Qualifier3, '~null~') =
             DECODE(mc.MC_Qualifier3,
                    '*',
                    nvl(mc.MC_Qualifier3, '~null~'),
                    nvl(xxcp_foundation.MC_DynamicAttribute(mq.Qualifier3_id), '~null~'))
         AND nvl(mc.MC_Qualifier4, '~null~') =
             DECODE(mc.MC_Qualifier4,
                    '*',
                    nvl(mc.MC_Qualifier4, '~null~'),
                    nvl(xxcp_foundation.MC_DynamicAttribute(mq.Qualifier4_id), '~null~'))
         -- Trx Qualifiers
         AND nvl(mc.TRX_Qualifier1, '~null~') =
             DECODE(mc.TRX_Qualifier1,
                    '*',
                    nvl(mc.TRX_Qualifier1, '~null~'),
                    nvl(pTRX_Qualifier1, '~null~'))
         AND nvl(mc.TRX_Qualifier2, '~null~') =
             DECODE(mc.TRX_Qualifier2,
                    '*',
                    nvl(mc.TRX_Qualifier2, '~null~'),
                    nvl(pTRX_Qualifier2, '~null~'))
         --
         AND mc.Active = 'Y'
         AND mc.Trading_Set_Id  = pTrading_Set_id
         AND mc.Model_name_id = ANY
       (SELECT ar.Model_name_id
                FROM XXCP_ACCOUNT_RULES ar
               WHERE ar.source_id = mc.source_id
                 AND ar.rule_type = 'L'
                 AND DECODE(ar.transaction_table,'*',pTransaction_Table,ar.transaction_table) = pTRANSACTION_TABLE
                 AND ar.active = 'Y'
                 )
         AND pTransaction_Date BETWEEN
             nvl(mc.Effective_From_Date, TO_DATE('01-JAN-1980','DD-MON-YYYY')) AND
             nvl(mc.Effective_To_Date, TO_DATE('31-DEC-2999','DD-MON-YYYY'))
       ORDER BY smn.model_sequence, mc.sql_build_id;

    vModel_Name     XXCP_SYS_MODEL_NAMES.model_name%TYPE;
    vModel_Name_ID  XXCP_SYS_MODEL_NAMES.model_name_id%TYPE;
    vTarget_Type_id XXCP_SYS_TARGET_TYPES.target_type_id%TYPE;

    -- Transaction Rules

    CURSOR AR(pTarget_Type_id IN NUMBER, pModel_Name_id IN NUMBER, pTransaction_Table IN VARCHAR2) IS
      SELECT ar.rule_name,
             ar.entity_type,
             ar.rule_value_name,
             ar.change_sign,
             ar.rule_id,
             ar.Target_Table,
             ar.entered_value_attribute_id,
             ar.entered_curr_attribute_id,
             nvl(ar.target_type_id, pTarget_Type_id) Target_Type_id,
             ar.transaction_class_replacement,
             ar.transaction_table,
             ar.transaction_class,
             ar.transaction_type,
             --
             t.short_description Target_Record_Status,
             decode(ar.EXCLUDE_FROM_BALANCING,'Y','N',t.target_rounding) Target_Rounding,
             t.round_suppressed_rules,
             nvl(t.enforce_accounting, 'Y') Enforce_accounting,
             nvl(t.Cross_validation, 'N') Target_Cross_validation,
             ar.rule_category_1, ar.rule_category_2,
             ar.Rounding_Group,
             ar.No_Rounding No_Rounding_Rule,
             nvl(ar.Zero_Suppression_rule,'T') Zero_Suppression_rule,
             ar.cost_plus_rule,
             -- From Target Table
             t.Extended_Values,
             t.Entered_Amt_Bal Entered_Amounts,
             t.Accounted_Amt_Bal Accounted_Amounts,
             ar.ACCOUNTED_VALUE_ATTRIBUTE_ID,
             ar.ACCOUNTED_CURR_ATTRIBUTE_ID,
             nvl(ar.Target_Suppression_Rule,'N') Target_Suppression_Rule,
             nvl(ar.History_Suppression_Rule,'N') History_Suppression_Rule,
             -- 03.06.20
             ar.report_settlement_rule,
             ar.attribute1 reporting_field1,
             ar.attribute2 reporting_field2,
             ar.attribute3 reporting_field3,
             ar.attribute4 reporting_field4,
             ar.attribute5 reporting_field5
        FROM XXCP_ACCOUNT_RULES ar, XXCP_SYS_TARGET_TABLES t
       WHERE model_name_id = pModel_Name_ID
         AND ar.rule_type = 'L'
         AND ar.target_table = t.transaction_table
         AND ar.active = 'Y'
         AND ar.source_id = t.source_id
         AND DECODE(ar.transaction_table,'*',pTransaction_Table,ar.transaction_table) = pTRANSACTION_TABLE
       Order by nvl(ar.rule_sequence,99999), Rule_id;

    vMC_QUALIFIER1             VARCHAR2(250);
    vMC_QUALIFIER2             VARCHAR2(250);
    vMC_Qualifier3             VARCHAR2(250);
    vMC_Qualifier4             VARCHAR2(250);

    vError_Message             XXCP_ERRORS.error_message%TYPE;
    -- NEW
    vIC_Adjustment_Rate        XXCP_TRANSACTION_ATTRIBUTES.adjustment_rate%TYPE;
    vModel_Entered_Amount      NUMBER;
    vModel_Entered_Currency    XXCP_TAX_REGISTRATIONS.legal_currency%TYPE;
    -- To
    vTo_Entered_Currency       XXCP_TAX_REGISTRATIONS.legal_currency%TYPE;
    -- From
    vFrom_Entered_Value        NUMBER;
    vFrom_Entered_Currency     xxcp_tax_registrations.legal_currency%TYPE;
    vFrom_Exchange_Rate_Type   xxcp_tax_registrations.exchange_rate_type%TYPE;
    --
    vEntered_Source            VARCHAR2(5);
    vPM_Entered_Exch_Rate_Type xxcp_tax_registrations.exchange_rate_type%TYPE;

    vExchange_Date             DATE;
    vED_Tax_Reg_id             xxcp_tax_registrations.tax_registration_id%type;
    vTarget_Exch_Rate_Type     XXCP_TAX_REGISTRATIONS.exchange_rate_type%TYPE;
    vTarget_Exch_Date          Date;
    vTarget_Reg_id             XXCP_TARGET_ASSIGNMENTS.reg_id%TYPE;
    vTarget_Set_of_books_id    XXCP_TARGET_ASSIGNMENTS.target_set_of_books_id%TYPE;
    vTarget_COA_ID             XXCP_TARGET_ASSIGNMENTS.target_coa_id%TYPE;
    vTarget_Instance_id        XXCP_TARGET_ASSIGNMENTS.target_instance_id%TYPE;
    vTarget_Zero_Flag          XXCP_TARGET_ASSIGNMENTS.zero_flag%TYPE;
    vTarget_Currency_code      XXCP_TAX_REGISTRATIONS.legal_currency%TYPE;
    vCommon_Exch_Curr          XXCP_TAX_REGISTRATIONS.legal_currency%TYPE;
    vTrade_currency            XXCP_TAX_REGISTRATIONS.legal_currency%TYPE;
    cOwner_Trade_Currency      XXCP_TAX_REGISTRATIONS.legal_currency%TYPE;
    cPartner_Trade_Currency    XXCP_TAX_REGISTRATIONS.legal_currency%TYPE;
    vTax_registration_id       XXCP_TAX_REGISTRATIONS.tax_registration_id%TYPE;
    vTarget_Assignment_id      XXCP_TARGET_ASSIGNMENTS.target_assignment_id%TYPE := 0;
    vFrom_Accounted_Value      NUMBER;
    vFrom_Accounted_Currency   VARCHAR2(15);
    vBalancing_Segment         NUMBER(4);
    vReplace_Trx_Date          xxcp_entity_documents.replace_trx_date%TYPE;
    vBatch_Source_Name         xxcp_entity_documents.trx_number_source%TYPE;
    vCust_Trx_Type_id          xxcp_entity_documents.trx_number_type_id%TYPE;
    vED_Trx_Type_Name          xxcp_entity_documents.trx_number_type%type;
    vTo_Entered_Amount         NUMBER  := 0;
    vTo_Accounted_Amount       NUMBER  := 0;
    vProcess_Switch            BOOLEAN;
    vPerferred_Exchange_Rate   NUMBER := 0;
    vPrice_Method_Calc_Value   Number;
    
    vTarget_rounding  varchar2(1);

  BEGIN

    xxcp_wks.Reset_Local_Arrays;
    xxcp_wks.I1009_ArrayTEMP:= xxcp_wks.I1009_ArrayLC;

    xxcp_trace.Transaction_Trace(cTrace_id => 17);

    vFrom_Entered_Currency := cTransaction_Currency;

    cInternalErrorCode         := 0;

    IF substr(cCreate_Local_Trx,1,1) <> 'N' THEN

      IF (cCreate_Local_Trx = 'Y') THEN
        cInternalErrorCode := 10417;
      END IF;

      FOR MCRec IN MC(cTransaction_Table,
                      cTransaction_Type,
                      cTRX_Qualifier1,
                      cTRX_Qualifier2,
                      cTransaction_Set_id,
                      cTransaction_Date,
                      xxcp_global.gCommon(1).current_Trading_Set_id)
      LOOP

        IF cInternalErrorCode = 10417 THEN
          cInternalErrorCode := 0;
        END IF;

        EXIT WHEN cInternalErrorCode <> 0;

        vEntered_Source            := NULL;
        vPrice_Method_Calc_Value   := Null;
        vExchange_Date             := cExchange_Date;
        vTarget_Type_id            := MCRec.Target_Type_id;
        vTo_Entered_Currency       := cTransaction_Currency;
        vModel_Name                := MCRec.Model_Name;
        vModel_Name_ID             := MCRec.Model_Name_ID;
        xxcp_wks.vBase_ArrayLC(26) := MCrec.Model_name;
        xxcp_wks.vBase_ArrayLC(57) := MCrec.Model_Ctl_id;

        xxcp_trace.Transaction_Trace(cTrace_id     => 18,
                                     cModel_Name   => MCRec.Model_Name,
                                     cModel_Ctl_id => MCRec.Model_Ctl_id
                                     );

        -- ############## Cache Summary Find ################## --
        IF ((xxcp_wks.BRK_SET_RCD(1).sum_qty > 0) and (gCacheSummaries = 'Y')) THEN
         Cache_Summary_Get('L');
        END IF;

        --
        -- TRADE INDICATOR LOGIC
        --
        IF cInternalErrorCode = 0 THEN
          IF xxcp_wks.Max_Configuration > 1 THEN

            vModel_Entered_Amount := NULL;
            --
            -- Formulas
             If cInternalErrorCode = 0 and nvl(mcRec.Formula_Set_Id,0) > 0 then
               gFormula_Pointers_1003 := Null;
               cInternalErrorCode := xxcp_te_formulas.Call_Formula( cSource_id        => xxcp_global.gCommon(1).Source_id,
                                                                    cRule_Type        => 'L',
                                                                    cFormula_Set_id   => mcRec.Formula_Set_Id,
                                                                    cFormula_Pointers => gFormula_Pointers_1003,
                                                                    cDynamic_Array    => xxcp_wks.D1003_ArrayLC);

               Exit when cInternalErrorCode <> 0;
            End If;

            IF MCRec.AC_Pricing_id IS NOT NULL THEN



              xxcp_te_values.Pricing(cAction                  => 'L',
                                     cRule_Type               => 'L',
                                     cSet_of_books_id         => cSet_of_books_id,
                                     cTransaction_Table       => cTransaction_Table,
                                     cTransaction_id          => cTransaction_id,
                                     cSourceRowid             => cSourceRowid,
                                     cTransaction_Date        => cTransaction_Date,
                                     cExchange_Date           => cExchange_Date,
                                     cQuantity                => cQuantity,
                                     cOwner_Tax_Reg_id        => cOwner_Tax_Reg_id,
                                     cPartner_Tax_Reg_id      => cPartner_Tax_Reg_id,
                                     cPrice_Method_id         => mcrec.AC_Pricing_ID,
                                     cPrice_Method_Type       => mcRec.AC_Pricing_Method_Type,
                                     cIC_Adjustment_Rate      => cIC_Adjustment_Rate,
                                     cModel_Name              => mcrec.Model_Name,
                                     cModel_Ctl_id            => mcrec.Model_ctl_id,
                                     cModel_Curr_Indicator    => mcrec.Trade_Curr_Indicator,
                                     --
                                     cTransaction_Currency    => cTransaction_Currency,
                                     cOwner_Legal_Currency    => cOwner_Legal_Currency,
                                     cPartner_Legal_Currency  => cPartner_Legal_Currency,
                                     cOwner_Trade_Currency    => cOwner_Trade_Currency,
                                     cPartner_Trade_Currency  => cPartner_Trade_Currency,
                                     -- OUT
                                     cAdjustment_Rate         => vIC_Adjustment_Rate,
                                     cTo_Entered_Currency     => vTo_Entered_Currency,
                                     cPrice_Method_Value      => vModel_Entered_Amount,
                                     cPrice_Method_Curr       => vModel_Entered_Currency,
                                     cPrice_Method_Exch_Type  => vPM_Entered_Exch_Rate_Type,
                                     cPrice_Method_Calc_Value => vPrice_Method_Calc_Value,
                                     cPrice_Exchange_Date     => vExchange_Date, -- Exchange Date can be modified
                                     cInternalErrorCode       => cInternalErrorCode);

              xxcp_wks.vBase_ArrayLC(135) := vPrice_Method_Calc_Value;

              IF nvl(cInternalErrorCode, 0) <> 0 THEN
                xxcp_foundation.FndWriteError(10308, 'Model Control id <' || TO_CHAR(mcrec.Model_ctl_id) || '> ');
              END IF;
            ELSE
              -- From Transaction Attributes
              vIC_Adjustment_Rate  := cIC_Adjustment_Rate;
            END IF;

            IF vModel_Entered_Amount IS NULL THEN
              vModel_Entered_Amount   := cParent_Entered_Amount;
              vModel_Entered_Currency := cParent_Entered_Currency;
              xxcp_trace.Transaction_Trace(cTrace_id  => 19);
            ELSE
              vEntered_Source := 'PM';
              xxcp_trace.Transaction_Trace(cTrace_id  => 20);
            END IF;

            IF (MCRec.ed_enabled in ('O','P')) AND (cInternalErrorCode = 0) THEN
              -- Entity Document can be by Owner or Partner.
              If mcrec.ed_enabled = 'P' then
                vED_Tax_Reg_id := cPartner_Tax_Reg_id;
              Else
                vED_Tax_Reg_id := cOwner_Tax_Reg_id;
              End If;

              -- Get the batch source
              Get_Entity_Document(cTaxRegId          => vED_Tax_Reg_id,
                                  cTarget_Type_id    => MCRec.Target_Type_id,
                                  cModel_Name_id     => MCRec.Model_Name_id,
                                  cED_Qualifier1     => cED_Qualifier1,
                                  cED_Qualifier2     => cED_Qualifier2,
                                  cED_Qualifier3     => cED_Qualifier3,
                                  cED_Qualifier4     => cED_Qualifier4,
                                  cEffective_Date    => cTransaction_Date,
                                  cBatch_Source_Name => vBatch_Source_Name,
                                  cCust_Trx_Type_id  => vCust_Trx_Type_id,
                                  cReplace_Trx_Date  => vReplace_Trx_Date,
                                  cED_Trx_Type_Name  => vED_Trx_Type_Name,
                                  cInternalErrorCode => cInternalErrorCode);

              -- Put values into the internal array
              xxcp_wks.vBase_ArrayLC(46) := vBatch_Source_Name;
              xxcp_wks.vBase_ArrayLC(48) := vReplace_Trx_Date;
              xxcp_wks.vBase_ArrayLC(47) := vCust_Trx_Type_id;
              xxcp_wks.vBase_ArrayLC(89) := vED_Trx_Type_Name;

            END IF;

            EXIT WHEN cInternalErrorCode <> 0;
            --
            -- Don't Create Local Rows if Assignment Control says so
            -- but always create IC Rows if a rule_type 'C' is found.
            -- ARL = Account Rule Level
            --

            IF cCreate_Local_Trx IN ('A', 'Y') THEN

              -- If looking for a Local Row it must be found.
              IF (cCreate_Local_Trx = 'Y') THEN
                cInternalErrorCode := 10411;
              END IF;


              FOR ARRec IN AR(vTarget_Type_id,vModel_Name_id, cTransaction_Table)
              LOOP
                vTarget_Type_id := ARRec.Target_Type_id;
                xxcp_wks.I1009_ArrayLC := xxcp_wks.vBase_ArrayLC;

                vProcess_Switch := TRUE;

                vFrom_Entered_Value      := Null;
                vFrom_Entered_Currency   := Null;
                vFrom_Accounted_Value    := Null;
                vFrom_Accounted_Currency := Null;

                IF cInternalErrorCode = 10411 THEN
                  cInternalErrorCode := 0;
                END IF;

                xxcp_global.gCommon(1).current_rule_id := ARRec.Rule_id;

                xxcp_wks.I1009_ArrayTEMP := xxcp_wks.I1009_ArrayLC;

                Get_Tax_Reg_Information(cTarget_Type_id        => ARRec.Target_Type_id,
                                        cOwner_Tax_Reg_id      => cOwner_Tax_Reg_id,
                                        cPartner_Tax_Reg_id    => cPartner_Tax_Reg_id,
                                        cEntity_Type           => ARRec.Entity_type,
                                        cTarget_Type_Match     => 'N',
                                        -- Returned
                                        cTarget_Assignment_id  => vTarget_Assignment_id,
                                        cTarget_Zero_Flag      => vTarget_Zero_Flag,
                                        cReg_id                => vTarget_Reg_id,
                                        cTax_Reg_id            => vTax_registration_id,
                                        cBal_Segment           => vBalancing_Segment,
                                        cExchange_Rate_Type    => vTarget_Exch_Rate_Type,
                                        cCOA_ID                => vTarget_COA_id,
                                        cInstance_ID           => vTarget_Instance_id,
                                        cSOB_ID                => vTarget_Set_of_books_id,
                                        cCurrency_Code         => vTarget_Currency_code,
                                        cCommon_Exch_Curr      => vCommon_Exch_Curr,
                                        cTrade_currency        => vTrade_currency,
                                        cInternalErrorCode     => cInternalErrorCode);

                xxcp_wks.I1009_ArrayLC := xxcp_wks.I1009_ArrayTEMP;

                EXIT WHEN cInternalErrorCode <> 0;

                xxcp_wks.I1009_ArrayLC(38) := nvl(cTransaction_Table,ARrec.transaction_table);
                xxcp_wks.I1009_ArrayLC(39) := nvl(cTransaction_Type,ARrec.transaction_type);
                xxcp_wks.I1009_ArrayLC(40) := nvl(cTransaction_Class,ARrec.transaction_class);
                xxcp_wks.I1009_ArrayLC(62) := arrec.rule_category_1;
                xxcp_wks.I1009_ArrayLC(63) := arrec.rule_category_2;
                xxcp_wks.I1009_ArrayLC(96) := arrec.entity_type;
                xxcp_wks.I1009_ArrayLC(145):= arrec.target_suppression_rule; 
                xxcp_wks.I1009_ArrayLC(146):= arrec.history_suppression_rule; 

                xxcp_trace.Transaction_Trace(cTrace_id           => 21,
                                             cRule_id             => ARRec.Rule_id,
                                             cRule_Name           => ARRec.Rule_name,
                                             cTarget_COA_id       => vTarget_COA_id,
                                             cTarget_Instance_id => vTarget_Instance_id
                                            );

                -- Work out Initial Value and Currency
                IF ARRec.Entered_Value_Attribute_id IS NOT NULL THEN
                  vEntered_Source := 'ARL';

                  vFrom_Entered_Value    := xxcp_foundation.Find_DynamicAttribute(ARRec.Entered_Value_Attribute_id,'L');
                  vFrom_Entered_Currency := xxcp_foundation.Find_DynamicAttribute(ARRec.Entered_Curr_Attribute_id,'L');

                  vFrom_Accounted_Value    := xxcp_foundation.Find_DynamicAttribute(ARRec.Accounted_Value_Attribute_id,'L');
                  vFrom_Accounted_Currency := xxcp_foundation.Find_DynamicAttribute(ARRec.Accounted_Curr_Attribute_id,'L');

                  IF (vFrom_Entered_Value IS NULL) OR (vFrom_Entered_Currency) IS NULL THEN
                    cInternalErrorCode := 10413;
                    xxcp_foundation.FndWriteError(cInternalErrorCode,
                        'Entered Value  <' || TO_CHAR(vFrom_Entered_Value) ||
                        '> Entered Currency  <' || vFrom_Entered_Currency ||
                        '> Entered Value ID <' || TO_CHAR(ARRec.Entered_Value_Attribute_id) || '> ' ||
                        '> Entered Currency ID <' || TO_CHAR(ARRec.Entered_Curr_Attribute_id) || '> ');
                  END IF;
                ELSE

                  vFrom_Entered_Value    := vModel_Entered_Amount;
                  vFrom_Entered_Currency := vModel_Entered_Currency;

                  IF vFrom_Entered_Value IS NULL OR vFrom_Entered_Currency IS NULL THEN
                    cInternalErrorCode := 10414;
                    IF vEntered_Source = 'PM' THEN
                      -- Price Method
                      vError_Message := 'Pricing Method Entered Value  <' || TO_CHAR(vFrom_Entered_Value) ||'> Entered Currency  <' || vFrom_Entered_Currency ||'> Entered Value ID <' || TO_CHAR(MCRec.AC_PRICING_ID) ||'> ';
                    ELSE
                      vError_Message := 'Source Entered Amount <' || TO_CHAR(vFrom_Entered_Value) ||'> Entered Currency <' || vFrom_Entered_Currency || '>';
                    END IF;
                    xxcp_foundation.FndWriteError(cInternalErrorCode, vError_Message);
                  END IF;

                END IF;

                IF vEntered_Source = 'PM' THEN
                  vFrom_Exchange_Rate_Type := nvl(MCRec.Exchange_Rate_Type,vPM_Entered_Exch_Rate_Type);
                ELSE
                  vFrom_Exchange_Rate_Type := nvl(MCRec.Exchange_Rate_Type,vTarget_Exch_Rate_Type);
                END IF;


                EXIT WHEN cInternalErrorCode <> 0;
                --
                -- End of Work out Initial Value and Currency
                --
                --

                --
                -- This is to take into account Cross Currency gain and Loss
                -- 26th Jan 2005.
                --
                If cTransaction_Type = 'CCURR' THEN
                  vTo_Entered_Currency := vFrom_Entered_Currency;
                End if;

                xxcp_wks.I1009_ArrayTEMP := xxcp_wks.I1009_ArrayLC;
                vTarget_Exch_Date        := vExchange_Date;
                vPerferred_Exchange_Rate := 0;
                If mcrec.Acc_Conversions = 'Y' then
                  -- New Accounted Preferred Currency type
                  If mcrec.Acc_Exchange_Rate_Type_id > 0 then
                    vTarget_Exch_Rate_Type := nvl(xxcp_foundation.Find_DynamicAttribute(mcrec.Acc_Exchange_Rate_Type_id,'L'),vTarget_Exch_Rate_Type);
                    If mcrec.Acc_Exchange_Date_Id > 0 then
                      vTarget_Exch_Date := nvl(to_date(xxcp_foundation.Find_DynamicAttribute(mcrec.Acc_Exchange_Date_id,'DD-MON-YYYY'),'L'),vTarget_Exch_Date);
                    End If;
                  End If;
                  -- New Accounted Preferred Rates
                  If ARRec.Entity_type = 'Owner' and mcrec.Acc_Owner_Exch_Rate_id > 0 then
                    vPerferred_Exchange_Rate := xxcp_foundation.Find_DynamicAttribute(mcrec.Acc_Owner_Exch_Rate_id,'L');
                  ElsIf ARRec.Entity_type = 'Partner' and mcrec.Acc_Partner_Exch_Rate_id > 0 then
                    vPerferred_Exchange_Rate := xxcp_foundation.Find_DynamicAttribute(mcrec.Acc_Partner_Exch_Rate_id,'L');
                  End If;
                End If;

                xxcp_te_values.DR_CR(cAction                  => 'L',
                                     cTo_Entered_Currency     => vTo_Entered_Currency,
                                     cTransaction_Quantity    => cTransaction_Quantity,
                                     --  Value and Currency
                                     cFrom_Entered_Value      => vFrom_Entered_Value,
                                     cFrom_Entered_Currency   => vFrom_Entered_Currency,
                                     cFrom_Exch_Rate_Type     => vFrom_Exchange_Rate_Type,
                                     -- Model Control Info
                                     cRule_Value_Name         => ARRec.Rule_Value_Name,
                                     cChange_Sign             => ARRec.Change_Sign,
                                     cEntity_type             => ARRec.Entity_type,
                                     cRule_name               => ARRec.Rule_name,
                                     -- Default Values
                                     cOwner_Common_Exch_Curr  => cOwner_Common_Exch_Curr,
                                     cExchange_Date           => vTarget_Exch_Date,
                                     -- Adjustment Rate id
                                     cAdjustment_Rate         => vIC_Adjustment_Rate,
                                     cTax_Rate                => cIC_Tax_Rate,
                                     -- Targets
                                     cTarget_Set_of_books_id  => vTarget_Set_of_books_id,
                                     cTarget_Exch_Rate_Type   => vTarget_Exch_Rate_Type,
                                     cAccounted_Currency      => vTarget_Currency_Code,
                                     --
                                     cModel_Name              => mcrec.Model_Name,
                                     cModel_Ctl_id            => mcrec.Model_ctl_id,
                                     --
                                     cPerferred_Exchange_Rate => vPerferred_Exchange_Rate,
                                     --
                                     cFrom_Accounted_Value    => vFrom_Accounted_Value,
                                     cFrom_Accounted_Currency => vFrom_Accounted_Currency,
                                     cUse_Extended_Precision  => mcrec.extended_precision,
                                     cTo_Entered_Amount       => vTo_Entered_Amount,
                                     cTo_Accounted_Amount     => vTo_Accounted_Amount,
                                     cInternalErrorCode       => cInternalErrorCode);

                EXIT WHEN cInternalErrorCode <> 0;

             --! ************************************************************************************
             --!        Surpression Logic
             --! ************************************************************************************

             If nvl(vTo_Entered_Amount,0) + nvl(vTo_Accounted_Amount,0) = 0 then
                If  ARRec.Zero_Suppression_Rule = 'N' then
                  vProcess_Switch := FALSE; -- Do not do it
                ElsIf ARRec.Zero_Suppression_Rule = 'E' and vTarget_Zero_Flag = 'N' then
                  vProcess_Switch := FALSE; -- Do not do it
                End If;

                If xxcp_global.gCOMMON(1).preview_zero_flag = 'Y' then
                 vProcess_Switch := TRUE;
                End If;

             End If;

             --! ************************************************************************************
             --!        vProcess_Switch
             --! ************************************************************************************

             If vProcess_Switch = TRUE then

                Get_Segment_Rules(cAction                  => 'L',
                                  cRule_id                 => ARRec.Rule_id,
                                  cRule_Name               => ARRec.Rule_Name,
                                  cChart_of_accounts_id    => vTarget_COA_id,
                                  cInstance_id             => vTarget_Instance_id,
                                  cEnforce_Accounting      => ARRec.Enforce_Accounting,
                                  cTarget_Cross_Validation => ARRec.Target_Cross_Validation,
                                  cEffective_Date          => cTransaction_Date,
                                  -- NCM 02.05.08
                                  cEntity_type             => ARRec.Entity_Type,
                                  cInternalErrorCode       => cInternalErrorCode);

                xxcp_wks.I1009_ArrayLC := xxcp_wks.I1009_ArrayTEMP;

                EXIT WHEN cInternalErrorCode <> 0;

                Get_Column_Attributes(cAction              => 'L',
                                      cTarget_Table        => ARRec.target_table,
                                      cTarget_Instance_id  => vTarget_Instance_id,
                                      cModel_Name_id       => vModel_Name_ID,
                                      cInternalErrorCode   => cInternalErrorCode);

                xxcp_wks.D1006_ArrayLC := xxcp_wks.D1006_Array;

                EXIT WHEN cInternalErrorCode <> 0;

                -- Apply Column Rules
                 Column_Rules(
                           cRule_type              => 'L',
                           cRule_id                => ARRec.Rule_id,
                           cModel_Ctl_id           => mcrec.Model_Ctl_id,
                           cTarget_Table           => ARRec.Target_Table,
                           cAttribute_id           => cAttribute_id,
                           cInterface_ID           => cInterface_id,
                           cTarget_Set_of_books_id => vTarget_Set_of_books_id,
                           cTransaction_Class      => cTransaction_Class,
                           cTarget_Type_id         => vTarget_Type_id,
                           cTarget_Record_Status   => ARRec.Target_Record_Status,
                           cTarget_Rounding        => vTarget_rounding,
                           cRound_Suppressed_Rules => ARRec.Round_Suppressed_Rules,
                           cTarget_Reg_id          => vTarget_Reg_id,
                           cNo_Rounding_Rule       => ARRec.No_Rounding_Rule,
                           cBalancing_Segment      => vBalancing_Segment,
                           cRounding_Group         => ARRec.Rounding_Group,
                           cRule_Entity_Type       => ARRec.Entity_type,
                           cTarget_Assignment_id   => vTarget_Assignment_id,
                           cTarget_Zero_Flag       => vTarget_Zero_Flag,
                           cTax_Registration_id    => vTax_registration_id,
                           cTarget_Instance_id     => vTarget_Instance_id,
                           cTarget_COA_id          => vTarget_COA_id,
                           cZero_suppression_rule  => ARRec.Zero_suppression_rule,
                           cCost_Plus_Rule         => ARRec.Cost_Plus_Rule,
                           cExtended_Values        => ARRec.Extended_Values,
                           cEntered_Amounts        => ARRec.Entered_Amounts,
                           cAccounted_Amounts      => ARRec.Accounted_Amounts,
                           -- 03.06.20
                           cSettlement_rule        => ARRec.Report_Settlement_Rule,
                           cReporting_field1       => ARRec.Reporting_Field1,
                           cReporting_field2       => ARRec.Reporting_Field2,
                           cReporting_field3       => ARRec.Reporting_Field3,
                           cReporting_field4       => ARRec.Reporting_Field4,
                           cReporting_field5       => ARRec.Reporting_Field5 
                          );

                IF cInternalErrorCode <> 0 THEN
                  xxcp_foundation.FndWriteError(cInternalErrorCode,vError_Message);
                END IF;

                EXIT WHEN cInternalErrorCode <> 0;

                End If; -- vProcess_Switch

              END LOOP; -- End Account Rules Loop

              IF cInternalErrorCode = 10411 THEN
                xxcp_foundation.FndWriteError(cInternalErrorCode,
                                              'Model Name Id <' ||TO_CHAR(vModel_Name_Id) ||
                                              '> Model Name <' ||vModel_Name ||
                                              '> Transaction Table <' ||cTransaction_Table ||
                                              '> Type <' ||cTransaction_Type ||
                                              '> Class <' ||cTransaction_Class || '>');
              END IF;
            END IF; -- End Check CREATE_LOCAL_TRX or vRule_Type

          ELSE
            xxcp_foundation.Set_InternalErrorCode(cInternalErrorCode,10402);
          END IF;
        END IF;
        -- End Create Interface Row
      END LOOP;
    END IF;

    IF cInternalErrorCode = 10417 THEN
      xxcp_foundation.FndWriteError(cInternalErrorCode,
                                    'Trading Relationship <' || TO_CHAR(cEntity_Set_id) ||
                                    '> Transaction Set <' || TO_CHAR(cTransaction_set_id) ||
                                    '> MC Qualifier 1 <' || vMC_Qualifier1 ||
                                    '> Qualifier 2 <' || vMC_Qualifier2 ||
                                    '> Qualifier 3 <' || vMC_Qualifier3 ||
                                    '> Qualifier 4 <' || vMC_Qualifier4 ||
                                    '> Trx Qualifier 1 <' || cTRX_Qualifier1 ||
                                    '> Qualifier 2 <' || cTRX_Qualifier2 ||
                                    '> Transaction Date <' || TO_CHAR(cTransaction_Date) ||
                                    '> Transaction Table <' || cTransaction_Table ||
                                    '> Type <' || cTransaction_Type ||
                                    '> Class <' || cTransaction_Class || '>');
    END IF;

  END Process_Local;
  
  
  
  --
  -- Set_Internal_Tax_Elements
  --
  Procedure Set_Internal_Tax_Elements(cSub_Rule_Type in varchar, cInternalErrorCode in out number) is
    
   k pls_integer;
   w pls_integer;
   
   --
   -- Clear_Internal_Tax_Elements
   Procedure Clear_Internal_Tax_Elements is
     begin
       xxcp_wks.I1009_ArrayCH(163) := Null;
       xxcp_wks.I1009_ArrayCH(164) := Null;
       xxcp_wks.I1009_ArrayCH(165) := Null;
       xxcp_wks.I1009_ArrayCH(166) := Null;
   End Clear_Internal_Tax_Elements;

  Begin
    
     w := xxcp_te_base.gTax_Element_Set.Count;
     Clear_Internal_Tax_Elements; 

     If cSub_Rule_Type != '0' then
       -- If a matching tax element is not found, the fule should not fire
       cInternalErrorCode := gDo_Not_Do_Rule; 
    
       For k in 1..w loop
         If xxcp_te_base.gTax_Element_Set(k).Tax_Element_Number = cSub_Rule_Type Then
           xxcp_wks.I1009_ArrayCH(163) := xxcp_te_base.gTax_Element_Set(k).Tax_Rate;
           xxcp_wks.I1009_ArrayCH(164) := xxcp_te_base.gTax_Element_Set(k).Tax_Amount;
           xxcp_wks.I1009_ArrayCH(165) := xxcp_te_base.gTax_Element_Set(k).Tax_Regime_Name;
           xxcp_wks.I1009_ArrayCH(166) := xxcp_te_base.gTax_Element_Set(k).Tax_Rate_Code;
           -- recovery           
           xxcp_wks.I1009_ArrayCH(167) := xxcp_te_base.gTax_Element_Set(k).Recovery_Rate;
           xxcp_wks.I1009_ArrayCH(168) := xxcp_te_base.gTax_Element_Set(k).Recovery_Amount;
           xxcp_wks.I1009_ArrayCH(170) := xxcp_te_base.gTax_Element_Set(k).Recovery_Rate_Code;
           
           xxcp_wks.I1009_Array(161):= xxcp_te_base.gTax_Record_Set(1).Tax_Engine_id;
     
           cInternalErrorCode := 0;
           Exit;
         End If; 
       End Loop;
     End If;
     
  End Set_Internal_Tax_Elements;

  --
  --## ***********************************************************************************************************
  --##
  --##                                Process_Standard
  --##
  --## ***********************************************************************************************************
  PROCEDURE Process_Standard(cSet_of_books_id         IN NUMBER,
                             cSourceRowid             IN ROWID,
                             cCreate_Child_trx        IN VARCHAR2,
                             cCreate_Tax_Rules        IN VARCHAR2,
                             cTransaction_id          IN NUMBER,
                             cTransaction_Table       IN VARCHAR2,
                             cTransaction_Type        IN VARCHAR2,
                             cTransaction_Class       IN VARCHAR2,
                             cQuantity                IN NUMBER,
                             cEntity_Set_id           IN NUMBER,
                             cTransaction_set_id      IN NUMBER,
                             cTRX_Qualifier1          IN VARCHAR2,
                             cTRX_Qualifier2          IN VARCHAR2,
                             cED_Qualifier1           IN VARCHAR2,
                             cED_Qualifier2           IN VARCHAR2,
                             cED_Qualifier3           IN VARCHAR2,
                             cED_Qualifier4           IN VARCHAR2,
                             cTransaction_Currency    IN VARCHAR2,
                             cTransaction_Quantity    IN NUMBER,
                             cTransaction_Date        IN DATE,
                             cOwner_Tax_Reg_id        IN NUMBER,
                             cPartner_Tax_Reg_id      IN NUMBER,
                             cOwner_Common_Exch_Curr  IN VARCHAR2,
                             cExchange_Date           IN DATE,
                             cOwner_Legal_Currency    IN VARCHAR2,
                             cOwner_Trade_Currency    IN VARCHAR2,
                             cPartner_Legal_Currency  IN VARCHAR2,
                             cPartner_Trade_Currency  IN VARCHAR2,
                             cAttribute_id            IN NUMBER,
                             cInterface_ID            IN NUMBER,
                             cIC_Tax_Rate             IN NUMBER,
                             cIC_Adjustment_Rate      IN NUMBER,
                             cParent_Entered_Amount   IN NUMBER,
                             cParent_Entered_Currency IN VARCHAR2,
                             cBase_SQL_Build_ID       IN NUMBER,
                             cInternalErrorCode       IN OUT NOCOPY NUMBER) IS

    vModel_Name_id             NUMBER(15);
    vTarget_Type_id            NUMBER(4);
    vPrevious_SQL_Build_id     NUMBER(15) := -7;

    vError_Message             XXCP_ERRORS.error_message%TYPE;
    vIC_Adjustment_Rate        XXCP_TRANSACTION_ATTRIBUTES.adjustment_rate%TYPE;
    vModel_Entered_Amount      NUMBER;
    vModel_Entered_Currency    VARCHAR2(15);
    -- vTo
    vTo_Entered_Currency       VARCHAR2(15);
    -- vFrom
    vFrom_Entered_Value        NUMBER;
    vFrom_Entered_Currency     VARCHAR2(15);
    vFrom_Exchange_Rate_Type   VARCHAR2(30);
    --
    vPM_Entered_Exch_Rate_Type VARCHAR2(30);
    vEntered_Source            VARCHAR2(5);
    vExchange_Date             DATE;
    vTarget_Exch_Rate_Type     XXCP_TAX_REGISTRATIONS.exchange_rate_type%TYPE;
    vTarget_Exch_Date          Date;
    vTarget_Reg_id             XXCP_TARGET_ASSIGNMENTS.reg_id%TYPE;
    vTarget_Set_of_books_id    XXCP_TARGET_ASSIGNMENTS.target_set_of_books_id%TYPE;
    vTarget_COA_ID             XXCP_TARGET_ASSIGNMENTS.target_coa_id%TYPE;
    vTarget_Instance_id        XXCP_TARGET_ASSIGNMENTS.target_instance_id%TYPE;
    vTarget_Currency_code      XXCP_TARGET_ASSIGNMENTS.target_acct_currency%TYPE;
    vCommon_Exch_Curr          XXCP_TAX_REGISTRATIONS.common_exch_curr%TYPE;
    vTrade_currency            XXCP_TAX_REGISTRATIONS.trade_currency%TYPE;
    vTarget_Zero_Flag          XXCP_TARGET_ASSIGNMENTS.zero_flag%TYPE;

    vD1004_Pointers            varchar2(500);
    vTax_registration_id       number(10);
    vBalancing_Segment         number(4);
    vTarget_Assignment_id      xxcp_target_assignments.target_assignment_id%type := 0;

    vReplace_Trx_Date          xxcp_entity_documents.replace_trx_date%TYPE;
    vBatch_Source_Name         xxcp_entity_documents.trx_number_source%TYPE;
    vCust_Trx_Type_id          xxcp_entity_documents.trx_number_type_id%TYPE;
    vED_Trx_Type_Name          xxcp_entity_documents.trx_number_type%type;
    vED_Tax_Reg_id             xxcp_tax_registrations.Tax_registration_id%type;

    vPerferred_Exchange_Rate   number := 0;

    -- Model Control
    CURSOR MC(pEntity_Set_Id      IN NUMBER
            , pTransaction_Table  IN VARCHAR2
            , pTransaction_Type   IN VARCHAR2
            , pTransaction_Class  IN VARCHAR2
            , pTRX_Qualifier1     IN VARCHAR2
            , pTRX_Qualifier2     IN VARCHAR2
            , pTransaction_Set_id IN NUMBER
            , pTransaction_Date   IN DATE
            , pTrading_Set_id     IN NUMBER
            , pTax_Rules          IN VARCHAR2) IS
      SELECT mc.SQL_build_Id,
             smn.MODEL_NAME,
             mc.Model_Name_id,
             mc.MC_Qualifier1,
             mc.MC_Qualifier2,
             mc.MC_Qualifier3,
             mc.MC_Qualifier4,
             mc.Account_Price_method_id AC_PRICING_ID,
             mc.Account_PRICE_METHOD_Type AC_Pricing_Method_Type,
             mc.Target_Type_id,
             Decode(mc.ED_Enabled,'Y','O',mc.ed_enabled) ed_enabled,
             mc.Model_ctl_id,
             mc.exchange_rate_type,
             smn.Extended_Precision,
             mc.ACC_CONVERSIONS,
             nvl(mc.acc_exchange_rate_type_id,0) acc_exchange_rate_type_id,
             nvl(mc.acc_exchange_date_id,0)      acc_exchange_date_id,
             nvl(mc.acc_owner_exch_rate_id,0)    acc_owner_exch_rate_id,
             nvl(mc.acc_partner_exch_rate_id,0)  acc_partner_exch_rate_id,
             mc.formula_set_id,
             mc.trade_curr_indicator,
             nvl(smn.target_type_match_ignore,'N') Target_Type_Match
        FROM xxcp_model_ctl mc,
             xxcp_sys_model_names smn,
             xxcp_sys_qualifier_groups mq
       WHERE mq.source_id          = xxcp_global.gCommon(1).Source_id
         AND mc.model_ctl_group_id = mq.qualifier_group_id
         AND mc.Transaction_set_id = pTransaction_set_id
         AND mq.qualifier_usage    = 'M'
         AND smn.model_name_id     = mc.model_name_id
         AND smn.source_id         = mc.source_id
         AND mc.Entity_set_id      = nvl(pEntity_Set_Id, mc.Entity_Set_Id)
         AND nvl(mc.MC_Qualifier1, '~null~') =
             DECODE(mc.MC_Qualifier1,
                    '*',
                    nvl(mc.MC_Qualifier1, '~null~'),
                    nvl(xxcp_foundation.MC_DynamicAttribute(mq.Qualifier1_id), '~null~'))
         AND nvl(mc.MC_Qualifier2, '~null~') =
             DECODE(mc.MC_Qualifier2,
                    '*',
                    nvl(mc.MC_Qualifier2, '~null~'),
                    nvl(xxcp_foundation.MC_DynamicAttribute(mq.Qualifier2_id), '~null~'))
         AND nvl(mc.MC_Qualifier3, '~null~') =
             DECODE(mc.MC_Qualifier3,
                    '*',
                    nvl(mc.MC_Qualifier3, '~null~'),
                    nvl(xxcp_foundation.MC_DynamicAttribute(mq.Qualifier3_id), '~null~'))
         AND nvl(mc.MC_Qualifier4, '~null~') =
             DECODE(mc.MC_Qualifier4,
                    '*',
                    nvl(mc.MC_Qualifier4, '~null~'),
                    nvl(xxcp_foundation.MC_DynamicAttribute(mq.Qualifier4_id), '~null~'))
         AND nvl(mc.TRX_Qualifier1, '~null~') =
             DECODE(mc.TRX_Qualifier1,
                    '*',
                    nvl(mc.TRX_Qualifier1, '~null~'),
                    nvl(pTRX_Qualifier1, '~null~'))
         AND nvl(mc.TRX_Qualifier2, '~null~') =
             DECODE(mc.TRX_Qualifier2,
                    '*',
                    nvl(mc.TRX_Qualifier2, '~null~'),
                    nvl(pTRX_Qualifier2, '~null~'))
         AND mc.Trading_Set_id  = pTrading_Set_id
         AND mc.Active = 'Y'
         AND mc.Model_Name_id = ANY
       (SELECT /*+ Index(ar) */
               ar.Model_name_id
                FROM XXCP_ACCOUNT_RULES ar
               WHERE ar.source_id = mc.source_id
                 AND DECODE(ar.transaction_table,'*',pTRANSACTION_TABLE, ar.transaction_table) = pTRANSACTION_TABLE
                 AND DECODE(ar.transaction_Type,'*',pTRANSACTION_TYPE,ar.transaction_Type)   = pTRANSACTION_TYPE
                 AND DECODE(ar.transaction_class,'*',pTRANSACTION_CLASS,ar.transaction_class) = pTRANSACTION_CLASS
                 AND ar.active = 'Y'
                 AND (ar.rule_type = 'C' OR ar.rule_type = 'O' OR ar.rule_type = pTax_Rules))
         AND pTransaction_Date BETWEEN nvl(mc.Effective_From_Date, TO_DATE('01-JAN-2000','DD-MON-YYYY')) AND nvl(mc.Effective_To_Date, TO_DATE('31-DEC-2999','DD-MON-YYYY'))
       ORDER BY smn.Model_Sequence, MC.SQL_BUILD_ID;

    -- Transaction Rules
    CURSOR AR(pTarget_Type_id IN NUMBER, pModel_Name_id IN NUMBER, pTransaction_Table IN VARCHAR2, pTransaction_Type IN VARCHAR2
            , pTransaction_Class IN VARCHAR2,pTax_Rules IN VARCHAR2) IS
      SELECT /*+ Index(AR) */
        ar.rule_name,
        ar.entity_type,
        ar.rule_value_name,
        ar.change_sign,
        ar.rule_id,
        ar.transaction_class,
        ar.Target_Table,
        ar.entered_value_attribute_id,
        ar.entered_curr_attribute_id,
        nvl(ar.target_type_id, pTarget_Type_id) Target_Type_id,
        ar.transaction_class_replacement,
        t.short_description Target_Record_Status,
        decode(ar.EXCLUDE_FROM_BALANCING,'Y','N',t.target_rounding) Target_Rounding,
        t.round_suppressed_rules,
        nvl(t.enforce_accounting, 'Y') Enforce_accounting,
        nvl(t.cross_validation, 'N') Target_Cross_Validation,
        ar.rule_category_1, ar.rule_category_2,
        DECODE(SUBSTR(ar.rule_type,1,1),'T','T','O','O','S') Rule_Type,
        ar.No_Rounding No_Rounding_Rule,
        ar.Rounding_Group,
        nvl(ar.Zero_Suppression_rule,'T') Zero_Suppression_rule,
        ar.cost_plus_rule,
        t.Extended_Values,
        t.Entered_Amt_Bal Entered_Amounts,
        t.Accounted_Amt_Bal Accounted_Amounts,
        ar.ACCOUNTED_VALUE_ATTRIBUTE_ID,
        ar.ACCOUNTED_CURR_ATTRIBUTE_ID,
        nvl(ar.Target_Suppression_Rule,'N') Target_Suppression_Rule,
        nvl(ar.History_Suppression_Rule,'N') History_Suppression_Rule,
        nvl(ar.ONCE_BRK_ATTRIBUTE1_ID,0) ONCE_BRK_ATTRIBUTE1_ID,
        nvl(ar.ONCE_BRK_ATTRIBUTE2_ID,0) ONCE_BRK_ATTRIBUTE2_ID,
        nvl(ar.ONCE_BRK_ATTRIBUTE3_ID,0) ONCE_BRK_ATTRIBUTE3_ID,
        nvl(ar.ONCE_BRK_ATTRIBUTE4_ID,0) ONCE_BRK_ATTRIBUTE4_ID,
        ar.rule_sequence,
        -- ICS V4
        ar.report_settlement_rule,
        ar.attribute1 reporting_field1,
        ar.attribute2 reporting_field2,
        ar.attribute3 reporting_field3,
        ar.attribute4 reporting_field4,
        ar.attribute5 reporting_field5,
        -- Tax
        nvl(ar.sub_rule_type,0) sub_rule_type
       FROM XXCP_ACCOUNT_RULES ar, XXCP_SYS_TARGET_TABLES t
       WHERE (ar.model_name_id = pModel_Name_id AND (ar.rule_type = 'C' OR ar.rule_type = 'O' OR ar.rule_type = pTax_Rules))
         AND ar.target_table = t.transaction_table
         AND ar.source_id = t.source_id
         AND ar.active = 'Y'
         AND DECODE(ar.transaction_table,'*',pTRANSACTION_TABLE, ar.transaction_table) = pTRANSACTION_TABLE
         AND DECODE(ar.transaction_Type,'*',pTRANSACTION_TYPE,ar.transaction_Type)   = pTRANSACTION_TYPE
         AND DECODE(ar.transaction_class,'*',pTRANSACTION_CLASS,ar.transaction_class) = pTRANSACTION_CLASS
       Order by nvl(ar.rule_sequence,99999);

    vStd_Rule_Found          BOOLEAN := FALSE;
    vTax_Rules               VARCHAR2(1);
    vOnce_cnt                NUMBER  := 0;
    vOnce_found              BOOLEAN := FALSE;

    vOnce_Break_Value1  varchar2(250);
    vOnce_Break_Value2  varchar2(250);
    vOnce_Break_Value3  varchar2(250);
    vOnce_Break_Value4  varchar2(250);

    vProcess_Switch          BOOLEAN := TRUE;
    k                        NUMBER;

    vTo_Entered_Value        NUMBER  := 0;
    vTo_Accounted_Value      NUMBER  := 0;
    vFrom_Accounted_Value    Number  := 0;
    vFrom_Accounted_Currency varchar2(15);
    vPrice_Method_Calc_Value Number := Null;
    vIC_Tax_Rate             Number;
    
    vTarget_Rounding         Varchar2(1);

  BEGIN

    IF cCreate_Tax_Rules = 'Y' THEN
     vTax_Rules := 'T'; -- Tax Rules
    ELSE
     vTax_Rules := 'C'; -- Just Std rules
    END IF;

    vFrom_Entered_Currency := cTransaction_Currency;

    IF xxcp_global.Trace_on = 'Y' THEN
      xxcp_wks.Trace_Log := xxcp_wks.Trace_Log ||CHR(10)|| 'STANDARD RULE'|| CHR(10);
    END IF;

    xxcp_wks.Reset_Child_Arrays;
    xxcp_wks.I1009_ArrayCH  := xxcp_wks.I1009_Array;
    xxcp_wks.vPrev_ArrayCH  := xxcp_wks.Clear_Dynamic;
    xxcp_wks.vBase_ArrayCH  := xxcp_wks.I1009_Array;
    xxcp_wks.I1009_ArrayTEMP:= xxcp_wks.I1009_ArrayCH;

    FOR MCRec IN MC(pEntity_Set_Id      => cEntity_Set_Id,
                    pTransaction_Set_id => cTransaction_Set_id,
                    pTransaction_Table  => cTransaction_Table,
                    pTransaction_Type   => cTransaction_Type,
                    pTransaction_Class  => cTransaction_Class,
                    pTRX_Qualifier1     => cTRX_Qualifier1,
                    pTRX_Qualifier2     => cTRX_Qualifier2,
                    pTransaction_Date   => cTransaction_Date,
                    pTrading_Set_id     => xxcp_global.gCommon(1).current_Trading_Set_id,
                    pTax_Rules          => vTax_Rules)
      LOOP

        EXIT WHEN cInternalErrorCode <> 0;

        vEntered_Source          := NULL;
        vExchange_Date           := cExchange_Date;
        vTarget_Type_id          := mcrec.Target_Type_id;
        vTo_Entered_Currency       := cTransaction_Currency;
        vModel_Name_ID             := mcrec.Model_Name_ID;
        xxcp_wks.vBase_ArrayCH(26) := mcrec.Model_name;
        xxcp_wks.vBase_ArrayCH(57) := mcrec.Model_Ctl_id;
        vPrice_Method_Calc_Value   := Null;

        xxcp_trace.Transaction_Trace(cTrace_id     => 22,
                                     cModel_Name   => MCRec.Model_Name,
                                     cModel_Ctl_id => MCRec.Model_Ctl_id,
                                     cTarget_Type_Match_Ignore => MCRec.Target_Type_Match
                                     );
        --
        IF ((cBase_SQL_BUILD_ID = mcrec.SQL_Build_id) OR (mcrec.SQL_Build_id = 0)) THEN
          --
          -- USE SQL From Base for Performance Reasons.
          --
          vPrevious_SQL_Build_id := cBase_SQL_BUILD_ID;
          -- 02.05.20a
          xxcp_wks.D1003_ArrayCH := xxcp_wks.D1003_Array;
          xxcp_wks.vPrev_ArrayCH := xxcp_wks.D1003_Array;
          --
        ELSIF (vPrevious_SQL_Build_id = MCrec.SQL_Build_id) THEN
          xxcp_wks.D1003_ArrayCH := xxcp_wks.vPrev_ArrayCH;
        ELSE

        --
        -- ACCOUNT DYNAMIC SQL
        --
        xxcp_wks.D1003_ArrayCH := xxcp_wks.Clear_Dynamic;

        xxcp_Dynamic_Sql.Non_Configurator(cActivity_Name          =>  'Transaction'
                                         ,cRule_type              =>   'S'
                                         ,cSet_of_books_id        =>  cSet_of_books_id
                                         ,cSourceRowid            =>  cSourceRowid
                                         ,cTransaction_id         =>  cTransaction_Id
                                         ,cTransaction_Table      => cTransaction_Table
                                         ,cTransaction_class      => cTransaction_class
                                         ,cTrading_Set            => xxcp_global.gCommon(1).current_Trading_Set
                                         ,cActivity_id            =>  1003
                                         ,cDynamic_Interface_id   =>  mcrec.SQL_Build_id
                                         ,cOwner                  =>  cOwner_Tax_Reg_id
                                         ,cPartner                =>  cPartner_Tax_Reg_id
                                         ,cKey                    =>  NULL
                                         ,cPrevious_Attribute_id  =>  xxcp_global.gCommon(1).Previous_Attribute_id
                                         ,cWhen_DI_Not_Found      =>  'F'
                                         ,cWhen_DI_Too_Many_Found =>  'F'
                                         ,cResults_Pointer        =>   vD1004_Pointers
                                         ,cInternalErrorCode      =>   cInternalErrorCode);

        xxcp_wks.D1003_ArrayCH  := xxcp_wks.DYNAMIC_RESULTS;
        xxcp_wks.vPrev_ArrayCH  := xxcp_wks.D1003_ArrayCH;
        vPrevious_SQL_Build_id  := mcrec.SQL_Build_id;
        xxcp_wks.dynamic_results := xxcp_wks.Clear_Dynamic;

      END IF;

      -- ############## Cache Summary Find ################## --
      IF ((gCacheSummaries = 'Y') AND (xxcp_wks.BRK_SET_RCD(1).sum_qty > 0)) THEN
         Cache_Summary_Get('S');
      END IF;

      -- ##
      IF cInternalErrorCode = 0 THEN
        IF xxcp_wks.Max_Accounting > 1 THEN

          vModel_Entered_Amount := NULL;

          --
          -- Formulas
           If cInternalErrorCode = 0 and nvl(mcRec.Formula_Set_Id,0) > 0 then
             gFormula_Pointers_1003 := Null;
             cInternalErrorCode := xxcp_te_formulas.Call_Formula( cSource_id        => xxcp_global.gCommon(1).Source_id,
                                                                  cRule_Type        => 'S',
                                                                  cFormula_Set_id   => mcRec.Formula_Set_Id,
                                                                  cFormula_Pointers => gFormula_Pointers_1003,
                                                                  cDynamic_Array    => xxcp_wks.D1003_ArrayCH);

             Exit when cInternalErrorCode <> 0;
          End If;

          IF nvl(mcrec.AC_PRICING_ID, 0) <> 0 THEN
            xxcp_te_values.Pricing(cAction                  => 'S',
                                   cRule_Type               => 'S',
                                   cSet_of_books_id         => cSet_of_books_id,
                                   cTransaction_Table       => cTransaction_Table,
                                   cTransaction_id          => cTransaction_id,
                                   cSourceRowid             => cSourceRowid,
                                   cTransaction_Date        => cTransaction_Date,
                                   cExchange_Date           => cExchange_Date,
                                   cQuantity                => cQuantity,
                                   cOwner_Tax_Reg_id        => cOwner_Tax_Reg_id,
                                   cPartner_Tax_Reg_id      => cPartner_Tax_Reg_id,
                                   cPrice_Method_id         => mcrec.AC_PRICING_ID,
                                   cPrice_Method_Type       => mcrec.AC_Pricing_Method_Type,
                                   cIC_Adjustment_Rate      => cIC_Adjustment_Rate,
                                   cModel_Name              => mcrec.Model_Name,
                                   cModel_Ctl_id            => mcrec.Model_ctl_id,
                                   cModel_Curr_Indicator    => MCRec.Trade_Curr_Indicator,
                                   cTransaction_Currency    => cTransaction_Currency,
                                   cOwner_Legal_Currency    => cOwner_Legal_Currency,
                                   cPartner_Legal_Currency  => cPartner_Legal_Currency,
                                   cOwner_Trade_Currency    => cOwner_Trade_Currency,
                                   cPartner_Trade_Currency  => cPartner_Trade_Currency,
                                   -- OUT
                                   cAdjustment_Rate         => vIC_Adjustment_Rate,
                                   cTo_Entered_Currency     => vTo_Entered_Currency,
                                   cPrice_Method_Value      => vModel_Entered_Amount,
                                   cPrice_Method_Curr       => vModel_Entered_Currency,
                                   cPrice_Method_Exch_Type  => vPM_Entered_Exch_Rate_Type,
                                   cPrice_Method_Calc_Value => vPrice_Method_Calc_Value,
                                   cPrice_Exchange_Date     => vExchange_Date, -- Exchange Date can be modified
                                   cInternalErrorCode       => cInternalErrorCode);

            xxcp_wks.vBase_ArrayCH(135) := vPrice_Method_Calc_Value;

            IF nvl(cInternalErrorCode, 0) <> 0 THEN
              xxcp_foundation.FndWriteError(10307,'Model Control ID <' ||TO_CHAR(mcrec.Model_ctl_id) || '> ');
            END IF;
          ELSE
            vIC_Adjustment_Rate  := cIC_Adjustment_Rate;
          END IF;

          IF vModel_Entered_Amount IS NULL OR vModel_Entered_Currency IS NULL Then
            -- Entered Amounts and currencies must stay together
            vModel_Entered_Amount   := cParent_Entered_Amount;
            vModel_Entered_Currency := cParent_Entered_Currency;
            vEntered_Source         := 'SRC';
            xxcp_trace.Transaction_Trace(cTrace_id  => 19);
          ELSE
            vEntered_Source := 'PM';
             xxcp_trace.Transaction_Trace(cTrace_id  => 20);
          END IF;

          IF (MCRec.ed_enabled in ('O','P')) AND (cInternalErrorCode = 0) THEN

            -- Entity Document can be by Owner or Partner.
            If mcrec.ed_enabled = 'P' then
                vED_Tax_Reg_id := cPartner_Tax_Reg_id;
            Else
                vED_Tax_Reg_id := cOwner_Tax_Reg_id;
            End If;

            -- Get the batch source
            Get_Entity_Document(  cTaxRegId          => vED_Tax_Reg_id,
                                  cTarget_Type_id    => MCRec.Target_Type_id,
                                  cModel_Name_id     => MCRec.Model_Name_id,
                                  cED_Qualifier1     => cED_Qualifier1,
                                  cED_Qualifier2     => cED_Qualifier2,
                                  cED_Qualifier3     => cED_Qualifier3,
                                  cED_Qualifier4     => cED_Qualifier4,
                                  cEffective_Date    => cTransaction_Date,
                                  cBatch_Source_Name => vBatch_Source_Name,
                                  cCust_Trx_Type_id  => vCust_Trx_Type_id,
                                  cReplace_Trx_Date  => vReplace_Trx_Date,
                                  cED_Trx_Type_Name  => vED_Trx_Type_Name,
                                  cInternalErrorCode => cInternalErrorCode);

            -- Put values into the internal array
            xxcp_wks.vBase_ArrayCH(46) := vBatch_Source_Name;
            xxcp_wks.vBase_ArrayCH(47) := vCust_Trx_Type_id;
            xxcp_wks.vBase_ArrayCH(48) := vReplace_Trx_Date;
            xxcp_wks.vBase_ArrayCH(89) := vED_Trx_Type_Name;

          END IF;

          --
          -- Don't Create Local Rows if Assignment Control says so
          -- but always create IC Rows if a rule_type 'C' is found.
          -- ARL = Account Rule Level
          --

          EXIT WHEN cInternalErrorCode <> 0;

          vOnce_found := FALSE;

          FOR ARRec IN AR(pTarget_Type_id    => vTarget_Type_id,
                          pModel_Name_id     => vModel_Name_id,
                          pTransaction_Table => cTransaction_Table,
                          pTransaction_Type  => cTransaction_Type,
                          pTransaction_Class => cTransaction_Class,
                          pTax_Rules         => vTax_Rules)
          LOOP

            EXIT WHEN cInternalErrorCode <> 0;
            -- Check for Once Records to ensure that they only happen once per model for this transaction

            vFrom_Entered_Value      := Null;
            vFrom_Entered_Currency   := Null;
            vFrom_Accounted_Value    := Null;
            vFrom_Accounted_Currency := Null;
            -- Break Points
            vOnce_Break_Value1       := Null;
            vOnce_Break_Value2       := Null;
            vOnce_Break_Value3       := Null;
            vOnce_Break_Value4       := Null;
            
            vTarget_rounding := ARRec.Target_rounding;
            
            
            If ARRec.Rule_Type = 'O' then
              -- New Once Break Values                           
              If ARRec.ONCE_BRK_ATTRIBUTE1_ID > 0 then
                vOnce_Break_Value1 := xxcp_foundation.Find_DynamicAttribute(ARRec.ONCE_BRK_ATTRIBUTE1_ID,'S');
              End If;
              If ARRec.ONCE_BRK_ATTRIBUTE2_ID > 0 then
                vOnce_Break_Value2 := xxcp_foundation.Find_DynamicAttribute(ARRec.ONCE_BRK_ATTRIBUTE2_ID,'S');
              End If;
              If ARRec.ONCE_BRK_ATTRIBUTE3_ID > 0 then
                vOnce_Break_Value3 := xxcp_foundation.Find_DynamicAttribute(ARRec.ONCE_BRK_ATTRIBUTE3_ID,'S');
              End If;
              If ARRec.ONCE_BRK_ATTRIBUTE4_ID > 0 then
                vOnce_Break_Value4 := xxcp_foundation.Find_DynamicAttribute(ARRec.ONCE_BRK_ATTRIBUTE4_ID,'S');
              End If;
            End If;

            IF ((ARRec.Rule_Type = 'O') AND (NOT vOnce_found)) THEN

              vOnce_Cnt := nvl(xxcp_wks.ONCE_RCD.COUNT,0);

              FOR k IN 1..vOnce_Cnt LOOP
               IF xxcp_wks.ONCE_RCD(k).Model_name_id = vModel_Name_id
                 AND xxcp_wks.ONCE_RCD(k).Transaction_Table = cTransaction_Table
                 AND xxcp_wks.ONCE_RCD(k).Transaction_Type  = cTransaction_Type
                 AND xxcp_wks.ONCE_RCD(k).Rule_id           = ARRec.Rule_id
                   AND nvl(xxcp_wks.ONCE_RCD(k).Break_value1,'*') = nvl(vOnce_Break_Value1,'*')
                   AND nvl(xxcp_wks.ONCE_RCD(k).Break_value2,'*') = nvl(vOnce_Break_Value2,'*')
                   AND nvl(xxcp_wks.ONCE_RCD(k).Break_value3,'*') = nvl(vOnce_Break_Value3,'*')
                   AND nvl(xxcp_wks.ONCE_RCD(k).Break_value4,'*') = nvl(vOnce_Break_Value4,'*')
                 AND xxcp_wks.ONCE_RCD(k).Trading_Set_id    = xxcp_global.gCommon(1).current_Trading_Set_id
                 THEN
                   vOnce_found := TRUE;
                   EXIT;
               END IF;
              END LOOP;

            END IF;

            --!! **********************************************************************************
            --!!                  Account Rule logic
            --!! **********************************************************************************
            IF ((ARRec.Rule_Type = 'O' AND vOnce_found = FALSE) OR (ARRec.Rule_Type != 'O')) THEN

             vStd_Rule_Found := TRUE;
             vProcess_Switch := TRUE;

             vTarget_Type_id := nvl(ARRec.Target_Type_id,mcrec.target_type_id);
             xxcp_wks.I1009_ArrayCH := xxcp_wks.vBase_ArrayCH;
             xxcp_global.gCommon(1).current_rule_id := ARRec.Rule_id;

             --
             -- Tax Registration Details
             --
             xxcp_wks.I1009_ArrayTEMP := xxcp_wks.I1009_ArrayCH;

             Get_Tax_Reg_Information(cTarget_Type_id        => vTarget_Type_id,
                                     cOwner_Tax_Reg_id      => cOwner_Tax_Reg_id,
                                     cPartner_Tax_Reg_id    => cPartner_Tax_Reg_id,
                                     cEntity_Type           => ARRec.Entity_type,
                                     cTarget_Type_Match     => McRec.Target_Type_Match,
                                    -- Returned
                                     cTarget_Assignment_id  => vTarget_Assignment_id,
                                     cTarget_Zero_Flag      => vTarget_Zero_Flag,
                                     cReg_id                => vTarget_Reg_id,
                                     cTax_Reg_id            => vTax_registration_id,
                                     cBal_Segment           => vBalancing_Segment,
                                     cExchange_Rate_Type    => vTarget_Exch_Rate_Type,
                                     cCOA_ID                => vTarget_COA_id,
                                     cInstance_ID           => vTarget_Instance_id,
                                     cSOB_ID                => vTarget_Set_of_books_id,
                                     cCurrency_Code         => vTarget_Currency_code,
                                     cCommon_Exch_Curr      => vCommon_Exch_Curr,
                                     cTrade_currency        => vTrade_currency,
                                     cInternalErrorCode     => cInternalErrorCode);

             xxcp_wks.I1009_ArrayCH := xxcp_wks.I1009_ArrayTEMP;

              vIC_Tax_Rate  := cIC_Tax_Rate;                      
              If ARRec.Rule_Type = 'T' Then
                 If cInternalErrorCode != gDo_Not_Do_Rule then

                   Set_Internal_Tax_Elements(cSub_Rule_Type     => ARRec.Sub_Rule_Type, 
                                             cInternalErrorCode => cInternalErrorCode);

                   If ARRec.Sub_Rule_Type != '0' then                          
                     vIC_Tax_Rate  :=  xxcp_wks.I1009_ArrayCH(163);
                   End If;
                 End If;
              End If;

              If cInternalErrorCode = gDo_Not_Do_Rule then
               cInternalErrorCode := 0;
              Else

               EXIT WHEN cInternalErrorCode <> 0;

               -- Internal Array
               xxcp_wks.I1009_ArrayCH(38) := cTransaction_Table;
               xxcp_wks.I1009_ArrayCH(39) := cTransaction_type;
               xxcp_wks.I1009_ArrayCH(62) := arrec.rule_category_1;
               xxcp_wks.I1009_ArrayCH(63) := arrec.rule_category_2;
               xxcp_wks.I1009_ArrayCH(96) := arrec.entity_type;
               xxcp_wks.I1009_ArrayCH(145):= arrec.target_suppression_rule;
               xxcp_wks.I1009_ArrayCH(146):= arrec.history_suppression_rule;              
               xxcp_wks.I1009_ArrayCH(147):= arrec.rule_sequence; 

               IF ARRec.transaction_class_replacement IS NOT NULL THEN
                 xxcp_wks.I1009_ArrayCH(40) := ARRec.transaction_class_replacement;
               ELSE
                 xxcp_wks.I1009_ArrayCH(40) := cTransaction_class;
               END IF;

               xxcp_trace.Transaction_Trace(cTrace_id           => 21,
                                            cRule_id            => ARRec.Rule_id,
                                            cRule_Name          => ARRec.rule_name,
                                            cTarget_COA_id      => vTarget_COA_id,
                                            cTarget_Instance_id => vTarget_Instance_id,
                                            cTarget_Type_id     => vTarget_Type_id
                                            );

             -- Work out Initial Value and Currency
             IF ARRec.Entered_Value_Attribute_id IS NOT NULL THEN

              vEntered_Source        := 'ARL';
              vFrom_Entered_Value    := xxcp_foundation.Find_DynamicAttribute(ARRec.Entered_Value_Attribute_id,'S');
              vFrom_Entered_Currency := xxcp_foundation.Find_DynamicAttribute(ARRec.Entered_Curr_Attribute_id,'S');
 
              vFrom_Accounted_Value    := xxcp_foundation.Find_DynamicAttribute(ARRec.Accounted_Value_Attribute_id,'S');
              vFrom_Accounted_Currency := xxcp_foundation.Find_DynamicAttribute(ARRec.Accounted_Curr_Attribute_id,'S');

              IF (vFrom_Entered_Value IS NULL) OR  (vFrom_Entered_Currency) IS NULL THEN
                cInternalErrorCode := 10213;
                xxcp_foundation.FndWriteError(cInternalErrorCode,
                                              'Rule <' ||TO_CHAR(ARRec.Rule_id) ||
                                              '> Entered Value  <' ||TO_CHAR(vFrom_Entered_Value) ||
                                              '> Entered Currency  <' ||vFrom_Entered_Currency ||
                                              '> Entered Value ID <' ||TO_CHAR(ARRec.Entered_Value_Attribute_id) ||
                                              '> Entered Currency ID <' ||TO_CHAR(ARRec.Entered_Curr_Attribute_id) || '> ');
              END IF;
             ELSE

              vFrom_Entered_Value    := vModel_Entered_Amount;
              vFrom_Entered_Currency := vModel_Entered_Currency;
              IF vFrom_Entered_Value IS NULL OR vFrom_Entered_Currency IS NULL THEN
                cInternalErrorCode := 10214;
                IF vEntered_Source = 'PM' THEN
                  -- Price Method
                  vError_Message := 'Pricing Method: Entered Value  <' ||TO_CHAR(vFrom_Entered_Value) ||
                                    '> Entered Currency  <' ||vFrom_Entered_Currency||
                                    '> Entered Value ID <' ||TO_CHAR(mcrec.AC_PRICING_ID) ||'>';
                ELSE
                  vError_Message := 'Source Entered Amount <' ||TO_CHAR(vFrom_Entered_Value) ||
                                    '> Entered Currency <' ||vFrom_Entered_Currency || '>';
                END IF;
                xxcp_foundation.FndWriteError(cInternalErrorCode,vError_Message);
              END IF;

             END IF;

             IF vEntered_Source = 'PM' THEN
               vFrom_Exchange_Rate_Type := nvl(MCRec.Exchange_Rate_Type,vPM_Entered_Exch_Rate_Type);
             ELSE
               vFrom_Exchange_Rate_Type := nvl(MCRec.Exchange_Rate_Type,vTarget_Exch_Rate_Type);
             END IF;

             EXIT WHEN cInternalErrorCode <> 0;
             --
             -- End of Work out Initial Value and Currency
             --

             --
             -- This is to take into account Cross Currency gain and Loss
             -- 26th Jan 2005.
             --
             IF cTransaction_Type = 'CCURR' THEN
               vTo_Entered_Currency := vFrom_Entered_Currency;
             END IF;

             xxcp_wks.I1009_ArrayTEMP  := xxcp_wks.I1009_ArrayCH;
             vTarget_Exch_Date         := vExchange_Date;
             vPerferred_Exchange_Rate := 0;

             If mcrec.Acc_Conversions = 'Y' then
                  -- New Accounted Preferred Currency type
                  If mcrec.Acc_Exchange_Rate_Type_id > 0 then
                    vTarget_Exch_Rate_Type := nvl(xxcp_foundation.Find_DynamicAttribute(mcrec.Acc_Exchange_Rate_Type_id,'S'),vTarget_Exch_Rate_Type);
                    If mcrec.Acc_Exchange_Date_id > 0 then
                      vTarget_Exch_Date := nvl(to_date(xxcp_foundation.Find_DynamicAttribute(mcrec.Acc_Exchange_Date_id,'DD-MON-YYYY'),'S'),vTarget_Exch_Date);
                    End If;
                  End If;
                  -- New Accounted Preferred Rates
                  If ARRec.Entity_type = 'Owner' and mcrec.Acc_Owner_Exch_Rate_id > 0 then
                    vPerferred_Exchange_Rate := xxcp_foundation.Find_DynamicAttribute(mcrec.Acc_Owner_Exch_Rate_id,'S');
                  ElsIf ARRec.Entity_type = 'Partner' and mcrec.Acc_Partner_Exch_Rate_id > 0 then
                    vPerferred_Exchange_Rate := xxcp_foundation.Find_DynamicAttribute(mcrec.Acc_Partner_Exch_Rate_id,'S');
                  End If;
             End If;
             
            ---keith 2

             xxcp_te_values.DR_CR(   cAction                  => 'S',
                                     cTo_Entered_Currency     => vTo_Entered_Currency,
                                     cTransaction_Quantity    => cTransaction_Quantity,
                                     --  Value and Currency
                                     cFrom_Entered_Value      => vFrom_Entered_Value,
                                     cFrom_Entered_Currency   => vFrom_Entered_Currency,
                                     cFrom_Exch_Rate_Type     => vFrom_Exchange_Rate_Type,
                                     -- Model Control Info
                                     cRule_Value_Name         => ARRec.Rule_Value_Name,
                                     cChange_Sign             => ARRec.Change_Sign,
                                     cEntity_type             => ARRec.Entity_type,
                                     cRule_name               => ARRec.Rule_name,
                                     -- Default Values
                                     cOwner_Common_Exch_Curr  => cOwner_Common_Exch_Curr,
                                     cExchange_Date           => vTarget_Exch_Date,
                                     -- Adjustment Rate id
                                     cAdjustment_Rate         => vIC_Adjustment_Rate,
                                     cTax_Rate                => vIC_Tax_Rate,
                                     -- Targets
                                     cTarget_Set_of_books_id  => vTarget_Set_of_books_id,
                                     cTarget_Exch_Rate_Type   => vTarget_Exch_Rate_Type,
                                     cAccounted_Currency      => vTarget_Currency_Code,
                                     --
                                     cModel_Name              => mcrec.Model_Name,
                                     cModel_Ctl_id            => mcrec.Model_ctl_id,
                                     --
                                     cPerferred_Exchange_Rate => vPerferred_Exchange_Rate,
                                     cFrom_Accounted_Value    => vFrom_Accounted_Value,
                                     cFrom_Accounted_Currency => vFrom_Accounted_Currency,
                                     --
                                     cUse_Extended_Precision  => mcrec.extended_precision,
                                     cTo_Entered_Amount       => vTo_Entered_Value,
                                     cTo_Accounted_Amount     => vTo_Accounted_Value,
                                     cInternalErrorCode       => cInternalErrorCode);

             EXIT WHEN cInternalErrorCode <> 0;

             --! ************************************************************************************
             --!        Surpression Logic
             --! ************************************************************************************

             If nvl(vTo_Entered_Value,0) + nvl(vTo_Accounted_Value,0) = 0 then
                If  ARRec.Zero_Suppression_Rule = 'N' then
                  vProcess_Switch := FALSE; -- Do not do it
                ElsIf ARRec.Zero_Suppression_Rule = 'E' and vTarget_Zero_Flag = 'N' then
                  vProcess_Switch := FALSE; -- Do not do it
                End If;
             End If;

            -- Balancing
            vTarget_Rounding := ARRec.Target_Rounding;

             --! ************************************************************************************
             --!        ONCE RULE
             --! ************************************************************************************
             If ARRec.Rule_Type = 'O' AND vProcess_Switch = TRUE then
               xxcp_wks.ONCE_RCD(vOnce_Cnt+1).Model_Name_id     := vModel_Name_id;
               xxcp_wks.ONCE_RCD(vOnce_Cnt+1).Transaction_Table := cTransaction_Table;
               xxcp_wks.ONCE_RCD(vOnce_Cnt+1).Transaction_Type  := cTransaction_Type;
               xxcp_wks.ONCE_RCD(vOnce_Cnt+1).Rule_id           := ARRec.Rule_id;
               xxcp_wks.ONCE_RCD(vOnce_Cnt+1).Trading_Set_id    := xxcp_global.gCommon(1).current_Trading_Set_id;
               xxcp_wks.ONCE_RCD(vOnce_Cnt+1).Break_Value1      := vOnce_Break_Value1;
               xxcp_wks.ONCE_RCD(vOnce_Cnt+1).Break_Value2      := vOnce_Break_Value2;
               xxcp_wks.ONCE_RCD(vOnce_Cnt+1).Break_Value3      := vOnce_Break_Value3;
               xxcp_wks.ONCE_RCD(vOnce_Cnt+1).Break_Value4      := vOnce_Break_Value4;
                   
             ElsIf ARRec.Rule_Type != 'O' AND  vProcess_Switch = FALSE then
                If xxcp_global.gCOMMON(1).preview_zero_flag = 'Y' then
                  vProcess_Switch := TRUE;
                End If;
             End If;

             --
             -- Populate "Value Sign" Internal Attribute (02.06.07)
             --
             If sign(vTo_Entered_Value) = 1 then
               xxcp_wks.I1009_ArrayTEMP(136) := 'Positive';
             Elsif sign(vTo_Entered_Value) = -1 then
               xxcp_wks.I1009_ArrayTEMP(136) := 'Negative';
             Else -- 0
               If sign(vTo_Accounted_Value) = 1 then
                 xxcp_wks.I1009_ArrayTEMP(136) := 'Positive';
               Elsif sign(vTo_Accounted_Value) = -1 then
                 xxcp_wks.I1009_ArrayTEMP(136) := 'Negative';
               Else -- 0
                 xxcp_wks.I1009_ArrayTEMP(136) := 'Zero';
               End if;
             End if;

             xxcp_wks.I1009_Array(136)   := xxcp_wks.I1009_ArrayTEMP(136);
             xxcp_wks.I1009_ArrayCH(136) := xxcp_wks.I1009_ArrayTEMP(136);

             --! ************************************************************************************
             --!        vProcess_Switch
             --! ************************************************************************************

             If vProcess_Switch = TRUE then

               Get_Segment_Rules(
                                cAction                  => 'S',
                                cRule_id                 => ARRec.Rule_id,
                                cRule_Name               => ARRec.Rule_Name,
                                cChart_of_accounts_id    => vTarget_COA_id,
                                cInstance_id             => vTarget_Instance_id,
                                cEnforce_Accounting      => ARRec.Enforce_Accounting,
                                cTarget_Cross_Validation => ARRec.Target_Cross_Validation,
                                cEffective_Date          => cTransaction_Date,
                                -- NCM 02.05.08
                                cEntity_type             => ARRec.Entity_Type,
                                cInternalErrorCode       => cInternalErrorCode);

              xxcp_wks.I1009_ArrayCH := xxcp_wks.I1009_ArrayTEMP;

              EXIT WHEN cInternalErrorCode <> 0;

              Get_Column_Attributes(cAction              => 'S',
                                    cTarget_Table        => ARRec.target_table,
                                    cTarget_Instance_id  => vTarget_Instance_id,
                                    cModel_Name_id       => vModel_Name_ID,
                                    cInternalErrorCode   => cInternalErrorCode);

              xxcp_wks.D1006_ArrayCH := xxcp_wks.D1006_Array;

              EXIT WHEN cInternalErrorCode <> 0;
              

              -- Apply Column Rules
               Column_Rules(
                           cRule_type              => ARRec.Rule_Type,
                           cRule_id                => ARRec.Rule_id,
                           cModel_Ctl_id           => mcrec.Model_Ctl_id,
                           cTarget_Table           => ARRec.Target_Table,
                           cAttribute_id           => cAttribute_id,
                           cInterface_ID           => cInterface_id,
                           cTarget_Set_of_books_id => vTarget_Set_of_books_id,
                           cTransaction_Class      => cTransaction_Class,
                           cTarget_Type_id         => vTarget_Type_id,
                           cTarget_Record_Status   => ARRec.Target_Record_Status,
                           cTarget_Rounding        => vTarget_Rounding,
                           cRound_Suppressed_Rules => ARRec.Round_Suppressed_Rules,
                           cTarget_Reg_id          => vTarget_Reg_id,
                           cNo_Rounding_Rule       => ARRec.No_Rounding_Rule,
                           cBalancing_Segment      => vBalancing_Segment,
                           cRounding_Group         => ARRec.Rounding_Group,
                           cRule_Entity_Type       => ARRec.Entity_type,
                           cTarget_Assignment_id   => vTarget_Assignment_id,
                           cTarget_Zero_Flag       => vTarget_Zero_Flag,
                           cTax_Registration_id    => vTax_registration_id,
                           cTarget_Instance_id     => vTarget_Instance_id,
                           cTarget_COA_id          => vTarget_COA_id,
                           cZero_Suppression_Rule  => ARRec.Zero_Suppression_Rule,
                           cCost_Plus_Rule         => ARRec.Cost_Plus_Rule,
                           cExtended_Values        => ARRec.Extended_Values,
                           cEntered_Amounts        => ARRec.Entered_Amounts,
                           cAccounted_Amounts      => ARRec.Accounted_Amounts,
                           -- 03.06.20
                           cSettlement_rule        => ARRec.Report_Settlement_Rule,
                           cReporting_field1       => ARRec.Reporting_Field1,
                           cReporting_field2       => ARRec.Reporting_Field2,
                           cReporting_field3       => ARRec.Reporting_Field3,
                           cReporting_field4       => ARRec.Reporting_Field4,
                           cReporting_field5       => ARRec.Reporting_Field5
                           );

               IF cInternalErrorCode <> 0 THEN
                 xxcp_foundation.FndWriteError(cInternalErrorCode,vError_Message);
               END IF;

               EXIT WHEN cInternalErrorCode <> 0;

              End If; -- vProcess_Switch
             End IF; -- Do not do rule

            End IF; -- Once rule end

            -- Reset Tax Rate
            xxcp_wks.I1009_ArrayCH(67) := cIC_Tax_Rate;  

          END LOOP; -- End Account Rules Loop
        ELSE
          xxcp_foundation.Set_InternalErrorCode(cInternalErrorCode, 10202);
        END IF;
        -- End Create Interface Row
      END IF;

    END LOOP; -- Model Control

    IF (vStd_Rule_Found = FALSE) AND (cCreate_Child_trx = 'R') AND cInternalErrorCode = 0 THEN
      cInternalErrorCode := 10217;
      xxcp_foundation.FndWriteError(cInternalErrorCode,
                                    'Trading Relationship <' ||TO_CHAR(cEntity_Set_id) ||
                                    '> Transaction Set <' ||TO_CHAR(cTransaction_set_id) ||
                                    '> Model Ctl Id <'||xxcp_wks.vBase_ArrayCH(57)||
                                    '> Model Name <'||xxcp_wks.vBase_ArrayCH(26)||
                                    '> TRX Qualifier 1 <' || cTRX_Qualifier1 ||
                                    '> Qualifier 2 <' || cTRX_Qualifier2 ||
                                    '> Transaction Date <' ||TO_CHAR(cTransaction_Date)||
                                    '> Transaction Table <' ||cTransaction_Table ||
                                    '> Type <' ||cTransaction_Type ||
                                    '> Class <' ||cTransaction_Class || '>');
    END IF;

  END Process_Standard;

  --
  -- ## ***********************************************************************************************************
  -- ##
  -- ##                                Process_Control
  -- ##
  -- ## ***********************************************************************************************************
  --
  PROCEDURE Process_Control(cSource_Assignment_id    IN NUMBER,
                            cSource_id               IN NUMBER,
                            cTransaction_Table       IN VARCHAR2,
                            cTransaction_Type        IN VARCHAR2,
                            cTransaction_Class       IN VARCHAR2,
                            cQuantity                IN NUMBER,
                            cTransaction_id          IN VARCHAR2,
                            cOwner_Tax_Reg_id        IN NUMBER,
                            cPartner_Tax_Reg_id      IN NUMBER,
                            cTrading_Set             IN VARCHAR2,
                            cTransaction_Currency    IN VARCHAR2,
                            cSet_of_books_id         IN NUMBER,
                            cAttribute_ID            IN NUMBER,
                            cParent_Trx_ID           IN NUMBER,
                            cTransaction_Date        IN DATE,
                            cMC_Qualifier1           IN VARCHAR2,
                            cMC_Qualifier2           IN VARCHAR2,
                            cMC_Qualifier3           IN VARCHAR2,
                            cMC_Qualifier4           IN VARCHAR2,
                            cTRX_Qualifier1          IN VARCHAR2,
                            cTRX_Qualifier2          IN VARCHAR2,
                            cED_Qualifier1           IN VARCHAR2,
                            cED_Qualifier2           IN VARCHAR2,
                            cED_Qualifier3           IN VARCHAR2,
                            cED_Qualifier4           IN VARCHAR2,
                            cExchange_date           IN DATE,
                            cTransaction_Quantity    IN NUMBER,
                            cIC_Unit_Price           IN NUMBER,
                            cIC_Price                IN NUMBER,
                            cIC_Unit_Currency        IN VARCHAR2,
                            cIC_Adjustment_Rate      IN NUMBER,
                            cIC_Tax_Rate             IN NUMBER,
                            cIC_Tax_Control          IN VARCHAR2,
                            --
                            cSourceRowid             IN ROWID,
                            cParent_Entered_Amount   IN NUMBER,
                            cParent_Entered_Currency IN VARCHAR2,
                            cInterface_ID            IN NUMBER,
                            --
                            cTransaction_set_id      IN NUMBER,
                            -- NCM 02.05.07
                            cOwner_Assign_rule_id    IN NUMBER,
                            cInternalErrorCode IN OUT NOCOPY NUMBER) IS

    CURSOR EP(pSource_id IN NUMBER, pOwner_Tax_Reg_id IN NUMBER, pPartner_Tax_reg_id IN NUMBER, pTransaction_Date in Date,
              pOwner_Tax_Reg_Group_id in number, pPartner_Tax_Reg_Group_id in number) IS
      SELECT ep.Entity_Set_id Entity_Partnership_id, tc.set_name Trading_Relationship_Name
        FROM xxcp_entity_partnerships ep,
             xxcp_table_set_ctl tc
       WHERE (ep.Owner_Tax_Reg_id   = pOwner_Tax_Reg_id
               or nvl(ep.owner_Tax_Reg_Group_id,0) =nvl( pOwner_Tax_Reg_Group_id,0))
         AND (ep.Partner_Tax_Reg_id = pPartner_Tax_Reg_id
           or nvl(ep.Partner_Tax_Reg_Group_id,0) =nvl( pPartner_Tax_Reg_Group_id,0))
         AND nvl(ep.source_id,0) IN (0,pSource_id)
         AND ep.active = 'Y'
         AND Decode(tc.attribute1,'Y',Sign( pOwner_Tax_reg_id- pPartner_Tax_reg_id),1) != 0
         AND pTransaction_Date between nvl(ep.effective_from_date, '01-JAN-1900') AND nvl(ep.effective_to_date, '31-DEC-2999')
         and ep.entity_set_id  = tc.set_id
         AND tc.set_base_table = 'CP_ENTITY_PARTNERSHIPS'
         ORDER BY ep.source_id DESC, decode(ep.owner_tax_Reg_id,0,0,10), decode(ep.partner_tax_Reg_id,0,0,10);

   -- NCM 02.05.07
   -- This cursor gets the default trading relationship if specified.
   CURSOR EPD (pDflt_trading_rel_name in varchar2) IS
      SELECT set_id Entity_Partnership_id, tc.set_name Trading_Relationship_Name
        FROM xxcp_table_set_ctl tc
       WHERE tc.set_name = pDflt_trading_rel_name
         AND tc.set_base_table = 'CP_ENTITY_PARTNERSHIPS'
         AND tc.source_id = 0;

    vCreate_Local_trx         VARCHAR2(1);
    vCreate_IC_Trx            VARCHAR2(1);
    vCreate_GainLoss_Trx      VARCHAR2(1);
    vCreate_Child_trx         VARCHAR2(1);
    vCreate_Tax_Results       VARCHAR2(1);
    vTransaction_set_id       NUMBER(10);
    vEntity_Partnership_id    NUMBER;
    vOwner_Tax_Reg_Id         XXCP_TAX_REGISTRATIONS.tax_registration_id%TYPE;
    vOwner_Legal_Currency     XXCP_TAX_REGISTRATIONS.legal_currency%TYPE;
    vOwner_Common_Exch_Curr   XXCP_TAX_REGISTRATIONS.legal_currency%TYPE;
    vOwner_Exch_Rate_Type     XXCP_TAX_REGISTRATIONS.exchange_rate_type%TYPE;
    vOwner_Trade_Currency     XXCP_TAX_REGISTRATIONS.legal_currency%TYPE;
    vPartner_Tax_Reg_id       XXCP_TAX_REGISTRATIONS.tax_registration_id%TYPE;
    vPartner_Legal_Currency   XXCP_TAX_REGISTRATIONS.legal_currency%TYPE;
    vPartner_Common_Exch_Curr XXCP_TAX_REGISTRATIONS.legal_currency%TYPE;
    vPartner_Exch_Rate_Type   XXCP_TAX_REGISTRATIONS.exchange_rate_type%TYPE;
    vPartner_Trade_Currency   XXCP_TAX_REGISTRATIONS.legal_currency%TYPE;

    x PLS_INTEGER;

    CURSOR DFX(pSource_id IN NUMBER, pTransaction_set_id IN NUMBER, pTrading_Set_id IN NUMBER, pOwner_Assign_Rule_id IN NUMBER) IS
         SELECT x.create_local_trx
               ,x.create_ic_trx
               ,x.create_child_trx
               ,x.create_gainloss_trx
               ,x.create_tax_results
               ,x.assignment_set_id
               ,x.sql_build_id
               ,x.create_trx_header
               ,x.create_trx_attributes
               ,s.trading_set
               ,a.dflt_trading_relationship
               ,nvl(x.rank,1) Rank
          FROM XXCP_ASSIGNMENT_CTL x,
               XXCP_ASSIGNMENT_QUALIFIERS a,
               XXCP_ASSIGNMENT_RULES r,
               XXCP_TRADING_SETS   s
         WHERE x.source_id            = pSource_id
           -- NCM 02.05.07
           AND x.assignment_set_id    = a.assignment_set_id
           AND a.assignment_qualifier_id = r.assignment_qualifier_id
           AND r.assignment_rule_id   = pOwner_assign_rule_id
           AND x.transaction_set_id   = pTransaction_set_id
           AND x.trading_set_id       = s.trading_set_id
           AND s.trading_set_id       = pTrading_set_id
           AND s.source_id            = x.source_id
           AND x.Active               = 'Y'
      ORDER BY s.SEQUENCE, x.Trading_Set_id,nvl(x.rank,1);

  DFXRec DFX%ROWTYPE;

  vRuleRequired              varchar2(1);
  vDflt_trading_relationship varchar2(1);
  vCreate_Tax_Rules          varchar2(1) := 'N';

  BEGIN

    cInternalErrorCode := 10501;

    xxcp_global.gCommon(1).Current_Attribute_id := cAttribute_ID;
    xxcp_wks.I1009_Array(35) := cAttribute_id; -- Transaction Attribute Unique ID

    xxcp_trace.Transaction_Trace( cTrace_id => 26,
                                  cTransaction_Type => cTransaction_type,
                                  cTransaction_Date => cTransaction_date);


    FOR z IN 1 .. 1
    LOOP

     OPEN DFX(xxcp_global.gCommon(1).source_id,cTransaction_Set_id, xxcp_global.gCommon(1).current_Trading_Set_id, cOwner_assign_rule_id);
     LOOP
        FETCH DFX INTO DFXRec;
        EXIT WHEN DFX%NOTFOUND;

        vRuleRequired      := 'N';
        cInternalErrorCode := 0;

        -- NCM 02.05.07
        vDflt_trading_relationship := DFXRec.dflt_trading_relationship;

        IF (DFXRec.Create_Local_Trx IN ('Y') OR DFXRec.Create_Child_trx IN ('Y','R') OR DFXRec.Create_gainloss_trx = 'Y') THEN
             vCreate_Local_trx    := DFXRec.Create_Local_Trx;
             vCreate_IC_Trx       := DFXRec.Create_IC_Trx;
             vCreate_GainLoss_Trx := DFXRec.create_gainloss_trx;
             vCreate_Child_trx    := DFXRec.Create_Child_trx;
             vCreate_Tax_Results  := DFXRec.Create_Tax_Results;
             vTransaction_set_id  := cTransaction_set_id;
             vRuleRequired := 'Y';
             EXIT;
        ELSE
         EXIT;
        END IF;

      END LOOP;
      CLOSE DFX;

      EXIT WHEN cInternalErrorCode <> 0;

      xxcp_trace.Transaction_Trace(cTrace_id => 24, cRuleRequired => vRuleRequired);

      IF vRuleRequired = 'Y' THEN

       vOwner_Tax_Reg_id   := cOwner_Tax_Reg_Id;
       vPartner_Tax_Reg_id := cPartner_Tax_Reg_Id;

      -- Trading Relationship

      -- Work out the Trading Relationship
      cInternalErrorCode := 
         xxcp_te_base.Get_Trading_Relationship(
                                    cAction_Type               => 'T',
                                    cSource_id                 => xxcp_global.gCOMMON(1).Source_id,
                                    cOwner_Tax_Reg_id          => vOwner_Tax_Reg_id, 
                                    cPartner_Tax_Reg_Id        => vPartner_Tax_Reg_Id,
                                    cTransaction_Date          => cTransaction_Date,
                                    cCreate_IC_Trx             => 'N',
                                    cDflt_Trading_Relationship => NVL(vDflt_trading_relationship,'N'),
                                    cEntity_Partnership_id     => vEntity_Partnership_id);

        xxcp_trace.Transaction_Trace(cTrace_id              => 25,
                                     cOwnerTaxid            => vOwner_Tax_Reg_id,
                                     cPartnerTaxid          => vPartner_Tax_Reg_id,
                                     cEntity_Partnership_id => vEntity_Partnership_id,
                                     cTransaction_Set_id    => cTransaction_Set_id,
                                     cTrading_Set           => cTrading_Set,
                                     cMC_Qualifier1         => cMC_Qualifier1,
                                     cMC_Qualifier2         => cMC_Qualifier2,
                                     cMC_Qualifier3         => cMC_Qualifier3,
                                     cMC_Qualifier4         => cMC_Qualifier4
                                     );

        EXIT WHEN cInternalErrorCode <> 0;

        -- Owner
        IF nvl(vOwner_Tax_Reg_id, 0) > 0 THEN
          xxcp_Memory_Pack.FindTaxRegOnlyInfo(vOwner_Tax_Reg_id,
                                              vOwner_Legal_Currency,
                                              vOwner_Trade_Currency,
                                              vOwner_Common_Exch_Curr,
                                              vOwner_Exch_Rate_type,
                                              xxcp_wks.I1009_Array(60),
                                              xxcp_wks.I1009_Array(100)
                                              );
        END IF;

        -- Partner
        IF nvl(vPartner_Tax_Reg_id, 0) > 0 THEN
          xxcp_Memory_Pack.FindTaxRegOnlyInfo(vPartner_Tax_Reg_id,
                                              vPartner_legal_Currency,
                                              vPartner_Trade_Currency,
                                              vPartner_Common_Exch_Curr,
                                              vPartner_Exch_Rate_type,
                                              xxcp_wks.I1009_Array(61),
                                              xxcp_wks.I1009_Array(101)
                                              );
        END IF;

        -- End of Internal Array filling..

        -- Populate Internal Array for use by other dynamic SQL substitutions
        xxcp_wks.I1009_Array(01)  := vOwner_Tax_Reg_id;
        xxcp_wks.I1009_Array(02)  := vPartner_Tax_Reg_id;
        xxcp_wks.I1009_Array(03)  := vOwner_Legal_Currency;
        xxcp_wks.I1009_Array(04)  := vPartner_legal_Currency;
        xxcp_wks.I1009_Array(22)  := cTransaction_Currency;
        xxcp_wks.I1009_Array(28)  := cTransaction_id;
        xxcp_wks.I1009_Array(30)  := cIC_Unit_Currency;
        xxcp_wks.I1009_Array(31)  := cIC_Unit_Price;
        xxcp_wks.I1009_Array(102) := cIC_Price;
        xxcp_wks.I1009_Array(33)  := cSet_of_books_id;
        xxcp_wks.I1009_Array(35)  := cAttribute_id; -- Transaction Attribute Unique ID
        xxcp_wks.I1009_Array(36)  := cParent_Trx_id; -- Transaction Parent Trx ID
        xxcp_wks.I1009_Array(55)  := cTransaction_Quantity;
        xxcp_wks.I1009_Array(65)  := cIC_Adjustment_Rate;
        xxcp_wks.I1009_Array(67)  := cIC_Tax_Rate;
        xxcp_wks.I1009_Array(29)  := cTransaction_Currency;

        xxcp_wks.vBase_Internal_Array :=  xxcp_wks.I1009_Array;
        
        --
        -- Load Tax Results into memory
        -- to be used by Formula Sets
        --
        If cIC_Tax_Rate is not Null then
          vCreate_Tax_Rules := 'Y';
        End If;
        
        If vCreate_Tax_Results = 'Y' and cIC_Tax_Control in ('O','P') then        
           --           
           xxcp_te_base.Calc_Tax_Rates(  
                        cSourceRowid            => cSourceRowid,
                        cTransaction_id         => cTransaction_id,
                        cTransaction_Date       => cTransaction_Date,
                        cOwnerTaxid             => cOwner_Tax_Reg_id,
                        cPartnerTaxId           => cPartner_Tax_Reg_id,
                        cIC_Control             => cIC_Tax_Control, 
                        cTransaction_Set_id     => cTransaction_Set_id,
                        cExchange_Date          => cExchange_Date,
                        cTaxable_Amount         => cIC_Price,
                        cTaxable_Currency       => cIC_Unit_Currency,
                        cInternalErrorCode      => cInternalErrorCode);           
                        
           If xxcp_te_base.gTax_Record_Set.Count > 0 then
             vCreate_Tax_Rules := 'Y';
           End If;
        End If;
        
        -- Standard Rules
        IF (vCreate_Child_Trx IN ('R', 'Y')) THEN

          Process_Standard(cSet_of_books_id         => cSet_of_books_id,
                           cSourceRowid             => cSourceRowid,
                           cCreate_Child_trx        => vCreate_Child_Trx,
                           cCreate_Tax_Rules        => vCreate_Tax_Rules,
                           cTransaction_id          => cTransaction_id,
                           cTransaction_Table       => cTransaction_Table,
                           cTransaction_Type        => cTransaction_Type,
                           cTransaction_Class       => cTransaction_Class,
                           cQuantity                => cQuantity,
                           cEntity_Set_id           => vEntity_Partnership_id,
                           cTransaction_set_id      => vTransaction_set_id,
                           cTRX_Qualifier1          => cTRX_Qualifier1,
                           cTRX_Qualifier2          => cTRX_Qualifier2,
                           cED_Qualifier1           => cED_Qualifier1,
                           cED_Qualifier2           => cED_Qualifier2,
                           cED_Qualifier3           => cED_Qualifier3,
                           cED_Qualifier4           => cED_Qualifier4,
                           cTransaction_Currency    => cTransaction_Currency,
                           cTransaction_Quantity    => cTransaction_Quantity,
                           cTransaction_Date        => cTransaction_Date,
                           cOwner_Tax_Reg_id        => vOwner_Tax_Reg_id,
                           cPartner_Tax_Reg_id      => vPartner_Tax_Reg_id,
                           cOwner_Common_Exch_Curr  => vOwner_Common_Exch_Curr,
                           cExchange_Date           => cExchange_date,
                           cOwner_Legal_Currency    => vOwner_Legal_Currency,
                           cOwner_Trade_Currency    => vOwner_Trade_Currency,
                           cPartner_Legal_Currency  => vPartner_Legal_Currency,
                           cPartner_Trade_Currency  => vPartner_Trade_Currency,
                           cAttribute_id            => cAttribute_id,
                           cInterface_id            => cInterface_id,
                           cIC_Tax_Rate             => cIC_Tax_Rate,
                           cIC_Adjustment_Rate      => cIC_Adjustment_Rate,
                           cParent_Entered_Amount   => cParent_Entered_Amount,
                           cParent_Entered_Currency => cParent_Entered_Currency,
                           cBase_SQL_Build_ID       => 0,
                           -- OUT
                           cInternalErrorCode       => cInternalErrorCode);

            EXIT WHEN cInternalErrorCode <> 0;
        END IF;
        -- Gain and Loss
        IF (vCreate_GainLoss_Trx = 'Y') THEN

            Process_Gain_Loss(cAction                 => 'B',
                              cSet_of_books_id        => cSet_of_books_id,
                              cSourceRowid            => cSourceRowid,
                              cTransaction_id         => cTransaction_id,
                              cTransaction_Table      => cTransaction_Table,
                              cTransaction_Type       => cTransaction_Type,
                              cTransaction_Class      => cTransaction_Class,
                              cQuantity               => cQuantity,
                              cEntity_Set_id          => vEntity_Partnership_id,
                              cTransaction_set_id     => vTransaction_set_id,
                              cTRX_Qualifier1         => cTRX_Qualifier1,
                              cTRX_Qualifier2         => cTRX_Qualifier2,
                              cTransaction_Currency   => cTransaction_Currency,
                              cEffective_Date         => cExchange_Date,
                              cOwner_Tax_Reg_id       => cOwner_Tax_Reg_id,
                              cPartner_Tax_Reg_id     => cPartner_Tax_Reg_id,
                              cAttribute_id           => cAttribute_id,
                              cInterface_id           => cInterface_id,
                              cOwner_Common_Exch_Curr => vOwner_Common_Exch_Curr,
                              cBase_SQL_Build_ID      => 0,
                              cInternalErrorCode      => cInternalErrorCode);

            EXIT WHEN cInternalErrorCode <> 0;
        END IF; -- End Gain and Loss
        -- Local
        IF vCreate_Local_Trx = 'Y' THEN

            Process_Local(
                          cSet_of_books_id         => cSet_of_books_id,
                          cSourceRowid             => cSourceRowid,
                          cCreate_Local_Trx        => vCreate_Local_Trx,
                          cTransaction_id          => cTransaction_id,
                          cTransaction_Table       => cTransaction_Table,
                          cTransaction_Type        => cTransaction_Type,
                          cTransaction_Class       => cTransaction_Class,
                          cQuantity                => cQuantity,
                          cEntity_Set_id           => vEntity_Partnership_id,
                          cTransaction_set_id      => vTransaction_set_id,
                          cED_Qualifier1           => cED_Qualifier1,
                          cED_Qualifier2           => cED_Qualifier2,
                          cED_Qualifier3           => cED_Qualifier3,
                          cED_Qualifier4           => cED_Qualifier4,
                          cTRX_Qualifier1          => cTRX_Qualifier1,
                          cTRX_Qualifier2          => cTRX_Qualifier2,
                          cTransaction_Currency    => cTransaction_Currency,
                          cTransaction_Quantity    => cTransaction_Quantity,
                          cTransaction_Date        => cTransaction_Date,
                          cOwner_Tax_Reg_id        => vOwner_Tax_Reg_id,
                          cPartner_Tax_Reg_id      => vPartner_Tax_Reg_id,
                          cOwner_Common_Exch_Curr  => vOwner_Common_Exch_Curr,
                          cExchange_Date           => cExchange_Date,
                          --
                          cOwner_Legal_Currency    => vOwner_Legal_Currency,
                          cPartner_Legal_Currency  => vPartner_Legal_Currency,
                          cAttribute_id            => cAttribute_id,
                          cInterface_ID            => cInterface_ID,
                          cIC_Tax_Rate             => cIC_Tax_Rate,
                          cIC_Adjustment_Rate      => cIC_Adjustment_Rate,
                          --
                          cParent_Entered_Amount   => cParent_Entered_Amount,
                          cParent_Entered_Currency => cParent_Entered_Currency,
                          -- OUT
                          cInternalErrorCode       => cInternalErrorCode);

             EXIT WHEN cInternalErrorCode <> 0;

        END IF;

        EXIT;
      ELSE
        -- No Transaction Rule Required
        EXIT;
      END IF;
    END LOOP;

    IF cInternalErrorCode = 10501 THEN
      xxcp_foundation.FndWriteError(cInternalErrorCode,
                                    'Transaction Table <' ||cTransaction_Table ||
                                    '> Transaction Type <' ||cTransaction_type ||
                                    '> Trading Set <' || cTrading_Set ||
                                    '> Source Assignment id <' ||TO_CHAR(cSource_Assignment_id) || '>');
    END IF;

  END Process_Control;

  --
  -- ## ***********************************************************************************************************
  -- ##                                Get_Transaction_Cache
  -- ## ***********************************************************************************************************
  --
  PROCEDURE Get_Transaction_Cache(cAttribute_id IN NUMBER ) IS
    CURSOR TC(pAttribute_id IN NUMBER) IS
      SELECT SEQ,
             CACHED_QTY,
             CACHED_VALUE1,CACHED_VALUE2,CACHED_VALUE3,CACHED_VALUE4,CACHED_VALUE5,
             CACHED_VALUE6,CACHED_VALUE7,CACHED_VALUE8,CACHED_VALUE9,CACHED_VALUE10,
             CACHED_VALUE11,CACHED_VALUE12,CACHED_VALUE13,CACHED_VALUE14,CACHED_VALUE15,
             CACHED_VALUE16,CACHED_VALUE17,CACHED_VALUE18,CACHED_VALUE19,CACHED_VALUE20,
             CACHED_VALUE21,CACHED_VALUE22,CACHED_VALUE23,CACHED_VALUE24,CACHED_VALUE25,
             CACHED_VALUE26,CACHED_VALUE27,CACHED_VALUE28,CACHED_VALUE29,CACHED_VALUE30,
             CACHED_VALUE31,CACHED_VALUE32,CACHED_VALUE33,CACHED_VALUE34,CACHED_VALUE35,
             CACHED_VALUE36,CACHED_VALUE37,CACHED_VALUE38,CACHED_VALUE39,CACHED_VALUE40,
             CACHED_VALUE41,CACHED_VALUE42,CACHED_VALUE43,CACHED_VALUE44,CACHED_VALUE45,
             CACHED_VALUE46,CACHED_VALUE47,CACHED_VALUE48,CACHED_VALUE49,CACHED_VALUE50
        FROM XXCP_TRANSACTION_CACHE
       WHERE attribute_id = pAttribute_id;

    TCRec TC%ROWTYPE;

    ----
    CURSOR TCP(pAttribute_id IN NUMBER) IS
      SELECT SEQ,
             CACHED_QTY,
             CACHED_VALUE1,CACHED_VALUE2,CACHED_VALUE3,CACHED_VALUE4,CACHED_VALUE5,
             CACHED_VALUE6,CACHED_VALUE7,CACHED_VALUE8,CACHED_VALUE9,CACHED_VALUE10,
             CACHED_VALUE11,CACHED_VALUE12,CACHED_VALUE13,CACHED_VALUE14,CACHED_VALUE15,
             CACHED_VALUE16,CACHED_VALUE17,CACHED_VALUE18,CACHED_VALUE19,CACHED_VALUE20,
             CACHED_VALUE21,CACHED_VALUE22,CACHED_VALUE23,CACHED_VALUE24,CACHED_VALUE25,
             CACHED_VALUE26,CACHED_VALUE27,CACHED_VALUE28,CACHED_VALUE29,CACHED_VALUE30,
             CACHED_VALUE31,CACHED_VALUE32,CACHED_VALUE33,CACHED_VALUE34,CACHED_VALUE35,
             CACHED_VALUE36,CACHED_VALUE37,CACHED_VALUE38,CACHED_VALUE39,CACHED_VALUE40,
             CACHED_VALUE41,CACHED_VALUE42,CACHED_VALUE43,CACHED_VALUE44,CACHED_VALUE45,
             CACHED_VALUE46,CACHED_VALUE47,CACHED_VALUE48,CACHED_VALUE49,CACHED_VALUE50
        FROM XXCP_PV_TRANSACTION_CACHE
       WHERE attribute_id = pAttribute_id
         AND preview_id   = xxcp_global.gCommon(1).Preview_id;

    TCPRec TCP%ROWTYPE;

    CURSOR TCForecast(pAttribute_id IN NUMBER) IS
      SELECT SEQ,
             CACHED_QTY,
             CACHED_VALUE1,CACHED_VALUE2,CACHED_VALUE3,CACHED_VALUE4,CACHED_VALUE5,
             CACHED_VALUE6,CACHED_VALUE7,CACHED_VALUE8,CACHED_VALUE9,CACHED_VALUE10,
             CACHED_VALUE11,CACHED_VALUE12,CACHED_VALUE13,CACHED_VALUE14,CACHED_VALUE15,
             CACHED_VALUE16,CACHED_VALUE17,CACHED_VALUE18,CACHED_VALUE19,CACHED_VALUE20,
             CACHED_VALUE21,CACHED_VALUE22,CACHED_VALUE23,CACHED_VALUE24,CACHED_VALUE25,
             CACHED_VALUE26,CACHED_VALUE27,CACHED_VALUE28,CACHED_VALUE29,CACHED_VALUE30,
             CACHED_VALUE31,CACHED_VALUE32,CACHED_VALUE33,CACHED_VALUE34,CACHED_VALUE35,
             CACHED_VALUE36,CACHED_VALUE37,CACHED_VALUE38,CACHED_VALUE39,CACHED_VALUE40,
             CACHED_VALUE41,CACHED_VALUE42,CACHED_VALUE43,CACHED_VALUE44,CACHED_VALUE45,
             CACHED_VALUE46,CACHED_VALUE47,CACHED_VALUE48,CACHED_VALUE49,CACHED_VALUE50
        FROM XXCP_FC_TRANSACTION_CACHE
       WHERE attribute_id = pAttribute_id;

    TCForecastRec TCForecast%ROWTYPE;
    --
    -- ## **************************************************
    -- ##
    -- ##        Distribute_Array
    -- ##
    -- ## **************************************************
    --
    PROCEDURE Distribute_Array(cCA_Value IN VARCHAR2) IS

      vCA_ID PLS_INTEGER;

    BEGIN

      BEGIN
       IF cCA_Value IS NOT NULL THEN

        vCA_ID := TO_NUMBER(SUBSTR(cCA_Value, 1, 4));

        IF vCA_ID BETWEEN 1000 AND 1999 THEN
          xxcp_wks.gDis_1001_Array(vCA_ID - 1000) := SUBSTR(cCA_Value, 5, 250);
        ELSIF vCA_ID BETWEEN 2000 AND 2999 THEN
          xxcp_wks.gDis_1002_Array(vCA_ID - 2000) := SUBSTR(cCA_Value, 5, 250);
        END IF;
       END IF;

       EXCEPTION WHEN OTHERS THEN
            xxcp_foundation.FndWriteError(12562,'Transaction Cache Error: Value <'||cCA_Value||'>');
            RAISE_APPLICATION_ERROR(-22562,'Error in Transaction Cache (12562)');
      END;
    END Distribute_Array;

  BEGIN

    xxcp_wks.gDis_1001_Array := xxcp_wks.Clear_Dynamic;
    xxcp_wks.gDis_1002_Array := xxcp_wks.Clear_Dynamic;

    IF nvl(cAttribute_id, 0) > 0 THEN

      IF xxcp_global.Preview_on = 'N' THEN

        IF nvl(xxcp_global.Forecast_on,'N') = 'Y' or nvl(xxcp_global.True_Up_on,'N') = 'Y' then
          FOR tcforecastrec IN TCForecast(cAttribute_id) LOOP

            Distribute_Array(tcforecastrec.CACHED_VALUE1);
            Distribute_Array(tcforecastrec.CACHED_VALUE2);
            Distribute_Array(tcforecastrec.CACHED_VALUE3);
            Distribute_Array(tcforecastrec.CACHED_VALUE4);
            Distribute_Array(tcforecastrec.CACHED_VALUE5);
            Distribute_Array(tcforecastrec.CACHED_VALUE6);
            Distribute_Array(tcforecastrec.CACHED_VALUE7);
            Distribute_Array(tcforecastrec.CACHED_VALUE8);
            Distribute_Array(tcforecastrec.CACHED_VALUE9);
            Distribute_Array(tcforecastrec.CACHED_VALUE10);

            IF nvl(tcforecastrec.CAChed_Qty, 0) > 10 THEN

              Distribute_Array(tcforecastrec.CACHED_VALUE11);
              Distribute_Array(tcforecastrec.CACHED_VALUE12);
              Distribute_Array(tcforecastrec.CACHED_VALUE13);
              Distribute_Array(tcforecastrec.CACHED_VALUE14);
              Distribute_Array(tcforecastrec.CACHED_VALUE15);
              Distribute_Array(tcforecastrec.CACHED_VALUE16);
              Distribute_Array(tcforecastrec.CACHED_VALUE17);
              Distribute_Array(tcforecastrec.CACHED_VALUE18);
              Distribute_Array(tcforecastrec.CACHED_VALUE19);
              Distribute_Array(tcforecastrec.CACHED_VALUE20);

            END IF;

            IF nvl(tcforecastrec.CAChed_Qty, 0) > 20 THEN

              Distribute_Array(tcforecastrec.CACHED_VALUE21);
              Distribute_Array(tcforecastrec.CACHED_VALUE22);
              Distribute_Array(tcforecastrec.CACHED_VALUE23);
              Distribute_Array(tcforecastrec.CACHED_VALUE24);
              Distribute_Array(tcforecastrec.CACHED_VALUE25);
              Distribute_Array(tcforecastrec.CACHED_VALUE26);
              Distribute_Array(tcforecastrec.CACHED_VALUE27);
              Distribute_Array(tcforecastrec.CACHED_VALUE28);
              Distribute_Array(tcforecastrec.CACHED_VALUE29);
              Distribute_Array(tcforecastrec.CACHED_VALUE30);

            END IF;

            IF nvl(tcforecastrec.CAChed_Qty, 0) > 30 THEN

              Distribute_Array(tcforecastrec.CACHED_VALUE31);
              Distribute_Array(tcforecastrec.CACHED_VALUE32);
              Distribute_Array(tcforecastrec.CACHED_VALUE33);
              Distribute_Array(tcforecastrec.CACHED_VALUE34);
              Distribute_Array(tcforecastrec.CACHED_VALUE35);
              Distribute_Array(tcforecastrec.CACHED_VALUE36);
              Distribute_Array(tcforecastrec.CACHED_VALUE37);
              Distribute_Array(tcforecastrec.CACHED_VALUE38);
              Distribute_Array(tcforecastrec.CACHED_VALUE39);
              Distribute_Array(tcforecastrec.CACHED_VALUE40);

            END IF;

            IF nvl(tcforecastrec.CAChed_Qty, 0) > 40 THEN

              Distribute_Array(tcforecastrec.CACHED_VALUE41);
              Distribute_Array(tcforecastrec.CACHED_VALUE42);
              Distribute_Array(tcforecastrec.CACHED_VALUE43);
              Distribute_Array(tcforecastrec.CACHED_VALUE44);
              Distribute_Array(tcforecastrec.CACHED_VALUE45);
              Distribute_Array(tcforecastrec.CACHED_VALUE46);
              Distribute_Array(tcforecastrec.CACHED_VALUE47);
              Distribute_Array(tcforecastrec.CACHED_VALUE48);
              Distribute_Array(tcforecastrec.CACHED_VALUE49);
              Distribute_Array(tcforecastrec.CACHED_VALUE50);
            END IF;

          END LOOP;
        ELSE
          FOR tcrec IN TC(cAttribute_id) LOOP


            Distribute_Array(tcrec.CACHED_VALUE1);
            Distribute_Array(tcrec.CACHED_VALUE2);
            Distribute_Array(tcrec.CACHED_VALUE3);
            Distribute_Array(tcrec.CACHED_VALUE4);
            Distribute_Array(tcrec.CACHED_VALUE5);
            Distribute_Array(tcrec.CACHED_VALUE6);
            Distribute_Array(tcrec.CACHED_VALUE7);
            Distribute_Array(tcrec.CACHED_VALUE8);
            Distribute_Array(tcrec.CACHED_VALUE9);
            Distribute_Array(tcrec.CACHED_VALUE10);

            IF nvl(tcrec.CAChed_Qty, 0) > 10 THEN

              Distribute_Array(tcrec.CACHED_VALUE11);
              Distribute_Array(tcrec.CACHED_VALUE12);
              Distribute_Array(tcrec.CACHED_VALUE13);
              Distribute_Array(tcrec.CACHED_VALUE14);
              Distribute_Array(tcrec.CACHED_VALUE15);
              Distribute_Array(tcrec.CACHED_VALUE16);
              Distribute_Array(tcrec.CACHED_VALUE17);
              Distribute_Array(tcrec.CACHED_VALUE18);
              Distribute_Array(tcrec.CACHED_VALUE19);
              Distribute_Array(tcrec.CACHED_VALUE20);

            END IF;

            IF nvl(tcrec.CAChed_Qty, 0) > 20 THEN

              Distribute_Array(tcrec.CACHED_VALUE21);
              Distribute_Array(tcrec.CACHED_VALUE22);
              Distribute_Array(tcrec.CACHED_VALUE23);
              Distribute_Array(tcrec.CACHED_VALUE24);
              Distribute_Array(tcrec.CACHED_VALUE25);
              Distribute_Array(tcrec.CACHED_VALUE26);
              Distribute_Array(tcrec.CACHED_VALUE27);
              Distribute_Array(tcrec.CACHED_VALUE28);
              Distribute_Array(tcrec.CACHED_VALUE29);
              Distribute_Array(tcrec.CACHED_VALUE30);

            END IF;

            IF nvl(tcrec.CAChed_Qty, 0) > 30 THEN

              Distribute_Array(tcrec.CACHED_VALUE31);
              Distribute_Array(tcrec.CACHED_VALUE32);
              Distribute_Array(tcrec.CACHED_VALUE33);
              Distribute_Array(tcrec.CACHED_VALUE34);
              Distribute_Array(tcrec.CACHED_VALUE35);
              Distribute_Array(tcrec.CACHED_VALUE36);
              Distribute_Array(tcrec.CACHED_VALUE37);
              Distribute_Array(tcrec.CACHED_VALUE38);
              Distribute_Array(tcrec.CACHED_VALUE39);
              Distribute_Array(tcrec.CACHED_VALUE40);

            END IF;

            IF nvl(tcrec.CAChed_Qty, 0) > 40 THEN

              Distribute_Array(tcrec.CACHED_VALUE41);
              Distribute_Array(tcrec.CACHED_VALUE42);
              Distribute_Array(tcrec.CACHED_VALUE43);
              Distribute_Array(tcrec.CACHED_VALUE44);
              Distribute_Array(tcrec.CACHED_VALUE45);
              Distribute_Array(tcrec.CACHED_VALUE46);
              Distribute_Array(tcrec.CACHED_VALUE47);
              Distribute_Array(tcrec.CACHED_VALUE48);
              Distribute_Array(tcrec.CACHED_VALUE49);
              Distribute_Array(tcrec.CACHED_VALUE50);
            END IF;

          END LOOP;
        END IF;
      ELSE

        FOR TCPRec IN TCP(cAttribute_id) LOOP

          If TCPRec.Seq > gMaxCacheRows then
            xxcp_foundation.FndWriteError(10517,'WARNING: ATTRIBUTE ID <'||to_char(cAttribute_id)||'>');
          End If;

          Distribute_Array(TCPRec.CACHED_VALUE1);
          Distribute_Array(TCPRec.CACHED_VALUE2);
          Distribute_Array(TCPRec.CACHED_VALUE3);
          Distribute_Array(TCPRec.CACHED_VALUE4);
          Distribute_Array(TCPRec.CACHED_VALUE5);
          Distribute_Array(TCPRec.CACHED_VALUE6);
          Distribute_Array(TCPRec.CACHED_VALUE7);
          Distribute_Array(TCPRec.CACHED_VALUE8);
          Distribute_Array(TCPRec.CACHED_VALUE9);
          Distribute_Array(TCPRec.CACHED_VALUE10);

          IF nvl(TCPRec.CAChed_Qty, 0) > 10 THEN

            Distribute_Array(TCPRec.CACHED_VALUE11);
            Distribute_Array(TCPRec.CACHED_VALUE12);
            Distribute_Array(TCPRec.CACHED_VALUE13);
            Distribute_Array(TCPRec.CACHED_VALUE14);
            Distribute_Array(TCPRec.CACHED_VALUE15);
            Distribute_Array(TCPRec.CACHED_VALUE16);
            Distribute_Array(TCPRec.CACHED_VALUE17);
            Distribute_Array(TCPRec.CACHED_VALUE18);
            Distribute_Array(TCPRec.CACHED_VALUE19);
            Distribute_Array(TCPRec.CACHED_VALUE20);

          END IF;

          IF nvl(TCPRec.CAChed_Qty, 0) > 20 THEN

            Distribute_Array(TCPRec.CACHED_VALUE21);
            Distribute_Array(TCPRec.CACHED_VALUE22);
            Distribute_Array(TCPRec.CACHED_VALUE23);
            Distribute_Array(TCPRec.CACHED_VALUE24);
            Distribute_Array(TCPRec.CACHED_VALUE25);
            Distribute_Array(TCPRec.CACHED_VALUE26);
            Distribute_Array(TCPRec.CACHED_VALUE27);
            Distribute_Array(TCPRec.CACHED_VALUE28);
            Distribute_Array(TCPRec.CACHED_VALUE29);
            Distribute_Array(TCPRec.CACHED_VALUE30);

          END IF;

          IF nvl(TCPRec.CAChed_Qty, 0) > 30 THEN

            Distribute_Array(TCPRec.CACHED_VALUE31);
            Distribute_Array(TCPRec.CACHED_VALUE32);
            Distribute_Array(TCPRec.CACHED_VALUE33);
            Distribute_Array(TCPRec.CACHED_VALUE34);
            Distribute_Array(TCPRec.CACHED_VALUE35);
            Distribute_Array(TCPRec.CACHED_VALUE36);
            Distribute_Array(TCPRec.CACHED_VALUE37);
            Distribute_Array(TCPRec.CACHED_VALUE38);
            Distribute_Array(TCPRec.CACHED_VALUE39);
            Distribute_Array(TCPRec.CACHED_VALUE40);

          END IF;

          IF nvl(TCPRec.CAChed_Qty, 0) > 40 THEN

            Distribute_Array(TCPRec.CACHED_VALUE41);
            Distribute_Array(TCPRec.CACHED_VALUE42);
            Distribute_Array(TCPRec.CACHED_VALUE43);
            Distribute_Array(TCPRec.CACHED_VALUE44);
            Distribute_Array(TCPRec.CACHED_VALUE45);
            Distribute_Array(TCPRec.CACHED_VALUE46);
            Distribute_Array(TCPRec.CACHED_VALUE47);
            Distribute_Array(TCPRec.CACHED_VALUE48);
            Distribute_Array(TCPRec.CACHED_VALUE49);
            Distribute_Array(TCPRec.CACHED_VALUE50);
          END IF;

        END LOOP;
      END IF;
    END IF;

    xxcp_wks.D1001_Array := xxcp_wks.gDis_1001_Array;
    xxcp_wks.D1002_Array := xxcp_wks.gDis_1002_Array;

  END Get_Transaction_Cache;

  --## ***********************************************************************************************************
  --##                               Set_IC_Prices
  --## ***********************************************************************************************************
  Procedure Set_IC_Prices(cIC_UNIT_PRICE      in number,
                          cIC_ADJUSTMENT_RATE in number,
                          cIC_TAX_RATE        in number,
                          cIC_PRICE           in number,
                          cQty                in number,
                          cPrecision          in number
                        ) is

    vIC_ADJ Number;

  Begin
    -- Init.
    xxcp_wks.I1009_Array(85)  := cIC_UNIT_PRICE;
    xxcp_wks.I1009_Array(86)  := cIC_UNIT_PRICE;
    xxcp_wks.I1009_Array(87)  := cIC_UNIT_PRICE;
    xxcp_wks.I1009_Array(88)  := cIC_UNIT_PRICE;
    -- Raw Extended
    xxcp_wks.I1009_Array(130) := nvl(cIC_UNIT_PRICE,0) * cQty;

    If cIC_ADJUSTMENT_RATE is not null then
      vIC_ADJ := cIC_UNIT_PRICE * cIC_ADJUSTMENT_RATE;
      xxcp_wks.I1009_Array(85) := vIC_ADJ;
      xxcp_wks.I1009_Array(86) := Round(vIC_ADJ*cQty,cPrecision); -- Extended

      If cIC_TAX_RATE is not null then
        xxcp_wks.I1009_Array(87) := vIC_ADJ * cIC_TAX_RATE;
        xxcp_wks.I1009_Array(87) := Round(vIC_ADJ*cIC_TAX_RATE*cQty,cPrecision); -- Extended
      End If;

    Else
      If cIC_TAX_RATE is not null then
        xxcp_wks.I1009_Array(87) := cIC_UNIT_PRICE * cIC_TAX_RATE;
        xxcp_wks.I1009_Array(87) := Round(cIC_UNIT_PRICE*cIC_TAX_RATE*cQty,cPrecision); -- Extended
      End If;

    End If;

  End Set_IC_Prices;



  --
  --## ***********************************************************************************************************
  --##
  --##                               Cache_Summary_Totals
  --##
  --## ***********************************************************************************************************
  --
  PROCEDURE Cache_Summary_Totals(cParent_Trx_id IN NUMBER, cSource_Table_id in Number, cSource_Assignment_id IN NUMBER) IS

  -- Real Cache
  CURSOR cx(pParent_Trx_id IN NUMBER, pSource_Table_id IN NUMBER, pSource_Assignment_id IN NUMBER) IS
   SELECT a.Attribute_id,
          t.trading_set,
          h.transaction_date,
          h.transaction_ref1,a.transaction_ref2,
          a.transaction_ref3,h.transaction_currency,
          h.exchange_date,
          a.quantity,
          a.uom,
          a.mc_qualifier1,a.mc_qualifier2,
          a.ed_qualifier1,a.ed_qualifier2,
          a.attribute1,a.attribute2,a.attribute3,a.attribute4,a.attribute5,
          a.attribute6,a.attribute7,a.attribute8,a.attribute9,a.attribute10,
          stt.transaction_table, sty.type transaction_type, stc.CLASS transaction_class,
          a.ic_unit_price, a.ic_tax_rate, a.adjustment_rate ic_adjustment_rate,a.ic_currency,
          a.owner_tax_reg_id, a.partner_tax_reg_id,
          a.transaction_id, a.header_id, a.ic_price, a.ic_control, a.ic_trade_tax_id
    from xxcp_transaction_attributes a,
         xxcp_transaction_header h,
         xxcp_trading_sets t,
         xxcp_sys_source_tables stt,
         xxcp_sys_source_types  sty,
         xxcp_sys_source_classes  stc
   WHERE a.parent_trx_id        = pParent_Trx_id
     AND a.source_assignment_id = pSource_Assignment_id
     AND a.source_table_id      = pSource_Table_id
     AND a.header_id            = h.header_id
     and a.trading_set_id       = t.trading_set_id
     and t.source_id            = xxcp_global.gCommon(1).Source_id
     and a.source_type_id       = sty.source_type_id
     and stt.source_id          = xxcp_global.gCommon(1).Source_id
     and sty.source_table_id    = stt.source_table_id
     and a.source_class_id      = stc.source_class_id;

  -- Preview Cache
  CURSOR pcx(pParent_Trx_id IN NUMBER, pSource_Table_id IN NUMBER, pSource_Assignment_id IN NUMBER) IS
   SELECT a.Attribute_id,
          t.trading_set,
          h.transaction_date,
          h.transaction_ref1,a.transaction_ref2,
          a.transaction_ref3,h.transaction_currency,
          h.exchange_date,
          a.quantity,
          a.uom,
          a.mc_qualifier1,a.mc_qualifier2,
          a.ed_qualifier1,a.ed_qualifier2,
          a.attribute1,a.attribute2,a.attribute3,a.attribute4,a.attribute5,
          a.attribute6,a.attribute7,a.attribute8,a.attribute9,a.attribute10,
          stt.transaction_table, sty.type transaction_type, stc.CLASS transaction_class,
          a.ic_unit_price, a.ic_tax_rate, a.adjustment_rate ic_adjustment_rate, a.ic_currency,
          a.owner_tax_reg_id, a.partner_tax_reg_id,
          a.transaction_id, a.header_id, a.ic_price, a.ic_control, a.ic_trade_tax_id
    from xxcp_pv_transaction_attributes a,
         xxcp_pv_transaction_header h,
         xxcp_trading_sets t,
         xxcp_sys_source_tables stt,
         xxcp_sys_source_types  sty,
         xxcp_sys_source_classes  stc
   WHERE a.parent_trx_id        = pParent_Trx_id
     AND a.source_assignment_id = pSource_Assignment_id
     AND a.source_table_id      = pSource_Table_id
     AND a.header_id            = h.header_id
     and a.trading_set_id       = t.trading_set_id
     and a.source_type_id       = sty.source_type_id
     and a.source_table_id      = stt.source_table_id
     and a.source_class_id      = stc.source_class_id
     and stt.source_id          = xxcp_global.gCommon(1).Source_id
     and h.preview_id           = xxcp_global.gCommon(1).Preview_id
     AND a.preview_id           = xxcp_global.gCommon(1).Preview_id;

  -- Forecast Cache
  CURSOR Fcx(pParent_Trx_id IN NUMBER, pSource_Table_id IN NUMBER, pSource_Assignment_id IN NUMBER) IS
   SELECT a.Attribute_id,
          t.trading_set,
          h.transaction_date,
          h.transaction_ref1,a.transaction_ref2,
          a.transaction_ref3,h.transaction_currency,
          h.exchange_date,
          a.quantity,
          a.uom,
          a.mc_qualifier1,a.mc_qualifier2,
          a.ed_qualifier1,a.ed_qualifier2,
          a.attribute1,a.attribute2,a.attribute3,a.attribute4,a.attribute5,
          a.attribute6,a.attribute7,a.attribute8,a.attribute9,a.attribute10,
          stt.transaction_table, sty.type transaction_type, stc.CLASS transaction_class,
          a.ic_unit_price, a.ic_tax_rate, a.adjustment_rate ic_adjustment_rate,a.ic_currency,
          a.owner_tax_reg_id, a.partner_tax_reg_id,
          a.transaction_id, a.header_id, a.ic_price, a.ic_control, a.ic_trade_tax_id
    from xxcp_fc_transaction_attributes a,
         xxcp_fc_transaction_header h,
         xxcp_trading_sets t,
         xxcp_sys_source_tables stt,
         xxcp_sys_source_types  sty,
         xxcp_sys_source_classes  stc
   WHERE a.parent_trx_id        = pParent_Trx_id
     AND a.source_assignment_id = pSource_Assignment_id
     AND a.source_table_id      = pSource_Table_id
     AND a.header_id            = h.header_id
     and a.trading_set_id       = t.trading_set_id
     and t.source_id            = xxcp_global.gCommon(1).Source_id
     and a.source_type_id       = sty.source_type_id
     and stt.source_id          = xxcp_global.gCommon(1).Source_id
     and sty.source_table_id    = stt.source_table_id
     and a.source_class_id      = stc.source_class_id;

  s   PLS_INTEGER := 0;
  t   PLS_INTEGER := 0;
  tf  PLS_INTEGER := 0;
  r   PLS_INTEGER := 0;
  f   PLS_INTEGER := 0;

  Q1  VARCHAR2(250);
  Q2  VARCHAR2(250);
  Q3  VARCHAR2(250);
  Q4  VARCHAR2(250);
  Q5  VARCHAR2(250);

  --
  --## ***********************************************************************************************************
  --##                               TRX_SUMMARY_CODE_BODY
  --## ***********************************************************************************************************
  --
  PROCEDURE Trx_Summary_Code_Body(cTrading_Set IN VARCHAR2 ) IS
   -- 02.06.11
   M    XXCP_DYNAMIC_ARRAY :=  XXCP_DYNAMIC_ARRAY('0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0');

   BEGIN

      -- Set Qualifiers 1-5 literals
      Q1 := xxcp_foundation.Find_DynamicAttribute(xxcp_wks.BRK_SET_RCD(1).BREAK_CACHE(1));
      Q2 := xxcp_foundation.Find_DynamicAttribute(xxcp_wks.BRK_SET_RCD(1).BREAK_CACHE(2));
      Q3 := xxcp_foundation.Find_DynamicAttribute(xxcp_wks.BRK_SET_RCD(1).BREAK_CACHE(3));
      Q4 := xxcp_foundation.Find_DynamicAttribute(xxcp_wks.BRK_SET_RCD(1).BREAK_CACHE(4));
      Q5 := xxcp_foundation.Find_DynamicAttribute(xxcp_wks.BRK_SET_RCD(1).BREAK_CACHE(5));

      -- Populate Array with array values from the cache
      FOR r IN 1..xxcp_wks.BRK_SET_RCD(1).sum_qty LOOP
        M(r) := nvl(xxcp_foundation.Find_DynamicAttribute(xxcp_wks.BRK_SET_RCD(1).SUMMARY_CACHE(r)),0);
      END LOOP;
      -- Summaries
      s  := xxcp_wks.SUM_GRP_RCD.COUNT;
      f  := 0;

      -- Look in the memroy to see if the Qualifers match an existing record
      -- If this is the first time the this will result in false.
      FOR r IN 1..s LOOP -- Is there a summary group already
        IF cTrading_Set = xxcp_wks.SUM_GRP_RCD(r).Trading_Set
          AND nvl(Q1,'~#~') = nvl(xxcp_wks.SUM_GRP_RCD(r).BREAK_CACHE_ATTRIBUTES(1),'~#~')
          AND nvl(Q2,'~#~') = nvl(xxcp_wks.SUM_GRP_RCD(r).BREAK_CACHE_ATTRIBUTES(2),'~#~')
          AND nvl(Q3,'~#~') = nvl(xxcp_wks.SUM_GRP_RCD(r).BREAK_CACHE_ATTRIBUTES(3),'~#~')
          AND nvl(Q4,'~#~') = nvl(xxcp_wks.SUM_GRP_RCD(r).BREAK_CACHE_ATTRIBUTES(4),'~#~')
          AND nvl(Q5,'~#~') = nvl(xxcp_wks.SUM_GRP_RCD(r).BREAK_CACHE_ATTRIBUTES(5),'~#~') THEN
          f := r; -- Found!!
          EXIT;
        END IF;
      END LOOP;

      IF f = 0 THEN
       -- Summary
       -- Match not found, so make a record in memory to represent the grouping
       f := xxcp_wks.SUM_GRP_RCD.COUNT+1;
       xxcp_wks.SUM_GRP_RCD(f).Trading_Set := cTrading_Set;
       xxcp_wks.SUM_GRP_RCD(f).BREAK_CACHE_ATTRIBUTES(1) := Q1;
       xxcp_wks.SUM_GRP_RCD(f).BREAK_CACHE_ATTRIBUTES(2) := Q2;
       xxcp_wks.SUM_GRP_RCD(f).BREAK_CACHE_ATTRIBUTES(3) := Q3;
       xxcp_wks.SUM_GRP_RCD(f).BREAK_CACHE_ATTRIBUTES(4) := Q4;
       xxcp_wks.SUM_GRP_RCD(f).BREAK_CACHE_ATTRIBUTES(5) := Q5;
       xxcp_wks.SUM_GRP_RCD(f).CNT := 0;

      End If;

      IF f > 0 THEN
       xxcp_wks.SUM_GRP_RCD(f).CNT := nvl(xxcp_wks.SUM_GRP_RCD(f).CNT,1)+1;
       FOR r IN 1..xxcp_wks.BRK_SET_RCD(1).Sum_Qty LOOP
         xxcp_wks.SUM_GRP_RCD(f).CACHE_VALUE(r) := nvl(xxcp_wks.SUM_GRP_RCD(f).CACHE_VALUE(r),0)+M(r);
       END LOOP;
      END IF;

      -- Trading Set
      t  := xxcp_wks.TST_GRP_RCD.COUNT;
      tf := 0;
      FOR r IN 1..t LOOP -- Is there a summary group already
        IF cTrading_Set = xxcp_wks.TST_GRP_RCD(r).Trading_Set THEN
          tf := r; -- Found!!
          EXIT;
       END IF;
      END LOOP;

      IF tf = 0 THEN
       -- Trading Set Totals
       tf := xxcp_wks.TST_GRP_RCD.COUNT+1;
       xxcp_wks.TST_GRP_RCD(tf).Trading_Set := cTrading_Set;
      END IF;

      -- Trading Set Totals
      FOR r IN 1..xxcp_wks.BRK_SET_RCD(1).sum_qty LOOP
         xxcp_wks.TST_GRP_RCD(tf).CACHE_VALUE(r) := nvl(xxcp_wks.TST_GRP_RCD(tf).CACHE_VALUE(r),0)+M(r);
      END LOOP;


  END Trx_Summary_Code_Body;

  BEGIN
    xxcp_wks.SUM_GRP_RCD.DELETE;
    xxcp_wks.TST_GRP_RCD.DELETE;

    -- Standard
    IF xxcp_global.Preview_on = 'N' THEN
      IF nvl(xxcp_global.Forecast_On,'N') = 'Y' or nvl(xxcp_global.True_Up_on,'N') = 'Y' then
        -- Read Forecast Cache
        FOR Rec IN Fcx(cParent_Trx_id, cSource_Table_id, cSource_Assignment_id) LOOP
          -- Populate Internal Arrays just in case they are needed
          xxcp_wks.I1009_Array(1)  := Rec.OWNER_TAX_REG_ID;
          xxcp_wks.I1009_Array(2)  := Rec.PARTNER_TAX_REG_ID;
          xxcp_wks.I1009_Array(28) := Rec.TRANSACTION_ID;
          xxcp_wks.I1009_Array(31) := Rec.IC_UNIT_PRICE;
          xxcp_wks.I1009_Array(35) := Rec.ATTRIBUTE_ID;
          xxcp_wks.I1009_Array(65) := Rec.IC_ADJUSTMENT_RATE;
          xxcp_wks.I1009_Array(67) := Rec.IC_TAX_RATE;
          xxcp_wks.I1009_Array(68) := Rec.HEADER_ID;
          xxcp_wks.I1009_Array(102):= Rec.IC_PRICE;
          xxcp_wks.I1009_Array(138):= Rec.Transaction_Id; -- new

          -- IC Internal Pricing
          If Rec.IC_UNIT_PRICE is not null then
            Set_IC_Prices(cIC_UNIT_PRICE      => Rec.IC_UNIT_PRICE,
                          cIC_ADJUSTMENT_RATE => Rec.IC_ADJUSTMENT_RATE,
                          cIC_TAX_RATE        => Rec.IC_TAX_RATE,
                          cIC_PRICE           => Rec.Ic_Price,
                          cQty                => nvl(Rec.QUANTITY,1),
                          cPrecision          => xxcp_foundation.Fnd_CurrencyPrecision(rec.IC_CURRENCY,2, 'N')
                          );

          Else
            xxcp_wks.I1009_Array(85) := 0;
            xxcp_wks.I1009_Array(86) := 0;
            xxcp_wks.I1009_Array(87) := 0;
            xxcp_wks.I1009_Array(88) := 0;
            xxcp_wks.I1009_Array(130):= 0;
          End If;

          -- Populated Cache into Arrays
          Get_Transaction_Cache(cAttribute_id => rec.attribute_id);
          -- Main body code
          Trx_Summary_Code_Body(cTrading_Set => rec.trading_set);
        END LOOP;
      ELSE
        -- Read Cache
        FOR Rec IN cx(cParent_Trx_id, cSource_Table_id, cSource_Assignment_id) LOOP
          -- Populate Internal Arrays just in case they are needed
          xxcp_wks.I1009_Array(1)  := Rec.OWNER_TAX_REG_ID;
          xxcp_wks.I1009_Array(2)  := Rec.PARTNER_TAX_REG_ID;
          xxcp_wks.I1009_Array(28) := Rec.TRANSACTION_ID;
          xxcp_wks.I1009_Array(31) := Rec.IC_UNIT_PRICE;
          xxcp_wks.I1009_Array(35) := Rec.ATTRIBUTE_ID;
          xxcp_wks.I1009_Array(65) := Rec.IC_ADJUSTMENT_RATE;
          xxcp_wks.I1009_Array(67) := Rec.IC_TAX_RATE;
          xxcp_wks.I1009_Array(68) := Rec.HEADER_ID;
          xxcp_wks.I1009_Array(102):= Rec.IC_PRICE;
          xxcp_wks.I1009_Array(138):= Rec.TRANSACTION_ID;

          -- IC Internal Pricing
          If Rec.IC_UNIT_PRICE is not null then
            Set_IC_Prices(cIC_UNIT_PRICE      => Rec.IC_UNIT_PRICE,
                          cIC_ADJUSTMENT_RATE => Rec.IC_ADJUSTMENT_RATE,
                          cIC_TAX_RATE        => Rec.IC_TAX_RATE,
                          cIC_PRICE           => Rec.IC_PRICE,
                          cQty                => nvl(Rec.QUANTITY,1),
                          cPrecision          => xxcp_foundation.Fnd_CurrencyPrecision(rec.IC_CURRENCY,2, 'N')
                          );

          Else
            xxcp_wks.I1009_Array(85) := 0;
            xxcp_wks.I1009_Array(86) := 0;
            xxcp_wks.I1009_Array(87) := 0;
            xxcp_wks.I1009_Array(88) := 0;
            xxcp_wks.I1009_Array(130):= 0;
          End If;

          -- Populated Cache into Arrays
          Get_Transaction_Cache(cAttribute_id => rec.attribute_id);
          -- Main body code
          Trx_Summary_Code_Body(cTrading_Set => rec.trading_set);
        END LOOP;
      END IF;
    ELSE
      -- Preview Mode
      -- Read Cache
      FOR Rec IN pcx(cParent_Trx_id, cSource_Table_id, cSource_Assignment_id) LOOP
        -- Populate Internal Arrays just in case they are needed
        xxcp_wks.I1009_Array(1)  := Rec.Owner_Tax_reg_id;
        xxcp_wks.I1009_Array(2)  := Rec.Partner_Tax_reg_id;
        xxcp_wks.I1009_Array(28) := Rec.Transaction_id;
        xxcp_wks.I1009_Array(31) := Rec.IC_Unit_Price;
        xxcp_wks.I1009_Array(35) := Rec.Attribute_id;
        xxcp_wks.I1009_Array(65) := Rec.IC_Adjustment_Rate;
        xxcp_wks.I1009_Array(67) := Rec.IC_TAX_RATE;
        xxcp_wks.I1009_Array(68) := Rec.header_id;
        xxcp_wks.I1009_Array(102):= Rec.IC_PRICE;


        -- IC Internal Pricing
        If Rec.IC_UNIT_PRICE is not null then
          Set_IC_Prices(cIC_UNIT_PRICE      => Rec.IC_UNIT_PRICE,
                        cIC_ADJUSTMENT_RATE => Rec.IC_ADJUSTMENT_RATE,
                        cIC_TAX_RATE        => Rec.IC_TAX_RATE,
                        cIC_PRICE           => Rec.IC_PRICE,
                        cQty                => nvl(Rec.QUANTITY,1),
                        cPrecision          => xxcp_foundation.Fnd_CurrencyPrecision(rec.IC_CURRENCY,2,'N')
                        );

        Else
          xxcp_wks.I1009_Array(85) := 0;
          xxcp_wks.I1009_Array(86) := 0;
          xxcp_wks.I1009_Array(87) := 0;
          xxcp_wks.I1009_Array(88) := 0;
          xxcp_wks.I1009_Array(130):= 0;
        End If;

        -- Populated Cache into Arrays
        Get_Transaction_Cache(rec.attribute_id);
        -- Main body code
        Trx_Summary_Code_Body(rec.trading_set);
      END LOOP;
    END IF;

    -- Trace
    xxcp_trace.Transaction_Trace(cTrace_id => 5);

    xxcp_wks.I1009_Array(31) := NULL;
    xxcp_wks.I1009_Array(65) := NULL;
    xxcp_wks.I1009_Array(67) := NULL;
    xxcp_wks.I1009_Array(102):= NULL;

  END Cache_Summary_Totals;

 --
 --## ***********************************************************************************************************
 --##                               Cache_Summary
 --## ***********************************************************************************************************
 --
  PROCEDURE Cache_Summary(cParent_Trx_id IN NUMBER, cSource_Table_id IN VARCHAR2, cSource_Assignment_id IN NUMBER, cTransaction_Table in VARCHAR2) IS
    -- Summary
    CURSOR CacheBreak(pSource_id IN NUMBER, pTransaction_Table IN VARCHAR2) IS
    SELECT   nvl(q.grp_qty,0) grp_qty,
             -- Break Points
             q.grp1_attribute,q.grp2_attribute,q.grp3_attribute,q.grp4_attribute,q.grp5_attribute,
             q.bpts_id, q.count_cache, q.trx_qualifier1_expr, q.trx_qualifier2_expr
      FROM xxcp_summary_break_pts  q
     WHERE source_id         = pSource_id
       AND transaction_table = pTransaction_table;

    -- Summary
    CURSOR CacheSum(pSource_id IN NUMBER, pBpts_id IN NUMBER) IS
    SELECT sum_attribute ,numeric_cache , ts_cache
      FROM XXCP_SUMMARY_CACHE  q
     WHERE source_id = pSource_id
       AND bpts_id   = pBpts_id;

   vSumQty INTEGER := 0;

  BEGIN

    If gCacheSummaries = 'Y' then
        --
        -- ***********************************************************************************************************
        -- Summary Record
        -- ***********************************************************************************************************
        --
        -- When the Transaction Table Changes read the Summary values in the table
        IF xxcp_wks.Last_Transaction_Table <> cTransaction_Table THEN

          -- Set Variables
          xxcp_wks.Last_Transaction_Table := cTransaction_Table;
          xxcp_wks.BRK_SET_RCD.DELETE;
          xxcp_wks.BRK_SET_RCD(1).Break_Qty := -1;
          xxcp_wks.BRK_SET_RCD(1).Sum_Qty   := -1;

          For CacheBreakRec IN CacheBreak(xxcp_global.gCommon(1).Source_id,cTransaction_Table) Loop
              -- Populate Array with Pointers for the Cache Summary Attributes
              xxcp_wks.BRK_SET_RCD(1).BREAK_QTY      := CacheBreakRec.grp_qty;
              xxcp_wks.BRK_SET_RCD(1).BREAK_POINT_ID := CacheBreakRec.bpts_id;
              xxcp_wks.BRK_SET_RCD(1).BREAK_CACHE(1) := CacheBreakRec.grp1_attribute;
              xxcp_wks.BRK_SET_RCD(1).BREAK_CACHE(2) := CacheBreakRec.grp2_attribute;
              xxcp_wks.BRK_SET_RCD(1).BREAK_CACHE(3) := CacheBreakRec.grp3_attribute;
              xxcp_wks.BRK_SET_RCD(1).BREAK_CACHE(4) := CacheBreakRec.grp4_attribute;
              xxcp_wks.BRK_SET_RCD(1).BREAK_CACHE(5) := CacheBreakRec.grp5_attribute;
              xxcp_wks.BRK_SET_RCD(1).COUNT_CACHE    := CacheBreakRec.count_cache;
              xxcp_wks.BRK_SET_RCD(1).Trx_Qualifier1_Expr := CacheBreakRec.TRX_Qualifier1_Expr;
              xxcp_wks.BRK_SET_RCD(1).Trx_Qualifier2_Expr := CacheBreakRec.TRX_Qualifier2_Expr;
              -- Populate Pointers for the Summary and Total Attributes.
              FOR CacheSumRec IN CacheSum(xxcp_global.gCommon(1).Source_id,CacheBreakRec.bpts_id) LOOP
                 vSumQty := vSumQty + 1;
                 xxcp_wks.BRK_SET_RCD(1).SUMMARY_CACHE(vSumQty)  := CacheSumRec.sum_attribute; -- What to summarize
                 xxcp_wks.BRK_SET_RCD(1).NUMERIC_CACHE(vSumQty)  := CacheSumRec.numeric_cache; -- Summary
                 xxcp_wks.BRK_SET_RCD(1).TS_CACHE(vSumQty)       := CacheSumRec.Ts_cache;      -- Total
                 EXIT WHEN vSumQty >= gCacheSumMaxQty; -- Max attribute pairs
              END LOOP;

              xxcp_wks.BRK_SET_RCD(1).Sum_Qty  := vSumQty;
              EXIT;
           END LOOP;
        END IF;
        --
        -- ***********************************************************************************************************
        -- End Summary Record
        -- ***********************************************************************************************************
        --

        IF ((gCacheSummaries = 'Y') and (xxcp_wks.BRK_SET_RCD(1).Sum_Qty > 0)) THEN
          gCacheSumArray := xxcp_wks.Clear_Dynamic; -- Clear and Size Array
          -- DO the Summarizing
          Cache_Summary_Totals(cParent_Trx_id, cSource_Table_id, cSource_Assignment_id);
          xxcp_trace.Transaction_Trace(cTrace_id => 6);
       END IF;

    End If;
  END Cache_Summary;

 --
 --## ***********************************************************************************************************
 --##                               Reset_Working_Storage
 --## ***********************************************************************************************************
 --
 Procedure Reset_Working_Storage is
 Begin
    xxcp_wks.D1001_Array     := xxcp_wks.Clear_Dynamic; -- Clear and Size Array
    xxcp_wks.D1002_Array     := xxcp_wks.Clear_Dynamic; -- Clear and Size Array
    xxcp_wks.D1003_Array     := xxcp_wks.Clear_Dynamic; -- Clear and Size Array
    xxcp_wks.D1004_Array     := xxcp_wks.Clear_Dynamic; -- Clear and Size Array
    xxcp_wks.D1006_Array     := xxcp_wks.Clear_Dynamic; -- Clear and Size Array
    xxcp_wks.D1007_Array     := xxcp_wks.Clear_Dynamic; -- Clear and Size Array
    xxcp_wks.I1009_Array     := xxcp_wks.Clear_Internal;

    xxcp_Custom_Events.gNew_Transaction := xxcp_global.Get_New_Trx_Flag;

    xxcp_wks.Trace_Log           := NULL;
    xxcp_wks.Values_Log          := NULL;
    xxcp_wks.Segments_Log        := NULL;
    xxcp_wks.ColumnAttrbutes_Log := NULL;

    xxcp_global.gCommon(1).Previous_Attribute_Id := 0;

    xxcp_wks.gLineCount.delete;

  End Reset_Working_Storage;

  --
  --## ***********************************************************************************************************
  --##                               CONTROL
  --##               This is the Entry point for any callling program.
  --## ***********************************************************************************************************
  --
  PROCEDURE Control(cSource_assignment_id    IN NUMBER,
                    cSource_Table_id         IN NUMBER,
                    cSource_Type_id          IN NUMBER,
                    cSource_Class_id         IN VARCHAR2,
                    cTransaction_id          IN NUMBER,
                    cParent_Trx_id           IN NUMBER,
                    cSourceRowid             IN ROWID,
                    cParent_Entered_Amount   IN NUMBER,
                    cParent_Entered_Currency IN VARCHAR2,
                    cClass_Mapping           IN VARCHAR2,
                    cClass_Mapping_Name      IN VARCHAR2,
                    cInterface_ID            IN NUMBER,
                    cExtraLoop               IN BOOLEAN,
                    cExtraClass              IN VARCHAR2,
                    cTransaction_set_id      IN NUMBER,
                    cTransaction_Table       IN VARCHAR2,
                    cTransaction_Type        IN VARCHAR2,
                    cTransaction_Class       IN VARCHAR2,
                    cInternalErrorCode       OUT NOCOPY NUMBER) IS

    --
    -- Cursor containing the data from the configurator for the
    -- for the transaction..
    --

    CURSOR TA(pParent_Trx_id IN NUMBER, pTransation_Table_id IN NUMBER, pTransaction_id IN NUMBER, pSource_Assignment_id IN NUMBER) IS
      SELECT -- NCM 07/08/2008 Removed leading hint
      -- TRANSACTION_HEADER
       h.OWNER_TAX_REG_ID,
       h.ROWID header_rowid,
       h.set_of_books_id,
       h.Frozen Header_Frozen,
       h.transaction_currency,
       h.transaction_date,
       h.owner_assign_rule_id,
       -- TRANSACTION_ATTRIBUTES
       a.Partner_Tax_Reg_id,
       a.Exchange_Date,
       a.Source_assignment_id,
       decode(substr(h.frozen,1,1), 'Y', a.Frozen, h.frozen) Frozen,
       t.Trading_Set ,
       a.ROWID Attribute_Rowid,
       a.Parent_trx_id,
       st.type transaction_type,
       stc.class transaction_class,
       a.transaction_id,
       nvl(a.Adjustment_Rate,0) IC_adjustment_Rate,
       a.Adjustment_Rate_id,
       a.IC_Unit_price,
       a.IC_Price,
       a.IC_Currency,
       nvl(a.IC_Tax_Rate,0) IC_Tax_Rate,
       a.ic_trade_tax_id,
       a.ic_control,
       decode(a.ic_tax_rate,Null,'N','Y') create_tax_rules,
       a.Quantity,
       a.UOM,
       A.ATTRIBUTE_ID,
       a.ED_Qualifier1,
       a.ED_Qualifier2,
       a.ED_Qualifier3,
       a.ED_Qualifier4,
       a.MC_Qualifier1 MC_Qualifier1,
       a.MC_Qualifier2 MC_Qualifier2,
       a.MC_Qualifier3 MC_Qualifier3,
       a.MC_Qualifier4 MC_Qualifier4,
       a.header_id,
       a.trading_set_id,
       t.sequence trading_set_sequence,
       a.TRANSACTION_REF1,a.TRANSACTION_REF2,a.TRANSACTION_REF3,
       a.ATTRIBUTE1,a.ATTRIBUTE2,a.ATTRIBUTE3,a.ATTRIBUTE4,a.ATTRIBUTE5,
       a.ATTRIBUTE6,a.ATTRIBUTE7,a.ATTRIBUTE8,a.ATTRIBUTE9,a.ATTRIBUTE10,
       tr.short_code Owner_Short_Name,
       tr.territory  Owner_Territory
       from  xxcp_trading_sets           t,
             xxcp_sys_source_types       st,
             xxcp_sys_source_classes     stc,
             xxcp_tax_registrations      tr,
             xxcp_transaction_header     h,
             xxcp_transaction_attributes a
       where h.header_id            = a.header_id
         and t.source_id            = xxcp_global.gcommon(1).source_id
         -- NCM 07/08/2008 Changed pSource_assignment_id to join to a instad of the h so it would use the
         -- a XXCP_TRANSACTION_ATTRIB_U1 index.
         and a.source_assignment_id = pSource_Assignment_id
         and a.parent_trx_id        = pParent_trx_id
         and a.source_table_id      = pTransation_table_id
         and a.transaction_id       = pTransaction_id
         and a.trading_set_id       = t.trading_set_id
         and a.source_type_id       = st.source_type_id
         and a.source_class_id      = stc.source_class_id
         and a.owner_tax_reg_id     = tr.tax_registration_id
       order by t.sequence;

    CURSOR TAEXP(pParent_Trx_id IN NUMBER, pTransation_Table_id IN NUMBER, pInterface_id IN NUMBER, pSource_Assignment_id IN NUMBER) IS
      SELECT -- NCM 07/08/2008 Removed leading hint
      -- TRANSACTION_HEADER
       h.OWNER_TAX_REG_ID,
       h.ROWID header_rowid,
       h.set_of_books_id,
       h.Frozen Header_Frozen,
       h.transaction_currency,
       h.transaction_date,
       -- NCM 02.05.07
       h.owner_assign_rule_id,
       -- TRANSACTION_ATTRIBUTES
       a.Partner_Tax_Reg_id,
       a.Exchange_Date,
       a.Source_assignment_id,
       decode(substr(h.frozen,1,1), 'Y', a.Frozen, h.frozen) Frozen,
       t.Trading_Set ,
       a.ROWID Attribute_Rowid,
       a.Parent_trx_id,
       st.type transaction_type,
       stc.class transaction_class,
       a.transaction_id,
       nvl(a.Adjustment_Rate,0) IC_adjustment_Rate,
       a.Adjustment_Rate_id,
       a.IC_Unit_price,
       a.IC_Price,
       a.IC_Currency,
       nvl(a.ic_tax_rate,0) ic_tax_rate,
       a.ic_trade_tax_id,
       a.ic_control,
       decode(a.ic_tax_rate,Null,'N','Y') create_tax_rules,
       a.Quantity,
       a.UOM,
       A.ATTRIBUTE_ID,
       a.ED_Qualifier1,
       a.ED_Qualifier2,
       a.ED_Qualifier3,
       a.ED_Qualifier4,
       a.MC_Qualifier1 MC_Qualifier1,
       a.MC_Qualifier2 MC_Qualifier2,
       a.MC_Qualifier3 MC_Qualifier3,
       a.MC_Qualifier4 MC_Qualifier4,
       a.header_id,
       a.trading_set_id,
       t.sequence trading_set_sequence,
       a.TRANSACTION_REF1,a.TRANSACTION_REF2,a.TRANSACTION_REF3,
       a.ATTRIBUTE1,a.ATTRIBUTE2,a.ATTRIBUTE3,a.ATTRIBUTE4,a.ATTRIBUTE5,
       a.ATTRIBUTE6,a.ATTRIBUTE7,a.ATTRIBUTE8,a.ATTRIBUTE9,a.ATTRIBUTE10
       ,tr.short_code Owner_Short_Name
       ,tr.territory  Owner_Territory
       from  xxcp_trading_sets           t,
             xxcp_sys_source_types       st,
             xxcp_sys_source_classes     stc,
             xxcp_tax_registrations      tr,
             xxcp_transaction_header     h,
             xxcp_transaction_attributes a
       where h.header_id            = a.header_id
         and t.source_id            = xxcp_global.gcommon(1).source_id
         -- NCM 07/08/2008 Changed pSource_assignment_id to join to a instad of the h so it would use the
         -- a XXCP_TRANSACTION_ATTRIB_U1 index.
         and a.source_assignment_id = pSource_Assignment_id
         and a.parent_trx_id        = pParent_trx_id
         and a.source_table_id      = pTransation_table_id
         and a.interface_id         = pInterface_id
         and a.trading_set_id       = t.trading_set_id
         and a.source_type_id       = st.source_type_id
         and a.source_class_id      = stc.source_class_id
         and a.owner_tax_reg_id     = tr.tax_registration_id
       order by t.sequence;

    -- This Cursor Must be kept in line with the TA cursor above. The only difference is the table names
    CURSOR TADebug(pParent_Trx_id IN NUMBER, pTransation_Table_id IN NUMBER, pTransaction_id IN NUMBER, pSource_assignment_id IN NUMBER) IS
      SELECT -- NCM 07/08/2008 Removed leading hint
      -- TRANSACTION_HEADER
       h.owner_tax_reg_id,
       h.rowid header_rowid,
       h.set_of_books_id,
       h.frozen header_frozen,
       h.transaction_currency,
       h.transaction_date,
       -- NCM 02.05.07
       h.owner_assign_rule_id,
       -- transaction_attributes
       a.partner_tax_reg_id,
       a.exchange_date,
       a.source_assignment_id,
       decode(substr(h.frozen,1,1), 'Y', a.frozen, h.frozen) frozen,
       t.trading_set ,
       a.rowid attribute_rowid,
       a.parent_trx_id,
       st.type transaction_type,
       stc.class transaction_class,
       a.transaction_id,
       nvl(a.adjustment_rate,0) ic_adjustment_rate,
       a.adjustment_rate_id,
       a.ic_unit_price,
       a.ic_price,
       a.ic_currency,
       nvl(a.ic_tax_rate,0) ic_tax_rate,
       a.ic_trade_tax_id,
       a.ic_control,
       decode(a.ic_tax_rate,Null,'N','Y') create_tax_rules,
       a.quantity,
       a.uom,
       a.attribute_id,
       a.ed_qualifier1,
       a.ed_qualifier2,
       a.ed_qualifier3,
       a.ed_qualifier4,
       a.mc_qualifier1 mc_qualifier1,
       a.mc_qualifier2 mc_qualifier2,
       a.mc_qualifier3 mc_qualifier3,
       a.mc_qualifier4 mc_qualifier4,
       a.header_id,
       a.trading_set_id,
       t.sequence trading_set_sequence,
       a.transaction_ref1,a.transaction_ref2,a.transaction_ref3,
       a.attribute1,a.attribute2,a.attribute3,a.attribute4,a.attribute5,
       a.attribute6,a.attribute7,a.attribute8,a.attribute9,a.attribute10,
       tr.short_code Owner_Short_Name,
       tr.territory  Owner_Territory
        from xxcp_trading_sets              t,
             xxcp_sys_source_types          st,
             xxcp_sys_source_classes        stc,
             xxcp_tax_registrations         tr,
             xxcp_pv_transaction_header     h,
             xxcp_pv_transaction_attributes a
       WHERE h.header_id            = a.header_id
         AND t.source_id            = xxcp_global.gCommon(1).source_id
         -- NCM 07/08/2008 Changed pSource_assignment_id to join to a instad of the h so it would use the
         -- a XXCP_TRANSACTION_ATTRIB_U1 index.
         and a.source_assignment_id = pSource_Assignment_id
         AND a.parent_trx_id        = pParent_trx_id
         AND a.source_table_id      = pTransation_table_id
         AND a.transaction_id       = pTransaction_id
         AND a.preview_id           = xxcp_global.gCommon(1).Preview_id
         and h.preview_id           = xxcp_global.gCommon(1).Preview_id
         and a.trading_set_id       = t.trading_set_id
         and a.source_type_id       = st.source_type_id
         and a.source_class_id      = stc.source_class_id
         and a.owner_tax_reg_id     = tr.tax_registration_id
        Order by t.sequence;


    -- This Cursor Must be kept in line with the TA cursor above. The only difference is the table names
    CURSOR TAPVEXP(pParent_Trx_id IN NUMBER, pTransation_Table_id IN NUMBER, pInterface_id IN NUMBER, pSource_assignment_id IN NUMBER) IS
      SELECT -- NCM 07/08/2008 Removed leading hint
      -- TRANSACTION_HEADER
       h.owner_tax_reg_id,
       h.rowid header_rowid,
       h.set_of_books_id,
       h.frozen header_frozen,
       h.transaction_currency,
       h.transaction_date,
       -- NCM 02.05.07
       h.owner_assign_rule_id,
       -- transaction_attributes
       a.partner_tax_reg_id,
       a.exchange_date,
       a.source_assignment_id,
       decode(substr(h.frozen,1,1), 'Y', a.frozen, h.frozen) frozen,
       t.trading_set ,
       a.rowid attribute_rowid,
       a.parent_trx_id,
       st.type transaction_type,
       stc.class transaction_class,
       a.transaction_id,
       nvl(a.adjustment_rate,0) ic_adjustment_rate,
       a.adjustment_rate_id,
       a.ic_unit_price,
       a.ic_price,
       a.ic_currency,
       nvl(a.ic_tax_rate,0) ic_tax_rate,
       a.ic_trade_tax_id,
       a.ic_control,
       decode(a.ic_tax_rate,Null,'N','Y') create_tax_rules,
       a.quantity,
       a.uom,
       a.attribute_id,
       a.ed_qualifier1,
       a.ed_qualifier2,
       a.ed_qualifier3,
       a.ed_qualifier4,
       a.mc_qualifier1 mc_qualifier1,
       a.mc_qualifier2 mc_qualifier2,
       a.mc_qualifier3 mc_qualifier3,
       a.mc_qualifier4 mc_qualifier4,
       a.header_id,
       a.trading_set_id,
       t.sequence trading_set_sequence,
       a.transaction_ref1,a.transaction_ref2,a.transaction_ref3,
       a.attribute1,a.attribute2,a.attribute3,a.attribute4,a.attribute5,
       a.attribute6,a.attribute7,a.attribute8,a.attribute9,a.attribute10, 
       tr.short_code Owner_Short_Name,
       tr.territory  Owner_Territory
        from xxcp_trading_sets              t,
             xxcp_sys_source_types          st,
             xxcp_sys_source_classes        stc,
             xxcp_tax_registrations         tr,
             xxcp_pv_transaction_header     h,
             xxcp_pv_transaction_attributes a
       WHERE h.header_id            = a.header_id
         AND t.source_id            = xxcp_global.gCommon(1).source_id
         -- NCM 07/08/2008 Changed pSource_assignment_id to join to a instad of the h so it would use the
         -- a XXCP_TRANSACTION_ATTRIB_U1 index.
         and a.source_assignment_id = pSource_Assignment_id
         AND a.parent_trx_id        = pParent_trx_id
         AND a.source_table_id      = pTransation_table_id
         AND a.Interface_id         = pInterface_id
         AND a.preview_id           = xxcp_global.gCommon(1).Preview_id
         and h.preview_id           = xxcp_global.gCommon(1).Preview_id
         and a.trading_set_id       = t.trading_set_id
         and a.source_type_id       = st.source_type_id
         and a.source_class_id      = stc.source_class_id
         and a.owner_tax_reg_id     = tr.tax_registration_id
        Order by t.sequence;


    CURSOR TAForecast(pParent_Trx_id IN NUMBER, pTransation_Table_id IN NUMBER, pTransaction_id IN NUMBER, pSource_Assignment_id IN NUMBER) IS
      SELECT -- NCM 07/08/2008 Removed leading hint
      -- TRANSACTION_HEADER
       h.OWNER_TAX_REG_ID,
       h.ROW_ID header_rowid,
       h.set_of_books_id,
       h.Frozen Header_Frozen,
       h.transaction_currency,
       h.transaction_date,
       -- NCM 02.05.07
       h.owner_assign_rule_id,
       -- TRANSACTION_ATTRIBUTES
       a.Partner_Tax_Reg_id,
       a.Exchange_Date,
       a.Source_assignment_id,
       decode(substr(h.frozen,1,1), 'Y', a.Frozen, h.frozen) Frozen,
       t.Trading_Set ,
       a.ROW_ID Attribute_Rowid,
       a.Parent_trx_id,
       st.type transaction_type,
       stc.class transaction_class,
       a.transaction_id,
       nvl(a.Adjustment_Rate,0) IC_adjustment_Rate,
       a.Adjustment_Rate_id,
       a.IC_Unit_price,
       a.IC_Price,
       a.IC_Currency,
       nvl(a.ic_tax_rate,0) ic_tax_rate,
       a.ic_trade_tax_id,
       a.ic_control,
       decode(a.ic_tax_rate,Null,'N','Y') create_tax_rules,
       a.Quantity,
       a.UOM,
       A.ATTRIBUTE_ID,
       a.ED_Qualifier1,
       a.ED_Qualifier2,
       a.ED_Qualifier3,
       a.ED_Qualifier4,
       a.MC_Qualifier1 MC_Qualifier1,
       a.MC_Qualifier2 MC_Qualifier2,
       a.MC_Qualifier3 MC_Qualifier3,
       a.MC_Qualifier4 MC_Qualifier4,
       a.header_id,
       a.trading_set_id,
       t.sequence trading_set_sequence,
       a.TRANSACTION_REF1,a.TRANSACTION_REF2,a.TRANSACTION_REF3,
       a.ATTRIBUTE1,a.ATTRIBUTE2,a.ATTRIBUTE3,a.ATTRIBUTE4,a.ATTRIBUTE5,
       a.ATTRIBUTE6,a.ATTRIBUTE7,a.ATTRIBUTE8,a.ATTRIBUTE9,a.ATTRIBUTE10,
       -- ksp 10/01/09 cpa
       tr.short_code Owner_Short_Name,
       tr.territory  Owner_Territory
        from xxcp_trading_sets           t,
             xxcp_sys_source_types       st,
             xxcp_sys_source_classes     stc,
             xxcp_tax_registrations      tr,
             xxcp_fc_trans_header_v     h,
             xxcp_fc_trans_attributes_v a
       where h.header_id            = a.header_id
         and t.source_id            = xxcp_global.gcommon(1).source_id 
         and a.source_assignment_id = pSource_Assignment_id
         and a.parent_trx_id        = pParent_trx_id
         and a.source_table_id      = pTransation_table_id
         and a.transaction_id       = pTransaction_id
         and a.trading_set_id       = t.trading_set_id
         and a.source_type_id        = st.source_type_id
         and a.source_class_id      = stc.source_class_id
         and a.owner_tax_reg_id     = tr.tax_registration_id
       order by t.sequence;

    CURSOR TAForecastEXP(pParent_Trx_id IN NUMBER, pTransation_Table_id IN NUMBER, pInterface_id IN NUMBER, pSource_Assignment_id IN NUMBER) IS
      SELECT -- NCM 07/08/2008 Removed leading hint
      -- TRANSACTION_HEADER
       h.OWNER_TAX_REG_ID,
       h.ROW_ID header_rowid,
       h.set_of_books_id,
       h.Frozen Header_Frozen,
       h.transaction_currency,
       h.transaction_date,
       -- NCM 02.05.07
       h.owner_assign_rule_id,
       -- TRANSACTION_ATTRIBUTES
       a.Partner_Tax_Reg_id,
       a.Exchange_Date,
       a.Source_assignment_id,
       decode(substr(h.frozen,1,1), 'Y', a.Frozen, h.frozen) Frozen,
       t.Trading_Set ,
       a.ROW_ID Attribute_Rowid,
       a.Parent_trx_id,
       st.type transaction_type,
       stc.class transaction_class,
       a.transaction_id,
       nvl(a.Adjustment_Rate,0) IC_adjustment_Rate,
       a.Adjustment_Rate_id,
       a.IC_Unit_price,
       a.IC_Price,
       a.IC_Currency,
       nvl(a.ic_tax_rate,0) ic_tax_rate,
       a.ic_trade_tax_id,
       a.ic_control,
       decode(a.ic_tax_rate,Null,'N','Y') create_tax_rules,
       a.Quantity,
       a.UOM,
       A.ATTRIBUTE_ID,
       a.ED_Qualifier1,
       a.ED_Qualifier2,
       a.ED_Qualifier3,
       a.ED_Qualifier4,
       a.MC_Qualifier1 MC_Qualifier1,
       a.MC_Qualifier2 MC_Qualifier2,
       a.MC_Qualifier3 MC_Qualifier3,
       a.MC_Qualifier4 MC_Qualifier4,
       a.header_id,
       a.trading_set_id,
       t.sequence trading_set_sequence,
       a.TRANSACTION_REF1,a.TRANSACTION_REF2,a.TRANSACTION_REF3,
       a.ATTRIBUTE1,a.ATTRIBUTE2,a.ATTRIBUTE3,a.ATTRIBUTE4,a.ATTRIBUTE5,
       a.ATTRIBUTE6,a.ATTRIBUTE7,a.ATTRIBUTE8,a.ATTRIBUTE9,a.ATTRIBUTE10,
       -- ksp 10/01/09 cpa
       tr.short_code Owner_Short_Name,
       tr.territory  Owner_Territory
        from xxcp_trading_sets           t,
             xxcp_sys_source_types       st,
             xxcp_sys_source_classes     stc,
             xxcp_tax_registrations      tr,
             xxcp_fc_trans_header_v     h,
             xxcp_fc_trans_attributes_v a
       where h.header_id            = a.header_id
         and t.source_id            = xxcp_global.gcommon(1).source_id 
         and a.source_assignment_id = pSource_Assignment_id
         and a.parent_trx_id        = pParent_trx_id
         and a.source_table_id      = pTransation_table_id
         and a.interface_id         = pInterface_id
         and a.trading_set_id       = t.trading_set_id
         and a.source_type_id       = st.source_type_id
         and a.source_class_id      = stc.source_class_id
         and a.owner_tax_reg_id     = tr.tax_registration_id
       order by t.sequence;

    vTransaction_Class xxcp_sys_source_classes.class%TYPE;

    vCtlLoop     INTEGER;
    vHAC_LoopCnt INTEGER;
    TAREC        TA%ROWTYPE;

    -- Explosion
    vExplosion_Summary        number;
    vLast_Source_Type_id      xxcp_sys_source_types.source_type_id%type := 0;
    vExplosion_sql            varchar2(4000);
    vRowsFound                Number;
    jx                        Number;
    vExplosion_Trx_id         number;
    vExplosion_Source_Rowid   varchar2(32);
    vExplosion_Trx_Type_id    Number(15);
    vExplosion_Trx_Class_id   Number(15);

    vPartner_Tax_Reg_Group_id Number(15);
  --
  --## ***********************************************************************************************************
  --##
  --##                               CONTROL_CODE_BODY
  --##     This is the main control body so that the two cursors always work in the same way
  --##
  --## ***********************************************************************************************************
  --
    PROCEDURE Control_Code_Body(cHDRClassCtl IN NUMBER, cInternalErrorCode IN OUT NUMBER) IS

      vCon   Pls_integer; -- Loop Control

      BEGIN

         FOR vCon IN 1..1 LOOP

            If xxcp_wks.BRK_SET_RCD.Count = 0 then -- Prevent the object being empty
              xxcp_wks.BRK_SET_RCD(1).Sum_Qty := 0;
            End If;

            IF cInternalErrorCode = 10506 THEN
              cInternalErrorCode := 0;
            END IF;

            IF TARec.Frozen = 'N' THEN
              cInternalErrorCode := 10507;
            END IF;

            EXIT WHEN cInternalErrorCode <> 0;

            xxcp_global.gCommon(1).current_Transaction_id := cTransaction_Id;
            xxcp_global.gCommon(1).current_Trading_Set_id := TARec.Trading_Set_id;

            -- Internal
            xxcp_wks.I1009_Array(68) := TARec.HEADER_ID;
            xxcp_wks.I1009_Array(71) := TARec.ATTRIBUTE1;
            xxcp_wks.I1009_Array(72) := TARec.ATTRIBUTE2;
            xxcp_wks.I1009_Array(73) := TARec.ATTRIBUTE3;
            xxcp_wks.I1009_Array(74) := TARec.ATTRIBUTE4;
            xxcp_wks.I1009_Array(75) := TARec.ATTRIBUTE5;
            xxcp_wks.I1009_Array(76) := TARec.ATTRIBUTE6;
            xxcp_wks.I1009_Array(77) := TARec.ATTRIBUTE7;
            xxcp_wks.I1009_Array(78) := TARec.ATTRIBUTE8;
            xxcp_wks.I1009_Array(79) := TARec.ATTRIBUTE9;
            xxcp_wks.I1009_Array(80) := TARec.ATTRIBUTE10;
            xxcp_wks.I1009_Array(129):= TARec.Owner_Territory;  -- 02.05.20
            
            xxcp_wks.I1009_Array(90) := TARec.IC_Tax_Rate;
            xxcp_wks.I1009_Array(91) := TARec.IC_Trade_Tax_Id;

            Set_IC_Prices(cIC_UNIT_PRICE      => TARec.IC_UNIT_PRICE,
                          cIC_ADJUSTMENT_RATE => TARec.IC_ADJUSTMENT_RATE,
                          cIC_TAX_RATE        => TARec.IC_TAX_RATE,
                          cIC_PRICE           => TAREC.IC_PRICE,
                          cQty                => nvl(TARec.Quantity,1),
                          cPrecision          => xxcp_foundation.Fnd_CurrencyPrecision(TARec.IC_CURRENCY,2,'N')
                        );

            Get_Transaction_Cache(cAttribute_id => TARec.Attribute_id);

            xxcp_global.gCommon(1).current_Transaction_Date := TARec.TRANSACTION_DATE;
            xxcp_global.gCommon(1).current_trading_set_seq  := TARec.trading_set_sequence;

            -- Poluate the Configuration Array
            --
            xxcp_wks.D1001_Array(1)  := TARec.TRANSACTION_DATE;
            xxcp_wks.D1001_Array(3)  := TARec.TRANSACTION_REF1;
            xxcp_wks.D1001_Array(4)  := TARec.TRANSACTION_REF2;
            xxcp_wks.D1001_Array(5)  := TARec.TRANSACTION_REF3;
            xxcp_wks.D1001_Array(6)  := TARec.TRANSACTION_CURRENCY;
            xxcp_wks.D1001_Array(7)  := TARec.EXCHANGE_DATE;
            xxcp_wks.D1001_Array(8)  := TARec.QUANTITY;
            xxcp_wks.D1001_Array(9)  := TARec.UOM;
            xxcp_wks.D1001_Array(10) := TARec.MC_Qualifier1;
            xxcp_wks.D1001_Array(11) := TARec.MC_Qualifier2;
            xxcp_wks.D1001_Array(21) := TARec.MC_Qualifier3;
            xxcp_wks.D1001_Array(22) := TARec.MC_Qualifier4;
            xxcp_wks.D1001_Array(12) := TARec.ED_Qualifier1;
            xxcp_wks.D1001_Array(13) := TARec.ED_Qualifier2;

            -- Cost-Plus
            If xxcp_global.gCommon(1).Source_id  = 31 then

              If gDisable_CPA_Calc <> 'Y' then
            
                If xxcp_global.Get_New_Trx_Flag = 'Y' and xxcp_global.gCommon(1).Previous_Attribute_Id = 0 then
                --
                -- Only set for first trading set. Previous_Attribute_Id is set to 0 in reset_working_storage.
                --
                  gTierOneOwner := TARec.Owner_Short_Name;
                  gTierOneTerr  := TARec.Owner_Territory;
                End If;

                -- Set Territory Internal Attribute for Cost Plus
                If gTierOne_Control = 'Y' then
                  xxcp_wks.I1009_Array(129):= gTierOneTerr;  -- 02.05.20
                end if;

                If nvl(xxcp_global.replan_on,'N') = 'N' then

                -- Forecast
                If xxcp_global.Forecast_on = 'Y' then
                  If gTierOne_Control = 'N' then
                    cInternalErrorCode := Forecast_Rates(TARec.TRANSACTION_DATE, tarec.Owner_Short_Name, tarec.owner_territory );
                  Else
                    cInternalErrorCode := Forecast_Rates(TARec.TRANSACTION_DATE, gTierOneOwner, gTierOneTerr );
                  End If;
                Elsif nvl(xxcp_global.True_Up_on,'N') = 'Y' then -- True-up
                  If gTierOne_Control = 'N' then
                    If xxcp_global.gReplan_Reprocess = 'N' then
                      cInternalErrorCode := True_Up_Rates(TARec.TRANSACTION_DATE, tarec.Owner_Short_Name, tarec.owner_territory, tarec.OWNER_TAX_REG_ID, tarec.PARTNER_TAX_REG_ID );
                    Else
                      cInternalErrorCode := FC_True_Up_Rates(TARec.TRANSACTION_DATE, tarec.Owner_Short_Name, tarec.owner_territory, tarec.OWNER_TAX_REG_ID, tarec.PARTNER_TAX_REG_ID );
                    End if;
                  Else
                      If xxcp_global.gReplan_Reprocess = 'N' then
                      cInternalErrorCode := True_Up_Rates(TARec.TRANSACTION_DATE, gTierOneOwner, gTierOneTerr, tarec.OWNER_TAX_REG_ID, tarec.PARTNER_TAX_REG_ID );
                    Else
                      cInternalErrorCode := FC_True_Up_Rates(TARec.TRANSACTION_DATE, gTierOneOwner, gTierOneTerr, tarec.OWNER_TAX_REG_ID, tarec.PARTNER_TAX_REG_ID );
                    End if;
                  End If;
                End If;

                Elsif nvl(xxcp_global.replan_on,'N') = 'Y' then
                  If gTierOne_Control = 'N' then
                    populate_replan(TARec.TRANSACTION_DATE, tarec.Owner_Short_Name, tarec.owner_territory );
                  Else
                    populate_replan(TARec.TRANSACTION_DATE, gTierOneOwner, gTierOneTerr );
                  End If;
                End If;
              Else
                --
                -- CPA Calculations Disabled.
                --
                
                -- Set Cost Category id & Tax Agreement Number
                xxcp_wks.I1009_Array(112) := xxcp_wks.gActual_Costs_Rec(1).fc_Cost_Category_id;
                xxcp_wks.I1009_Array(171) := xxcp_wks.gActual_Costs_Rec(1).tax_agreement_number;
              
              End if;
            -- 03.06.xx
            -- Po Matching
            Elsif xxcp_global.gCommon(1).Source_id  = 43 then 

              -- Set Quantity Flag
              If ABS(xxcp_foundation.Find_DynamicAttribute(xxcp_te_base.Get_Quantity_Attribute)) > 0 Then
                xxcp_global.Set_Quantity_Flag('Y');
              End If;  
                  
              -- 03.06.30 If the trx is both matched and has no quantity discrepancy then we can notify ICS 
              --          that it is eligible for approval
              If nvl(xxcp_global.Get_Quantity_Flag,'N') = 'N' then  
                xxcp_global.Add_Approved_Matched_Parent(cInvoice_Matching_Rec => xxcp_global.gInvoice_Matching_Common(1));
              Else
                xxcp_global.Add_Rejected_Matched_Parent(cInvoice_Matching_Rec => xxcp_global.gInvoice_Matching_Common(1));
              End if;
            End If;

            -- Report if there is an error
            If nvl(cInternalErrorCode,0) <> 0 then
              Exit;
            End If;


            -- Clear Trx Qualifiers
            xxcp_wks.I1009_Array(69) := NULL;
            xxcp_wks.I1009_Array(70) := NULL;

            IF ((gCacheSummaries = 'Y') and (nvl(xxcp_wks.BRK_SET_RCD(1).sum_qty,0) > 0)) THEN
              Cache_Summary_Find(TARec.Trading_Set);
              cInternalErrorCode := Cache_Summary_Qualifiers(xxcp_wks.I1009_Array(69),xxcp_wks.I1009_Array(70));
              IF cInternalErrorCode != 0 THEN
                 Exit;
              End If;
            END IF;

            vTransaction_Class := nvl(vTransaction_Class,TARec.Transaction_Class);

            IF cHDRClassCtl > 1 THEN
              vTransaction_Class := cExtraClass;
            END IF;

            If ((xxcp_global.gCommon(1).Custom_events = 'Y') and
                (xxcp_wks.I1009_Array(69) IS NULL AND xxcp_wks.I1009_Array(70) IS NULL)) then

                 If gCacheSummaries = 'Y' then
                    Cache_Summary_Get('B');
                 End If;

                 cInternalErrorCode :=
                   xxcp_Custom_Events.Get_Transaction_Qualifiers(
                         cSource_id                   => xxcp_global.gCommon(1).Source_id,
                         cSource_Assignment_id        => xxcp_global.gCommon(1).Current_Assignment_id,
                         cParent_Trx_id               => TARec.Parent_Trx_ID,
                         cSource_Rowid                => cSourceRowid,
                         cAttribute_id                => TARec.Attribute_ID,
                         cTransaction_id              => TARec.Transaction_id,
                         cOwner_Tax_Reg_id            => TARec.Owner_Tax_Reg_id, -- Owner
                         cPartner_Tax_Reg_id          => TARec.Partner_Tax_Reg_id, -- Partner
                         cTrading_Set                 => TARec.Trading_Set,
                         --
                         cTransaction_Table           => cTransaction_Table,
                         cTransaction_Type            => TARec.Transaction_Type,
                         cTransaction_Class           => vTransaction_Class,
                         -- OUT Trx Qualifers 1 and 2
                         cTransaction_Qualifier1      => xxcp_wks.I1009_Array(69),
                         cTransaction_Qualifier2      => xxcp_wks.I1009_Array(70)
                         );

                  EXIT WHEN cInternalErrorCode > 0;
            END IF;

            xxcp_trace.Transaction_Trace(cTrace_id => 2, cTransaction_Class => vTransaction_Class);

            xxcp_global.gCommon(1).current_trading_set := TARec.Trading_Set;
            

            Process_Control(
                            cSource_assignment_id    => cSource_Assignment_id,
                            cSource_id               => xxcp_global.gCommon(1).Source_id,
                            cTransaction_Table       => cTransaction_Table,
                            cTransaction_Type        => TARec.Transaction_Type,
                            cTransaction_Class       => vTransaction_Class,
                            cQuantity                => TARec.Quantity,
                            cTransaction_id          => TARec.Transaction_id,
                            cOwner_Tax_Reg_id        => TARec.Owner_Tax_Reg_id,
                            cPartner_Tax_Reg_id      => TARec.Partner_Tax_Reg_id,
                            cTrading_Set             => TARec.Trading_Set,
                            cTransaction_Currency    => TARec.Transaction_Currency,
                            cSet_of_books_id         => TARec.Set_of_books_id,
                            cAttribute_ID            => TARec.Attribute_ID,
                            cParent_Trx_ID           => TARec.Parent_Trx_ID,
                            cTransaction_Date        => TARec.Transaction_Date,
                            -- NCM 02.05.07
                            cOwner_assign_rule_id    => TARec.Owner_Assign_Rule_Id,
                            -- MC
                            cMC_Qualifier1           => TARec.MC_Qualifier1,
                            cMC_Qualifier2           => TARec.MC_Qualifier2,
                            cMC_Qualifier3           => TARec.MC_Qualifier3,
                            cMC_Qualifier4           => TARec.MC_Qualifier4,
                            -- Trx Qualifiers 1 and 2
                            cTRX_Qualifier1          => xxcp_wks.I1009_Array(69),
                            cTRX_Qualifier2          => xxcp_wks.I1009_Array(70),
                            -- ED
                            cED_Qualifier1           => TARec.ED_Qualifier1,
                            cED_Qualifier2           => TARec.ED_Qualifier2,
                            cED_Qualifier3           => TARec.ED_Qualifier3,
                            cED_Qualifier4           => TARec.ED_Qualifier4,
                            cExchange_date           => TARec.Exchange_date,
                            cTransaction_Quantity    => TARec.Quantity,
                            cIC_Unit_Price           => TARec.IC_Unit_Price,
                            cIC_Price                => TARec.IC_Price,
                            cIC_Unit_Currency        => TARec.IC_Currency,
                            cIC_Adjustment_Rate      => TARec.IC_Adjustment_Rate,
                            cIC_Tax_Rate             => TARec.IC_Tax_Rate,
                            cIC_Tax_Control          => TARec.ic_control,
                            cSourceRowid             => cSourceRowid,
                            cParent_Entered_Amount   => cParent_Entered_Amount,
                            cParent_Entered_Currency => cParent_Entered_Currency,
                            cInterface_ID            => cInterface_ID,
                            cTransaction_set_id      => cTransaction_set_id,
                            cInternalErrorCode       => cInternalErrorCode);

            xxcp_wks.D1007_Array(1) := TARec.Owner_Tax_Reg_id;
            xxcp_wks.D1007_Array(2) := TARec.Partner_Tax_Reg_id;
            xxcp_wks.D1007_Array(3) := TARec.IC_Unit_Price;
            xxcp_wks.D1007_Array(4) := TARec.IC_Currency;
            xxcp_wks.D1007_Array(5) := TARec.Trading_Set;

            xxcp_global.gCommon(1).Previous_Attribute_Id := TARec.Attribute_id;

            EXIT; -- Only loop once
          END LOOP;
       END Control_Code_Body;

  BEGIN

    Reset_Working_Storage;
    xxcp_global.gCommon(1).current_source_table_id  := cSource_Table_id;
    xxcp_global.gCommon(1).current_Source_rowid     := cSourceRowid;
    xxcp_global.gCommon(1).Explosion_Transaction_id := 0; -- new

    IF xxcp_global.Get_New_Trx_Flag = 'Y' THEN
      -- Fetch the Cache Summaries once per transaction
      xxcp_wks.ONCE_RCD.DELETE;
      If gCacheSummaries = 'Y' then
        Cache_Summary(cParent_Trx_id,cSource_Table_id, cSource_Assignment_id, cTransaction_Table);
      End If;
    END IF;

    IF nvl(cSource_Assignment_id, 0) = 0 THEN
      cInternalErrorCode := 10521; -- Source Assignment not specified
    ELSE

      vTransaction_Class := cTransaction_Class;

      -- Apply HEADER Loop
      IF nvl(xxcp_global.gCommon(1).Trx_Explosion_id,0) > 0  THEN
        
        vHAC_LoopCnt := 0;
        cInternalErrorCode := 0;
        
        If vLast_Source_Type_id != cSource_Type_Id then
        
          cInternalErrorCode :=
            xxcp_dynamic_sql.ReadExplosionDef(cExplosion_id    => xxcp_global.gCommon(1).Trx_Explosion_id,
                                              cExplosion_sql   => vExplosion_SQL,
                                              cSummary_Columns => vExplosion_Summary);
                                             
          vLast_Source_Type_id := cSource_Type_id;
        End If;
        
        If cInternalErrorCode = 0 then
             cInternalErrorCode :=
                 xxcp_dynamic_sql.ExplosionTrxProcess(
                                   cSource_Type_Id    => cSource_Type_Id,
                                   cSource_Rowid      => cSourceRowid,
                                   cInterface_id      => xxcp_global.gCommon(1).Current_Interface_id,
                                   cExplosion_SQL     => vExplosion_SQL,
                                   cExplosion_Summary => vExplosion_Summary,
                                   cColumnCount       => 2,
                                   cRowsFound         => vRowsFound,
                                   cTransaction_Type  => cTransaction_Type,
                                   cTransaction_Class => cTransaction_Class
                                   );
                                   
            vHAC_LoopCnt :=  vRowsFound;
            -- Populate Internal Parameters
            If cInternalErrorCode = 0 and xxcp_global.gCommon(1).Trx_Explosion_id > 0 then
               xxcp_wks.I1009_Array(137) := xxcp_global.gCommon(1).Explosion_RowsFound;
               xxcp_wks.I1009_Array(138) := xxcp_global.gCommon(1).Explosion_Transaction_id;
               xxcp_wks.I1009_Array(139) := xxcp_global.gCommon(1).Explosion_Rowid;
               xxcp_wks.I1009_Array(141) := xxcp_global.gCommon(1).Explosion_Summary1;
               xxcp_wks.I1009_Array(142) := xxcp_global.gCommon(1).Explosion_Summary2;
               xxcp_wks.I1009_Array(143) := xxcp_global.gCommon(1).Explosion_Summary3;
            End If;                                 
        End If;        
      ELSE
        xxcp_wks.I1009_Array(137) := 0;
        vHAC_LoopCnt := 1;
      END IF;

      xxcp_trace.Transaction_Trace(cTrace_id             => 1,
                                   cTransaction_id       => cTransaction_id,
                                   cParent_Trx_id        => cParent_Trx_id,
                                   cTransaction_table    => cTransaction_Table,
                                   cSource_assignment_id => cSource_assignment_id,
                                   cEntered_Currency     => cParent_Entered_Currency,
                                   cEntered_Value        => nvl(cParent_Entered_Amount,0)
                                   );

      FOR vCtlLoop IN 1..vHAC_LoopCnt
      LOOP

        EXIT WHEN cInternalErrorCode <> 0;

        cInternalErrorCode := 10506;

        IF xxcp_global.Preview_on = 'N' THEN
          -- Forecast
          if nvl(xxcp_global.Forecast_on,'N') = 'Y' or nvl(xxcp_global.True_up_on,'N') = 'Y' or nvl(xxcp_global.replan_on,'N') = 'Y' then
            If nvl(xxcp_global.gCommon(1).Explosion_id,0) > 0 then
              -- Forecast Explosion
              FOR TAForecastRec IN TAForecastEXP(cParent_trx_id,cSource_Table_id,cInterface_id, cSource_assignment_id )
              LOOP
                TARec := TAForecastRec;
                Control_Code_Body(vCtlLoop, cInternalErrorCode);
                xxcp_trace.Transaction_Write_Trace;
                EXIT WHEN cInternalErrorCode <> 0; -- Exit loop if you find an error
              END LOOP;
            Else
              -- Standard Forecast
              FOR TAForecastRec IN TAForecast(cParent_trx_id,cSource_Table_id,cTransaction_id, cSource_assignment_id )
              LOOP
                TARec := TAForecastRec;
                Control_Code_Body(vCtlLoop, cInternalErrorCode);
                xxcp_trace.Transaction_Write_Trace;
                EXIT WHEN cInternalErrorCode <> 0; -- Exit loop if you find an error
              END LOOP;
            End if;
          Else
            If nvl(xxcp_global.gCommon(1).Explosion_id,0) > 0 then
              -- Main Explosion
              FOR TAMAIN IN TAEXP(cParent_trx_id,cSource_Table_id,cInterface_id, cSource_assignment_id )
              LOOP
                TARec := TAMAIN;
                xxcp_global.gCommon(1).Explosion_Transaction_id := TARec.Transaction_Id; --new
                xxcp_wks.I1009_Array(138) := TARec.Transaction_Id;

                Control_Code_Body(vCtlLoop, cInternalErrorCode);
                xxcp_trace.Transaction_Write_Trace;
                EXIT WHEN cInternalErrorCode <> 0; -- Exit loop if you find an error
              END LOOP;
            Else
              -- Main
              FOR TAMAIN IN TA(cParent_trx_id,cSource_Table_id,cTransaction_id, cSource_assignment_id )
              LOOP
                TARec := TAMAIN;
                Control_Code_Body(vCtlLoop, cInternalErrorCode);
                xxcp_trace.Transaction_Write_Trace;
                EXIT WHEN cInternalErrorCode <> 0; -- Exit loop if you find an error
              END LOOP;
            End If;
          End if;
        ELSE

         If nvl(xxcp_global.gCommon(1).Explosion_id,0) > 0 then
          -- Debug Explosion
          FOR TADebugRec IN TAPVExp(cParent_trx_id,cSource_Table_id,cInterface_id, cSource_Assignment_id)
           LOOP
              TARec := TADebugRec;
              xxcp_global.gCommon(1).Explosion_Transaction_id := TARec.Transaction_Id; --new
              xxcp_wks.I1009_Array(138) := TARec.Transaction_Id;
              Control_Code_Body(vCtlLoop,cInternalErrorCode);
              xxcp_trace.Transaction_Write_Trace;
              EXIT WHEN cInternalErrorCode <> 0; -- Exit loop if you find an error
           END LOOP;
          Else
             -- Debug mode
             FOR TADebugRec IN TADebug(cParent_trx_id,cSource_Table_id,cTransaction_id, cSource_Assignment_id)
              LOOP
                TARec := TADebugRec;
                Control_Code_Body(vCtlLoop,cInternalErrorCode);
                xxcp_trace.Transaction_Write_Trace;
                EXIT WHEN cInternalErrorCode <> 0; -- Exit loop if you find an error
              END LOOP;
         End If;

        END IF;

        EXIT WHEN cInternalErrorCode <> 0;
      END LOOP; -- End vHDRClassCtl Loop

      xxcp_trace.Transaction_Write_Trace;

      IF cInternalErrorCode = 10506 THEN
        xxcp_foundation.FndWriteError(cInternalErrorCode,
          'Transaction Id <'||to_char(cTransaction_id)||'> Parent Trx Id <'||to_char(cParent_trx_id) ||'> Transaction Table <' ||cSource_Table_id || '>');
      END IF;
    END IF;

  END Control;

--
--## ************************************************************************************
--##
--##                            INIT
--##     This procedure groups together the code that is executed when
--##     the package is first called.
--##
--## ************************************************************************************
--
 PROCEDURE INIT is

   CURSOR SAB_CLASS IS
     SELECT lookup_Code Tax_Class_Name
       FROM XXCP_LOOKUPS c
      WHERE c.lookup_type = 'AR TAX CLASS NAME'
        AND c.enabled_flag = 'Y';

   CURSOR CROSVAL IS
     SELECT COUNT(*) PrevCrossValOvr_cnt
       FROM XXCP_LOOKUPS c
      WHERE c.lookup_type = 'CROSS VALIDATION IN PREVIEW'
        AND SUBSTR(c.lookup_code,1,1) = 'Y'
        AND c.enabled_flag = 'Y';

   CURSOR CSHSUM is
     select Source_id,Count(*) CNT
       from xxcp_summary_cache
       where source_id = xxcp_global.gCommon(1).Source_id
     Group by Source_id;

   CURSOR TUOFF is
        select Profile_Value Offset
        from XXCP_SYS_PROFILE
       where profile_name = 'TRUE-UP PERIOD OFFSET';

   CURSOR T1CTL is
        select Profile_Value TierOneCtl
        from XXCP_SYS_PROFILE
       where profile_name = 'CPA TIER ONE CONTROL';

   CURSOR CPA_Calcs is
        select Profile_Value disable_cpa_calc
        from XXCP_SYS_PROFILE
       where profile_name     = 'DISABLE CPA CALCULATION'
         and profile_category = 'CP';

   Cursor PayAtt is
        select Profile_Value
        from   XXCP_SYS_PROFILE
        where  profile_name = 'PAYER ATTRIBUTE';

   Cursor RetProAtt is
        select Profile_Value
        from   XXCP_SYS_PROFILE
        where  profile_name = 'PAYER RET/PRO ATTRIBUTE';

   Cursor TargType is
        select Profile_Value
        from   XXCP_SYS_PROFILE
        where  profile_name = 'TARGET TYPE NAME';
        
   Cursor ValColAtts is
        select Profile_Value
        from   XXCP_SYS_PROFILE
        where  profile_name = 'VALIDATE COLUMN ATTRIBUTES';
        
 Begin
/*   -- Sabrix
   gTP_TAX_Class_Name := 'SABRIX';
   For Rec in SAB_CLASS loop
     gTP_TAX_Class_Name := Rec.Tax_Class_Name;
   End Loop;*/
   -- KSP17032009 Default Offset
   gTrue_up_Offset := 1;
   For Rec in TUOFF loop
     gTrue_up_Offset := Rec.offset;
   End Loop;
   -- KSP17032009 Tier One Control
   For Rec in T1CTL loop
     gTierOne_Control := Rec.TierOneCtl;
   End loop;

   For Rec in CPA_Calcs loop
     gDisable_CPA_Calc := Rec.disable_cpa_calc;     
   End loop;

   -- Fetch Payer and Ret/Pro indicator dynamic attribute placeholders.
   open  PayAtt;
   fetch PayAtt into gPayerAttribute;
   close PayAtt;

   open  RetProAtt;
   fetch RetProAtt into gPayerRetProAttribute;
   close RetProAtt;

   open  TargType;
   fetch TargType into gTargetTypeName;
   close TargType;
   
   open  ValColAtts;
   fetch ValColAtts into gValidateColumnAttributes;
   close ValColAtts;   

   -- Check to See you can call Oracles Cross validation logic in preview mode
   xxcp_te_engine.gPrevCrossValovr := 'N';
   For rec in CROSVAL loop
      If rec.prevcrossvalovr_cnt > 0 then
          xxcp_te_engine.gPrevCrossValOvr := 'Y';
      End If;
   End loop;
   -- Check to see if Cache Summaries are needed, if not then
   -- for performance reasons do not call them.

   gCacheSummaries  := 'Y';

    For rec in CSHSUM loop
     gCacheSummaries  := 'N';
      If Rec.Cnt > 0 then
        gCacheSummaries  := 'Y';
      End If;
    End Loop;
    -- Reset the Transaction Table each time the engine rules.
    -- The preview control module will reset it separateley.
    xxcp_wks.Last_Transaction_Table := '~#~';
 End INIT;

--  Initialize Package
BEGIN
   Init;
END XXCP_TE_ENGINE;
/
