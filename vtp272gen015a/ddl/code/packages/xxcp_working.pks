CREATE OR REPLACE PACKAGE XXCP_WKS IS
/******************************************************************************
                        V I R T A L  T R A D E R  L I M I T E D 
  		      (Copyright 2007-2012 Virtual Trader Ltd. All rights Reserved)

   NAME:       XXCP_WKS
   PURPOSE:    Define Working Storage
   
   Version [03.06.15] Build Date [21-NOV-2012] Name [XXCP_WKS]

******************************************************************************/


   Max_Dynamic_Attribute  pls_integer := 0;
   Max_Column_Attribute   pls_integer := 0;
   --
   Max_Configuration      pls_integer := 0;
   Max_IC_Pricing         pls_integer := 0;
   Max_Summary            pls_integer := 20;
   Max_Pricing            pls_integer := 2;
   Max_Accounting         pls_integer := 2;
   Max_Custom_Attributes  pls_integer := 0;
   Max_Internal_Attribute pls_integer := 0;
   
   Max_Array_size         pls_integer := 150;
   -- Trace Eliments
   Trace_Log              Long;
   Custom_Event_Log       Long;
	 Values_Log             Long;
	 Segments_Log           Long;
	 ColumnAttrbutes_Log    Long;
   Pricing_Ladder_Log     Long;
   Ownership_Log          Long;
	 Formulas_Log           Long;
   Pass_Through_Log       Long;
   
	 Zero_Suppression_rule  Varchar2(1);

   vClear_Balancing_Array XXCP_DYNAMIC_ARRAY_LONG :=  XXCP_DYNAMIC_ARRAY_LONG(' ',' ',' ',' ',' ',' ',' ',' ',' ',' '); 

   Last_Transaction_Table xxcp_sys_source_tables.transaction_Table%TYPE := '~#~';

   -- RealTime Results
   TYPE gREALTIME_RESULTS_TYPE is  RECORD(
       ATTRIBUTE1           xxcp_rt_interface.VT_RESULTS_ATTRIBUTE1%type,
       ATTRIBUTE2           xxcp_rt_interface.VT_RESULTS_ATTRIBUTE2%type,
       ATTRIBUTE3           xxcp_rt_interface.VT_RESULTS_ATTRIBUTE3%type,
       ATTRIBUTE4           xxcp_rt_interface.VT_RESULTS_ATTRIBUTE4%type,
       ATTRIBUTE5           xxcp_rt_interface.VT_RESULTS_ATTRIBUTE5%type,
       ATTRIBUTE6           xxcp_rt_interface.VT_RESULTS_ATTRIBUTE6%type,
       ATTRIBUTE7           xxcp_rt_interface.VT_RESULTS_ATTRIBUTE7%type,
       ATTRIBUTE8           xxcp_rt_interface.VT_RESULTS_ATTRIBUTE8%type,
       ATTRIBUTE9           xxcp_rt_interface.VT_RESULTS_ATTRIBUTE9%type,
       ATTRIBUTE10          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE10%type,
       -- 02
       ATTRIBUTE11          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE11%type,
       ATTRIBUTE12          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE12%type,
       ATTRIBUTE13          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE13%type,
       ATTRIBUTE14          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE14%type,
       ATTRIBUTE15          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE15%type,
       ATTRIBUTE16          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE16%type,
       ATTRIBUTE17          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE17%type,
       ATTRIBUTE18          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE18%type,
       ATTRIBUTE19          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE19%type,
       ATTRIBUTE20          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE20%type,
       ATTRIBUTE21          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE21%type,
       ATTRIBUTE22          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE22%type,
       ATTRIBUTE23          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE23%type,
       ATTRIBUTE24          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE24%type,
       ATTRIBUTE25          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE25%type,
       ATTRIBUTE26          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE26%type,
       ATTRIBUTE27          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE27%type,
       ATTRIBUTE28          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE28%type,
       ATTRIBUTE29          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE29%type,
       ATTRIBUTE30          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE30%type,
       --02.06.05
       ATTRIBUTE31          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE31%type,
       ATTRIBUTE32          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE32%type,
       ATTRIBUTE33          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE33%type,
       ATTRIBUTE34          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE34%type,
       ATTRIBUTE35          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE35%type,
       ATTRIBUTE36          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE36%type,
       ATTRIBUTE37          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE37%type,
       ATTRIBUTE38          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE38%type,
       ATTRIBUTE39          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE39%type,
       ATTRIBUTE40          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE40%type,
       ATTRIBUTE41          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE41%type,
       ATTRIBUTE42          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE42%type,
       ATTRIBUTE43          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE43%type,
       ATTRIBUTE44          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE44%type,
       ATTRIBUTE45          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE45%type,
       ATTRIBUTE46          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE46%type,
       ATTRIBUTE47          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE47%type,
       ATTRIBUTE48          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE48%type,
       ATTRIBUTE49          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE49%type,
       ATTRIBUTE50          xxcp_rt_interface.VT_RESULTS_ATTRIBUTE50%type
      );
      
   gRealTime_Results       gREALTIME_RESULTS_TYPE;
   gEmpty_RealTime_Results gREALTIME_RESULTS_TYPE;

   TYPE gActual_costs_Rec_Type is RECORD(
      TRANSACTION_ID                  xxcp_actual_costs.TRANSACTION_ID%type,
      SOURCE_ASSIGNMENT_ID            xxcp_actual_costs.SOURCE_ASSIGNMENT_ID%type,
      COMPANY                         xxcp_actual_costs.COMPANY%type,
      DEPARTMENT                      xxcp_actual_costs.DEPARTMENT%type,
      ACCOUNT                         xxcp_actual_costs.ACCOUNT%type,
      PERIOD_NET_DR                   xxcp_actual_costs.PERIOD_NET_DR%type,
      PERIOD_NET_CR                   xxcp_actual_costs.PERIOD_NET_CR%type,
      CURRENCY_CODE                   xxcp_actual_costs.CURRENCY_CODE%type,
      COLLECTOR_ID                    xxcp_actual_costs.COLLECTOR_ID%type,
      PERIOD_SET_NAME_ID              xxcp_actual_costs.PERIOD_SET_NAME_ID%type,
      COLLECTION_PERIOD_ID            xxcp_actual_costs.COLLECTION_PERIOD_ID%type,
      REQUEST_ID                      xxcp_actual_costs.REQUEST_ID%type,
      PERIOD_END_DATE                 xxcp_actual_costs.PERIOD_END_DATE%type,
      FC_COST_CATEGORY_ID             number(15),  --xxcp_actual_costs.FC_COST_CATEGORY_ID%type,
      TU_COST_CATEGORY_ID             number(15),  --xxcp_actual_costs.TU_COST_CATEGORY_ID%type,
      FC_CATEGORY_DATA_SOURCE         varchar2(2), --xxcp_actual_costs.FC_CATEGORY_DATA_SOURCE%type,
      TU_CATEGORY_DATA_SOURCE         varchar2(2), --xxcp_actual_costs.TU_CATEGORY_DATA_SOURCE%type,
      DATA_SOURCE                     xxcp_actual_costs.DATA_SOURCE%type,   
      PERIOD_ID                       xxcp_actual_costs.COLLECTION_PERIOD_ID%type,
      PAYER                           varchar2(10),
      PAYER_RET_PRO_IND               varchar2(1),
      REPLAN                          varchar2(1),
      INSTANCE_ID                     number(4),
      ATTRIBUTE1                      xxcp_cost_plus_interface.attribute1%type,
      ATTRIBUTE2                      xxcp_cost_plus_interface.attribute2%type,
      ATTRIBUTE3                      xxcp_cost_plus_interface.attribute3%type,
      ATTRIBUTE4                      xxcp_cost_plus_interface.attribute4%type,
      ATTRIBUTE5                      xxcp_cost_plus_interface.attribute5%type,
      ATTRIBUTE6                      xxcp_cost_plus_interface.attribute6%type,
      ATTRIBUTE7                      xxcp_cost_plus_interface.attribute7%type,
      ATTRIBUTE8                      xxcp_cost_plus_interface.attribute8%type,
      ATTRIBUTE9                      xxcp_cost_plus_interface.attribute9%type,
      ATTRIBUTE10                     xxcp_cost_plus_interface.attribute10%type,
      ATTRIBUTE11                     xxcp_cost_plus_interface.attribute11%type,
      ATTRIBUTE12                     xxcp_cost_plus_interface.attribute12%type,
      ATTRIBUTE13                     xxcp_cost_plus_interface.attribute13%type,
      ATTRIBUTE14                     xxcp_cost_plus_interface.attribute14%type,
      ATTRIBUTE15                     xxcp_cost_plus_interface.attribute15%type,
      ATTRIBUTE16                     xxcp_cost_plus_interface.attribute16%type,
      ATTRIBUTE17                     xxcp_cost_plus_interface.attribute17%type,
      ATTRIBUTE18                     xxcp_cost_plus_interface.attribute18%type,
      ATTRIBUTE19                     xxcp_cost_plus_interface.attribute19%type,
      ATTRIBUTE20                     xxcp_cost_plus_interface.attribute20%type,
      TRANSACTION_TABLE               xxcp_cost_plus_interface.vt_transaction_table%type,
      INTERFACE_ID                    xxcp_cost_plus_interface.vt_interface_id%type, 
      CREATION_DATE                   xxcp_actual_costs.CREATION_DATE%type,
      CREATED_BY                      xxcp_actual_costs.CREATED_BY%type,
      LAST_UPDATE_LOGIN               xxcp_actual_costs.LAST_UPDATE_LOGIN%type,
      LAST_UPDATED_BY                 xxcp_actual_costs.LAST_UPDATED_BY%type,
      LAST_UPDATE_DATE                xxcp_actual_costs.LAST_UPDATE_DATE%type,
      VT_REPLAN_COMPANY               xxcp_cost_plus_interface.VT_REPLAN_COMPANY%type,
      VT_REPLAN_DEPARTMENT            xxcp_cost_plus_interface.VT_REPLAN_DEPARTMENT%type,
      VT_REPLAN_ACCOUNT               xxcp_cost_plus_interface.VT_REPLAN_ACCOUNT%type,
      VT_REPLAN_TRANSACTION_ID        xxcp_cost_plus_interface.VT_REPLAN_TRANSACTION_ID%type,
      VT_REPLAN_TRADING_SET_ID        xxcp_cost_plus_interface.VT_REPLAN_TRADING_SET_ID%type,
      VT_REPLAN_AMOUNT                xxcp_cost_plus_interface.VT_REPLAN_AMOUNT%type,
      VT_REPLAN_CURRENCY_CODE         xxcp_cost_plus_interface.VT_REPLAN_CURRENCY_CODE%type,
      VT_REPLAN_OWNER_TAX_REG_ID      xxcp_cost_plus_interface.VT_REPLAN_OWNER_TAX_REG_ID%type,
      VT_REPLAN_PARTNER_TAX_REG_ID    xxcp_cost_plus_interface.VT_REPLAN_PARTNER_TAX_REG_ID%type,
      VT_REPLAN_MC_QUALIFIER1         xxcp_cost_plus_interface.VT_REPLAN_MC_QUALIFIER1%type,
      VT_REPLAN_COST_CATEGORY_ID      xxcp_cost_plus_interface.VT_REPLAN_COST_CATEGORY_ID%type,
      VT_REPLAN_CATEGORY_DATA_SOURCE  xxcp_cost_plus_interface.VT_REPLAN_CATEGORY_DATA_SOURCE%type,
      VT_REPLAN_PROCESS_HISTORY_ID    xxcp_cost_plus_interface.VT_REPLAN_PROCESS_HISTORY_ID%type,
      VT_REPLAN_PERIOD_ID             xxcp_cost_plus_interface.VT_REPLAN_PERIOD_ID%type,
      VT_REPLAN_PAYER1                xxcp_cost_plus_interface.VT_REPLAN_PAYER1%type,
      VT_REPLAN_PAYER1_PERCENT        xxcp_cost_plus_interface.VT_REPLAN_PAYER1_PERCENT%type,
      VT_REPLAN_PAYER2                xxcp_cost_plus_interface.VT_REPLAN_PAYER2%type,
      VT_REPLAN_PAYER2_PERCENT        xxcp_cost_plus_interface.VT_REPLAN_PAYER2_PERCENT%type,
      VT_REPLAN_INTER_PAYER           xxcp_cost_plus_interface.VT_REPLAN_INTER_PAYER%type,
      VT_REPLAN_ACCOUNT_TYPE          xxcp_cost_plus_interface.VT_REPLAN_ACCOUNT_TYPE%type,
      COST_PLUS_SET_ID                xxcp_cost_plus_interface.COST_PLUS_SET_ID%type,
      TAX_AGREEMENT_NUMBER            xxcp_cost_plus_interface.TAX_AGREEMENT_NUMBER%type       
    );
   
   
   TYPE gActual_Costs_Type is TABLE of gActual_costs_Rec_Type
                           INDEX BY BINARY_INTEGER;
    
   
   gActual_Costs_Rec        gActual_Costs_Type;
      

   --
   -- Finance Engine Array
   --
   TYPE gVT_TYPE is  RECORD(
       request_id               xxcp_process_history.request_id%type,
       rule_id                  xxcp_column_rules.rule_id%type,
       model_ctl_id             xxcp_model_ctl.model_ctl_id%type,
       target_table             xxcp_sys_target_tables.transaction_table%type,
       transaction_table        xxcp_sys_source_tables.transaction_table%type,
       trading_set              xxcp_trading_sets.trading_set%type,
       trading_set_id           xxcp_trading_sets.trading_set_id%type,
       trading_set_seq          xxcp_trading_sets.sequence%type,
       target_reg_id            xxcp_tax_registrations.reg_id%type,
       activity_type            varchar2(10),
       --
       balancing_segment        xxcp_target_assignments.balancing_segment%type,
       rounding_group           xxcp_account_rules.rounding_group%type,
       --
       status                   varchar2(10),
       transaction_ref1         xxcp_transaction_attributes.transaction_ref1%type,
       source_rowid             rowid,
       parent_rowid             rowid,
       source_pos               pls_integer,
       table_level              varchar2(1),
			 Rule_Entity_Type         varchar2(1),
       attribute_id             xxcp_transaction_attributes.attribute_id%type,
       record_type              xxcp_process_history.record_type%type,
       interface_id             xxcp_process_history.interface_id%type,
       set_of_books_id          xxcp_process_history.set_of_books_id%type,
       tax_registration_id      xxcp_process_history.tax_registration_id%type,
       target_assignment_id     xxcp_process_history.target_assignment_id%type,
       transaction_class        xxcp_process_history.transaction_class%type,
			 target_zero_flag         xxcp_target_assignments.zero_flag%type,
       target_type_id           xxcp_sys_target_types.target_id%type,
       transaction_id           xxcp_transaction_attributes.transaction_id%type,
       target_instance_id       xxcp_target_assignments.target_instance_id%type,
			 --
       entered_amount           xxcp_process_history.entered_cr%type,
			 entered_currency         xxcp_tax_registrations.legal_currency%type,
       entered_rnd_amount       xxcp_process_history.rounding_amount%type,
			 --
			 accounted_currency       xxcp_tax_registrations.legal_currency%type,
       accounted_amount         xxcp_process_history.accounted_cr%type,
       accounted_rnd_amount     xxcp_process_history.rounding_amount%type,
			 --
			 record_status            xxcp_process_history.status%type,
			 target_rounding          varchar2(1),
       round_suppressed_rules   varchar2(1),
			 --
			 no_rounding_rule         varchar2(1),
			 Zero_Suppression_rule    varchar2(1),
       Extended_Values          xxcp_sys_target_tables.extended_values%type,
       Entered_Amounts          xxcp_sys_target_tables.entered_amt_bal%type,
       Accounted_Amounts        xxcp_sys_target_tables.accounted_amt_bal%type,
       -- 02.06.01
       p1001_array              xxcp_dynamic_array, -- Parent Assignment
       c1001_array              xxcp_dynamic_array, -- Common Assignment
			 --
       d1001_array              xxcp_dynamic_array, -- Assignment
       d1002_array              xxcp_dynamic_array, -- i.c pricing
       d1003_array              xxcp_dynamic_array, -- accounting
       d1004_array              xxcp_dynamic_array, -- pricing
       d1005_array              xxcp_dynamic_array, -- custom events
       d1006_array              xxcp_dynamic_array, -- custom attributes
       i1009_array              xxcp_dynamic_array, -- internal
       process_history_id       xxcp_process_history.process_history_id%type,
       balancing_elements       xxcp_dynamic_array_long := xxcp_dynamic_array_long(' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '),
       cost_plus_rule           xxcp_account_rules.cost_plus_rule%TYPE,
       Record_Number            number := 0,  -- fred
       Line_Number_Assigned     number := 0,
       Target_Suppression_rule  varchar2(1),
       History_Suppression_rule varchar2(1)
    );

   TYPE gVT_REC    is TABLE of gVT_TYPE    INDEX by BINARY_INTEGER;



   -- Working Storage    
   WORKING_RCD       gVT_REC;
   WORKING_CNT       pls_Integer := 0;
   
   -- Only used in the package
   WORKING_HIGH_TIDE pls_integer:= 0;
   
   DYNAMIC_RESULTS_CNT pls_integer;
   DYNAMIC_RESULTS XXCP_DYNAMIC_ARRAY;
   CLEAR_DYNAMIC_RESULTS XXCP_DYNAMIC_ARRAY;
   
   TYPE gBL_TYPE is  RECORD(
	   Target_Type_id           Number,
		 Target_Record_Status     Varchar2(10),
     balancing_element1       Varchar2(300),
     balancing_element2       Varchar2(300),
     balancing_element3       Varchar2(300),
     balancing_element4       Varchar2(300),
     balancing_element5       Varchar2(300),
     balancing_element6       Varchar2(300),
     balancing_element7       Varchar2(300),
     balancing_element8       Varchar2(300),
     balancing_element9       Varchar2(300),
     balancing_element10      Varchar2(300),
     balancing_element11      Varchar2(300),
     balancing_element12      Varchar2(300),
		 --
		 record_count              number,
		 -- Accounted 
     Highest_Accounted_Pointer number,
     Highest_Accounted_Value   number,
     Lowest_Accounted_Pointer  number,
     Lowest_Accounted_Value    number,
     Accounted_Sum             number,
		 -- Entered
     Highest_Entered_Pointer   number,
     Highest_Entered_Value     number,
     Lowest_Entered_Pointer    number,
     Lowest_Entered_Value      number,
		 Entered_Sum               number
     );

   TYPE gBL_REC    is TABLE of gBL_TYPE    INDEX by BINARY_INTEGER;
   
   BALANCING_RCD       gBL_REC;
   BALANCING_CNT       pls_integer := 0;
   BALANCING_HIGH_TIDE pls_integer := 0;

   SOURCE_ROWID          dbms_sql.varchar2_table;
   SOURCE_STATUS         dbms_sql.varchar2_table;
   SOURCE_SUB_CODE       dbms_sql.varchar2_table;
   SOURCE_CNT            pls_integer;
   SOURCE_HIGH_TIDE      pls_integer := 0;

   Source_Balancing xxcp_dynamic_array_long := xxcp_dynamic_array_long(' ');
   
   vEnabled_Account_Segments  XXCP_DYNAMIC_ARRAY := XXCP_DYNAMIC_ARRAY('N','N','N','N','N','N','N','N','N','N'||
                                                        'N','N','N','N','N','N','N','N','N','N'||
                                                        'N','N','N','N','N','N','N','N','N','N'||
                                                        'N','N','N','N','N','N','N','N','N','N'||
                                                        'N','N','N','N','N','N','N','N','N','N');
														
   -- 
   -- Enabled Account Segments 
   -- 
   gENABLED_SEGS_Cnt pls_integer := 0;												
   TYPE gENABLED_SEGS_TYPE is  RECORD(
     Chart_of_accounts_id     Number(15),
     Instance                 Number(4),
     Enabled_Account_Segments XXCP_DYNAMIC_ARRAY:= XXCP_DYNAMIC_ARRAY('N','N','N','N','N','N','N','N','N','N'||
                                                        'N','N','N','N','N','N','N','N','N','N'||
                                                        'N','N','N','N','N','N','N','N','N','N'||
                                                        'N','N','N','N','N','N','N','N','N','N'||
                                                        'N','N','N','N','N','N','N','N','N','N')
     );
	 
   TYPE gENABLED_SEGS_REC    is TABLE of gENABLED_SEGS_TYPE    INDEX by BINARY_INTEGER;

   gENABLED_SEGS gENABLED_SEGS_REC;
	 
   -- 
   -- SEGMENTS  
   -- 

   Clear_Segments  XXCP_DYNAMIC_ARRAY := XXCP_DYNAMIC_ARRAY(' ',' ',' ',' ',' ',' ',' ',' ',' ',' '||
                                                        ' ',' ',' ',' ',' ',' ',' ',' ',' ',' '||
                                                        ' ',' ',' ',' ',' ',' ',' ',' ',' ',' '||
                                                        ' ',' ',' ',' ',' ',' ',' ',' ',' ',' '||
                                                        ' ',' ',' ',' ',' ',' ',' ',' ',' ',' ');

   Clear_Dynamic        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   Clear_1002_Dynamic   XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   Clear_1003_Dynamic   XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   Clear_1004_Dynamic   XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   Clear_1006_Dynamic   XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   Clear_Internal       XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');

   --
   -- TEMP 
   -- 
   I1006_ArrayTEMP               XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   I1009_ArrayTEMP               XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   I1009_ArrayGLOBAL             XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' '); -- CHG20090702SJS
   vChild_Internal_Base_Array    XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   vChild_Internal_Base_Cnt      pls_integer; 
   gDis_1001_Array               XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   gDis_1002_Array               XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   
   vBase_Internal_Array          XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   vLocal_Internal_Base_Array    XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   -- Long data type for transaction references
   gLongRef_Count                Number(3) := 40;
   Clear_L1001_Array             xxcp_dynamic_array_long := xxcp_dynamic_array_long(' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
                                                                                    ' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ');
   L1001_Array                   xxcp_dynamic_array_long := xxcp_dynamic_array_long(' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
                                                                                    ' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ');  
   ---
   --- Standard WKS
   ---
   -- 02.06.01
   C1001_Array        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   --
   P1001_Array        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   --
   D1001_Array        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   D1002_Array        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   D1003_Array        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   D1004_Array        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   D1005_Array        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   D1006_Array        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   D1007_Array        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   D1008_Array        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   I1009_Array        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   
   -- 02.06.01
   C1001_Count        pls_integer;
   D1001_Count        pls_integer;
   D1002_Count        pls_integer;
   D1003_Count        pls_integer;
   D1004_Count        pls_integer;
   D1005_Count        pls_integer;
   D1006_Count        pls_integer;
   D1007_Count        pls_integer;
   D1008_Count        pls_integer;
   I1009_Count        pls_integer;


   ---
   --- Child WKS - Transaction Engine
   ---
   D1001_ArrayCH        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   D1002_ArrayCH        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   D1003_ArrayCH        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   D1004_ArrayCH        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   D1005_ArrayCH        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   D1006_ArrayCH        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   D1007_ArrayCH        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   D1008_ArrayCH        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   I1009_ArrayCH        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   vPrev_ArrayCH        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   vBase_ArrayCH        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   D1001_CountCH        pls_integer;
   D1002_CountCH        pls_integer;
   D1003_CountCH        pls_integer;
   D1004_CountCH        pls_integer;
   D1005_CountCH        pls_integer;
   D1006_CountCH        pls_integer;
   D1007_CountCH        pls_integer;
   D1008_CountCH        pls_integer;
   I1009_CountCH        pls_integer;

   ---
   --- Local WKS - Transaction Engine
   ---
   D1001_ArrayLC        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   D1002_ArrayLC        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   D1003_ArrayLC        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   D1004_ArrayLC        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   D1005_ArrayLC        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   D1006_ArrayLC        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   D1007_ArrayLC        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   D1008_ArrayLC        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   I1009_ArrayLC        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' '); 
   vBase_ArrayLC        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');

   D1001_CountLC        pls_integer;
   D1002_CountLC        pls_integer;
   D1003_CountLC        pls_integer;
   D1004_CountLC        pls_integer;
   D1005_CountLC        pls_integer;
   D1006_CountLC        pls_integer;
   D1007_CountLC        pls_integer;
   D1008_CountLC        pls_integer;
   I1009_CountLC        pls_integer;

   ---
   --- Gain and Loss WKS - Transaction Engine
   ---
   D1001_ArrayGL        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   D1002_ArrayGL        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   D1003_ArrayGL        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   D1004_ArrayGL        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   D1005_ArrayGL        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   D1006_ArrayGL        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   D1007_ArrayGL        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   D1008_ArrayGL        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   I1009_ArrayGL        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' '); 
   vBase_ArrayGL        XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');

   D1001_CountGL        pls_integer;
   D1002_CountGL        pls_integer;
   D1003_CountGL        pls_integer;
   D1004_CountGL        pls_integer;
   D1005_CountGL        pls_integer;
   D1006_CountGL        pls_integer;
   D1007_CountGL        pls_integer;
   D1008_CountGL        pls_integer;
   I1009_CountGL        pls_integer;

   Account_Segments     XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   Enabled_Segments     XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   Source_Segments      XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');
   

   --- Break Points
   TYPE gBRK_SET_TYPE is  RECORD(
     BREAK_QTY            Number,
		 BREAK_POINT_ID       Number, -- New
     BREAK_CACHE          XXCP_DYNAMIC_ARRAY :=  XXCP_DYNAMIC_ARRAY('0','0','0','0','0'),
     --
     SUM_QTY              Number,
     SUMMARY_CACHE        XXCP_DYNAMIC_ARRAY :=  XXCP_DYNAMIC_ARRAY('0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'),
     --
     NUMERIC_CACHE        XXCP_DYNAMIC_ARRAY :=  XXCP_DYNAMIC_ARRAY('0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'),
     TS_CACHE             XXCP_DYNAMIC_ARRAY :=  XXCP_DYNAMIC_ARRAY('0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'),
		 TRX_QUALIFIER1_EXPR  XXCP_SUMMARY_BREAK_PTS.TRX_QUALIFIER1_EXPR%type,  
		 TRX_QUALIFIER2_EXPR  XXCP_SUMMARY_BREAK_PTS.TRX_QUALIFIER1_EXPR%type,
     COUNT_CACHE          Number
     );

   TYPE gBRK_SET_REC    is TABLE of gBRK_SET_TYPE    INDEX by BINARY_INTEGER;
   BRK_SET_RCD      gBRK_SET_REC;

   --- Cache Summaries
   TYPE gSUM_SET_TYPE is  RECORD(
     Trading_Set             xxcp_trading_sets.trading_set%type,
     BREAK_CACHE_ATTRIBUTES  XXCP_DYNAMIC_ARRAY :=  XXCP_DYNAMIC_ARRAY('~#~','~#~','~#~','~#~','~#~','~#~','~#~','~#~','~#~','~#~','~#~','~#~','~#~','~#~','~#~','~#~','~#~','~#~'),
     CACHE_VALUE             XXCP_DYNAMIC_ARRAY :=  XXCP_DYNAMIC_ARRAY('0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'),
     COUNT_VALUE             XXCP_DYNAMIC_ARRAY :=  XXCP_DYNAMIC_ARRAY('0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0'),
     CNT                     number
     );

   TYPE gSUM_SET_REC    is TABLE of gSUM_SET_TYPE    INDEX by BINARY_INTEGER;

   SUM_GRP_RCD       gSUM_SET_REC;
   TST_GRP_RCD       gSUM_SET_REC;

   TYPE gONCE_TYPE is  RECORD(
     Model_Name_id     xxcp_sys_model_names.model_name_id%type,
     Transaction_Table xxcp_sys_source_tables.transaction_table%type,
     Transaction_Type  xxcp_sys_source_types.type%type,
     Trading_Set_id    xxcp_trading_sets.trading_set_id%type,
     Rule_id           xxcp_account_rules.rule_id%type,
     --
     Break_Value1      varchar2(250),
     Break_Value2      varchar2(250),
     Break_Value3      varchar2(250),
     Break_Value4      varchar2(250)
     );

   TYPE gONCE_REC is TABLE of gONCE_TYPE    INDEX by BINARY_INTEGER;

   ONCE_RCD       gONCE_REC;

   ----------------------------------------------------------------------
   -- !! Sabrix Constants
   ----------------------------------------------------------------------
   gSABRIX_SUCCESS			 CONSTANT NUMBER(2) := 0;
   gSABRIX_NO_RATE			 CONSTANT NUMBER(2) := 2;
   gSABRIX_UNEXPERR			 CONSTANT NUMBER(2) := 9;

   --
   -- Tax Engine Variables
   -- Designed to work with Sabrix,
   -- but could be used with others.
   --
/*  TYPE gTax_Rec_Type IS RECORD
    (
    TRX_LINE_ID         NUMBER,
    TAX_CODE            VARCHAR2(100),
    TAX_ID              NUMBER,
    TAX_LINE_NUMBER     NUMBER,
    TAX_AMOUNT          NUMBER,
    TAX_RATE            NUMBER,
    TAXABLE_BASIS       NUMBER,
    GROSS_AMOUNT        NUMBER,
    CNT_COMPONENTS      NUMBER,
    CNT_TAXCODES        NUMBER,
    USED_FLAG           VARCHAR2(1),
    STATE_TAX_AMOUNT    NUMBER,
    STATE_TAX_RATE      NUMBER,
    COUNTY_TAX_AMOUNT   NUMBER,
    COUNTY_TAX_RATE     NUMBER,
    CITY_TAX_AMOUNT     NUMBER,
    CITY_TAX_RATE       NUMBER,
    LOCAL_TAX_AMOUNT    NUMBER,
    LOCAL_TAX_RATE      NUMBER,
    VAT_AMOUNT          NUMBER,
    VAT_RATE            NUMBER
    );

    TYPE gTaxCodeTab IS TABLE OF gTax_Rec_Type;

    gTaxCodes gTaxCodeTab := gTaxCodeTab();*/

   ----------------------------------------------------------------------
   -- !! End Sabrix Constants
   ----------------------------------------------------------------------
   --
   -- LineCount_Type  -- new
   -- vt272.p8
   TYPE gLineCount_Type IS RECORD(
     Target_Table             xxcp_sys_source_tables.transaction_table%type,
     Target_Instance_id       xxcp_target_assignments.target_instance_id%type,
     Trading_Set_id           xxcp_trading_sets.trading_set_id%type := 0,
     Line_Number              number := 0
    );

   TYPE gLineCount_REC    IS TABLE OF gLineCount_TYPE    INDEX BY BINARY_INTEGER;
   gLineCount gLineCount_REC;

   --
   --
   -- function
   --

   Function Software_Version RETURN VARCHAR2;

   procedure init;
   procedure DumpWriteError(cInternalErrorCode in Number,
                            cErrorMessage      in varchar2,
                            cLong_Message      in Long Default Null);

   Procedure Reset_D1001;
   Procedure Reset_D1002;
   Procedure Reset_D1003;
   Procedure Reset_D1004;

   Procedure Reset_Gain_Loss_Arrays(cAction in varchar2);
   Procedure Reset_Local_Arrays;
   Procedure Reset_Child_Arrays;
   procedure reset_wkr;
   procedure Reset_Column_Segments;
   procedure Reset_Account_Segments;
   procedure Reset_Enabled_Segments;
   procedure Reset_Source_Segments;
   procedure Reset_TIER_Array;
   procedure Reset_8000_Array;
   procedure Reset_Internal_Array;
   procedure Reset_Dynamic_Results;

   Procedure Check_Trace_Log(cText in varchar2);

END XXCP_WKS;
/
