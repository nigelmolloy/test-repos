CREATE OR REPLACE PACKAGE BODY XXCP_TE_CONFIGURATOR AS
  /**************************************************************************************************************
                          V I R T A L  T R A D E R  L I M I T E D
        (Copyright 2005-2012 Virtual Trader Ltd. All rights Reserved)

     NAME:       xxcp_te_configurator
     PURPOSE:    The Main Assignment process.

     REVISIONS:
     Ver        Date      Author  Description
     ---------  --------  ------- ------------------------------------
     02.00.00   02/08/02  Keith   First Coding
     02.01.02   12/04/04  Keith   Changed to TE version that uses Memory.
     02.01.03   16/05/04  Keith   vIC_UNIT_Price rounded.
     02.01.04   19/05/04  Keith   Removed Rounding for Joli.
     02.01.05   29/06/04  Keith   Adjustment Rate take out of memory
     02.01.06   24/08/04  Keith   9200,9201 Adjustment
     02.01.07   02/09/04  Keith   Changed Attribute to created only if header id <> 0
     02.01.08   18/02/05  Keith   Moved Global arrays into package to save on moving
                                  them around
     02.01.10   01/07/05  Keith   Tax Qualifiers held.
     02.01.11   20/09/05  Keith   Protection Around gCommon
     02.01.12   24/10/05  Keith   Tax for all, Rule_type in SQL
     02.02.00   09/11/05  Keith   Added MC3 MC4 to CC cursor
     02.02.01   22/11/05  Keith   Transaction and Target Currency match dont do Gain and Loss
     02.02.02   17/12/05  Keith   Entity Document Qualifier 3 and 4
     02.03.00   09/02/06  Keith   Def.
     02.03.02   20/03/06  Keith   Configuration Control is not mandary for IC Records.
     02.03.03   10/07/06  Keith   A new Alt Dynamic Interface can be used for AR Invoices
     02.03.04   11/09/06  Keith   Added Effectivity Dates to assignment Control
     02.03.06   20/11/06  Keith   Check Trx Date, Exch. Date and Currency after config.
     02.03.07   03/10/07  Keith   OLS flags
     02.03.08   20/10/07  Keith   Pass Through Logic added
     02.03.09   26/12/07  Keith   Assignment control logic changed
     02.03.10   14/03/08  Keith   Make IC Values available in memory for RT Engine
     02.03.11   08/04/08  Keith   Added gGivenAdjustmentRate
     02.03.12   22/04/08  Keith   Added vCalcualtion Exchange Rate
     02.03.14   04/05/08  Keith   Added Pricing Default added to Pricing Methods and set dff
     02.05.00   29/05/08  Keith   Transaction labels link error 10020
     02.05.01   24/05/08  Keith   added Outbound
     02.05.03   07/10/08  Keith   Improved Default value in Pricing Method
     02.05.04   19/11/08  Nigel   Added default trading relationship change
     02.05.05   05/12/08  Keith   Added more trace around (12) exchange rate type
     02.05.06   12/12/08  Simon   Modified MakeHeader MakeAttribute for Cost Plus Forecast.
     02.05.07   16/03/09  Nigel   Fixed make_transacition_cache to correctly write 100 characters.
     02.05.08   31/03/09  Nigel   Added reset_parent_trx_id
     02.05.09   31/05/09  Simon   Added CPA_Type and True_Up_On.
     02.05.10   26/06/09  Simon   Price_method_id on transaction attributes will now be populated for shared pricing.
     02.05.11   02/07/09  Simon   Add I1009_ArrayGLOBAL for CPA.
     02.06.01   01/07/09  Nigel   Made changes for "Common Assignment"
     02.06.02   10/08/09  Keith   Pricing Ladder and Pricing Method Change
     02.06.03   14/09/09  Keith   Shared Pricing now using Pricing Ladder logic
     02.06.04   21/09/09  Keith   Added set Trading Exchange Rate Type to global default at start
     02.06.05   25/09/09  Keith   Added Formuals
     02.06.06   12/10/09  Simon   Enable Passthrough logic/ Moved StandardPriceAttribute
     02.06.07   08/12/09  Simon   Common DI Pointers not being passed to Normal DI correctly.
     02.06.08   09/12/09  Keith   Added New Soft Assignment Logic for Update
     02.06.09   15/12/09  Nigel   Now passing transaction_class, trading_Set to xxcp_dynamic_sql.non_configurator
     02.06.10   21/12/09  Simon   Populate transaction_date in cache, adjustment rate date in trace
     02.06.11   25/01/10  Keith   Populate Attributes DFF even if SAE record is not created
     02.06.12   02/02/10  Keith   Fixed XAM currency Attribute not being populated
     02.06.14   19/02/10  Keith   adjusted xxcp_sa_errors to update in none preview mode
     02.06.14A  06/05/10  Keith   added more fields to outbound array
     02.06.15   19/05/10  Keith   Pricing Type
     02.06.16   23/08/10  Simon   Fix Pass Through Qualfifiers subscript errors.
     02.06.17   12/11/10  Keith   Added Timing to Custom Events
     02.06.18   18/02/11  Nigel   Now populating vAS_Qualifier1 and vAS_Qualifier2 for common assignment
     02.06.19   28/02/11  Keith   Explosion - Transaction id
     02.06.20   04/07/11  Keith   Added Date to Pass through
     02.06.21   12/07/11  Keith   Added Pass Through Sequences for multiple checks
     02.06.22   21/07/11  Keith   Corrected fault in duplicate dynamic rows found in IC Pricing Di
     02.06.22A  23/11/11  Keith   Added Multiple Owners Logic
     03.06.23   24/01/12  Keith   Increase Transaction Cache records
     03.06.24   27/02/12  Nigel   Fixed issue with formula sets not being called in common,  
                                  added pragma autonomous_transaction on SA. 
     03.06.25   28/03/12  Keith   Added No Cache (OOD-91)
     03.06.26   19/07/12  Keith   Fixed but in Assignment logic to use 7002
     03.06.27   27/07/12  Nigel   Fixed parent being fired for each transaction (OOD-223)
     03.06.28   27/07/12  Nigel   Fixed issue with Assignment Rule required not working for second leg (OOD-221)
     03.06.29   15/08/12  Simon   Fixed vHac_Cnt not incrementing (OOD-254).
     03.06.30   12/09/12  Nigel   Added Unmatched processing 
     03.06.31   19/09/12  Simon   Added Cost_Plus_Set_id (OOD-293).
     03.06.32   05/10/12  Simon   Increase Dyn Attr length from 100 to 250 (OOD-195).
     03.06.33   15/10/12  Keith   Added Transction References call
     03.06.34   24/10/12  Keith   Added New Tax Rates Logic
  **************************************************************************************************************/

  gLastICCurrency      Varchar2(15);
  gLastICPMID          Number(8)   := 0;
  gNexTrxParentTrxId   Number      := 0;
  gNewTrxTableId       Number      := 0;
  gLastICPrice         Number      := 0;
  gLastDftPMused       Varchar2(1) := 'N';
  gMax_OLS_Date        Date        := to_date('31-DEC-2999','DD-MON-YYYY');
  gCentrySwitchDate    Date        := to_date('01-JAN-1950','DD-MON-YYYY');
  gMaxPricingLadder    Number      := 21; -- 20 steps + 1 to fail on

  gInboundRec          xxcp_vt_interface%rowtype;
  gLastLadderSetId     Number      := 0;

  gTraceStartTime      timestamp;
  gTraceEndTime        timestamp;
  
  Type gLadderQualifiers_Type IS RECORD (
     qualifier1_id   xxcp_dynamic_attributes.column_id%type,
     qualifier2_id   xxcp_dynamic_attributes.column_id%type,
     qualifier3_id   xxcp_dynamic_attributes.column_id%type,
     qualifier4_id   xxcp_dynamic_attributes.column_id%type
   );

  gLadderQualifiers gLadderQualifiers_Type;

  Type gLadder_Type IS RECORD(
     Ladder_Leg               pls_integer,
     Ladder_Set_id            xxcp_pricing_ladder.ladder_set_id%type,
     Prior_Ladder_Set_id      xxcp_pricing_ladder.ladder_set_id%type,
     Prior_Ladder_Seq         xxcp_pricing_ladder.seq%type
     );

   TYPE gLadder_REC    IS TABLE OF gLadder_Type    INDEX BY BINARY_INTEGER;

  gLadder_Set     gLadder_REC;

  Type gXAM_Type IS RECORD(
     Owner_Legal_Currency     xxcp_tax_registrations.legal_currency%type,
     Partner_Legal_Currency   xxcp_tax_registrations.legal_currency%type,
     Owner_Trade_Currency     xxcp_tax_registrations.legal_currency%type,
     Partner_Trade_Currency   xxcp_tax_registrations.legal_currency%type,
     To_Curr_Attribute        xxcp_tax_registrations.legal_currency%type,
     To_Curr_Indicator        xxcp_pricing_method.to_curr_indicator%type,
     To_IC_Currency           xxcp_transaction_header.owner_legal_curr%type,
     Trade_Exch_Rate_Type     xxcp_pricing_method.exchange_rate_type%type,
     Partner_Common_Exch_Curr xxcp_tax_registrations.legal_currency%type,
     From_IC_Currency         xxcp_tax_registrations.legal_currency%type,
     From_IC_Unit_Price       xxcp_transaction_attributes.ic_unit_price%type,
     From_IC_Price            xxcp_transaction_attributes.ic_price%type,
     IC_Exch_Rate             xxcp_transaction_attributes.partner_legal_exch_rate%type,
     Pricing_Calculation      xxcp_pricing_method.pricing_calculation%type,
     Dflt_CommExchCurr        xxcp_tax_registrations.legal_currency%type
     );

   TYPE gXAM_REC    IS TABLE OF gXAM_Type    INDEX BY BINARY_INTEGER;

   gXAM_Set     gXAM_REC;

  --
  -- Transaction_Labels RECORD
  --
  gLast_Source_Type_id Number := 0;

  TYPE gLabels_Type IS RECORD(
     custom_attribute1_id     xxcp_transaction_labels.custom_attribute1_id%Type,
     custom_attribute2_id     xxcp_transaction_labels.custom_attribute1_id%Type,
     custom_attribute3_id     xxcp_transaction_labels.custom_attribute1_id%Type,
     custom_attribute4_id     xxcp_transaction_labels.custom_attribute1_id%Type,
     custom_attribute5_id     xxcp_transaction_labels.custom_attribute1_id%Type,
     custom_attribute6_id     xxcp_transaction_labels.custom_attribute1_id%Type,
     custom_attribute7_id     xxcp_transaction_labels.custom_attribute1_id%Type,
     custom_attribute8_id     xxcp_transaction_labels.custom_attribute1_id%Type,
     custom_attribute9_id     xxcp_transaction_labels.custom_attribute1_id%Type,
     custom_attribute10_id    xxcp_transaction_labels.custom_attribute1_id%Type,
     -- new
     custom_attribute11_id     xxcp_transaction_labels.custom_attribute1_id%Type,
     custom_attribute12_id     xxcp_transaction_labels.custom_attribute1_id%Type,
     custom_attribute13_id     xxcp_transaction_labels.custom_attribute1_id%Type,
     custom_attribute14_id     xxcp_transaction_labels.custom_attribute1_id%Type,
     custom_attribute15_id     xxcp_transaction_labels.custom_attribute1_id%Type,
     custom_attribute16_id     xxcp_transaction_labels.custom_attribute1_id%Type,
     custom_attribute17_id     xxcp_transaction_labels.custom_attribute1_id%Type,
     custom_attribute18_id     xxcp_transaction_labels.custom_attribute1_id%Type,
     custom_attribute19_id     xxcp_transaction_labels.custom_attribute1_id%Type,
     custom_attribute20_id     xxcp_transaction_labels.custom_attribute1_id%Type,
     custom_attribute21_id     xxcp_transaction_labels.custom_attribute1_id%Type,
     custom_attribute22_id     xxcp_transaction_labels.custom_attribute1_id%Type,
     custom_attribute23_id     xxcp_transaction_labels.custom_attribute1_id%Type,
     custom_attribute24_id     xxcp_transaction_labels.custom_attribute1_id%Type,
     custom_attribute25_id     xxcp_transaction_labels.custom_attribute1_id%Type,
     custom_attribute26_id     xxcp_transaction_labels.custom_attribute1_id%Type,
     custom_attribute27_id     xxcp_transaction_labels.custom_attribute1_id%Type,
     custom_attribute28_id     xxcp_transaction_labels.custom_attribute1_id%Type,
     custom_attribute29_id     xxcp_transaction_labels.custom_attribute1_id%Type,
     custom_attribute30_id     xxcp_transaction_labels.custom_attribute1_id%Type
     );

  TYPE gLabels_REC    IS TABLE OF gLabels_Type    INDEX BY BINARY_INTEGER;

  gLabel_Set     gLabels_REC;
  gClear_Labels  gLabels_REC;

  gOutbound_Empty xxcp_global.gOutBoundTab%Type;
  gOutboundTab    xxcp_global.gOutboundTab%Type;
  gOutbound_Cnt   pls_integer := 0;

  gFormula_Pointers_1001  xxcp_formula_gen.build_attributes%type;
  -- 03.06.24 Common Assignment Pointers
  gFormula_Pointers_1001c xxcp_formula_gen.build_attributes%type;
  gFormula_Pointers_1002  xxcp_formula_gen.build_attributes%type;

  -- *******************************************************************
  --                  Software_Version
  -- *******************************************************************
  FUNCTION Software_Version RETURN VARCHAR2 IS
  BEGIN
    RETURN('Version [03.06.34] Build Date [24-OCT-2012] Name [XXCP_TE_CONFIGURATOR]');
  END Software_Version;

  -- *******************************************************************
  --                  Set_SAE_Inbound
  -- *******************************************************************
  PROCEDURE Set_SAE_Inbound(cInboundRec in xxcp_vt_interface%rowtype) is
  BEGIN
    gInboundRec := cInboundRec;
  END Set_SAE_Inbound;

  -- *******************************************************************
  --                  Reset_Parent_Trx_id
  -- *******************************************************************
  PROCEDURE Reset_Parent_Trx_id is
  BEGIN
    gNexTrxParentTrxId := 0;
  End Reset_Parent_Trx_id;

  -- *******************************************************************
  --                  CustomEventTiming
  -- *******************************************************************
  Procedure CustomEventTiming(cEvent_Name in varchar2, cTrading_Set in varchar2 default null) is

    vTimeDiff varchar2(30);

    begin
      If cTrading_Set is not null then
        xxcp_wks.Custom_Event_Log := xxcp_wks.Custom_Event_Log || chr(10)||' Trading Set <'|| cTrading_Set ||'>'|| chr(10);
      End If;

      vTimeDiff := xxcp_foundation.FormatTimeStamp(gTraceStartTime,gTraceEndTime,'D');

      xxcp_wks.Custom_Event_Log := xxcp_wks.Custom_Event_Log || ' '||rpad(cEvent_Name,30,' ')||': Execution Time <'||vTimeDiff||'>'||Chr(10);
    End CustomEventTiming;


  -- *******************************************************************
  --                    StandardPriceAttribute
  -- *******************************************************************
  Function StandardPriceAttribute(cShared_Pricing in varchar2, cAttribute_id in number) return varchar2 is

    vSharedPriceAttribute varchar2(250);

    Cursor SPA(pAttribute_id in number) is
       select m.to_attribute_id
        from xxcp_cross_source_mapping m
        where m.source_id = xxcp_global.gCommon(1).Source_id -- this is right
          and from_attribute_id = pAttribute_id;

    Begin
      If(cShared_Pricing = 'Y' and (cAttribute_id between 1031 and 2999)) then
        For Rec in SPA(cAttribute_id) Loop
           vSharedPriceAttribute := xxcp_foundation.Find_DynamicAttribute(Rec.to_attribute_id);
        End Loop;
      Else
        -- Standard
        vSharedPriceAttribute := xxcp_foundation.Find_DynamicAttribute(cAttribute_id);
      End If;

      Return(vSharedPriceAttribute);

  End StandardPriceAttribute;
  --
  -- CommonAttribute
  --
  Function GetCommonAttribute(cAttribute_id in number) return varchar2 is

    vAttribute varchar2(250) := Null;

    Begin
      
      If cAttribute_id between 1000 and 1999 then
         vAttribute := xxcp_wks.C1001_Array(cAttribute_id-1000);
      End If;    

      Return(vAttribute);

  End GetCommonAttribute;




  -- *******************************************************************
  --                  FindAdjustmentRates
  -- *******************************************************************
  PROCEDURE FindAdjustmentRates(cRate_Set_id           IN NUMBER,
                                cQualifier1            IN VARCHAR2,
                                cQualifier2            IN VARCHAR2,
                                cQualifier3            IN VARCHAR2,
                                cQualifier4            IN VARCHAR2,
                                cTransaction_Date      IN DATE,
                                cGiven_Adjustment_Rate IN NUMBER,
                                cAdjustment_Rate_id    OUT NUMBER,
                                cAdjustment_Rate       OUT VARCHAR2,
                                cInternalErrorCode  IN OUT NUMBER) IS

    CURSOR aj(pRate_Set_id IN NUMBER, pQualifier1 IN VARCHAR2, pQualifier2 IN VARCHAR2, pQualifier3 IN VARCHAR2, pQualifier4 IN VARCHAR2, pEffective_Date IN DATE) IS
      SELECT r.Rate, r.Adjustment_Rate_id
        FROM xxcp_adjustment_rates r,
             xxcp_table_set_ctl s
       WHERE r.rate_set_id                = pRate_Set_id
         AND r.rate_set_id                = s.set_id
         AND s.set_base_table             = 'CP_ADJUSTMENT_RATES'
         AND nvl(owner, '~null~')         = pQualifier1
         AND nvl(partner, '~null~')       = pQualifier2
         AND nvl(ar_qualifier1, '~null~') = pQualifier3
         AND nvl(ar_qualifier2, '~null~') = pQualifier4
         AND (pEffective_Date BETWEEN
             nvl(r.Effective_From_Date, TO_DATE('01-JAN-1980','DD-MON-YYYY')) AND
             nvl(r.Effective_To_Date, TO_DATE('31-DEC-2999','DD-MON-YYYY')));

    vCounter          pls_integer := 0;
    vRate             xxcp_adjustment_rates.rate%Type := 0;
    vAdj_Rate_id      xxcp_adjustment_rates.adjustment_rate_id%Type:= 0;

  BEGIN

    FOR AdRec IN Aj(pRate_Set_id    => cRate_Set_id ,
                    pQualifier1     => nvl(cQualifier1, '~null~'),
                    pQualifier2     => nvl(cQualifier2, '~null~'),
                    pQualifier3     => nvl(cQualifier3, '~null~'),
                    pQualifier4     => nvl(cQualifier4, '~null~'),
                    pEffective_Date => Trunc(cTransaction_Date)
                    ) LOOP
      vCounter     := vCounter + 1;
      vRate        := AdRec.Rate;
      vAdj_Rate_id := AdRec.Adjustment_Rate_id;
    END LOOP;

    -- One Adjustment found
    IF vCounter = 1 THEN
      cAdjustment_Rate_id := vAdj_Rate_id;
      cAdjustment_Rate    := vRate;
      cInternalErrorCode  := 0;
    ElsIf cGiven_Adjustment_Rate is not null Then
        cAdjustment_Rate    := cGiven_Adjustment_Rate;
        cAdjustment_Rate_id := 0;
        cInternalErrorCode  := 0;
    Else
        -- Report error
        IF vCounter = 0 THEN
          cInternalErrorCode := 3081;
        ELSE
          cInternalErrorCode := 3082;
        END IF;
    END IF;

    xxcp_trace.ASSIGNMENT_TRACE(
                                 cTrace_id               => 23,
                                 cAdj_Rate_set_id        => cRate_Set_id,
                                 cOwner_attribute        => cQualifier1,
                                 cPartner_attribute      => cQualifier2,
                                 cADJ_Qualifier1         => cQualifier3,
                                 cADJ_Qualifier2         => cQualifier4,
                                 cTransaction_Date       => cTransaction_date,
                                 cAdjustment_Rate        => cAdjustment_Rate,
                                 cAdjustment_Rate_id     => cAdjustment_Rate_id
                                 );



  END FindAdjustmentRates;

  -- *******************************************************************
  --                  FindAdjustmentRates - OverLoaded
  -- *******************************************************************
  PROCEDURE FindAdjustmentRates(cRate_Set_id           IN NUMBER,
                                cQualifier1            IN VARCHAR2,
                                cQualifier2            IN VARCHAR2,
                                cQualifier3            IN VARCHAR2,
                                cQualifier4            IN VARCHAR2,
                                cTransaction_Date      IN DATE,
                                cAdjustment_Rate_id    OUT NUMBER,
                                cAdjustment_Rate       OUT VARCHAR2,
                                cInternalErrorCode  IN OUT NUMBER) IS
  Begin

    FindAdjustmentRates(cRate_Set_id           => cRate_Set_id,
                        cQualifier1            => cQualifier1,
                        cQualifier2            => cQualifier2,
                        cQualifier3            => cQualifier3,
                        cQualifier4            => cQualifier4,
                        cTransaction_Date      => cTransaction_Date,
                        cGiven_Adjustment_Rate => Null,
                        cAdjustment_Rate_id    => cAdjustment_Rate_id,
                        cAdjustment_Rate       => cAdjustment_Rate,
                        cInternalErrorCode     => cInternalErrorCode);

  End FindAdjustmentRates;

  --## ********************************************************************************************
  --##                     Get_Last_IC_Values
  --## ********************************************************************************************
  PROCEDURE Get_Last_IC_Values(cLastICPrice    OUT NUMBER,
                               cLastICCurrency OUT VARCHAR2,
                               cLastICPMID     OUT NUMBER,
                               cLastDftPMused  OUT VARCHAR2
                              ) is
  Begin
    -- This is used by the Real Time Wrapper
    -- to populate the vt_ic_price details
    -- It was first used for Cisco System Inc.
    cLastICPrice    := gLastICPrice;
    cLastICPMID     := gLastICPMID;
    cLastICCurrency := gLastICCurrency;
    cLastDftPMused  := gLastDftPMused;

  End Get_Last_IC_Values;

  --## ********************************************************************************************
  --##                     Clear_Last_IC_Values
  --## ********************************************************************************************
  PROCEDURE Clear_Last_IC_Values is
  Begin
    -- This is used by the Real Time Wrapper
    -- to populate the vt_ic_price details
    -- It was first used for Cisco System Inc.
    gLastICPrice    := Null;
    gLastICPMID     := Null;
    gLastICCurrency := Null;
    gLastDftPMused  := 'N';    -- ksp20052010
  End Clear_Last_IC_Values;

  -- *******************************************************************
  --                  Make_Header
  -- *******************************************************************
  PROCEDURE Make_Header(cRecordAction             IN VARCHAR2,
                        cSource_Assignment_id     IN NUMBER,
                        cSet_of_books_id          IN NUMBER,
                        cParent_trx_id            IN NUMBER,
                        cSource_Table_id          IN NUMBER,
                        cTrading_set_id           IN NUMBER,
                        cTransaction_date         IN DATE,
                        cTransaction_currency     IN VARCHAR2,
                        cTransaction_exch_rate    IN NUMBER,
                        cOwner_tax_reg_id         IN NUMBER,
                        cOwner_legal_curr         IN VARCHAR2,
                        cOwner_legal_exch_rate    IN NUMBER,
                        cOwner_Assoc_id           IN NUMBER,
                        cOwner_Assignment_rule_id IN NUMBER,
                        cExchange_date            IN DATE,
                        cTransaction_ref1         IN VARCHAR2,
                        cTransaction_ref2         IN VARCHAR2,
                        cTransaction_ref3         IN VARCHAR2,
                        cHeaderRowId              IN ROWID,
                        cInternalErrorCode        IN OUT NUMBER,
                        cHeader_ID                IN OUT NUMBER) IS

    vHeader_ID            NUMBER;
    vSetInternalErrorCode NUMBER;

  BEGIN

    -- Final Check on data.
    IF cTransaction_date IS NULL THEN
      cInternalErrorCode := 3501; -- Transaction Date is null
    ELSIF cTransaction_currency IS NULL THEN
      cInternalErrorCode := 3504; -- Transaction Currency is null
    ELSIF cExchange_Date IS NULL THEN
      cInternalErrorCode := 3505; -- Exchange Date is null
    END IF;

    vSetInternalErrorCode := cInternalErrorCode;

    IF cInternalErrorCode <> 0 THEN
      IF cInternalErrorCode BETWEEN 3500 AND 3599 THEN
        vSetInternalErrorCode := cInternalErrorCode;
      ELSE
        vSetInternalErrorCode := NULL;
      END IF;
    ELSE
      vSetInternalErrorCode := 0;
    END IF;

    vHeader_ID := cHeader_ID;

    IF (cRecordAction = 'I' AND xxcp_global.Preview_on = 'N') THEN
      BEGIN

        if nvl(xxcp_global.forecast_on,'N') = 'Y' or nvl(xxcp_global.true_up_on,'N') = 'Y' or nvl(xxcp_global.replan_on,'N') = 'Y' then

          INSERT INTO XXCP_FC_TRANSACTION_HEADER
            (FORECAST_ID,
             CPA_TYPE,
             COST_PLUS_SET_ID,
             HEADER_ID,
             SET_OF_BOOKS_ID,
             PARENT_TRX_ID,
             SOURCE_TABLE_ID,
             TRADING_SET_ID,
             TRANSACTION_DATE,
             TRANSACTION_CURRENCY,
             TRANSACTION_EXCH_RATE,
             OWNER_TAX_REG_ID,
             --
             OWNER_LEGAL_CURR,
             OWNER_LEGAL_EXCH_RATE,
             --
             OWNER_ASSOC_ID,
             FROZEN,
             EXCHANGE_DATE,
             INTERNAL_ERROR_CODE,
             TRANSACTION_REF1,
             TRANSACTION_REF2,
             TRANSACTION_REF3,
             Source_Assignment_ID,
             OWNER_ASSIGN_RULE_ID,
             CREATED_BY,
             CREATION_DATE,
             LAST_UPDATE_LOGIN)
          VALUES
            (xxcp_global.gCommon(1).Forecast_ID,
             xxcp_global.gCommon(1).Cpa_Type,
             xxcp_wks.gActual_Costs_Rec(1).cost_plus_set_id,
             xxcp_header_id_seq.NEXTVAL,
             cSet_of_books_id,
             cParent_Trx_Id,
             cSource_Table_id,
             cTRADING_SET_ID,
             cTRANSACTION_DATE,
             cTransaction_Currency,
             cTransaction_Exch_Rate,
             cOwner_TAX_REG_ID,
             --
             cOwner_LEGAL_CURR,
             cOwner_LEGAL_EXCH_RATE,
             --
             cOwner_ASSOC_ID,
             DECODE(NVL(vSetInternalErrorCode, 0), 0, 'Y', 'N'),
             cExchange_Date,
             DECODE(vSetInternalErrorCode, 0, NULL, vSetInternalErrorCode),
             cTransaction_Ref1,
             cTransaction_Ref2,
             cTransaction_Ref3,
             cSource_Assignment_id,
             cOwner_Assignment_Rule_id,
             xxcp_global.user_id,
             xxcp_global.Systemdate,
             xxcp_global.Login_id) RETURNING Header_id INTO vHeader_ID;
        else
          INSERT INTO XXCP_TRANSACTION_HEADER
            (HEADER_ID,
             SET_OF_BOOKS_ID,
             PARENT_TRX_ID,
             SOURCE_TABLE_ID,
             TRADING_SET_ID,
             TRANSACTION_DATE,
             TRANSACTION_CURRENCY,
             TRANSACTION_EXCH_RATE,
             OWNER_TAX_REG_ID,
             --
             OWNER_LEGAL_CURR,
             OWNER_LEGAL_EXCH_RATE,
             --
             OWNER_ASSOC_ID,
             FROZEN,
             EXCHANGE_DATE,
             INTERNAL_ERROR_CODE,
             TRANSACTION_REF1,
             TRANSACTION_REF2,
             TRANSACTION_REF3,
             Source_Assignment_ID,
             OWNER_ASSIGN_RULE_ID,
             CREATED_BY,
             CREATION_DATE,
             LAST_UPDATE_LOGIN)
          VALUES
            (xxcp_header_id_seq.NEXTVAL,
             cSet_of_books_id,
             cParent_Trx_Id,
             cSource_Table_id,
             cTRADING_SET_ID,
             cTRANSACTION_DATE,
             cTransaction_Currency,
             cTransaction_Exch_Rate,
             cOwner_TAX_REG_ID,
             --
             cOwner_LEGAL_CURR,
             cOwner_LEGAL_EXCH_RATE,
             --
             cOwner_ASSOC_ID,
             DECODE(NVL(vSetInternalErrorCode, 0), 0, 'Y', 'N'),
             cExchange_Date,
             DECODE(vSetInternalErrorCode, 0, NULL, vSetInternalErrorCode),
             cTransaction_Ref1,
             cTransaction_Ref2,
             cTransaction_Ref3,
             cSource_Assignment_id,
             cOwner_Assignment_Rule_id,
             xxcp_global.user_id,
             xxcp_global.Systemdate,
             xxcp_global.Login_id) RETURNING Header_id INTO vHeader_ID;
        end if;
      EXCEPTION
        WHEN OTHERS THEN
           cInternalErrorCode := 3550;
           xxcp_foundation.FndWriteError(cInternalErrorCode,SQLERRM);

      END;
    ELSIF (cRecordAction = 'U' AND xxcp_global.Preview_on = 'N') THEN
      BEGIN
        UPDATE xxcp_transaction_header
           SET HEADER_ID             = vHeader_ID,
               SET_OF_BOOKS_ID       = cSet_of_books_id,
               PARENT_TRX_ID         = cPARENT_TRX_ID,
               TRANSACTION_DATE      = cTRANSACTION_DATE,
               TRANSACTION_CURRENCY  = cTransaction_Currency,
               TRANSACTION_EXCH_RATE = cTransaction_Exch_Rate,
               OWNER_TAX_REG_ID      = cOwner_TAX_REG_ID,
               OWNER_LEGAL_CURR      = cOwner_LEGAL_CURR, -- Owner Legal
               OWNER_LEGAL_EXCH_RATE = cOwner_LEGAL_EXCH_RATE,
               OWNER_ASSOC_ID        = cOwner_ASSOC_ID,
               OWNER_ASSIGN_RULE_ID  = cOwner_Assignment_Rule_id,
               EXCHANGE_DATE         = cExchange_Date,
               FROZEN                = DECODE(NVL(vSetInternalErrorCode, 0),
                                              0,'Y','N'),
               INTERNAL_ERROR_CODE   = DECODE(vSetInternalErrorCode,
                                              0,NULL,vSetInternalErrorCode),
               TRANSACTION_REF1      = cTransaction_Ref1,
               TRANSACTION_REF2      = cTransaction_Ref2,
               TRANSACTION_REF3      = cTransaction_Ref3,
               SOURCE_ASSIGNMENT_ID  = cSource_Assignment_id,
               LAST_UPDATED_BY       = xxcp_global.User_id,
               LAST_UPDATE_DATE      = xxcp_global.SystemDate,
               LAST_UPDATE_LOGIN     = xxcp_global.Login_id
         WHERE ROWID = cHeaderRowid;

      EXCEPTION
        WHEN OTHERS THEN
           cInternalErrorCode := 3550;
           xxcp_foundation.FndWriteError(cInternalErrorCode,SQLERRM);
      END;
    ELSIF (cRecordAction = 'I' AND xxcp_global.Preview_on = 'Y') THEN
      BEGIN

        INSERT INTO xxcp_pv_transaction_header
          (preview_Id,
           HEADER_ID,
           SET_OF_BOOKS_ID,
           PARENT_TRX_ID,
           SOURCE_TABLE_ID,
           TRADING_SET_ID,
           TRANSACTION_DATE,
           TRANSACTION_CURRENCY,
           TRANSACTION_EXCH_RATE,
           OWNER_TAX_REG_ID,
           --
           OWNER_LEGAL_CURR,
           OWNER_LEGAL_EXCH_RATE,
           --
           OWNER_ASSOC_ID,
           FROZEN,
           EXCHANGE_DATE,
           INTERNAL_ERROR_CODE,
           TRANSACTION_REF1,
           TRANSACTION_REF2,
           TRANSACTION_REF3,
           Source_Assignment_ID,
           OWNER_ASSIGN_RULE_ID,
           LAST_UPDATED_BY,
           LAST_UPDATE_DATE,
           LAST_UPDATE_LOGIN)
        VALUES
          (xxcp_global.gCommon(1).Preview_id,
           xxcp_PREVIEW_ATT_SEQ.NEXTVAL,
           cSet_of_books_id,
           cParent_Trx_Id,
           cSource_Table_id,
           cTrading_Set_ID,
           cTransaction_Date,
           cTransaction_Currency,
           cTransaction_Exch_Rate,
           cOwner_Tax_Reg_ID,
           --
           cOwner_Legal_Curr,
           cOwner_Legal_Exch_Rate,
           --
           cOwner_Assoc_ID,
           Decode(NVL(vSetInternalErrorCode, 0), 0, 'Y', 'N'),
           cExchange_Date,
           Decode(vSetInternalErrorCode, 0, NULL, vSetInternalErrorCode),
           cTransaction_Ref1,
           cTransaction_Ref2,
           cTransaction_Ref3,
           cSource_Assignment_id,
           cOwner_Assignment_Rule_id,
           xxcp_global.User_id,
           xxcp_global.SystemDate,
           xxcp_global.Login_id)
        RETURNING HEADER_ID INTO vHeader_ID;

      EXCEPTION
        WHEN OTHERS THEN
           cInternalErrorCode := 3550;
           xxcp_foundation.FndWriteError(cInternalErrorCode,SQLERRM);
      END;
    ELSIF (cRecordAction = 'U' AND xxcp_global.Preview_on = 'Y') THEN
      BEGIN
        UPDATE xxcp_pv_transaction_header
           SET preview_Id            = xxcp_global.gCommon(1).Preview_id,
               HEADER_ID             = vHeader_ID,
               SET_OF_BOOKS_ID       = cSet_of_books_id,
               PARENT_TRX_ID         = cPARENT_TRX_ID,
               TRANSACTION_DATE      = cTRANSACTION_DATE,
               TRANSACTION_CURRENCY  = cTransaction_Currency,
               TRANSACTION_EXCH_RATE = cTransaction_Exch_Rate,
               OWNER_TAX_REG_ID      = cOwner_TAX_REG_ID,
               OWNER_LEGAL_CURR      = cOwner_LEGAL_CURR, -- Owner Legal
               OWNER_LEGAL_EXCH_RATE = cOwner_LEGAL_EXCH_RATE,
               OWNER_ASSOC_ID        = cOwner_ASSOC_ID,
               OWNER_ASSIGN_RULE_ID  = cOwner_Assignment_Rule_id,
               EXCHANGE_DATE         = cExchange_Date,
               FROZEN                = DECODE(NVL(vSetInternalErrorCode, 0),0,'Y','N'),
               INTERNAL_ERROR_CODE   = DECODE(vSetInternalErrorCode,0,NULL,vSetInternalErrorCode),
               TRANSACTION_REF1      = cTransaction_Ref1,
               TRANSACTION_REF2      = cTransaction_Ref2,
               TRANSACTION_REF3      = cTransaction_Ref3,
               Source_Assignment_id  = cSource_Assignment_id,
               LAST_UPDATED_BY       = xxcp_global.User_id,
               LAST_UPDATE_DATE      = xxcp_global.Systemdate,
               LAST_UPDATE_LOGIN     = xxcp_global.Login_id
         WHERE ROWID = cHeaderRowid;

      EXCEPTION
        WHEN OTHERS THEN
           cInternalErrorCode := 3550;
           xxcp_foundation.FndWriteError(cInternalErrorCode,SQLERRM);
      END;
    END IF; -- End UPDATE Preview


    xxcp_trace.ASSIGNMENT_TRACE(cTrace_id => 8, cHeader_id => vHeader_id, cRecordAction => cRecordAction);

    cHeader_ID := vHeader_ID;

  END Make_Header;
 

  -- *******************************************************************
  --                  Make_Transaction_Cache
  -- *******************************************************************
  PROCEDURE Make_Transaction_Cache(cAttribute_id      IN NUMBER,
                                   cRecordAction      IN VARCHAR2,
                                   cTransaction_Date  IN DATE,
                                   cD1000_Pointers    IN VARCHAR2,
                                   cD1002_Pointers    IN VARCHAR2,
                                   cInternalErrorCode IN OUT NUMBER) IS

   -- Constants
    vMaxFields    INTEGER := 50;
    vMaxCacheRows INTEGER := 5;

    -- 02.05.07
    vCA_Value xxcp_DYNAMIC_ARRAY_long := xxcp_DYNAMIC_ARRAY_long(' ');

    r INTEGER := 0;
    p INTEGER := 0;
    f INTEGER := 0;
    k INTEGER := 0;

    vWorkPointers VARCHAR2(2500);

    vSeq          INTEGER := 0;
    vCached_QTY   INTEGER := 0;

    -- find out which attributes to not put into cache.  
    Cursor CacheVar(pAttribute_id in number) is
     select w.none_cache include_in_cache
     from xxcp_dynamic_attributes w
     where w.source_id in (0,xxcp_global.gCommon(1).Source_id)
       and w.column_id = pAttribute_id
       and (nvl(w.none_cache,'N') = 'N' or nvl(xxcp_global.gCommon(1).preview_none_cache_flag,'N') = 'Y');
       
    --
    -- Write Cache
    --
    Procedure Write_Cache(cATTRIBUTE_ID      IN NUMBER,
                          cSEQ               IN NUMBER,
                          cTransaction_Date  IN DATE,
                          cCACHED_QTY        IN NUMBER,
                          cInternalErrorCode IN OUT NUMBER) is

      Begin

       IF xxcp_global.Preview_on = 'N' THEN
          -- Insert new record
          if nvl(xxcp_global.forecast_on,'N') = 'Y' or nvl(xxcp_global.true_up_on,'N') = 'Y' then
            INSERT INTO XXCP_FC_TRANSACTION_CACHE
              (FORECAST_ID,
               CPA_TYPE,
               COST_PLUS_SET_ID,
               ATTRIBUTE_ID,
               SEQ,
               TRANSACTION_DATE,
               CACHED_QTY,
               CACHED_VALUE1,CACHED_VALUE2,CACHED_VALUE3,CACHED_VALUE4,CACHED_VALUE5,
               CACHED_VALUE6,CACHED_VALUE7,CACHED_VALUE8,CACHED_VALUE9,CACHED_VALUE10,
               CACHED_VALUE11,CACHED_VALUE12,CACHED_VALUE13,CACHED_VALUE14,CACHED_VALUE15,
               CACHED_VALUE16,CACHED_VALUE17,CACHED_VALUE18,CACHED_VALUE19,CACHED_VALUE20,
               CACHED_VALUE21,CACHED_VALUE22,CACHED_VALUE23,CACHED_VALUE24,CACHED_VALUE25,
               CACHED_VALUE26,CACHED_VALUE27,CACHED_VALUE28,CACHED_VALUE29,CACHED_VALUE30,
               CACHED_VALUE31,CACHED_VALUE32,CACHED_VALUE33,CACHED_VALUE34,CACHED_VALUE35,
               CACHED_VALUE36,CACHED_VALUE37,CACHED_VALUE38,CACHED_VALUE39,CACHED_VALUE40,
               CACHED_VALUE41,CACHED_VALUE42,CACHED_VALUE43,CACHED_VALUE44,CACHED_VALUE45,
               CACHED_VALUE46,CACHED_VALUE47,CACHED_VALUE48,CACHED_VALUE49,CACHED_VALUE50)
            VALUES
              (xxcp_global.gCommon(1).Forecast_ID,
               xxcp_global.gCommon(1).CPA_Type,
               xxcp_wks.gActual_Costs_Rec(1).cost_plus_set_id,
               cAttribute_id,
               cSEQ,
               cTransaction_Date,
               cCACHED_QTY,
               vCA_VALUE(1), vCA_VALUE(2),vCA_VALUE(3),vCA_VALUE(4),vCA_VALUE(5),
               vCA_VALUE(6), vCA_VALUE(7),vCA_VALUE(8),vCA_VALUE(9),vCA_VALUE(10),
               vCA_VALUE(11),vCA_VALUE(12),vCA_VALUE(13),vCA_VALUE(14),vCA_VALUE(15),
               vCA_VALUE(16),vCA_VALUE(17),vCA_VALUE(18),vCA_VALUE(19),vCA_VALUE(20),
               vCA_VALUE(21),vCA_VALUE(22),vCA_VALUE(23),vCA_VALUE(24),vCA_VALUE(25),
               vCA_VALUE(26),vCA_VALUE(27),vCA_VALUE(28),vCA_VALUE(29),vCA_VALUE(30),
               vCA_VALUE(31),vCA_VALUE(32),vCA_VALUE(33),vCA_VALUE(34),vCA_VALUE(35),
               vCA_VALUE(36),vCA_VALUE(37),vCA_VALUE(38),vCA_VALUE(39),vCA_VALUE(40),
               vCA_VALUE(41),vCA_VALUE(42),vCA_VALUE(43),vCA_VALUE(44),vCA_VALUE(45),
               vCA_VALUE(46),vCA_VALUE(47),vCA_VALUE(48),vCA_VALUE(49),vCA_VALUE(50));
          else
            INSERT INTO XXCP_TRANSACTION_CACHE
              (ATTRIBUTE_ID,
               SEQ,
               TRANSACTION_DATE,
               CACHED_QTY,
               CACHED_VALUE1,CACHED_VALUE2,CACHED_VALUE3,CACHED_VALUE4,CACHED_VALUE5,
               CACHED_VALUE6,CACHED_VALUE7,CACHED_VALUE8,CACHED_VALUE9,CACHED_VALUE10,
               CACHED_VALUE11,CACHED_VALUE12,CACHED_VALUE13,CACHED_VALUE14,CACHED_VALUE15,
               CACHED_VALUE16,CACHED_VALUE17,CACHED_VALUE18,CACHED_VALUE19,CACHED_VALUE20,
               CACHED_VALUE21,CACHED_VALUE22,CACHED_VALUE23,CACHED_VALUE24,CACHED_VALUE25,
               CACHED_VALUE26,CACHED_VALUE27,CACHED_VALUE28,CACHED_VALUE29,CACHED_VALUE30,
               CACHED_VALUE31,CACHED_VALUE32,CACHED_VALUE33,CACHED_VALUE34,CACHED_VALUE35,
               CACHED_VALUE36,CACHED_VALUE37,CACHED_VALUE38,CACHED_VALUE39,CACHED_VALUE40,
               CACHED_VALUE41,CACHED_VALUE42,CACHED_VALUE43,CACHED_VALUE44,CACHED_VALUE45,
               CACHED_VALUE46,CACHED_VALUE47,CACHED_VALUE48,CACHED_VALUE49,CACHED_VALUE50)
            VALUES
              (cAttribute_id,
               cSEQ,
               cTransaction_Date,
               cCACHED_QTY,
               vCA_VALUE(1), vCA_VALUE(2),vCA_VALUE(3),vCA_VALUE(4),vCA_VALUE(5),
               vCA_VALUE(6), vCA_VALUE(7),vCA_VALUE(8),vCA_VALUE(9),vCA_VALUE(10),
               vCA_VALUE(11),vCA_VALUE(12),vCA_VALUE(13),vCA_VALUE(14),vCA_VALUE(15),
               vCA_VALUE(16),vCA_VALUE(17),vCA_VALUE(18),vCA_VALUE(19),vCA_VALUE(20),
               vCA_VALUE(21),vCA_VALUE(22),vCA_VALUE(23),vCA_VALUE(24),vCA_VALUE(25),
               vCA_VALUE(26),vCA_VALUE(27),vCA_VALUE(28),vCA_VALUE(29),vCA_VALUE(30),
               vCA_VALUE(31),vCA_VALUE(32),vCA_VALUE(33),vCA_VALUE(34),vCA_VALUE(35),
               vCA_VALUE(36),vCA_VALUE(37),vCA_VALUE(38),vCA_VALUE(39),vCA_VALUE(40),
               vCA_VALUE(41),vCA_VALUE(42),vCA_VALUE(43),vCA_VALUE(44),vCA_VALUE(45),
               vCA_VALUE(46),vCA_VALUE(47),vCA_VALUE(48),vCA_VALUE(49),vCA_VALUE(50));
          end if;
        ELSE
          -- Insert new record
          INSERT INTO XXCP_PV_TRANSACTION_CACHE
            (PREVIEW_ID,
             ATTRIBUTE_ID,
             SEQ,
             TRANSACTION_DATE,
             CACHED_QTY,
             CACHED_VALUE1,CACHED_VALUE2,CACHED_VALUE3,CACHED_VALUE4,CACHED_VALUE5,
             CACHED_VALUE6,CACHED_VALUE7,CACHED_VALUE8,CACHED_VALUE9,CACHED_VALUE10,
             CACHED_VALUE11,CACHED_VALUE12,CACHED_VALUE13,CACHED_VALUE14,CACHED_VALUE15,
             CACHED_VALUE16,CACHED_VALUE17,CACHED_VALUE18,CACHED_VALUE19,CACHED_VALUE20,
             CACHED_VALUE21,CACHED_VALUE22,CACHED_VALUE23,CACHED_VALUE24,CACHED_VALUE25,
             CACHED_VALUE26,CACHED_VALUE27,CACHED_VALUE28,CACHED_VALUE29,CACHED_VALUE30,
             CACHED_VALUE31,CACHED_VALUE32,CACHED_VALUE33,CACHED_VALUE34,CACHED_VALUE35,
             CACHED_VALUE36,CACHED_VALUE37,CACHED_VALUE38,CACHED_VALUE39,CACHED_VALUE40,
             CACHED_VALUE41,CACHED_VALUE42,CACHED_VALUE43,CACHED_VALUE44,CACHED_VALUE45,
             CACHED_VALUE46,CACHED_VALUE47,CACHED_VALUE48,CACHED_VALUE49,CACHED_VALUE50)
          VALUES
              (xxcp_global.gCommon(1).Preview_id,
               cAttribute_id,
               cSEQ,
               cTransaction_Date,
               cCACHED_QTY,
               vCA_VALUE(1), vCA_VALUE(2),vCA_VALUE(3),vCA_VALUE(4),vCA_VALUE(5),
               vCA_VALUE(6), vCA_VALUE(7),vCA_VALUE(8),vCA_VALUE(9),vCA_VALUE(10),
               vCA_VALUE(11),vCA_VALUE(12),vCA_VALUE(13),vCA_VALUE(14),vCA_VALUE(15),
               vCA_VALUE(16),vCA_VALUE(17),vCA_VALUE(18),vCA_VALUE(19),vCA_VALUE(20),
               vCA_VALUE(21),vCA_VALUE(22),vCA_VALUE(23),vCA_VALUE(24),vCA_VALUE(25),
               vCA_VALUE(26),vCA_VALUE(27),vCA_VALUE(28),vCA_VALUE(29),vCA_VALUE(30),
               vCA_VALUE(31),vCA_VALUE(32),vCA_VALUE(33),vCA_VALUE(34),vCA_VALUE(35),
               vCA_VALUE(36),vCA_VALUE(37),vCA_VALUE(38),vCA_VALUE(39),vCA_VALUE(40),
               vCA_VALUE(41),vCA_VALUE(42),vCA_VALUE(43),vCA_VALUE(44),vCA_VALUE(45),
               vCA_VALUE(46),vCA_VALUE(47),vCA_VALUE(48),vCA_VALUE(49),vCA_VALUE(50));
        END IF;

        Exception when OTHERS then
           cInternalErrorCode := 3594; -- New error code

      End Write_Cache;

  BEGIN

    IF xxcp_global.gCommon(1).Cached_Attributes <> 'N' THEN

      xxcp_trace.ASSIGNMENT_TRACE(cTrace_id => 9);

      -- 03.06.24 Write Common Assignment Formula Set Values
--      vWorkPointers := cD1000_Pointers || cD1002_Pointers || gFormula_Pointers_1001 || gFormula_Pointers_1001c || gFormula_Pointers_1002;

      vWorkPointers := Null;
      For jk in 31..xxcp_wks.Max_Configuration loop
        If jk = 31 then -- Release date
          If xxcp_wks.D1001_Array(17) is not null then
            vWorkPointers := vWorkPointers || (1000+17);
          End If;
        End If;  
        If xxcp_wks.D1001_Array(jk) is not null then
          vWorkPointers := vWorkPointers || (1000+jk);
        End If;  
      End Loop;
      -- IC Pricing
      For jk in 1..xxcp_wks.Max_IC_Pricing loop
        If xxcp_wks.D1002_Array(jk) is not null then
          vWorkPointers := vWorkPointers || (2000+jk);
        End If;  
      End Loop;

      IF LENGTH(vWorkPointers) >= 3 THEN
        -- Remove existing record
        IF cRecordAction = 'U' and xxcp_global.Preview_on = 'N' THEN
          BEGIN
            DELETE FROM xxcp_transaction_cache
             WHERE attribute_id = cAttribute_id;
          EXCEPTION
            WHEN OTHERS THEN
              NULL;
          END;
        END IF;

        -- Create Transaction Cache
        f := LENGTH(vWorkPointers) /4;

        vCA_value.EXTEND(vMaxFields);

        FOR r IN 1 .. f LOOP

          p := TO_NUMBER(SUBSTR(vWorkPointers, 1 + k, 4));

          IF p IS NULL OR vCached_QTY >= vMaxFields THEN
            EXIT;
          END IF;
          
          --
          -- If none_cache = Y, do not put in transaction cache table
          --
          
          For ncRec in CacheVar(pAttribute_id => p ) Loop

              IF p BETWEEN 1031 AND 1999 THEN
                IF xxcp_wks.D1001_Array(p - 1000) IS NOT NULL THEN
                  vCached_QTY := vCached_QTY + 1;
                  vCA_Value(vCached_QTY) := TO_CHAR(p) || xxcp_wks.D1001_Array(p - 1000);
                END IF;
              ELSIF p = 1017 THEN
                IF xxcp_wks.D1001_Array(p - 1000) IS NOT NULL THEN
                  vCached_QTY := vCached_QTY + 1;
                  vCA_Value(vCached_QTY) := TO_CHAR(p) || xxcp_wks.D1001_Array(p - 1000);
                END IF;
              ELSIF p BETWEEN 2000 AND 2999 THEN
                IF xxcp_wks.D1002_Array(p - 2000) IS NOT NULL THEN
                  vCached_QTY := vCached_QTY + 1;
                  vCA_Value(vCached_QTY) := TO_CHAR(p) || xxcp_wks.D1002_Array(p - 2000);
                END IF;
              END IF;

          End Loop;

          k := k + 4;

          If ((vCached_QTY >= vMaxFields) and (vSEQ < vMaxCacheRows)) then
            vSEQ := vSEQ + 1;

            Write_Cache(cAttribute_id      => cAttribute_id,
                        cSEQ               => vSEQ,
                        cTransaction_Date  => cTransaction_Date,
                        cInternalErrorCode => cInternalErrorCode,
                        cCached_Qty        => vCached_QTY
                        );

            vCA_value.DELETE;
            vCA_value.EXTEND(vMaxFields);

            vCached_QTY := 0;
          End If;

        END LOOP;

        -- Final cache write
        If ((vCached_QTY > 0) and (vSEQ < vMaxCacheRows)) then
            vSEQ := vSEQ + 1;
            Write_Cache(cAttribute_id      => cAttribute_id,
                        cSEQ               => vSEQ,
                        cTransaction_Date  => cTransaction_Date,
                        cInternalErrorCode => cInternalErrorCode,
                        cCached_Qty        => vCached_QTY
                        );
        End If;

      END IF;
    END IF;
  End Make_Transaction_Cache;

  -- *******************************************************************
  --                        Set_Attribute_DFF
  -- *******************************************************************
  Procedure Set_Attribute_DFF(cSource_Type_id in number) is

    CURSOR FillAtt(pSource_Type_id in number) is
      select l.custom_attribute1_id,
             l.custom_attribute2_id,
             l.custom_attribute3_id,
             l.custom_attribute4_id,
             l.custom_attribute5_id,
             l.custom_attribute6_id,
             l.custom_attribute7_id,
             l.custom_attribute8_id,
             l.custom_attribute9_id,
             l.custom_attribute10_id,
             l.custom_attribute11_id,
             l.custom_attribute12_id,
             l.custom_attribute13_id,
             l.custom_attribute14_id,
             l.custom_attribute15_id,
             l.custom_attribute16_id,
             l.custom_attribute17_id,
             l.custom_attribute18_id,
             l.custom_attribute19_id,
             l.custom_attribute20_id,
             l.custom_attribute21_id,
             l.custom_attribute22_id,
             l.custom_attribute23_id,
             l.custom_attribute24_id,
             l.custom_attribute25_id,
             l.custom_attribute26_id,
             l.custom_attribute27_id,
             l.custom_attribute28_id,
             l.custom_attribute29_id,
             l.custom_attribute30_id
        from xxcp_transaction_labels l
       where l.source_type_id = pSource_Type_id;

       -- Populate variable
       Function Get_DFF_Value(cExisting_Value in varchar2, cCustom_Attribute1_Id in number) return varchar2 is
         vDFF xxcp_transaction_attributes.attribute1%Type;
         Begin
            If nvl(cCustom_Attribute1_Id,0) > 0 then
              vDFF := StandardPriceAttribute('N', cCustom_Attribute1_Id);
            ElsIf cExisting_Value is not null then
              vDFF := cExisting_Value; -- do not override existing values
            End If;
            Return(vDFF);
         End Get_Dff_Value;

   Begin

    -- Only Read records when Transaction Type has changed.

    If cSource_Type_id != gLast_Source_Type_id then
      gLast_Source_Type_id := cSource_Type_id;
      gLabel_Set           := gClear_Labels;
      For FilRec in FillAtt(cSource_Type_id) Loop

        gLabel_Set(1).custom_attribute1_id   := FilRec.Custom_Attribute1_Id;
        gLabel_Set(1).custom_attribute2_id   := FilRec.Custom_Attribute2_Id;
        gLabel_Set(1).custom_attribute3_id   := FilRec.Custom_Attribute3_Id;
        gLabel_Set(1).custom_attribute4_id   := FilRec.Custom_Attribute4_Id;
        gLabel_Set(1).custom_attribute5_id   := FilRec.Custom_Attribute5_Id;
        gLabel_Set(1).custom_attribute6_id   := FilRec.Custom_Attribute6_Id;
        gLabel_Set(1).custom_attribute7_id   := FilRec.Custom_Attribute7_Id;
        gLabel_Set(1).custom_attribute8_id   := FilRec.Custom_Attribute8_Id;
        gLabel_Set(1).custom_attribute9_id   := FilRec.Custom_Attribute9_Id;
        gLabel_Set(1).custom_attribute10_id  := FilRec.Custom_Attribute10_Id;
        -- new
        gLabel_Set(1).custom_attribute11_id  := FilRec.Custom_Attribute11_Id;
        gLabel_Set(1).custom_attribute12_id  := FilRec.Custom_Attribute12_Id;
        gLabel_Set(1).custom_attribute13_id  := FilRec.Custom_Attribute13_Id;
        gLabel_Set(1).custom_attribute14_id  := FilRec.Custom_Attribute14_Id;
        gLabel_Set(1).custom_attribute15_id  := FilRec.Custom_Attribute15_Id;
        gLabel_Set(1).custom_attribute16_id  := FilRec.Custom_Attribute16_Id;
        gLabel_Set(1).custom_attribute17_id  := FilRec.Custom_Attribute17_Id;
        gLabel_Set(1).custom_attribute18_id  := FilRec.Custom_Attribute18_Id;
        gLabel_Set(1).custom_attribute19_id  := FilRec.Custom_Attribute19_Id;
        gLabel_Set(1).custom_attribute20_id  := FilRec.Custom_Attribute20_Id;
        gLabel_Set(1).custom_attribute21_id  := FilRec.Custom_Attribute21_Id;
        gLabel_Set(1).custom_attribute22_id  := FilRec.Custom_Attribute22_Id;
        gLabel_Set(1).custom_attribute23_id  := FilRec.Custom_Attribute23_Id;
        gLabel_Set(1).custom_attribute24_id  := FilRec.Custom_Attribute24_Id;
        gLabel_Set(1).custom_attribute25_id  := FilRec.Custom_Attribute25_Id;
        gLabel_Set(1).custom_attribute26_id  := FilRec.Custom_Attribute26_Id;
        gLabel_Set(1).custom_attribute27_id  := FilRec.Custom_Attribute27_Id;
        gLabel_Set(1).custom_attribute28_id  := FilRec.Custom_Attribute28_Id;
        gLabel_Set(1).custom_attribute29_id  := FilRec.Custom_Attribute29_Id;
        gLabel_Set(1).custom_attribute30_id  := FilRec.Custom_Attribute30_Id;
      End Loop;
    End If;

    xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE1  := Get_DFF_Value(xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE1,gLabel_Set(1).Custom_Attribute1_Id);
    xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE2  := Get_DFF_Value(xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE2,gLabel_Set(1).Custom_Attribute2_Id);
    xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE3  := Get_DFF_Value(xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE3,gLabel_Set(1).Custom_Attribute3_Id);
    xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE4  := Get_DFF_Value(xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE4,gLabel_Set(1).Custom_Attribute4_Id);
    xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE5  := Get_DFF_Value(xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE5,gLabel_Set(1).Custom_Attribute5_Id);
    xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE6  := Get_DFF_Value(xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE6,gLabel_Set(1).Custom_Attribute6_Id);
    xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE7  := Get_DFF_Value(xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE7,gLabel_Set(1).Custom_Attribute7_Id);
    xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE8  := Get_DFF_Value(xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE8,gLabel_Set(1).Custom_Attribute8_Id);
    xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE9  := Get_DFF_Value(xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE9,gLabel_Set(1).Custom_Attribute9_Id);
    xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE10 := Get_DFF_Value(xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE10,gLabel_Set(1).Custom_Attribute10_Id);
    --
    xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE11  := Get_DFF_Value(xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE11,gLabel_Set(1).Custom_Attribute11_Id);
    xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE12  := Get_DFF_Value(xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE12,gLabel_Set(1).Custom_Attribute12_Id);
    xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE13  := Get_DFF_Value(xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE13,gLabel_Set(1).Custom_Attribute13_Id);
    xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE14  := Get_DFF_Value(xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE14,gLabel_Set(1).Custom_Attribute14_Id);
    xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE15  := Get_DFF_Value(xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE15,gLabel_Set(1).Custom_Attribute15_Id);
    xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE16  := Get_DFF_Value(xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE16,gLabel_Set(1).Custom_Attribute16_Id);
    xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE17  := Get_DFF_Value(xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE17,gLabel_Set(1).Custom_Attribute17_Id);
    xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE18  := Get_DFF_Value(xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE18,gLabel_Set(1).Custom_Attribute18_Id);
    xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE19  := Get_DFF_Value(xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE19,gLabel_Set(1).Custom_Attribute19_Id);
    xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE20  := Get_DFF_Value(xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE20,gLabel_Set(1).Custom_Attribute20_Id);
    xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE21  := Get_DFF_Value(xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE21,gLabel_Set(1).Custom_Attribute21_Id);
    xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE22  := Get_DFF_Value(xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE22,gLabel_Set(1).Custom_Attribute22_Id);
    xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE23  := Get_DFF_Value(xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE23,gLabel_Set(1).Custom_Attribute23_Id);
    xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE24  := Get_DFF_Value(xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE24,gLabel_Set(1).Custom_Attribute24_Id);
    xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE25  := Get_DFF_Value(xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE25,gLabel_Set(1).Custom_Attribute25_Id);
    xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE26  := Get_DFF_Value(xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE26,gLabel_Set(1).Custom_Attribute26_Id);
    xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE27  := Get_DFF_Value(xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE27,gLabel_Set(1).Custom_Attribute27_Id);
    xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE28  := Get_DFF_Value(xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE28,gLabel_Set(1).Custom_Attribute28_Id);
    xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE29  := Get_DFF_Value(xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE29,gLabel_Set(1).Custom_Attribute29_Id);
    xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE30  := Get_DFF_Value(xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE30,gLabel_Set(1).Custom_Attribute30_Id);
  End Set_Attribute_DFF;

  -- *********************************************************************************
  --                                     MAKE_ATTRIBUTE
  -- *********************************************************************************
  FUNCTION Make_Attributes(cAttribute_id               IN NUMBER,
                           cRecordAction               IN VARCHAR2,
                           cSource_Assignment_id       IN NUMBER,
                           cSET_OF_BOOKS_ID            IN NUMBER,
                           cPARENT_TRX_ID              IN NUMBER,
                           cINTERFACE_ID               IN NUMBER,
                           cQUANTITY                   IN NUMBER,
                           cUOM                        IN VARCHAR2,
                           cTRANSACTION_ID             IN NUMBER,
                           cSOURCE_TABLE_ID            IN NUMBER,
                           cSOURCE_TYPE_ID             IN NUMBER,
                           cSOURCE_CLASS_ID            IN NUMBER,
                           cTRADING_SET_ID             IN NUMBER,
                           cTRANSACTION_DATE           IN DATE,
                           --
                           cPARTNER_ASSOC_ID           IN NUMBER,
                           cPARTNER_ASSIGNMENT_RULE_ID IN NUMBER,
                           --
                           cPARTNER_TAX_REG_ID         IN NUMBER,
                           cPARTNER_LEGAL_CURR         IN VARCHAR2,
                           cPARTNER_LEGAL_EXCH_RATE    IN NUMBER,
                           cEXCHANGE_DATE              IN DATE,
                           -- Adjustment Qualifiers
                           cADJ_QUALIFIER3             IN VARCHAR2,
                           cADJ_QUALIFIER4             IN VARCHAR2,
                           -- Configuration Control
                           cIC_CONTROL                 IN VARCHAR2,
                           cMC_QUALIFIER1              IN VARCHAR2,
                           cMC_QUALIFIER2              IN VARCHAR2,
                           cMC_QUALIFIER3              IN VARCHAR2,
                           cMC_QUALIFIER4              IN VARCHAR2,
                           -- Entity Documents
                           cED_QUALIFIER1              IN VARCHAR2,
                           cED_QUALIFIER2              IN VARCHAR2,
                           cED_QUALIFIER3              IN VARCHAR2,
                           cED_QUALIFIER4              IN VARCHAR2,
                           -- Tax Qualifiers
                           cTAX_QUALIFIER1             IN VARCHAR2,
                           cTAX_QUALIFIER2             IN VARCHAR2,
                           -- OLS
                           cOLS_FLAG                   IN VARCHAR2,
                           cOLS_DATE                   IN DATE,
                           -- Assignment
                           cD1000_Pointers             IN VARCHAR2,
                           -- Assignment Pricing
                           cD1002_Pointers             IN VARCHAR2,
                           --
                           cAdjustment_Rate            IN NUMBER,
                           cAdjustment_Rate_id         IN NUMBER,
                           cIC_TRADE_TAX_ID            IN NUMBER,
                           cIC_TAX_RATE                IN NUMBER,
                           cTRANSACTION_REF1           IN VARCHAR2,
                           cTRANSACTION_REF2           IN VARCHAR2,
                           cTRANSACTION_REF3           IN VARCHAR2,
                           --
                           cStep_Number                IN VARCHAR2,
                           --
                           cOwner_Tax_Reg_id           IN NUMBER,
                           --
                           cIC_Unit_Price              IN NUMBER,
                           cIC_Currency                IN VARCHAR2,
                           cIC_Price                   IN NUMBER,
                           cDocument_Exch_Rate         IN NUMBER,
                           --
                           cPrice_method_id            IN NUMBER,
                           --
                           cDocument_Currency          IN VARCHAR2,
                           cAttributeRowId             IN ROWID,
                           cHeader_ID                  IN NUMBER,
                           cRef_Ctl_id                 IN NUMBER,
                           cInternalErrorCode          IN OUT NUMBER
                           ) RETURN NUMBER IS

    vFrozen               xxcp_transaction_attributes.frozen%Type:= 'N';
    vSetInternalErrorCode xxcp_transaction_attributes.internal_error_code%Type := 0;
    vAttribute_Id         xxcp_transaction_attributes.attribute_id%Type;
    vOLS_Date             xxcp_transaction_attributes.ols_date%Type;
    
    vTax_Rate       number;
    vTax_Rate_id    number;

  BEGIN

    IF nvl(cInternalErrorCode, 0) = 0 THEN
      -- New OLS Frozen flag logic - VT268
      vFrozen := 'Y';
      If cOLS_Flag = 'H' then
        vFrozen   := cOLS_Flag;
        vOLS_Date := gMax_OLS_Date;
      ElsIf cOLS_Flag = 'F' then
        vFrozen   := cOLS_Flag;
        vOLS_Date := Trunc(cOLS_Date);
        If vOLS_Date < cTransaction_Date then
          vOLS_Date := cTransaction_Date;
        End If;
      Else
        vOLS_Date := cTransaction_Date;
      End If;

      -- New Feature for RT
      gLastICPrice       := nvl(cIC_Price,0);
      gLastICPMID        := cPrice_method_id;
      gLastICCurrency    := cIC_Currency;

    END IF;

    vSetInternalErrorCode := cInternalErrorCode;
    
    IF ((cInternalErrorCode <> 0) AND (cInternalErrorCode BETWEEN 3500 AND 3599)) THEN
      vSetInternalErrorCode := 3600;
    ELSIF cTRANSACTION_ID IS NULL THEN
      vSetInternalErrorCode := 3001;
    END IF;

    Set_Attribute_DFF(cSource_Type_id => cSource_Type_id);

    IF ((xxcp_global.Preview_on = 'N') AND (cRecordAction = 'I')) THEN
      BEGIN

        if nvl(xxcp_global.forecast_on,'N') = 'Y' or nvl(xxcp_global.true_up_on,'N') = 'Y' or nvl(xxcp_global.replan_on,'N') = 'Y' then
          INSERT INTO XXCP_FC_TRANSACTION_ATTRIBUTES
            (FORECAST_ID,
             CPA_TYPE,
             COST_PLUS_SET_ID,
             ATTRIBUTE_ID,
             SET_OF_BOOKS_ID,
             SOURCE_ASSIGNMENT_ID,
             PARENT_TRX_ID,
             TRANSACTION_ID,
             INTERFACE_ID,
             SOURCE_TABLE_ID,
             SOURCE_TYPE_ID,
             SOURCE_CLASS_ID,
             TRADING_SET_ID,
             TRANSACTION_DATE,
             --
             PARTNER_ASSOC_ID,
             PARTNER_ASSIGN_RULE_ID,
             PARTNER_TAX_REG_ID,
             --
             PARTNER_LEGAL_CURR,
             PARTNER_LEGAL_EXCH_RATE,
             --
             EXCHANGE_DATE,
             TC_QUALIFIER1,
             TC_QUALIFIER2,
             MC_QUALIFIER1,
             MC_QUALIFIER2,
             MC_QUALIFIER3,
             MC_QUALIFIER4,
             ED_QUALIFIER1,
             ED_QUALIFIER2,
             ED_QUALIFIER3,
             ED_QUALIFIER4,
             TAX_QUALIFIER1,
             TAX_QUALIFIER2,
             ADJUSTMENT_RATE,
             ADJUSTMENT_RATE_ID,
             IC_TRADE_TAX_ID,
             IC_TAX_RATE,
             FROZEN,
             OLS_DATE,
             INTERNAL_ERROR_CODE,
             TRANSACTION_REF1,
             TRANSACTION_REF2,
             TRANSACTION_REF3,
             STEP_NUMBER,
             OWNER_TAX_REG_ID,
             IC_UNIT_PRICE,
             IC_PRICE,
             IC_CONTROL,
             IC_CURRENCY,
             DOCUMENT_CURRENCY,
             DOCUMENT_EXCH_RATE,
             QUANTITY,
             UOM,
             PRICE_METHOD_ID,
             HEADER_ID,
             ATTRIBUTE1,
             ATTRIBUTE2,
             ATTRIBUTE3,
             ATTRIBUTE4,
             ATTRIBUTE5,
             ATTRIBUTE6,
             ATTRIBUTE7,
             ATTRIBUTE8,
             ATTRIBUTE9,
             ATTRIBUTE10,
             ATTRIBUTE11,
             ATTRIBUTE12,
             ATTRIBUTE13,
             ATTRIBUTE14,
             ATTRIBUTE15,
             ATTRIBUTE16,
             ATTRIBUTE17,
             ATTRIBUTE18,
             ATTRIBUTE19,
             ATTRIBUTE20,
             ATTRIBUTE21,
             ATTRIBUTE22,
             ATTRIBUTE23,
             ATTRIBUTE24,
             ATTRIBUTE25,
             ATTRIBUTE26,
             ATTRIBUTE27,
             ATTRIBUTE28,
             ATTRIBUTE29,
             ATTRIBUTE30,
             CREATED_BY,
             CREATION_DATE,
             LAST_UPDATE_LOGIN)
          VALUES
            (xxcp_global.gCommon(1).Forecast_ID,
             xxcp_global.gCommon(1).Cpa_Type,
             xxcp_wks.gActual_Costs_Rec(1).cost_plus_set_id,
             xxcp_attribute_id.NEXTVAL,
             cSET_OF_BOOKS_ID,
             cSOURCE_ASSIGNMENT_ID,
             cPARENT_TRX_ID,
             cTRANSACTION_ID,
             xxcp_global.gCommon(1).Current_Interface_id,
             cSOURCE_TABLE_ID,
             cSOURCE_TYPE_ID,
             cSOURCE_CLASS_ID,
             cTRADING_SET_ID,
             cTRANSACTION_DATE,
             cPartner_ASSOC_ID,
             cPARTNER_ASSIGNMENT_RULE_ID,
             cPARTNER_TAX_REG_ID,
             --
             cPartner_LEGAL_CURR,
             cPartner_LEGAL_EXCH_RATE,
             --
             cEXCHANGE_DATE,
             cADJ_QUALIFIER3,
             cADJ_QUALIFIER4,
             cMC_QUALIFIER1,
             cMC_QUALIFIER2,
             cMC_QUALIFIER3,
             cMC_QUALIFIER4,
             cED_QUALIFIER1,
             cED_QUALIFIER2,
             cED_QUALIFIER3,
             cED_QUALIFIER4,
             cTAX_QUALIFIER1,
             cTAX_QUALIFIER2,
             CADJUSTMENT_RATE,
             CADJUSTMENT_RATE_ID,
             vTAX_RATE_ID,
             vTAX_RATE,
             vFrozen,
             vOLS_Date,
             DECODE(vSetInternalErrorCode, 0, NULL, vSetInternalErrorCode),
             cTRANSACTION_REF1,
             cTRANSACTION_REF2,
             cTRANSACTION_REF3,
             cSTEP_NUMBER,
             cOWNER_TAX_REG_ID,
             cIC_UNIT_PRICE,
             cIC_Price,
             cIC_CONTROL,
             cIC_CURRENCY,
             cDOCUMENT_CURRENCY,
             cDOCUMENT_EXCH_RATE,
             cQUANTITY,
             cUOM,
             cPRICE_METHOD_ID,
             cHeader_ID,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE1,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE2,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE3,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE4,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE5,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE6,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE7,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE8,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE9,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE10,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE11,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE12,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE13,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE14,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE15,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE16,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE17,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE18,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE19,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE20,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE21,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE22,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE23,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE24,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE25,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE26,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE27,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE28,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE29,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE30,
             xxcp_global.User_id,
             xxcp_global.Systemdate,
             xxcp_global.Login_id) RETURNING Attribute_ID INTO vAttribute_id;

          MAKE_TRANSACTION_CACHE(cAttribute_id      => vAttribute_id,
                                 cRecordAction      => cRecordAction,
                                 cTransaction_Date  => cTransaction_Date,
                                 cD1000_Pointers    => cD1000_Pointers,
                                 cD1002_Pointers    => cD1002_Pointers,
                                 cInternalErrorCode => cInternalErrorCode);
        Else
          INSERT INTO XXCP_TRANSACTION_ATTRIBUTES
            (ATTRIBUTE_ID,
             SET_OF_BOOKS_ID,
             SOURCE_ASSIGNMENT_ID,
             PARENT_TRX_ID,
             TRANSACTION_ID,
             INTERFACE_ID,
             SOURCE_TABLE_ID,
             SOURCE_TYPE_ID,
             SOURCE_CLASS_ID,
             TRADING_SET_ID,
             TRANSACTION_DATE,
             --
             PARTNER_ASSOC_ID,
             PARTNER_ASSIGN_RULE_ID,
             PARTNER_TAX_REG_ID,
             --
             PARTNER_LEGAL_CURR,
             PARTNER_LEGAL_EXCH_RATE,
             --
             EXCHANGE_DATE,
             TC_QUALIFIER1,
             TC_QUALIFIER2,
             MC_QUALIFIER1,
             MC_QUALIFIER2,
             MC_QUALIFIER3,
             MC_QUALIFIER4,
             ED_QUALIFIER1,
             ED_QUALIFIER2,
             ED_QUALIFIER3,
             ED_QUALIFIER4,
             TAX_QUALIFIER1,
             TAX_QUALIFIER2,
             ADJUSTMENT_RATE,
             ADJUSTMENT_RATE_ID,
             IC_TRADE_TAX_ID,
             IC_TAX_RATE,
             FROZEN,
             OLS_DATE,
             INTERNAL_ERROR_CODE,
             TRANSACTION_REF1,
             TRANSACTION_REF2,
             TRANSACTION_REF3,
             STEP_NUMBER,
             OWNER_TAX_REG_ID,
             IC_UNIT_PRICE,
             IC_PRICE,
             IC_CONTROL,
             IC_CURRENCY,
             DOCUMENT_CURRENCY,
             DOCUMENT_EXCH_RATE,
             QUANTITY,
             UOM,
             PRICE_METHOD_ID,
             HEADER_ID,
             ATTRIBUTE1,
             ATTRIBUTE2,
             ATTRIBUTE3,
             ATTRIBUTE4,
             ATTRIBUTE5,
             ATTRIBUTE6,
             ATTRIBUTE7,
             ATTRIBUTE8,
             ATTRIBUTE9,
             ATTRIBUTE10,
             ATTRIBUTE11,
             ATTRIBUTE12,
             ATTRIBUTE13,
             ATTRIBUTE14,
             ATTRIBUTE15,
             ATTRIBUTE16,
             ATTRIBUTE17,
             ATTRIBUTE18,
             ATTRIBUTE19,
             ATTRIBUTE20,
             ATTRIBUTE21,
             ATTRIBUTE22,
             ATTRIBUTE23,
             ATTRIBUTE24,
             ATTRIBUTE25,
             ATTRIBUTE26,
             ATTRIBUTE27,
             ATTRIBUTE28,
             ATTRIBUTE29,
             ATTRIBUTE30,
             CREATED_BY,
             CREATION_DATE,
             LAST_UPDATE_LOGIN)
          VALUES
            (xxcp_attribute_id.NEXTVAL,
             cSET_OF_BOOKS_ID,
             cSOURCE_ASSIGNMENT_ID,
             cPARENT_TRX_ID,
             cTRANSACTION_ID,
             xxcp_global.gCommon(1).Current_Interface_id,
             cSOURCE_TABLE_ID,
             cSOURCE_TYPE_ID,
             cSOURCE_CLASS_ID,
             cTRADING_SET_ID,
             cTRANSACTION_DATE,
             cPartner_ASSOC_ID,
             cPARTNER_ASSIGNMENT_RULE_ID,
             cPARTNER_TAX_REG_ID,
             --
             cPartner_LEGAL_CURR,
             cPartner_LEGAL_EXCH_RATE,
             --
             cEXCHANGE_DATE,
             cADJ_QUALIFIER3,
             cADJ_QUALIFIER4,
             cMC_QUALIFIER1,
             cMC_QUALIFIER2,
             cMC_QUALIFIER3,
             cMC_QUALIFIER4,
             cED_QUALIFIER1,
             cED_QUALIFIER2,
             cED_QUALIFIER3,
             cED_QUALIFIER4,
             cTAX_QUALIFIER1,
             cTAX_QUALIFIER2,
             CADJUSTMENT_RATE,
             CADJUSTMENT_RATE_ID,
             vTAX_RATE_ID,
             vTAX_RATE,
             vFrozen,
             vOLS_Date,
             DECODE(vSetInternalErrorCode, 0, NULL, vSetInternalErrorCode),
             cTRANSACTION_REF1,
             cTRANSACTION_REF2,
             cTRANSACTION_REF3,
             cSTEP_NUMBER,
             cOWNER_TAX_REG_ID,
             cIC_UNIT_PRICE,
             cIC_Price,
             cIC_CONTROL,
             cIC_CURRENCY,
             cDOCUMENT_CURRENCY,
             cDOCUMENT_EXCH_RATE,
             cQUANTITY,
             cUOM,
             cPRICE_METHOD_ID,
             cHeader_ID,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE1,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE2,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE3,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE4,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE5,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE6,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE7,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE8,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE9,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE10,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE11,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE12,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE13,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE14,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE15,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE16,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE17,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE18,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE19,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE20,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE21,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE22,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE23,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE24,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE25,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE26,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE27,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE28,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE29,
             xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE30,
             xxcp_global.User_id,
             xxcp_global.Systemdate,
             xxcp_global.Login_id) RETURNING Attribute_ID INTO vAttribute_id;

             MAKE_TRANSACTION_CACHE(cAttribute_id      => vAttribute_id,
                                    cRecordAction      => cRecordAction,
                                    cTransaction_Date  => cTransaction_Date,
                                    cD1000_Pointers    => cD1000_Pointers,
                                    cD1002_Pointers    => cD1002_Pointers,
                                    cInternalErrorCode => cInternalErrorCode);
                                 
              If cRef_Ctl_id > 0 and cInternalErrorCode = 0 then
               -- Make Transction Reference             
               cInternalErrorCode := xxcp_te_reference.Create_Reference(
                                                  cRecord_Action        => cRecordAction,
                                                  cReference_Ctl_id     => cRef_Ctl_id,
                                                  cSource_Assignment_id => cSource_Assignment_id ,
                                                  cAttribute_id         => vAttribute_Id,
                                                  cTransaction_Ref      => cTransaction_Ref1,
                                                  cTransaction_id       => cTransaction_id,
                                                  cTransaction_Date     => cTransaction_date,
                                                  cParent_Trx_id        => cParent_Trx_id,
                                                  cTrading_Set_id       => cTrading_Set_id,
                                                  cInterface_id         => cInterface_id);
            End If;                     
                                 
                                 
        end if;

      EXCEPTION
        WHEN OTHERS THEN
           cInternalErrorCode := 3050;
           xxcp_foundation.FndWriteError(cInternalErrorCode,SQLERRM);

      END;

    ELSIF ((xxcp_global.Preview_on = 'N') AND (cRecordAction = 'U')) THEN
      -- Update Mode
      BEGIN
        vAttribute_Id := cAttribute_id;

        MAKE_TRANSACTION_CACHE(cAttribute_id      => vAttribute_id,
                               cRecordAction      => cRecordAction,
                               cTransaction_Date  => cTransaction_Date,
                               cD1000_Pointers    => cD1000_Pointers,
                               cD1002_Pointers    => cD1002_Pointers,
                               cInternalErrorCode => cInternalErrorCode);


         If cRef_Ctl_id > 0 and cInternalErrorCode = 0 then
               -- Make Transction Reference             
               cInternalErrorCode := xxcp_te_reference.Create_Reference(
                                                  cRecord_Action        => cRecordAction,
                                                  cReference_Ctl_id     => cRef_Ctl_id,
                                                  cSource_Assignment_id => cSource_Assignment_id ,
                                                  cAttribute_id         => vAttribute_Id,
                                                  cTransaction_Ref      => cTransaction_Ref1,
                                                  cTransaction_id       => cTransaction_id,
                                                  cTransaction_Date     => cTransaction_date,
                                                  cParent_Trx_id        => cParent_Trx_id,
                                                  cTrading_Set_id       => cTrading_Set_id,
                                                  cInterface_id         => cInterface_id);
         End If; 
         
         UPDATE XXCP_TRANSACTION_ATTRIBUTES
           SET SET_OF_BOOKS_ID        = cSET_OF_BOOKS_ID,
               Source_Assignment_Id   = cSource_Assignment_id,
               HEADER_ID              = cHeader_ID,
               INTERFACE_ID           = cInterface_id,
               PARENT_TRX_ID          = cPARENT_TRX_ID,
               TRANSACTION_DATE       = cTRANSACTION_DATE,
               PARTNER_ASSOC_ID       = cPartner_ASSOC_ID,
               PARTNER_ASSIGN_RULE_ID = cPartner_Assignment_Rule_id,
               PARTNER_TAX_REG_ID     = cPARTNER_TAX_REG_ID,
               --
               PARTNER_LEGAL_CURR      = cPartner_LEGAL_CURR,
               PARTNER_LEGAL_EXCH_RATE = cPartner_LEGAL_EXCH_RATE,
               -- New
               Quantity                = cQuantity,
               UOM                     = cUOM,
               --
               EXCHANGE_DATE       = cEXCHANGE_DATE,
               TC_QUALIFIER1       = cADJ_QUALIFIER3,
               TC_QUALIFIER2       = cADJ_QUALIFIER4,
               MC_QUALIFIER1       = cMC_QUALIFIER1,
               MC_QUALIFIER2       = cMC_QUALIFIER2,
               MC_QUALIFIER3       = cMC_QUALIFIER3,
               MC_QUALIFIER4       = cMC_QUALIFIER4,
               ED_QUALIFIER1       = cED_QUALIFIER1,
               ED_QUALIFIER2       = cED_QUALIFIER2,
               ED_QUALIFIER3       = cED_QUALIFIER3,
               ED_QUALIFIER4       = cED_QUALIFIER4,
               TAX_QUALIFIER1      = cTAX_QUALIFIER1,
               TAX_QUALIFIER2      = cTAX_QUALIFIER2,
               ATTRIBUTE1          = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE1,
               ATTRIBUTE2          = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE2,
               ATTRIBUTE3          = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE3,
               ATTRIBUTE4          = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE4,
               ATTRIBUTE5          = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE5,
               ATTRIBUTE6          = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE6,
               ATTRIBUTE7          = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE7,
               ATTRIBUTE8          = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE8,
               ATTRIBUTE9          = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE9,
               ATTRIBUTE10         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE10,
               ATTRIBUTE11         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE11,
               ATTRIBUTE12         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE12,
               ATTRIBUTE13         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE13,
               ATTRIBUTE14         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE14,
               ATTRIBUTE15         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE15,
               ATTRIBUTE16         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE16,
               ATTRIBUTE17         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE17,
               ATTRIBUTE18         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE18,
               ATTRIBUTE19         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE19,
               ATTRIBUTE20         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE20,
               ATTRIBUTE21         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE21,
               ATTRIBUTE22         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE22,
               ATTRIBUTE23         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE23,
               ATTRIBUTE24         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE24,
               ATTRIBUTE25         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE25,
               ATTRIBUTE26         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE26,
               ATTRIBUTE27         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE27,
               ATTRIBUTE28         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE28,
               ATTRIBUTE29         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE29,
               ATTRIBUTE30         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE30,
               Adjustment_Rate     = cAdjustment_Rate,
               Adjustment_Rate_id  = cAdjustment_Rate_id,
               IC_TRADE_TAX_ID     = vTAX_RATE_ID,
               IC_TAX_RATE         = vTAX_RATE,
               FROZEN              = vFrozen,
               OLS_DATE            = vOLS_Date,
               INTERNAL_ERROR_CODE = DECODE(vSetInternalErrorCode,
                                            0,
                                            NULL,
                                            vSetInternalErrorCode),
               TRANSACTION_REF1    = cTRANSACTION_REF1,
               TRANSACTION_REF2    = cTRANSACTION_REF2,
               TRANSACTION_REF3    = cTRANSACTION_REF3,
               STEP_NUMBER         = cSTEP_NUMBER,
               OWNER_Tax_Reg_ID    = cOwner_Tax_Reg_ID,
               IC_UNIT_PRICE       = cIC_UNIT_PRICE,
               IC_PRICE            = cIC_Price,
               DOCUMENT_Currency   = cDocument_Currency,
               IC_CONTROL          = cIC_Control,
               IC_Currency         = cIC_Currency,
               DOCUMENT_Exch_Rate  = cDocument_Exch_Rate,
               PRICE_METHOD_ID     = cPRICE_METHOD_ID,
               LAST_UPDATED_BY     = xxcp_global.User_id,
               LAST_UPDATE_DATE    = xxcp_global.Systemdate,
               LAST_UPDATE_LOGIN   = xxcp_global.Login_id
         WHERE ROWID = cAttributeRowid;

      EXCEPTION
        WHEN OTHERS THEN
           cInternalErrorCode := 3050;
           xxcp_foundation.FndWriteError(cInternalErrorCode,SQLERRM);
      END;
    ELSIF ((xxcp_global.Preview_on = 'Y') AND (cRecordAction = 'I')) THEN
      BEGIN


        INSERT INTO XXCP_PV_TRANSACTION_ATTRIBUTES
          (preview_id,
           set_of_books_id,
           source_assignment_id,
           parent_trx_id,
           transaction_id,
           interface_id,
           source_table_id,
           source_type_id,
           source_class_id,
           trading_set_id,
           transaction_date,
           partner_assoc_id,
           partner_assign_rule_id,
           partner_tax_reg_id,
           partner_legal_curr,
           partner_legal_exch_rate,
           exchange_date,
           tc_qualifier1,
           tc_qualifier2,
           mc_qualifier1,
           mc_qualifier2,
           mc_qualifier3,
           mc_qualifier4,
           ed_qualifier1,
           ed_qualifier2,
           ed_qualifier3,
           ed_qualifier4,
           tax_qualifier1,
           tax_qualifier2,
           adjustment_rate,
           adjustment_rate_id,
           ic_trade_tax_id,
           ic_tax_rate,
           frozen,
           ols_date,
           internal_error_code,
           transaction_ref1,
           transaction_ref2,
           transaction_ref3,
           step_number,
           owner_tax_reg_id,
           ic_unit_price,
           ic_price,
           quantity,
           uom,
           price_method_id,
           attribute_id,
           header_id,
           ic_currency,
           ic_control,
           attribute1,
           attribute2,
           attribute3,
           attribute4,
           attribute5,
           attribute6,
           attribute7,
           attribute8,
           attribute9,
           attribute10,
           attribute11,
           attribute12,
           attribute13,
           attribute14,
           attribute15,
           attribute16,
           attribute17,
           attribute18,
           attribute19,
           attribute20,
           attribute21,
           attribute22,
           attribute23,
           attribute24,
           attribute25,
           attribute26,
           attribute27,
           attribute28,
           attribute29,
           attribute30,

           created_by,
           creation_date,
           last_update_login)
        VALUES
          (xxcp_global.gCommon(1).Preview_id,
           cSET_OF_BOOKS_ID,
           cSource_Assignment_id,
           cPARENT_TRX_ID,
           cTRANSACTION_ID,
           xxcp_global.gCommon(1).Current_Interface_id,
           cSOURCE_TABLE_ID,
           cSOURCE_TYPE_ID,
           cSOURCE_CLASS_ID,
           cTRADING_SET_ID,
           cTRANSACTION_DATE,
           cPartner_ASSOC_ID,
           cPARTNER_ASSIGNMENT_RULE_ID,
           cPARTNER_TAX_REG_ID,
           cPartner_LEGAL_CURR,
           cPartner_LEGAL_EXCH_RATE,
           cEXCHANGE_DATE,
           cADJ_QUALIFIER3,
           cADJ_QUALIFIER4,
           cMC_QUALIFIER1,
           cMC_QUALIFIER2,
           cMC_QUALIFIER3,
           cMC_QUALIFIER4,
           cED_QUALIFIER1,
           cED_QUALIFIER2,
           cED_QUALIFIER3,
           cED_QUALIFIER4,
           cTAX_QUALIFIER1,
           cTAX_QUALIFIER2,
           cAdjustment_Rate,
           cAdjustment_Rate_id,
           vTax_Rate_id,
           vTax_Rate,
           vFROZEN,
           vOLS_DATE,
           DECODE(vSetInternalErrorCode, 0, NULL, vSetInternalErrorCode),
           cTRANSACTION_REF1,
           cTRANSACTION_REF2,
           cTRANSACTION_REF3,
           cSTEP_NUMBER,
           cOwner_Tax_Reg_id,
           cIC_UNIT_PRICE,
           cIC_Price,
           cQuantity,
           cUOM,
           cPRICE_METHOD_ID,
           xxcp_PREVIEW_ATT_SEQ.NEXTVAL,
           cHeader_ID,
           cIC_Currency,
           cIC_Control,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE1,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE2,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE3,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE4,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE5,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE6,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE7,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE8,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE9,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE10,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE11,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE12,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE13,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE14,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE15,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE16,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE17,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE18,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE19,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE20,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE21,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE22,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE23,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE24,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE25,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE26,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE27,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE28,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE29,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE30,
           xxcp_global.User_id,
           xxcp_global.SystemDate,
           xxcp_global.Login_id)
        RETURNING ATTRIBUTE_ID INTO vAttribute_id;

        MAKE_TRANSACTION_CACHE(vAttribute_id,
                               cRecordAction,
                               cTransaction_Date,
                               cD1000_Pointers,
                               cD1002_Pointers,
                               cInternalErrorCode);


      EXCEPTION
        WHEN OTHERS THEN
           cInternalErrorCode := 3050;
           xxcp_foundation.FndWriteError(cInternalErrorCode,SQLERRM);

      END;
    ELSIF ((xxcp_global.Preview_on = 'Y') AND (cRecordAction = 'U')) THEN
      BEGIN
        vAttribute_Id := cAttribute_id;

        UPDATE XXCP_PV_TRANSACTION_ATTRIBUTES
           SET preview_id             = xxcp_global.gCommon(1).Preview_id,
               SET_OF_BOOKS_ID        = cSET_OF_BOOKS_ID,
               Source_Assignment_Id   = cSource_Assignment_id,
               HEADER_ID              = cHeader_ID,
               PARENT_TRX_ID          = cPARENT_TRX_ID,
               INTERFACE_ID           = cInterface_id,
               TRANSACTION_DATE       = cTRANSACTION_DATE,
               PARTNER_ASSOC_ID       = cPartner_ASSOC_ID,
               PARTNER_ASSIGN_RULE_ID = cPartner_Assignment_Rule_id,
               PARTNER_TAX_REG_ID     = cPARTNER_TAX_REG_ID,
               --
               PARTNER_LEGAL_CURR      = cPartner_LEGAL_CURR,
               PARTNER_LEGAL_EXCH_RATE = cPartner_LEGAL_EXCH_RATE,
               -- New
               Quantity            = cQuantity,
               UOM                 = cUOM,
               --
               EXCHANGE_DATE       = cEXCHANGE_DATE,
               TC_QUALIFIER1       = cADJ_QUALIFIER3,
               TC_QUALIFIER2       = cADJ_QUALIFIER4,
               IC_CONTROL          = cIC_CONTROL,
               MC_QUALIFIER1       = cMC_QUALIFIER1,
               MC_QUALIFIER2       = cMC_QUALIFIER2,
               MC_QUALIFIER3       = cMC_QUALIFIER3,
               MC_QUALIFIER4       = cMC_QUALIFIER4,
               ED_QUALIFIER1       = cED_QUALIFIER1,
               ED_QUALIFIER2       = cED_QUALIFIER2,
               ED_QUALIFIER3       = cED_QUALIFIER3,
               ED_QUALIFIER4       = cED_QUALIFIER4,
               TAX_QUALIFIER1      = cTAX_QUALIFIER1,
               TAX_QUALIFIER2      = cTAX_QUALIFIER2,
               STEP_NUMBER         = cSTEP_NUMBER,
               ATTRIBUTE1          = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE1,
               ATTRIBUTE2          = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE2,
               ATTRIBUTE3          = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE3,
               ATTRIBUTE4          = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE4,
               ATTRIBUTE5          = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE5,
               ATTRIBUTE6          = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE6,
               ATTRIBUTE7          = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE7,
               ATTRIBUTE8          = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE8,
               ATTRIBUTE9          = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE9,
               ATTRIBUTE10         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE10,
               ATTRIBUTE11         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE11,
               ATTRIBUTE12         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE12,
               ATTRIBUTE13         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE13,
               ATTRIBUTE14         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE14,
               ATTRIBUTE15         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE15,
               ATTRIBUTE16         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE16,
               ATTRIBUTE17         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE17,
               ATTRIBUTE18         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE18,
               ATTRIBUTE19         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE19,
               ATTRIBUTE20         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE20,
               ATTRIBUTE21         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE21,
               ATTRIBUTE22         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE22,
               ATTRIBUTE23         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE23,
               ATTRIBUTE24         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE24,
               ATTRIBUTE25         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE25,
               ATTRIBUTE26         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE26,
               ATTRIBUTE27         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE27,
               ATTRIBUTE28         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE28,
               ATTRIBUTE29         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE29,
               ATTRIBUTE30         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE30,
               Adjustment_Rate     = cAdjustment_Rate,
               Adjustment_Rate_id  = cAdjustment_Rate_id,
               IC_TRADE_TAX_ID     = vTax_Rate_id,
               IC_TAX_RATE         = vTax_Rate,
               Frozen              = vFrozen,
               OLS_Date            = vOLS_Date,
               Internal_error_code = DECODE(vSetInternalErrorCode,
                                            0,
                                            NULL,
                                            vSetInternalErrorCode),
               Transaction_ref1    = cTransaction_ref1,
               Transaction_ref2    = cTransaction_ref2,
               Transaction_ref3    = cTransaction_ref3,
               Owner_Tax_Reg_ID    = cOwner_Tax_Reg_ID,
               IC_Unit_Price       = cIC_Unit_Price,
               IC_PRICE            = cIC_Price,
               IC_Currency         = cIC_Currency,
               Price_Method_id     = cPrice_Method_id,
               Last_updated_by     = xxcp_global.User_id,
               Last_Update_date    = xxcp_global.Systemdate,
               Last_Update_login   = xxcp_global.Login_id
         WHERE ROWID = cAttributeRowid;

        MAKE_TRANSACTION_CACHE(vAttribute_id,
                               cRecordAction,
                               cTransaction_Date,
                               cD1000_Pointers,
                               cD1002_Pointers,
                               cInternalErrorCode);

      EXCEPTION
        WHEN OTHERS THEN
           cInternalErrorCode := 3050;
           xxcp_foundation.FndWriteError(cInternalErrorCode,SQLERRM);
      END;
    END IF;

    xxcp_trace.ASSIGNMENT_TRACE( cTrace_id => 10, cAttribute_id => vAttribute_id, cRecordAction => cRecordAction);

    RETURN(vAttribute_id);

  END Make_Attributes;

  -- *******************************************************************
  --                        MAKE_SA_ATTRIBUTE
  -- *******************************************************************
  FUNCTION Make_SA_Attributes(
                           cRecordAction               IN VARCHAR2,
                           cSource_Assignment_id       IN NUMBER,
                           cPARENT_TRX_ID              IN NUMBER,
                           cQUANTITY                   IN NUMBER,
                           cUOM                        IN VARCHAR2,
                           cTRANSACTION_ID             IN NUMBER,
                           cSOURCE_TABLE_ID            IN NUMBER,
                           cSOURCE_TYPE_ID             IN NUMBER,
                           cSOURCE_CLASS_ID            IN NUMBER,
                           cTRADING_SET_ID             IN NUMBER,
                           cTRANSACTION_DATE           IN DATE,
                           --
                           cTransaction_currency     IN VARCHAR2,
                           cOwner_legal_curr         IN VARCHAR2,
                           cOwner_legal_exch_rate    IN NUMBER,
                           cOwner_Assoc_id           IN NUMBER,
                           cOwner_Assignment_rule_id IN NUMBER,
                           --
                           cPARTNER_ASSOC_ID           IN NUMBER,
                           cPARTNER_ASSIGNMENT_RULE_ID IN NUMBER,
                           --
                           cPARTNER_TAX_REG_ID         IN NUMBER,
                           cPARTNER_LEGAL_CURR         IN VARCHAR2,
                           cPARTNER_LEGAL_EXCH_RATE    IN NUMBER,
                           cEXCHANGE_DATE              IN DATE,
                           -- Adjustment Qualifiers
                           cADJ_QUALIFIER1             IN VARCHAR2,
                           cADJ_QUALIFIER2             IN VARCHAR2,
                           -- Configuration Control
                           cIC_CONTROL                 IN VARCHAR2,
                           cMC_QUALIFIER1              IN VARCHAR2,
                           cMC_QUALIFIER2              IN VARCHAR2,
                           cMC_QUALIFIER3              IN VARCHAR2,
                           cMC_QUALIFIER4              IN VARCHAR2,
                           -- Tax Qualifiers
                           cTAX_QUALIFIER1             IN VARCHAR2,
                           cTAX_QUALIFIER2             IN VARCHAR2,
                           -- OLS
                           cOLS_FLAG                   IN VARCHAR2,
                           cOLS_DATE                   IN DATE,
                           --
                           cAdjustment_Rate            IN NUMBER,
                           cAdjustment_Rate_id         IN NUMBER,
                           cIC_TRADE_TAX_ID            IN NUMBER,
                           cIC_TAX_RATE                IN NUMBER,
                           cTRANSACTION_REF1           IN VARCHAR2,
                           cTRANSACTION_REF2           IN VARCHAR2,
                           cTRANSACTION_REF3           IN VARCHAR2,
                           --
                           cOwner_Tax_Reg_id           IN NUMBER,
                           --
                           cIC_Unit_Price              IN NUMBER,
                           cIC_Currency                IN VARCHAR2,
                           cIC_Price                   IN NUMBER,
                           cDocument_Exch_Rate         IN NUMBER,
                           --
                           cPrice_method_id            IN NUMBER,
                           --
                           cDocument_Currency          IN VARCHAR2,
                           cInternalErrorCode          IN OUT NUMBER
                           ) RETURN NUMBER IS

    pragma autonomous_transaction;

    vSetInternalErrorCode xxcp_transaction_attributes.internal_error_code%Type := 0;
    vAttribute_Id         xxcp_transaction_attributes.attribute_id%Type;
    vOLS_Date             xxcp_transaction_attributes.ols_date%Type;

    -- Lookup
    Cursor satab(pTransaction_date date , pSource_Assignment_id in number, pTrading_Set_id in number, pSource_Table_Id in number, pTransaction_id in number) is
      select sa.rowid sa_rowid, sa.attribute_id
      from xxcp_sa_attributes sa
      where sa.source_assignment_id = pSource_Assignment_id
        and sa.source_table_id      = pSource_Table_id
        and sa.transaction_id       = pTransaction_id
        and sa.trading_set_id       = pTrading_Set_id
        and sa.transaction_date     = pTransaction_date
        order by attribute_id desc;

    vSA_Rowid rowid;
    vAction   varchar2(1) := 'I';

  BEGIN

    IF nvl(cInternalErrorCode, 0) = 0 THEN
      If xxcp_global.gControlRec.Update_Existing_Row = 'Y' then
         -- Find Record
         For saRec in saTab(cTransaction_date , cSource_Assignment_id, cTrading_Set_id , cSource_Table_Id, cTransaction_id ) Loop
           vSa_Rowid     := saRec.Sa_Rowid;
           vAttribute_id := saRec.Attribute_id;
           vAction       := 'U'; -- This will only update the last record created.
           Exit;
         End Loop;
      End If;

      -- New OLS Frozen flag logic - VT268
      If cOLS_Flag = 'H' then
        vOLS_Date := gMax_OLS_Date;
      ElsIf cOLS_Flag = 'F' then
        vOLS_Date := Trunc(cOLS_Date);
        If vOLS_Date < cTransaction_Date then
          vOLS_Date := cTransaction_Date;
        End If;
      Else
        vOLS_Date := cTransaction_Date;
      End If;

      -- New Feature for RT
      gLastICPrice       := nvl(cIC_Price,0);
      gLastICPMID        := cPrice_method_id;
      gLastICCurrency    := cIC_Currency;

    END IF;

    vSetInternalErrorCode := cInternalErrorCode;
    IF ((cInternalErrorCode <> 0) AND
       (cInternalErrorCode BETWEEN 3500 AND 3599)) THEN
      vSetInternalErrorCode := 3600;
    ELSIF cTRANSACTION_ID IS NULL THEN
      vSetInternalErrorCode := 3001;
    END IF;

    Set_Attribute_DFF(cSource_Type_id => cSource_Type_id);

    IF vSetInternalErrorCode = 0 and vAction != 'U' THEN
      BEGIN

        INSERT INTO XXCP_SA_ATTRIBUTES
          (ATTRIBUTE_ID,
           SOURCE_ASSIGNMENT_ID,
           PARENT_TRX_ID,
           TRANSACTION_ID,
           INTERFACE_ID,
           SOURCE_TABLE_ID,
           SOURCE_TYPE_ID,
           SOURCE_CLASS_ID,
           TRADING_SET_ID,
           TRANSACTION_DATE,
           -- Header
           TRANSACTION_CURRENCY,
           OWNER_LEGAL_CURR,
           OWNER_LEGAL_EXCH_RATE,
           OWNER_ASSOC_ID,
           OWNER_ASSIGN_RULE_ID,
           --
           PARTNER_ASSOC_ID,
           PARTNER_ASSIGN_RULE_ID,
           PARTNER_TAX_REG_ID,
           --
           PARTNER_LEGAL_CURR,
           PARTNER_LEGAL_EXCH_RATE,
           --
           EXCHANGE_DATE,
           TC_QUALIFIER1,
           TC_QUALIFIER2,
           MC_QUALIFIER1,
           MC_QUALIFIER2,
           MC_QUALIFIER3,
           MC_QUALIFIER4,
           TAX_QUALIFIER1,
           TAX_QUALIFIER2,
           ADJUSTMENT_RATE,
           ADJUSTMENT_RATE_ID,
           IC_TRADE_TAX_ID,
           IC_TAX_RATE,
           OLS_FLAG,
           OLS_DATE,
           TRANSACTION_REF1,
           TRANSACTION_REF2,
           TRANSACTION_REF3,
           OWNER_TAX_REG_ID,
           IC_UNIT_PRICE,
           IC_PRICE,
           IC_CONTROL,
           IC_CURRENCY,
           DOCUMENT_CURRENCY,
           DOCUMENT_EXCH_RATE,
           QUANTITY,
           UOM,
           PRICE_METHOD_ID,
           ATTRIBUTE1,
           ATTRIBUTE2,
           ATTRIBUTE3,
           ATTRIBUTE4,
           ATTRIBUTE5,
           ATTRIBUTE6,
           ATTRIBUTE7,
           ATTRIBUTE8,
           ATTRIBUTE9,
           ATTRIBUTE10,
           ATTRIBUTE11,
           ATTRIBUTE12,
           ATTRIBUTE13,
           ATTRIBUTE14,
           ATTRIBUTE15,
           ATTRIBUTE16,
           ATTRIBUTE17,
           ATTRIBUTE18,
           ATTRIBUTE19,
           ATTRIBUTE20,
           ATTRIBUTE21,
           ATTRIBUTE22,
           ATTRIBUTE23,
           ATTRIBUTE24,
           ATTRIBUTE25,
           ATTRIBUTE26,
           ATTRIBUTE27,
           ATTRIBUTE28,
           ATTRIBUTE29,
           ATTRIBUTE30,
           CREATED_BY,
           CREATION_DATE,
           LAST_UPDATED_BY,
           LAST_UPDATE_DATE,
           LAST_UPDATE_LOGIN)
        VALUES
          (XXCP_SAE_INT_SEQ.NEXTVAL,
           cSOURCE_ASSIGNMENT_ID,
           cPARENT_TRX_ID,
           cTRANSACTION_ID,
           xxcp_global.gCommon(1).Current_Interface_id,
           cSOURCE_TABLE_ID,
           cSOURCE_TYPE_ID,
           cSOURCE_CLASS_ID,
           cTRADING_SET_ID,
           cTRANSACTION_DATE,
           -- Header
           cTRANSACTION_CURRENCY,
           cOWNER_LEGAL_CURR,
           cOWNER_LEGAL_EXCH_RATE,
           cOWNER_ASSOC_ID,
           cOwner_Assignment_rule_id,
           --
           cPartner_ASSOC_ID,
           cPARTNER_ASSIGNMENT_RULE_ID,
           cPARTNER_TAX_REG_ID,
           --
           cPartner_LEGAL_CURR,
           cPartner_LEGAL_EXCH_RATE,
           --
           cEXCHANGE_DATE,
           cADJ_QUALIFIER1,
           cADJ_QUALIFIER2,
           cMC_QUALIFIER1,
           cMC_QUALIFIER2,
           cMC_QUALIFIER3,
           cMC_QUALIFIER4,
           cTAX_QUALIFIER1,
           cTAX_QUALIFIER2,
           cADJUSTMENT_RATE,
           cADJUSTMENT_RATE_ID,
           cIC_TRADE_TAX_ID,
           cIC_TAX_RATE,
           cOLS_Flag,
           vOLS_Date,
           cTRANSACTION_REF1,
           cTRANSACTION_REF2,
           cTRANSACTION_REF3,
           cOWNER_TAX_REG_ID,
           cIC_UNIT_PRICE,
           cIC_Price,
           cIC_CONTROL,
           cIC_CURRENCY,
           cDOCUMENT_CURRENCY,
           cDOCUMENT_EXCH_RATE,
           cQUANTITY,
           cUOM,
           cPRICE_METHOD_ID,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE1,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE2,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE3,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE4,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE5,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE6,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE7,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE8,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE9,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE10,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE11,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE12,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE13,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE14,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE15,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE16,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE17,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE18,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE19,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE20,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE21,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE22,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE23,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE24,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE25,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE26,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE27,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE28,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE29,
           xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE30,
           xxcp_global.User_id,
           xxcp_global.Systemdate,
           xxcp_global.User_id,
           xxcp_global.Systemdate,
           xxcp_global.Login_id) RETURNING Attribute_ID INTO vAttribute_id;

      EXCEPTION
        WHEN OTHERS THEN
           cInternalErrorCode := 3050;
           xxcp_foundation.FndWriteError(cInternalErrorCode,SQLERRM);

      END;
    ElsIF vSetInternalErrorCode = 0 and vAction = 'U' THEN
      BEGIN

        UPDATE XXCP_SA_ATTRIBUTES
           SET PARENT_TRX_ID       = cPARENT_TRX_ID,
           -- Header
           TRANSACTION_CURRENCY    = cTRANSACTION_CURRENCY,
           OWNER_LEGAL_CURR        = cOWNER_LEGAL_CURR,
           OWNER_LEGAL_EXCH_RATE   = cOWNER_LEGAL_EXCH_RATE,
           OWNER_ASSOC_ID          = cOWNER_ASSOC_ID,
           OWNER_ASSIGN_RULE_ID    = cOwner_Assignment_rule_id,
           PARTNER_ASSOC_ID        = cPartner_ASSOC_ID,
           PARTNER_ASSIGN_RULE_ID  = cPARTNER_ASSIGNMENT_RULE_ID,
           PARTNER_TAX_REG_ID      = cPARTNER_TAX_REG_ID,
           PARTNER_LEGAL_CURR      = cPartner_LEGAL_CURR,
           PARTNER_LEGAL_EXCH_RATE = cPartner_LEGAL_EXCH_RATE,
           --
           EXCHANGE_DATE      = cEXCHANGE_DATE,
           TC_QUALIFIER1      = cADJ_QUALIFIER1,
           TC_QUALIFIER2      = cADJ_QUALIFIER2,
           MC_QUALIFIER1      = cMC_QUALIFIER1,
           MC_QUALIFIER2      = cMC_QUALIFIER2,
           MC_QUALIFIER3      = cMC_QUALIFIER3,
           MC_QUALIFIER4      = cMC_QUALIFIER4,
           TAX_QUALIFIER1     = cTAX_QUALIFIER1,
           TAX_QUALIFIER2     = cTAX_QUALIFIER2,
           ADJUSTMENT_RATE    = cADJUSTMENT_RATE,
           ADJUSTMENT_RATE_ID = cADJUSTMENT_RATE_ID,
           IC_TRADE_TAX_ID    = cIC_TRADE_TAX_ID,
           IC_TAX_RATE        = cIC_TAX_RATE,
           OLS_FLAG           = cOLS_Flag,
           OLS_DATE           = vOLS_Date,
           TRANSACTION_REF1   = cTRANSACTION_REF1,
           TRANSACTION_REF2   = cTRANSACTION_REF2,
           TRANSACTION_REF3   = cTRANSACTION_REF3,
           OWNER_TAX_REG_ID   = cOWNER_TAX_REG_ID,
           IC_UNIT_PRICE      = cIC_UNIT_PRICE,
           IC_PRICE           = cIC_Price,
           IC_CONTROL         = cIC_CONTROL,
           IC_CURRENCY        = cIC_CURRENCY,
           DOCUMENT_CURRENCY  = cDOCUMENT_CURRENCY,
           DOCUMENT_EXCH_RATE = cDOCUMENT_EXCH_RATE,
           QUANTITY           = cQUANTITY,
           UOM                = cUOM,
           PRICE_METHOD_ID    = cPRICE_METHOD_ID,
           ATTRIBUTE1         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE1,
           ATTRIBUTE2         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE2,
           ATTRIBUTE3         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE3,
           ATTRIBUTE4         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE4,
           ATTRIBUTE5         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE5,
           ATTRIBUTE6         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE6,
           ATTRIBUTE7         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE7,
           ATTRIBUTE8         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE8,
           ATTRIBUTE9         = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE9,
           ATTRIBUTE10        = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE10,
           ATTRIBUTE11        = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE11,
           ATTRIBUTE12        = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE12,
           ATTRIBUTE13        = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE13,
           ATTRIBUTE14        = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE14,
           ATTRIBUTE15        = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE15,
           ATTRIBUTE16        = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE16,
           ATTRIBUTE17        = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE17,
           ATTRIBUTE18        = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE18,
           ATTRIBUTE19        = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE19,
           ATTRIBUTE20        = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE20,
           ATTRIBUTE21        = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE21,
           ATTRIBUTE22        = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE22,
           ATTRIBUTE23        = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE23,
           ATTRIBUTE24        = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE24,
           ATTRIBUTE25        = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE25,
           ATTRIBUTE26        = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE26,
           ATTRIBUTE27        = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE27,
           ATTRIBUTE28        = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE28,
           ATTRIBUTE29        = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE29,
           ATTRIBUTE30        = xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE30,
           LAST_UPDATED_BY    = xxcp_global.User_id ,
           LAST_UPDATE_DATE   = xxcp_global.Systemdate,
           LAST_UPDATE_LOGIN  = xxcp_global.Login_id
         Where Rowid = vSA_Rowid;

      EXCEPTION
        WHEN OTHERS THEN
           cInternalErrorCode := 3051;
           xxcp_foundation.FndWriteError(cInternalErrorCode,SQLERRM);

      END;
    END IF;

    xxcp_trace.ASSIGNMENT_TRACE( cTrace_id => 10, cAttribute_id => vAttribute_id, cRecordAction => cRecordAction);

    RETURN(vAttribute_id);

  END Make_SA_Attributes;
  -- *********************************************************************************
  --                             Report_SA_Errors
  -- *********************************************************************************
  Procedure Report_SA_Errors( cInternalErrorCode IN OUT NUMBER) IS
    
  pragma autonomous_transaction;
   
  Begin
    -- Log file to be created when an error is found

    IF nvl(cInternalErrorCode, 0) != 0  and xxcp_global.Preview_on = 'N' THEN
     Begin
       INSERT INTO XXCP_SA_ERRORS(
          ATTRIBUTE1,
          ATTRIBUTE10,
          ATTRIBUTE11,
          ATTRIBUTE12,
          ATTRIBUTE13,
          ATTRIBUTE14,
          ATTRIBUTE15,
          ATTRIBUTE16,
          ATTRIBUTE17,
          ATTRIBUTE18,
          ATTRIBUTE19,
          ATTRIBUTE2,
          ATTRIBUTE20,
          ATTRIBUTE21,
          ATTRIBUTE22,
          ATTRIBUTE23,
          ATTRIBUTE24,
          ATTRIBUTE25,
          ATTRIBUTE26,
          ATTRIBUTE27,
          ATTRIBUTE28,
          ATTRIBUTE29,
          ATTRIBUTE3,
          ATTRIBUTE30,
          ATTRIBUTE31,
          ATTRIBUTE32,
          ATTRIBUTE33,
          ATTRIBUTE34,
          ATTRIBUTE35,
          ATTRIBUTE36,
          ATTRIBUTE37,
          ATTRIBUTE38,
          ATTRIBUTE39,
          ATTRIBUTE4,
          ATTRIBUTE40,
          ATTRIBUTE41,
          ATTRIBUTE42,
          ATTRIBUTE43,
          ATTRIBUTE44,
          ATTRIBUTE45,
          ATTRIBUTE46,
          ATTRIBUTE47,
          ATTRIBUTE48,
          ATTRIBUTE49,
          ATTRIBUTE5,
          ATTRIBUTE50,
          ATTRIBUTE6,
          ATTRIBUTE7,
          ATTRIBUTE8,
          ATTRIBUTE9,
          CURRENCY_CODE,
          ENTERED_CR,
          ENTERED_DR,
          EXCHANGE_RATE_DATE,
          LAST_UPDATED_BY,
          LAST_UPDATE_DATE,
          LAST_UPDATE_LOGIN,
          LEDGER_ID,
          LINKED_ROWID,
          ORG_ID,
          SEGMENT1,
          SEGMENT10,
          SEGMENT11,
          SEGMENT12,
          SEGMENT13,
          SEGMENT14,
          SEGMENT15,
          SEGMENT2,
          SEGMENT3,
          SEGMENT4,
          SEGMENT5,
          SEGMENT6,
          SEGMENT7,
          SEGMENT8,
          SEGMENT9,
          SET_OF_BOOKS_ID,
          VT_CREATED_DATE,
          VT_DATE_PROCESSED,
          VT_GROUPING_RULE_ID,
          VT_INTERNAL_ERROR_CODE,
          VT_STATUS,
          VT_PARENT_TRX_ID,
          VT_QUALIFIER1,
          VT_QUALIFIER2,
          VT_REQUEST_ID,
          VT_SOURCE_ASSIGNMENT_ID,
          VT_STATUS_CODE,
          VT_TRANSACTION_CLASS,
          VT_TRANSACTION_DATE,
          VT_TRANSACTION_GROUP,
          VT_TRANSACTION_ID,
          VT_TRANSACTION_LINE,
          VT_TRANSACTION_REF,
          VT_TRANSACTION_TABLE,
          VT_TRANSACTION_TYPE,
          CREATION_DATE,
          CREATED_BY,
          VT_INTERFACE_ID
        )
        VALUES
          (
          gInboundRec.ATTRIBUTE1,
          gInboundRec.ATTRIBUTE10,
          gInboundRec.ATTRIBUTE11,
          gInboundRec.ATTRIBUTE12,
          gInboundRec.ATTRIBUTE13,
          gInboundRec.ATTRIBUTE14,
          gInboundRec.ATTRIBUTE15,
          gInboundRec.ATTRIBUTE16,
          gInboundRec.ATTRIBUTE17,
          gInboundRec.ATTRIBUTE18,
          gInboundRec.ATTRIBUTE19,
          gInboundRec.ATTRIBUTE2,
          gInboundRec.ATTRIBUTE20,
          gInboundRec.ATTRIBUTE21,
          gInboundRec.ATTRIBUTE22,
          gInboundRec.ATTRIBUTE23,
          gInboundRec.ATTRIBUTE24,
          gInboundRec.ATTRIBUTE25,
          gInboundRec.ATTRIBUTE26,
          gInboundRec.ATTRIBUTE27,
          gInboundRec.ATTRIBUTE28,
          gInboundRec.ATTRIBUTE29,
          gInboundRec.ATTRIBUTE3,
          gInboundRec.ATTRIBUTE30,
          gInboundRec.ATTRIBUTE31,
          gInboundRec.ATTRIBUTE32,
          gInboundRec.ATTRIBUTE33,
          gInboundRec.ATTRIBUTE34,
          gInboundRec.ATTRIBUTE35,
          gInboundRec.ATTRIBUTE36,
          gInboundRec.ATTRIBUTE37,
          gInboundRec.ATTRIBUTE38,
          gInboundRec.ATTRIBUTE39,
          gInboundRec.ATTRIBUTE4,
          gInboundRec.ATTRIBUTE40,
          gInboundRec.ATTRIBUTE41,
          gInboundRec.ATTRIBUTE42,
          gInboundRec.ATTRIBUTE43,
          gInboundRec.ATTRIBUTE44,
          gInboundRec.ATTRIBUTE45,
          gInboundRec.ATTRIBUTE46,
          gInboundRec.ATTRIBUTE47,
          gInboundRec.ATTRIBUTE48,
          gInboundRec.ATTRIBUTE49,
          gInboundRec.ATTRIBUTE5,
          gInboundRec.ATTRIBUTE50,
          gInboundRec.ATTRIBUTE6,
          gInboundRec.ATTRIBUTE7,
          gInboundRec.ATTRIBUTE8,
          gInboundRec.ATTRIBUTE9,
          gInboundRec.CURRENCY_CODE,
          gInboundRec.ENTERED_CR,
          gInboundRec.ENTERED_DR,
          gInboundRec.EXCHANGE_RATE_DATE,
          gInboundRec.LAST_UPDATED_BY,
          gInboundRec.LAST_UPDATE_DATE,
          gInboundRec.LAST_UPDATE_LOGIN,
          gInboundRec.LEDGER_ID,
          gInboundRec.LINKED_ROWID,
          gInboundRec.ORG_ID,
          gInboundRec.SEGMENT1,
          gInboundRec.SEGMENT10,
          gInboundRec.SEGMENT11,
          gInboundRec.SEGMENT12,
          gInboundRec.SEGMENT13,
          gInboundRec.SEGMENT14,
          gInboundRec.SEGMENT15,
          gInboundRec.SEGMENT2,
          gInboundRec.SEGMENT3,
          gInboundRec.SEGMENT4,
          gInboundRec.SEGMENT5,
          gInboundRec.SEGMENT6,
          gInboundRec.SEGMENT7,
          gInboundRec.SEGMENT8,
          gInboundRec.SEGMENT9,
          gInboundRec.SET_OF_BOOKS_ID,
          gInboundRec.VT_CREATED_DATE,
          gInboundRec.VT_DATE_PROCESSED,
          gInboundRec.VT_GROUPING_RULE_ID,
          cInternalErrorCode,
          'ERROR',
          gInboundRec.VT_PARENT_TRX_ID,
          gInboundRec.VT_QUALIFIER1,
          gInboundRec.VT_QUALIFIER2,
          gInboundRec.VT_REQUEST_ID,
          gInboundRec.VT_SOURCE_ASSIGNMENT_ID,
          gInboundRec.VT_STATUS_CODE,
          gInboundRec.VT_TRANSACTION_CLASS,
          gInboundRec.VT_TRANSACTION_DATE,
          gInboundRec.VT_TRANSACTION_GROUP,
          gInboundRec.VT_TRANSACTION_ID,
          gInboundRec.VT_TRANSACTION_LINE,
          gInboundRec.VT_TRANSACTION_REF,
          gInboundRec.VT_TRANSACTION_TABLE,
          gInboundRec.VT_TRANSACTION_TYPE,
          sysdate,
          fnd_global.user_id
         ,XXCP_SAE_INT_SEQ.NEXTVAL
          );
      EXCEPTION
        WHEN OTHERS THEN
           cInternalErrorCode := 3050;
           xxcp_foundation.FndWriteError(cInternalErrorCode,SQLERRM);
      END;
    END IF;

  END Report_SA_Errors;

  --
  -- Set_RealTime_Results
  --
  Procedure Set_RealTime_Results(cSource_Type_id in number) is

    Cursor c1(pSource_type_id in number) is
    select
     custom_attribute1_id
    ,custom_attribute2_id
    ,custom_attribute3_id
    ,custom_attribute4_id
    ,custom_attribute5_id
    ,custom_attribute6_id
    ,custom_attribute7_id
    ,custom_attribute8_id
    ,custom_attribute9_id
    ,custom_attribute10_id
    ,custom_attribute11_id
    ,custom_attribute12_id
    ,custom_attribute13_id
    ,custom_attribute14_id
    ,custom_attribute15_id
    ,custom_attribute16_id
    ,custom_attribute17_id
    ,custom_attribute18_id
    ,custom_attribute19_id
    ,custom_attribute20_id
    ,custom_attribute21_id
    ,custom_attribute22_id
    ,custom_attribute23_id
    ,custom_attribute24_id
    ,custom_attribute25_id
    ,custom_attribute26_id
    ,custom_attribute27_id
    ,custom_attribute28_id
    ,custom_attribute29_id
    ,custom_attribute30_id
    ,custom_attribute31_id
    ,custom_attribute32_id
    ,custom_attribute33_id
    ,custom_attribute34_id
    ,custom_attribute35_id
    ,custom_attribute36_id
    ,custom_attribute37_id
    ,custom_attribute38_id
    ,custom_attribute39_id
    ,custom_attribute40_id
    ,custom_attribute41_id
    ,custom_attribute42_id
    ,custom_attribute43_id
    ,custom_attribute44_id
    ,custom_attribute45_id
    ,custom_attribute46_id
    ,custom_attribute47_id
    ,custom_attribute48_id
    ,custom_attribute49_id
    ,custom_attribute50_id
    from xxcp_result_labels r
    where r.source_type_id = pSource_Type_id;

  Begin

    For Rec in c1(pSource_Type_id => cSource_Type_id) Loop

      If rec.custom_attribute1_id is not null then
        xxcp_wks.gRealTime_Results.Attribute1 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute1_id);
      End If;
      If rec.custom_attribute2_id is not null then
        xxcp_wks.gRealTime_Results.Attribute2 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute2_id);
      End If;
      If rec.custom_attribute3_id is not null then
        xxcp_wks.gRealTime_Results.Attribute3 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute3_id);
      End If;
      If rec.custom_attribute4_id is not null then
        xxcp_wks.gRealTime_Results.Attribute4 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute4_id);
      End If;
      If rec.custom_attribute5_id is not null then
        xxcp_wks.gRealTime_Results.Attribute5 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute5_id);
      End If;
      If rec.custom_attribute6_id is not null then
        xxcp_wks.gRealTime_Results.Attribute6 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute6_id);
      End If;
      If rec.custom_attribute7_id is not null then
        xxcp_wks.gRealTime_Results.Attribute7 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute7_id);
      End If;
      If rec.custom_attribute8_id is not null then
        xxcp_wks.gRealTime_Results.Attribute8 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute8_id);
      End If;
      If rec.custom_attribute9_id is not null then
        xxcp_wks.gRealTime_Results.Attribute9 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute9_id);
      End If;
      If rec.custom_attribute10_id is not null then
        xxcp_wks.gRealTime_Results.Attribute10 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute10_id);
      End If;
      If rec.custom_attribute11_id is not null then
        xxcp_wks.gRealTime_Results.Attribute11 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute11_id);
      End If;
      If rec.custom_attribute12_id is not null then
        xxcp_wks.gRealTime_Results.Attribute12 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute12_id);
      End If;
      If rec.custom_attribute13_id is not null then
        xxcp_wks.gRealTime_Results.Attribute13 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute13_id);
      End If;
      If rec.custom_attribute14_id is not null then
        xxcp_wks.gRealTime_Results.Attribute14 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute14_id);
      End If;
      If rec.custom_attribute15_id is not null then
        xxcp_wks.gRealTime_Results.Attribute15 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute15_id);
      End If;
      If rec.custom_attribute16_id is not null then
        xxcp_wks.gRealTime_Results.Attribute16 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute16_id);
      End If;
      If rec.custom_attribute17_id is not null then
        xxcp_wks.gRealTime_Results.Attribute17 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute17_id);
      End If;
      If rec.custom_attribute18_id is not null then
        xxcp_wks.gRealTime_Results.Attribute18 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute18_id);
      End If;
      If rec.custom_attribute19_id is not null then
        xxcp_wks.gRealTime_Results.Attribute19 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute19_id);
      End If;
      If rec.custom_attribute20_id is not null then
        xxcp_wks.gRealTime_Results.Attribute20 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute20_id);
      End If;
      If rec.custom_attribute21_id is not null then
        xxcp_wks.gRealTime_Results.Attribute21 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute21_id);
      End If;
      If rec.custom_attribute22_id is not null then
        xxcp_wks.gRealTime_Results.Attribute22 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute22_id);
      End If;
      If rec.custom_attribute23_id is not null then
        xxcp_wks.gRealTime_Results.Attribute23 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute23_id);
      End If;
      If rec.custom_attribute24_id is not null then
        xxcp_wks.gRealTime_Results.Attribute24 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute24_id);
      End If;
      If rec.custom_attribute25_id is not null then
        xxcp_wks.gRealTime_Results.Attribute25 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute25_id);
      End If;
      If rec.custom_attribute26_id is not null then
        xxcp_wks.gRealTime_Results.Attribute26 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute26_id);
      End If;
      If rec.custom_attribute27_id is not null then
        xxcp_wks.gRealTime_Results.Attribute27 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute27_id);
      End If;
      If rec.custom_attribute28_id is not null then
        xxcp_wks.gRealTime_Results.Attribute28 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute28_id);
      End If;
      If rec.custom_attribute29_id is not null then
        xxcp_wks.gRealTime_Results.Attribute29 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute29_id);
      End If;
      If rec.custom_attribute30_id is not null then
        xxcp_wks.gRealTime_Results.Attribute30 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute30_id);
      End If;
      If rec.custom_attribute31_id is not null then
        xxcp_wks.gRealTime_Results.Attribute31 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute31_id);
      End If;
      If rec.custom_attribute32_id is not null then
        xxcp_wks.gRealTime_Results.Attribute32 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute32_id);
      End If;
      If rec.custom_attribute33_id is not null then
        xxcp_wks.gRealTime_Results.Attribute33 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute33_id);
      End If;
      If rec.custom_attribute34_id is not null then
        xxcp_wks.gRealTime_Results.Attribute34 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute34_id);
      End If;
      If rec.custom_attribute35_id is not null then
        xxcp_wks.gRealTime_Results.Attribute35 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute35_id);
      End If;
      If rec.custom_attribute36_id is not null then
        xxcp_wks.gRealTime_Results.Attribute36 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute36_id);
      End If;
      If rec.custom_attribute37_id is not null then
        xxcp_wks.gRealTime_Results.Attribute37 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute37_id);
      End If;
      If rec.custom_attribute38_id is not null then
        xxcp_wks.gRealTime_Results.Attribute38 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute38_id);
      End If;
      If rec.custom_attribute39_id is not null then
        xxcp_wks.gRealTime_Results.Attribute39 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute39_id);
      End If;
      If rec.custom_attribute40_id is not null then
        xxcp_wks.gRealTime_Results.Attribute40 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute40_id);
      End If;
      If rec.custom_attribute41_id is not null then
        xxcp_wks.gRealTime_Results.Attribute41 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute41_id);
      End If;
      If rec.custom_attribute42_id is not null then
        xxcp_wks.gRealTime_Results.Attribute42 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute42_id);
      End If;
      If rec.custom_attribute43_id is not null then
        xxcp_wks.gRealTime_Results.Attribute43 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute43_id);
      End If;
      If rec.custom_attribute44_id is not null then
        xxcp_wks.gRealTime_Results.Attribute44 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute44_id);
      End If;
      If rec.custom_attribute45_id is not null then
        xxcp_wks.gRealTime_Results.Attribute45 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute45_id);
      End If;
      If rec.custom_attribute46_id is not null then
        xxcp_wks.gRealTime_Results.Attribute46 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute46_id);
      End If;
      If rec.custom_attribute47_id is not null then
        xxcp_wks.gRealTime_Results.Attribute47 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute47_id);
      End If;
      If rec.custom_attribute48_id is not null then
        xxcp_wks.gRealTime_Results.Attribute48 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute48_id);
      End If;
      If rec.custom_attribute49_id is not null then
        xxcp_wks.gRealTime_Results.Attribute49 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute49_id);
      End If;
      If rec.custom_attribute50_id is not null then
        xxcp_wks.gRealTime_Results.Attribute50 := xxcp_foundation.Find_DynamicAttribute(cPosition => rec.custom_attribute50_id);
      End If;
    End Loop;

  End Set_RealTime_Results;

  -- *******************************************************************
  --                    Table_Actions
  -- *******************************************************************
  PROCEDURE Table_Actions(cSource_Table_id       IN NUMBER,
                          cTrading_Set_ID        IN NUMBER,
                          cTransaction_id        IN NUMBER,
                          cSource_Type_id        IN VARCHAR2,
                          cParent_Trx_Id         IN NUMBER,
                          cMultiple_Owners       IN VARCHAR2,
                          cMultipleOwnerTaxRegId IN NUMBER,                          
                          cAttributeRowid        IN OUT ROWID,
                          cCreate                IN VARCHAR2,
                          cRefresh               IN VARCHAR2,
                          cSource_Assignment_id  IN VARCHAR2,
                          cFrozen                OUT VARCHAR2,
                          cOwnerTaxId            OUT NUMBER,
                          cHeaderFrozen          OUT VARCHAR2,
                          cHeaderRowid           OUT ROWID,
                          cRecordHeaderAction    OUT VARCHAR2,
                          cRecordAttributeAction OUT VARCHAR2,
                          cTransaction_Currency  OUT VARCHAR2,
                          cHeader_ID             OUT NUMBER) IS

    vTransaction_Currency xxcp_transaction_header.transaction_currency%Type;
    vAttribute_ID         xxcp_transaction_attributes.attribute_id%Type;
    vHeader_ID            xxcp_transaction_attributes.header_id%Type;
    vParent_Trx_ID        xxcp_transaction_attributes.parent_trx_id%Type;
    vOwnerExists          BOOLEAN;

  BEGIN
    --
    -- See if Header/Owner already exists...
    --
    If nvl(cMultiple_Owners,'N') = 'N' then
    
         vOwnerExists := 
                    xxcp_foundation.Transaction_Header_Exists(cSource_Assignment_id  => cSource_Assignment_id,
                                                              cTrading_Set_id        => cTrading_Set_id,
                                                              cParent_Trx_Id         => cParent_Trx_Id,
                                                              cSource_Table_id       => cSource_Table_id,
                                                              cOwner_Trx_id          => cOwnerTaxId,
                                                              cFrozen                => cHeaderFrozen,
                                                              cHeaderRowid           => cHeaderRowid,
                                                              cHeader_ID             => vHeader_ID,
                                                              cTransaction_Currency  => vTransaction_Currency);

    ElsIf cMultiple_Owners = 'Y' then
         vOwnerExists := 
                    xxcp_foundation.Transaction_Header_ExistsII(
                                                              cSource_Assignment_id  => cSource_Assignment_id,
                                                              cTrading_Set_id        => cTrading_Set_id,
                                                              cParent_Trx_Id         => cParent_Trx_Id,
                                                              cSource_Table_id       => cSource_Table_id,
                                                              cMultipleOwnerTaxRegId => cMultipleOwnerTaxRegId,
                                                              cOwner_Trx_id          => cOwnerTaxId,
                                                              cFrozen                => cHeaderFrozen,
                                                              cHeaderRowid           => cHeaderRowid,
                                                              cHeader_ID             => vHeader_ID,
                                                              cTransaction_Currency  => vTransaction_Currency);


    End If;
    --
    -- If the calling process had found a record the
    -- cAttributeRowid would have a value if so then copy
    -- the required values from the parameters passed..
    --
    IF cAttributeRowid IS NOT NULL THEN
      IF ((cRefresh = 'Y') OR (NVL(cFrozen, 'N') = 'N')) THEN
        cRecordAttributeAction := 'U';
      ELSE
        cRecordAttributeAction := 'N';
      END IF;
    ELSE
      --
      -- The Calling Process had not looked for an Attribute row so
      -- it will need to be looked for..
      --

      IF xxcp_foundation.Transaction_Attribute_Exists(cSource_Assignment_id => cSource_Assignment_id,
                                                      cTrading_Set_Id       => cTrading_Set_id,
                                                      cTransaction_Id       => cTransaction_Id,
                                                      cSource_Table_id      => cSource_Table_id,
                                                      cSource_Type_id       => cSource_Type_id,
                                                      cFrozen               => cFrozen,
                                                      cAttributeRowid       => cAttributeRowid,
                                                      cAttribute_Id         => vAttribute_ID,
                                                      cParent_Trx_id        => vParent_Trx_ID) THEN

        IF ((cRefresh = 'Y') OR (cFrozen <> 'Y')) THEN
          cRecordAttributeAction := 'U';
        ELSE
          cRecordAttributeAction := 'N';
        END IF;
      ELSE
        --
        -- If Attribute row doesn't exist then insert the new row..
        --
        IF cCreate = 'Y' THEN
          cRecordAttributeAction := 'I';
        ELSE
          cRecordAttributeAction := 'N';
        END IF;
      END IF;
    END IF;

    --
    -- Work out if to insert a new row, or update the existing
    -- XXCP_TRANSACTION_HEADER row...
    --
    IF vOwnerExists THEN
      IF ((cRefresh = 'Y') OR (cHeaderFrozen <> 'Y')) THEN
        cRecordHeaderAction := 'U';
      ELSE
        cRecordHeaderAction := 'N';
      END IF;
    ELSIF cCreate = 'Y' THEN
      cRecordHeaderAction := 'I';
    ELSE
      cRecordHeaderAction := 'N';
    END IF;

    cTransaction_Currency := vTransaction_Currency;
    cHeader_ID            := vHeader_ID;
  END Table_Actions;

  -- *******************************************************************
  --                    Pass Through/Ignore Rule
  -- *******************************************************************
  Function Pass_Through_Rule
                        ( cPass_Through_Set_id   IN NUMBER,
                          cTransaction_Date      IN DATE,
                          cOwnerLegalEntity      IN NUMBER,
                          cPartnerLegalEntity    IN NUMBER,
                          cOwnerTaxId            IN NUMBER,
                          cPartnerTaxId          IN NUMBER,
                          cOwnerTaxAuthority     IN VARCHAR,
                          cPartnerTaxAuthority   IN VARCHAR,
                          cOwnerTaxAttribute1    IN VARCHAR,
                          cPartnerTaxAttribute1  IN VARCHAR
                        ) return Number is

   vPT_Qualifier1 xxcp_pass_through_rules.pt_qualifier1%Type;
   vPT_Qualifier2 xxcp_pass_through_rules.pt_qualifier2%Type;
   vPT_Qualifier3 xxcp_pass_through_rules.pt_qualifier3%Type;
   vPT_Qualifier4 xxcp_pass_through_rules.pt_qualifier4%Type;

   vResult number(6) := 0;

   cursor ps (pPass_Through_Set_id in number) is
    select r.Name Pass_Through_Set,
           s.sequence,
           s.sequence_name,
           s.ptq1_attribute_id,
           s.ptq2_attribute_id,
           s.ptq3_attribute_id,
           s.ptq4_attribute_id,
           r.parent_level_control
    from xxcp_pass_through_rule_sets r,
         xxcp_pass_through_rule_sq   s
     where s.Pass_Through_Set_id = pPass_Through_Set_id
       and s.pass_through_set_id = r.pass_through_set_id
       and s.active = 'Y'
     Order by s.sequence;

   -- Pass through rules
   cursor pr ( pPass_Through_Set_id   in number
              ,pPass_Through_Sequence in number
              ,pTransaction_Date      in date
              ,pPT_Qualifier1         in varchar2
              ,pPT_Qualifier2         in varchar2
              ,pPT_Qualifier3         in varchar2
              ,pPT_Qualifier4         in varchar2
              ) is
      SELECT r.match_legal_entity
           , r.match_tax_reg
           , r.match_tax_authority
           , r.match_tax_reg_attribute1
           , r.match_entity_partnership_id
           , nvl(r.match_tax_reg_group,'N') match_tax_reg_group
           , r.Action_type
           , r.Pass_through_id
        FROM xxcp_pass_through_rules r
       WHERE r.Pass_Through_Set_id = pPass_Through_Set_id
         AND r.sequence            = pPass_Through_Sequence
         AND pTransaction_Date between NVL(r.effective_from_date, '01-JAN-1900') AND NVL(r.effective_to_date, '31-DEC-2999')
         AND NVL(r.PT_Qualifier1, '~null~') =
             DECODE(r.PT_Qualifier1,
                    '*',
                    NVL(r.PT_Qualifier1, '~null~'),
                    NVL(pPT_Qualifier1, '~null~'))
         AND NVL(r.PT_Qualifier2, '~null~') =
             DECODE(r.PT_Qualifier2,
                    '*',
                    NVL(r.PT_Qualifier2, '~null~'),
                    NVL(pPT_Qualifier2, '~null~'))
         AND NVL(r.PT_Qualifier3, '~null~') =
             DECODE(r.PT_Qualifier3,
                    '*',
                    NVL(r.PT_Qualifier3, '~null~'),
                    NVL(pPT_Qualifier3, '~null~'))
         AND NVL(r.PT_Qualifier4, '~null~') =
             DECODE(r.PT_Qualifier4,
                    '*',
                    NVL(r.PT_Qualifier4, '~null~'),
                    NVL(pPT_Qualifier4, '~null~'));

   vScore                 pls_integer := 0;
   vMatch_EP_Id           xxcp_pass_through_rules.Match_Entity_Partnership_id%Type;
   vMatch_Legal_entity    xxcp_pass_through_rules.MATCH_LEGAL_ENTITY%Type;
   vMatch_Tax_reg         xxcp_pass_through_rules.MATCH_TAX_REG%Type;
   vMatch_Tax_authority   xxcp_pass_through_rules.MATCH_TAX_AUTHORITY%Type;
   vMatch_Tax_reg_attrib1 xxcp_pass_through_rules.MATCH_TAX_REG%Type;
   vPass_through_id       xxcp_pass_through_rules.Pass_through_id%Type;

   vEntity_Partnership_id     xxcp_entity_partnerships.entity_set_id%type;
   
   Cursor TR_GRP(pOwner_Registration_id in number, pPartner_Registration_id in number) is
    select nvl(r1.tax_reg_group_id,0) Tax_Reg_Group_Id
     from xxcp_tax_registrations r1,
          xxcp_tax_registrations r2
     where r1.tax_registration_id = pOwner_Registration_id
       and r2.tax_registration_id = pPartner_Registration_id
       and r1.tax_reg_group_id    = r2.tax_reg_group_id;
     
  Begin

    For Rec in ps(pPass_Through_Set_id => cPass_Through_Set_id) Loop

       vPT_Qualifier1 := Null;
       vPT_Qualifier2 := Null;
       vPT_Qualifier3 := Null;
       vPT_Qualifier4 := Null;

       If nvl(rec.ptq1_attribute_id,0) > 0 then
         vPT_Qualifier1 := xxcp_foundation.Find_DynamicAttribute(NVL(rec.ptq1_attribute_id,0));
       End If;
       If nvl(rec.ptq2_attribute_id,0) > 0 then
         vPT_Qualifier2 := xxcp_foundation.Find_DynamicAttribute(NVL(rec.ptq2_attribute_id,0));
       End If;
       If nvl(rec.ptq3_attribute_id,0) > 0 then
         vPT_Qualifier3 := xxcp_foundation.Find_DynamicAttribute(NVL(rec.ptq3_attribute_id,0));
       End If;
       If nvl(rec.ptq4_attribute_id,0) > 0 then
         vPT_Qualifier4 := xxcp_foundation.Find_DynamicAttribute(NVL(rec.ptq4_attribute_id,0));
       End If;

       For RecPR in pr(pPass_Through_Set_id   => cPass_Through_Set_id,
                       pPass_Through_Sequence => rec.sequence,
                       pTransaction_Date      => cTransaction_Date,
                       pPT_Qualifier1         => vPT_Qualifier1,
                       pPT_Qualifier2         => vPT_Qualifier2,
                       pPT_Qualifier3         => vPT_Qualifier3,
                       pPT_Qualifier4         => vPT_Qualifier4
                       ) Loop

           vScore := 0;

           vMatch_legal_entity    := RecPR.MATCH_LEGAL_ENTITY;
           vMatch_tax_reg         := RecPR.MATCH_TAX_REG;
           vMatch_tax_authority   := RecPR.MATCH_TAX_AUTHORITY;
           vMatch_tax_reg_attrib1 := RecPR.Match_Tax_Reg_Attribute1;
           vMatch_EP_Id           := RecPR.Match_Entity_Partnership_id;

           IF (RecPR.Match_Tax_Reg = 'Y'
             OR RecPR.Match_Legal_Entity = 'Y'
             OR RecPR.Match_Tax_Authority = 'Y'
             OR RecPR.Match_Tax_Reg_attribute1 = 'Y'
             OR RecPR.Match_Entity_Partnership_id > 0)
              THEN

             If cOwnerTaxId = cPartnerTaxId and RecPR.Match_Tax_Reg = 'Y' then
               vScore := 1;
             ElsIf cOwnerLegalEntity = cPartnerLegalEntity and RecPR.Match_Legal_Entity = 'Y' then
               vScore := 2;
             ElsIf cOwnerTaxAuthority = cPartnerTaxAuthority and RecPR.Match_Tax_Authority = 'Y' then
               vScore := 3;
             ElsIf cOwnerTaxAttribute1 = cPartnerTaxAttribute1 And RecPR.Match_Tax_Reg_attribute1 = 'Y' then
               vScore := 4;
             ElsIf RecPR.Match_Tax_Reg_Group = 'Y' then
               If cOwnerTaxId = cPartnerTaxId then
                 vScore := 5;
               Else
                  For RecTR in TR_GRP(cOwnerTaxId,cPartnerTaxId)  Loop
                    vScore := 5; 
                  End Loop;
               End If;                 
             ElsIf RecPR.Match_Entity_Partnership_id > 0 then
                   -- Work out the Trading Relationship
                If xxcp_te_base.Get_Trading_Relationship(
                                    cAction_Type               => 'PT',
                                    cSource_id                 => xxcp_global.gCOMMON(1).Source_id,
                                    cOwner_Tax_Reg_id          => cOwnerTaxid, 
                                    cPartner_Tax_Reg_Id        => cPartnerTaxId,
                                    cTransaction_Date          => cTransaction_Date,
                                    cCreate_IC_Trx             => 'N',
                                    cDflt_Trading_Relationship => 'N',
                                    cEntity_Partnership_id     => vEntity_Partnership_id) = 0 
                then
                 vScore := vScore + 1;
                End If;
             
             End If;

            Else -- No matches required.
              vScore := 10;
            End If;

           If vScore > 0 then
             vPass_through_id := RecPR.Pass_through_id;
             If RecPR.Action_type = 'N' then
                vResult := 7021; -- Ignored
             Else
                vResult := 7025; -- Pass
             End If;
           End If;

      End Loop;

      If vResult > 0 And Rec.parent_level_control = 'F' then    -- Error and Passthough
         vResult  := vResult + 1;
         xxcp_global.gPassThroughCode := vResult;
      Elsif vResult > 0 And Rec.parent_level_control = 'S' then -- Do not Error current Trx
         xxcp_global.gPassThroughCode := vResult + 2;
         vResult  := 0;
      End If;

      xxcp_trace.PASS_THROUGH_TRACE(
                               cTrace_id             => 71
                             , cPass_Through_Name    => Rec.sequence_name
                             , cPass_Through_Set     => Rec.Pass_Through_Set
                             , cPass_Through_Seq     => Rec.Sequence
                             , cPass_Through_Set_ID  => cPass_Through_Set_id
                             , cPass_Through_ID      => vPass_through_id
                             , cPT_Qualifier1        => vPT_Qualifier1
                             , cPT_Qualifier2        => vPT_Qualifier2
                             , cPT_Qualifier3        => vPT_Qualifier3
                             , cPT_Qualifier4        => vPT_Qualifier4
                             , cOwner_Tax_Reg_id     => cOwnerTaxId
                             , cPartner_Tax_Reg_id   => cPartnerTaxId
                             , cMatch_Legal_Entity   => vMatch_legal_entity
                             , cMatch_Tax_Reg        => vMatch_tax_reg
                             , cMatch_Tax_Authority  => vMatch_tax_authority
                             , cMatch_Tax_Reg_Attr1  => vMatch_tax_reg_attrib1
                             , cMatch_EP_id          => vMatch_EP_Id
                             , cControl_Flag         => Rec.parent_level_control
                             , cResult               => xxcp_global.gPassThroughCode
                          );

        If vResult > 0 then
           Exit;
        End If;

    End Loop;

    Return(vResult);

  End Pass_Through_Rule;

                                
  --
  -- Tax_Coding
  --
  Procedure Tax_Coding (
                        cSourceRowid            IN ROWID,
                        cTransaction_id         IN NUMBER,
                        cTransaction_Date       IN DATE,
                        cOwnerTaxid             IN NUMBER,
                        cPartnerTaxId           IN NUMBER,
                        --
                        cTAX_Qualifier1         IN VARCHAR2,
                        cTAX_Qualifier2         IN VARCHAR2,
                        cTax_Code_Indicator     IN VARCHAR2,
                        --
                        cIC_Control             IN VARCHAR2,
                        cTax_Code_id           OUT NUMBER,
                        cTax_Rate              OUT NUMBER,
                        cInternalErrorCode  IN OUT NUMBER) IS

    CURSOR TaxOne(pTax_Code_Indicator IN VARCHAR2, pTransaction_Date IN DATE, pTaxOwnerTaxid IN NUMBER, pTaxPartnerTaxId IN NUMBER
                , pTax_Qualifier1 IN VARCHAR2, pTax_Qualifier2 IN VARCHAR2) IS
      SELECT DISTINCT tc.ic_trad_tax_id tax_id
                    , Decode(tc.ar_tax_rate,0,0,NULL,NULL,tc.ar_tax_rate/100) AR_TAX_RATE
                    , tc.ar_tax_code_id, tc.ar_instance_id
        FROM xxcp_tax_codes tc
       WHERE DECODE(pTax_Code_Indicator,
                    'O',TC.OWNER_TAX_REG_ID,
                    'P',TC.PARTNER_TAX_REG_ID) =
             DECODE(pTax_Code_Indicator,
                    'O',pTaxOwnerTaxid,
                    'P',pTaxPartnerTaxId)
         AND NVL(tc.TAX_QUALIFIER1, '~null~') = NVL(pTAX_Qualifier1, '~null~')
         AND NVL(tc.tax_qualifier2, '~null~') = NVL(ptax_qualifier2, '~null~')
         AND pTransaction_date BETWEEN
             NVL(effective_from_date, '01-JAN-1900') AND
             NVL(effective_to_date, '31-DEC-2999');

    CURSOR TaxTwo(pTransaction_Date IN DATE, pTaxOwnerTaxid IN NUMBER, pTaxPartnerTaxId IN NUMBER
                 , pTax_Qualifier1 IN VARCHAR2, pTax_Qualifier2 IN VARCHAR2) IS
      SELECT /* + index(tc) */
      DISTINCT tc.ic_trad_tax_id tax_id
             , DECODE(tc.ar_tax_rate,0,0,NULL,NULL,tc.ar_tax_rate/100) AR_TAX_RATE
             , tc.ar_tax_code_id, tc.ar_instance_id
        FROM xxcp_tax_codes tc
       WHERE TC.OWNER_TAX_REG_ID = pTaxOwnerTaxId
         AND TC.PARTNER_TAX_REG_ID = pTaxPartnerTaxId
         AND NVL(tc.TAX_QUALIFIER1, '~null~') = NVL(pTAX_Qualifier1, '~null~')
         AND NVL(tc.TAX_QUALIFIER2, '~null~') = NVL(pTAX_Qualifier2, '~null~')
         AND pTransaction_date BETWEEN
             nvl(effective_from_date, '01-JAN-1900') AND
             nvl(effective_to_date, '31-DEC-2999');

    CURSOR TaxRate(pAR_tax_code_id IN NUMBER, pAR_instance_id IN NUMBER) IS
      SELECT r.tax_rate
        FROM xxcp_instance_ar_tax_code_v r
       WHERE r.tax_id       = pAR_tax_code_id
         AND r.instance_id  = pAR_instance_id;

    x pls_integer;

    vTaxOwnerTaxId          xxcp_tax_registrations.tax_registration_id%Type;
    vTaxPartnerTaxId        xxcp_tax_registrations.tax_registration_id%Type;
    vIC_Tax_Description     xxcp_transaction_attributes.ATTRIBUTE9%Type;
    vIC_Tax_Code            xxcp_tax_codes.tax_qualifier1%Type;

    Begin

       -- TAX CODES
      IF cInternalErrorCode = 0 AND (cIC_Control <> 'N') THEN
        --
        -- Work out the Tax Id ....   ("B" has not been implemented yet")
        --
        IF cIC_Control = 'P' THEN
          -- Swap Owner and Partner
          vTaxOwnerTaxId   := cPartnerTaxId;
          vTaxPartnerTaxId := cOwnerTaxId;
        ELSE
          vTaxOwnerTaxId   := cOwnerTaxId;
          vTaxPartnerTaxId := cPartnerTaxId;
        END IF;

        -- TAX Codes
        IF cTax_Code_Indicator IN ('O', 'P', 'B') THEN

            cTax_Code_id := NULL;

            IF cTax_Code_Indicator IN ('O', 'P') THEN
                --
                -- Find Tax code based on Owner or Partner
                --
                x  := 0;
                cInternalErrorCode := 3064;

                FOR Rec IN TaxOne(pTax_Code_Indicator   => cTax_Code_Indicator,
                                  pTransaction_Date     => cTransaction_Date,
                                  pTaxOwnerTaxid        => vTaxOwnerTaxId,
                                  pTaxPartnerTaxId      => vTaxPartnerTaxId,
                                  pTax_Qualifier1       => cTax_Qualifier1,
                                  pTax_Qualifier2       => cTax_Qualifier2)
                LOOP
                  cTax_Code_Id := Rec.Tax_id;
                  --
                  -- GET Tax Rate
                  --
                  IF Rec.AR_TAX_RATE IS NULL  THEN -- Do not nvl this!!  Perforance reasons
                    cTax_Rate := 0;

                    FOR RATERec IN TaxRate(pAR_tax_code_id => Rec.AR_tax_code_id,
                                           pAR_instance_id => Rec.AR_instance_id)
                    LOOP

                      IF RATERec.Tax_Rate = 0 OR RATERec.Tax_Rate IS NULL THEN
                        cTax_Rate  := 0;
                      ELSE
                        cTax_Rate := RATERec.Tax_Rate/100;
                      END IF;

                    END LOOP;

                  ELSE
                    cTax_Rate := Rec.AR_TAX_RATE; -- Already devided by 100.
                  END IF;
                  cInternalErrorCode := 0;
                  x := x + 1;
                END LOOP;

                IF x > 1 THEN
                  -- Too many tax codes
                  cInternalErrorCode := 3065;
                END IF;

                IF cInternalErrorCode <> 0 THEN
                  xxcp_foundation.FndWriteError(cInternalErrorCode,
                                                'Tax Code Indicator <' ||cTax_Code_Indicator ||
                                                '> Owner Tax Reg. <' ||TO_CHAR(vTaxOwnerTaxid) ||
                                                '> Partner Tax Reg. <' ||TO_CHAR(vTaxPartnerTaxId) ||
                                                '> Qualifier 1 <'||cTax_Qualifier1||
                                                '> Qualifier 2 <'||cTax_Qualifier2||
                                                '> IC Control <' ||cIC_Control ||
                                                '> Transaction Date <' ||TO_CHAR(cTransaction_Date) || '>');
                END IF;

              ELSE

                x                  := 0;
                cInternalErrorCode := 3064;

                FOR Rec IN TaxTwo(pTransaction_Date   => cTransaction_Date,
                                  pTaxOwnerTaxid      => vTaxOwnerTaxId,
                                  pTaxPartnerTaxId    => vTaxPartnerTaxId,
                                  pTax_Qualifier1     => cTax_Qualifier1,
                                  pTax_Qualifier2     => cTax_Qualifier2)
                LOOP
                  cTax_Code_Id := Rec.Tax_id;
                  --
                  -- GET Tax Rate if it has not been set.
                  --
                  IF Rec.AR_TAX_RATE IS NULL THEN -- Do not nvl this!!  Performance reasons
                    cTax_Rate := 0;
                    FOR RATERec IN TaxRate(pAR_tax_code_id => Rec.AR_tax_code_id,
                                           pAR_instance_id => Rec.AR_instance_id)
                    LOOP
                      IF RATERec.Tax_Rate = 0 OR RATERec.Tax_Rate IS NULL THEN
                        cTax_Rate  := 0;
                      ELSE
                        cTax_Rate := RATERec.Tax_Rate/100;
                      END IF;
                    END LOOP;
                  ELSE
                    cTax_Rate := Rec.AR_TAX_RATE;
                  END IF;
                  --
                  --
                  cInternalErrorCode := 0;
                  x := x + 1;
                END LOOP;

                IF x > 1 THEN
                  -- Too many tax codes
                  cInternalErrorCode := 3065;
                END IF;

                IF cInternalErrorCode <> 0 THEN
                  xxcp_foundation.FndWriteError(cInternalErrorCode,
                                                  'Owner Tax Reg. <' ||TO_CHAR(vTaxOwnerTaxId) ||
                                                '> Partner Tax Reg. <' ||TO_CHAR(vTaxPartnerTaxId) ||
                                                '> Qualifier 1 <'||cTax_Qualifier1||
                                                '> Qualifier 2 <'||cTax_Qualifier2||
                                                '> IC Control <' ||cIC_Control ||
                                                '> Transaction Date <' ||TO_CHAR(cTransaction_Date) || '>');
                END IF;
              END IF;
          ELSIF cTax_Code_Indicator = 'E' THEN -- External Tax Engine

              cTax_Code_Id := Null;
              cTax_Rate    := Null;

              If xxcp_global.Timing_Adv_Mode = 'Y' then
                gTraceStartTime := systimestamp;
              End If;

              cInternalErrorCode :=
                 xxcp_custom_events.IC_TAX_ENGINE(
                               cSource_id             => xxcp_global.gCommon(1).Source_id,
                               cSource_Assignment_id  => xxcp_global.gCommon(1).Current_Assignment_id,
                               cTransaction_Table     => xxcp_global.gCommon(1).Current_Transaction_Table,
                               cParent_Trx_id         => xxcp_global.gCommon(1).Current_Parent_Trx_id,
                               cSource_Rowid          => cSourceRowid,
                               cTransaction_id        => cTransaction_Id,
                               cTransaction_type      => xxcp_global.gCommon(1).Current_Transaction_Type,
                               cTrading_Set           => xxcp_global.gCommon(1).Current_Trading_Set,
                               cOwner_Tax_Reg_id      => vTaxOwnerTaxId,
                               cPartner_Tax_Reg_id    => vTaxPartnerTaxId,
                               cTransaction_Date      => cTransaction_Date,
                               cTax_Qualifier1        => cTax_Qualifier1,
                               cTax_Qualifier2        => cTax_Qualifier2,
                               cIC_Control            => cIC_Control,
                               --
                               cIC_Tax_Rate           => cTax_Rate,
                               cIC_Tax_Ref_id         => cTax_Code_Id,
                               cIC_Tax_Description    => vIC_Tax_Description,
                               cIC_Tax_Code           => vIC_Tax_Code
                              ) ;

                If xxcp_global.Timing_Adv_Mode = 'Y' then
                 gTraceEndTime := systimestamp;
                 CustomEventTiming('IC Tax Engine');
                End If;

                -- This needs looking at
                 xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE9 := vIC_Tax_Description;
                 xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE10:= vIC_Tax_Code;

                IF nvl(cInternalErrorCode,0) <> 0 THEN
                  xxcp_foundation.FndWriteError(cInternalErrorCode,'External:'||
                                                  'Owner Tax Reg. <' ||TO_CHAR(vTaxOwnerTaxId) ||
                                                '> Partner Tax Reg. <' ||TO_CHAR(vTaxPartnerTaxId) ||
                                                '> Qualifier 1 <'||cTax_Qualifier1||
                                                '> Qualifier 2 <'||cTax_Qualifier2||
                                                '> IC Control <' ||cIC_Control ||
                                                '> Transaction Date <' ||TO_CHAR(cTransaction_Date) || '>');
                END IF;
          END IF;

      END IF; -- End cInternalErrorCode
 
  End Tax_Coding;

  -- *******************************************************************
  --                    PRICING METHOD
  -- *******************************************************************
  Procedure Pricing_Method(cPrice_Method_id        IN OUT NUMBER,
                           cSource_id              IN NUMBER,
                           cPricing_Type           IN VARCHAR2,
                           cShared_Pricing         IN VARCHAR2,
                           cSourceRowid            IN ROWID,
                           cTransaction_id         IN NUMBER,
                           cOwnerTaxId             IN VARCHAR2,
                           cPartnerTaxId           IN VARCHAR2,
                           cTransaction_Currency   IN VARCHAR2,
                           cKey                    IN VARCHAR2,
                           cSet_of_books_id        IN NUMBER,
                           cPrevious_Attribute_id  IN NUMBER,
                           cTransaction_Date       IN DATE,
                           cCreate_IC_Trx          IN VARCHAR2,
                           cQuantity               IN NUMBER,
                           cTax_Rate               IN NUMBER,
                           cADJ_Qualifier1         OUT VARCHAR2,
                           cADJ_Qualifier2         OUT VARCHAR2,
                           cADJ_Qualifier3         OUT VARCHAR2,
                           cADJ_Qualifier4         OUT VARCHAR2,
                           cIC_Unit_Price          OUT VARCHAR2,
                           cIC_Currency            OUT VARCHAR2,
                           cIC_Price               OUT VARCHAR2,
                           cDiagnostics_code       OUT VARCHAR2,
                           cD1002_Pointers         OUT VARCHAR2,
                           cAdjustment_Rate_id     OUT NUMBER,
                           cAdjustment_Rate        OUT NUMBER,
                           cExchange_Rate_Type     OUT VARCHAR2,
                           cExchange_Date          IN OUT DATE,
                           cInternalErrorCode      IN OUT NUMBER
                          ) IS

      CURSOR PM(pPrice_Method_id IN NUMBER) IS
      SELECT Price_Method_id,
             Method_Name,
             Rate_set_id,
             SQL_build_id Dynamic_Interface_id,
             Base_val_attribute_id,
             Base_curr_attribute_id,
             Arqc1_attribute_id,
             Arqc2_attribute_id,
             Owner_attribute_id,
             Partner_attribute_id,
             Exchange_Rate_Type,
             Exchange_date_attribute_id,
             To_Curr_indicator,
             To_Curr_attribute_id,
             Data_source,
             Given_Adjustment_Rate_id,
             nvl(PRICING_CALCULATION,'Base Value') PRICING_CALCULATION,
             PRICING_PRECISION_TYPE,
             PRICING_EXTENDED,
             ADJ_DATE_ATTRIBUTE_ID,
             WHEN_NO_ADJUSTMENT_RATE,
             WHEN_BASE_VAL_ZERO,
             WHEN_IC_PRICE_ZERO,
             WHEN_DI_NOT_FOUND,
             WHEN_DI_TOO_MANY_FOUND,
             WHEN_FORMULA_SET_FAILS,
             DFT_BASE_VAL_ATTRIBUTE_ID,
             DFT_BASE_CURR_ATTRIBUTE_ID,
             DFT_BASE_VAL_NOT_ZERO,
             DFT_BASE_VAL_EXTENDED,
             Diagnostics_Code,
             DFT_Diagnostics_Code,
             pm.formula_set_id
        FROM xxcp_pricing_Method pm
       WHERE pm.Price_Method_id = pPrice_Method_id;

    vDft_Diagnostics_code     varchar2(3);
    vDiagnostics_code         varchar2(3);
    vDefault_Value_Required   varchar2(1) := 'N';
    vDefault_Why_Code         xxcp_internal_codes.error_code%Type := 0;
    vTo_Curr_Indicator        xxcp_pricing_method.to_curr_indicator%Type;
    vTo_Curr_Attribute        xxcp_tax_registrations.legal_currency%Type;
    vFrom_IC_Currency         xxcp_tax_registrations.legal_currency%Type;
    vTo_IC_Currency           xxcp_tax_registrations.legal_currency%Type;
    vGiven_Adjustment_Rate    xxcp_transaction_attributes.adjustment_rate%Type;
    vExchange_Date            xxcp_transaction_header.exchange_date%Type;
    vPM_Default_Value         number := 0;
    vPM_Default_Currency      xxcp_tax_registrations.legal_currency%Type;
    vPricing_Interface_id     xxcp_sql_build.sql_build_id%Type;
    vIC_Price_Extended        number := 0;
    vIC_Price                 number := 0;
    vIC_Unit_Price            number := 0;
    vPrecision                number := 0;
    vIC_Exch_Rate             number := 0;
    vIC_Currency              xxcp_tax_registrations.legal_currency%Type;
    vPricing_Calculation      xxcp_pricing_method.pricing_calculation%type;
    vCustom_Bind1             xxcp_transaction_attributes.attribute1%Type;
    vCustom_Bind2             xxcp_transaction_attributes.attribute1%Type;
    vCustom_Bind3             xxcp_transaction_attributes.attribute1%Type;
    vCustom_Bind4             xxcp_transaction_attributes.attribute1%Type;
    vCustom_Bind5             xxcp_transaction_attributes.attribute1%Type;
    vCustom_Bind6             xxcp_transaction_attributes.attribute1%Type;
    vCustom_Bind7             xxcp_transaction_attributes.attribute1%Type;
    vCustom_Bind8             xxcp_transaction_attributes.attribute1%Type;

    vShared_D1002_Array       XXCP_DYNAMIC_ARRAY  :=  XXCP_DYNAMIC_ARRAY(' ');

    -- Cross Source Mapping
    Cursor mx(pSql_Build_id in number) is
    select from_attribute_id, to_attribute_id
      from xxcp_dynamic_attribute_sets s,
           xxcp_cross_source_mapping m
     where s.attribute_set_id = pSQL_Build_id
       and s.sequence_number  = m.from_attribute_id
       and m.source_id        = xxcp_global.gcommon(1).Source_id -- This is right
       and m.to_attribute_id is not null;

    vD1002_Pointers           varchar2(800);
    vAdj_Rate_Date            Date;
    vFormula_Set_id           Number := 0;

    temp_1002_array           xxcp_dynamic_array;
     

 Begin

   gFormula_Pointers_1002 := Null;
   temp_1002_array        := xxcp_wks.d1002_array;

   gXAM_Set(1).To_Curr_Attribute    := Null;
   gXAM_Set(1).To_Curr_Indicator    := Null;
   gXAM_Set(1).To_IC_Currency       := Null;
   gXAM_Set(1).From_IC_Currency     := Null;
   gXAM_Set(1).From_IC_Unit_Price   := Null;
   gXAM_Set(1).From_IC_Price        := Null;
   gXAM_Set(1).IC_Exch_Rate         := Null;
   gXAM_Set(1).Pricing_Calculation  := Null;

   vExchange_Date := cExchange_Date;

   If cShared_Pricing = 'Y' then
     vShared_D1002_Array   := xxcp_wks.d1002_array;
   End If;

   -- Pricing Method
   IF cCreate_IC_Trx = 'Y' THEN
     cInternalErrorCode := 3098; -- This is set to zero if a row is returned
   END IF;

   FOR PMRec IN PM(pPrice_Method_id => cPrice_Method_id) LOOP

      cInternalErrorCode     := 0;
      cExchange_Rate_Type    := nvl(PMRec.Exchange_Rate_type, xxcp_global.gCommon_Lookups.Exchange_Rate_Type);

      vPricing_Interface_id  := PMRec.Dynamic_Interface_id;
      vDft_Diagnostics_code  := PMRec.Dft_Diagnostics_Code;
      vDiagnostics_code      := PMRec.Diagnostics_Code;
      
      gXAM_SET(1).Pricing_Calculation  := PMRec.Pricing_Calculation;
      gXAM_Set(1).Trade_Exch_Rate_Type := cExchange_Rate_Type; -- this is not just used for XAM
      -- xxxxx

      If nvl(PMRec.Dynamic_Interface_id, 0) <> 0 THEN
        If xxcp_global.gCommon(1).Custom_events = 'Y' then

           If xxcp_global.Timing_Adv_Mode = 'Y' then
              gTraceStartTime := systimestamp;
           End If;

           cInternalErrorCode :=
                      xxcp_custom_events.IC_PRICING_BINDS(cSource_id             => cSource_id,
                                                          cSource_Assignment_ID  => xxcp_global.gCommon(1).Current_Assignment_id,
                                                          cTransaction_Table     => xxcp_global.gCommon(1).Current_Transaction_Table,
                                                          cParent_Trx_id         => xxcp_global.gCommon(1).Current_Parent_Trx_id,
                                                          cSource_Rowid          => cSourceRowid,
                                                          cTransaction_Id        => cTransaction_Id,
                                                          cTransaction_Type      => xxcp_global.gCommon(1).Current_Transaction_Type,
                                                          cTrading_Set           => xxcp_global.gCommon(1).Current_Trading_Set,
                                                          cPricing_Type          => cPricing_Type,
                                                          -- OUT
                                                          cCustom_Bind1          => vCustom_Bind1,
                                                          cCustom_Bind2          => vCustom_Bind2,
                                                          cCustom_Bind3          => vCustom_Bind3,
                                                          cCustom_Bind4          => vCustom_Bind4,
                                                          cCustom_Bind5          => vCustom_Bind5,
                                                          cCustom_Bind6          => vCustom_Bind6,
                                                          cCustom_Bind7          => vCustom_Bind7,
                                                          cCustom_Bind8          => vCustom_Bind8
                                                          );

           If xxcp_global.Timing_Adv_Mode = 'Y' then
              gTraceEndTime := systimestamp;
              CustomEventTiming('IC Pricing Binds');
           End If;

           If nvl(cInternalErrorCode,0) > 0 then
             Exit; -- API has returned an error code
           End If;

        End If; -- End Custom Events

        xxcp_te_base.Curr_Conv_Set(cPricing_Method_id    => cPrice_Method_id,
                                   cOwner_Tax_Reg_id     => cOwnerTaxId,
                                   cPartner_Tax_Reg_id   => cPartnerTaxId,
                                   cTransaction_Currency => cTransaction_Currency,
                                   cPricing_Method_Type  => 'C',
                                   cAction               => 'Z');

        xxcp_dynamic_sql.Non_Configurator( cActivity_Name          => 'I.C. Pricing',
                                           cRule_type              => 'N', -- No rule type
                                           cSet_of_books_id        => cSet_of_books_id,
                                           cSourceRowid            => cSourceRowid,
                                           cTransaction_Id         => cTransaction_Id,
                                           cTransaction_Table      => xxcp_global.gCommon(1).Current_Transaction_Table,
                                           -- 02.06.09
                                           cTransaction_class      => xxcp_global.gCommon(1).Current_Transaction_class,
                                           cTrading_Set            => xxcp_global.gCommon(1).Current_Trading_Set,
                                           cActivity_id            => 1002,
                                           cDynamic_Interface_id   => PMRec.Dynamic_Interface_id,
                                           cOwner                  => cOwnerTaxid,
                                           cPartner                => cPartnerTaxId,
                                           cKey                    => cKey,
                                           cPrevious_Attribute_id  => cPrevious_Attribute_id,
                                           cCustom_Bind1           => vCustom_Bind1,
                                           cCustom_Bind2           => vCustom_Bind2,
                                           cCustom_Bind3           => vCustom_Bind3,
                                           cCustom_Bind4           => vCustom_Bind4,
                                           cCustom_Bind5           => vCustom_Bind5,
                                           cCustom_Bind6           => vCustom_Bind6,
                                           cCustom_Bind7           => vCustom_Bind7,
                                           cCustom_Bind8           => vCustom_Bind8,
                                           cWhen_DI_Not_Found      => PMRec.When_DI_Not_Found,
                                           cWhen_DI_Too_Many_Found => PMRec.When_DI_Too_Many_Found,
                                           cResults_Pointer        => vD1002_Pointers,
                                           cInternalErrorCode      => cInternalErrorCode);

            xxcp_wks.D1002_Array := xxcp_wks.DYNAMIC_RESULTS;
            cD1002_Pointers      := vD1002_Pointers;

      End If;

      If cInternalErrorCode > 0 then
        -- Dynamic Interface failed (But continue specified)
        If PMRec.When_DI_Not_Found = 'C' and cInternalErrorCode = 2304 then
          cInternalErrorCode  := 0;
        ElsIf PMRec.When_DI_Too_Many_Found = 'C' and cInternalErrorCode = 2305 then
          cInternalErrorCode  := 0;
        ElsIf PMRec.When_DI_Not_Found = 'S' and (cInternalErrorCode in (2304,2305)) then
          cInternalErrorCode  := 3097;
        End If;
      End If;

      --
      -- Formulas
      If cInternalErrorCode = 0 then
          If PMRec.Formula_Set_Id > 0 then
             vFormula_Set_id := PMRec.Formula_Set_Id;
             cInternalErrorCode := xxcp_te_formulas.Call_Formula( cSource_id        => cSource_id,
                                                                  cFormula_Set_id   => PMRec.Formula_Set_Id,
                                                                  cFormula_Pointers => gFormula_Pointers_1002,
                                                                  cDynamic_Array    => xxcp_wks.D1002_Array);

             If  cInternalErrorCode > 0 then
                If PMRec.When_Formula_Set_Fails = 'C' then
                  cInternalErrorCode  := 0;
                ElsIf PMRec.When_DI_Not_Found = 'S' then
                  cInternalErrorCode  := 3097;
                ElsIf PMRec.When_DI_Not_Found = 'D' then
                  vDefault_Value_Required := 'Y'; -- Do Default value logic below
                End If;
             End If;
          End If;
      End If;

      IF cInternalErrorCode = 0 THEN
        vTo_Curr_indicator := PMRec.To_Curr_indicator;
        IF PMRec.To_Curr_Attribute_id IS NOT NULL THEN
          vTo_Curr_Attribute :=  StandardPriceAttribute(cShared_Pricing, PMRec.To_Curr_Attribute_id);
        END IF;

        vIC_UNIT_PRICE     :=  StandardPriceAttribute(cShared_Pricing ,PMRec.BASE_VAL_ATTRIBUTE_ID);
        vIC_CURRENCY       :=  StandardPriceAttribute(cShared_Pricing ,PMRec.BASE_CURR_ATTRIBUTE_ID);

        IF PMRec.Exchange_Date_Attribute_id IS NOT NULL THEN
          vExchange_Date :=  StandardPriceAttribute(cShared_Pricing ,PMRec.EXCHANGE_DATE_ATTRIBUTE_ID);
          IF vExchange_Date IS NOT NULL THEN
            cExchange_Date := vExchange_Date;
          END IF;
        END IF;
        --
        If PMRec.Given_Adjustment_Rate_id is not null then
          vGiven_Adjustment_Rate :=  StandardPriceAttribute(cShared_Pricing ,PMRec.Given_Adjustment_Rate_id);
        End If;

        IF PMRec.Rate_set_id IS NULL THEN
          cAdjustment_Rate_id := 0;
          cAdjustment_Rate    := NULL;
          If vGiven_Adjustment_Rate is not null then
            cAdjustment_Rate := vGiven_Adjustment_Rate;
          End If;
        ELSE
           --------------------------------------------------------------------------------
           -- Fetch the Adjustment Rate
           --------------------------------------------------------------------------------
           cAdjustment_Rate_id := NULL;
           cInternalErrorCode  := 3081;
           cADJ_Qualifier1     :=  StandardPriceAttribute(cShared_Pricing ,PMRec.OWNER_ATTRIBUTE_ID);
           cADJ_Qualifier2     :=  StandardPriceAttribute(cShared_Pricing ,PMRec.PARTNER_ATTRIBUTE_ID);
           cADJ_QUALIFIER3     :=  StandardPriceAttribute(cShared_Pricing ,PMRec.ARQC1_ATTRIBUTE_ID);
           cADJ_QUALIFIER4     :=  StandardPriceAttribute(cShared_Pricing ,PMRec.ARQC2_ATTRIBUTE_ID);
           vADJ_RATE_DATE      :=  StandardPriceAttribute(cShared_Pricing ,PMRec.ADJ_DATE_ATTRIBUTE_ID);

           If vADJ_RATE_DATE is null then
             vAdj_Rate_Date := cTransaction_Date;
           End If;

           FindAdjustmentRates(       cRate_Set_id           => PMRec.Rate_set_id,
                                      cQualifier1            => cADJ_QUALIFIER1,
                                      cQualifier2            => cADJ_QUALIFIER2,
                                      cQualifier3            => cADJ_QUALIFIER3,
                                      cQualifier4            => cADJ_QUALIFIER4,
                                      cTransaction_Date      => vAdj_Rate_Date,
                                      cGiven_Adjustment_Rate => vGiven_Adjustment_Rate,
                                      cAdjustment_Rate_id    => cAdjustment_Rate_id,
                                      cAdjustment_Rate       => cAdjustment_Rate,
                                      cInternalErrorCode     => cInternalErrorCode);

           -- Default Adjustment Rate not found to Zero.
           If cInternalErrorCode = 3081 then
             If PMRec.When_No_Adjustment_Rate = 'C' then
               cAdjustment_Rate   := 0;
               cInternalErrorCode := 0; -- Set adjustment to Zero when not found.
             ElsIf PMRec.When_No_Adjustment_Rate = 'S' then
               cAdjustment_Rate   := 0;
               cInternalErrorCode := 3097;
             ElsIf PMRec.When_No_Adjustment_Rate = 'D' then
               cAdjustment_Rate    := Null;
               cAdjustment_Rate_id := Null;
               cInternalErrorCode  := 0;
               vDefault_Why_Code   := 3081; -- Why it would have failed.
               vDefault_Value_Required  := 'Y'; -- Do Default value logic below
             End If;
           End If;

          END IF;

          -- IC Pricing Method
          If vIC_Unit_Price = 0 then
            If PMRec.When_Base_Val_Zero = 'F'  then
              cInternalErrorCode := 3084;
            ElsIf PMRec.When_Base_Val_Zero = 'S'  then
              cInternalErrorCode := 3097;
            ElsIf PMRec.When_Base_Val_Zero = 'D' then
              vDefault_Why_Code := 3084; -- Why it would have failed.
              vDefault_Value_Required := 'Y'; -- Do Default value logic below
            End If;
          End If;

        Else -- Dynamic Interface failed
          If cInternalErrorCode = 2304 And PMRec.When_DI_Not_Found = 'D' then
            vDefault_Why_Code   := 2304; -- Why it would have failed.
            cInternalErrorCode  := 0;
            vDefault_Value_Required := 'Y'; -- Do Default value logic below
          ElsIf cInternalErrorCode = 2304 And PMRec.When_DI_Not_Found = 'S' then
            cInternalErrorCode := 3097;
          Elsif cInternalErrorCode = 2305 And PMRec.When_DI_Too_Many_Found = 'D' then
            vDefault_Why_Code   := 2305; -- Why it would have failed.
            cInternalErrorCode  := 0;
            vDefault_Value_Required := 'Y'; -- Do Default value logic below
          ElsIf cInternalErrorCode = 2305 And PMRec.When_DI_Not_Found = 'S' then
            cInternalErrorCode := 3097;
          End If;
        End If;


        IF cInternalErrorCode > 0 and cInternalErrorCode != 3097 THEN
          vIC_Unit_Price := Null;
          vIC_Currency   := Null;
          vIC_Price      := 0;

          If NOT cInternalErrorCode in (3071,3072,3073) Then
             xxcp_foundation.FndWriteError(cInternalErrorCode, 'Rate Set Id <' ||TO_CHAR(PMRec.Rate_set_id) ||
                                              '> Qualifier 1 <' ||cADJ_QUALIFIER1 ||'> Qualifier 2 <' ||cADJ_QUALIFIER2 ||
                                              '> Qualifier 3 <' ||cADJ_QUALIFIER3 ||'> Qualifer 4 <' ||cADJ_QUALIFIER4 ||
                                              '> Transaction Date <' ||TO_CHAR(cTransaction_Date) ||'> Adjustment Date <'||vAdj_Rate_Date||
                                              '> Pricing Method ID <' ||TO_CHAR(cPrice_Method_id) ||'> Pricing Method SQL <' ||TO_CHAR(PMRec.Dynamic_Interface_id) ||
                                              '> Owner Tax Reg <' ||TO_CHAR(cOwnerTaxid) ||'> Partner Tax Reg <' ||TO_CHAR(cPartnerTaxId) || '>');
          End If;
        Else
          -- Pricing Calculation
          If (cInternalErrorCode = 0 And PMRec.Pricing_Calculation is not null) And vDefault_Value_Required = 'N' Then

            vPricing_Calculation := PMRec.Pricing_Calculation;
            vFrom_IC_Currency    := vIC_CURRENCY;

            gXAM_Set(1).From_IC_Currency   := vFrom_IC_Currency;
            gXAM_Set(1).From_IC_Unit_Price := vIC_Unit_Price;
            gXAM_Set(1).From_IC_Price      := vIC_Price;

  -- ksp18022010 Start
            If vTo_Curr_indicator IS NOT NULL Then
                vTo_IC_Currency :=
                   xxcp_foundation.Trade_Indicator_Translation(cTrade_Curr_Indicator    => vTo_Curr_Indicator,
                                                               cTransaction_Currency    => cTransaction_Currency,
                                                               cOwner_Legal_Currency    => gXAM_Set(1).Owner_Legal_Currency,
                                                               cOwner_Tax_Reg           => cOwnerTaxid,
                                                               cPartner_Legal_Currency  => gXAM_Set(1).Partner_Legal_Currency,
                                                               cPartner_Tax_Reg         => cPartnerTaxId,
                                                               cOwner_Trade_Currency    => gXAM_Set(1).Owner_Trade_Currency,
                                                               cPartner_Trade_Currency  => gXAM_Set(1).Partner_Trade_Currency,
                                                               cTo_Curr_Attribute       => vTo_Curr_Attribute,
                                                               cIC_Unit_Currency        => NULL);

            Else
                vTo_IC_Currency := cTransaction_Currency;
            End If;

            vIC_Currency := vTo_IC_Currency;  -- IC Currency

            --
            -- Pricing Control: Calculate IC Price now that the core values are worked out.
            --
            If nvl(xxcp_global.gCommon(1).Direct_Currencies,'Y') = 'Y' then
              Case PMRec.Pricing_Precision_Type
                When 'S' then vPrecision := xxcp_foundation.Get_CurrencyPrecision(vTo_IC_Currency,2,'N');
                When 'E' then vPrecision := xxcp_foundation.Get_CurrencyPrecision(vTo_IC_Currency,2,'Y');
              Else
                vPrecision := 10;
              End Case;
            Else
              Case PMRec.Pricing_Precision_Type
                When 'S' then vPrecision := xxcp_foundation.Get_CurrencyPrecision_Memory(vTo_IC_Currency,2,'N');
                When 'E' then vPrecision := xxcp_foundation.Get_CurrencyPrecision_Memory(vTo_IC_Currency,2,'Y');
              Else
                vPrecision := 10;
              End Case;
            End if;
            --
            -- Currency Conversion
            --
            If (vFrom_IC_Currency IS NOT NULL AND vTo_IC_Currency IS NOT NULL) Then
                IF vFrom_IC_Currency = vTo_IC_Currency THEN
                  vIC_Exch_Rate := 1;
                ELSE
                  vIC_Exch_Rate :=
                     xxcp_foundation.Currency_Conversions(cTransaction_Currency     => vFrom_IC_Currency,
                                                          cTag                      => 'IC Currency',
                                                          cExchange_Currency        => vTo_IC_Currency,
                                                          cDefault_Common_Exch_Curr => gXAM_Set(1).Dflt_CommExchCurr,
                                                          cExchange_Date            => vExchange_Date,
                                                          cExchange_Type            => gXAM_Set(1).Trade_Exch_Rate_Type,
                                                          cOnFailure_ErrorCode      => 3028,
                                                          cCommon_Exch_Currency     => gXAM_Set(1).Partner_Common_Exch_Curr,
                                                          cInternalErrorCode        => cInternalErrorCode);
               END IF;

               vIC_Unit_Price := NVL(vIC_Unit_Price, 0) *  NVL(vIC_Exch_Rate, 0);
               vIC_Price      := vIC_Unit_Price;
            Else  -- New
               vIC_Unit_Price  := vIC_Unit_Price;
               vTo_IC_Currency := vFrom_IC_Currency;
            End If;

            vIC_Unit_Price := Round(vIC_Unit_Price,vPrecision);

            -- ksp18022010 End
            --
            -- Pricing Calculation
            --
            If (vDefault_Value_Required = 'N' and cInternalErrorCode = 0) Then
              vIC_Price := xxcp_foundation.FndEnteredValueCalculation(cRule_Value_Name        => PMRec.Pricing_Calculation,
                                                                      cEntered_Value          => vIC_Unit_Price,
                                                                      cAdjustment_Rate        => cAdjustment_Rate,
                                                                      cTax_Rate               => cTax_Rate,
                                                                      cChangeSign             => 'N',
                                                                      cTransaction_Quantity   => cQuantity,
                                                                      cPrecision              => vPrecision,
                                                                      cExtended_Entered_Value => vIC_Price_Extended,
                                                                      cInternalErrorCode      => cInternalErrorCode);

               If PMRec.Pricing_Extended = 'Y' then
                  vIC_Price := nvl(vIC_Price_Extended,vIC_Price);
               End If;

               vIC_Price      := Round(vIC_Price,vPrecision);
             End If;
             --
             -- End Pricing Calculation
             --

             -- Not Zero on IC Price
             If vIC_Price = 0 and PMRec.When_IC_Price_Zero = 'D' then
               vDefault_Value_Required := 'Y';
               vDefault_Why_Code := 3085; -- Why it would have failed.
             End If;
             End If;
            --
            -- Default Value
            --
            If (vDefault_Value_Required = 'Y' and cInternalErrorCode = 0) Then
                -- Assign Default Value
                cAdjustment_Rate_id  := Null;
                cAdjustment_Rate     := Null;
                vPM_Default_Value    := StandardPriceAttribute(cShared_Pricing ,PMRec.DFT_BASE_VAL_ATTRIBUTE_ID);
                vPM_Default_Currency := StandardPriceAttribute(cShared_Pricing ,PMRec.DFT_BASE_CURR_ATTRIBUTE_ID);
                vIC_Unit_Price       := vPM_Default_Value;
                vIC_Price            := vIC_Unit_Price;

                gXAM_Set(1).From_IC_Currency   := vPM_Default_Currency;
                gXAM_Set(1).From_IC_Unit_Price := vIC_Unit_Price;
                gXAM_Set(1).From_IC_Price      := vIC_Price;

                -- Currency Translation
                IF vTo_Curr_indicator IS NOT NULL THEN
                 vTo_IC_Currency :=
                   xxcp_foundation.Trade_Indicator_Translation(cTrade_Curr_Indicator    => vTo_Curr_Indicator,
                                                               cTransaction_Currency    => cTransaction_Currency,
                                                               cOwner_Legal_Currency    => gXAM_Set(1).Owner_Legal_Currency,
                                                               cOwner_Tax_Reg           => cOwnerTaxid,
                                                               cPartner_Legal_Currency  => gXAM_Set(1).Partner_Legal_Currency,
                                                               cPartner_Tax_Reg         => cPartnerTaxId,
                                                               cOwner_Trade_Currency    => gXAM_Set(1).Owner_Trade_Currency,
                                                               cPartner_Trade_Currency  => gXAM_Set(1).Partner_Trade_Currency,
                                                               cTo_Curr_Attribute       => vTo_Curr_Attribute,
                                                               cIC_Unit_Currency        => NULL);

                 ELSE
                  vTo_IC_Currency := cTransaction_Currency;
                 END IF;
                 -- Precision
                 IF nvl(xxcp_global.gCommon(1).Direct_Currencies,'Y') = 'Y' Then
                  Case PMRec.Pricing_Precision_Type
                    When 'S' then vPrecision := xxcp_foundation.Get_CurrencyPrecision(vTo_IC_Currency,2,'N');
                    When 'E' then vPrecision := xxcp_foundation.Get_CurrencyPrecision(vTo_IC_Currency,2,'Y');
                  Else
                    vPrecision := 10;
                  End Case;
                 ELSE
                  Case PMRec.Pricing_Precision_Type
                    When 'S' then vPrecision := xxcp_foundation.Get_CurrencyPrecision_Memory(vTo_IC_Currency,2,'N');
                    When 'E' then vPrecision := xxcp_foundation.Get_CurrencyPrecision_Memory(vTo_IC_Currency,2,'Y');
                  Else
                    vPrecision := 10;
                  End Case;
                 END IF;
                  -- Default
                 If vTo_IC_Currency = vPM_Default_Currency then
                   vIC_Exch_Rate := 1;
                  Else
                    vIC_Exch_Rate :=
                     xxcp_foundation.Currency_Conversions(cTransaction_Currency     => vPM_Default_Currency,
                                                          cTag                      => 'IC Currency',
                                                          cExchange_Currency        => vTo_IC_Currency,
                                                          cDefault_Common_Exch_Curr => gXAM_Set(1).Dflt_CommExchCurr,
                                                          cExchange_Date            => vExchange_Date,
                                                          cExchange_Type            => gXAM_Set(1).Trade_Exch_Rate_Type,
                                                          cOnFailure_ErrorCode      => 3029,
                                                          cCommon_Exch_Currency     => gXAM_Set(1).Partner_Common_Exch_Curr,
                                                          cInternalErrorCode        => cInternalErrorCode);

                  End If;
                  -- Default Price
                  vIC_Unit_Price := vIC_Unit_Price * vIC_Exch_Rate;
                  vIC_Unit_Price := Round(vIC_Unit_Price , vPrecision);
                  vIC_Price      := vIC_Unit_Price;
                  vIC_Currency   := vTo_IC_Currency;
                  xxcp_global.set_diagnostics(vDft_Diagnostics_code);

                  If PMRec.Dft_Base_Val_Extended = 'Y' then
                   vIC_Price := vIC_Price * cQuantity;
                   vIC_Price := Round(vIC_Price , vPrecision);
                  End If;
                  -- Not Zero on Default
                  If PMRec.Dft_Base_Val_Not_Zero = 'Y'and vIC_Price = 0 then
                   cInternalErrorCode := 3086;
                  End If;
                --
            ElsIf nvl(vIC_Price,0) = 0 and PMRec.When_IC_Price_Zero = 'F' then
                cInternalErrorCode := 3085;
            ElsIf nvl(vIC_Price,0) = 0 and PMRec.When_IC_Price_Zero = 'S' then
                cInternalErrorCode := 3097;
            End If;
         End If;

         EXIT;

    END LOOP;

   If cInternalErrorCode = 3098 Then
     -- Pricing Method Error Message
     xxcp_foundation.FndWriteError(cInternalErrorCode,'Pricing Method Id <' || TO_CHAR(cPrice_Method_id) || '>');
   End If;

   -- Rearrange for Cache
   If cShared_Pricing = 'Y' and vD1002_Pointers is not null then
      vD1002_Pointers := Null;
      For Rec in MX(pSql_Build_id => vPricing_Interface_id)
      Loop
        vShared_D1002_Array(rec.to_attribute_id-2000) := xxcp_wks.d1002_array(rec.from_attribute_id-2000);
        vD1002_Pointers := vD1002_Pointers || to_char(rec.to_attribute_id);
      End Loop;
      xxcp_wks.D1002_array := vShared_D1002_Array;
   End If;

   cD1002_Pointers        := vD1002_Pointers;
   cDiagnostics_code      := vDiagnostics_code;

   cIC_Unit_Price         := vIC_Unit_Price;
   cIC_Currency           := vIC_Currency;
   cIC_Price              := vIC_Price;

   gXAM_Set(1).IC_Exch_Rate      := vIC_Exch_Rate;
   gXAM_Set(1).To_Curr_Indicator := vTo_Curr_indicator;

   -- Ensure values have a currency
   If nvl(cIC_Price,0) != 0 And cIC_Currency is null then
     cInternalErrorCode := 3096;
   End If;

   If cInternalErrorCode != 3097 then
        xxcp_trace.ASSIGNMENT_TRACE(
                                       cTrace_id               => 13,
                                       cPricing_Type           => cPricing_Type,
                                       cPRICE_METHOD_ID        => cPrice_Method_id,
                                       cIC_PRICING_SQL_ID      => vPricing_Interface_id,
                                       cIC_UNIT_PRICE          => cIC_Unit_Price,
                                       cIC_CURRENCY            => cIC_Currency,
                                       cIC_Price               => cIC_Price,
                                       cExchange_Date          => cExchange_Date,
                                       cAdjustment_Rate_Date   => vAdj_Rate_Date,
                                       cTransaction_Date       => cTransaction_Date,
                                       cTrade_Curr_Indicator   => vTo_Curr_indicator,
                                       cPM_Default_Used        => vDefault_Value_Required,
                                       cPM_Default_Value       => vPM_Default_Value,
                                       cPM_Default_Currency    => vPM_Default_Currency,
                                       cPM_Pricing_Calculation => vPricing_Calculation,
                                       cTrade_Exch_Rate_Type   => cExchange_Rate_Type,
                                       cFormula_Set_id         => vFormula_Set_id
                                       );
   Else
      xxcp_wks.d1002_array  := temp_1002_array;
   End If;


  End Pricing_Method;


  -- *******************************************************************
  --                    PRICING LADDER
  -- *******************************************************************
  PROCEDURE Pricing_Ladder(cSource_id              IN Number,
                           cPricing_Type           IN VARCHAR,
                           cLadder_Set_Id          IN Number,
                           cShared_Pricing         IN varchar2,
                           cLadder_Leg             IN Number,
                           cSourceRowid            IN ROWID,
                           cTransaction_id         IN NUMBER,
                           cOwnerTaxId             IN VARCHAR2,
                           cPartnerTaxId           IN VARCHAR2,
                           cTransaction_Currency   IN VARCHAR2,
                           cKey                    IN VARCHAR2,
                           cSet_of_books_id        IN NUMBER,
                           cPrevious_Attribute_id  IN NUMBER,
                           cTransaction_Date       IN DATE,
                           cCreate_IC_Trx          IN VARCHAR2,
                           cQuantity               IN NUMBER,
                           cTax_Rate               IN NUMBER,
                           cLadder_Seq             IN OUT Number,
                           cUsed_Price_Method_id   OUT NUMBER,
                           cNext_Pricing_Set_Id    OUT NUMBER,
                           cADJ_Qualifier1         OUT VARCHAR2,
                           cADJ_Qualifier2         OUT VARCHAR2,
                           cADJ_Qualifier3         OUT VARCHAR2,
                           cADJ_Qualifier4         OUT VARCHAR2,
                           cIC_Unit_Price          OUT VARCHAR2,
                           cIC_Currency            OUT VARCHAR2,
                           cIC_Price               OUT VARCHAR2,
                           cDiagnostics_code       OUT VARCHAR2,
                           cD1002_Pointers         OUT VARCHAR2,
                           cAdjustment_Rate_id     OUT NUMBER,
                           cAdjustment_Rate        OUT NUMBER,
                           cExchange_Rate_Type     OUT VARCHAR2,
                           cExchange_Date          IN OUT DATE,
                           cInternalErrorCode      IN OUT NUMBER
                           ) is

    vLadder_Found Boolean := False;

    vLadder_match_req varchar2(1) := 'N';

    Cursor LadCtl(pSource_id in number, pLadder_Set_id in number) is
     select gr.qualifier_group_id,gr.Qualifier1_id,gr.Qualifier2_id,gr.Qualifier3_id,gr.Qualifier4_id, t.attribute3 ladder_match_req
      from xxcp_sys_qualifier_groups gr,
           xxcp_table_set_ctl t
     where t.source_id        = decode(cShared_Pricing,'Y',40,pSource_id)
       and t.set_id           = pLadder_Set_id
       and t.attribute1       = '1001'
       and t.set_base_table   = 'CP_PRICING_LADDER'
       and t.enabled_flag     = 'Y'
       and gr.qualifier_usage = 'L'
       and gr.qualifier_group_id = t.attribute4;

    Cursor Lad(pLadder_Set_id in number, pSource_id in number, pTransaction_Date in Date, pLadder_Seq in number,
               pQualifier1 in varchar2, pQualifier2 in varchar2, pQualifier3 in varchar2, pQualifier4 in varchar2
              ) is
     select nvl(s.price_method_id,0) Price_Method_id, nvl(s.next_pricing_ladder_id,0) Next_Ladder_Set_id, s.seq,
            s.pl_qualifier1, s.pl_qualifier2, s.pl_qualifier3, s.pl_qualifier4
      from xxcp_pricing_ladder s
     where s.source_id        = decode(cShared_Pricing,'Y',40,pSource_id)
       and s.ladder_set_id    = pLadder_Set_id
       and s.activity_id      = 1001
       and s.seq              > pLadder_Seq
       and nvl(s.PL_qualifier1, '~null~') =
             DECODE(s.PL_Qualifier1,'*',nvl(s.PL_Qualifier1, '~null~'),nvl(pQualifier1, '~null~'))
         and nvl(s.PL_Qualifier2, '~null~') =
             DECODE(s.PL_Qualifier2,'*',nvl(s.PL_Qualifier2, '~null~'),nvl(pQualifier2, '~null~'))
         and nvl(s.PL_Qualifier3, '~null~') =
             DECODE(s.PL_Qualifier3,'*',nvl(s.PL_Qualifier3, '~null~'),nvl(pQualifier3, '~null~'))
         and nvl(s.PL_Qualifier4, '~null~') =
             DECODE(s.PL_Qualifier4,'*',nvl(s.PL_Qualifier4, '~null~'),nvl(pQualifier4, '~null~'))
       and pTransaction_Date between NVL(s.effective_from_date, '01-JAN-1900') and NVL(s.effective_to_date, '31-DEC-2999')
       order by s.seq;

    vQualifier1   xxcp_pricing_ladder.pl_qualifier1%Type;
    vQualifier2   xxcp_pricing_ladder.pl_qualifier1%Type;
    vQualifier3   xxcp_pricing_ladder.pl_qualifier1%Type;
    vQualifier4   xxcp_pricing_ladder.pl_qualifier1%Type;

    vInternalErrorCode xxcp_errors.internal_error_code%type;

  Begin
     xxcp_trace.ASSIGNMENT_TRACE(cTrace_id  => 19, cPRICE_METHOD_ID => cLadder_Set_id);

     If gLastLadderSetId != 0 and gLastLadderSetId = cLadder_Set_id then
         vQualifier1 := StandardPriceAttribute(cShared_Pricing ,gLadderQualifiers.qualifier1_id);
         vQualifier2 := StandardPriceAttribute(cShared_Pricing ,gLadderQualifiers.qualifier2_id);
         vQualifier3 := StandardPriceAttribute(cShared_Pricing ,gLadderQualifiers.qualifier3_id);
         vQualifier4 := StandardPriceAttribute(cShared_Pricing ,gLadderQualifiers.qualifier4_id);
     Else
         -- Pricing Ladder Ctl
       vInternalErrorCode := 3103;
       For Rec in LadCtl(pSource_id => cSource_id,  pLadder_Set_id => cLadder_Set_id) Loop
         vInternalErrorCode := 0;

         vQualifier1 := StandardPriceAttribute(cShared_Pricing ,Rec.Qualifier1_id);
         vQualifier2 := StandardPriceAttribute(cShared_Pricing ,Rec.Qualifier2_id);
         vQualifier3 := StandardPriceAttribute(cShared_Pricing ,Rec.Qualifier3_id);
         vQualifier4 := StandardPriceAttribute(cShared_Pricing ,Rec.Qualifier4_id);

         vLadder_match_req := Rec.ladder_match_req;

         gLadderQualifiers.qualifier1_id := Rec.Qualifier1_id;
         gLadderQualifiers.qualifier2_id := Rec.Qualifier2_id;
         gLadderQualifiers.qualifier3_id := Rec.Qualifier3_id;
         gLadderQualifiers.qualifier4_id := Rec.Qualifier4_id;
       End Loop;
     End If;

     cNext_Pricing_Set_id := 0;
     xxcp_trace.ASSIGNMENT_TRACE(cTrace_id  => 22, cPC_Qualifier1 => vQualifier1, cPC_Qualifier2 => vQualifier2, cPC_Qualifier3 => vQualifier3, cPC_Qualifier4 => vQualifier4 );

     If vInternalErrorCode = 0 then
      -- Pricing Ladder
      For Rec2 in Lad(pSource_id => cSource_id, pLadder_Set_id => cLadder_Set_id,
                      pTransaction_Date => cTransaction_Date, pLadder_Seq => cLadder_Seq,
                      pQualifier1 => vQualifier1, pQualifier2 => vQualifier2, pQualifier3 => vQualifier3, pQualifier4 => vQualifier4)
      Loop

        vLadder_match_req := 'F';
        gLadder_Set(cLadder_Leg).Prior_Ladder_Seq := Rec2.Seq;

        If Rec2.Price_Method_Id > 0 then
               cUsed_Price_Method_id  := Rec2.Price_Method_Id;
               cLadder_Seq            := Rec2.Seq;
               vLadder_Found          := True;
               cNext_Pricing_Set_id   := 0;


               Pricing_Method(
                           cPrice_Method_id        => cUsed_Price_Method_id,
                           cSource_id              => cSource_id,
                           cPricing_Type        => cPricing_Type,
                           cShared_Pricing         => cShared_Pricing,
                           cSourceRowid            => cSourceRowid,
                           cTransaction_id         => cTransaction_id,
                           cOwnerTaxId             => cOwnerTaxid,
                           cPartnerTaxId           => cPartnerTaxId,
                           cTransaction_Currency   => cTransaction_Currency,
                           cKey                    => cKey,
                           cSet_of_books_id        => cSet_of_books_id,
                           cPrevious_Attribute_id  => cPrevious_Attribute_id,
                           cTransaction_Date       => cTransaction_Date,
                           cCreate_IC_Trx          => cCreate_IC_trx,
                           cTax_Rate               => cTax_Rate,
                           cQuantity               => cQuantity,
                           cADJ_QUALIFIER1         => cADJ_QUALIFIER1,
                           cADJ_QUALIFIER2         => cADJ_QUALIFIER2,
                           cADJ_Qualifier3         => cADJ_Qualifier3,
                           cADJ_Qualifier4         => cADJ_Qualifier4,
                           cIC_Unit_Price          => cIC_Unit_Price,
                           cIC_Currency            => cIC_Currency,
                           cIC_Price               => cIC_Price,
                           cAdjustment_Rate_id     => cAdjustment_Rate_id,
                           cAdjustment_Rate        => cAdjustment_Rate,
                           cExchange_Date          => cExchange_Date,
                           cD1002_Pointers         => cD1002_Pointers,
                           cExchange_Rate_Type     => cExchange_Rate_Type,
                           cDiagnostics_code       => cDiagnostics_code,
                           cInternalErrorCode      => cInternalErrorCode
                          );

        Else
          cNext_Pricing_Set_id  := Rec2.Next_Ladder_Set_Id;
          cUsed_Price_Method_id := 0;
          cLadder_Seq := 0;
          Exit;
        End If;

        xxcp_trace.ASSIGNMENT_TRACE(cTrace_id  => 20, cPRICE_METHOD_ID => Rec2.Price_Method_Id, cLadder_Set_id => Rec2.Next_Ladder_Set_Id, cSeq => Rec2.Seq, cSource_id => cSource_id);

        If cInternalErrorCode != 3097 then -- do next one
          Exit;
        End If;

      End Loop;
     End If;

     If Not vLadder_Found and cNext_Pricing_Set_id = 0 then
       xxcp_trace.ASSIGNMENT_TRACE(cTrace_id  => 100, cMessage => '  No Matches Found');
       If vLadder_match_req = 'Y' then
         cInternalErrorCode := 3102;
       End If;
     End If;

  End Pricing_Ladder;

  -- *******************************************************************
  --                    PRICING CONTROL
  -- *******************************************************************
  PROCEDURE IC_Pricing( cSourceRowId               IN ROWID,
                        cTransaction_id            IN NUMBER,
                        cTransaction_Set_id        IN NUMBER,
                        cTransaction_Date          IN DATE,
                        cTrading_Set_id            IN NUMBER,
                        cOwnerTaxid                IN NUMBER,
                        cPartnerTaxId              IN NUMBER,
                        cSet_of_books_id           IN NUMBER,
                        cCreate_IC_trx             IN VARCHAR2,
                        cCreate_Tax_Results        IN VARCHAR2,
                        cKey                       IN VARCHAR2,
                        cQuantity                  IN NUMBER,
                        cMC_Qualifier1             IN VARCHAR2,
                        cMC_Qualifier2             IN VARCHAR2,
                        cMC_Qualifier3             IN VARCHAR2,
                        cMC_Qualifier4             IN VARCHAR2,
                        cTAX_Qualifier1            IN VARCHAR2,
                        cTAX_Qualifier2            IN VARCHAR2,
                        cPrevious_Attribute_id     IN NUMBER,
                        cTransaction_Currency      IN VARCHAR2,
                        cDflt_Trading_Relationship IN VARCHAR2,
                        -- Out
                        cD1002_Pointers           OUT VARCHAR2,
                        cADJ_Qualifier3           OUT VARCHAR2,
                        cADJ_Qualifier4           OUT VARCHAR2,
                        cIC_Unit_Price            OUT VARCHAR2,
                        cIC_Currency              OUT VARCHAR2,
                        cIC_Price                 OUT VARCHAR2,
                        cPrice_Method_id          OUT NUMBER,
                        cIC_Control               OUT VARCHAR2,
                        cAdjustment_Rate_id       OUT NUMBER,
                        cAdjustment_Rate          OUT NUMBER,
                        cTax_Code_id              OUT NUMBER,
                        cTax_Rate                 OUT NUMBER,
                        cExchange_Rate_Type       OUT VARCHAR2,
                        cExchange_Date         IN OUT DATE,
                        cInternalErrorCode     IN OUT NUMBER) IS


  Type vD1002_Array_Type is Record(
     D1002_Array  xxcp_dynamic_array
   );

  TYPE vD1002_Array_REC    IS TABLE OF vD1002_Array_Type    INDEX BY BINARY_INTEGER;

  vD1002_Array_Tab          vD1002_Array_REC;

    -- Pricing Type zero
  Type vStd_IC_Price_Type IS RECORD(
     Pricing_Ctl_Found        boolean,
     D1002_Pointers           varchar2(900),
     Pricing_Method_id        xxcp_transaction_attributes.price_method_id%type,
     Exchange_Date            xxcp_transaction_attributes.exchange_date%type,
     Exchange_Rate_Type       xxcp_tax_registrations.exchange_rate_type%type,
     IC_Unit_Price            xxcp_transaction_attributes.ic_unit_price%type,
     IC_Unit_Price_Extended   xxcp_transaction_attributes.ic_unit_price%type,
     IC_Price                 xxcp_transaction_attributes.ic_unit_price%type,
     IC_Price_Extended        xxcp_transaction_attributes.ic_unit_price%type,
     IC_Currency              xxcp_transaction_attributes.ic_currency%type,
     Adjustment_Rate          xxcp_transaction_attributes.adjustment_rate%type,
     Adjustment_Rate_id       xxcp_transaction_attributes.adjustment_rate_id%type,
     Tax_Rate                 xxcp_transaction_attributes.ic_tax_rate%type,
     IC_Control               xxcp_transaction_attributes.ic_control%type,
     Tax_Code_id              xxcp_transaction_attributes.ic_trade_tax_id%type,
     Tax_Qualifier1           xxcp_transaction_attributes.tax_qualifier1%type,
     Tax_Qualifier2           xxcp_transaction_attributes.tax_qualifier2%type,
     ADJ_Qualifier1           xxcp_transaction_attributes.tc_qualifier1%type,
     ADJ_Qualifier2           xxcp_transaction_attributes.tc_qualifier1%type,
     ADJ_Qualifier3           xxcp_transaction_attributes.tc_qualifier1%type,
     ADJ_Qualifier4           xxcp_transaction_attributes.tc_qualifier1%type,
     MC_Qualifier1            xxcp_transaction_attributes.mc_qualifier1%type,
     MC_Qualifier2            xxcp_transaction_attributes.mc_qualifier2%type,
     MC_Qualifier3            xxcp_transaction_attributes.mc_qualifier3%type,
     MC_Qualifier4            xxcp_transaction_attributes.mc_qualifier4%type
     );

    TYPE vStd_IC_Price_Type_REC    IS TABLE OF vStd_IC_Price_Type    INDEX BY BINARY_INTEGER;

    vStd_IC_Pricing          vStd_IC_Price_Type_REC;
    vBuild_D1002_Pointers    varchar2(100);

    CURSOR PT(pSource_id in number, pTransaction_Set_id IN NUMBER) is
      Select pt.pricing_type,
             pt.pricing_type_id,
             pt.unit_price_attribute_id,
             pt.currency_attribute_id,
             pt.price_attribute_id,
             pt.adj_rate_attribute_id,
             pt.tax_rate_attribute_id,
             pt.seq
      from xxcp_pricing_types pt
        where Source_id          = pSource_id
          and Transaction_Set_id = pTransaction_Set_id
         Order by pt.seq;

    CURSOR CC(pTransaction_Date IN Date, pTransaction_Set_id IN NUMBER, pTrading_Set_id IN NUMBER, pEntity_Partnership_id IN NUMBER, pPricing_Type_id in NUMBER) IS
      select
             nvl(cc.config_price_method_id,0) price_method_id,
             cc.config_price_method_type,
             cc.mc_qualifier1,
             cc.mc_qualifier2,
             nvl(cc.tax_code_indicator,'N') Tax_Code_Indicator,
             substr(cc.ic_control,1,1) ic_control,
             cc.configuration_ctl_id,
             Decode(cc.config_price_method_type,'D','N','L','N','Y') Shared_Pricing,
             cc.rank,
             cc.pricing_type_id
        from xxcp_configuration_ctl cc,
             xxcp_sys_qualifier_groups mq
       WHERE mq.source_id           = xxcp_global.gCommon(1).Source_id
         AND mq.qualifier_usage     = 'P'
         AND cc.config_ctl_group_id = mq.qualifier_group_id
         AND cc.transaction_set_id  = pTransaction_set_id
         AND cc.trading_set_id      = pTrading_set_id
         AND cc.entity_set_id       = pEntity_Partnership_id
         AND cc.pricing_type_id     = pPricing_type_id
         --
         AND nvl(cc.MC_Qualifier1, '~null~') =
             DECODE(cc.MC_Qualifier1,
                    '*',
                    nvl(cc.MC_Qualifier1, '~null~'),
                    nvl(xxcp_foundation.MC_DynamicAttribute(mq.Qualifier1_id), '~null~'))
         AND nvl(cc.MC_Qualifier2, '~null~') =
             DECODE(cc.MC_Qualifier2,
                    '*',
                    nvl(cc.MC_Qualifier2, '~null~'),
                    nvl(xxcp_foundation.MC_DynamicAttribute(mq.Qualifier2_id), '~null~'))
         AND nvl(cc.MC_Qualifier3, '~null~') =
             DECODE(cc.MC_Qualifier3,
                    '*',
                    nvl(cc.MC_Qualifier3, '~null~'),
                    nvl(xxcp_foundation.MC_DynamicAttribute(mq.Qualifier3_id), '~null~'))
         AND nvl(cc.MC_Qualifier4, '~null~') =
             DECODE(cc.MC_Qualifier4,
                    '*',
                    nvl(cc.MC_Qualifier4, '~null~'),
                    nvl(xxcp_foundation.MC_DynamicAttribute(mq.Qualifier4_id), '~null~'))
       AND cc.Active  = 'Y'
         AND pTransaction_Date between NVL(cc.effective_from_date, '01-JAN-1900') AND NVL(cc.effective_to_date, '31-DEC-2999')
       ORDER BY cc.rank,nvl(config_price_method_id, 0) DESC;

    vPrice_Method_ID         xxcp_pricing_method.price_method_id%Type;
    vPrice_Method_Used       xxcp_pricing_method.price_method_id%Type;
    vADJ_QUALIFIER1          varchar2(250);
    vADJ_QUALIFIER2          varchar2(250);
    vEntity_Partnership_id   xxcp_entity_partnerships.entity_set_id%Type;


    x                         pls_Integer;
    vDefault_Value_Required   varchar2(1) := 'N';
    vDiagnostics_code         varchar2(3);
    vDefault_Why_Code         xxcp_internal_codes.error_code%Type := 0;
    vPricing_Ctl_Row_Found    boolean := False;
    vTax_Code_Indicator       varchar2(1);
    vPricingExit              boolean := False;
    vExchange_Rate_Type       xxcp_pricing_method.exchange_rate_type%Type;
    vNextPricing_Set_id       xxcp_pricing_ladder.pricing_ladder_id%Type;
    vLadder_Leg               pls_integer;
    vLadder_Seq               pls_integer;
    vShared_Pricing           varchar2(1) := 'N';
    vD1002_Array_Cnt          number := 0;
    

  BEGIN

    xxcp_wks.Reset_D1002;

    cIC_Control := 'N';
    xxcp_wks.I1009_Array(1) := cOwnerTaxid;
    xxcp_wks.I1009_Array(2) := cPartnerTaxId;
    vExchange_Rate_Type     := xxcp_global.gCommon_Lookups.Exchange_Rate_Type;
    -- These
    vStd_IC_Pricing(1).Pricing_Method_id  := 0;
    vStd_IC_Pricing(1).Pricing_ctl_Found  := False;
    vStd_IC_Pricing(1).Exchange_Rate_Type := vExchange_Rate_Type;
    vStd_IC_Pricing(1).IC_Control         := cIC_Control;

    IF nvl(cInternalErrorCode, 0) = 0 THEN
      
      -- Work out the Trading Relationship
      cInternalErrorCode := 
         xxcp_te_base.Get_Trading_Relationship(
                                    cAction_Type               => 'A',
                                    cSource_id                 => xxcp_global.gCOMMON(1).Source_id,
                                    cOwner_Tax_Reg_id          => cOwnerTaxid, 
                                    cPartner_Tax_Reg_Id        => cPartnerTaxId,
                                    cTransaction_Date          => cTransaction_Date,
                                    cCreate_IC_Trx             => cCreate_IC_trx,
                                    cDflt_Trading_Relationship => cDflt_Trading_Relationship,
                                    cEntity_Partnership_id     => vEntity_Partnership_id);
                                    

     IF cInternalErrorCode <> 3094 THEN
       -- Pricing Type
       For PTRec in PT(pSource_id => xxcp_global.gCOMMON(1).Source_id, pTransaction_Set_id => cTransaction_Set_id) Loop

        vPricing_Ctl_Row_Found := False;
        -- Get Configuration Control - GET first Match within Pricing Type!!
        For CCRec in CC(pTransaction_Date      => cTransaction_Date,
                        pTransaction_Set_id    => cTransaction_Set_id,
                        pTrading_Set_id        => cTrading_Set_id,
                        pEntity_Partnership_id => vEntity_Partnership_id,
                        pPricing_Type_Id       => ptRec.Pricing_Type_Id
                       ) Loop

            vPricing_Ctl_Row_Found := True;

            vTax_Code_Indicator := CCRec.Tax_Code_Indicator;
            vPrice_Method_id    := CCRec.Price_Method_Id;
            gLastICPrice        := CCRec.Configuration_Ctl_Id;
            cPrice_Method_id    := vPrice_Method_Id;
            cIC_Control         := CCRec.IC_Control;
            vShared_Pricing     := CCRec.Shared_Pricing;

            cInternalErrorCode  := 0;

            xxcp_trace.ASSIGNMENT_TRACE( cTrace_id              => 12,
                                         cPricing_Type          => ptRec.Pricing_Type,
                                         cConfig_Ctl_id         => CCRec.Configuration_Ctl_id,
                                         cIC_Control            => cIC_Control,
                                         cTax_Code_Indicator    => vTax_Code_Indicator,
                                         cEntity_Partnership_id => vEntity_Partnership_id);

            -- Tax Coding
            If ccRec.Tax_Code_Indicator != 'None' and cCreate_Tax_Results = 'N' then
                 Tax_Coding (
                        cSourceRowid           => cSourceRowid,
                        cTransaction_id        => cTransaction_id,
                        cTransaction_Date      => cTransaction_Date,
                        cOwnerTaxid            => cOwnerTaxid,
                        cPartnerTaxId          => cPartnerTaxId,
                        cTAX_Qualifier1        => cTAX_Qualifier1,
                        cTAX_Qualifier2        => cTAX_Qualifier2,
                        cTax_Code_Indicator    => vTax_Code_Indicator,
                        cIC_Control            => cIC_Control,
                        cTax_Code_id           => cTax_Code_id,
                        cTax_Rate              => cTax_Rate,
                        cInternalErrorCode     => cInternalErrorCode);
               If cInternalErrorCode != 0 then         
                  vPricingExit := True;         
                  Exit;
               End If;
            End If;

            IF CCRec.Config_price_method_type = 'D' or CCRec.Config_price_method_type = 'SD' then
               -- Standard Pricing Method
               Pricing_Method(
                           cPrice_Method_id        => vPrice_Method_id,
                           cSource_id              => xxcp_global.gCOMMON(1).Source_id,
                           cPricing_Type           => ptRec.Pricing_Type,
                           cShared_Pricing         => vShared_Pricing,
                           cSourceRowid            => cSourceRowid,
                           cTransaction_id         => cTransaction_id,
                           cOwnerTaxId             => cOwnerTaxid,
                           cPartnerTaxId           => cPartnerTaxId,
                           cTransaction_Currency   => cTransaction_Currency,
                           cKey                    => cKey,
                           cSet_of_books_id        => cSet_of_books_id,
                           cPrevious_Attribute_id  => cPrevious_Attribute_id,
                           cTransaction_Date       => cTransaction_Date,
                           cCreate_IC_Trx          => cCreate_IC_trx,
                           cTax_Rate               => cTax_Rate,
                           cQuantity               => cQuantity,
                           cADJ_QUALIFIER1         => vADJ_QUALIFIER1,
                           cADJ_QUALIFIER2         => vADJ_QUALIFIER2,
                           cADJ_Qualifier3         => cADJ_Qualifier3,
                           cADJ_Qualifier4         => cADJ_Qualifier4,
                           cIC_Unit_Price          => cIC_Unit_Price,
                           cIC_Currency            => cIC_Currency,
                           cIC_Price               => cIC_Price,
                           cAdjustment_Rate_id     => cAdjustment_Rate_id,
                           cAdjustment_Rate        => cAdjustment_Rate,
                           cExchange_Date          => cExchange_Date,
                           cD1002_Pointers         => cD1002_Pointers,
                           cExchange_Rate_Type     => vExchange_Rate_Type,
                           cDiagnostics_code       => vDiagnostics_code,
                           cInternalErrorCode      => cInternalErrorCode
                          );

                  If cInternalErrorCode != 3097 then
                     vPricingExit := True;
                  Else
                    xxcp_foundation.FndWriteError(cInternalErrorCode,'Pricing Method <'||xxcp_translations.Pricing_Method_Name(vPrice_Method_id)||'>');
                  End If;

               ElsIf ccRec.config_price_method_type = 'L' or ccRec.config_price_method_type = 'SL' then -- Ladder

                 gLadder_Set.Delete;
                 vPrice_Method_id := ccRec.Price_Method_Id;

                 vLadder_Leg  := 1;
                 gLadder_Set(1).Prior_Ladder_Seq    := 0;
                 gLadder_Set(1).Ladder_Set_id       := vPrice_Method_id;
                 gLadder_Set(1).Prior_Ladder_Set_id := 0;

                 For x in 1..gMaxPricingLadder Loop

                   If x = gMaxPricingLadder or vPrice_Method_id = 0 then
                     xxcp_foundation.FndWriteError(3091,'No Pricing Found');
                     Exit;
                   End If;

                   vLadder_Seq  := gLadder_Set(vLadder_Leg).prior_Ladder_Seq;

                   Pricing_Ladder(
                           cSource_id              => xxcp_global.gCommon(1).Source_id ,
                           cPricing_Type           => ptRec.Pricing_Type,
                           cLadder_Set_Id          => vPrice_Method_id,
                           cLadder_Leg             => vLadder_Leg,
                           cShared_Pricing         => vShared_Pricing,
                           cSourceRowid            => cSourceRowid,
                           cTransaction_id         => cTransaction_id,
                           cOwnerTaxId             => cOwnerTaxid,
                           cPartnerTaxId           => cPartnerTaxId,
                           cTransaction_Currency   => cTransaction_Currency,
                           cKey                    => cKey,
                           cSet_of_books_id        => cSet_of_books_id,
                           cPrevious_Attribute_id  => cPrevious_Attribute_id,
                           cTransaction_Date       => cTransaction_Date,
                           cCreate_IC_Trx          => cCreate_IC_trx,
                           cTax_Rate               => cTax_Rate,
                           cQuantity               => cQuantity,
                           cNext_Pricing_Set_id    => vNextPricing_Set_id,
                           cLadder_Seq             => vLadder_Seq,
                           cUsed_Price_Method_id   => vPrice_Method_Used,
                           cADJ_QUALIFIER1         => vADJ_QUALIFIER1,
                           cADJ_QUALIFIER2         => vADJ_QUALIFIER2,
                           cADJ_Qualifier3         => cADJ_Qualifier3,
                           cADJ_Qualifier4         => cADJ_Qualifier4,
                           cIC_Unit_Price          => cIC_Unit_Price,
                           cIC_Currency            => cIC_Currency,
                           cIC_Price               => cIC_Price,
                           cAdjustment_Rate_id     => cAdjustment_Rate_id,
                           cAdjustment_Rate        => cAdjustment_Rate,
                           cExchange_Date          => cExchange_Date,
                           cD1002_Pointers         => cD1002_Pointers,
                           cExchange_Rate_Type     => vExchange_Rate_Type,
                           cDiagnostics_code       => vDiagnostics_code,
                           cInternalErrorCode      => cInternalErrorCode
                          );

                          If cInternalErrorCode != 3097 then
                            vPricingExit := True;
                            Exit;
                          ElsIf vNextPricing_Set_id > 0 then -- going down a level
                            vLadder_Leg := vLadder_Leg +1;
                            gLadder_Set(vLadder_Leg).Ladder_Set_id       := vNextPricing_Set_id;
                            gLadder_Set(vLadder_Leg).Prior_Ladder_Set_id := vPrice_Method_id;
                            gLadder_Set(vLadder_Leg).Prior_Ladder_Seq    := 0;
                            vPrice_Method_id := vNextPricing_Set_id;
                          ElsIf vNextPricing_Set_id = 0 then -- coming up a level
                            vPrice_Method_id := gLadder_Set(vLadder_Leg).prior_ladder_set_id;
                            vLadder_Seq      := gLadder_Set(vLadder_Leg).prior_Ladder_Seq;
                            vLadder_Leg      := vLadder_leg - 1;
                            If vLadder_leg < 1 then
                              Exit;
                            End If;
                          End If;
                   End Loop;

                   vPrice_Method_id := vPrice_Method_Used;

              End If;
              -- Diagnostics
              If vDefault_Value_Required != 'Y' and cInternalErrorCode = 0 then
                xxcp_global.set_diagnostics(vDiagnostics_code);
              End If;
              -- ksp
              vD1002_Array_Cnt := vD1002_Array_Cnt + 1;
              vD1002_Array_Tab(vD1002_Array_Cnt).D1002_Array := xxcp_wks.d1002_array;

              -- Pricing Type Store for Zero
              If ptRec.Pricing_Type_Id = 0 then
                vStd_IC_Pricing(1).Pricing_ctl_Found  := vPricing_Ctl_Row_Found;
                vStd_IC_Pricing(1).IC_Unit_Price      := cIC_Unit_Price;
                vStd_IC_Pricing(1).IC_Currency        := cIC_Currency;
                vStd_IC_Pricing(1).IC_Price           := cIC_Price;
                vStd_IC_Pricing(1).Adjustment_Rate    := cAdjustment_Rate;
                vStd_IC_Pricing(1).Adjustment_Rate_id := cAdjustment_Rate_id;
                vStd_IC_Pricing(1).Pricing_Method_id  := vPrice_Method_id;
                vStd_IC_Pricing(1).Exchange_Rate_Type := vExchange_Rate_Type;
                vStd_IC_Pricing(1).Tax_Rate           := cTax_Rate;
                vStd_IC_Pricing(1).Tax_Code_Id        := cTax_Code_id;
                vStd_IC_Pricing(1).D1002_Pointers     := vStd_IC_Pricing(1).D1002_Pointers||cD1002_Pointers;
                vStd_IC_Pricing(1).ADJ_Qualifier1     := vADJ_QUALIFIER1;
                vStd_IC_Pricing(1).ADJ_Qualifier2     := vADJ_QUALIFIER2;
                vStd_IC_Pricing(1).ADJ_Qualifier3     := cADJ_QUALIFIER3;
                vStd_IC_Pricing(1).ADJ_Qualifier4     := cADJ_QUALIFIER4;
                vStd_IC_Pricing(1).Exchange_Date      := cExchange_Date;
                vStd_IC_Pricing(1).IC_Control         := cIC_Control;
                -- Internal
                xxcp_wks.I1009_Array(30) := cIC_Currency;
                xxcp_wks.I1009_Array(31) := cIC_Unit_Price;
                xxcp_wks.I1009_Array(102):= cIC_Price;

              ElsIf ptRec.Pricing_Type_Id > 0 then
                -- None Standard Pricing Types
                vBuild_D1002_Pointers := Null;
                If ptRec.unit_price_attribute_id between 2001 and 2999 then
                  xxcp_wks.d1002_array(ptRec.unit_price_attribute_id-2000) := cIC_Unit_Price;
                  vBuild_D1002_Pointers := vBuild_D1002_Pointers ||to_char(ptRec.unit_price_attribute_id);
                End If;
                If ptRec.currency_attribute_id between 2001 and 2999 then
                  xxcp_wks.d1002_array(ptRec.currency_attribute_id-2000) := cIC_Currency;
                  vBuild_D1002_Pointers := vBuild_D1002_Pointers ||to_char(ptRec.currency_attribute_id);
                End If;
                If ptRec.price_attribute_id between 2001 and 2999 then
                  xxcp_wks.d1002_array(ptRec.price_attribute_id-2000) := cIC_Price;
                  vBuild_D1002_Pointers := vBuild_D1002_Pointers ||to_char(ptRec.price_attribute_id);
                End If;
                If ptRec.adj_rate_attribute_id between 2001 and 2999 then
                  xxcp_wks.d1002_array(ptRec.adj_rate_attribute_id-2000) := cAdjustment_Rate;
                  vBuild_D1002_Pointers := vBuild_D1002_Pointers ||to_char(ptRec.adj_rate_attribute_id);
                End If;
                If ptRec.tax_rate_attribute_id between 2001 and 2999 then
                  xxcp_wks.d1002_array(ptRec.tax_rate_attribute_id-2000) := cTax_Rate;
                  vBuild_D1002_Pointers := vBuild_D1002_Pointers ||to_char(ptRec.tax_rate_attribute_id);
                End If;
                vStd_IC_Pricing(1).D1002_Pointers := vStd_IC_Pricing(1).D1002_Pointers||vBuild_D1002_Pointers;
              End If;

              -- Pricing Sequencing
              If vPricingExit then
               Exit;
              End If;

        End Loop;  -- Pricing Control Cursor

        If vPricing_Ctl_Row_Found = TRUE then
                     
           xxcp_trace.ASSIGNMENT_TRACE(
                                 cTrace_id               => 14,
                                 cIC_Control             => cIC_Control,
                                 cIC_Price               => cIC_Price,
                                 cQuantity               => cQuantity,
                                 cTax_Code_Indicator     => vTax_Code_Indicator,
                                 cTaxOwnerTaxid          => cOwnerTaxid,
                                 cTaxPartnerTaxId        => cPartnerTaxId,
                                 cTax_Qualifier1         => cTax_Qualifier1,
                                 cTax_Qualifier2         => cTax_Qualifier2,
                                 cTax_Rate               => cTax_Rate,
                                 cTax_Code_Id            => cTax_Code_Id,
                                 cDefault_Price_Why_Code => vDefault_Why_Code,
                                 cPricing_Calculation    => gXAM_Set(1).Pricing_Calculation
                                 );
        End If;

        If cInternalErrorCode = 3097 then
          xxcp_foundation.FndWriteError(cInternalErrorCode,'Pricing Method <'||xxcp_translations.Pricing_Method_Name(vPrice_Method_id)||'>');
        ElsIf cInternalErrorCode > 0 then
          Exit;
        End If;

      End Loop; -- Pricing Type
     End If;

     -- Return Values ** Investage
     cPrice_Method_id      := vPrice_Method_id;
     gLastDftPMused        := vDefault_Value_Required;
     -- Only Return values for Pricing Type zero are returned
     -- these are written on the transaction attributes table
     --
     cExchange_Rate_Type    := vStd_IC_Pricing(1).Exchange_Rate_Type;
     cIC_Unit_Price         := vStd_IC_Pricing(1).IC_Unit_Price;
     cIC_Currency           := vStd_IC_Pricing(1).IC_Currency;
     cIC_Price              := vStd_IC_Pricing(1).IC_Price;
     cPrice_Method_id       := vStd_IC_Pricing(1).Pricing_Method_id;
     cAdjustment_Rate       := vStd_IC_Pricing(1).Adjustment_Rate;
     cAdjustment_Rate_id    := vStd_IC_Pricing(1).Adjustment_Rate_id;
     cIC_Control            := vStd_IC_Pricing(1).IC_Control;
     cTax_Code_id           := vStd_IC_Pricing(1).Tax_Code_id;
     cTax_Rate              := vStd_IC_Pricing(1).Tax_Rate;
     cD1002_Pointers        := vStd_IC_Pricing(1).D1002_Pointers;
     cADJ_Qualifier3        := vStd_IC_Pricing(1).ADJ_Qualifier3;
     cADJ_Qualifier4        := vStd_IC_Pricing(1).ADJ_Qualifier4;

     gLastICPrice           := cIC_Price;
     gLastICCurrency        := cIC_Currency;
     gLastICPMID            := cPrice_Method_id;

    -- End Only Return values for Pricing Type zero

     IF cInternalErrorCode IN (3092,3094,3096) THEN
      IF Not vStd_IC_Pricing(1).Pricing_ctl_Found Then
        xxcp_trace.ASSIGNMENT_TRACE(cTrace_id => 11);
      End If;

      xxcp_foundation.FndWriteError(cInternalErrorCode,
                                   'Transaction Set <' ||TO_CHAR(ctransaction_set_id) ||
                                    '> Trading Relationship <'||TO_CHAR(vEntity_Partnership_id)||
                                    '> Trading Set <' ||xxcp_global.gCommon(1).Current_trading_set ||
                                    '> Owner <' ||TO_CHAR(NVL(cOwnerTaxid, 0)) ||
                                    '> Partner <' ||TO_CHAR(NVL(cPartnerTaxId, 0)) ||
                                    '> Q1 <'||cMC_Qualifier1||'> Q2 <'||cMC_Qualifier2||
                                    '> Q3 <'||cMC_Qualifier3||'> Q4 <'||cMC_Qualifier4||'>');

     ElsIf vShared_Pricing = 'N' then  -- not Sharing
       NULL;
     End If;
    End If;
  END IC_Pricing;
  
  -- *******************************************************************
  --                   Assignment
  -- *******************************************************************
  PROCEDURE Assignment( cRefresh               IN VARCHAR2,
                        cCreate                IN VARCHAR2,
                        cAttribute_id          IN NUMBER,
                        cAttributeRowid        IN ROWID,
                        cAttributeFrozen       IN VARCHAR2,
                        cSource_Assignment_id  IN NUMBER,
                        cSet_of_books_id       IN NUMBER,
                        cTrading_Set_id        IN NUMBER,
                        cSource_Table_id       IN NUMBER,
                        cSource_Class_id       IN VARCHAR2,
                        cSource_Type_id        IN VARCHAR2,
                        cTransaction_Id        IN NUMBER,
                        cTransaction_Ref1      IN VARCHAR2,
                        cTransaction_Ref2      IN VARCHAR2,
                        cTransaction_Ref3      IN VARCHAR2,
                        cTransaction_Date      IN DATE,
                        cParent_Trx_Id         IN NUMBER,
                        cQuantity              IN NUMBER,
                        cUOM                   IN VARCHAR2,
                        cTransaction_Currency  IN OUT VARCHAR2,
                        cExchange_Date         IN DATE,
                        cD1001_Pointers        IN VARCHAR2,
                        --
                        cCreate_IC_Trx         IN VARCHAR2,
                        cCreate_Tax_Results    IN VARCHAR2,
                        cMultiple_Owners       IN VARCHAR2,
                        cAssignment_set_id     IN NUMBER,
                        cAssign_Qualifier_id   IN NUMBER,
                        cTransaction_set_id    IN NUMBER,
                        --
                        cDflt_CommExchCurr     IN VARCHAR2,
                        cStep_Number           IN VARCHAR2,
                        cKey                   IN VARCHAR2,
                        cPrevious_Attribute_id IN NUMBER,
                        cSourceRowid           IN ROWID,
                        cOLS_Date              IN DATE,
                        cOLS_Flag              IN VARCHAR2,
                        cPass_Through_set_id   IN NUMBER,
                        -- NCM 02.05.04
                        cDflt_Trading_Relationship IN VARCHAR2,
                        cRef_Ctl_id            IN NUMBER,
                        -- KSP
                        cFormula_Set_Id        IN NUMBER,
                        --
                        cMC_Qualifier1         IN OUT  VARCHAR2,
                        cMC_Qualifier2         IN OUT  VARCHAR2,
                        cMC_Qualifier3         IN OUT  VARCHAR2,
                        cMC_Qualifier4         IN OUT  VARCHAR2,
                        cED_Qualifier1         IN OUT VARCHAR2,
                        cED_Qualifier2         IN OUT VARCHAR2,
                        cED_Qualifier3         IN OUT VARCHAR2,
                        cED_Qualifier4         IN OUT VARCHAR2,
                        cTAX_Qualifier1        IN OUT VARCHAR2,
                        cTAX_Qualifier2        IN OUT VARCHAR2,
                        --
                        cHeaderRowid          OUT ROWID,
                        cNew_Attribute_id     OUT NUMBER,
                        cInternalErrorCode IN OUT NUMBER) IS

    y                         pls_integer := 0;
    vFrozen                   xxcp_transaction_attributes.frozen%Type;
    vTrade_Exch_Rate_Type     xxcp_pricing_method.exchange_rate_type%Type;
    vD1002_Pointers           Varchar2(800);
    vHeaderRowid              ROWID;
    vAttributeRowid           ROWID;
    vAdjustment_Rate_id       xxcp_transaction_attributes.adjustment_rate_id%Type;
    vAdjustment_Rate          xxcp_transaction_attributes.adjustment_rate%Type;
    vTransaction_Currency     xxcp_transaction_header.transaction_currency%Type;
    vTransaction_exch_rate    xxcp_transaction_header.owner_legal_exch_rate%Type;
    vOwner_Company_Number     xxcp_tax_registrations.company_number%Type;
    vPartner_Company_Number   xxcp_tax_registrations.company_number%Type;
    -- Tax Codes
    vTax_Code_Id              xxcp_tax_codes.ic_trad_tax_id%Type;
    vTax_Rate                 xxcp_tax_codes.ar_tax_rate%Type;
    -- Owner
    vOwnerTaxId               xxcp_transaction_header.owner_tax_reg_id%Type;
    cMultipleOwnerTaxRegId    xxcp_transaction_header.owner_tax_reg_id%Type;
    vOwner_Association_Id     xxcp_transaction_header.owner_assign_rule_id%Type;
    vOwner_Assignment_Rule    xxcp_transaction_header.owner_assign_rule_id%Type;
    vOwner_Com_Exch_Curr      xxcp_transaction_header.transaction_currency%Type;
    vOwner_Legal_Currency     xxcp_transaction_header.transaction_currency%Type;
    vOwner_Exch_Rate_Type     xxcp_pricing_method.exchange_rate_type%Type;
    vOwner_Trade_Currency     xxcp_transaction_header.transaction_currency%Type;
    vOwner_Legal_Exch_Rate    xxcp_transaction_header.transaction_exch_rate%Type;
    vPartnerTaxId             xxcp_transaction_attributes.partner_tax_reg_id%Type;
    vPartner_Association_Id   xxcp_transaction_attributes.partner_assign_rule_id%Type;
    vPartner_Common_Exch_Curr xxcp_transaction_header.transaction_currency%Type;
    vPartner_Legal_Currency   xxcp_transaction_attributes.partner_legal_curr%Type;
    vPartner_Trade_Currency   xxcp_transaction_attributes.partner_legal_curr%Type;
    vPartner_Exch_Rate_Type   xxcp_model_ctl.exchange_rate_type%Type;
    vPartner_Legal_Exch_Rate  xxcp_transaction_attributes.partner_legal_exch_rate%Type;
    vPartner_Assignment_Rule  xxcp_transaction_attributes.partner_assoc_id%Type;
    vRecordAttributeAction    varchar2(1); -- Either Insert, Update or Null (No Action)
    vRecordHeaderAction       varchar2(1); -- Owner Either Insert, Update or Null (No Action)
    vHeaderFrozen             xxcp_transaction_attributes.frozen%Type;
    vIC_Currency              xxcp_transaction_attributes.ic_currency%Type;
    vIC_Unit_Price            xxcp_transaction_attributes.ic_unit_price%Type;
    vIC_Price                 xxcp_transaction_attributes.ic_price%Type := 0;
    vExchange_Date            xxcp_transaction_attributes.exchange_date%Type;
    vDocument_Currency        xxcp_transaction_header.transaction_currency%Type;
    vDocument_Exch_Rate       xxcp_transaction_attributes.document_exch_rate%Type;
    vPrice_Method_ID          xxcp_pricing_method.price_method_id%Type;
    vFetched_Trx_Currency     xxcp_transaction_header.transaction_currency%Type;
    vADJ_Qualifier1           xxcp_transaction_attributes.tc_qualifier1%Type;
    vADJ_Qualifier2           xxcp_transaction_attributes.tc_qualifier2%Type;
    vHeader_ID                xxcp_transaction_attributes.header_id%Type;
    vNew_Attribute_id         xxcp_transaction_attributes.attribute_id%Type;
    vDefault_to_Owner         xxcp_assignment_rules.default_owner%Type;
    vOwner_Adj_group          xxcp_tax_registrations.adjustment_rate_group%Type;
    vOwner_Short_Code         xxcp_tax_registrations.short_code%Type;
    vPartner_Adj_group        xxcp_tax_registrations.adjustment_rate_group%Type;
    vPartner_Short_Code       xxcp_tax_registrations.short_code%Type;
    vIC_Control               xxcp_configuration_ctl.ic_control%Type;
    -- Owner
    vPTO_Entity_id            xxcp_trading_entities.entity_id%Type := 0;
    vPTO_Tax_Authority        xxcp_tax_registrations.tax_authority%Type;
    vPTO_Tax_Reg_Attribute1   xxcp_tax_registrations.attribute1%Type;
    -- Partner
    vPTP_Entity_id            xxcp_trading_entities.entity_id%Type := 0;
    vPTP_Tax_Authority        xxcp_tax_registrations.tax_authority%Type;
    vPTP_Tax_Reg_Attribute1   xxcp_tax_registrations.attribute1%Type;
    
  BEGIN

    xxcp_wks.Reset_Internal_Array;
    -- 03.06.24 Could be populated in common.
    -- gFormula_Pointers_1001 := Null;
    gFormula_Pointers_1002 := Null;

    -- Added for Cost Plus
    xxcp_wks.I1009_Array  := xxcp_wks.I1009_ArrayGLOBAL;  -- 02.05.11
    vTrade_Exch_Rate_Type := xxcp_global.gCommon_Lookups.Exchange_Rate_Type;
    --
    vTransaction_Currency    := cTransaction_Currency;
    xxcp_wks.I1009_Array(10) := cSet_of_books_id;
    vExchange_Date           := cExchange_Date;

    xxcp_wks.I1009_Array(28) := xxcp_global.gCommon(1).Current_transaction_id;
    xxcp_wks.I1009_Array(38) := xxcp_global.gCommon(1).Current_transaction_table;
    xxcp_wks.I1009_Array(36) := xxcp_global.gCommon(1).Current_parent_trx_id;
    xxcp_wks.I1009_Array(38) := xxcp_global.gCommon(1).Current_Interface_id;
    xxcp_wks.I1009_Array(39) := xxcp_global.gCommon(1).Current_Transaction_Type;
    xxcp_wks.I1009_Array(40) := xxcp_global.gCommon(1).Current_Transaction_Class;

    If xxcp_global.gCommon(1).Explosion_id > 0 then
       xxcp_wks.I1009_Array(137) := xxcp_global.gCommon(1).Explosion_RowsFound;
       xxcp_wks.I1009_Array(138) := xxcp_global.gCommon(1).explosion_transaction_id;
       xxcp_wks.I1009_Array(139) := xxcp_global.gCommon(1).explosion_Rowid;
       xxcp_wks.I1009_Array(141) := xxcp_global.gCommon(1).Explosion_Summary1;
       xxcp_wks.I1009_Array(142) := xxcp_global.gCommon(1).Explosion_Summary2;
       xxcp_wks.I1009_Array(143) := xxcp_global.gCommon(1).Explosion_Summary3;
    End If;

    --
    -- Do not create records when no parent exists...
    --
    IF cParent_Trx_Id > 0 THEN
      --
      -- Process Transaction for each Trading Set..
      --
      vAttributeRowid := cAttributeRowid;
      vFrozen         := Substr(cAttributeFrozen,1,1);

      -- New Soft Engine Logic
      If xxcp_global.gCommon(1).Soft_Engine IN ('S','Y') Then
        vRecordHeaderAction    := 'I';
        vRecordAttributeAction := 'I';
      Else
        
        If cMultiple_Owners = 'Y' then
          
          vOwnerTaxId :=
               xxcp_foundation.FndGetTaxID(cEntity_Type             => 'Owner',
                                           cEffective_Date          => cTransaction_Date,
                                           cAssignment_set_id       => cAssignment_set_id,
                                           cAssignment_Qualifier_id => cAssign_Qualifier_id,
                                           cSourceErrorCode         => 3502,
                                           cAssociation_Id          => vOwner_Association_Id,
                                           cAssignment_Rule_id      => vOwner_Assignment_Rule,
                                           cDefault_to_Owner        => vDefault_to_Owner,
                                           cEntity_id               => vPTO_Entity_id,
                                           cTax_Authority           => vPTO_Tax_Authority,
                                           cTax_Reg_Attribute1      => vPTO_Tax_Reg_Attribute1,
                                           cCompany_Number          => vOwner_Company_Number,
                                           cSource_Rowid            => cSourceRowid
                                           );

           xxcp_wks.i1009_array(08) := vOwner_Company_Number;

           IF NVL(vOwnerTaxId, 0) = 0 THEN
              cInternalErrorCode := 3502;
           END IF;
        End If;
        
        If cInternalErrorCode = 0 then      
          cMultipleOwnerTaxRegId := vOwnerTaxId;
          -- Standard Engine Logic
          Table_Actions(
                    cSource_Table_id        => cSource_Table_id,
                    cTrading_Set_ID         => cTrading_Set_ID,
                    cTransaction_id         => cTransaction_id,
                    cSource_Type_id         => cSource_Type_id,
                    cParent_Trx_Id          => cParent_Trx_Id,
                    cMultiple_Owners        => cMultiple_Owners,
                    cMultipleOwnerTaxRegId  => cMultipleOwnerTaxRegId,
                    cAttributeRowid         => vAttributeRowid,
                    cCreate                 => cCreate,
                    cRefresh                => cRefresh,
                    cSource_Assignment_id   => cSource_Assignment_id,
                    cFrozen                 => vFrozen,
                    cOwnerTaxId             => vOwnerTaxId,
                    cHeaderFrozen           => vHeaderFrozen,
                    cHeaderRowid            => vHeaderRowid,
                    cRecordHeaderAction     => vRecordHeaderAction,
                    cRecordAttributeAction  => vRecordAttributeAction,
                    cTransaction_Currency   => vFetched_Trx_Currency, -- Transaction Currency from the Header
                    cHeader_ID              => vHeader_ID);

           -- new         
           If vOwnerTaxId = 0 then
             vOwnerTaxId := cMultipleOwnerTaxRegId;
           End If;         
         
           cHeaderRowid := vHeaderRowid;

         End If;
         
      End If;
      vTransaction_Currency := NVL(vFetched_Trx_Currency,vTransaction_Currency);

      IF ((vRecordAttributeAction IN ('I', 'U')) OR (vRecordHeaderAction IN ('I', 'U'))) THEN

        IF vRecordHeaderAction IN ('I', 'U') THEN

          -- ***************** Owner ONLY **************************
          -- Work out the Owner Tax Id using the Ladder logic..
          -- *******************************************************
          IF cInternalErrorCode = 0 and cMultiple_Owners = 'N' THEN

            vOwnerTaxId :=
               xxcp_foundation.FndGetTaxID(cEntity_Type             => 'Owner',
                                           cEffective_Date          => cTransaction_Date,
                                           cAssignment_set_id       => cAssignment_set_id,
                                           cAssignment_Qualifier_id => cAssign_Qualifier_id,
                                           cSourceErrorCode         => 3502,
                                           cAssociation_Id          => vOwner_Association_Id,
                                           cAssignment_Rule_id      => vOwner_Assignment_Rule,
                                           cDefault_to_Owner        => vDefault_to_Owner,
                                           cEntity_id               => vPTO_Entity_id,
                                           cTax_Authority           => vPTO_Tax_Authority,
                                           cTax_Reg_Attribute1      => vPTO_Tax_Reg_Attribute1,
                                           cCompany_Number          => vOwner_Company_Number,
                                           cSource_Rowid            => cSourceRowid
                                           );

            xxcp_wks.i1009_array(08) := vOwner_Company_Number;

            IF NVL(vOwnerTaxId, 0) = 0 THEN
              cInternalErrorCode := 3502;
            END IF;

          END IF;
        END IF;
                    
        -- **************************************************************
        -- ********************** END OWNER ONLY ************************
        -- **************************************************************

        IF cInternalErrorCode = 0 THEN
          -- ******* SET GLOBAL SOB/ALL CURRENCIES *******************
          cInternalErrorCode := 3503;
          FOR y IN 1 .. xxcp_global.gTRO_Cnt LOOP
            IF xxcp_global.gTRO(y).Tax_Registration_id = vOwnerTaxId THEN
              cInternalErrorCode    := 0;
              vOwner_Legal_Currency := xxcp_global.gTRO(y).Legal_Currency;
              vOwner_Trade_Currency := xxcp_global.gTRO(y).Trade_Currency;
              vOwner_Exch_Rate_Type := xxcp_global.gTRO(y).Exchange_Rate_Type;
              EXIT;
            END IF;
          END LOOP;

          IF cInternalErrorCode <> 0 THEN
            xxcp_foundation.FndWriteError(cInternalErrorCode,'Owner  <' ||TO_CHAR(vOwnerTaxId) || '>');
          END IF;
        END IF;
        --
        -- Set Internal Array
        --
        IF cInternalErrorCode = 0 THEN
          xxcp_wks.I1009_Array(1) := nvl(vOwnerTaxId, 0);
          xxcp_wks.I1009_Array(3) := vOwner_Legal_Currency;
        END IF;
        vPartnerTaxId             := vOwnerTaxId;
        xxcp_wks.I1009_Array(2) := vPartnerTaxId;
        -- *2
        IF cInternalErrorCode = 0 THEN
          IF cCreate_IC_Trx = 'Y' THEN
            --
            -- Work out the Partner using ladder logic.
            --
            vPartnerTaxId :=
               xxcp_foundation.FndGetTaxID(cEntity_Type             => 'Partner',
                                           cEffective_Date          => cTransaction_Date,
                                           cAssignment_set_id       => cAssignment_set_id,
                                           cAssignment_Qualifier_id => cAssign_Qualifier_id,
                                           cSourceErrorCode         => 3002,
                                           cAssociation_Id          => vPartner_Association_id,
                                           cAssignment_Rule_id      => vPartner_Assignment_Rule,
                                           cDefault_to_Owner        => vDefault_to_Owner,
                                           cEntity_id               => vPTP_Entity_id,
                                           cTax_Authority           => vPTP_Tax_Authority,
                                           cTax_Reg_Attribute1      => vPTP_Tax_Reg_Attribute1,
                                           cCompany_Number          => vPartner_Company_Number,
                                           cSource_Rowid            => cSourceRowid);

            xxcp_wks.i1009_array(09) := vPartner_Company_Number;
          ELSE
            vPartnerTaxId := vOwnerTaxId;
            xxcp_wks.i1009_array(09) := xxcp_wks.i1009_array(08);
          END IF;

          IF NVL(vPartnerTaxId, 0) = 0 THEN
            IF NVL(vDefault_to_owner, 'N') = 'Y' THEN
              vPartnerTaxId := vOwnerTaxId;
            ELSE
              xxcp_foundation.Set_InternalErrorCode(cInternalErrorCode,3002);
            END IF;
          END IF;

          xxcp_wks.I1009_Array(2) := vPartnerTaxId;

          -- Get Adjustment Group
          FOR y IN 1 .. xxcp_global.gTRO.COUNT LOOP
            IF xxcp_global.gTRO(y).Tax_Registration_id = vOwnerTaxId THEN
              vOwner_Adj_group  := xxcp_global.gTRO(y).Adjustment_Rate_Group;
              vOwner_Short_Code := xxcp_global.gTRO(y).Tax_Reg_Short_Code;
              EXIT;
            END IF;
          END LOOP;
          -- Get Adjustment Group if Owner <> Partner
          IF vOwnerTaxId = vPartnerTaxId THEN
            vPartner_Adj_group  := vOwner_Adj_group;
            vPartner_Short_Code := vOwner_Short_Code;
          ELSE
            FOR y IN 1 .. xxcp_global.gTRO.COUNT LOOP
              IF xxcp_global.gTRO(y).Tax_Registration_id = vPartnerTaxId THEN
                vPartner_Adj_group  := xxcp_global.gTRO(y).Adjustment_Rate_Group;
                vPartner_Short_Code := xxcp_global.gTRO(y).Tax_Reg_Short_Code;
                EXIT;
              END IF;
            END LOOP;
          END IF;

          xxcp_wks.I1009_Array(60)  := vOwner_Adj_Group;
          xxcp_wks.I1009_Array(61)  := vPartner_Adj_Group;
          xxcp_wks.I1009_Array(100) := vOwner_Short_Code;
          xxcp_wks.I1009_Array(101) := vPartner_Short_Code;

          IF cInternalErrorCode = 0 THEN
              IF NVL(vPartnerTaxId, 0) <> 0 THEN
                cInternalErrorCode := 3012;
                FOR y IN 1 .. xxcp_global.gTRO_Cnt LOOP
                  IF xxcp_global.gTRO(y).Tax_Registration_id = vPartnerTaxId Then
                    cInternalErrorCode        := 0;
                    vPartner_Legal_Currency   := xxcp_global.gTRO(y).Legal_Currency;
                    vPartner_Exch_Rate_Type   := xxcp_global.gTRO(y).Exchange_Rate_Type;
                    vPartner_Trade_Currency   := xxcp_global.gTRO(y).Trade_Currency;
                    vPartner_Common_Exch_Curr := NVL(xxcp_global.gTRO(y).common_exch_currency,cDflt_CommExchCurr);

                    xxcp_wks.I1009_Array(4) := vPartner_Legal_Currency;

                    EXIT;
                  END IF;
                END LOOP;

                IF (cInternalErrorCode <> 0 OR vPartner_Legal_Currency IS NULL) THEN
                  xxcp_foundation.Set_InternalErrorCode(cInternalErrorCode,3012);
                  xxcp_foundation.FndWriteError(cInternalErrorCode,'Partner  <' ||TO_CHAR(vPartnerTaxId) || '>');
                END IF;
              END IF; -- End Internal Error Check
            END IF; -- End PricipalTaxId is zero
           END IF;
          -- ksp18022010
          -- ksp06072011S
          If cInternalErrorCode = 0 and cFormula_Set_Id > 0 then
             cInternalErrorCode := xxcp_te_formulas.Call_Formula( cSource_id        => xxcp_global.gCommon(1).Source_id,
                                                                  cFormula_Set_id   => cFormula_Set_Id,
                                                                  cFormula_Pointers => gFormula_Pointers_1001,
                                                                  cDynamic_Array    => xxcp_wks.D1001_Array);
          End If;
          IF ((cInternalErrorCode = 0) AND (cPass_Through_set_id > 0)) THEN
            -- New Pass Through Logic.
             cInternalErrorCode :=
                       Pass_Through_Rule( cPass_Through_set_id   => cPass_Through_set_id,
                                          cTransaction_Date      => cTransaction_Date,
                                          cOwnerLegalEntity      => vPTO_Entity_id,
                                          cPartnerLegalEntity    => vPTP_Entity_id,
                                          cOwnerTaxId            => vOwnerTaxId,
                                          cPartnerTaxId          => vPartnerTaxId,
                                          cOwnerTaxAuthority     => vPTO_Tax_Authority,
                                          cPartnerTaxAuthority   => vPTP_Tax_Authority,
                                          cOwnerTaxAttribute1    => vPTO_Tax_Reg_Attribute1,
                                          cPartnerTaxAttribute1  => vPTP_Tax_Reg_Attribute1
                                          );
             If cInternalErrorCode <> 0 then  -- Suppress Header and Attributes
               vRecordHeaderAction    := 'P';
               vRecordAttributeAction := 'P';
             End if;
          END IF;


          If cInternalErrorCode = 0 And xxcp_global.gCommon(1).Custom_events = 'Y' then

             If xxcp_global.Timing_Adv_Mode = 'Y' then
               gTraceStartTime := systimestamp;
             End If;

             cInternalErrorCode :=
                  xxcp_custom_events.IC_PRICING_QUALIFIERS(cSource_id              => xxcp_global.gCommon(1).Source_id ,
                                                           cSource_Assignment_id   => cSource_Assignment_id ,
                                                           cTransaction_Date       => cTransaction_Date,
                                                           cTransaction_Table      => xxcp_global.gCommon(1).Current_Transaction_Table,
                                                           cParent_Trx_id          => cParent_Trx_Id,
                                                           cSource_Rowid           => cSourceRowid,
                                                           cTransaction_id         => cTransaction_Id,
                                                           cTransaction_type       => xxcp_global.gCommon(1).Current_Transaction_Type,
                                                           cTrading_Set            => xxcp_global.gCommon(1).Current_Trading_Set,
                                                           cOwner_Tax_Reg_Id       => vOwnerTaxId,
                                                           cOwner_Company_Number   => vOwner_Company_Number,
                                                           cPartner_Tax_Reg_id     => vPartnerTaxId,
                                                           cPartner_Company_Number => vPartner_Company_Number,
                                                           cPC_Qualifier1          => cMC_Qualifier1,
                                                           cPC_Qualifier2          => cMC_Qualifier2,
                                                           cPC_Qualifier3          => cMC_Qualifier3,
                                                           cPC_Qualifier4          => cMC_Qualifier4,
                                                           cTAX_Qualifier1         => cTAX_Qualifier1,
                                                           cTAX_Qualifier2         => cTAX_Qualifier2,
                                                           cED_Qualifier1          => cED_Qualifier1,
                                                           cED_Qualifier2          => cED_Qualifier2,
                                                           cED_Qualifier3          => cED_Qualifier3,
                                                           cED_Qualifier4          => cED_Qualifier4
                                                           );

           If xxcp_global.Timing_Adv_Mode = 'Y' then
              gTraceEndTime := systimestamp;
              CustomEventTiming('IC Pricing Qualifiers');
           End If;

            cInternalErrorCode := nvl(cInternalErrorCode,0);
          End If;

          xxcp_trace.ASSIGNMENT_TRACE(cTrace_id        => 18,
                                      cPC_Qualifier1   => cMC_Qualifier1,
                                      cPC_Qualifier2   => cMC_Qualifier2,
                                      cPC_Qualifier3   => cMC_Qualifier3,
                                      cPC_Qualifier4   => cMC_Qualifier4,
                                      cED_Qualifier1   => cED_Qualifier1,
                                      cED_Qualifier2   => cED_Qualifier2,
                                      cED_Qualifier3   => cED_Qualifier3,
                                      cED_Qualifier4   => cED_Qualifier4,
                                      cTAX_Qualifier1  => cTAX_Qualifier1,
                                      cTAX_Qualifier2  => cTAX_Qualifier2
                                      );



          IF cInternalErrorCode = 0 THEN
            IF vRecordHeaderAction IN ('I', 'U') THEN
                --
                -- Legal Owner Exchange Rate between the Owner's Legal Currency
                --
                IF cInternalErrorCode = 0 THEN
                  If xxcp_global.gCommon(1).Calc_Legal_Exch_Rate = 'N' then
                    vOwner_Legal_Exch_Rate := Null;
                  Else
                    -- Owner Legal
                    vOwner_Legal_Exch_Rate :=
                     xxcp_foundation.Currency_Conversions(cTransaction_Currency     => vTransaction_Currency,
                                                          cTag                      => 'Owner Legal',
                                                          cExchange_Currency        => vOwner_Legal_Currency,
                                                          cDefault_Common_Exch_Curr => cDflt_CommExchCurr,
                                                          cExchange_Date            => vExchange_Date,
                                                          cExchange_Type            => vOwner_Exch_Rate_Type,
                                                          cOnFailure_ErrorCode      => 3520,
                                                          cCommon_Exch_Currency     => vOwner_Com_Exch_Curr,
                                                          cInternalErrorCode        => cInternalErrorCode);

                     IF nvl(vOwner_Legal_Exch_Rate, 0) = 0 THEN
                        xxcp_foundation.Set_InternalErrorCode(cInternalErrorCode,3520);
                     END IF;
                  End If;

                END IF;
              End If;
            END IF; -- If Header in insert/update

              --
              -- Partner Legal Exchange Rate
              --
              IF (NVL(vPartnerTaxId, 0) <> 0) AND (cInternalErrorCode = 0) THEN
                If xxcp_global.gCommon(1).Calc_Legal_Exch_Rate = 'N' then
                  vPartner_Legal_Exch_Rate := Null;
                Else
                  -- Partner Legal
                  vPartner_Legal_Exch_Rate :=
                     xxcp_foundation.Currency_Conversions(cTransaction_Currency     => vTransaction_Currency,
                                                          cTag                      => 'Partner Legal',
                                                          cExchange_Currency        => vPartner_Legal_Currency,
                                                          cDefault_Common_Exch_Curr => cDflt_CommExchCurr,
                                                          cExchange_Date            => vExchange_Date,
                                                          cExchange_Type            => vPartner_Exch_Rate_Type,
                                                          cOnFailure_ErrorCode      => 3020,
                                                          cCommon_Exch_Currency     => vPartner_Common_Exch_Curr,
                                                          cInternalErrorCode        => cInternalErrorCode);
                End If;
              END IF;


            gXAM_Set(1).Owner_Legal_Currency     := vOwner_Legal_Currency;
            gXAM_Set(1).Partner_Legal_Currency   := vPartner_Legal_Currency;
            gXAM_Set(1).Owner_Trade_Currency     := vOwner_Trade_Currency;
            gXAM_Set(1).Partner_Trade_Currency   := vPartner_Trade_Currency;
            gXAM_Set(1).Dflt_CommExchCurr        := cDflt_CommExchCurr;
            gXAM_Set(1).Trade_Exch_Rate_Type     := vTrade_Exch_Rate_Type;
            gXAM_Set(1).Partner_Common_Exch_Curr := vPartner_Common_Exch_Curr;


            -- Pricing Type
            IC_Pricing(    cSourceRowId             => cSourceRowId,
                           cTransaction_id          => cTransaction_id,
                           cTransaction_Set_id      => cTransaction_Set_id,
                           cTransaction_Date        => cTransaction_Date,
                           cTrading_Set_ID          => cTrading_Set_ID,
                           cOwnerTaxid              => vOwnerTaxId,
                           cPartnerTaxId            => vPartnerTaxId,
                           cSet_of_books_id         => cSet_of_books_id,
                           cCreate_IC_trx           => cCreate_IC_Trx,
                           cCreate_Tax_Results      => cCreate_Tax_Results,
                           cKey                     => cKey,
                           cQuantity                => cQuantity,
                           cMC_Qualifier1           => cMC_Qualifier1,
                           cMC_Qualifier2           => cMC_Qualifier2,
                           cMC_Qualifier3           => cMC_Qualifier3,
                           cMC_Qualifier4           => cMC_Qualifier4,
                           cTAX_Qualifier1          => cTax_Qualifier1,
                           cTAX_Qualifier2          => cTax_Qualifier2,
                           cPrevious_Attribute_id   => cPrevious_Attribute_id,
                           cTransaction_Currency    => cTransaction_Currency,
                           cDflt_Trading_Relationship => cDflt_Trading_Relationship,
                           -- Out
                           cD1002_Pointers          => vD1002_Pointers,
                           cADJ_Qualifier3          => vADJ_Qualifier1,
                           cADJ_Qualifier4          => vADJ_Qualifier2,
                           cIC_Unit_Price           => vIC_Unit_Price,
                           cIC_Currency             => vIC_Currency,
                           cIC_Price                => vIC_Price,
                           cPrice_method_id         => vPrice_Method_id,
                           -- OUT
                           cIC_Control              => vIC_Control,
                           cAdjustment_Rate_id      => vAdjustment_Rate_id,
                           cAdjustment_Rate         => vAdjustment_Rate,
                           cTax_Code_id             => vTax_Code_Id,
                           cTax_Rate                => vTax_Rate,
                           cExchange_Rate_Type      => vTrade_Exch_Rate_Type,
                           cExchange_Date           => vExchange_Date,
                           cInternalErrorCode       => cInternalErrorCode);

           -- *3

          --
          -- Calculate Value Rate and Currency
          -- XAM was here
          If cInternalErrorCode = 0 And xxcp_global.gCommon(1).Custom_events = 'Y' then

             If xxcp_global.Timing_Adv_Mode = 'Y' then
               gTraceStartTime := systimestamp;
             End If;

             cInternalErrorCode := xxcp_custom_events.BEFORE_ASSIGNMENT_COMPLETE(
                                    cSource_id               => xxcp_global.gCommon(1).Source_id ,
                                    cSource_Assignment_id    => cSource_Assignment_id ,
                                    cTransaction_Date        => cTransaction_Date,
                                    cTransaction_Table       => xxcp_global.gCommon(1).Current_Transaction_Table,
                                    cParent_Trx_id           => cParent_Trx_Id,
                                    cSource_Rowid            => cSourceRowid,
                                    cTransaction_id          => cTransaction_Id,
                                    cTransaction_type        => xxcp_global.gCommon(1).Current_Transaction_Type,
                                    cTrading_Set             => xxcp_global.gCommon(1).Current_Trading_Set,
                                    cOwner_Tax_Reg_Id        => vOwnerTaxId,
                                    cOwner_Company_Number    => vOwner_Company_Number,
                                    cPartner_Tax_Reg_id      => vPartnerTaxId,
                                    cPartner_Company_Number  => vPartner_Company_Number,
                                    cPricing_Method_id       => vPrice_Method_ID,
                                    cIC_Unit_Price           => vIC_Unit_Price,
                                    cIC_Price                => vIC_Price,
                                    cIC_Currency             => vIC_Currency,
                                    cAdjustment_Rate         => vAdjustment_Rate,
                                    cIC_Tax_Rate             => vTax_Rate
                             );

           If xxcp_global.Timing_Adv_Mode = 'Y' then
              gTraceEndTime := systimestamp;
              CustomEventTiming('Before Assignment Complete');
           End If;

         End If;

        END IF; -- Check Error Code is null

        xxcp_trace.ASSIGNMENT_TRACE(cTrace_id             => 15,
                                    cOwnerTaxid           => vOwnerTaxid,
                                    cPartnerTaxId         => vPartnerTaxId,
                                    cTo_Curr_Indicator    => gXAM_Set(1).To_Curr_Indicator,
                                    cTransaction_Currency => cTransaction_Currency,
                                    cQuantity             => cQuantity,
                                    cExchange_Date        => vExchange_Date,
                                    cTrade_Exch_Rate_Type => vTrade_Exch_Rate_Type,
                                    cFrom_IC_Currency     => gXAM_Set(1).From_IC_Currency,
                                    cFrom_IC_Price        => gXAM_Set(1).From_IC_Unit_Price,
                                    cTo_IC_Currency       => vIC_Currency,
                                    cIC_Unit_Price        => vIC_Unit_Price,
                                    cIC_Price             => vIC_Price,
                                    cIC_Exch_Rate         => gXAM_Set(1).IC_Exch_Rate);

        --  Save Header
        --  Save Attribute
        --  Soft_Engine = S stands for used in SQL select statement, so no DML
        If xxcp_global.gCommon(1).Soft_Engine = 'Y' Then
           vRecordHeaderAction    := 'X';
           vRecordAttributeAction := 'X';

           -- Turn off Records
           If xxcp_global.gControlRec.Create_Row = 'Y' And cInternalErrorCode = 0 then

             vNew_Attribute_id :=
               MAKE_SA_ATTRIBUTES(
                           cRECORDACTION               => vRecordAttributeAction,
                           cSOURCE_ASSIGNMENT_ID       => cSource_Assignment_id,
                           cPARENT_TRX_ID              => cParent_Trx_Id,
                           cQUANTITY                   => cQuantity,
                           cUOM                        => cUOM,
                           cTRANSACTION_ID             => cTransaction_Id,
                           cSOURCE_TABLE_ID            => cSource_Table_id,
                           cSOURCE_TYPE_ID             => cSource_Type_id,
                           cSOURCE_CLASS_ID            => cSource_Class_id,
                           cTRADING_SET_ID             => cTrading_Set_id,
                           cTRANSACTION_DATE           => cTransaction_Date,
                           cTransaction_currency       => vTransaction_Currency,
                           cOwner_legal_curr           => vOwner_Legal_Currency,
                           cOwner_legal_exch_rate      => vOwner_Legal_Exch_Rate,
                           cOwner_Assoc_id             => vOwner_Association_Id,
                           cOwner_Assignment_rule_id   => vOwner_Assignment_Rule,
                           cPARTNER_ASSOC_ID           => vPartner_Association_Id,
                           cPARTNER_ASSIGNMENT_RULE_ID => vPartner_Assignment_Rule,
                           cPARTNER_TAX_REG_ID         => vPartnerTaxId,
                           cPARTNER_LEGAL_CURR         => vPartner_Legal_Currency,
                           cPARTNER_LEGAL_EXCH_RATE    => vPartner_Legal_Exch_Rate,
                           cEXCHANGE_DATE              => NVL(vExchange_Date,cExchange_Date),
                           -- Adjustment Qualifiers
                           cADJ_QUALIFIER1             => vADJ_Qualifier1,
                           cADJ_QUALIFIER2             => vADJ_Qualifier2,
                           -- Configuration Control
                           cIC_CONTROL                 => vIC_Control,
                           cMC_QUALIFIER1              => cMC_Qualifier1,
                           cMC_QUALIFIER2              => cMC_Qualifier2,
                           cMC_QUALIFIER3              => cMC_Qualifier3,
                           cMC_QUALIFIER4              => cMC_Qualifier4,
                           -- Tax Qualifiers
                           cTAX_QUALIFIER1             => cTAX_Qualifier1,
                           cTAX_QUALIFIER2             => cTAX_Qualifier2,
                           cOLS_Date                   => cOLS_Date,
                           cOLS_Flag                   => cOLS_Flag,
                           cADJUSTMENT_RATE            => vAdjustment_Rate,
                           cADJUSTMENT_RATE_ID         => vAdjustment_Rate_id,
                           cIC_TRADE_TAX_ID            => vTax_Code_Id,
                           cIC_TAX_RATE                => vTax_Rate,
                           cTRANSACTION_REF1           => cTransaction_ref1,
                           cTRANSACTION_REF2           => cTransaction_ref2,
                           cTRANSACTION_REF3           => cTransaction_ref3,
                           cOWNER_TAX_REG_ID           => vOwnerTaxId,
                           cIC_UNIT_PRICE              => vIC_Unit_Price,
                           cIC_CURRENCY                => vIC_Currency,
                           cIC_PRICE                   => vIC_Price,
                           cDOCUMENT_EXCH_RATE         => vDocument_Exch_Rate,
                           cPRICE_METHOD_ID            => vPrice_Method_id,
                           cDOCUMENT_CURRENCY          => vDocument_Currency,
                           cInternalErrorCode          => cInternalErrorCode
                           );

                 cNew_Attribute_id := vNew_Attribute_id;
           ElsIf xxcp_global.gCommon(1).Soft_Engine = 'Y' then
             Report_SA_Errors(cInternalErrorCode => cInternalErrorCode);
           End If;

           -- Soft Engine
           If xxcp_global.gControlRec.Create_Row != 'Y' then
            -- 02.06.11 ksp
            Set_Attribute_DFF(cSource_Type_id => cSource_Type_id);
           End If;

           gOutboundTab(gOutbound_Cnt).Internal_Error_Code     := nvl(cInternalErrorCode,0);
           If gOutboundTab(gOutbound_Cnt).Internal_Error_Code > 0 then
             gOutboundTab(gOutbound_Cnt).Error_Message         := xxcp_foundation.Get_MessageII(gOutboundTab(gOutbound_Cnt).Internal_Error_Code);
           End If;

           gOutboundTab(gOutbound_Cnt).Attribute_id            := vNew_Attribute_id;
           gOutboundTab(gOutbound_Cnt).Interface_id            := xxcp_global.gCommon(1).Current_Interface_id;
           gOutboundTab(gOutbound_Cnt).Transaction_Ref         := gInboundRec.vt_Transaction_Ref; --cTransaction_Ref1;
           gOutboundTab(gOutbound_Cnt).IC_Unit_Price           := vIC_Unit_Price;
           gOutboundTab(gOutbound_Cnt).IC_Price                := vIC_Price;
           gOutboundTab(gOutbound_Cnt).IC_Currency             := vIC_Currency;
           gOutboundTab(gOutbound_Cnt).Adjustment_Rate         := vAdjustment_Rate;
           gOutboundTab(gOutbound_Cnt).Adjustment_Rate_Id      := vAdjustment_Rate_id;
           gOutboundTab(gOutbound_Cnt).Quantity                := cQuantity;
           gOutboundTab(gOutbound_Cnt).UOM                     := cUOM;
           gOutboundTab(gOutbound_Cnt).Price_Method_id         := vPrice_Method_id;
           gOutboundTab(gOutbound_Cnt).Owner_Tax_Reg_id        := vOwnerTaxid;
           gOutboundTab(gOutbound_Cnt).Owner_Company_Number    := vOwner_Company_Number;
           gOutboundTab(gOutbound_Cnt).Owner_Exchange_Rate     := vOwner_Legal_Exch_Rate;
           gOutboundTab(gOutbound_Cnt).Partner_Tax_Reg_id      := vPartnerTaxid;
           gOutboundTab(gOutbound_Cnt).Partner_Company_Number  := vPartner_Company_Number;
           gOutboundTab(gOutbound_Cnt).Partner_Exchange_Rate   := vPartner_Legal_Exch_Rate;
           -- DFF
           gOutboundTab(gOutbound_Cnt).Attribute1   := xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE1;
           gOutboundTab(gOutbound_Cnt).Attribute2   := xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE2;
           gOutboundTab(gOutbound_Cnt).Attribute3   := xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE3;
           gOutboundTab(gOutbound_Cnt).Attribute4   := xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE4;
           gOutboundTab(gOutbound_Cnt).Attribute5   := xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE5;
           gOutboundTab(gOutbound_Cnt).Attribute6   := xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE6;
           gOutboundTab(gOutbound_Cnt).Attribute7   := xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE7;
           gOutboundTab(gOutbound_Cnt).Attribute8   := xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE8;
           gOutboundTab(gOutbound_Cnt).Attribute9   := xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE9;
           gOutboundTab(gOutbound_Cnt).Attribute10  := xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE10;
           gOutboundTab(gOutbound_Cnt).Attribute11  := xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE11;
           gOutboundTab(gOutbound_Cnt).Attribute12  := xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE12;
           gOutboundTab(gOutbound_Cnt).Attribute13  := xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE13;
           gOutboundTab(gOutbound_Cnt).Attribute14  := xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE14;
           gOutboundTab(gOutbound_Cnt).Attribute15  := xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE15;
           gOutboundTab(gOutbound_Cnt).Attribute16  := xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE16;
           gOutboundTab(gOutbound_Cnt).Attribute17  := xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE17;
           gOutboundTab(gOutbound_Cnt).Attribute18  := xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE18;
           gOutboundTab(gOutbound_Cnt).Attribute19  := xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE19;
           gOutboundTab(gOutbound_Cnt).Attribute20  := xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE20;
           gOutboundTab(gOutbound_Cnt).Attribute21  := xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE21;
           gOutboundTab(gOutbound_Cnt).Attribute22  := xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE22;
           gOutboundTab(gOutbound_Cnt).Attribute23  := xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE23;
           gOutboundTab(gOutbound_Cnt).Attribute24  := xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE24;
           gOutboundTab(gOutbound_Cnt).Attribute25  := xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE25;
           gOutboundTab(gOutbound_Cnt).Attribute26  := xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE26;
           gOutboundTab(gOutbound_Cnt).Attribute27  := xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE27;
           gOutboundTab(gOutbound_Cnt).Attribute28  := xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE28;
           gOutboundTab(gOutbound_Cnt).Attribute29  := xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE29;
           gOutboundTab(gOutbound_Cnt).Attribute30  := xxcp_global.gTA_Attributes(1).TA_ATTRIBUTE30;

           -- ksp 06-may-10
           gOutboundTab(gOutbound_Cnt).Transaction_ref1       := cTransaction_ref1;
           gOutboundTab(gOutbound_Cnt).Transaction_ref2       := cTransaction_ref2;
           gOutboundTab(gOutbound_Cnt).Transaction_ref3       := cTransaction_ref3;
           gOutboundTab(gOutbound_Cnt).Transaction_Currency   := vTransaction_Currency;
           gOutboundTab(gOutbound_Cnt).Transaction_date       := cTransaction_Date;
           gOutboundTab(gOutbound_Cnt).Exchange_date          := nvl(vExchange_Date,cExchange_Date);
           gOutboundTab(gOutbound_Cnt).Owner_Assoc_id         := vOwner_Association_Id;
           gOutboundTab(gOutbound_Cnt).Owner_Assign_Rule_id   := vOwner_Assignment_Rule;
           gOutboundTab(gOutbound_Cnt).Partner_Assoc_ID       := vPartner_Association_Id;
           gOutboundTab(gOutbound_Cnt).Partner_Assign_Rule_id := vPartner_Assignment_Rule;
           gOutboundTab(gOutbound_Cnt).IC_Trade_Tax_Id        := vTax_Code_Id;
           gOutboundTab(gOutbound_Cnt).IC_Tax_Rate            := vTax_Rate;
           -- end ksp 06-may-10

        End If;

        IF vRecordHeaderAction IN ('I', 'U')  THEN

            Make_Header(cRecordAction             => vRecordHeaderAction,
                        cSource_Assignment_id     => cSource_Assignment_id,
                        cSet_of_books_id          => cSet_of_books_id,
                        cParent_trx_id            => cParent_Trx_Id,
                        cSource_Table_id          => cSource_Table_id,
                        cTrading_set_id           => cTrading_set_id,
                        cTransaction_date         => cTransaction_date,
                        cTransaction_currency     => vTransaction_Currency,
                        cTransaction_exch_rate    => vTransaction_Exch_Rate,
                        cOwner_tax_reg_id         => vOwnerTaxId,
                        cOwner_legal_curr         => vOwner_Legal_Currency,
                        cOwner_legal_exch_rate    => vOwner_Legal_Exch_Rate,
                        cOwner_Assoc_id           => vOwner_Association_Id,
                        cOwner_Assignment_rule_id => vOwner_Assignment_Rule,
                        cExchange_date            => nvl(vExchange_Date, cExchange_Date),
                        cTransaction_ref1         => cTransaction_Ref1,
                        cTransaction_ref2         => cTransaction_Ref2,
                        cTransaction_ref3         => cTransaction_Ref3,
                        cHeaderRowId              => vHeaderRowid,
                        cInternalErrorCode        => cInternalErrorCode,
                        cHeader_ID                => vHeader_ID
                        );

         END IF;

         IF vOwnerTaxId IS NULL THEN
           BEGIN
             --
             -- If Header Owner Tax Reg is null then there should not be any
             -- transaction attribute rows. 17/6/2002.
             --
             vRecordAttributeAction := 'A'; -- ABORT

             DELETE FROM xxcp_transaction_attributes
                   WHERE Source_Assignment_id = cSource_Assignment_id
                     AND parent_trx_id        = cParent_Trx_id
                     AND source_type_id       = cSource_Type_id
                     AND trading_set_id       = cTrading_set_id;

             EXCEPTION WHEN OTHERS THEN NULL;
           END;
         END IF; -- End vOwnerTaxId is null

         IF vRecordAttributeAction IN ('I', 'U') THEN
            IF NVL(vHeader_ID, 0) <> 0 THEN

                If xxcp_global.gCommon(1).Explosion_id > 0 then
                 vNew_Attribute_id :=
                   MAKE_ATTRIBUTES(
                           cATTRIBUTE_ID               => cAttribute_id,
                           cRECORDACTION               => vRecordAttributeAction,
                           cSOURCE_ASSIGNMENT_ID       => cSource_Assignment_id,
                           cSET_OF_BOOKS_ID            => cSet_of_books_id,
                           cPARENT_TRX_ID              => cParent_Trx_Id,
                           cInterface_ID               => xxcp_global.gCommon(1).Current_Interface_id,
                           cQUANTITY                   => cQuantity,
                           cUOM                        => cUOM,
                           cTRANSACTION_ID             => xxcp_global.gCommon(1).Explosion_Transaction_id, --cTransaction_Id,
                           cSOURCE_TABLE_ID            => cSource_Table_id,
                           cSOURCE_TYPE_ID             => cSource_Type_id,
                           cSOURCE_CLASS_ID            => cSource_Class_id,
                           cTRADING_SET_ID             => cTrading_Set_id,
                           cTRANSACTION_DATE           => cTransaction_Date,
                           --
                           cPARTNER_ASSOC_ID           => vPartner_Association_Id,
                           cPARTNER_ASSIGNMENT_RULE_ID => vPartner_Assignment_Rule,
                           --
                           cPARTNER_TAX_REG_ID         => vPartnerTaxId,
                           cPARTNER_LEGAL_CURR         => vPartner_Legal_Currency,
                           cPARTNER_LEGAL_EXCH_RATE    => vPartner_Legal_Exch_Rate,
                           cEXCHANGE_DATE              => NVL(vExchange_Date,cExchange_Date),
                           -- Adjustment Qualifiers
                           cADJ_QUALIFIER3             => vADJ_Qualifier1,
                           cADJ_QUALIFIER4             => vADJ_Qualifier2,
                           -- Configuration Control
                           cIC_CONTROL                 => vIC_Control,
                           cMC_QUALIFIER1              => cMC_Qualifier1,
                           cMC_QUALIFIER2              => cMC_Qualifier2,
                           cMC_QUALIFIER3              => cMC_Qualifier3,
                           cMC_QUALIFIER4              => cMC_Qualifier4,
                           -- Entity Documents
                           cED_QUALIFIER1              => cED_Qualifier1,
                           cED_QUALIFIER2              => cED_Qualifier2,
                           cED_QUALIFIER3              => cED_Qualifier3,
                           cED_QUALIFIER4              => cED_Qualifier4,
                           -- Tax Qualifiers
                           cTAX_QUALIFIER1             => cTAX_Qualifier1,
                           cTAX_QUALIFIER2             => cTAX_Qualifier2,
                           cOLS_Date                   => cOLS_Date,
                           cOLS_Flag                   => cOLS_Flag,
                           -- Configuration
                           cD1000_Pointers             => cD1001_Pointers,
                           -- Configuration Pricing
                           cD1002_Pointers             => vD1002_Pointers,
                           --
                           cADJUSTMENT_RATE            => vAdjustment_Rate,
                           cADJUSTMENT_RATE_ID         => vAdjustment_Rate_id,
                           cIC_TRADE_TAX_ID            => vTax_Code_Id,
                           cIC_TAX_RATE                => vTax_Rate,
                           cTRANSACTION_REF1           => cTransaction_ref1,
                           cTRANSACTION_REF2           => cTransaction_ref2,
                           cTRANSACTION_REF3           => cTransaction_ref3,
                           cSTEP_NUMBER                => cStep_Number,
                           cOWNER_TAX_REG_ID           => vOwnerTaxId,
                           --
                           cIC_UNIT_PRICE              => vIC_Unit_Price,
                           cIC_CURRENCY                => vIC_Currency,
                           cIC_PRICE                   => vIC_Price,
                           cDOCUMENT_EXCH_RATE         => vDocument_Exch_Rate,
                           --
                           cPRICE_METHOD_ID            => vPrice_Method_id,
                           --
                           cDOCUMENT_CURRENCY          => vDocument_Currency,
                           cATTRIBUTEROWID             => vAttributeRowId,
                           cHEADER_ID                  => vHeader_id,
                           cRef_Ctl_id                 => cRef_Ctl_id,
                           cInternalErrorCode          => cInternalErrorCode
                           );
                 Else

                 vNew_Attribute_id :=
                   MAKE_ATTRIBUTES(
                           cATTRIBUTE_ID               => cAttribute_id,
                           cRECORDACTION               => vRecordAttributeAction,
                           cSOURCE_ASSIGNMENT_ID       => cSource_Assignment_id,
                           cSET_OF_BOOKS_ID            => cSet_of_books_id,
                           cPARENT_TRX_ID              => cParent_Trx_Id,
                           cInterface_ID               => xxcp_global.gCommon(1).Current_Interface_id,
                           cQUANTITY                   => cQuantity,
                           cUOM                        => cUOM,
                           cTRANSACTION_ID             => cTransaction_Id,
                           cSOURCE_TABLE_ID            => cSource_Table_id,
                           cSOURCE_TYPE_ID             => cSource_Type_id,
                           cSOURCE_CLASS_ID            => cSource_Class_id,
                           cTRADING_SET_ID             => cTrading_Set_id,
                           cTRANSACTION_DATE           => cTransaction_Date,
                           --
                           cPARTNER_ASSOC_ID           => vPartner_Association_Id,
                           cPARTNER_ASSIGNMENT_RULE_ID => vPartner_Assignment_Rule,
                           --
                           cPARTNER_TAX_REG_ID         => vPartnerTaxId,
                           cPARTNER_LEGAL_CURR         => vPartner_Legal_Currency,
                           cPARTNER_LEGAL_EXCH_RATE    => vPartner_Legal_Exch_Rate,
                           cEXCHANGE_DATE              => NVL(vExchange_Date,cExchange_Date),
                           -- Adjustment Qualifiers
                           cADJ_QUALIFIER3             => vADJ_Qualifier1,
                           cADJ_QUALIFIER4             => vADJ_Qualifier2,
                           -- Configuration Control
                           cIC_CONTROL                 => vIC_Control,
                           cMC_QUALIFIER1              => cMC_Qualifier1,
                           cMC_QUALIFIER2              => cMC_Qualifier2,
                           cMC_QUALIFIER3              => cMC_Qualifier3,
                           cMC_QUALIFIER4              => cMC_Qualifier4,
                           -- Entity Documents
                           cED_QUALIFIER1              => cED_Qualifier1,
                           cED_QUALIFIER2              => cED_Qualifier2,
                           cED_QUALIFIER3              => cED_Qualifier3,
                           cED_QUALIFIER4              => cED_Qualifier4,
                           -- Tax Qualifiers
                           cTAX_QUALIFIER1             => cTAX_Qualifier1,
                           cTAX_QUALIFIER2             => cTAX_Qualifier2,
                           cOLS_Date                   => cOLS_Date,
                           cOLS_Flag                   => cOLS_Flag,
                           -- Configuration
                           cD1000_Pointers             => cD1001_Pointers,
                           -- Configuration Pricing
                           cD1002_Pointers             => vD1002_Pointers,
                           --
                           cADJUSTMENT_RATE            => vAdjustment_Rate,
                           cADJUSTMENT_RATE_ID         => vAdjustment_Rate_id,
                           cIC_TRADE_TAX_ID            => vTax_Code_Id,
                           cIC_TAX_RATE                => vTax_Rate,
                           cTRANSACTION_REF1           => cTransaction_ref1,
                           cTRANSACTION_REF2           => cTransaction_ref2,
                           cTRANSACTION_REF3           => cTransaction_ref3,
                           cSTEP_NUMBER                => cStep_Number,
                           cOWNER_TAX_REG_ID           => vOwnerTaxId,
                           --
                           cIC_UNIT_PRICE              => vIC_Unit_Price,
                           cIC_CURRENCY                => vIC_Currency,
                           cIC_PRICE                   => vIC_Price,
                           cDOCUMENT_EXCH_RATE         => vDocument_Exch_Rate,
                           --
                           cPRICE_METHOD_ID            => vPrice_Method_id,
                           --
                           cDOCUMENT_CURRENCY          => vDocument_Currency,
                           cATTRIBUTEROWID             => vAttributeRowId,
                           cHEADER_ID                  => vHeader_id,
                           cRef_Ctl_id                 => cRef_Ctl_id,
                           cInternalErrorCode          => cInternalErrorCode
                           );
                     End If;

                 cNew_Attribute_id := vNew_Attribute_id;

            END IF;
         END IF; -- End Make_Attribute

      END IF; -- Check RecordActions

    -- Tier Logic
    xxcp_wks.D1007_Array := xxcp_wks.D1001_Array;
    xxcp_wks.D1007_Count := xxcp_wks.D1001_Count;

  END Assignment;

  --
  -- Set Global Common vars
  --
  Procedure SetCommon(cName in varchar2, cValue in varchar2, cNumber in number) is
  Begin
    If upper(cName) = 'TRADING_SET' then
      xxcp_global.gCommon(1).Current_trading_set := cValue;
    ElsIf upper(cName) = 'PREVIOUS_ATTRIBUTE' then
      xxcp_global.gCommon(1).Previous_Attribute_Id := cNumber;
    ElsIf upper(cName) = 'TRANSACTION_TABLE' then
      xxcp_global.gCommon(1).Current_Transaction_table := cValue;
    ElsIf upper(cName) = 'TRANSACTION_ID' then
      xxcp_global.gCommon(1).Current_Transaction_id := cNumber;
    End If;
  End SetCommon;

  --
  -- ## **************************************************************************************
  -- ##                    Main Assignment Control
  -- ## **************************************************************************************
  --
  PROCEDURE Main_Control(cSource_Assignment_ID IN NUMBER,
                         cSet_of_books_id      IN NUMBER,
                         cTransaction_Date     IN DATE,
                         cSource_id            IN NUMBER,
                         cSource_Table_id      IN NUMBER,
                         cSource_Type_id       IN NUMBER,
                         cSource_Class_id      IN NUMBER,
                         cTransaction_id       IN NUMBER,
                         cSourceRowid          IN ROWID,
                         cParent_Trx_id        IN NUMBER,
                         cTransaction_Set_id   IN NUMBER,
                         cSpecial_Array        IN VARCHAR2,
                         cKey                  IN VARCHAR2,
                         cTransaction_Table    IN VARCHAR2,
                         cTransaction_Type     IN VARCHAR2,
                         cTransaction_Class    IN VARCHAR2,
                         cOutboundTab         OUT OutboundTab%Type,
                         cInternalErrorCode    IN OUT NUMBER) IS

    CURSOR ACF(pSource_id IN NUMBER, pTransaction_set_id IN NUMBER) IS
      SELECT x.create_local_trx,
             substr(x.create_ic_trx, 1, 1) Create_IC_Trx,
             x.create_child_trx,
             x.create_gainloss_trx,
             nvl(x.create_tax_results,'N') create_tax_results,
             x.Assignment_set_id,
             x.sql_build_id Dynamic_Interface_id,
             x.create_trx_header,
             x.create_trx_attributes,
             s.trading_set,
             s.sequence,
             s.trading_set_id,
             x.alt_sql_build_id Alt_Dynamic_Interface_id,
             s.common_assignment,
             s.parent_assignment,
             nvl(x.multiple_owners,'N') multiple_owners, 
             nvl(x.ac_qualifier1,'~null~') AC_Qualifier1,
             nvl(x.ac_qualifier2,'~null~') AC_Qualifier2,
             nvl(x.acq_required,'N') acq_required,
             nvl(x.rank,1) Rank,
             nvl(x.assign_ctl_group_id,0) assign_ctl_group_id
           , w.qualifier1_id
           , w.qualifier2_id
           , w.qualifier_group_name
        FROM xxcp_assignment_ctl x,
             xxcp_trading_sets   s,
             xxcp_sys_qualifier_groups w
       WHERE x.source_id           = pSource_id
         AND x.transaction_set_id  = pTransaction_set_id
         AND x.source_id           = s.source_id
         AND x.Trading_set_id      = s.trading_set_id
         AND x.Active              = 'Y'
         AND x.assign_ctl_group_id = w.qualifier_group_id
         AND x.source_id           = w.source_id
         AND w.qualifier_usage     = 'A'
       ORDER BY s.Sequence, x.trading_set_id, nvl(x.rank,1);

    ACFRec ACF%ROWTYPE;

    CURSOR ASQ(pAssignment_set_id IN NUMBER, pAS_Qualifier1 IN VARCHAR2, pAS_Qualifier2 IN VARCHAR2, pTransaction_Date DATE) IS
      SELECT q.Assignment_set_id,
             q.Assignment_qualifier_id,
             nvl(q.Pass_Through_set_id, 0) Pass_Through_set_id,
             nvl(q.formula_set_id,0) Formula_Set_Id,
             q.dflt_trading_relationship Dflt_Trading_Relationship,
             nvl(q.ref_ctl_id,0) ref_ctl_id
        FROM xxcp_assignment_qualifiers q
       WHERE Assignment_set_id            = pAssignment_set_id
         AND NVL(AS_Qualifier1, '~null~') = NVL(pAS_Qualifier1, '~null~')
         AND NVL(AS_Qualifier2, '~null~') = NVL(PAS_Qualifier2, '~null~')
         AND pTransaction_Date between
             NVL(q.effective_from_date, '01-JAN-1900') AND NVL(q.effective_to_date, '31-DEC-2999')
       ORDER BY Assignment_Qualifier_id;

    ASQRec ASQ%ROWTYPE;

    -- Common Assignment
    Cursor ASR(pAssignment_Set_id in number ) IS
     select distinct r.tax_reg_attribute_id
       from xxcp_assignment_rules r
      where r.Assignment_set_id       = ASQRec.Assignment_Set_Id
        and r.Assignment_Qualifier_id = AsQRec.Assignment_Qualifier_Id
        and r.common_assignment = 'Y';


    vAttributeRowid      ROWID;
    vHeaderRowid         ROWID;
    vAttributeExists     Boolean;
    vConfig_Pointers     Varchar2(800);
    vRelease_Date        Date;
    --
    -- Cursor containing the data from the configurator for the
    -- for the transaction..
    --
    vAttribute_ID         xxcp_transaction_attributes.attribute_id%Type  := 0;
    vParent_Trx_id        xxcp_transaction_attributes.parent_trx_id%Type := 0;
    vFrozen               xxcp_transaction_attributes.frozen%Type;
    vTrading_Set          xxcp_trading_sets.trading_set%Type;
    vTrading_Set_id       xxcp_trading_sets.trading_set_id%Type;

    vTransaction_Ref1     xxcp_transaction_attributes.transaction_ref1%Type;
    vTransaction_Ref2     xxcp_transaction_attributes.transaction_ref2%Type;
    vTransaction_Ref3     xxcp_transaction_attributes.transaction_ref3%Type;
    vTransaction_Date     DATE;
    vTransaction_Currency xxcp_transaction_attributes.ic_currency%Type;
    vExchange_Date        DATE;

    vTAX_Qualifier1       xxcp_transaction_attributes.mc_qualifier1%Type;
    vTAX_Qualifier2       xxcp_transaction_attributes.mc_qualifier2%Type;
    vStep_Number          xxcp_transaction_attributes.step_number%Type;

    vPC_Qualifier1        xxcp_transaction_attributes.mc_qualifier1%Type;
    vPC_Qualifier2        xxcp_transaction_attributes.mc_qualifier2%Type;
    vPC_Qualifier3        xxcp_transaction_attributes.mc_qualifier1%Type;
    vPC_Qualifier4        xxcp_transaction_attributes.mc_qualifier2%Type;
    vED_Qualifier1        xxcp_transaction_attributes.ed_qualifier1%Type;
    vED_Qualifier2        xxcp_transaction_attributes.ed_qualifier2%Type;
    vED_Qualifier3        xxcp_transaction_attributes.ed_qualifier3%Type;
    vED_Qualifier4        xxcp_transaction_attributes.ed_qualifier4%Type;
    vAS_Qualifier1        xxcp_transaction_attributes.ed_qualifier1%Type;
    vAS_Qualifier2        xxcp_transaction_attributes.ed_qualifier2%Type;
    vAS_Qualifer_Found    Boolean := FALSE;
    vQty                  xxcp_transaction_attributes.quantity%Type;
    vUOM                  xxcp_transaction_attributes.UOM%Type;

    -- 02.06.01
    vTaxRegId             xxcp_tax_registrations.tax_registration_id%Type;
    vMandatory_Tax_Reg    Varchar2(1);
    vDynamic_Interface_id xxcp_sql_build.sql_build_id%Type;

    vCustom_Bind1         xxcp_transaction_attributes.attribute1%Type;
    vCustom_Bind2         xxcp_transaction_attributes.attribute1%Type;
    vCustom_Bind3         xxcp_transaction_attributes.attribute1%Type;
    vCustom_Bind4         xxcp_transaction_attributes.attribute1%Type;
    vCustom_Bind5         xxcp_transaction_attributes.attribute1%Type;
    vCustom_Bind6         xxcp_transaction_attributes.attribute1%Type;
    vCustom_Bind7         xxcp_transaction_attributes.attribute1%Type;
    vCustom_Bind8         xxcp_transaction_attributes.attribute1%Type;

    vAssignment_Set_id    xxcp_assignment_qualifiers.assignment_set_id%Type;
    vOLS_Flag             varchar2(1);
    vOLS_DATE             varchar2(50);
    vAnyAttributeFound    boolean := False;

    vCommonAssignment_Pointers varchar2(800);
    -- new
    vAC_Qualifier1_Id     xxcp_dynamic_attributes.column_id%type := 0;
    vAC_Qualifier2_Id     xxcp_dynamic_attributes.column_id%type := 0;  
    vCommonQualifier1     xxcp_assignment_ctl.ac_qualifier1%type := '~null~';  
    vCommonQualifier2     xxcp_assignment_ctl.ac_qualifier1%type := '~null~'; 
    vParent_Common_Label  varchar2(30) := 'Common';    
    
    vHAC_Count            number := 0;
    vLast_Trading_Set_id  Number;

  BEGIN

    cInternalErrorCode := 10020;
    xxcp_wks.Reset_TIER_Array;

    vCommonAssignment_Pointers := Null;

    xxcp_global.gCommon(1).Current_Transaction_id := cTransaction_id;
    xxcp_global.gCommon(1).Current_Source_rowid   := cSourceRowid;
    
    gOutbound_Cnt  := 0;
    gOutboundTab   := gOutbound_Empty;
    vParent_Trx_id := cParent_Trx_id;

    xxcp_wks.Custom_Event_Log := Null;

    -- New Transaction Flag.
    If (nvl(gNexTrxParentTrxId, 0) != cParent_trx_id OR nvl(gNewTrxTableId, 0) != cSource_Table_id) then
      xxcp_global.Set_New_Trx_Flag('Y');
      gNexTrxParentTrxId := cParent_trx_id;
      gNewTrxTableId     := cSource_Table_id;
      xxcp_te_base.SetPassThroughIgnore('N');
      xxcp_te_base.SetPassThroughPrvCode(NULL);
      xxcp_wks.P1001_Array := xxcp_wks.Clear_Dynamic;
      xxcp_wks.C1001_Array := xxcp_wks.Clear_Dynamic;
    Else
      xxcp_global.Set_New_Trx_Flag('N');
    End If;
    
    If xxcp_global.gCommon(1).Explosion_id > 0 then
      xxcp_wks.I1009_Array(137) := xxcp_global.gCommon(1).Explosion_RowsFound;
      xxcp_wks.I1009_Array(138) := xxcp_global.gCommon(1).Explosion_Transaction_id;
      xxcp_wks.I1009_Array(139) := xxcp_global.gCommon(1).Explosion_Rowid;
      xxcp_wks.I1009_Array(141) := xxcp_global.gCommon(1).Explosion_Summary1;
      xxcp_wks.I1009_Array(142) := xxcp_global.gCommon(1).Explosion_Summary2;
      xxcp_wks.I1009_Array(143) := xxcp_global.gCommon(1).Explosion_Summary3;
    End If;

    If xxcp_te_base.GetPassThroughIgnore = 'N' then

      SetCommon('PREVIOUS_ATTRIBUTE', Null, 0);

      xxcp_global.gCommon(1).Current_Transaction_date  := cTransaction_Date;
      xxcp_global.gCommon(1).Current_Source_Table_id   := cSource_Table_id;
      xxcp_global.gCommon(1).Current_Transaction_Type  := cTransaction_Type;
      xxcp_global.gCommon(1).Current_Transaction_class := cTransaction_class;

      xxcp_wks.I1009_Array(39) := cTransaction_Type;
      xxcp_wks.I1009_Array(40) := cTransaction_Class;

      xxcp_trace.Assignment_Trace(cTrace_id          => 1,
                                  cTransaction_Table => cTransaction_Table,
                                  cTransaction_type  => cTransaction_Type,
                                  cTransaction_Date  => cTransaction_Date,
                                  cTransaction_id    => cTransaction_id);

      xxcp_global.gTA_Attributes := xxcp_global.gTA_Attributes_Clear;
      
      vLast_Trading_Set_id := -707;
      
      OPEN ACF(cSource_id, cTransaction_Set_id);
      LOOP
        FETCH ACF INTO ACFRec;
        EXIT WHEN ACF%NOTFOUND;

        cInternalErrorCode := 0;
        vTrading_Set       := ACFRec.Trading_Set;
        vTrading_Set_id    := ACFRec.Trading_Set_id;
        xxcp_global.gCommon(1).Current_trading_set_id := vTrading_Set_id;

        -- Records
        gOutbound_Cnt := gOutbound_Cnt + 1;
        gOutboundTab(gOutbound_Cnt).Sequence       := ACFRec.Sequence;
        gOutboundTab(gOutbound_Cnt).Trading_Set    := vTrading_Set;
        gOutboundTab(gOutbound_Cnt).Trading_Set_id := vTrading_Set_id;
        -- Tax Record
        xxcp_te_base.gTax_Record_Set.delete;
        --
        SetCommon('TRADING_SET', ACFRec.Trading_Set, Null);
        SetCommon('TRADING_SET_ID', NULL, ACFRec.Trading_Set_id);
        --
        vAssignment_Set_id   := ACFRec.Assignment_set_id;
        xxcp_wks.D1001_Array := xxcp_wks.Clear_Dynamic;

        -- 03.06.27 Only perform this for the first trx in parent
        If (ACFRec.Parent_Assignment = 'Y' and xxcp_global.Get_New_Trx_Flag = 'Y') then
           xxcp_wks.P1001_Array := xxcp_wks.Clear_Dynamic;
           vParent_Common_Label := 'Parent';
        ElsIf ACFRec.Common_Assignment = 'Y' then
           xxcp_wks.C1001_Array :=  xxcp_wks.P1001_Array;
           vParent_Common_Label := 'Common';
        Else
           xxcp_wks.D1001_Array :=  xxcp_wks.C1001_Array;
        End If;

        -- Trace
        xxcp_trace.Assignment_Trace(cTrace_id           => 2,
                                    cTransaction_Set_id => cTransaction_Set_id,
                                    cAssignment_Set_id  => vAssignment_Set_id);

        -- 02.06.01
        -- If Common Assignment is used then branch
        If ACFRec.Common_Assignment = 'Y' or (ACFRec.Parent_Assignment = 'Y' and xxcp_global.Get_New_Trx_Flag = 'Y') Then
          gOutboundTab   := gOutbound_Empty;
          gOutbound_Cnt  := 0;
          -- ***********************************************
          --     DYNAMIC CONFIGURATOR Control
          -- ***********************************************
          xxcp_wks.D1001_Array := xxcp_wks.C1001_Array;

          xxcp_wks.Trace_Log   := xxcp_wks.Trace_Log || CHR(10) || CHR(10) ||'  '||vParent_Common_Label||' Assignment Dynamic Interface <' ||
                                  to_char(ACFRec.Dynamic_Interface_id) || '> ' ||xxcp_Translations.Dynamic_Interface(ACFRec.Dynamic_Interface_id);

          If xxcp_global.gCommon(1).Custom_events = 'Y' then
            If xxcp_global.Timing_Adv_Mode = 'Y' then
              gTraceStartTime := systimestamp;
            End If;

            cInternalErrorCode := xxcp_custom_events.CONFIGURATION_BINDS(cSource_id            => xxcp_global.gCommon(1).Source_id,
                                                                         cSource_Assignment_ID => cSource_Assignment_id,
                                                                         cTransaction_Table    => xxcp_global.gCommon(1).Current_Transaction_Table,
                                                                         cParent_Trx_id        => cParent_trx_id,
                                                                         cSource_Rowid         => cSourceRowid,
                                                                         cTransaction_Id       => cTransaction_id,
                                                                         cTransaction_Type     => xxcp_global.gCommon(1).Current_Transaction_Type,
                                                                         cTrading_Set          => vTrading_Set,
                                                                         cCustom_Bind1         => vCustom_Bind1,
                                                                         cCustom_Bind2         => vCustom_Bind2,
                                                                         cCustom_Bind3         => vCustom_Bind3,
                                                                         cCustom_Bind4         => vCustom_Bind4,
                                                                         cCustom_Bind5         => vCustom_Bind5,
                                                                         cCustom_Bind6         => vCustom_Bind6,
                                                                         cCustom_Bind7         => vCustom_Bind7,
                                                                         cCustom_Bind8         => vCustom_Bind8);
            If xxcp_global.Timing_Adv_Mode = 'Y' then
              gTraceEndTime := systimestamp;
              CustomEventTiming('Configuration Bind (Common)');
            End If;

            If nvl(cInternalErrorCode, 0) > 0 then
              Exit; -- API has returned an error code -> Exit
            End If;

          End If;

          If ACFRec.Dynamic_Interface_id > 0 then
                xxcp_wks.D1001_Array := xxcp_wks.C1001_Array; 
                -- Common Assignment
                xxcp_dynamic_sql.Configurator(
                                        cTransaction_Table     => xxcp_global.gCommon(1).Current_Transaction_Table,
                                        cTransaction_Id        => cTransaction_id,
                                        cTransaction_Class     => cTransaction_Class,
                                        cTransaction_Type      => xxcp_global.gCommon(1).Current_Transaction_Type,
                                        cTrading_Set           => vTrading_Set,
                                        cSet_of_books_id       => cSet_of_books_id,
                                        cSourceRowid           => cSourceRowid,
                                        cActivity_id           => 1001,
                                        cDynamic_Interface_id  => ACFRec.Dynamic_Interface_id,
                                        cPrevious_Attribute_Id => xxcp_global.gCommon(1).Previous_Attribute_Id,
                                        cSpecial_Array         => cSpecial_Array,
                                        cCustom_Bind1          => vCustom_Bind1,
                                        cCustom_Bind2          => vCustom_Bind2,
                                        cCustom_Bind3          => vCustom_Bind3,
                                        cCustom_Bind4          => vCustom_Bind4,
                                        cCustom_Bind5          => vCustom_Bind5,
                                        cCustom_Bind6          => vCustom_Bind6,
                                        cCustom_Bind7          => vCustom_Bind7,
                                        cCustom_Bind8          => vCustom_Bind8,
                                        cArray_Positions       => vConfig_Pointers,
                                        cInternalErrorCode     => cInternalErrorCode);

                -- 02.06.01
                -- Set the "Common" dynamic array.
                
                xxcp_wks.C1001_Array       := xxcp_wks.Dynamic_Results;
                xxcp_wks.D1001_Array       := xxcp_wks.Clear_Dynamic;
                
                If ACFRec.Parent_Assignment = 'Y' and xxcp_global.Get_New_Trx_Flag = 'Y' then 
                  xxcp_wks.P1001_Array := xxcp_wks.C1001_Array; -- new
                End If;              
--- YYYY
                -- Assignment Trading Set Qualifier
                If vAC_Qualifier1_Id > 0 then
                  vCommonQualifier1 := xxcp_wks.C1001_Array(vAC_Qualifier1_Id);
                End If;
            
                If vAC_Qualifier2_Id > 0 then
                  vCommonQualifier2 :=xxcp_wks.C1001_Array(vAC_Qualifier2_Id);
                End If;                            
--- YYY                
          End If;

          xxcp_wks.D1001_Array       := xxcp_wks.C1001_Array;
          xxcp_wks.DYNAMIC_RESULTS   := xxcp_wks.Clear_Dynamic;
          -- 02.06.18
          vAS_Qualifier1  := xxcp_wks.D1001_Array(19);
          vAS_Qualifier2  := xxcp_wks.D1001_Array(20);

          IF NVL(cInternalErrorCode, 0) = 0 THEN
            cInternalErrorCode := 0;
            --
            -- Dynamic Configurator SQL OK
            --
            xxcp_wks.D1001_Count := xxcp_wks.Max_Dynamic_Attribute;
            vTransaction_Date    := xxcp_wks.D1001_Array(1);
            IF vTransaction_Date < gCentrySwitchDate THEN
              vTransaction_date := add_months(vTransaction_Date, 1200); -- 100 Years
            END IF;

            xxcp_global.gCommon(1).Current_transaction_date := vTransaction_date;

            IF xxcp_global.Preview_on = 'N' THEN
              vParent_Trx_Id := cParent_Trx_id;
            ELSE
              gLast_Source_Type_id := 0;
              vParent_Trx_Id       := xxcp_global.gCommon(1).Preview_Parent_trx_id;
            END IF;

            xxcp_wks.D1001_Array(2) := vParent_Trx_id;
          End If; -- dynamic sql has no errors

          EXIT WHEN cInternalErrorCode <> 0;

          If xxcp_global.gCommon(1).Custom_events = 'Y' then

            If xxcp_global.Timing_Adv_Mode = 'Y' then
              gTraceStartTime := systimestamp;
            End If;

            cInternalErrorCode := xxcp_custom_events.ASSIGMENT_QUALIFIERS(cSource_id             => xxcp_global.gCommon(1).Source_id,
                                                                          cSource_Assignment_ID  => cSource_Assignment_id,
                                                                          cTransaction_Table     => xxcp_global.gCommon(1).Current_Transaction_Table,
                                                                          cParent_Trx_id         => cParent_trx_id,
                                                                          cSource_Rowid          => cSourceRowid,
                                                                          cTransaction_Id        => cTransaction_id,
                                                                          cTransaction_Type      => xxcp_global.gCommon(1).Current_Transaction_Type,
                                                                          cTrading_Set           => vTrading_Set,
                                                                          cTransaction_Date      => vTransaction_Date,
                                                                          cAssignment_Attributes => xxcp_wks.D1001_Array,
                                                                          cAssignment_Qualifier1 => vAS_Qualifier1,
                                                                          cAssignment_Qualifier2 => vAS_Qualifier2);

            If xxcp_global.Timing_Adv_Mode = 'Y' then
              gTraceEndTime := systimestamp;
              CustomEventTiming('Assignment Qualifiers (Common)');
            End If;

            EXIT WHEN nvl(cInternalErrorCode, 0) <> 0;

          End If;

          xxcp_trace.Assignment_Trace(cTrace_id             => 4,
                                      cTransaction_Currency => vTransaction_Currency,
                                      cTransaction_Date     => vTransaction_Date,
                                      cExchange_Date        => vExchange_Date,
                                      cAS_Qualifier1        => vAS_Qualifier1,
                                      cAS_Qualifier2        => vAS_Qualifier2);

          OPEN ASQ(pAssignment_set_id => vAssignment_Set_id, pAS_Qualifier1 => vAS_Qualifier1, pAS_Qualifier2 => vAS_Qualifier2, pTransaction_Date => vTransaction_date);
          LOOP
            FETCH ASQ INTO ASQRec;
            EXIT WHEN ASQ%NOTFOUND;

            -- 02.06.01
            -- Find the dynamic attributes to store the set of tax reg id in.
            For ASRrec in ASR(pAssignment_Set_id => ASQRec.Assignment_Set_id) Loop

              vTaxRegId := xxcp_foundation.GetCATaxRegId(cTax_reg_attribute_id    => ASRrec.tax_reg_attribute_id,
                                                         cEffective_Date          => cTransaction_Date,
                                                         cAssignment_set_id       => ASQRec.Assignment_Set_Id,
                                                         cAssignment_Qualifier_id => AsQRec.Assignment_Qualifier_Id,
                                                         cSource_Rowid            => cSourceRowid,
                                                         cMandatory_tax_reg       => vMandatory_tax_reg);

              vConfig_Pointers := vConfig_Pointers || to_char(ASRrec.Tax_Reg_Attribute_id);

              xxcp_wks.c1001_array(ASRrec.tax_reg_attribute_id - 1000) := vTaxRegId;

              If vMandatory_tax_reg = 'Y' and nvl(vTaxRegId, 0) = 0 Then
                cInternalErrorCode := 3509;
                exit;
              End If;

            End loop;

            If cInternalErrorCode = 0 and ASQRec.Formula_Set_Id > 0 then
             cInternalErrorCode := xxcp_te_formulas.Call_Formula( cSource_id        => xxcp_global.gCommon(1).Source_id,
                                                                  cFormula_Set_id   => ASQRec.Formula_Set_Id,
                                                                  cFormula_Pointers => gFormula_Pointers_1001c,
                                                                  cDynamic_Array    => xxcp_wks.c1001_array);
            End If;
            -- This is key
            If vAC_Qualifier1_Id > 0 then
              vCommonQualifier1 := xxcp_wks.C1001_Array(vAC_Qualifier1_Id);
            End If;
            
            If vAC_Qualifier2_Id > 0 then
              vCommonQualifier2 :=xxcp_wks.C1001_Array(vAC_Qualifier2_Id);
            End If;                            

            -- 03.06.24 Moved call to after formula set
            EXIT; -- Only perform for one qualifier set

          END LOOP;

          vCommonAssignment_Pointers := vCommonAssignment_Pointers || vConfig_Pointers;  -- 02.06.07

          -- 03.06.27 
          If ACFRec.Parent_Assignment = 'Y' and xxcp_global.Get_New_Trx_Flag = 'Y' then -- Parent Level Assignment
            xxcp_wks.P1001_Array := xxcp_wks.C1001_Array;
          End If;          
          
          CLOSE ASQ; -- End Common Assignment
          
          xxcp_trace.Common_Write_Trace;

       -- 03.06.27 Parent and not new parent_trx_id then do nothing          
       Elsif (ACFRec.Common_Assignment = 'N' and ACFRec.Parent_Assignment = 'Y' and xxcp_global.Get_New_Trx_Flag = 'N') Then                 
         Null;       
        -- 02.06.01
        -- If not common assignment.
       -- Normal Processing Starts Here.
       Else
         -- ** NEW      
         vCommonQualifier1 := GetCommonAttribute(ACFRec.Qualifier1_Id);
         vCommonQualifier2 := GetCommonAttribute(ACFRec.Qualifier2_Id);
         -- ** END NEW   
       
         
         If ((ACFRec.AC_QUALIFIER1 IN ('*',nvl(vCommonQualifier1,'~null~')) 
           And (ACFRec.AC_QUALIFIER2 IN ('*',nvl(vCommonQualifier2,'~null~'))))) 
             And vLast_Trading_Set_id != ACFRec.Trading_Set_ID Then         
          
          
          vLast_Trading_Set_id := ACFRec.Trading_Set_ID; 
          --
          --  Check to see if the configurator already has worked out the
          --  Partner details and therefore the owner..
          --
          vAttributeExists := xxcp_foundation.Transaction_Attribute_Exists(cSource_Assignment_id => cSource_Assignment_ID,
                                                                           cTrading_Set_id       => ACFRec.Trading_Set_ID,
                                                                           cTransaction_Id       => cTransaction_Id,
                                                                           cSource_Table_id      => cSource_Table_id,
                                                                           cSource_Type_id       => cSource_Type_id,
                                                                           cFrozen               => vFrozen,
                                                                           cAttributeRowid       => vAttributeRowid,
                                                                           cAttribute_Id         => vAttribute_Id,
                                                                           cParent_Trx_id        => vParent_Trx_id);

          IF ((vAttributeExists = TRUE) AND (vFrozen = 'Y')) THEN

            vAnyAttributeFound := TRUE;
            SetCommon('PREVIOUS_ATTRIBUTE', Null, vAttribute_id);
            xxcp_trace.Assignment_Trace(cTrace_id          => 3,
                                        cAttribute_id      => vAttribute_id,
                                        cParent_Trx_id     => cParent_Trx_id,
                                        cExt_Parent_Trx_id => vParent_Trx_id);
                                        
            vHAC_Count := vHAC_Count +1;

          ELSE

            --
            -- If XXCP_TRANSACTION_ATTRIBUTE row does not exist then call then
            -- configurator to create it...
            --
            IF ((vAttributeExists = FALSE) OR (vFrozen <> 'Y')) THEN

              -- Test before calling Dynamic SQL
              IF cInternalErrorCode <> 0 THEN
                xxcp_foundation.FndWriteError(cInternalErrorCode,
                                              'Source Assignment Id <' ||to_char(cSource_Assignment_ID) ||'> Transaction Table <' ||
                                               xxcp_global.gCommon(1).Current_Transaction_Table ||'> Type <' ||xxcp_global.gCommon(1).Current_Transaction_Type ||
                                               '> Trading Set <' ||xxcp_global.gCommon(1).Current_trading_set || '>');
              ELSE

                -- ***********************************************
                --     DYNAMIC CONFIGURATOR Control
                -- ***********************************************
                -- 02.06.01
                -- If dynamic interface Id is 0 then do nothing.
                If ACFRec.Dynamic_Interface_id = 0 then
                  xxcp_wks.D1001_Array := xxcp_wks.C1001_Array;
                  xxcp_wks.Trace_Log   := xxcp_wks.Trace_Log || CHR(10) ||CHR(10) ||'  Common Assignment Values';
                  
                  vConfig_Pointers := vCommonAssignment_Pointers;

                Else
                 -- xxcp_wks.D1001_Array := xxcp_wks.Clear_Dynamic;
                  xxcp_wks.D1001_Array := xxcp_wks.C1001_Array;
                  xxcp_wks.Trace_Log   := xxcp_wks.Trace_Log || CHR(10) ||CHR(10) ||'  Assignment Dynamic Interface <' ||TO_CHAR(ACFRec.Dynamic_Interface_id) || '> ';-- ||xxcp_Translations.Dynamic_Interface(ACFRec.Dynamic_Interface_id);

                  If xxcp_global.gCommon(1).Custom_events = 'Y' then
                    If xxcp_global.Timing_Adv_Mode = 'Y' then
                      gTraceStartTime := systimestamp;
                    End If;

                    cInternalErrorCode := xxcp_custom_events.CONFIGURATION_BINDS(cSource_id            => xxcp_global.gCommon(1).Source_id,
                                                                                 cSource_Assignment_ID => cSource_Assignment_id,
                                                                                 cTransaction_Table    => xxcp_global.gCommon(1).Current_Transaction_Table,
                                                                                 cParent_Trx_id        => cParent_trx_id,
                                                                                 cSource_Rowid         => cSourceRowid,
                                                                                 cTransaction_Id       => cTransaction_id,
                                                                                 cTransaction_Type     => xxcp_global.gCommon(1).Current_Transaction_Type,
                                                                                 cTrading_Set          => vTrading_Set,
                                                                                 cCustom_Bind1         => vCustom_Bind1,
                                                                                 cCustom_Bind2         => vCustom_Bind2,
                                                                                 cCustom_Bind3         => vCustom_Bind3,
                                                                                 cCustom_Bind4         => vCustom_Bind4,
                                                                                 cCustom_Bind5         => vCustom_Bind5,
                                                                                 cCustom_Bind6         => vCustom_Bind6,
                                                                                 cCustom_Bind7         => vCustom_Bind7,
                                                                                 cCustom_Bind8         => vCustom_Bind8);
                   If xxcp_global.Timing_Adv_Mode = 'Y' then
                     gTraceEndTime := systimestamp;
                     CustomEventTiming('Configuration Binds', vTrading_Set);
                   End If;

                    If nvl(cInternalErrorCode, 0) > 0 then
                      Exit; -- API has returned an error code -> Exit
                    End If;
                  End If;

                  vDynamic_Interface_id := ACFRec.Dynamic_Interface_id;

                  -- Take into account the AR Invoice Dynamic Interface which could be different.
                  If xxcp_global.gCommon(1).Current_Source_Activity = 'VARC' then
                    If nvl(ACFRec.Alt_Dynamic_Interface_id, 0) > 0 then
                      vDynamic_Interface_id := ACFRec.Alt_Dynamic_Interface_id;
                    End If;
                  End If;

                  xxcp_dynamic_sql.Configurator(cTransaction_Table     => xxcp_global.gCommon(1).Current_Transaction_Table,
                                                cTransaction_Id        => cTransaction_id,
                                                cTransaction_Class     => cTransaction_Class,
                                                cTransaction_Type      => xxcp_global.gCommon(1).Current_Transaction_Type,
                                                cTrading_Set           => vTrading_Set,
                                                cSet_of_books_id       => cSet_of_books_id,
                                                cSourceRowid           => cSourceRowid,
                                                cActivity_id           => 1001,
                                                cDynamic_Interface_id  => vDynamic_Interface_id,
                                                cPrevious_Attribute_Id => xxcp_global.gCommon(1).Previous_Attribute_Id,
                                                cSpecial_Array         => cSpecial_Array,
                                                cCustom_Bind1          => vCustom_Bind1,
                                                cCustom_Bind2          => vCustom_Bind2,
                                                cCustom_Bind3          => vCustom_Bind3,
                                                cCustom_Bind4          => vCustom_Bind4,
                                                cCustom_Bind5          => vCustom_Bind5,
                                                cCustom_Bind6          => vCustom_Bind6,
                                                cCustom_Bind7          => vCustom_Bind7,
                                                cCustom_Bind8          => vCustom_Bind8,
                                                cArray_Positions       => vConfig_Pointers,
                                                cInternalErrorCode     => cInternalErrorCode);

                  xxcp_wks.D1001_Array     := xxcp_wks.DYNAMIC_RESULTS;
                  xxcp_wks.DYNAMIC_RESULTS := xxcp_wks.Clear_Dynamic;

                  vConfig_Pointers := vConfig_Pointers || vCommonAssignment_Pointers;

                End If;
              End if;

              IF NVL(cInternalErrorCode, 0) = 0 THEN
                cInternalErrorCode := 0;
                --
                --
                -- Dynamic Configurator SQL OK
                --
                xxcp_wks.D1001_Count := xxcp_wks.Max_Dynamic_Attribute;
                vTransaction_Date    := xxcp_wks.D1001_Array(1);

                IF vTransaction_Date < gCentrySwitchDate THEN
                  vTransaction_date := add_months(vTransaction_Date, 1200); -- 100 Years
                END IF;

                xxcp_global.gCommon(1).Current_Transaction_date := vTransaction_date;

                IF xxcp_global.Preview_on = 'N' THEN
                  vParent_Trx_Id := cParent_Trx_id;
                ELSE
                  gLast_Source_Type_id := 0;
                  vParent_Trx_Id       := xxcp_global.gCommon(1).Preview_Parent_trx_id;
                END IF;

                xxcp_wks.D1001_Array(2) := vParent_Trx_id;
                vTransaction_Ref1       := xxcp_wks.D1001_Array(3);
                vTransaction_Ref2       := xxcp_wks.D1001_Array(4);
                vTransaction_Ref3       := xxcp_wks.D1001_Array(5);
                vTransaction_Currency   := xxcp_wks.D1001_Array(6);
                vExchange_Date          := xxcp_wks.D1001_Array(7);

                IF TO_CHAR(vExchange_Date, 'DD-MON-YYYY') < gCentrySwitchDate THEN
                  vExchange_Date := Add_Months(vExchange_Date, 1200); -- 100 Years
                END IF;

                vQty            := xxcp_wks.D1001_Array(8);
                vUOM            := xxcp_wks.D1001_Array(9);
                vPC_Qualifier1  := xxcp_wks.D1001_Array(10);
                vPC_Qualifier2  := xxcp_wks.D1001_Array(11);
                vED_Qualifier1  := xxcp_wks.D1001_Array(12);
                vED_Qualifier2  := xxcp_wks.D1001_Array(13);
                vStep_Number    := substr(xxcp_wks.D1001_Array(14), 1, 15);
                vED_Qualifier3  := xxcp_wks.D1001_Array(15);
                vED_Qualifier4  := xxcp_wks.D1001_Array(16);
                vAS_Qualifier1  := xxcp_wks.D1001_Array(19);
                vAS_Qualifier2  := xxcp_wks.D1001_Array(20);
                vPC_Qualifier3  := xxcp_wks.D1001_Array(21);
                vPC_Qualifier4  := xxcp_wks.D1001_Array(22);
                vTAX_Qualifier1 := xxcp_wks.D1001_Array(23);
                vTAX_Qualifier2 := xxcp_wks.D1001_Array(24);
                vOLS_Flag       := xxcp_wks.D1001_Array(25);
                vOLS_Date       := xxcp_wks.D1001_Array(26);

                IF vOLS_Date < gCentrySwitchDate THEN
                  vOLS_Date := Add_Months(vOLS_Date, 1200); -- 100 Years
                END IF;

                IF vTransaction_date IS NULL THEN
                  cInternalErrorCode := 3511; -- Transaction Date is null
                END IF;

                -- Release Date
                If xxcp_wks.D1001_Array(17) IS NOT NULL Then
                  vRelease_Date := xxcp_wks.D1001_Array(17);
                  IF vRelease_Date < gCentrySwitchDate THEN
                    vRelease_Date := add_months(vRelease_Date, 1200); -- 100 Years
                  End If;
                  If vRelease_Date > xxcp_global.SystemDate then
                    xxcp_global.Set_Release_Date(vRelease_Date);
                  End If;
                End If;
                
                -- 03.06.30 This global variable is primarily for the PO matching source (43)
                -- Unmatched Transaction Flag
                If xxcp_foundation.Find_DynamicAttribute(xxcp_te_base.Get_Unmatched_Attribute) = 'Y' Then
                  xxcp_global.Set_Unmatched_Flag('Y');
                End If;
                
                -- 03.06.30 If the trx is unmatched then we can notify ICS that it is NOT eligible for approval
                If nvl(xxcp_global.Get_Unmatched_Flag,'N') = 'Y' then  
                  xxcp_global.Add_Unmatched_Parent(cInvoice_Matching_Rec => xxcp_global.gInvoice_Matching_Common(1));
                End If;

              END IF; -- dynamic sql has no errors

            END IF; -- end attribute exists or is not frozen

            EXIT WHEN cInternalErrorCode <> 0;

            If xxcp_global.gCommon(1).Custom_events = 'Y' then

              If xxcp_global.Timing_Adv_Mode = 'Y' then
                gTraceStartTime := systimestamp;
              End If;

              cInternalErrorCode := xxcp_custom_events.ASSIGMENT_QUALIFIERS(cSource_id             => xxcp_global.gCommon(1).Source_id,
                                                                            cSource_Assignment_ID  => cSource_Assignment_id,
                                                                            cTransaction_Table     => xxcp_global.gCommon(1).Current_Transaction_Table,
                                                                            cParent_Trx_id         => cParent_trx_id,
                                                                            cSource_Rowid          => cSourceRowid,
                                                                            cTransaction_Id        => cTransaction_id,
                                                                            cTransaction_Type      => xxcp_global.gCommon(1).Current_Transaction_Type,
                                                                            cTrading_Set           => vTrading_Set,
                                                                            cTransaction_Date      => vTransaction_Date,
                                                                            cAssignment_Attributes => xxcp_wks.D1001_Array,
                                                                            cAssignment_Qualifier1 => vAS_Qualifier1,
                                                                            cAssignment_Qualifier2 => vAS_Qualifier2);

              If xxcp_global.Timing_Adv_Mode = 'Y' then
                gTraceEndTime := systimestamp;
                CustomEventTiming('Assignment Qualifiers');
              End If;

              EXIT WHEN nvl(cInternalErrorCode, 0) <> 0;
            End If;

            xxcp_trace.Assignment_Trace(cTrace_id             => 4,
                                        cTransaction_Currency => vTransaction_Currency,
                                        cTransaction_Date     => vTransaction_Date,
                                        cExchange_Date        => vExchange_Date,
                                        cAS_Qualifier1        => vAS_Qualifier1,
                                        cAS_Qualifier2        => vAS_Qualifier2);
                                        
            -- 03.06.28 Reset variable for 2nd + legs
            if ACFRec.ACQ_Required = 'Y' then 
              vAS_Qualifer_Found := FALSE;
            end if;

            OPEN ASQ(pAssignment_Set_id => vAssignment_Set_id, pAS_Qualifier1 => vAS_Qualifier1, pAS_Qualifier2 => vAS_Qualifier2, pTransaction_date => vTransaction_date);
            LOOP
              FETCH ASQ INTO ASQRec;
              EXIT WHEN ASQ%NOTFOUND;

              vAS_Qualifer_Found := TRUE;

              Assignment(   cRefresh               => NULL,
                            cCreate                => 'Y',
                            cAttribute_id          => vAttribute_Id,
                            cAttributeRowid        => vAttributeRowId,
                            cAttributeFrozen       => vFrozen,
                            cSource_Assignment_id  => cSource_Assignment_ID,
                            cSet_of_books_id       => cSet_of_Books_id,
                            cTrading_Set_id        => vTrading_Set_id,
                            cSource_Table_id       => cSource_Table_id,
                            cSource_Class_id       => cSource_Class_id,
                            cSource_Type_id        => cSource_Type_id,
                            cTransaction_Id        => cTransaction_Id,
                            cTransaction_Ref1      => vTransaction_Ref1,
                            cTransaction_Ref2      => vTransaction_Ref2,
                            cTransaction_Ref3      => vTransaction_Ref3,
                            cTransaction_Date      => vTransaction_Date,
                            cParent_Trx_Id         => vParent_Trx_Id,
                            cQuantity              => vQty,
                            cUOM                   => vUOM,
                            cTransaction_Currency  => vTransaction_Currency,
                            cExchange_Date         => vExchange_Date,
                            cD1001_Pointers        => vConfig_Pointers,
                            cCreate_IC_Trx         => ACFRec.Create_IC_Trx,
                            cCreate_Tax_Results    => ACFRec.Create_Tax_Results,
                            cMultiple_Owners       => ACFRec.Multiple_Owners,
                            cAssignment_set_id     => vAssignment_Set_id,
                            cAssign_Qualifier_id   => ASQRec.Assignment_Qualifier_id,
                            -- NCM 02.05.04
                            cDflt_Trading_Relationship => ASQRec.Dflt_Trading_Relationship,
                            cFormula_Set_Id        => ASQRec.formula_set_id,
                            cRef_Ctl_id            => ASQRec.Ref_Ctl_Id,
                            cTransaction_set_id    => cTransaction_set_id,
                            cDflt_CommExchCurr     => xxcp_global.GetCommonExchangeCurrency,
                            cStep_Number           => vStep_Number,
                            cKey                   => cKey,
                            cPrevious_Attribute_id => xxcp_global.gCommon(1).Previous_Attribute_Id,
                            cSourceRowid           => cSourceRowid,
                            cOls_date              => vOLS_date,
                            cOls_flag              => vOLS_flag,
                            cPass_Through_set_id   => ASQRec.Pass_Through_set_id,
                            cMC_Qualifier1         => vPC_Qualifier1,
                            cMC_Qualifier2         => vPC_Qualifier2,
                            cMC_Qualifier3         => vPC_Qualifier3,
                            cMC_Qualifier4         => vPC_Qualifier4,
                            cED_Qualifier1         => vED_Qualifier1,
                            cED_Qualifier2         => vED_Qualifier2,
                            cED_Qualifier3         => vED_Qualifier3,
                            cED_Qualifier4         => vED_Qualifier4,
                            cTax_Qualifier1        => vTax_Qualifier1,
                            cTax_Qualifier2        => vTax_Qualifier2,
                            cHeaderRowid           => vHeaderRowid,
                            cNew_Attribute_id      => vAttribute_Id,
                            cInternalErrorCode     => cInternalErrorCode);

              xxcp_trace.Assignment_Write_Trace;

              IF cInternalErrorCode = 0 THEN
                vAttributeExists := TRUE;
                vHAC_Count := vHAC_Count +1;
                IF ((NOT vAttributeExists) AND (xxcp_global.Preview_on <> 'Y')) THEN
                  cInternalErrorCode := 10025;
                ELSE
                  xxcp_global.gCommon(1).Previous_Attribute_Id := vAttribute_id;
                END IF;
              END IF;

              EXIT; -- Only perform for one qualifier set

            END LOOP;
            CLOSE ASQ;

            If cInternalErrorCode = 0 and ACFRec.ACQ_Required = 'Y' and Not vAS_Qualifer_Found then     
              cInternalErrorCode := 3121; -- New Error Code
            End If;

          END IF; -- Already Exists
          
          -- 02.06.01
        End If;
        
        End If;

        EXIT WHEN cInternalErrorCode <> 0;

      END LOOP;
      CLOSE ACF;
      -- END OF ACF
      -- Real Time Results
      If cSource_id in (17,43) and cInternalErrorCode = 0 then
        Set_RealTime_Results(cSource_Type_id => cSource_Type_id);
      End If;
      --
      -- Error handling
      --
      IF cInternalErrorCode <> 0 THEN
        If cInternalErrorCode between 7001 and 7099 then
          If cInternalErrorCode in (7022, 7024, 7026, 7028) then
            xxcp_te_base.SetPassThroughIgnore('Y');
          End If;
          xxcp_te_base.SetPassThroughPrvCode(cInternalErrorCode);
        Else
          xxcp_foundation.FndWriteError(cInternalErrorCode,
                                        'Source Assignment Id <' ||TO_CHAR(cSource_Assignment_ID) ||'> Transaction Table <' ||xxcp_global.gCommon(1).Current_Transaction_Table ||
                                         '> Type <' ||xxcp_global.gCommon(1).Current_Transaction_Type ||'> Date <' ||to_char(cTransaction_Date) || '>');
        End If;
      ELSIF ((NOT vAS_Qualifer_Found) AND (NOT vAttributeExists)) then
        If (vAnyAttributeFound) THEN
          cInternalErrorCode := 0;
        Else
          cInternalErrorCode := -1;
        End If;
      END IF;
    Else
      cInternalErrorCode := xxcp_te_base.GetPassThroughPrvCode;
    End If; -- gPassThoughIgnore

    If vHAC_Count = 0 and cInternalErrorCode = 0 then
      cInternalErrorCode := -1;  
    End If;

    cOutboundTab := gOutboundTab;

    xxcp_trace.Assignment_Write_Trace; -- Sweep up

    If xxcp_global.Timing_Adv_Mode = 'Y' and xxcp_global.Trace_on = 'Y' then
      xxcp_foundation.FndWriteError(100,'TRACE (Assignment - Custom Event Timing)', 'Custom Events '||chr(10)||xxcp_wks.Custom_Event_Log);
      xxcp_wks.Custom_Event_Log := Null;
    End If;
    --
    -- End Pass back processing information
    --
  END Main_Control;

  -- **************************************************************************************
  --  Control
  -- **************************************************************************************
  PROCEDURE Control(cSource_Assignment_ID   IN NUMBER,
                    cSet_of_books_id        IN NUMBER,
                    cTransaction_Date       IN DATE,
                    cSource_id              IN NUMBER,
                    cSource_Table_id        IN NUMBER,
                    cSource_Type_id         IN NUMBER,
                    cSource_Class_id        IN NUMBER,
                    cTransaction_id         IN NUMBER,
                    cSourceRowid            IN ROWID,
                    cParent_Trx_id          IN NUMBER,
                    cTransaction_Set_id     IN NUMBER,
                    cSpecial_Array          IN VARCHAR2,
                    cKey                    IN VARCHAR2,
                    cTransaction_Table      IN VARCHAR2,
                    cTransaction_Type       IN VARCHAR2,
                    cTransaction_Class      IN VARCHAR2,
                    cInternalErrorCode      IN OUT NUMBER) IS

    vOutboundTab  OutboundTab%Type;

  Begin

         gLastLadderSetId := 0;

         Main_Control(
                    cSource_Assignment_ID   => cSource_Assignment_ID,
                    cSet_of_books_id        => cSet_of_books_id,
                    cTransaction_Date       => cTransaction_Date,
                    cSource_id              => cSource_id,
                    cSource_Table_id        => cSource_Table_id ,
                    cSource_Type_id         => cSource_Type_id,
                    cSource_Class_id        => cSource_Class_id,
                    cTransaction_id         => cTransaction_id,
                    cSourceRowid            => cSourceRowid,
                    cParent_Trx_id          => cParent_Trx_id,
                    cTransaction_Set_id     => cTransaction_Set_id,
                    cSpecial_Array          => cSpecial_Array,
                    cKey                    => cKey,
                    cTransaction_Table      => cTransaction_Table,
                    cTransaction_Type       => cTransaction_Type,
                    cTransaction_Class      => cTransaction_Class,
                    cOutboundTab            => vOutboundTab,
                    cInternalErrorCode      => cInternalErrorCode);

  End Control;

-- Init
Begin

   xxcp_global.user_id    := fnd_global.user_id;
   xxcp_global.Systemdate := Sysdate;
   gClear_Labels(1).Custom_Attribute1_id := Null;
   -- Clear Working Storage
   xxcp_wks.P1001_Array := xxcp_wks.Clear_Dynamic;
   xxcp_wks.C1001_Array := xxcp_wks.Clear_Dynamic;

END XXCP_TE_CONFIGURATOR;
/
