CREATE OR REPLACE PACKAGE XXCP_PREVIEW_CTL AS
/******************************************************************************
                        V I R T A L  T R A D E R  L I M I T E D
            (Copyright 2004-2012 Virtual Trader Ltd. All rights Reserved)

   NAME:       XXCP_PREVIEW_CTL
   PURPOSE:    Preview Mode Control
   
   Version [03.05.15] Build Date [31-OCT-2012] Name [XXCP_PREVIEW_CTL]

******************************************************************************/

  Function Software_Version RETURN VARCHAR2;


  Function Control( cSource_Assignment_id    IN Number,
                    cSource_Rowid            IN Rowid,
                    cParent_trx_id           IN Number,
                    cPreview_id              IN Number,
                    cTransaction_Table       IN VARCHAR2,
                    cTransaction_Type        IN VARCHAR2,
                    cTransaction_Id          IN Number,
                    cRemoval_Process         IN Number,
                    cUser_id                 IN NUMBER,
                    cLogin_id                IN NUMBER,
                    cPV_Mode                 IN VARCHAR2,
                    cPV_Zero_Flag            IN VARCHAR2 Default 'N',
                    cJobStatus              OUT Number) return Number;
 


  Function Copy_Input_Row(
                     cSource_id            in number
                    ,cSource_Rowid         in Rowid
                    ,cSource_Table         in varchar2
                    ,cPreview_id           in number
                    --
                    ,cSource_Assignment_id in number
                    ,cTransaction_Table    in varchar2
                    ,cTransaction_id       in number
                    ,cParent_Trx_id        in number
                    ,cUnique_Request_id    in number
                    --
                    ,cPV_Mode              in varchar2 default null
                    ,cPreview_ctl          in number   default 0) return boolean;


  Function Journal_Control( 
                    cPreview_id              IN Number,
                    cJournal_Header_id       IN Number
                    ) return Number;
                    
  Function Bug_Test(cUser_Name            in varchar2,
                    cSource_Assignment_id in number,
                    cInterface_id         in number) return number;                    

END XXCP_PREVIEW_CTL;
/
