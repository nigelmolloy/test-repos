CREATE OR REPLACE PACKAGE BODY XXCP_PV_CPA_ENGINE AS
/******************************************************************************
                        V I R T A L  T R A D E R  L I M I T E D
  		      (Copyright 2008-2009 Virtual Trader Ltd. All rights Reserved)

   NAME:       XXCP_PV_CPA_ENGINE
   PURPOSE:    CPA Engine in Preview Mode

   REVISIONS:
   Ver        Date      Author  Description
   ---------  --------  ------- ------------------------------------
   02.04.00   15/12/08  Simon   First Coding
   02.04.01   19/02/09  Keith   Segments using Mapping
   02.04.02   24/03/09  Keith   Now using Collection_Period_id
   02.04.03   03/04/09  Simon   Use Collector Instance view in GLI cursor.
   02.04.04   09/04/09  Simon   Add Data Source to GLI and package variable.
   02.04.05   01/05/09  Simon   Add Category Data Source.
   02.04.06   14/05/09  Simon   Check for xxcp_global.Replan_on.
   02.04.07   18/05/09  Simon   Add Instance_id.
   02.04.08   18/05/09  Simon   Accounting date now based on Posting Period.
   02.04.09   04/06/09  Simon   Add CPA_Type and True_Up_On for Trial True-up.
   02.04.10   02/07/09  Simon   Add I1009_ArrayGLOBAL.
   02.04.11   13/07/09  Simon   Add "VT_REPLAN_" columns instead of attributes.
   02.04.12   28/07/09  Simon   Modify Get_Payer_Details.
   02.04.13   03/09/09  Simon   Remove CPA Trace output.
   02.04.14   21/09/09  Simon   Added xxcp_global.gDefCostCatUsed.
   02.06.01   21/10/09  Simon   Grouping now done by transaction_table not assignment_id..
   02.06.02   28/12/09  Keith   Added Calc_legal_exch_rate   
   03.06.03   20/04/12  Simon   Fixed TER cursor to use company_number instead of Tax_ragistration_id (OOD-111). 
   03.06.04   19/09/12  Simon   Added Cost_Plus_Set_Id (OOD-293). 
   03.06.05   19/12/12  Simon   Added Dynamic Explosion.
******************************************************************************/

  vTiming             xxcp_global.gTiming_rec;
	vTimingCnt          pls_integer;

  -- Current Records
  gBalancing_Array    XXCP_DYNAMIC_ARRAY := XXCP_DYNAMIC_ARRAY(' ',' ',' ',' ',' ',' ',' ',' ',' ',' ');
  --gTrans_Table_Array  XXCP_DYNAMIC_ARRAY := XXCP_DYNAMIC_ARRAY();

  gForcasting                 boolean := FALSE;
  gCPA_Attr_Override_exists   boolean := FALSE;

  TYPE gCursors_Rec is RECORD(
    v_cursorId     pls_Integer := 0,
    v_Target_Table XXCP_column_rules.transaction_table%type,
    v_usage        varchar2(1),
    v_OpenCur      varchar2(1) := 'N',
    v_LastCur      pls_integer := -1);

  TYPE gCUR_REC is TABLE of gCursors_Rec INDEX by BINARY_INTEGER;
  gCursors gCUR_REC;


  -- *************************************************************
  --                     Software Version Control
  --                   -----------------------------
  -- This package can be checked with SQL to ensure it's installed
  -- select packagename.software_version from dual;
  -- *************************************************************
  Function Software_Version RETURN VARCHAR2 IS
   BEGIN
     RETURN('Version [03.06.05] Build Date [19-DEC-2012] Name [XXCP_PV_CPA_ENGINE]');
   END Software_Version;


  -- !! ***********************************************************************************************************
  -- !!
  -- !!                                             Fetch_Source_Rowid
  -- !!
  -- !! ***********************************************************************************************************
	Function Fetch_Source_Rowid(cRowid in rowid) Return Rowid is

   Cursor SX (pSource_Rowid in rowid) is
      Select vt_source_rowid
        from XXCP_PV_INTERFACE l
       where rowid            = pSource_Rowid
         and l.vt_preview_id  = xxcp_global.gCommon(1).Preview_id;

  vReturn_rowid rowid;

  Begin
	  For SXRec in SX(cRowid) Loop
      vReturn_rowid := SXRec.vt_source_rowid;
    End Loop;

		Return(vReturn_rowid);

  End Fetch_Source_Rowid;

  -- !! ***********************************************************************************************************
  -- !!
  -- !!                                             Open_Cursors
  -- !!
  -- !! ***********************************************************************************************************
  Function Open_Cursor(cUsage in varchar2, cTarget_Table in varchar2) Return pls_integer is

    i    pls_integer;
    vPos pls_integer := 0;

  Begin
    For i in 1 .. gCursors.count loop
      If (gCursors(i).v_Target_Table = cTarget_Table) AND (gCursors(i).v_usage = cUsage) then
        vPos := i;
        If gCursors(i).v_OpenCur = 'N' then
          gCursors(i).v_cursorId := DBMS_SQL.OPEN_CURSOR;
          gCursors(i).v_OpenCur := 'Y';
          gCursors(i).v_LastCur := -1;
          exit;
        End If;
      End If;
    End Loop;

    Return(vPos);
  End Open_Cursor;

  -- *************************************************************
  --
  --                  ClearWorkingStorage
  --
  -- *************************************************************
  Procedure ClearWorkingStorage IS



  begin
    -- Processed Records
    xxcp_wks.WORKING_CNT := 0;
    xxcp_wks.WORKING_RCD.Delete;

    -- Current Records before processing
    xxcp_wks.SOURCE_CNT := 0;
    XXCP_WKS.SOURCE_ROWID.Delete;
    XXCP_WKS.SOURCE_STATUS.Delete;
    XXCP_WKS.SOURCE_SUB_CODE.Delete;
    -- Balancing
    xxcp_wks.BALANCING_CNT := 0;
    XXCP_WKS.BALANCING_RCD.Delete;

  End ClearWorkingStorage;

 --
 -- Category_Change_Transaction
 --
 Procedure Category_Change_Transaction(cInternalErrorCode  in out Number) is

 vVT_Source_Rowid Rowid;

 Begin
   -- Set all transactions in group to error status
   Begin
    Update XXCP_PV_INTERFACE
      set vt_status         = 'ERROR'
         ,vt_status_code    = cInternalErrorCode -- Associated errors
    where vt_Source_Assignment_id   = xxcp_global.gCommon(1).current_Assignment_id
      and vt_Parent_Trx_id          = xxcp_global.gCommon(1).current_Parent_Trx_id
      and vt_preview_id             = xxcp_global.gCommon(1).Preview_id
      and vt_Transaction_Table      = xxcp_global.gCommon(1).current_Transaction_Table;
   End;
   -- Update Record that actually caused the problem.
   Begin
     Update XXCP_PV_INTERFACE
        set VT_STATUS              = 'ERROR'
           ,VT_STATUS_CODE        = cInternalErrorCode
           ,VT_DATE_PROCESSED      = Sysdate
      where Rowid                  = xxcp_global.gCommon(1).current_source_rowid
        and vt_preview_id          = xxcp_global.gCommon(1).Preview_id;
   End;

   ClearWorkingStorage;

  End Category_Change_Transaction;
  
--
--   !! ***********************************************************************************************************
--   !!
--   !!                                   Error_Whole_Gl_Transaction
--   !!                If one part of the wrapper finds a problem whilst processing a record,
--   !!                          then we need to error the whole transaction.
--   !!
--   !! ***********************************************************************************************************
--
 Procedure Error_Whole_Transaction(cInternalErrorCode  in out Number) is

 vVT_Source_Rowid Rowid;

 Begin
   -- Set all transactions in group to error status
   If nvl(xxcp_global.gCommon(1).explosion_id,0) = 0 then
       -- Standard
       Update xxcp_pv_interface
         set vt_status = 'ERROR'
            ,vt_internal_Error_code    = 12823 -- Associated errors
       where vt_Source_Assignment_id   = xxcp_global.gCommon(1).current_Assignment_id
         and vt_Parent_Trx_id          = xxcp_global.gCommon(1).current_Parent_Trx_id
         and vt_preview_id             = xxcp_global.gCommon(1).Preview_id
         and vt_Transaction_Table      = xxcp_global.gCommon(1).current_Transaction_Table;
   Else
       -- Explosion
       Update xxcp_pv_interface
         set vt_status = 'ERROR'
            ,vt_internal_Error_code    = 12823 -- Associated errors
       where vt_Source_Assignment_id   = xxcp_global.gCommon(1).current_Assignment_id
         and vt_Parent_Trx_id          = xxcp_global.gCommon(1).current_Parent_Trx_id
         and vt_preview_id             = xxcp_global.gCommon(1).Preview_id
         and vt_Transaction_Table      = xxcp_global.gCommon(1).current_Transaction_Table
         and vt_status                != 'EXPLOSION';
   End if;
   
   
   -- Update Record that actually caused the problem.
   Begin
     If nvl(xxcp_global.gCommon(1).explosion_id,0) = 0 then
       -- Standard
       Update xxcp_pv_interface
          set VT_STATUS              = 'ERROR'
             ,VT_INTERNAL_ERROR_CODE = cInternalErrorCode
             ,VT_DATE_PROCESSED      = Sysdate
        where Rowid                  = xxcp_global.gCommon(1).current_source_rowid
          and vt_preview_id          = xxcp_global.gCommon(1).Preview_id;
     else
       -- Explosion
       Update xxcp_pv_interface
          set VT_STATUS_CODE         = 7060
             ,VT_INTERNAL_ERROR_CODE = cInternalErrorCode
             ,VT_DATE_PROCESSED      = Sysdate
        where vt_transaction_id      = xxcp_global.gCommon(1).Explosion_Transaction_id
          and vt_preview_id          = xxcp_global.gCommon(1).Preview_id;
     end if;
   End;

    If xxcp_global.gCommon(1).Custom_events = 'Y' then

		  vVT_Source_Rowid := fetch_source_rowid(xxcp_global.gCommon(1).current_source_rowid);

      xxcp_custom_events.ON_TRANSACTION_ERROR(xxcp_global.gCommon(1).Source_id,
                                              xxcp_global.gCommon(1).current_assignment_id,
                                              vVT_Source_Rowid,
                                              xxcp_global.gCommon(1).current_Transaction_Table,
                                              xxcp_global.gCommon(1).current_Parent_Trx_id,
                                              xxcp_global.gCommon(1).current_transaction_id,
                                              cInternalErrorCode);
    End If;

    ClearWorkingStorage;


End Error_Whole_Transaction;

--
--   !! ***********************************************************************************************************
--   !!
--   !!                                     No_Action_GL_Transaction
--   !!                                If there are no records to process.
--   !!
--   !! ***********************************************************************************************************
--
Procedure No_Action_GL_Transaction(cSource_Rowid in rowid) is
Begin
  -- Update Record that actually had not action required
  Begin
    Update XXCP_PV_INTERFACE
       set VT_STATUS              = 'SUCCESSFUL'
          ,VT_INTERNAL_ERROR_CODE = Null
          ,VT_DATE_PROCESSED      = xxcp_global.SystemDate
          ,VT_STATUS_CODE         = 7003
     where Rowid                  = cSource_Rowid
       and vt_preview_id          = xxcp_global.gCommon(1).Preview_id;
  End;
End No_Action_GL_Transaction;

/*
  !! ***********************************************************************************************************
  !!
  !!                                   Transaction_Group_Stamp
  !!                 Stamp Incomming record with parent Trx to create groups of
  ||                    transactions that resemble on document transaction.
  !!
  !! ***********************************************************************************************************
*/
Procedure Transaction_Group_Stamp(cStamp_Parent_Trx in varchar2,cSource_assignment_id in number, cInternalErrorCode out number) IS

 vStatement          varchar2(2000);
 vInternalErrorCode  Number(8) := 0;

 -- Distinct list of Transaction Tables
 -- populated in Set_Running_Status
 CURSOR cTrxTbl is
   SELECT transaction_table,
          nvl(grouping_rule_id,0) grouping_rule_id
   FROM   XXCP_sys_source_tables
   where  source_id = xxcp_global.gCommon(1).Source_id
   and    transaction_table in (select * from TABLE(CAST(xxcp_global.gTrans_Table_Array AS XXCP_DYNAMIC_ARRAY)));

 Cursor c1 (pSource_assignment_id in number,
            pTransaction_Table    IN VARCHAR2) IS
      select distinct 'Update XXCP_pv_interface j
                          set vt_parent_trx_id = (select '||t.Parent_trx_column|| ' from XXCP_cost_plus_interface k where k.rowid = j.vt_source_rowid)
                        where vt_status = ''ASSIGNMENT''
                          and vt_preview_id  = '||to_char(xxcp_global.gCommon(1).Preview_id)||'
                          and vt_source_assignment_id = '||to_char(pSource_assignment_id)||'
                          and vt_transaction_table = '''||g.vt_transaction_table||'''' Statement_line
           ,t.parent_trx_column
           ,t.Transaction_Table
        from XXCP_pv_interface g
            ,XXCP_sys_source_tables t
  	        ,XXCP_source_assignments x
       where g.vt_transaction_table    = t.transaction_table
         and t.source_id               = x.source_id
         and g.vt_preview_id           = xxcp_global.gCommon(1).preview_id
         and x.source_assignment_id    = psource_assignment_id
         and g.vt_source_assignment_id = x.source_assignment_id
         and g.vt_transaction_table    = pTransaction_Table;     -- 02.06.01

 Cursor er1(psource_assignment_id in number,
            pTransaction_Table    IN VARCHAR2) IS
      select g.vt_interface_id, g.rowid source_rowid, g.vt_transaction_table
        from XXCP_pv_interface g
       where g.vt_status = 'ASSIGNMENT'
         and g.vt_source_assignment_id = psource_assignment_id
         and g.vt_preview_id           = xxcp_global.gCommon(1).preview_id
         and g.vt_transaction_table    = pTransaction_Table      -- 02.06.01
         and g.vt_parent_trx_id is null;

 Begin

     For cTblRec in cTrxTbl Loop  -- Loop for all Transaction Tables within SA.

        If cTblRec.Grouping_Rule_Id > 0 THEN  --02.06.02
          XXCP_DYNAMIC_SQL.Apply_Grouping_Rules(cInternalErrorCode => vInternalErrorCode,
                                                cTransaction_Table => cTblRec.Transaction_Table);

          -- Error Checking for Null Parent Trx id
          For er1Rec in er1(cSource_assignment_id,
                            cTblRec.transaction_table) LOOP
           update XXCP_pv_interface l
              set vt_status                = 'ERROR'
                 ,vt_parent_trx_id         = Null
                 ,l.vt_internal_error_code = 11090
            where l.vt_transaction_table = er1rec.vt_transaction_table
              and l.vt_preview_id        = xxcp_global.gCommon(1).preview_id;
          End loop;

        Else

          -- Mass update of Parent Trx id
          For rec in c1(cSource_assignment_id,
                        cTblRec.transaction_table) LOOP

             If rec.parent_trx_column is null then
                vInternalErrorCode := 10665;
                xxcp_foundation.FndWriteError(vInternalErrorCode,'Transaction Table <'||Rec.Transaction_Table||'>');
             Else
               vStatement := rec.Statement_line;
               Begin
                Execute Immediate vStatement;

                Exception when OTHERS then
                   vInternalErrorCode := 10667;
                    xxcp_foundation.FndWriteError(vInternalErrorCode,SQLERRM,vStatement);
               End;
             End If;
          End loop;
        End If;
      End Loop;
      
      cInternalErrorCode := vInternalErrorCode;

  End Transaction_Group_Stamp;

  -- ************************************************************************
  --
  --       FIND_DYNAMICATTRIBUTE 
  --
  -- Reads the WORKING_RCD Array that is being used in the Flush
  -- ************************************************************************
  Function Find_DynamicAttribute(cPosition in number,
                                 cRecord   in number) Return Varchar2 is

    vAttribute varchar2(100);

  Begin

   If nvl(cPosition,0) != 0 then
    If cPosition between 9000 and 9999 then
        vAttribute := RTRIM(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(cPosition - 9000));
    ElsIf cPosition between 1000 and 1999 then
      -- Assignment
        vAttribute := RTRIM(xxcp_wks.WORKING_RCD(cRecord).D1001_Array(cPosition - 1000));
    ElsIf cPosition between 2000 and 2999 then
      -- IC Pricing
        vAttribute := RTRIM(xxcp_wks.WORKING_RCD(cRecord).D1002_Array(cPosition - 2000));
    ElsIf cPosition between 3000 and 3999 then
      -- Transaction
        vAttribute := RTRIM(xxcp_wks.WORKING_RCD(cRecord).D1003_Array(cPosition - 3000));
    ElsIf cPosition between 4000 and 4999 then
      -- Transaction Pricing
        vAttribute := RTRIM(xxcp_wks.WORKING_RCD(cRecord).D1004_Array(cPosition - 4000));
    ElsIf cPosition between 6000 and 6999 then
      -- Column Attributes
        vAttribute := RTRIM(xxcp_wks.WORKING_RCD(cRecord).D1006_Array(cPosition - 6000));
    ElsIf cPosition between 5000 and 5999 then
      -- Custom Events
        vAttribute := RTRIM(xxcp_wks.WORKING_RCD(cRecord).D1005_Array(cPosition - 5000));
    End If;

   End If;

   Return(vAttribute);
  
  Exception
    When others then
      Return(null); 
  
  End Find_DynamicAttribute;
  --
  -- Check_CPA_Attribute_Override
  --
  Procedure Check_CPA_Attribute_Override is
    Cursor c1 is select 'Y'
                 from   xxcp_sys_profile
                 where  profile_name in ('UPLIFT RATE ATTRIBUTE',
                                         'GROWTH RATE ATTRIBUTE',
                                         'AVG PERIOD COST ATTRIBUTE',
                                         'TOTAL COST ATTRIBUTE',
                                         'CONSIDERED PERIODS ATTRIBUTE',
                                         'CURRENCY CODE ATTRIBUTE',
                                         'COST CATEGORY ID ATTRIBUTE',
                                         'UPLIFTED AMOUNT ATTRIBUTE',
                                         'TRUE UP AMOUNT ATTRIBUTE',
                                         'PERIODS REQUIRED ATTRIBUTE',
                                         'PAYER1 ATTRIBUTE',
                                         'PAYER1 PERCENT ATTRIBUTE',
                                         'PAYER2 ATTRIBUTE',
                                         'PAYER2 PERCENT ATTRIBUTE',
                                         'INTERMEDIATE PAYER ATTRIBUTE',
                                         'ACCOUNT ATTRIBUTE')
                 and    profile_category = 'CP';  
  Begin
    for c1_rec in c1 loop
      gCPA_Attr_Override_exists := TRUE;
      exit;
    end loop; 
  End Check_CPA_Attribute_Override;

  --
  -- Set_CPA_Internal_Attributes
  --
  Procedure Set_CPA_Internal_Attributes (cRecord IN NUMBER) is
    
  Begin
     
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(106) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(106),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('UPLIFT RATE ATTRIBUTE'),cRecord));
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(107) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(107),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('GROWTH RATE ATTRIBUTE'),cRecord));
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(108) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(108),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('AVG PERIOD COST ATTRIBUTE'),cRecord));
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(109) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(109),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('TOTAL COST ATTRIBUTE'),cRecord));
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(110) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(110),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('CONSIDERED PERIODS ATTRIBUTE'),cRecord));
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(111) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(111),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('CURRENCY CODE ATTRIBUTE'),cRecord));
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(112) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(112),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('COST CATEGORY ID ATTRIBUTE'),cRecord));
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(114) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(114),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('UPLIFTED AMOUNT ATTRIBUTE'),cRecord));
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(116) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(116),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('TRUE UP AMOUNT ATTRIBUTE'),cRecord));
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(122) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(122),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('PERIODS REQUIRED ATTRIBUTE'),cRecord));
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(123) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(123),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('PAYER1 ATTRIBUTE'),cRecord));
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(124) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(124),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('PAYER1 PERCENT ATTRIBUTE'),cRecord));
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(125) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(125),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('PAYER2 ATTRIBUTE'),cRecord));
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(126) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(126),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('PAYER2 PERCENT ATTRIBUTE'),cRecord));
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(127) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(127),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('INTERMEDIATE PAYER ATTRIBUTE'),cRecord));
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(128) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(128),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('ACCOUNT ATTRIBUTE'),cRecord));

  End Set_CPA_Internal_Attributes;

  --
  -- Process_Fcast_Hist_Insert
  --
  Procedure Process_Fcast_Hist_Insert ( cPeriod_Set_Name_id      in number
                                       ,cSource_Rowid            in  Rowid
                                       ,cParent_Rowid            in  Rowid
                                       ,cTarget_table            in  varchar2
                                       ,cTarget_Instance_id      in  number
                                       ,cProcess_history_id      in  number
                                       ,cTarget_assignment_id    in  number
                                       ,cAttribute_id            in  number
                                       ,cRecord_type             in  varchar2
                                       ,cRequest_id              in  number
                                       ,cStatus                  in  varchar2
                                       ,cInterface_id            in  number
                                       ,cModel_Ctl_id            in  number
                                       ,cRule_id                 in  number
                                       ,cTax_registration_id     in  number
                                       ,cEntered_Rounding_Amount in  number
                                       ,cAccount_Rounding_amount in  number
                                       ,cD1001_Array             in  xxcp_dynamic_array
                                       ,cD1002_Array             in  xxcp_dynamic_array
                                       ,cD1003_Array             in  xxcp_dynamic_array
                                       ,cD1004_Array             in  xxcp_dynamic_array
                                       ,cD1005_Array             in  xxcp_dynamic_array
                                       ,cD1006_Array             in  xxcp_dynamic_array
                                       ,cI1009_Array             in  xxcp_dynamic_array
                                       ,cErrorMessage           out  NOCOPY varchar2
                                       ,cInternalErrorCode   in out  NOCOPY number) is

  Begin

        Insert into XXCP_PV_CPA_HISTORY
                   ( PREVIEW_ID, 
                     CPA_TYPE,
                     PROCESS_HISTORY_ID,
                     SOURCE_ASSIGNMENT_ID,
                     PERIOD_SET_NAME_ID,
                     PERIOD_ID,
                     TRANSACTION_ID,
                     TARGET_ASSIGNMENT_ID,
                     TRADING_SET_ID,
                      INTERFACE_ID,
                     ACCOUNTING_DATE, 
                     COMPANY   ,
                     DEPARTMENT,
                     ACCOUNT   , 
                     UPLIFT_RATE       ,
                     GROWTH_RATE       ,
                     AVG_PERIOD_COST   ,
                     TOTAL_COST       ,
                     UPLIFTED_AMOUNT,
                     COST_CATEGORY_ID  ,
                     CURRENCY_CODE     ,
                     CONSIDERED_PERIODS,
                     PERIODS_REQUIRED, 
                     TAX_AGREEMENT_NUMBER, 
                     CREATION_DATE     ,
                     CREATED_BY        ,
                     LAST_UPDATE_LOGIN
                   )
                   VALUES
                   ( xxcp_global.gCommon(1).Preview_id, 
                     'F',
                     cProcess_History_ID,
                     xxcp_global.gCommon(1).current_assignment_id,
                     cPeriod_Set_Name_id,
                     xxcp_wks.gActual_Costs_Rec(1).Collection_Period_id, -- Period Id
                     cI1009_Array(28),
                     cTarget_assignment_id,
                     xxcp_global.gCommon(1).current_trading_set_id,
                     cInterface_id,
                     cI1009_Array(52), -- Accounting Date
                     xxcp_wks.gActual_Costs_Rec(1).Company, -- Company
                     xxcp_wks.gActual_Costs_Rec(1).Department, -- Department
                     xxcp_wks.gActual_Costs_Rec(1).Account, -- Account
                     cI1009_Array(106), -- Uplift
                     cI1009_Array(107), -- Growth Rate
                     cI1009_Array(108), -- Avg Period Cost
                     cI1009_Array(109), -- Total Cost
                     cI1009_Array(114), -- Uplifted
                     cI1009_Array(112), -- Cost Category id
                     cI1009_Array(111), -- Currency Code
                     cI1009_Array(110), -- Considered Periods
                     cI1009_Array(122), -- Periods Required
                     cI1009_Array(171), -- Tax Agreement Number
                     --
                     xxcp_global.Systemdate,
                     xxcp_global.User_id,
                     xxcp_global.Login_id
                    );


    Exception when OTHERS then
       cErrorMessage := 'Error Inserting into XXCP_PV_CPA_HISTORY - '||SQLERRM;
       cInternalErrorCode := 12809;
       
  End Process_Fcast_Hist_Insert;
  
  --
  -- Process_True_Up_Hist_Insert
  --
  Procedure Process_True_Up_Hist_Insert(cPeriod_Set_Name_id      in  number
                                       ,cSource_Rowid            in  Rowid
                                       ,cParent_Rowid            in  Rowid 
                                       ,cTarget_table            in  varchar2
                                       ,cTarget_Instance_id      in  number
                                       ,cProcess_history_id      in  number
                                       ,cTarget_assignment_id    in  number
                                       ,cAttribute_id            in  number
                                       ,cRecord_type             in  varchar2
                                       ,cRequest_id              in  number
                                       ,cStatus                  in  varchar2
                                       ,cInterface_id            in  number
                                       ,cModel_Ctl_id            in  number
                                       ,cRule_id                 in  number
                                       ,cTax_registration_id     in  number
                                       ,cEntered_Rounding_Amount in  number
                                       ,cAccount_Rounding_amount in  number
                                       ,cI1009_Array             in  xxcp_dynamic_array
                                       ,cErrorMessage           out  NOCOPY varchar2
                                       ,cInternalErrorCode   in out  NOCOPY number) is

  Begin

        Insert into XXCP_pv_cpa_history
                   ( PREVIEW_ID, 
                     CPA_TYPE,
                     PROCESS_HISTORY_ID,
                     PERIOD_ID,
                     SOURCE_ASSIGNMENT_ID,
                     TRANSACTION_ID,
                     TARGET_ASSIGNMENT_ID,
                     TRADING_SET_ID,
                     INTERFACE_ID,
                     ACCOUNTING_DATE, 
                     COMPANY   ,
                     DEPARTMENT,
                     ACCOUNT   , 
                     UPLIFT_RATE       ,
                     GROWTH_RATE       ,
                     AVG_PERIOD_COST   ,
                     TOTAL_COST       ,
                     TRUE_UP_AMOUNT,
                     COST_CATEGORY_ID  ,
                     CURRENCY_CODE     ,
                     CONSIDERED_PERIODS,
                     PERIOD_SET_NAME_ID,
                     TAX_AGREEMENT_NUMBER, 
                     CREATION_DATE     ,
                     CREATED_BY        ,
                     LAST_UPDATE_LOGIN
                   )
                   VALUES
                   ( xxcp_global.gCommon(1).Preview_id, 
                     'T',
                     cProcess_History_ID,
                     xxcp_wks.gActual_Costs_Rec(1).Collection_Period_id, -- Period Id
                     xxcp_global.gCommon(1).current_assignment_id,
                     cI1009_Array(28),
                     cTarget_assignment_id,
                     to_number(xxcp_global.gCommon(1).current_trading_set_id),
                     cInterface_id,
                     cI1009_Array(52), -- Accounting Date 
                     xxcp_wks.gActual_Costs_Rec(1).Company, -- Company
                     xxcp_wks.gActual_Costs_Rec(1).Department, -- Department
                     xxcp_wks.gActual_Costs_Rec(1).Account, -- Account 
                     cI1009_Array(106), -- Uplift
                     cI1009_Array(107), -- Growth Rate
                     cI1009_Array(108), -- Avg Period Cost
                     cI1009_Array(109), -- Total Cost
                     cI1009_Array(116), -- True Up Amount
                     cI1009_Array(112), -- Cost Category id
                     cI1009_Array(111), -- Currency Code
                     cI1009_Array(110), -- Considered Periods
                     cPeriod_Set_Name_id, 
                     cI1009_Array(171), -- Tax Agreement Number
                     xxcp_global.Systemdate,
                     xxcp_global.User_id,
                     xxcp_global.Login_id
                    );

  Exception when OTHERS then
    cErrorMessage := 'Error Inserting into XXCP_PV_CPA_HISTORY - '||SQLERRM;
    cInternalErrorCode := 12808;
  End Process_True_Up_Hist_Insert;


 --
 -- !! ***********************************************************************************************************
 -- !!
 -- !!                                   FlushRecordsToTarget
 -- !!     Control The process of moving the Array elements throught to creating the new output records
 -- !!
 -- !! ***********************************************************************************************************
  Function FlushRecordsToTarget(cPeriod_Set_Name_id        in number,
                                cGlobal_Rounding_Tolerance in number,
                                cGlobalPrecision           in number,
                                cTransaction_Type          in varchar2,
                                cInternalErrorCode         in out number)
    return number is

    j                  pls_integer := 0;
    fe                 pls_integer := xxcp_wks.WORKING_CNT;
    c                  pls_integer := 0;
    vCnt               pls_integer := 0;
    vEntered_DR        Number      := 0;
    vEntered_CR        Number      := 0;
    vRC                pls_integer := xxcp_wks.SOURCE_CNT;
    vRC_Records        pls_integer := 0;
    vRC_Error          pls_integer := 0;

    vStatus            XXCP_cost_plus_interface.vt_status%type;

    vD1005_Array       xxcp_dynamic_array; 

    vErrorMessage      varchar2(3000);
    UDI_Cnt            pls_integer := 0;
    vVT_Source_Rowid   Rowid;
    vZero_Action       pls_integer := 0;

BEGIN
    If fe > 0 then
      xxcp_te_base.Account_Rounding(cGlobal_Rounding_Tolerance,
                                    cGlobalPrecision,
                                    cTransaction_Type,
                                    vEntered_DR,
                                    vEntered_CR,
                                    cInternalErrorCode);
    End If;

    -- Check that for each attribute record we have a records for output.
    For c in 1 .. vRC Loop
      vRC_Error := 0;
      FOR j in 1 .. fe LOOP
        If xxcp_wks.Source_rowid(c) = xxcp_wks.WORKING_RCD(j).Source_rowid then
          xxcp_wks.WORKING_RCD(j).Source_Pos := c;
          xxcp_wks.SOURCE_STATUS(c) := 'FLUSH';
          vRC_Error   := 1;
          vRC_Records := vRC_Records + 1;
        end if;
      End loop;
      IF vRC_Error = 0 THEN
          xxcp_wks.SOURCE_SUB_CODE(c) := 7003;
          xxcp_wks.SOURCE_STATUS(c) := 'NO RULES';
          No_Action_GL_Transaction(xxcp_wks.Source_rowid(c));
      END IF;
    END LOOP;

    Savepoint TARGET_INSERT;

    If cInternalErrorCode = 0 and vRC_Records > 0 then

      For j in 1 .. fe Loop

        Exit when cInternalErrorCode <> 0;

        vCnt := vCnt + 1;

        -- Custom Events
        If xxcp_global.gCommon(1).Custom_events = 'Y' then
   				xxcp_te_base.Custom_Event_Insert_Row(j,cInternalErrorCode);
        End If;

        -- Override CPA Internal Attributes?
        If gCPA_Attr_Override_exists then
          Set_CPA_Internal_Attributes(j);
        End if;

        -- Tag Zero Account Value rows
        If cInternalErrorCode = 0 then

		      xxcp_global.gCommon(1).current_source_rowid := xxcp_wks.Source_rowid(xxcp_wks.WORKING_RCD(j).Source_Pos);

					-- Find the Action to take for Zero Accounted values
          vZero_Action := xxcp_te_base.Zero_Suppression_Rule(j);

          IF vZero_Action > 0 then

                 Select XXCP_PREVIEW_HIST_SEQ.nextval into xxcp_wks.WORKING_RCD(j).Process_History_id from dual;

                 vVT_Source_Rowid := fetch_source_rowid(xxcp_wks.WORKING_RCD(j).Source_rowid);

                 If cInternalErrorCode = 0 then


                    ---
                    --- XXCP_PROCESS_HISTORY
                    ---
                    XXCP_PV_PROCESS_HIST_CPA.Process_History_Insert(
                                                              cSource_Rowid            => vVT_Source_Rowid,  -- Preview Mode only,
																											        cParent_Rowid            => Null,
                                                              cTarget_Table            => xxcp_wks.WORKING_RCD(j).Target_Table,
                                                              cTarget_Instance_id      => xxcp_wks.WORKING_RCD(j).Target_Instance_id,
                                                              cProcess_History_id      => xxcp_wks.WORKING_RCD(j).Process_History_id,
                                                              cTarget_assignment_id    => xxcp_wks.WORKING_RCD(j).Target_Assignment_id,
                                                              cAttribute_id            => xxcp_wks.WORKING_RCD(j).Attribute_id,
                                                              cRecord_type             => xxcp_wks.WORKING_RCD(j).Record_type,
                                                              cRequest_id              => xxcp_global.gCommon(1).current_request_id,
                                                              cStatus                  => xxcp_wks.WORKING_RCD(j).Record_Status,
                                                              cInterface_id            => xxcp_wks.WORKING_RCD(j).Interface_id,
                                                              cModel_ctl_id            => xxcp_wks.WORKING_RCD(j).Model_ctl_id,
                                                              cRule_id                 => xxcp_wks.WORKING_RCD(j).Rule_id,
                                                              cTax_registration_id     => xxcp_wks.WORKING_RCD(j).Tax_Registration_id,
																															cEntered_rounding_amount => xxcp_wks.WORKING_RCD(j).Entered_Rnd_Amount,
																															cAccount_rounding_amount => xxcp_wks.WORKING_RCD(j).Accounted_Rnd_Amount,
                                                              cD1001_Array             => xxcp_wks.WORKING_RCD(j).D1001_Array,
                                                              cD1002_Array             => xxcp_wks.WORKING_RCD(j).D1002_Array,
                                                              cD1003_Array             => xxcp_wks.WORKING_RCD(j).D1003_Array,
                                                              cD1004_Array             => xxcp_wks.WORKING_RCD(j).D1004_Array,
																											        cD1005_Array             => xxcp_wks.WORKING_RCD(j).D1005_Array,
                                                              cD1006_Array             => xxcp_wks.WORKING_RCD(j).D1006_Array,
                                                              cI1009_Array             => xxcp_wks.WORKING_RCD(j).I1009_Array,
                                                              cErrorMessage            => vErrorMessage,
                                                              cInternalErrorCode       => cInternalErrorCode);


                               If xxcp_wks.WORKING_RCD(j).cost_plus_rule = 'Y' then   -- If Cost-Plus Rule
                                 If gForcasting Then   -- If Cost Plus Rule
                                   Process_Fcast_Hist_Insert(
                                                     cPeriod_Set_Name_id      => cPeriod_Set_Name_id,
                                                     cSource_Rowid            => xxcp_wks.WORKING_RCD(j).Source_rowid,
                                                     cParent_Rowid            => Null,
                                                     cTarget_Table            => xxcp_wks.WORKING_RCD(j).Target_Table,
                                                     cTarget_Instance_id      => xxcp_wks.WORKING_RCD(j).Target_Instance_id,
                                                     cProcess_History_id      => xxcp_wks.WORKING_RCD(j).Process_History_id,
                                                     cTarget_assignment_id    => xxcp_wks.WORKING_RCD(j).Target_Assignment_id,
                                                     cAttribute_id            => xxcp_wks.WORKING_RCD(j).Attribute_id,
                                                     cRecord_type             => xxcp_wks.WORKING_RCD(j).Record_type,
                                                     cRequest_id              => xxcp_global.gCommon(1).current_request_id,
                                                     cStatus                  => xxcp_wks.WORKING_RCD(j).Record_Status,
                                                     cInterface_id            => xxcp_wks.WORKING_RCD(j).Interface_id,
                                                     cModel_ctl_id            => xxcp_wks.WORKING_RCD(j).Model_ctl_id,
                                                     cRule_id                 => xxcp_wks.WORKING_RCD(j).Rule_id,
                                                     cTax_registration_id     => xxcp_wks.WORKING_RCD(j).Tax_Registration_id,
                                                     cEntered_rounding_amount => xxcp_wks.WORKING_RCD(j).Entered_Rnd_Amount,
                                                     cAccount_rounding_amount => xxcp_wks.WORKING_RCD(j).Accounted_Rnd_Amount,
                                                     cD1001_Array             => xxcp_wks.WORKING_RCD(j).D1001_Array,
                                                     cD1002_Array             => xxcp_wks.WORKING_RCD(j).D1002_Array,
                                                     cD1003_Array             => xxcp_wks.WORKING_RCD(j).D1003_Array,
                                                     cD1004_Array             => xxcp_wks.WORKING_RCD(j).D1004_Array,
                                                     cD1005_Array             => xxcp_wks.WORKING_RCD(j).D1005_Array,
                                                     cD1006_Array             => xxcp_wks.WORKING_RCD(j).D1006_Array,
                                                     cI1009_Array             => xxcp_wks.WORKING_RCD(j).I1009_Array,
                                                     cErrorMessage            => vErrorMessage,
                                                     cInternalErrorCode       => cInternalErrorCode);
                                     Else
                                       Process_True_Up_Hist_Insert(
                                                     cPeriod_Set_Name_id      => cPeriod_Set_Name_id,
                                                     cSource_Rowid            => xxcp_wks.WORKING_RCD(j).Source_rowid,
                                                     cParent_Rowid            => Null,
                                                     cTarget_Table            => xxcp_wks.WORKING_RCD(j).Target_Table,
                                                     cTarget_Instance_id      => xxcp_wks.WORKING_RCD(j).Target_Instance_id,
                                                     cProcess_History_id      => xxcp_wks.WORKING_RCD(j).Process_History_id,
                                                     cTarget_assignment_id    => xxcp_wks.WORKING_RCD(j).Target_Assignment_id,
                                                     cAttribute_id            => xxcp_wks.WORKING_RCD(j).Attribute_id,
                                                     cRecord_type             => xxcp_wks.WORKING_RCD(j).Record_type,
                                                     cRequest_id              => xxcp_global.gCommon(1).current_request_id,
                                                     cStatus                  => xxcp_wks.WORKING_RCD(j).Record_Status,
                                                     cInterface_id            => xxcp_wks.WORKING_RCD(j).Interface_id,
                                                     cModel_ctl_id            => xxcp_wks.WORKING_RCD(j).Model_ctl_id,
                                                     cRule_id                 => xxcp_wks.WORKING_RCD(j).Rule_id,
                                                     cTax_registration_id     => xxcp_wks.WORKING_RCD(j).Tax_Registration_id,
                                                     cEntered_rounding_amount => xxcp_wks.WORKING_RCD(j).Entered_Rnd_Amount,
                                                     cAccount_rounding_amount => xxcp_wks.WORKING_RCD(j).Accounted_Rnd_Amount,
                                                     cI1009_Array             => xxcp_wks.WORKING_RCD(j).I1009_Array,
                                                     cErrorMessage            => vErrorMessage,
                                                     cInternalErrorCode       => cInternalErrorCode);
                                     End If;
                                  
                                  End If;


                     If cInternalErrorCode = 0 then
                       vTiming(vTimingCnt).Flush_Records := vTiming(vTimingCnt).Flush_Records + 1;
										 End If;
                  End If;

                  If cInternalErrorCode = 0 then
                     vStatus := 'SUCCESSFUL';
				          Else
                     vStatus := 'ERROR';
                     xxcp_wks.SOURCE_SUB_CODE(xxcp_wks.WORKING_RCD(j).Source_Pos):= Null;
                  End If;

				          xxcp_wks.SOURCE_STATUS(xxcp_wks.WORKING_RCD(j).Source_Pos) := vStatus;
            Else
              -- ZAV
              xxcp_wks.SOURCE_SUB_CODE(xxcp_wks.WORKING_RCD(j).Source_Pos):= 7008;
			        xxcp_wks.SOURCE_STATUS(xxcp_wks.WORKING_RCD(j).Source_Pos) := 'SUCCESSFUL';
            End If;

			      UDI_Cnt := UDI_Cnt + 1;
          END IF;
      END LOOP;

      If cInternalErrorCode = 0 and UDI_Cnt > 0 then
        FORALL j in 1 .. xxcp_wks.SOURCE_CNT
          Update XXCP_pv_interface
             Set vt_Status              = xxcp_wks.SOURCE_STATUS(j),
                 vt_internal_Error_code = Null,
                 vt_date_processed      = xxcp_global.SystemDate,
                 vt_Status_Code         = decode(xxcp_global.gDefCostCatUsed,'Y',7016,xxcp_wks.SOURCE_SUB_CODE(j))
           where rowid = xxcp_wks.Source_rowid(j);
      End If;

    END IF;

	 -- Source_Pos

    If cInternalErrorCode <> 0 then
      -- Rollback
      If vCnt > 0 then
        Rollback To TARGET_INSERT;
        xxcp_foundation.FndWriteError(cInternalErrorCode, vErrorMessage);
      End If;

      Error_Whole_Transaction(cInternalErrorCode);

      If cInternalErrorCode = 12800 then
        xxcp_foundation.FndWriteError(cInternalErrorCode,
          'Entered DR <' ||to_char(vEntered_DR) ||'> Entered CR <' ||to_char(vEntered_CR) || '>');
      End If;

    End If;

    -- Remove Array Elements
    ClearWorkingStorage;

    Return(vCnt);
  End FlushRecordsToTarget;


  --  !! ***********************************************************************************************************
  --  !!
  --  !!                                   Reset_Errored_Transactions
  --  !!                              Reset Transactions from a previous run.
  --  !!
  --  !! ***********************************************************************************************************
  Procedure Reset_Errored_Transactions(cSource_assignment_id in number, cRequest_id in number) is

  begin

    vTiming(vTimingCnt).Reset_Start := to_char(sysdate, 'HH24:MI:SS');

    -- Update Interface
    Begin

      Update XXCP_pv_interface r
         set vt_status              = 'NEW',
             vt_Internal_Error_Code = Null,
             vt_status_code         = Null,
             vt_request_id          = cRequest_id
       where r.vt_source_assignment_id = cSource_assignment_id
			   and r.vt_preview_id           = xxcp_global.gCommon(1).Preview_id
         and (vt_status in
             ('ERROR', 'PROCESSING', 'TRANSACTION', 'ASSIGNMENT'));

    exception
      when OTHERS then
        null;
    End;

    -- Remove Errors
    Begin
      Delete from XXCP_pv_errors cx
       where source_assignment_id = cSource_assignment_id
			   and preview_id        = xxcp_global.gCommon(1).Preview_id;

      Exception
        when OTHERS then
          null;
    End;
    Commit;
  End RESET_ERRORED_TRANSACTIONS;


 -- !! ***********************************************************************************************************
 -- !!
 -- !!                                  Set_Running_Status
 -- !!                      Flag the records that are going to be processed.
 -- !!
 -- !! ***********************************************************************************************************
  Procedure Set_Running_Status(cSource_id            in number,
                               cSource_assignment_id in number,
                               cRequest_id           in number) is

    cursor rx(pSource_id in number, pSource_Assignment_id in number, pRequest_id in number) is
      select r.vt_transaction_table,
             r.vt_transaction_id,
             r.rowid source_rowid,
             r.vt_transaction_type,
             nvl(t.grouping_rule_id,0) grouping_rule_id
        from XXCP_pv_interface r, XXCP_sys_source_tables t
       where r.vt_preview_id = xxcp_global.gCommon(1).Preview_id
         and vt_source_assignment_id = pSource_assignment_id
         and vt_status = 'NEW'
         and r.vt_transaction_table = t.transaction_table
         and t.source_id = pSource_id
    order by t.transaction_table 
         for update;

    vGrouping_rule_id XXCP_cost_plus_interface.vt_grouping_rule_id%type;

    -- Find records with no vt_grouping_rule_id
    cursor er2(pSource_assignment_id in number) is
      select Distinct g.vt_transaction_table
        from XXCP_pv_interface g, XXCP_sys_source_tables t
       where g.vt_request_id = cRequest_id
         and g.vt_preview_id = xxcp_global.gCommon(1).Preview_id
         and g.vt_status = 'ASSIGNMENT'
         AND g.vt_transaction_table    = t.transaction_table
         and g.vt_source_assignment_id = pSource_assignment_id
         AND t.grouping_rule_id IS NOT NULL  -- 02.06.01
         and g.vt_grouping_rule_id is null;

    -- Find records with invalid Grouping rule
    cursor er3(pSource_assignment_id in number) is
      select Distinct g.vt_transaction_table
        from XXCP_pv_interface g, XXCP_grouping_rules r, XXCP_sys_source_tables t
       where g.vt_preview_id = xxcp_global.gCommon(1).Preview_id
         and g.vt_status = 'ASSIGNMENT'
         AND g.vt_transaction_table    = t.transaction_table
         and g.vt_source_assignment_id = pSource_assignment_id
         and g.vt_grouping_rule_id = r.grouping_rule_id(+)
         AND t.grouping_rule_id IS NOT NULL  -- 02.06.01
         and r.grouping_rule_id is null;

    vCurrent_request_id number;
    vGroupingRuleExists BOOLEAN     := FALSE;
    vPrevTransaction_Table XXCP_sys_source_tables.transaction_table%TYPE := '~NULL~';

  begin

    vTiming(vTimingCnt).Set_Running_Start := to_char(sysdate, 'HH24:MI:SS');

    vCurrent_Request_id := nvl(cRequest_id, 0);

    For rec in rx(cSource_id, cSource_assignment_id, vCurrent_Request_id) Loop
      vGrouping_rule_id := Null;

      -- Populate Transaction_Table Array
      if vPrevTransaction_Table <> rec.vt_transaction_table then
        xxcp_global.gTrans_Table_Array.extend;
        xxcp_global.gTrans_Table_Array(xxcp_global.gTrans_Table_Array.count) := rec.vt_transaction_table;
      End if;

      vPrevTransaction_Table := rec.vt_transaction_table;

      IF Rec.grouping_rule_id is not null then  -- 02.06.02
      
        vGroupingRuleExists := TRUE;

				xxcp_global.gCommon(1).current_transaction_table := Rec.vt_Transaction_Table;
        xxcp_global.gCommon(1).current_transaction_id    := Rec.vt_Transaction_id;

        vGrouping_rule_id := xxcp_custom_events.get_group_rule_id(cSource_id,
                                                                  cSource_assignment_id,
                                                                  rec.source_rowid,
                                                                  rec.vt_transaction_table,
                                                                  rec.vt_transaction_type);

        If nvl(vGrouping_rule_id, 0) = 0 then
          vGrouping_rule_id := Rec.grouping_rule_id; -- Default
        End If;
      End If;
      -- Set Processing Status
      begin
        update XXCP_pv_interface r
           set vt_status           = 'ASSIGNMENT',
               vt_date_processed   = xxcp_global.SystemDate,
               vt_status_code      = Null,
               vt_request_id       = cRequest_id,
               vt_Grouping_rule_id = vGrouping_rule_id
         where r.rowid = rec.source_rowid
				   and r.VT_PREVIEW_ID = xxcp_global.gCommon(1).Preview_id;

     -- Exception
     --   when OTHERS then null;
      End;
    End Loop;

    -- Error Checking
    If vGroupingRuleExists then  -- 02.06.01 Don't Check If no Grouping Rule used.
      -- Find records with no vt_grouping_rule_id
      For Rec in er2(cSource_assignment_id) loop
        update XXCP_pv_interface g
           set g.vt_status              = 'ERROR',
               g.vt_date_processed      = xxcp_global.SystemDate,
               g.vt_internal_error_code = 11088
         where g.vt_preview_id           = xxcp_global.gCommon(1).Preview_id
           and g.vt_status     = 'ASSIGNMENT'
           and g.vt_source_assignment_id = cSource_assignment_id
           and g.vt_transaction_table = rec.vt_transaction_table;
      End Loop;
      -- Find records with invalid Grouping rule
      For Rec in er3(cSource_assignment_id) loop
        update XXCP_pv_interface g
           set g.vt_status              = 'ERROR',
               g.vt_date_processed      = xxcp_global.SystemDate,
               g.vt_internal_error_code = 11089
         where vt_preview_id             = xxcp_global.gCommon(1).Preview_id
           and g.vt_status               = 'ASSIGNMENT'
           and g.vt_source_assignment_id = cSource_assignment_id
           and g.vt_transaction_table = rec.vt_transaction_table;
      End Loop;
    End If;

    Commit;

  End Set_Running_Status;

 -- !! ***********************************************************************************************************
 -- !!
 -- !!                                    CONTROL
 -- !!                  External Procedure (Entry Point) to start the AR Engine.
 -- !!
 -- !! ***********************************************************************************************************

  Function Control(cSource_Group_id         IN NUMBER,
	                 cSource_Assignment_id    IN NUMBER,
                   cPreview_id              IN NUMBER,
                   cParent_trx_id           IN NUMBER   DEFAULT 0,
                   cUser_id                 IN NUMBER,
                   cLogin_id                IN NUMBER,
									 cPV_Zero_Flag            IN VARCHAR2) RETURN NUMBER is


    -- Loop Controls vars
    i    pls_integer := 0;
    k    number;
    j    integer := 0;
    g    number  := 0;
    xw   number  := 0;
    gx   number  := 0;

    vPeriod_Set_Name_id number(2) := 1;

    -- Assignment
    cursor cfg(pSource_id in number, pSource_Assignment_id in Number, pRequest_id in number) is
                select x.vt_transaction_table,
                       x.vt_transaction_type,
                       x.vt_transaction_class,
                       x.vt_source_rowid,
                       x.vt_transaction_id,
                       x.vt_parent_trx_id,
											 x.vt_transaction_ref,
                       s.set_of_books_id,
                       x.rowid source_rowid,
                       Null currency_code,
                       0 entered_dr,
                       Null entered_cr,
                       s.source_assignment_id,
                       s.set_of_books_id source_set_of_books_id,
                       x.vt_interface_id,
                       w.transaction_set_name,
                       nvl(w.Transaction_Set_id,0) Transaction_Set_Id,
											 x.vt_transaction_date,
						           y.source_table_id,
						           y.source_type_id,
											 cx.source_class_id,
                       nvl(ac.company,
                           substr(x.vt_transaction_ref,1,instr(x.vt_transaction_ref,'-')-1)) company,
                       nvl(ac.department,'~NULL~') department,
                       nvl(ac.account,'~NULL~') account,
                       t.calc_legal_exch_rate,
                       g.cost_plus_set_id,
                       nvl(y.explosion_id,0) asg_explosion_id,
                       g.Tax_Agreement_Number
                  from XXCP_pv_interface            x,
                       XXCP_actual_costs            ac,
                       XXCP_source_assignments      s,
                       XXCP_cost_plus_interface     g,
											 XXCP_sys_source_tables       t,
                       XXCP_sys_source_types        y,
                       XXCP_sys_source_classes      cx,
                       XXCP_source_transaction_sets w
                 where x.vt_status               = 'ASSIGNMENT'
                   and x.vt_source_assignment_id = pSource_assignment_id
                   and x.vt_source_assignment_id = s.source_assignment_id
                   and g.rowid                   = x.vt_source_rowid
                   -- Actual Costs
           --        and g.vt_transaction_id       = ac.transaction_id
                   and decode(g.vt_transaction_type,'STD',g.vt_transaction_id,g.vt_replan_transaction_id) 
                                                 = ac.transaction_id (+)
                   and g.vt_source_assignment_id = ac.source_assignment_id (+)
                   -- Table
                   and g.vt_transaction_table    = t.transaction_table
                   and t.source_id               = pSource_id
                   -- Type
                   and g.vt_transaction_type     = y.type
									 and y.latch_only              = 'N'
                   and t.source_table_id         = y.source_table_id
                   -- Class
                   and g.vt_transaction_class    = decode(t.class_mapping_req,'Y',cx. system_latch,cx.class)
									 and cx.latch_only             = 'N'
                   and t.source_table_id         = cx.source_table_id
                   -- Transaction_Set_id
                   and y.Transaction_Set_id      = w.Transaction_Set_id
                   --
                   and x.vt_preview_id = xxcp_global.gCommon(1).Preview_id
                 order by g.vt_transaction_table, w.sequence,cx.sequence, x.vt_transaction_id;

    CFGRec CFG%rowtype;

    -- Transaction
    cursor GLI(pSource_id in number, pSource_Assignment_id in Number, pRequest_id in number) is
      select g.vt_transaction_table,
             g.vt_transaction_type,
             g.vt_transaction_class,
             x.vt_source_rowid,
             g.vt_interface_id,
             x.vt_transaction_id,
						 x.vt_transaction_ref,
             s.instance_id vt_instance_id,
		         Null Code_Combination_id,
             --
             -- Balancing segment
             'Cost-Plus'      be2, --  USER_JE_SOURCE_NAME
             0                be3, -- GROUP_ID
             'Cost-Plus'      be4, -- USER_JE_CATEGORY_NAME
             g.vt_transaction_Ref be6,
             Null       be7, -- Reference2
             Null       be8, -- Reference5
             -- Inventory
             0 Transaction_Cost,
             -- History
             'Cost-Plus' Category_name,
             'Cost-Plus' Source_name,
             g.vt_transaction_date  accounting_date,
             --
             Null parent_entered_currency,
             ac.currency_code currency_code,   
             x.vt_parent_trx_id,
             -- Fixed Values for the engine. The pricing engine should would out cost
             0 Group_id,
             0 Parent_Entered_Amount,
             ac.period_net_dr Entered_DR,
             ac.period_net_cr Entered_CR,
             0 Accounted_DR,
             Null Accounted_CR,
						 -- End Fixed Values
             x.rowid source_rowid,
             s.set_of_books_id,
             Null Segment1,
             Null Segment2,
             Null Segment3,
             Null Segment4,
             Null Segment5,
             Null Segment6,
             Null Segment7,
             Null Segment8,
             Null Segment9,
             Null Segment10,
             Null Segment11,
             Null Segment12,
             Null Segment13,
             Null Segment14,
             Null Segment15,
             s.source_assignment_id,
             s.set_of_books_id source_set_of_books_id,
             w.transaction_set_name,
             w.Transaction_Set_id,
						 --
						 t.source_table_id,
						 y.source_type_id,
						 cx.source_class_id,
						 t.class_mapping_req,
             -- Cost-Plus
             ac.Collection_Period_id,
             nvl(ac.company,
                 substr(x.vt_transaction_ref,1,instr(x.vt_transaction_ref,'-')-1)) company,
             nvl(ac.department,'~NULL~') department,
             nvl(ac.account,'~NULL~') account,
--             ac.fc_cost_category_id,
--             ac.tu_cost_category_id,
--             cc.cost_category_id fc_cost_category_id,
--             cc.cost_category_id tu_cost_category_id,
--             cc.category_data_source fc_category_data_source,
--             cc.category_data_source tu_category_data_source,
             ac.collector_id,
             ac.data_source,
             g.period_set_name_id,
--             mp.company company_seg,
--             mp.department department_seg,
--             mp.account account_seg,
             g.attribute1,
             g.attribute2,
             g.attribute3,
             g.attribute4,
             g.attribute5,
             g.attribute6,
             g.attribute7,
             g.attribute8,
             g.attribute9,
             g.attribute10,
             g.posting_period_id,
             ac.Period_End_Date,                          
						 g.vt_transaction_date,
             x.vt_results_attribute1 cost_category_id,
             x.vt_results_attribute2 category_data_source,
             x.vt_results_attribute3 cost_category,
             g.vt_replan_company,             
             g.vt_replan_department,          
             g.vt_replan_account,             
             g.vt_replan_transaction_id,      
             g.vt_replan_trading_set_id,      
             g.vt_replan_amount,              
             g.vt_replan_currency_code,       
             g.vt_replan_owner_tax_reg_id,    
             g.vt_replan_partner_tax_reg_id,  
             g.vt_replan_mc_qualifier1,       
             g.vt_replan_cost_category_id,    
             g.vt_replan_category_data_source,
             g.vt_replan_process_history_id,  
             g.vt_replan_period_id,           
             g.vt_replan_payer1,              
             g.vt_replan_payer1_percent,      
             g.vt_replan_payer2,              
             g.vt_replan_payer2_percent,      
             g.vt_replan_inter_payer,         
             g.vt_replan_account_type,
             g.cost_plus_set_id,
             nvl(y.explosion_id,0) asg_explosion_id,
             nvl(y.trx_explosion_id,0) trx_explosion_id,
             g.Tax_Agreement_Number
        from XXCP_cost_plus_interface g,
             XXCP_actual_costs        ac,
             XXCP_pv_interface        x,
             XXCP_source_assignments  s ,
--             xxcp_instance_cpa_collector_v cc, --XXCP_cost_plus_collector_ctl cc,
--             XXCP_cost_plus_mapping       mp,
             -- New
             XXCP_sys_source_tables       t,
             XXCP_sys_source_types        y,
             XXCP_sys_source_classes      cx,
             XXCP_source_transaction_sets w
       where g.rowid                   = x.vt_source_rowid
         and x.vt_preview_id           = xxcp_global.gCommon(1).Preview_id
         and x.vt_status = 'TRANSACTION'
         and x.vt_source_assignment_id = pSource_Assignment_ID
         and x.vt_source_assignment_id = s.source_assignment_id
         -- Mapping
--         and ac.collector_id           = cc.collector_id
--         and cc.chart_of_accounts_id   = mp.chart_of_accounts_id
--         and cc.instance_id            = mp.instance_id
         -- Actuals table link
    --     and g.vt_transaction_id       = ac.transaction_id
         and decode(g.vt_transaction_type,'STD',g.vt_transaction_id,g.vt_replan_transaction_id) 
                                       = ac.transaction_id (+)
         and g.vt_source_assignment_id = ac.source_assignment_id (+)
         -- Table
         and g.vt_transaction_table = t.transaction_table
         and t.source_id            = pSource_id
         -- Type
         and g.vt_transaction_type  = y.type
				 and y.latch_only              = 'N'
         and t.source_table_id         = y.source_table_id
         -- Class
         and g.vt_transaction_class    = decode(t.class_mapping_req,'Y',cx. system_latch,cx.class)
				 and cx.latch_only             = 'N'
         and t.source_table_id         = cx.source_table_id
         -- Transaction_Set_id
         and y.Transaction_Set_id   = w.Transaction_Set_id
       order by g.vt_transaction_table, w.sequence,x.vt_parent_trx_id, cx.sequence,x.vt_transaction_id;

    GLIRec GLI%Rowtype;


    Cursor ConfErr(pSource_Assignment_id in number, pRequest_id in number) is
      select Distinct k.vt_parent_trx_id, k.vt_transaction_table
        from XXCP_pv_interface k
       where k.vt_request_id           = pRequest_id
         and k.vt_source_assignment_id = pSource_Assignment_id
         and k.vt_status               = 'ERROR';

    -- Assignments
    Cursor sr1(pSource_Assignment_id in number) is
      select set_of_books_id,
             source_assignment_id,
             instance_id source_instance_id,
             source_id,
						 t.stamp_parent_trx
        from XXCP_source_assignments t
       where t.Source_Assignment_id = pSource_Assignment_id
         and t.active               = 'Y';

    vInternalErrorCode        XXCP_errors.INTERNAL_ERROR_CODE%type := 0;
    vExchange_Rate_Type       XXCP_tax_registrations.EXCHANGE_RATE_TYPE%type;
    vCommon_Exch_Curr         XXCP_tax_registrations.COMMON_EXCH_CURR%type;

    vGlobalPrecision          pls_integer := 2;
    CommitCnt                 pls_Integer := 0;
    vSource_Activity          varchar2(4) := 'CPA';
    vJob_Status               Number(1)   := 0;

    vTransaction_Class        XXCP_sys_source_classes.Class%type;
    vRequest_ID               XXCP_process_history.request_id%type;

    -- NEW PARAMETERS
    vCurrent_Parent_Trx_id     XXCP_transaction_attributes.parent_trx_id%type := 0;
    vCurrent_Transaction_Table XXCP_sys_source_tables.transaction_Table%type := '~#~';
    vTransaction_Type          XXCP_sys_source_types.type%type;
    vGlobal_Rounding_Tolerance Number := 0;
    vTransaction_Abort         Boolean := False;

    -- Used for the REC Distribution
    vExtraLoop            Boolean := False;
    vExtraClass           XXCP_sys_source_tables.extra_record_type%type;
    --
    vSource_Assignment_id XXCP_source_assignments.source_assignment_id%type;
    vStamp_Parent_Trx     XXCP_source_assignments.stamp_parent_trx%type := 'N';
    vSource_instance_id   XXCP_source_assignments.instance_id%type;
    vSource_ID            XXCP_sys_sources.source_id%type;

    vClass_Mapping_Name   XXCP_sys_source_classes.Class%type;

    vTiming_Start         Number;
	  vDML_Compiled         varchar2(1);

    vStaged_Records       XXCP_sys_sources.staged_records%type;
    vEnd_Date             Date;
    vCost_Category        varchar2(100);
    vTerritory            varchar2(15);

    Cursor SF(pSource_Group_id in number) is
      select s.Source_Activity,
             s.Source_id,
             s.Preview_ctl,
             s.Timing,
             s.Custom_events,
             s.Cached,
             s.DML_Compiled,
             S.Staged_Records
        from XXCP_source_assignment_groups g, XXCP_sys_sources s
       where g.source_group_id = pSource_Group_id
         and s.source_id       = g.source_id;

    Cursor Tolx is
      Select Numeric_Code Global_Rounding_Tolerance
        from XXCP_lookups
       where lookup_type = 'ROUNDING TOLERANCE'
         and lookup_code = 'GLOBAL';

	  Cursor CRL(pSource_id in number) is
     select Distinct Transaction_table, source_id
       from XXCP_column_rules
      where source_id = pSource_id;

    Cursor CPP(pPeriod_id in number, pPeriod_Set_Name_id in number, pInstance_id in number) is
     select (period_year*100)+period_num Period_id, d.period_year, d.period_num, d.Instance_id, d.Start_Date, d.End_Date, d.period_type, d.period_name
       from xxcp_instance_gl_periods_v d
      where (period_year*100)+period_num  = pPeriod_id
        and  d.period_set_name_id         = pPeriod_Set_Name_id
        and  d.instance_id                = pInstance_id;

    Cursor TER(pCompany_number in varchar2) is
      select territory
      from   XXCP_tax_registrations
      where  company_number = pCompany_number
      and    cost_plus      = 'Y';
        

    -- Dynamic Explosion
    vExplosion_summary      NUMBER;
    vLast_source_type_id    xxcp_sys_source_types.source_type_id%TYPE := 0;
    vExplosion_sql          VARCHAR2(4000);
    vRowsfound              NUMBER;
    jx                      NUMBER;
    vExplosion_trx_id       NUMBER;
    vExplosion_source_rowid VARCHAR2(32);
    vExplosion_trx_type_id  NUMBER(15);
    vExplosion_trx_class_id NUMBER(15);

  BEGIN

  -- Start Up

    xxcp_global.User_id  := cUser_id;
    xxcp_global.Login_id := fnd_global.login_id;

    xxcp_global.Trace_on := xxcp_reporting.Get_Trace_Mode(cUser_id);
    xxcp_global.Preview_on := 'Y';
    xxcp_global.SystemDate := Sysdate;
    xxcp_global.gCommon(1).Preview_id  := cPreview_id;
		vTimingCnt := 1;

    For SFRec in SF(cSource_Group_id) loop

      xxcp_global.gCommon(1).Source_id               := SFRec.Source_id;
      xxcp_global.gCommon(1).Preview_ctl             := SFRec.Preview_ctl;
      xxcp_global.gCommon(1).Preview_on              := xxcp_global.Preview_on;
      xxcp_global.gCommon(1).Custom_events           := SFRec.Custom_events;
      xxcp_global.gCommon(1).current_Source_activity := SFRec.Source_Activity;
      xxcp_global.gCommon(1).Cached_Attributes       := SFRec.Cached;
      xxcp_global.gCommon(1).Grouping_Rule           := vStamp_Parent_Trx;

      vSource_Activity := SFRec.Source_Activity;
      vSource_id       := SFRec.Source_id;
	    vDML_Compiled    := SFRec.DML_Compiled;
      vStaged_Records  := SFRec.Staged_Records;
    End Loop;

    xxcp_global.SET_SOURCE_ID(cSource_id => vSource_id);

    --     SYS.DBMS_SUPPORT.START_TRACE( waits=>true, binds=>true );

    vTiming(vTimingCnt).CF_Records    := 0;
    vTiming(vTimingCnt).Flush_Records := 0;
    vTiming(vTimingCnt).Start_time    := to_char(sysdate, 'HH24:MI:SS');
    vTiming(vTimingCnt).Flush         := 0;
    vTiming_Start            := xxcp_reporting.Get_Seconds;

    If xxcp_global.gCommon(1).Custom_events = 'Y' then
      xxcp_custom_events.Set_system_mode(xxcp_global.gCommon);
    End If;

  	If vDML_Compiled = 'N' then
	  -- Prevent Processing and the column rules have changed and
	  -- they need compiling.
	  vJob_Status := 4;
	  xxcp_foundation.show('ERROR: Column Rules have changed, but not re-generated');
	  xxcp_foundation.FndWriteError(10999,'Column Rules have changed, but not re-generated');

	End If;
    --
    -- ## *******************************************************************************************************
    -- ##
    -- ##                            Check to See the process is already in use
    -- ##
    -- ## *******************************************************************************************************
    --

    xxcp_global.gCommon(1).Preview_id := cPreview_id;

    If vJob_Status = 0 then
      vRequest_id := cPreview_id;

      xxcp_foundation.show('Activity locked....' || to_char(vRequest_id));

      xxcp_global.gCommon(1).Current_request_id := vRequest_id;
			xxcp_global.gCommon(1).Preview_Zero_flag  := cPV_Zero_Flag;

      If xxcp_global.gCommon(1).Custom_events = 'Y' then
        vInternalErrorCode := xxcp_custom_events.BEFORE_PROCESSING(vSource_id);
      End If;

      -- Check for Override of Internal Attributes      
      Check_CPA_Attribute_Override;
      
      --
      -- ## **************************************************************************************************
      -- ##
      -- ##                                Setup Ready for the Run
      -- ##
      -- ## **************************************************************************************************
      --

      For REC in SR1(cSource_Assignment_id) Loop

        Exit when vJob_Status <> 0;

        vSource_instance_id   := rec.source_instance_id;
        vSource_Assignment_id := rec.Source_Assignment_id;
        --vStamp_Parent_Trx     := rec.Stamp_Parent_Trx;
        --xxcp_global.gCommon(1).Grouping_Rule := rec.Stamp_Parent_Trx;

				If vTimingCnt = 1 then
          xxcp_foundation.show('Initialization....');
          vTiming(vTimingCnt).Init_Start := to_char(sysdate, 'HH24:MI:SS');
          xxcp_te_base.Engine_Init(cSource_id         => vSource_id,
				                           cSource_Group_id   => cSource_Group_id,
																   cInternalErrorCode => vInternalErrorCode);
        End If;

				vTiming(vTimingCnt).Source_Assignment_id := vSource_Assignment_id;

        -- Setup Globals
        xxcp_global.gCommon(1).current_process_name  := 'CPAFC';
        xxcp_global.gCommon(1).current_assignment_id := vSource_Assignment_ID;

        -- 02.06.01
        xxcp_global.gTrans_Table_Array.delete;

        --
        -- *********************************************************************************************************
        --                            Populate Column Rules and Initialize Cursors
        -- *********************************************************************************************************
        --
        vTiming(vTimingCnt).Memory_Start := to_char(sysdate, 'HH24:MI:SS');

				For Rec in CRL(vSource_id) Loop
          -- Used for column Attributes and error messages
          xxcp_memory_Pack.LoadColumnRules(vSource_id, 'XXCP_cost_plus_interface',vSource_instance_id,Rec.Transaction_table,vRequest_id,vInternalErrorCode);
          xxcp_te_base.Init_Cursors(Rec.Transaction_table);
				End Loop;

        --
        -- *******************************************************************************************************
        --     Reset Previosly Errored Transactions and Assign Parent Trx Id to Incomming transactions
        -- *******************************************************************************************************
        --
        Reset_Errored_Transactions(cSource_assignment_id => vSource_Assignment_id,
				                           cRequest_id           => vRequest_id);

        Set_Running_Status(cSource_id            => vSource_id,
				                   cSource_assignment_id => vSource_Assignment_id,
													 cRequest_id           => vRequest_id);

        -- Exclusion logic
        vInternalErrorCode := xxcp_dynamic_sql.Apply_Exclude_Rule(cSource_Assignment_id => vSource_assignment_id); 

        Transaction_Group_Stamp(cStamp_Parent_Trx     => vStamp_Parent_Trx,
				                        cSource_assignment_id => vSource_Assignment_id,
																cInternalErrorCode    => vInternalErrorCode);

        vTiming(vTimingCnt).Init_End := to_char(sysdate, 'HH24:MI:SS');
        --
        -- GLOBAL ROUNDING TOLERANCE
        --
        vGlobal_Rounding_Tolerance := 0;
        For Rec in Tolx loop
          vGlobal_Rounding_Tolerance := Rec.Global_Rounding_Tolerance;
        End loop;

				-- Data Staging
				If nvl(vInternalErrorCode,0) = 0 then

				  vInternalErrorCode    := xxcp_custom_events.Data_Staging(cSource_id            => vSource_id,
					                                                         cSource_Assignment_id => vSource_Assignment_id,
																																	 cRequest_id           => Null,
																																	 cPreview_id           => cPreview_id);
        End If;

        If vInternalErrorCode = 0 then
          --
          -- Get Standard Parameters
          --
          vInternalErrorCode := xxcp_foundation.fnd_gl_lookup(cExchange_Rate_Type => vExchange_Rate_Type,
                                                              cGlobalPrecision    => vGlobalPrecision,
                                                              cCommon_Exch_Curr   => vCommon_Exch_Curr);

        End If;

        Commit;

        If vInternalErrorCode = 0 then

          --
          -- ## ***********************************************************************************************************
          -- ##
          -- ##                                Assignment
          -- ##
          -- ## ***********************************************************************************************************
          --
          i := 0;

          vTiming(vTimingCnt).CF_last     := xxcp_reporting.Get_Seconds;
          vTiming(vTimingCnt).CF            := 0;
          vTiming(vTimingCnt).CF_Records    := 0;
          vTiming(vTimingCnt).CF_Start_Date := Sysdate;
          xxcp_global.SystemDate := sysdate;

          For CFGRec in CFG(vSource_id, vSource_Assignment_id, vRequest_id) Loop

                i := i + 1;
                
                If CFGRec.Vt_Transaction_Table = 'FORECAST' then
                  gForcasting := TRUE;
                Else
                  gForcasting := FALSE;
                End If;

                vTiming(vTimingCnt).CF_Records := vTiming(vTimingCnt).CF_Records + 1;

                xxcp_global.gCommon(1).current_source_rowid      := CFGRec.Source_Rowid;
                xxcp_global.gCommon(1).current_transaction_date  := CFGRec.vt_Transaction_Date;
                xxcp_global.gCommon(1).current_transaction_table := CFGRec.vt_Transaction_Table;
                xxcp_global.gCommon(1).current_transaction_id    := CFGRec.vt_Transaction_id;
                xxcp_global.gCommon(1).current_parent_trx_id     := CFGRec.vt_Parent_Trx_id;
                xxcp_global.gCommon(1).current_Interface_id      := CFGRec.vt_interface_id;
								xxcp_global.gcommon(1).current_interface_ref     := CFGREC.vt_transaction_ref;
                xxcp_global.gCommon(1).calc_legal_exch_rate      := CFGRec.calc_legal_exch_rate;
                xxcp_global.gCommon(1).preview_parent_trx_id     := CFGRec.vt_parent_trx_id;
                xxcp_global.gCommon(1).preview_source_rowid      := CFGRec.vt_source_rowid;
                xxcp_global.gCommon(1).explosion_id              := CFGRec.ASG_Explosion_Id;

                vTransaction_Class := CFGRec.VT_Transaction_Class;

                If XXCP_TE_BASE.Ignore_Class(CFGRec.VT_Transaction_Table, vTransaction_Class) then
                  Begin
                    update XXCP_pv_interface
                       set vt_status              = 'IGNORED',
                           vt_internal_error_code = Null,
                           vt_date_processed      = xxcp_global.SystemDate
                     where rowid = CFGRec.Source_Rowid
                       and vt_preview_id  = xxcp_global.gCommon(1).Preview_id;

                  Exception
                    when others then
                      null;
                  End;
                Else
                  If CFGRec.ASG_Explosion_Id > 0 Then
                    If CFGREC.Source_Type_Id != vLast_Source_Type_id then
                        vInternalErrorCode :=
                         xxcp_dynamic_sql.ReadExplosionDef(cExplosion_id    => CFGRec.ASG_Explosion_Id,
                                                           cExplosion_sql   => vExplosion_SQL,
                                                           cSummary_Columns => vExplosion_Summary);

                        vLast_Source_Type_id := CFGREC.SOURCE_TYPE_ID;

                    End If;

                    If vInternalErrorCode = 0 then
                      vInternalErrorCode :=
                         xxcp_dynamic_sql.ExplosionInitProcess(
                                  cSource_Type_Id    => CFGREC.Source_Type_Id,
                                  cSource_Rowid      => CFGREC.vt_Source_Rowid,
                                  cInterface_id      => CFGREC.vt_Interface_Id,
                                  cExplosion_SQL     => vExplosion_SQL,
                                  cExplosion_Summary => vExplosion_Summary,
                                  cColumnCount       => 2, -- not used
                                  cRowsFound         => vRowsFound,
                                  cTransaction_Type  => CFGRec.VT_Transaction_Type,
                                  cTransaction_Class => CFGRec.VT_Transaction_Class
                                  );
                    End if;

                    
                      If vRowsFound > 0 and vInternalErrorCode = 0 then

                         For jx in 1..vRowsFound Loop

                          i := i + 1;

                          xxcp_global.gCommon(1).Explosion_Rowid          := xxcp_dynamic_sql.gDataColumn1(jx);
                          xxcp_global.gCommon(1).Explosion_Transaction_id := xxcp_dynamic_sql.gDataColumn2(jx);
                          xxcp_global.gCommon(1).Explosion_RowsFound      := vRowsFound;

                          vTransaction_Type       := xxcp_dynamic_sql.gDataColumn3(jx);
                          vTransaction_Class      := xxcp_dynamic_sql.gDataColumn4(jx);
                          vExplosion_Trx_Type_id  := xxcp_dynamic_sql.gDataColumnTypeId(jx);
                          vExplosion_Trx_Class_id := xxcp_dynamic_sql.gDataColumnClassId(jx);

                          -- ***********************

                          --
                          -- Get Cost Category
                          -- 
                          vInternalErrorCode := xxcp_te_base.Get_Cost_Category(CFGRec.company,
                                                                  CFGRec.department,
                                                                  CFGRec.account,
                                                                  CFGRec.vt_transaction_date,
                                                                  xxcp_wks.gActual_Costs_Rec(1).fc_Cost_Category_id,
                                                                  xxcp_wks.gActual_Costs_Rec(1).fc_Category_Data_Source,
                                                                  vCost_Category,
                                                                  CFGRec.Cost_Plus_Set_Id);

                          xxcp_wks.gActual_Costs_Rec(1).tu_Cost_Category_id     := xxcp_wks.gActual_Costs_Rec(1).fc_Cost_Category_id;
                          xxcp_wks.gActual_Costs_Rec(1).tu_Category_Data_Source := xxcp_wks.gActual_Costs_Rec(1).fc_Category_Data_Source;

                          --
                          -- Territory
                          --
                          open  TER(CFGRec.company);
                          fetch TER into vTerritory;
                          close TER;
                           
                          --
                          -- Get Payee Control Info
                          --  
                          xxcp_wks.Reset_Internal_Array;  -- Clear Internal Array

                          vInternalErrorCode := xxcp_te_base.Get_Payer_Details (cPayee            => CFGRec.company, 
                                                                   cTerritory        => vTerritory,
                                                                   cCost_Category_id => xxcp_wks.gActual_Costs_Rec(1).fc_Cost_Category_id, --vCost_Category,
                                                                   cData_Source      => xxcp_wks.gActual_Costs_Rec(1).fc_Category_Data_Source,
                                                                   cTransaction_Date => CFGRec.vt_transaction_date,
                                                                   cPayer1           => xxcp_wks.I1009_Array(123),
                                                                   cPayer1_Percent   => xxcp_wks.I1009_Array(124),
                                                                   cPayer2           => xxcp_wks.I1009_Array(125),
                                                                   cPayer2_Percent   => xxcp_wks.I1009_Array(126),
                                                                   cInter_Payer      => xxcp_wks.I1009_Array(127),
                                                                   cAccount_Type     => xxcp_wks.I1009_Array(128));

                          -- Tax Agreement Number Internal Attribute
                          xxcp_wks.I1009_Array(171) := CFGrec.Tax_Agreement_Number;

                          -- Hold Internals for Use in configurator
                          xxcp_wks.I1009_ArrayGLOBAL := xxcp_wks.Clear_Internal;
                          xxcp_wks.I1009_ArrayGLOBAL := xxcp_wks.I1009_Array;
                          
                          -- ***********************

                          XXCP_TE_CONFIGURATOR.Control(
                                                    cSource_Assignment_ID     => vSource_assignment_id,
                                                    cSet_of_books_id          => CFGRec.Source_Set_of_books_id,
                                                    cTransaction_Date         => CFGRec.VT_Transaction_Date,
                                                    cSource_id                => vSource_id,
                                                    cSource_Table_id          => CFGRec.Source_table_id,
                                                    cSource_Type_id           => vExplosion_Trx_Type_id,
                                                    cSource_Class_id          => vExplosion_Trx_Class_id,
                                                    cTransaction_id           => CFGRec.VT_Transaction_id,
                                                    cSourceRowid              => CFGRec.Source_Rowid,
                                                    cParent_Trx_id            => CFGRec.VT_Parent_Trx_id,
                                                    cTransaction_Set_id       => CFGRec.Transaction_Set_id,
                                                    cSpecial_Array            => 'N',
                                                    cKey                      => Null,
                                                    cTransaction_Table        => CFGRec.VT_Transaction_Table,
                                                    cTransaction_Type         => vTransaction_Type,
                                                    cTransaction_Class        => vTransaction_Class,
                                                    cInternalErrorCode        => vInternalErrorCode);

                          xxcp_global.gCommon(1).Explosion_transaction_id := CFGRec.VT_Transaction_id;
                          -- Check for Explosion Config Error
                          If nvl(vInternalErrorCode, 0) <> 0 then
                             Update xxcp_pv_interface
                                 set vt_status_code = 7060,
                                     vt_internal_error_code = vInternalErrorCode,
                                     vt_date_processed = xxcp_global.SystemDate
                               where rowid         = xxcp_dynamic_sql.gDataColumnPvRowid(jx)
                                and vt_preview_id  = xxcp_global.gCommon(1).Preview_id;
                          End if;

                          Exit when vInternalErrorCode > 0;

                         End Loop;

                      ElsIf vInternalErrorCode = 0 then
                           vInternalErrorCode := 3441;
                      End If;
                                        
                  Else
                 
                    --
                    -- Get Cost Category
                    -- 
                    vInternalErrorCode := xxcp_te_base.Get_Cost_Category(CFGRec.company,
                                                            CFGRec.department,
                                                            CFGRec.account,
                                                            CFGRec.vt_transaction_date,
                                                            xxcp_wks.gActual_Costs_Rec(1).fc_Cost_Category_id,
                                                            xxcp_wks.gActual_Costs_Rec(1).fc_Category_Data_Source,
                                                            vCost_Category,
                                                            CFGRec.Cost_Plus_Set_Id);

                    xxcp_wks.gActual_Costs_Rec(1).tu_Cost_Category_id     := xxcp_wks.gActual_Costs_Rec(1).fc_Cost_Category_id;
                    xxcp_wks.gActual_Costs_Rec(1).tu_Category_Data_Source := xxcp_wks.gActual_Costs_Rec(1).fc_Category_Data_Source;

                    --
                    -- Territory
                    --
                    open  TER(CFGRec.company);
                    fetch TER into vTerritory;
                    close TER;
                     
                    --
                    -- Get Payee Control Info
                    --  
                    xxcp_wks.Reset_Internal_Array;  -- Clear Internal Array

                    vInternalErrorCode := xxcp_te_base.Get_Payer_Details (cPayee            => CFGRec.company, 
                                                             cTerritory        => vTerritory,
                                                             cCost_Category_id => xxcp_wks.gActual_Costs_Rec(1).fc_Cost_Category_id, --vCost_Category,
                                                             cData_Source      => xxcp_wks.gActual_Costs_Rec(1).fc_Category_Data_Source,
                                                             cTransaction_Date => CFGRec.vt_transaction_date,
                                                             cPayer1           => xxcp_wks.I1009_Array(123),
                                                             cPayer1_Percent   => xxcp_wks.I1009_Array(124),
                                                             cPayer2           => xxcp_wks.I1009_Array(125),
                                                             cPayer2_Percent   => xxcp_wks.I1009_Array(126),
                                                             cInter_Payer      => xxcp_wks.I1009_Array(127),
                                                             cAccount_Type     => xxcp_wks.I1009_Array(128));


                    -- Tax Agreement Number Internal Attribute
                    xxcp_wks.I1009_Array(171) := CFGrec.Tax_Agreement_Number;

                    -- Hold Internals for Use in configurator
                    xxcp_wks.I1009_ArrayGLOBAL := xxcp_wks.Clear_Internal;
                    xxcp_wks.I1009_ArrayGLOBAL := xxcp_wks.I1009_Array;
                  
                    XXCP_TE_CONFIGURATOR.Control(
                                                 cSource_Assignment_ID     => vSource_assignment_id,
                                                 cSet_of_books_id          => CFGRec.Source_Set_of_books_id,
                                                 cTransaction_Date         => CFGRec.VT_Transaction_Date,
                                                 cSource_id                => vSource_id,
                                                 cSource_Table_id          => CFGRec.Source_table_id,
                                                 cSource_Type_id           => CFGRec.Source_type_id,
                                                 cSource_Class_id          => CFGRec.Source_class_id,
                                                 cTransaction_id           => CFGRec.VT_Transaction_ID,
                                                 cSourceRowid              => CFGRec.Source_Rowid,
                                                 cParent_Trx_id            => CFGRec.VT_Parent_Trx_id,
                                                 cTransaction_Set_id       => CFGRec.Transaction_Set_id,
                                                 cSpecial_Array            => 'N',
                                                 cKey                      => Null,
                                                 cTransaction_Table        => CFGRec.VT_Transaction_Table,
                                                 cTransaction_Type         => CFGRec.VT_Transaction_Type,
                                                 cTransaction_Class        => vTransaction_Class,
                                                 cInternalErrorCode        => vInternalErrorCode);

                  End if; -- Explosion

                  -- Check Status
                  If vInternalErrorCode = -1 then
                    Update XXCP_PV_INTERFACE
                       set vt_status              = 'SUCCESSFUL',
                           vt_internal_error_code = Null,
                           vt_date_processed      = xxcp_global.SystemDate,
                           vt_status_code         = 7001
                     where rowid = CFGRec.Source_Rowid
                     and vt_preview_id  = xxcp_global.gCommon(1).Preview_id;
                  ElsIf nvl(vInternalErrorCode, 0) <> 0 then
                    Update XXCP_PV_INTERFACE
                       set vt_status              = 'ERROR',
                           vt_internal_error_code = 12000,
                           vt_date_processed      = xxcp_global.SystemDate
                     where rowid = CFGRec.Source_Rowid
                     and vt_preview_id  = xxcp_global.gCommon(1).Preview_id;

                  ElsIf nvl(vInternalErrorCode, 0) = 0 then


                    Update XXCP_PV_INTERFACE
                       set vt_status              = 'TRANSACTION',
                           vt_internal_error_code = Null,
                           vt_date_processed      = xxcp_global.SystemDate,
                           vt_results_attribute1  = xxcp_wks.gActual_Costs_Rec(1).fc_Cost_Category_id,
                           vt_results_attribute2  = xxcp_wks.gActual_Costs_Rec(1).fc_Category_Data_Source,
                           vt_results_attribute3  = vCost_Category
                     where rowid = CFGRec.Source_Rowid
                     and vt_preview_id  = xxcp_global.gCommon(1).Preview_id;

                  End If;

                End If; -- End configuration

                -- ##
                -- ## Commit Configured rows
                -- ##

                If i >= 500 then
                  xxcp_global.SystemDate    := Sysdate;
                  Commit;
                  i           := 0;
                  -- !! Dont test for termination
                  Exit when vJob_Status > 0; -- Controlled Exit
                End If;


            --
          End loop;
          xxcp_dynamic_sql.Close_Session;
          -- #
          -- # Clear Common vars
          -- #
          xxcp_global.gCommon(1).current_transaction_table := Null;
          xxcp_global.gCommon(1).current_transaction_id    := Null;
          xxcp_global.gCommon(1).current_parent_trx_id     := Null;
					xxcp_global.gcommon(1).current_interface_ref     := Null;

          Commit; -- Final Configuration Commit.
          --
          If xxcp_global.gCommon(1).Custom_events = 'Y' then
            xxcp_custom_events.AFTER_ASSIGNMENT_PROCESSING(vSource_id,
                                                           vSource_assignment_id);
          End If;

          vTiming(vTimingCnt).CF := xxcp_reporting.Get_Seconds_Diff(vTiming(vTimingCnt).CF_last,xxcp_reporting.Get_Seconds);
          vTiming(vTimingCnt).CF_End_Date := Sysdate;

          -- ***********************************************************************************************************
          --
          --  Set associated Transactions to error if anyone of the Transactions is a set has errored in configuration
          --
          -- ***********************************************************************************************************
          --
          xxcp_foundation.show('Reporting Configuration errors....');
          xxcp_global.SystemDate := Sysdate;
          vInternalErrorCode := 0;
          For ConfErrRec in ConfErr(vSource_Assignment_id, vRequest_id) loop

            Begin
              update XXCP_pv_interface
                 set vt_status              = 'ERROR',
                     vt_internal_Error_code = 12824, -- Associated errors
                     vt_date_processed      = xxcp_global.SystemDate
               where vt_request_id           = vRequest_id
                 and vt_Source_Assignment_id = vSource_Assignment_id
                 and vt_status               IN ('ASSIGNMENT', 'TRANSACTION')
                 and vt_parent_trx_id        = ConfErrRec.vt_parent_trx_id
                 and vt_transaction_table    = ConfErrRec.vt_Transaction_table;

            Exception
              when OTHERS then
                vInternalErrorCode := 12819; -- Update Failed.

            End;
          End Loop;

          --## ***********************************************************************************************************
          --##
          --##                                    Transaction Engine
          --##
          --## ***********************************************************************************************************

          vTiming(vTimingCnt).TE_Last       := xxcp_reporting.Get_Seconds;
          vTiming(vTimingCnt).TE_Records    := 0;
          vTiming(vTimingCnt).TE_Start_Date := Sysdate;

          vExtraLoop             := False;
          vExtraClass            := Null;
          xxcp_global.SystemDate := Sysdate;
          i                := 0;
          If vInternalErrorCode = 0 and vJob_Status = 0 then

            xxcp_foundation.show('Transactions....');
            xxcp_wks.Reset_Source_Segments;
            gBalancing_Array      := xxcp_wks.Clear_Segments;
            ClearWorkingStorage;

            For  GLIRec in  GLI(vSOURCE_ID, vSOURCE_ASSIGNMENT_ID, vRequest_id) Loop

                  vInternalErrorCode := 0;

                  vTiming(vTimingCnt).TE_Records := vTiming(vTimingCnt).TE_Records + 1;
                  
                  If glirec.vt_transaction_table = 'FORECAST' then
                    xxcp_global.Forecast_on := 'Y';
                    xxcp_global.True_Up_on  := 'N';
                    xxcp_global.gCommon(1).cpa_type := 'F';
                  Elsif glirec.vt_transaction_table = 'TRUE-UP' then
                    xxcp_global.Forecast_on := 'N';                  
                    xxcp_global.True_Up_on  := 'Y';
                    xxcp_global.gCommon(1).cpa_type := 'T';
                  Else
                    xxcp_global.Forecast_on := 'N';                  
                    xxcp_global.True_Up_on  := 'N';
                  End if;
                  
                  If glirec.vt_transaction_type like 'R%' then
                    xxcp_global.Replan_on := 'Y';
                  Else
                    xxcp_global.Replan_on := 'N';                  
                  End if;
                  

                  -- Dynamic Explosion at Transaction Level
                  xxcp_global.gCommon(1).Explosion_id     := GLIRec.ASG_Explosion_Id;
                  xxcp_global.gCommon(1).Trx_Explosion_id := GLIRec.Trx_Explosion_id;

                  --
                  -- ***********************************************************************************************************
                  --        Set the balancing Setments to be written to the Column Rules Array
                  -- ***********************************************************************************************************
                  --
                  -- Poplulate the Balancing Array
                  xxcp_wks.Source_Balancing(1) := Null;
                  xxcp_wks.Source_Balancing(2) := GLIRec.be2; -- User JE Source Name
                  xxcp_wks.Source_Balancing(3) := GLIRec.be3; -- Group Id
                  xxcp_wks.Source_Balancing(4) := GLIRec.be4; -- User JE Category Name
                  xxcp_wks.Source_Balancing(5) := Null;
                  xxcp_wks.Source_Balancing(6) := GLIRec.be6; -- Reference 1
                  xxcp_wks.Source_Balancing(7) := GLIRec.be7; -- Reference 2
                  xxcp_wks.Source_Balancing(8) := GLIRec.be8; -- Reference 3
                  xxcp_wks.Source_Balancing(9) := Null;
                  xxcp_wks.Source_Balancing(10):= Null;
                  xxcp_wks.Source_Balancing(11):= Null;  -- Balancing Segment
                  xxcp_wks.Source_Balancing(12):= Null;  -- Rounding Group
                  
                  vPeriod_Set_Name_id          := GLIRec.Period_Set_Name_Id;

                  -- Get Dates for Posting_period
                  for CPP_rec in CPP(GLIRec.Posting_period_id,
                                     GLIRec.Period_Set_Name_id,
                                     GLIRec.vt_Instance_id) loop
                    vEnd_Date := CPP_rec.End_Date;
                  end loop;
                  

                  --
                  -- ***********************************************************************************************************
                  --         Move Accounting Codes from Master Record to Array
                  -- ***********************************************************************************************************
                  -- Populate Source Account Segments
                  xxcp_wks.Source_Segments(01) := GLIRec.Segment1;
                  xxcp_wks.Source_Segments(02) := GLIRec.Segment2;
                  xxcp_wks.Source_Segments(03) := GLIRec.Segment3;
                  xxcp_wks.Source_Segments(04) := GLIRec.Segment4;
                  xxcp_wks.Source_Segments(05) := GLIRec.Segment5;
                  xxcp_wks.Source_Segments(06) := GLIRec.Segment6;
                  xxcp_wks.Source_Segments(07) := GLIRec.Segment7;
                  xxcp_wks.Source_Segments(08) := GLIRec.Segment8;
                  xxcp_wks.Source_Segments(09) := GLIRec.Segment9;
                  xxcp_wks.Source_Segments(10) := GLIRec.Segment10;
                  xxcp_wks.Source_Segments(11) := GLIRec.Segment11;
                  xxcp_wks.Source_Segments(12) := GLIRec.Segment12;
                  xxcp_wks.Source_Segments(13) := GLIRec.Segment13;
                  xxcp_wks.Source_Segments(14) := GLIRec.Segment14;
                  xxcp_wks.Source_Segments(15) := GLIRec.Segment15;

--                  xxcp_wks.Source_Segments(GLIRec.company_seg)    := GLIRec.Company;
--                  xxcp_wks.Source_Segments(GLIRec.Department_Seg) := GLIRec.Department;
--                  xxcp_wks.Source_Segments(GLIRec.Account_Seg)    := GLIRec.Account;

                  xxcp_global.gCommon(1).preview_parent_trx_id   := GLIRec.VT_PARENT_TRX_ID;
                  xxcp_global.gCommon(1).preview_source_rowid    := GLIRec.vt_source_rowid;
                  xxcp_global.New_Transaction := 'N';


                  --
                  -- ***********************************************************************************************************
                  --         When a New Transaction is detected then flush the current buffer
                  -- ***********************************************************************************************************
                  --
                  If (vCurrent_Parent_Trx_id <> GLIRec.vt_parent_Trx_id) then

                    vTransaction_Abort := False;

                    --
                    -- !! ******************************************************************************************************
                    --                             FLUSH TRANSACTION
                    -- !! ******************************************************************************************************
                    --
                    If vCurrent_Parent_Trx_id <> 0 then
                      CommitCnt := CommitCnt + 1; -- Number of records process since last commit.

                      k := FlushRecordsToTarget(vPeriod_Set_Name_id,
                                                vGlobal_Rounding_Tolerance,
                                                vGlobalPrecision,
                                                vTransaction_Type,
                                                vInternalErrorCode);
                    End If;

                    --
                    -- ***********************************************************************************************************
                    --            Store Variables that are only set once per Parent Trx Id
                    -- ***********************************************************************************************************
                    --
                    xxcp_global.gCommon(1).current_transaction_table := GLIRec.vt_Transaction_Table;
                    xxcp_global.gCommon(1).current_parent_trx_id     := GLIRec.vt_Parent_Trx_id;

                    vCurrent_Parent_Trx_id := GLIRec.vt_Parent_Trx_id;
                    vTransaction_Type      := GLIRec.vt_Transaction_Type;

                    xxcp_global.gCommon(1).current_source_rowid    := GLIRec.Source_Rowid;
                    xxcp_global.gCommon(1).current_Interface_id    := GLIRec.vt_interface_id;
--                    xxcp_global.gCommon(1).current_Accounting_date := GLIRec.Accounting_Date;
                    xxcp_global.gCommon(1).current_Accounting_date := vEnd_Date;             -- 02.04.08
                    xxcp_global.gCommon(1).current_Batch_number    := GLIRec.Group_id;
                    xxcp_global.gCommon(1).current_Category_name   := GLIRec.Category_Name;
                    xxcp_global.gCommon(1).current_Source_name     := GLIRec.Source_name;
										xxcp_global.gcommon(1).current_interface_ref   := GLIRec.vt_transaction_ref;

                    xxcp_global.New_Transaction := 'Y';
                    --
                    -- ***********************************************************************************************************
                    --                         Extra Loops for different Classes
                    -- ***********************************************************************************************************
                    --
                    vExtraLoop  := False;
                    vExtraClass := Null;
                    For j in 1 .. xxcp_global.gSRE.Count loop
                      xw := j;
                      If GLIRec.vt_Transaction_Table = xxcp_global.gSRE(j).Transaction_table then
                        vExtraClass := xxcp_global.gSRE(j).Extra_Record_type;
                        vExtraLoop  := TRUE;
                        Exit;
                      End If;
                    End Loop;

                  Else
                    vExtraLoop  := FALSE; -- !!!!
                    vExtraClass := Null;
                  End If;

                  xxcp_global.gCommon(1).current_transaction_id := GLIRec.vt_Transaction_id;
                  --
                  -- ***********************************************************************************************************
                  --                                   Store Working Variables
                  -- ***********************************************************************************************************
                  --
                  xxcp_global.gCommon(1).current_source_rowid    := GLIRec.Source_Rowid;
                  xxcp_global.gCommon(1).current_Interface_id    := GLIRec.vt_interface_id;
--                  xxcp_global.gCommon(1).current_Accounting_date := GLIRec.Accounting_Date;
                  xxcp_global.gCommon(1).current_Accounting_date := vEnd_Date;               -- 02.04.08
                  xxcp_global.gCommon(1).current_Batch_number    := GLIRec.Group_id;
                  xxcp_global.gCommon(1).current_Category_name   := GLIRec.Category_Name;
                  xxcp_global.gCommon(1).current_Source_name     := GLIRec.Source_name;
                  --
                  -- **************************************************************************************************
                  --                                        Cost-Plus Values
                  -- **************************************************************************************************
                  -- 
--                  xxcp_wks.gActual_Costs_Rec(1).fc_Cost_Category_id     := GLIRec.fc_Cost_Category_id;              
--                  xxcp_wks.gActual_Costs_Rec(1).tu_Cost_Category_id     := GLIRec.tu_Cost_Category_id;              
--                  xxcp_wks.gActual_Costs_Rec(1).fc_Category_Data_Source := GLIRec.fc_Category_Data_Source;              
--                  xxcp_wks.gActual_Costs_Rec(1).tu_Category_Data_Source := GLIRec.tu_Category_Data_Source;              
                  xxcp_wks.gActual_Costs_Rec(1).Period_Set_Name_id      := GLIRec.Period_Set_Name_id;              
                  xxcp_wks.gActual_Costs_Rec(1).Collection_Period_id    := GLIRec.Collection_Period_id;              
                  xxcp_wks.gActual_Costs_Rec(1).Currency_Code           := GLIRec.Currency_Code;              
                  xxcp_wks.gActual_Costs_Rec(1).Collector_id            := GLIRec.Collector_id;    
                  xxcp_wks.gActual_Costs_Rec(1).Company                 := GLIRec.Company;              
                  xxcp_wks.gActual_Costs_Rec(1).Department              := GLIRec.Department;              
                  xxcp_wks.gActual_Costs_Rec(1).Account                 := GLIRec.Account;              
                  xxcp_wks.gActual_Costs_Rec(1).data_source             := GLIRec.data_source;  -- 02.04.02
                  xxcp_wks.gActual_Costs_Rec(1).Instance_id             := GLIRec.vt_instance_id;              
                  xxcp_wks.gActual_Costs_Rec(1).Period_id               := GLIRec.Posting_period_id;
--                  xxcp_wks.gActual_Costs_Rec(1).Period_id               := xxcp_translations.Period_id(cPeriod_Name => cPeriod_Name,
--                                                                                                       cInstance_id => GLIRec.vt_instance_id);               

                  xxcp_wks.gActual_Costs_Rec(1).Attribute1            := GLIRec.Attribute1;
                  xxcp_wks.gActual_Costs_Rec(1).Attribute2            := GLIRec.Attribute2;
                  xxcp_wks.gActual_Costs_Rec(1).Attribute3            := GLIRec.Attribute3;
                  xxcp_wks.gActual_Costs_Rec(1).Attribute4            := GLIRec.Attribute4;
                  xxcp_wks.gActual_Costs_Rec(1).Attribute5            := GLIRec.Attribute5;
                  xxcp_wks.gActual_Costs_Rec(1).Attribute6            := GLIRec.Attribute6;
                  xxcp_wks.gActual_Costs_Rec(1).Attribute7            := GLIRec.Attribute7;
                  xxcp_wks.gActual_Costs_Rec(1).Attribute8            := GLIRec.Attribute8;

                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_COMPANY              := GLIRec.VT_REPLAN_COMPANY;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_DEPARTMENT           := GLIRec.VT_REPLAN_DEPARTMENT;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_ACCOUNT              := GLIRec.VT_REPLAN_ACCOUNT; 
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_TRANSACTION_ID       := GLIRec.VT_REPLAN_TRANSACTION_ID;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_TRADING_SET_ID       := GLIRec.VT_REPLAN_TRADING_SET_ID;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_AMOUNT               := GLIRec.VT_REPLAN_AMOUNT;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_CURRENCY_CODE        := GLIRec.VT_REPLAN_CURRENCY_CODE;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_OWNER_TAX_REG_ID     := GLIRec.VT_REPLAN_OWNER_TAX_REG_ID;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_PARTNER_TAX_REG_ID   := GLIRec.VT_REPLAN_PARTNER_TAX_REG_ID;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_MC_QUALIFIER1        := GLIRec.VT_REPLAN_MC_QUALIFIER1;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_COST_CATEGORY_ID     := GLIRec.VT_REPLAN_COST_CATEGORY_ID;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_CATEGORY_DATA_SOURCE := GLIRec.VT_REPLAN_CATEGORY_DATA_SOURCE;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_PROCESS_HISTORY_ID   := GLIRec.VT_REPLAN_PROCESS_HISTORY_ID;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_PERIOD_ID            := GLIRec.VT_REPLAN_PERIOD_ID; 
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_PAYER1               := GLIRec.VT_REPLAN_PAYER1;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_PAYER1_PERCENT       := GLIRec.VT_REPLAN_PAYER1_PERCENT;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_PAYER2               := GLIRec.VT_REPLAN_PAYER2;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_PAYER2_PERCENT       := GLIRec.VT_REPLAN_PAYER2_PERCENT; 
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_INTER_PAYER          := GLIRec.VT_REPLAN_INTER_PAYER;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_ACCOUNT_TYPE         := GLIRec.VT_REPLAN_ACCOUNT_TYPE;

                  -- Tax Agreement Number Internal Attribute
                  xxcp_wks.I1009_Array(171) := GLIrec.Tax_Agreement_Number;


                  --
                  -- Get Cost Category (stamped on interface prior to config)
                  --
                  xxcp_wks.gActual_Costs_Rec(1).fc_Cost_Category_id     := GLIRec.Cost_Category_Id;
                  xxcp_wks.gActual_Costs_Rec(1).fc_Category_Data_Source := GLIRec.Category_Data_Source;
                  vCost_Category                                        := GLIRec.Cost_Category;
                   
                  xxcp_wks.gActual_Costs_Rec(1).tu_Cost_Category_id     := xxcp_wks.gActual_Costs_Rec(1).fc_Cost_Category_id;
                  xxcp_wks.gActual_Costs_Rec(1).tu_Category_Data_Source := xxcp_wks.gActual_Costs_Rec(1).fc_Category_Data_Source;
                  xxcp_wks.gActual_Costs_Rec(1).cost_plus_set_id        := GLIRec.cost_plus_set_id;

                  xxcp_wks.gActual_Costs_Rec(1).tax_agreement_number    := GLIRec.tax_agreement_number;
                  --
                  -- ***********************************************************************************************************
                  --                                   Call the Transaction Engine
                  -- ***********************************************************************************************************
                  --
                  If vTransaction_Abort = False then
                    vInternalErrorCode := 0; -- Reset for each row

                    i := i + 1; -- Count the number of rows processed.

                    xxcp_wks.Source_CNT  := xxcp_wks.SOURCE_CNT + 1;
                    xxcp_wks.Source_rowid(xxcp_wks.source_cnt)   := GLIRec.Source_Rowid;
                    xxcp_wks.SOURCE_SUB_CODE(xxcp_wks.source_cnt):= 0;
                    xxcp_wks.SOURCE_STATUS(xxcp_wks.source_cnt)  := '?';
                    xxcp_global.gDefCostCatUsed                  := 'N';


                    -- *********************************************************************
                    -- Class Mapping
                    -- *********************************************************************
										If GLIRec.class_mapping_req = 'Y' then
  									    vClass_Mapping_Name := xxcp_te_base.class_mapping(
												                                   cSource_id              => vSource_id,
										                                       cClass_Mapping_Required => GLIRec.class_mapping_req,
																													 cTransaction_Table      => GLIRec.vt_transaction_table,
																													 cTransaction_Type       => GLIRec.vt_transaction_type,
																													 cTransaction_Class      => GLIRec.vt_transaction_class,
																													 cTransaction_id         => GLIRec.vt_transaction_id,
																													 cCode_Combination_id    => GLIRec.code_combination_id,
																													 cAmount                 => GLIRec.Transaction_Cost);

                    End If;

                    -- *********************************************************************
                    -- End Class Mapping
                    -- *********************************************************************
                    XXCP_TE_ENGINE.Control(cSource_assignment_id    => vSource_Assignment_id,
                                           cSource_Table_id         => GLIRec.Source_Table_id,
																					 cSource_Type_id          => GLIRec.Source_Type_id,
                                           cSource_Class_id         => GLIRec.Source_Class_id,
                                           cTransaction_id          => GLIRec.vt_Transaction_id,
                                           cParent_Trx_id           => GLIRec.vt_Parent_Trx_id,
                                           cSourceRowid             => GLIRec.Source_Rowid,
                                           cParent_Entered_Amount   => GLIRec.Parent_Entered_Amount,
                                           cParent_Entered_Currency => GLIRec.Parent_Entered_Currency,
																					 cClass_Mapping           => GLIRec.class_mapping_req,
                                           cClass_Mapping_Name      => vClass_Mapping_Name,
                                           -- Segments 1-15
                                           cInterface_ID            => GLIRec.VT_Interface_id,
                                           cExtraLoop               => vExtraLoop,
                                           cExtraClass              => vExtraClass,
                                           cTransaction_set_id      => GLIRec.Transaction_Set_id,
																					 --
																					 cTransaction_Table       => GLIRec.VT_Transaction_Table,
																					 cTransaction_Type        => GLIRec.VT_Transaction_Type,
                                           cTransaction_Class       => GLIRec.vt_Transaction_Class,
                                           cInternalErrorCode       => vInternalErrorCode);


                    xxcp_global.gCommon(1).current_trading_set := Null;

                    -- Report Errors
                    If vInternalErrorCode <> 0 then
                      If vInternalErrorCode = 7015 then
                        Category_Change_Transaction(vInternalErrorCode);
                      Else
                        Error_Whole_Transaction(vInternalErrorCode);
                      End If;
                      vTransaction_Abort := True;
                    End If;

                  End If;
                  --
                  -- ***********************************************************************************************************
                  --                     Test to See if Oracle or the User has terminated this process
                  --                                   And Commit after Each 1000 Records processed
                  -- ***********************************************************************************************************
                  --
                  IF CommitCnt >= 100 THEN
                    CommitCnt := 0;
                    xxcp_global.SystemDate    := sysdate;
                    Commit;
                    -- !! Dont test for termination in Preview Mode
                    Exit when vJob_Status > 0; -- Controlled Exit
                  END IF;

            END LOOP;

            xxcp_te_base.Close_Cursors;

            --
            --  !!***********************************************************************************************************
            --                     Flush the Buffer for a Final Time
            -- !! ***********************************************************************************************************
            --
            IF vCurrent_Parent_Trx_id <> 0 and vInternalErrorCode = 0 then
              If vInternalErrorCode = 0 then
                k := FlushRecordsToTarget(vPeriod_Set_Name_id,
                                          vGlobal_Rounding_Tolerance,
                                          vGlobalPrecision,
                                          vTransaction_type,
                                          vInternalErrorCode);
              End If;
            End If;
          End If; -- Internal Error Check GLI
        End If; -- End Internal Error Check

        vTiming(vTimingCnt).TE := xxcp_reporting.Get_Seconds_Diff(vTiming(vTimingCnt).TE_last,xxcp_reporting.Get_Seconds);

        xxcp_foundation.show('Clear down...');
        xxcp_global.SystemDate := Sysdate;
        -- *******************************************************************
        -- Error out records remaining with a Processing status after the run
        -- *******************************************************************
        Begin
             update XXCP_pv_interface
                 set vt_status              = 'ERROR'
                    ,vt_internal_error_code = Decode(vt_status,'TRANSACTION',12999,'ASSIGNMENT',12998)
                    ,vt_date_processed      = sysdate
               where rowid                  = any
			              (select g.Rowid
                    from XXCP_pv_interface g
                   where g.vt_status               in ('ASSIGNMENT', 'TRANSACTION')
                     and g.vt_preview_id           = xxcp_global.gCommon(1).preview_id
                     and g.vt_source_assignment_id = vSource_assignment_id);


        Exception when OTHERS then Null;
        End;

        -- #
        -- # Clear Common vars
        -- #
        xxcp_global.gCommon(1).current_transaction_table := Null;
        xxcp_global.gCommon(1).current_transaction_id    := Null;
        xxcp_global.gCommon(1).current_parent_trx_id     := Null;
				xxcp_global.gcommon(1).current_interface_ref     := Null;
        --
        -- ***********************************************************************************************************
        --                     Clear Down
        -- ***********************************************************************************************************
        --
        ClearWorkingStorage;
        --
        -- ***********************************************************************************************************
        --                     DeActive the Lock in XXCP_ACTIVITY_CONTROL
        -- ***********************************************************************************************************
        --
        Commit;
        vTiming(vTimingCnt).TE_End_Date := Sysdate;
				vTimingCnt := vTimingCnt + 1;
      End loop; -- SR1


      xxcp_te_base.ClearDown;
      xxcp_memory_pack.ClearDown;
      xxcp_dynamic_sql.Close_Session;

      If xxcp_global.gCommon(1).Custom_events = 'Y' then
        xxcp_custom_events.after_processing(cSource_id      => vSource_id,
                                            cSource_Group_id => cSource_Group_id);
      End If;
      Commit;

    Else
      --
      -- Report If this process was already in use
      --
      xxcp_foundation.show('Engine already in use');
      vJob_Status := 1;
    End If;

    xxcp_te_base.Write_Timings(vTiming, vTiming_Start);
    --
    -- ************************************************************************************************
    --                     Show Time Statistics when called from SQL*Plus
    -- ************************************************************************************************
    --
    xxcp_global.gCommon(1).Current_Request_id := Null;
    --
    -- ***********************************************************************************************************
    --                     Return Job Status Code and Finish!!
    -- ***********************************************************************************************************
    --
    Return(vJob_Status);
  End Control;


Begin
  -- Get System Date once
  xxcp_global.SystemDate := Sysdate;

END XXCP_PV_CPA_ENGINE;
/
