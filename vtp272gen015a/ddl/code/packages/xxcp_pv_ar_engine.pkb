CREATE OR REPLACE PACKAGE BODY XXCP_PV_AR_ENGINE AS
/******************************************************************************
                        V I R T A L  T R A D E R  L I M I T E D 
  		      (Copyright 2005-2012 Virtual Trader Ltd. All rights Reserved)

   NAME:       XXCP_PV_AR_ENGINE
   PURPOSE:    AR Engine in Preview Mode

   REVISIONS:
   Ver        Date      Author  Description
   ---------  --------  ------- ------------------------------------
   02.01.00   28/06/02  Keith   First Coding
   02.01.01   07/01/04  Keith   Restructure for VT 2.1
   02.01.02   20/05/04  Keith   ransaction Stamp by vt_transaction_group
   02.01.03   28/05/04  Keith   Fixed Rounding Amount Issue
   02.01.05   29/09/04  Keith   Added debug on Entered DR, CR Not matching
   02.01.07   20/10/04  Keith   Added debug on Entered DR, CR Not matching
   02.02.00   07/02/05  Keith   Adding Inventory Logic
   02.02.01   14/02/05  Keith   Changed Error Checking Logic
	 02.02.02   19/10/05  Keith   Change to use the x.vt_parent_trx_id not
	 															g.vt_parent_trx_id in engine
	 02.03.01   08/02/06  Keith   Moved .init upwards
	 02.03.02   25/08/06  Keith   Only put header on record with an amount
	 02.03.03   14/09/06  Keith   Changed vExtra Loop count logic
	 02.03.04   25/06/07  Keith   Removed Zero_Amount_Trx
	 02.03.05   16/07/07  Keith   Improved Error message around Sabrix Tax
   02.06.01   21/10/09  Simon   Grouping now done by transaction_table not assignment_id.
   02.06.02   28/12/09  Keith   Added Calc_legal_exch_rate
   02.06.03   02/11/10  Nigel   Fixed issue with get_grouping_rule_id custom event.
   02.06.04   04/07/11  Keith   Added Pass through at Assignment
   03.06.05   27/03/12  Keith   Added Exclude Rule
   03.06.06   21/11/12  Keith   Removed old Sabrix call.
******************************************************************************/
 
  vTiming             xxcp_global.gTiming_rec;
	vTimingCnt          pls_integer;
  -- Current Records

  gBalancing_Array    XXCP_DYNAMIC_ARRAY := XXCP_DYNAMIC_ARRAY(' ',' ',' ',' ',' ',' ',' ',' ',' ',' ');
  gTrans_Table_Array  XXCP_DYNAMIC_ARRAY := XXCP_DYNAMIC_ARRAY();

  
  TYPE gCursors_Rec is RECORD(
    v_cursorId     pls_Integer := 0,
    v_Target_Table XXCP_column_rules.transaction_table%type,
    v_usage        varchar2(1),
    v_OpenCur      varchar2(1) := 'N',
    v_LastCur      pls_integer := -1);

  TYPE gCUR_REC is TABLE of gCursors_Rec INDEX by BINARY_INTEGER;
  gCursors gCUR_REC;

  -- *************************************************************
  --                     Software Version Control
  --                   -----------------------------
  -- This package can be checked with SQL to ensure it's installed
  -- select packagename.software_version from dual;
  -- *************************************************************
  Function Software_Version RETURN VARCHAR2 IS
   BEGIN
     RETURN('Version [03.06.06] Build Date [21-NOV-2012] Name [XXCP_PV_AR_ENGINE]');
   END Software_Version;
	 
  -- !! ***********************************************************************************************************
  -- !!
  -- !!                                             Fetch_Source_Rowid 
  -- !!
  -- !! ***********************************************************************************************************
	Function Fetch_Source_Rowid(cRowid in rowid) Return Rowid is

   Cursor SX (pSource_Rowid in rowid) is
      Select vt_source_rowid
        from XXCP_PV_INTERFACE l
       where rowid            = pSource_Rowid
         and l.vt_preview_id  = xxcp_global.gCommon(1).Preview_id;

  vReturn_rowid rowid;
				 
  Begin
	  For SXRec in SX(cRowid) Loop
      vReturn_rowid := SXRec.vt_source_rowid;
    End Loop;
		
		Return(vReturn_rowid);
                 
  End Fetch_Source_Rowid;
	 
  -- !! ***********************************************************************************************************
  -- !!
  -- !!                                             Open_Cursors
  -- !!
  -- !! ***********************************************************************************************************
  Function Open_Cursor(cUsage in varchar2, cTarget_Table in varchar2) Return pls_integer is
  
    i    pls_integer;
    vPos pls_integer := 0;

  Begin
    For i in 1 .. gCursors.count loop
      If (gCursors(i).v_Target_Table = cTarget_Table) AND (gCursors(i).v_usage = cUsage) then
        vPos := i;
        If gCursors(i).v_OpenCur = 'N' then
          gCursors(i).v_cursorId := DBMS_SQL.OPEN_CURSOR;
          gCursors(i).v_OpenCur := 'Y';
          gCursors(i).v_LastCur := -1;
          exit;
        End If;
      End If;
    End Loop;
  
    Return(vPos);
  End Open_Cursor;

  -- *************************************************************
  --
  --                  ClearWorkingStorage
  --
  -- *************************************************************
  Procedure ClearWorkingStorage IS
  begin
    -- Processed Records
    xxcp_wks.working_cnt := 0;
    xxcp_wks.working_rcd.delete;
    -- Current Records before processing
    xxcp_wks.source_cnt := 0;
    xxcp_wks.source_rowid.delete;
    xxcp_wks.source_status.delete;
    xxcp_wks.source_sub_code.delete;
  End ClearWorkingStorage;

 --  !! ***********************************************************************************************************
 --  !!   Stop_Whole_Transaction
 --  !! ***********************************************************************************************************
 Procedure Stop_Whole_Transaction is
 

 Begin
   -- Set all transactions in group to error status
   Begin
    Update XXCP_pv_interface
      set vt_status              = 'WAITING'
         ,vt_internal_Error_code = 0 -- Associated errors
         ,VT_DATE_PROCESSED  = xxcp_global.SystemDate
       WHERE vt_preview_id             = xxcp_global.gCommon(1).Preview_id
         AND vt_Source_Assignment_id = xxcp_global.gCommon(1).current_assignment_id
         AND vt_parent_trx_id = xxcp_global.gCommon(1).current_parent_Trx_id
         AND NOT vt_status IN ('TRASH', 'IGNORED');
   End;
   -- Update Record that actually caused the problem.
   Begin
     Update XXCP_pv_interface
        set vt_status          = 'WAITING'
           ,VT_DATE_PROCESSED  = xxcp_global.SystemDate
      where Rowid                  = xxcp_global.gCommon(1).current_source_rowid
        and vt_preview_id          = xxcp_global.gCommon(1).Preview_id;
   End;

   ClearWorkingStorage;

 End Stop_Whole_Transaction;

 --
 --   !! ***********************************************************************************************************
 --   !!
 --   !!                                   Error_Whole_Transaction
 --   !!                If one part of the wrapper finds a problem whilst processing a record,
 --   !!                          then we need to error the whole transaction.
 --   !!
 --   !! ***********************************************************************************************************
 --
 Procedure Error_Whole_Transaction(cInternalErrorCode  in out Number) is

 vVT_Source_Rowid Rowid;

 Begin
   -- Set all transactions in group to error status
   Begin
    update XXCP_pv_interface
      set vt_status = 'ERROR'
         ,vt_internal_Error_code    = 12823 -- Associated errors
    where vt_Source_Assignment_id   = xxcp_global.gCommon(1).current_Assignment_id
      and vt_Parent_Trx_id          = xxcp_global.gCommon(1).current_Parent_Trx_id
      and vt_preview_id             = xxcp_global.gCommon(1).Preview_id
      and vt_Transaction_Table      = xxcp_global.gCommon(1).current_Transaction_Table;
   End;
   -- Update Record that actually caused the problem.
   Begin
     update XXCP_pv_interface
        set vt_status              = 'ERROR'
           ,vt_internal_error_code = cInternalerrorcode
           ,vt_date_processed      = sysdate
      where Rowid                  = xxcp_global.gCommon(1).current_source_rowid
        and vt_preview_id          = xxcp_global.gCommon(1).Preview_id;
   End;

    If xxcp_global.gCommon(1).Custom_events = 'Y' then

		  vVT_Source_Rowid := fetch_source_rowid(xxcp_global.gCommon(1).current_source_rowid);

      xxcp_custom_events.ON_TRANSACTION_ERROR(xxcp_global.gCommon(1).Source_id,
                                              xxcp_global.gCommon(1).current_assignment_id,
                                              vVT_Source_Rowid,
                                              xxcp_global.gCommon(1).current_Transaction_Table,
                                              xxcp_global.gCommon(1).current_Parent_Trx_id,
                                              xxcp_global.gCommon(1).current_transaction_id,
                                              cInternalErrorCode);
    End If;

    ClearWorkingStorage;

End Error_Whole_Transaction;

--
--   !! ***********************************************************************************************************
--   !!
--   !!                                     No_Action_Transaction
--   !!                                If there are no records to process.
--   !!
--   !! ***********************************************************************************************************
--
Procedure No_Action_Transaction(cSource_Rowid in rowid) is
Begin
  -- Update Record that actually had not action required
  Begin
    update XXCP_pv_interface
       set vt_status              = 'SUCCESSFUL'
          ,vt_internal_error_code = null
          ,vt_date_processed      = xxcp_global.systemdate
          ,vt_status_code         = 7003
     where rowid                  = cSource_rowid
       and vt_preview_id          = xxcp_global.gCommon(1).Preview_id;
  End;
End No_Action_Transaction;

--   !! ***********************************************************************************************************
--   !!
--   !!                                     Pass_Through_Whole_Trx
--   !!                                If there are no records to process.
--   !!
--   !! ***********************************************************************************************************
Procedure Pass_Through_Whole_Trx(cSource_Rowid in Rowid, cPassThroughCode in number) is


  vStatus XXCP_pv_interface.vt_status%type;
 
 Begin
 
   If cPassThroughCode between 7021 AND 7024 then
	   vStatus := 'IGNORED';
	 Else
	   vStatus := 'PASSTHROUGH';
	 End If;
 
   If cPassThroughCode in (7022, 7026) then -- Parent level   
     xxcp_global.gPassThroughCode := cPassThroughCode;
	 End if;
      
   Update XXCP_pv_interface p
      set p.vt_status               = vStatus
         ,p.vt_internal_Error_code  = Null
         ,p.vt_status_code          = cPassThroughCode
         ,p.vt_date_processed       = xxcp_global.SystemDate
    where p.rowid = cSource_Rowid
      and p.vt_request_id           = xxcp_global.gCommon(1).Preview_id;
	 
 End Pass_Through_Whole_Trx;

/*
  !! ***********************************************************************************************************
  !!
  !!                                   Transaction_Group_Stamp
  !!                 Stamp Incomming record with parent Trx to create groups of
  ||                    transactions that resemble on document transaction.
  !!
  !! ***********************************************************************************************************
*/
Procedure Transaction_Group_Stamp(cStamp_Parent_Trx in varchar2,cSource_assignment_id in number, cSource_Rowid in Rowid, cInternalErrorCode out number) IS
 
   

    vStatement          varchar2(2000);
    vInternalErrorCode  Number(8) := 0;

    -- Distinct list of Transaction Tables
    -- populated in Set_Running_Status
    -- 02.06.03 Changed to look at all the distinct grouping rules in the interface table. 
    CURSOR cTrxTbl is
      select distinct vt_grouping_rule_id Grouping_Rule_Id
      from   XXCP_pv_interface g
      where  g.vt_preview_id           = xxcp_global.gCommon(1).preview_id
      and    g.vt_source_assignment_id = cSource_assignment_id
      and    g.vt_status               = 'ASSIGNMENT';      

    Cursor c1 (pSource_assignment_id in number) IS
      select distinct 'Update XXCP_pv_interface j
                          set vt_parent_trx_id = (select '||t.Parent_trx_column|| ' from XXCP_ra_interface_lines k where k.rowid = j.vt_source_rowid)
                        where vt_status = ''ASSIGNMENT''
                          and vt_preview_id  = '||to_char(xxcp_global.gCommon(1).Preview_id)||'
                          and vt_source_assignment_id = '||to_char(pSource_assignment_id)||'
                          and vt_transaction_table = '''||g.vt_transaction_table||'''' Statement_line
           ,t.parent_trx_column
           ,t.Transaction_Table
        from XXCP_pv_interface g
            ,XXCP_sys_source_tables t
  	        ,XXCP_source_assignments x
       where g.vt_transaction_table    = t.transaction_table
         and t.source_id               = x.source_id
         and g.vt_preview_id           = xxcp_global.gCommon(1).preview_id
         and x.source_assignment_id    = psource_assignment_id
         and g.vt_source_assignment_id = x.source_assignment_id;

    cursor er1(psource_assignment_id in number) IS
      select g.vt_interface_id, g.rowid source_rowid, g.vt_transaction_table
        from XXCP_pv_interface g
       where g.vt_status = 'ASSIGNMENT'
         and g.vt_source_assignment_id = psource_assignment_id
         and g.vt_preview_id           = xxcp_global.gCommon(1).preview_id
	       and g.vt_parent_trx_id is null;


Begin

     For cTblRec in cTrxTbl Loop  -- Loop for all grouping rules within this set of interface records.

        If cTblRec.Grouping_Rule_Id > 0 THEN  --02.06.02 
          XXCP_DYNAMIC_SQL.Apply_Grouping_Rules(cInternalErrorCode => vInternalErrorCode,
                                                cGrouping_rule_id  => cTblRec.Grouping_Rule_Id); -- 02.06.03

         -- Error Checking for Null Parent Trx id
           For er1Rec in er1(cSource_assignment_id) LOOP
           update XXCP_pv_interface l
           set vt_status                = 'ERROR'
              ,vt_parent_trx_id         = null
              ,l.vt_internal_error_code = 11090
          where l.vt_transaction_table = er1rec.vt_transaction_table
            and l.vt_preview_id        = xxcp_global.gCommon(1).preview_id;
        end loop;
  		  
        Else

           -- Mass update of Parent Trx id
           For rec in c1(cSource_assignment_id) LOOP

             If rec.parent_trx_column is null then
                vInternalErrorCode := 10665;
                xxcp_foundation.FndWriteError(vInternalErrorCode,'Transaction Table <'||Rec.Transaction_Table||'>');
             Else
               vStatement := rec.Statement_line;
             Begin
                Execute Immediate vStatement;

                Exception when OTHERS then
                   vInternalErrorCode := 10667;
                   xxcp_foundation.FndWriteError(vInternalErrorCode,SQLERRM,vStatement);
               End;
             End If;
           End loop;
        End If;
			End Loop;
			xxcp_te_base.Preview_Mode_Restrictions(cCalling_Wrapper      => 'XXCP_PV_GL_ENGINE', 
			                                       cSource_assignment_id => cSource_assignment_id, 
																             cSource_Rowid         => cSource_Rowid  
																             ); 

      cInternalErrorCode := vInternalErrorCode;

  End Transaction_Group_Stamp;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
 --                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
 -- !! ***********************************************************************************************************                                                                                                                                                                                                                                                                                                                                                                                                  
 -- !!                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
 -- !!                                   FlushRecordsToTarget
 -- !!     Control The process of moving the Array elements throught to creating the new output records
 -- !!
 -- !! ***********************************************************************************************************
  Function FlushRecordsToTarget(cGlobal_Rounding_Tolerance in number,
                                cGlobalPrecision           in number,
                                cTransaction_Type          in varchar2,
                                cInternalErrorCode         in out number)
    return number is

    c              pls_integer := 0;
    j              pls_integer := 0;

    vCnt           pls_integer := 0;
  
    vEntered_DR    Number := 0;
    vEntered_CR    Number := 0;
    fe             pls_integer := xxcp_wks.working_cnt;
    vRC            pls_integer := xxcp_wks.source_cnt;
    vRC_Records    pls_integer := 0;
    vRC_Error      pls_integer := 0; 
  
    vStatus XXCP_gl_interface.vt_status%type; 
    vErrorMessage varchar2(3000);
  
    UDI_Cnt       pls_integer := 0;

   vVT_Source_Rowid  Rowid;
   vZero_Action      pls_integer;
	
BEGIN

    -- Rounding is Optional in AR 
    If fe > 0  then
      xxcp_te_base.Account_Rounding(cGlobal_Rounding_Tolerance,
                                    cGlobalPrecision,
                                    cTransaction_Type,
                                    vEntered_DR,
                                    vEntered_CR,
                                    cInternalErrorCode);
    End If;

    -- Check that for each attribute record we have a records for output.
    For c in 1 .. vRC Loop
      vRC_Error := 0;
      FOR j in 1 .. fe LOOP
        If xxcp_wks.Source_rowid(c) = xxcp_wks.WORKING_RCD(j).Source_rowid then
		      xxcp_wks.WORKING_RCD(j).Source_Pos := c;
		      xxcp_wks.SOURCE_STATUS(c) := 'FLUSH';
          vRC_Error   := 1;
          vRC_Records := vRC_Records + 1;
        End if;
      End loop;

      IF vRC_Error = 0 THEN
        xxcp_wks.SOURCE_SUB_CODE(c) := 7003;
		    xxcp_wks.SOURCE_STATUS(c) := 'NO RULES';
		    No_Action_Transaction(xxcp_wks.Source_rowid(c));
      END IF;
    END LOOP;
   
    Savepoint TARGET_INSERT;
  
    If cInternalErrorCode = 0 and vRC_Records > 0 then
 

      For j in 1 .. fe Loop
      
        Exit when cInternalErrorCode <> 0;
      
        vCnt := vCnt + 1;
      
        -- Custom Events 
        If xxcp_global.gCommon(1).Custom_events = 'Y' then
   				xxcp_te_base.Custom_Event_Insert_Row(j,cInternalErrorCode);	        
        End If;

        -- Tag Zero Account Value rows

        If cInternalErrorCode = 0 then
        
		      xxcp_global.gCommon(1).current_source_rowid := xxcp_wks.Source_rowid(xxcp_wks.WORKING_RCD(j).Source_Pos);
					
        	-- Find the Action to take for Zero Accounted values
          vZero_Action := xxcp_te_base.Zero_Suppression_Rule(j);

          IF vZero_Action > 0 THEN
                 Select XXCP_PREVIEW_HIST_SEQ.nextval into xxcp_wks.WORKING_RCD(j) .Process_History_id from dual;

                 vVT_Source_Rowid := fetch_source_rowid(xxcp_wks.WORKING_RCD(j).Source_rowid);
          
                  If cInternalErrorCode = 0 then

                    ---
                    --- XXCP_PROCESS_HISTORY 
                    ---
                    XXCP_PV_PROCESS_HIST_AR.Process_History_Insert(
										                                          cSource_Rowid            => vVT_Source_Rowid,  -- Preview Mode only,
																											        cParent_Rowid            => Null,
                                                              cTarget_Table            => xxcp_wks.WORKING_RCD(j).Target_Table,
                                                              cTarget_Instance_id      => xxcp_wks.WORKING_RCD(j).Target_Instance_id,
                                                              cProcess_History_id      => xxcp_wks.WORKING_RCD(j).Process_History_id,
                                                              cTarget_assignment_id    => xxcp_wks.WORKING_RCD(j).Target_Assignment_id,
                                                              cAttribute_id            => xxcp_wks.WORKING_RCD(j).Attribute_id,
                                                              cRecord_type             => xxcp_wks.WORKING_RCD(j).Record_type,
                                                              cRequest_id              => xxcp_global.gCommon(1).current_request_id,
                                                              cStatus                  => xxcp_wks.WORKING_RCD(j).Record_Status,
                                                              cInterface_id            => xxcp_wks.WORKING_RCD(j).Interface_id,
                                                              cModel_ctl_id            => xxcp_wks.WORKING_RCD(j).Model_ctl_id,
                                                              cRule_id                 => xxcp_wks.WORKING_RCD(j).Rule_id,
                                                              cTax_registration_id     => xxcp_wks.WORKING_RCD(j).Tax_Registration_id,
																															cEntered_rounding_amount => xxcp_wks.WORKING_RCD(j).Entered_Rnd_Amount,
																															cAccount_rounding_amount => xxcp_wks.WORKING_RCD(j).Accounted_Rnd_Amount,
                                                              cD1001_Array             => xxcp_wks.WORKING_RCD(j).D1001_Array,
                                                              cD1002_Array             => xxcp_wks.WORKING_RCD(j).D1002_Array,
                                                              cD1003_Array             => xxcp_wks.WORKING_RCD(j).D1003_Array,
                                                              cD1004_Array             => xxcp_wks.WORKING_RCD(j).D1004_Array,
																											        cD1005_Array             => xxcp_wks.WORKING_RCD(j).D1005_Array,
                                                              cD1006_Array             => xxcp_wks.WORKING_RCD(j).D1006_Array,
                                                              cI1009_Array             => xxcp_wks.WORKING_RCD(j).I1009_Array,
                                                              cErrorMessage            => vErrorMessage,
                                                              cInternalErrorCode       => cInternalErrorCode);
                                                              
                     If cInternalErrorCode = 0 then
                       vTiming(vTimingCnt).Flush_Records := nvl(vTiming(vTimingCnt).Flush_Records,0) + 1;
										 End If;					  
                  End If;
                
                  If cInternalErrorCode = 0 then
                     vStatus := 'SUCCESSFUL';
				          Else
                     vStatus := 'ERROR';
                     xxcp_wks.SOURCE_SUB_CODE(xxcp_wks.WORKING_RCD(j).Source_Pos):= 7008;
                  End If;
			      
				      xxcp_wks.SOURCE_STATUS(xxcp_wks.WORKING_RCD(j).Source_Pos) := vStatus;
            Else
              -- Zero Accounted Records 
              xxcp_wks.SOURCE_SUB_CODE(xxcp_wks.WORKING_RCD(j).Source_Pos):= 7008;
			        xxcp_wks.SOURCE_STATUS(xxcp_wks.WORKING_RCD(j).Source_Pos) := 'SUCCESSFUL';
            End If;
            
			      UDI_Cnt := UDI_Cnt + 1;

          
          END IF;
   
      END LOOP;
    
      If cInternalErrorCode = 0 and UDI_Cnt > 0 then
        FORALL j in 1 .. xxcp_wks.SOURCE_CNT
          Update XXCP_pv_interface
             Set vt_Status              = xxcp_wks.SOURCE_STATUS(j),
                 vt_internal_Error_code = Null,
                 vt_date_processed      = xxcp_global.SystemDate,
                 vt_Status_Code         = xxcp_wks.SOURCE_SUB_CODE(j)
           where rowid = xxcp_wks.Source_rowid(j);
      End If;

    END IF;
	
	 -- Source_Pos
  
    If cInternalErrorCode <> 0 then
      -- Rollback
      If vCnt > 0 then
        Rollback To TARGET_INSERT;
        xxcp_foundation.FndWriteError(cInternalErrorCode, vErrorMessage);
      End If;
    
      Error_Whole_Transaction(cInternalErrorCode);
    
      If cInternalErrorCode = 12800 then
        xxcp_foundation.FndWriteError(cInternalErrorCode,
          'Entered DR <' ||to_char(vEntered_DR) ||'> Entered CR <' ||to_char(vEntered_CR) || '>');
      End If;
    
    End If;
  
    -- Remove Array Elements
    ClearWorkingStorage;
  
    Return(vCnt);
  End FlushRecordsToTarget;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
  --  !! ***********************************************************************************************************
  --  !!
  --  !!                                   Reset_Errored_Transactions                                                                                                                                                                                                                                                                                                                                                                                                                                                   
  --  !!                              Reset Transactions from a previous run.
  --  !!                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
  --  !! ***********************************************************************************************************                                                                                                                                                                                                                                                                                                                                                                                                    
  Procedure Reset_Errored_Transactions(cSource_assignment_id in number,
                                       cRequest_id           in number) is
  
  begin
  
    vTiming(vTimingCnt).Reset_Start := to_char(sysdate, 'HH24:MI:SS');
  
    -- Update Interface
    Begin
    
      Update XXCP_pv_interface r
         set vt_status              = 'NEW',
             vt_Internal_Error_Code = Null,
             vt_status_code         = Null,
             vt_request_id          = cRequest_id
       where vt_source_assignment_id = cSource_assignment_id
			   and vt_preview_id             = xxcp_global.gCommon(1).Preview_id
         and (vt_status in
             ('ASSIGNMENT','ERROR', 'GROUPING','PROCESSING', 'TRANSACTION','WAITING'));
    
    exception
      when OTHERS then
        null;
    End;
  
    -- Remove Errors 
    Begin
      Delete from XXCP_pv_errors cx
       where source_assignment_id = cSource_assignment_id
			   and preview_id           = xxcp_global.gCommon(1).Preview_id;

    Exception
      when OTHERS then
        null;
    End;
    Commit;
  End RESET_ERRORED_TRANSACTIONS;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
 -- !! ***********************************************************************************************************                                                                                                                                                                                                                                                                                                                                                                                                    
 -- !!
 -- !!                                  Set_Running_Status                                                                                                                                                                                                                                                                                                                                                                                                                                                            
 -- !!                      Flag the records that are going to be processed.                                                                                                                                                                                                                                                                                                                                                                                                                                          
 -- !!                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
 -- !! ***********************************************************************************************************                                                                                                                                                                                                                                                                                                                                                                                                    
  Procedure Set_Running_Status(cSource_id            in number,
                               cSource_assignment_id in number,
                               cRequest_id           in number) is
  
    cursor rx(pSource_id in number, pSource_Assignment_id in number, pRequest_id in number) is
      select r.vt_transaction_table,
             r.vt_transaction_id,
             r.rowid source_rowid,
             r.vt_transaction_type,
             nvl(t.grouping_rule_id,0) grouping_rule_id,
						 r.vt_source_rowid
        from XXCP_pv_interface r, XXCP_sys_source_tables t
       where r.vt_preview_id = xxcp_global.gCommon(1).Preview_id
         and vt_source_assignment_id = pSource_assignment_id
         and vt_status = 'NEW'
         and r.vt_transaction_table = t.transaction_table
         and t.source_id = pSource_id
    order by t.transaction_table
         for update;
  
    vGrouping_rule_id XXCP_gl_interface.vt_grouping_rule_id%type;
  
    -- Find records with no vt_grouping_rule_id
    cursor er2(pSource_assignment_id in number) is
      select Distinct g.vt_transaction_table
        from XXCP_pv_interface g, XXCP_sys_source_tables t
       where g.vt_request_id = cRequest_id
         and g.vt_preview_id = xxcp_global.gCommon(1).Preview_id
         and g.vt_status = 'ASSIGNMENT'
         AND g.vt_transaction_table    = t.transaction_table
         and g.vt_source_assignment_id = pSource_assignment_id
         AND t.grouping_rule_id IS NOT NULL  -- 02.06.01
         and g.vt_grouping_rule_id is null;
  
    -- Find records with invalid Grouping rule 
    cursor er3(pSource_assignment_id in number) is
      select Distinct g.vt_transaction_table
        from XXCP_pv_interface g, XXCP_grouping_rules r, XXCP_sys_source_tables t
       where g.vt_preview_id = xxcp_global.gCommon(1).Preview_id
         and g.vt_status = 'ASSIGNMENT'
         AND g.vt_transaction_table    = t.transaction_table
         and g.vt_source_assignment_id = pSource_assignment_id
         and g.vt_grouping_rule_id = r.grouping_rule_id(+)
         AND t.grouping_rule_id IS NOT NULL  -- 02.06.01
         and r.grouping_rule_id is null;

    vCurrent_request_id number;
    vGroupingRuleExists BOOLEAN     := FALSE; 
  
  begin
  
    vTiming(vTimingCnt).Set_Running_Start := to_char(sysdate, 'HH24:MI:SS');
  
    vCurrent_Request_id := nvl(cRequest_id, 0);
  
    For rec in rx(cSource_id, cSource_assignment_id, vCurrent_Request_id) Loop
      vGrouping_rule_id := Null;

      -- 02.06.03 No longer needed. Needs to be removed in all wrappers.  
/*      -- Populate Transaction_Table Array
      if vPrevTransaction_Table <> rec.vt_transaction_table then
        gTrans_Table_Array.extend;
        gTrans_Table_Array(gTrans_Table_Array.count) := rec.vt_transaction_table;
      End if;

      vPrevTransaction_Table := rec.vt_transaction_table;
*/
      IF Rec.grouping_rule_id is not null then  -- 02.06.02
      
        vGroupingRuleExists := TRUE;
      
				xxcp_global.gCommon(1).current_transaction_table := Rec.vt_Transaction_Table;
        xxcp_global.gCommon(1).current_transaction_id    := Rec.vt_Transaction_id;

        vGrouping_rule_id := xxcp_custom_events.get_group_rule_id(cSource_id,
                                                                  cSource_assignment_id,
                                                                  rec.vt_source_rowid,
                                                                  rec.vt_transaction_table,
                                                                  rec.vt_transaction_type);

        If nvl(vGrouping_rule_id, 0) = 0 then
          vGrouping_rule_id := Rec.grouping_rule_id; -- Default
        End If;
      End If;
      -- Set Processing Status   
      begin
        update XXCP_pv_interface r
           set vt_status           = 'ASSIGNMENT',
               vt_date_processed   = xxcp_global.SystemDate,
               vt_status_code      = Null,
               vt_request_id       = cRequest_id,
               vt_Grouping_rule_id = vGrouping_rule_id
         where r.rowid = rec.source_rowid
				  and  vt_preview_id       = xxcp_global.gCommon(1).Preview_id;
      
      Exception
        when OTHERS then null;
      End;
    End Loop;
  
    -- Error Checking 
    If vGroupingRuleExists then  -- 02.06.01 Don't Check If no Grouping Rule used.
      -- Find records with no vt_grouping_rule_id 
      For Rec in er2(cSource_assignment_id) loop
        update XXCP_pv_interface g
           set g.vt_status              = 'ERROR',
               g.vt_date_processed      = xxcp_global.SystemDate,
               g.vt_internal_error_code = 11088
         where g.vt_preview_id           = xxcp_global.gCommon(1).Preview_id
           and g.vt_status     = 'ASSIGNMENT'
           and g.vt_source_assignment_id = cSource_assignment_id
           and g.vt_transaction_table = rec.vt_transaction_table;
      End Loop;
      -- Find records with invalid Grouping rule 
      For Rec in er3(cSource_assignment_id) loop
        update XXCP_pv_interface g
           set g.vt_status              = 'ERROR',
               g.vt_date_processed      = xxcp_global.SystemDate,
               g.vt_internal_error_code = 11089
         where vt_preview_id             = xxcp_global.gCommon(1).Preview_id
           and g.vt_status               = 'ASSIGNMENT'
           and g.vt_source_assignment_id = cSource_assignment_id
           and g.vt_transaction_table = rec.vt_transaction_table;
      End Loop;
    End If;
  
    Commit;
  
  End Set_Running_Status;
 
 -- !! ***********************************************************************************************************
 -- !!
 -- !!                                    CONTROL
 -- !!                  External Procedure (Entry Point) to start the AR Engine.
 -- !!
 -- !! ***********************************************************************************************************
  Function Control(cSource_Group_id         IN NUMBER,
	                 cSource_Assignment_id    IN NUMBER,
                   cPreview_id              IN NUMBER,
                   cParent_trx_id           IN NUMBER   DEFAULT 0,
                   cUser_id                 IN NUMBER,
                   cLogin_id                IN NUMBER,
									 cPV_Zero_Flag            IN VARCHAR2,
									 cSource_Rowid            IN Rowid default null) RETURN NUMBER is

    -- Assignment
    cursor cfg(pSource_id in number, pSource_Assignment_id in Number, pRequest_id in number) is
                select  g.VT_TRANSACTION_TABLE
                       ,g.VT_TRANSACTION_TYPE
                       ,g.VT_TRANSACTION_CLASS
                       ,g.VT_TRANSACTION_ID
                       ,x.VT_PARENT_TRX_ID
											 ,x.VT_TRANSACTION_REF
                       ,s.SET_OF_BOOKS_ID
                       ,x.VT_Source_Assignment_ID
                       ,x.ROWID SOURCE_ROWID
                       ,g.CURRENCY_CODE
                       ,0 Entered_DR
                       ,0 Entered_CR
                       ,s.instance_id source_instance_id
                       ,s.source_assignment_id
                       ,s.source_id
                       ,s.set_of_books_id Source_Set_of_books_id
                       ,g.vt_interface_id
                       ,x.vt_source_rowid
                       ,w.transaction_set_name
                       ,nvl(w.Transaction_Set_id,0) Transaction_Set_id
											 ,x.vt_transaction_date
											 ,y.source_table_id
											 ,y.source_type_id
											 ,cx.source_class_id
                       ,t.calc_legal_exch_rate
                  from XXCP_pv_interface       x,
                       XXCP_source_assignments s,
                       XXCP_ra_interface_lines       g,
                       -- New
                       XXCP_sys_source_tables       t,
                       XXCP_sys_source_types        y,
                       XXCP_sys_source_classes     cx,
                       XXCP_source_transaction_sets w
                 where x.vt_status = 'ASSIGNMENT'
                   and x.vt_source_assignment_id = pSource_assignment_id
                   and x.vt_source_assignment_id = s.source_assignment_id
                   -- Table
                   and g.vt_transaction_table = t.transaction_table
                   and t.source_id            = pSource_id
                   -- Type
                   and g.vt_transaction_type  = y.type
									 and y.latch_only           = 'N'
                   and y.source_table_id      = t.source_table_id
                   -- Class
                   and g.vt_transaction_class = cx.class
									 and cx.latch_only          = 'N'
                   and y.source_table_id      = cx.source_table_id
                   -- Transaction_Set_id
                   and y.Transaction_Set_id   = w.Transaction_Set_id
                   --
                   and g.rowid = x.vt_source_rowid
                   and x.vt_preview_id = xxcp_global.gCommon(1).Preview_id
                   order by x.vt_transaction_table, w.sequence, g.vt_parent_trx_id, cx.sequence, x.vt_transaction_id;

  
    CFGRec CFG%rowtype;
  
    -- Transaction
    cursor GLI(pSource_id in number, pSource_Assignment_id in Number, pRequest_id in number) is
            select       
             g.vt_transaction_table
            ,g.vt_transaction_type
            ,g.vt_Interface_id
            ,g.vt_transaction_class
            ,x.vt_source_rowid
            ,g.vt_transaction_id
						,x.vt_transaction_Ref
            ,s.instance_id vt_instance_id
            --
            ,g.Cust_trx_type_name Category
            ,g.INTERFACE_LINE_CONTEXT Source
            ,g.gl_date Accounting_Date
            --
            ,g.Currency_Code
            ,x.vt_parent_trx_id
            ,x.ROWID Source_Rowid
            ,s.set_of_books_id
            ,g.amount Parent_Entered_Amount
            ,g.Currency_Code Parent_Entered_Currency
            ,s.source_assignment_id
            ,s.source_id
            ,w.transaction_set_name
            ,w.Transaction_Set_id
					  ,y.source_table_id
						,y.source_type_id						
					  ,cx.source_class_id
						,class_mapping_req
						,0 Transaction_Cost
						,0 code_combination_id
        from XXCP_ra_interface_lines    g,
             XXCP_pv_interface          x,
             XXCP_source_assignments    s,
             -- New
             XXCP_sys_source_tables       t,
             XXCP_sys_source_types        y,
             XXCP_sys_source_classes      cx,
             XXCP_source_transaction_sets w
       where g.rowid                   = x.vt_source_rowid
         and x.vt_preview_id           = xxcp_global.gCommon(1).Preview_id
         and x.vt_status               = 'TRANSACTION'
         and x.vt_source_assignment_id = pSource_Assignment_ID
         and x.vt_source_assignment_id = s.source_assignment_id
         -- Table 
         and g.vt_transaction_table = t.transaction_table
         and t.source_id            = pSource_id
         -- Type 
         and g.vt_transaction_type  = y.type
				 and y.latch_only             = 'N'
         and t.source_table_id      = y.source_table_id
         -- Class
         and g.vt_transaction_class    = cx.class
				 and cx.latch_only             = 'N'
         and y.source_table_id         = cx.source_table_id
         -- Transaction_Set_id
         and y.Transaction_Set_id   = w.Transaction_Set_id
         --
        ORDER BY g.vt_transaction_table DESC, w.sequence, g.vt_parent_trx_id, cx.sequence, x.vt_parent_trx_id, x.vt_transaction_id;
                
    GLIRec GLI%Rowtype;
  
    Cursor ConfErr(pSource_Assignment_id in number, pRequest_id in number) is
      select Distinct k.vt_parent_trx_id, k.vt_transaction_table
        from XXCP_pv_interface k
       where k.vt_request_id = pRequest_id
         and k.vt_source_assignment_id = pSource_Assignment_id
         and k.vt_status = 'ERROR';
  
    -- Assignments
    Cursor sr1(pSource_Assignment_id in number) is
      select set_of_books_id,
             Source_Assignment_id,
             instance_id source_instance_id,
             Stamp_Parent_Trx,
             Source_id
        from XXCP_source_assignments t
       where t.Source_Assignment_id = pSource_Assignment_id
			   and t.active               = 'Y';
  
    vInternalErrorCode        XXCP_errors.INTERNAL_ERROR_CODE%type := 0;
    vExchange_Rate_Type       XXCP_tax_registrations.EXCHANGE_RATE_TYPE%type;
    vCommon_Exch_Curr         XXCP_tax_registrations.COMMON_EXCH_CURR%type;

    vGlobalPrecision          pls_integer := 2;
    CommitCnt                 pls_Integer := 0;
    vSource_Activity          varchar2(4) := 'AR';
    vJob_Status               Number(1) := 0;
  
    vTransaction_Class        XXCP_sys_source_classes.Class%type;
    vRequest_ID               XXCP_process_history.request_id%type;
  
    -- NEW PARAMETERS
    vCurrent_Parent_Trx_id    XXCP_transaction_attributes.parent_trx_id%type := 0;
    vTransaction_Type         XXCP_sys_source_types.Type%type;

    --
    i                          pls_integer := 0;
    k                          Number;
    j                          Integer:= 0;
    
    vGlobal_Rounding_Tolerance Number := 0;
    vTransaction_Error         Boolean := False;
  
    -- Used for the REC Distribution
    vExtraLoop          boolean := False;
		vIsExtraClass       varchar2(30);
    vExtraClass         varchar2(30);
		vExtraCount         pls_integer := 0;
    --
    vSource_Assignment_id XXCP_source_assignments.source_assignment_id%type;
    vStamp_Parent_Trx     XXCP_source_assignments.stamp_parent_trx%type := 'N';
    vSource_instance_id   XXCP_source_assignments.instance_id%type;
    vSource_ID            XXCP_sys_sources.source_id%type;
    vClass_Mapping_Name   XXCP_sys_source_classes.CLASS%type;
	   
    g  number;
		
    vTiming_Start      number;
    vDML_Compiled      varchar2(1); 
  
    Cursor SF(pSource_Group_id in number) is
      select s.source_activity,
             s.Source_id,
             Preview_ctl,
             Timing,
             Custom_events,
             s.Cached,
             s.DML_COMPILED
        from XXCP_source_assignment_groups g, XXCP_sys_sources s
       where source_group_id = pSource_Group_id
         and s.source_id = g.source_id
         and s.active = 'Y';
 
    Cursor Tolx is
      Select Numeric_Code Global_Rounding_Tolerance
        from XXCP_lookups
       where lookup_type = 'ROUNDING TOLERANCE'
         and lookup_code = 'GLOBAL';
  
    Cursor CRL(pSource_id in number) is
     select Distinct Transaction_table, source_id
       from XXCP_column_rules
      where source_id = pSource_id;

  
  BEGIN
  
    xxcp_global.User_id  := cUser_id;
    xxcp_global.Login_id := fnd_global.login_id;
    xxcp_global.Trace_on := xxcp_reporting.Get_Trace_Mode(cUser_id);
    xxcp_global.Preview_on := 'Y';
    xxcp_global.SystemDate := Sysdate;
    xxcp_global.gCommon(1).Preview_id := cPreview_id;
		xxcp_global.gCommon(1).Preview_Zero_flag  := cPV_Zero_Flag;
		vTimingCnt := 1;

    For SFRec in SF(cSource_Group_id) loop
      xxcp_global.gCommon(1).Source_id               := SFRec.Source_id;
      xxcp_global.gCommon(1).Preview_ctl             := SFRec.Preview_ctl;
      xxcp_global.gCommon(1).Preview_on              := xxcp_global.Preview_on;
      xxcp_global.gCommon(1).Custom_events           := SFRec.Custom_events;
      xxcp_global.gCommon(1).current_Source_activity := SFRec.Source_Activity;
      xxcp_global.gCommon(1).Cached_Attributes       := SFRec.Cached;

      vSource_Activity := SFRec.Source_Activity;
      vSource_id       := SFRec.Source_id;
    	vDML_Compiled    := SFRec.DML_Compiled;

    End Loop;
		 		
    
    xxcp_global.SET_SOURCE_ID(cSource_id => vSource_id);  
    --     SYS.DBMS_SUPPORT.START_TRACE( waits=>true, binds=>true );
    
		vTiming.Delete;
		
    vTiming(vTimingCnt).CF_Records    := 0;
    vTiming(vTimingCnt).Flush_Records := 0;
    vTiming(vTimingCnt).Start_time    := to_char(sysdate, 'HH24:MI:SS');
    vTiming(vTimingCnt).Flush         := 0;
    vTiming_Start            := xxcp_reporting.Get_Seconds;
  
    If xxcp_global.gCommon(1).Custom_events = 'Y' then
      xxcp_custom_events.Set_system_mode(xxcp_global.gCommon);
    End If;
	
    If vDML_Compiled = 'N' then
	    -- Prevent Processing and the column rules have changed and
	    -- they need compiling.
	    vJob_Status := 4;
	    xxcp_foundation.show('ERROR: Column Rules have changed, but not re-generated'); 
	    xxcp_foundation.FndWriteError(10999,'Column Rules have changed, but not re-generated');
    End If;

    --
    -- ## *******************************************************************************************************
    -- ##
    -- ##                            Check to See the process is already in use
    -- ##
    -- ## *******************************************************************************************************
    --

    If vJob_Status = 0 then
      vRequest_id := cPreview_id;
 
      xxcp_foundation.show('Activity locked....' || to_char(vRequest_id));

      xxcp_global.gCommon(1).Current_request_id := vRequest_id;
      If xxcp_global.gCommon(1).Custom_events = 'Y' then
        vInternalErrorCode := xxcp_custom_events.BEFORE_PROCESSING(vSource_id);
      End If;
      --
      -- ## **************************************************************************************************
      -- ##
      -- ##                                Setup Ready for the Run
      -- ##
      -- ## **************************************************************************************************
      --
			
			-- Preview Mode only needs to look at the one source Assignemnt Id, not the whole group.
      For REC in sr1(cSource_Assignment_id) Loop
      
        Exit when vJob_Status <> 0;

        vSource_instance_id   := rec.source_instance_id;
        vSource_Assignment_id := rec.Source_Assignment_id;
        --vStamp_Parent_Trx     := rec.Stamp_Parent_Trx;
        --xxcp_global.gCommon(1).Grouping_Rule := rec.Stamp_Parent_Trx;

        -- Setup Globals
        xxcp_global.gCommon(1).current_process_name  := 'XXCP_ARENG';
        xxcp_global.gCommon(1).current_assignment_id := vSource_Assignment_ID;
      
        --
        -- *********************************************************************************************************
        --                            Populate Column Rules and Initialize Cursors
        -- *********************************************************************************************************
        --
		    If vTimingCnt = 1 Then
         vTiming(vTimingCnt).Init_Start := to_char(sysdate, 'HH24:MI:SS');
				 
         xxcp_te_base.Engine_Init(cSource_id           => vSource_id,
				                          cSource_Group_id     => cSource_Group_id,
																  cInternalErrorCode   => vInternalErrorCode);

        End If;

				vTiming(vTimingCnt).Source_Assignment_id := vSource_Assignment_id; 
      
        -- 02.06.01
        gTrans_Table_Array.delete;

        --
        -- *********************************************************************************************************
        --                            Populate Column Rules and Initialize Cursors
        -- *********************************************************************************************************
        --
        vTiming(vTimingCnt).Memory_Start := to_char(sysdate, 'HH24:MI:SS');

				For Rec in CRL(vSource_id) Loop
          -- Used for column Attributes and error messages 
          xxcp_memory_Pack.LoadColumnRules(vSource_id, 'XXCP_RA_INTERFACE_LINES',vSource_instance_id,Rec.Transaction_table,vRequest_id,vInternalErrorCode);
          xxcp_te_base.Init_Cursors(Rec.Transaction_table);
				End Loop;
        
        vInternalErrorCode := 0;
      
        --
        -- *******************************************************************************************************
        --     Reset Previosly Errored Transactions and Assign Parent Trx Id to Incomming transactions
        -- *******************************************************************************************************
        --
        Reset_Errored_Transactions(cSource_assignment_id => vSource_Assignment_id,
				                           cRequest_id           => vRequest_id);
				
        Set_Running_Status(cSource_id            => vSource_id, 
				                   cSource_assignment_id => vSource_Assignment_id, 
													 cRequest_id           => vRequest_id);
                           
        vInternalErrorCode := xxcp_dynamic_sql.Apply_Exclude_Rule(cSource_assignment_id => vSource_Assignment_id);                      

        If vInternalErrorCode = 0 then

          Transaction_Group_Stamp(cStamp_Parent_Trx     => vStamp_Parent_Trx, 
				                          cSource_assignment_id => vSource_Assignment_id,
						  										cSource_Rowid         => cSource_Rowid,
							  									cInternalErrorCode    => vInternalErrorCode);
        End If;
        
        vTiming(vTimingCnt).Init_End := to_char(sysdate, 'HH24:MI:SS');
        --
        -- GLOBAL ROUNDING TOLERANCE
        --
        vGlobal_Rounding_Tolerance := 0;
        For Rec in Tolx loop
          vGlobal_Rounding_Tolerance := Rec.Global_Rounding_Tolerance;
        End loop;
				
				-- Data Staging 
				If nvl(vInternalErrorCode,0) = 0 then
				  vInternalErrorCode := xxcp_custom_events.Data_Staging(vSource_id,vSource_Assignment_id,Null, cPreview_id);
        End If;

        If vInternalErrorCode = 0 then
          --
          -- Get Standard Parameters
          --
          vInternalErrorCode := xxcp_foundation.fnd_gl_lookup(cExchange_Rate_Type => vExchange_Rate_Type,
                                                              cGlobalPrecision    => vGlobalPrecision,
                                                              cCommon_Exch_Curr   => vCommon_Exch_Curr);
        End If;
		
        Commit;

        If vInternalErrorCode = 0 then
        
          --
          -- ## ***********************************************************************************************************
          -- ##
          -- ##                                Assignment 
          -- ##
          -- ## ***********************************************************************************************************
          --
          i := 0;
        
          vTiming(vTimingCnt).CF_last     := xxcp_reporting.Get_Seconds;
          vTiming(vTimingCnt).CF          := 0;
          vTiming(vTimingCnt).CF_Records  := 0;
          vTiming(vTimingCnt).CF_Start_Date := Sysdate;
          xxcp_global.SystemDate := sysdate;
          
          For CFGRec in CFG(vSource_id, vSource_Assignment_id, vRequest_id) Loop

                i := i + 1;
              
                vTiming(vTimingCnt).CF_Records := vTiming(vTimingCnt).CF_Records + 1;
              
                xxcp_global.gCommon(1).current_source_rowid      := CFGRec.Source_Rowid;
                xxcp_global.gCommon(1).current_transaction_date  := CFGRec.vt_Transaction_Date;
                xxcp_global.gCommon(1).current_transaction_table := CFGRec.vt_Transaction_Table;
                xxcp_global.gCommon(1).current_transaction_id    := CFGRec.vt_Transaction_id;
                xxcp_global.gCommon(1).current_parent_trx_id     := CFGRec.vt_Parent_Trx_id;
                xxcp_global.gCommon(1).current_Interface_id      := CFGRec.vt_interface_id;
								xxcp_global.gcommon(1).current_interface_ref     := cfgrec.vt_transaction_ref; 
                xxcp_global.gCommon(1).preview_parent_trx_id     := CFGRec.vt_parent_trx_id;
                xxcp_global.gCommon(1).preview_source_rowid      := CFGRec.vt_source_rowid;
								xxcp_global.gcommon(1).current_interface_ref     := cfgrec.vt_transaction_ref; 
                xxcp_global.gCommon(1).calc_legal_exch_rate      := cfgRec.calc_legal_exch_rate;
              
                vTransaction_Class := CFGRec.VT_Transaction_Class;
              
                If XXCP_TE_BASE.Ignore_Class(CFGRec.VT_Transaction_Table, vTransaction_Class) then
                  Begin
                    update XXCP_pv_interface
                       set vt_status              = 'IGNORED',
                           vt_internal_error_code = Null,
                           vt_date_processed      = xxcp_global.SystemDate
                     where rowid = CFGRec.Source_Rowid
                       and vt_preview_id  = xxcp_global.gCommon(1).Preview_id;

                  Exception
                    when OTHERS then Null;
                  End;
                Else

                   XXCP_TE_CONFIGURATOR.Control(
                                               cSource_Assignment_ID     => vSource_assignment_id,
                                               cSet_of_books_id          => CFGRec.Source_Set_of_books_id,
																							 cTransaction_Date         => CFGRec.VT_Transaction_Date,
																							 cSource_id                => vSource_id,
                                               cSource_Table_id          => CFGRec.Source_table_id,
                                               cSource_Type_id           => CFGRec.Source_type_id,
                                               cSource_Class_id          => CFGRec.Source_class_id,
                                               cTransaction_id           => CFGRec.VT_Transaction_ID,
                                               cSourceRowid              => CFGRec.Source_Rowid,
                                               cParent_Trx_id            => CFGRec.VT_Parent_Trx_id,
                                               cTransaction_Set_id       => CFGRec.Transaction_Set_id,
                                               cSpecial_Array            => 'N', 
                                               cKey                      => Null, 
																							 cTransaction_Table        => CFGRec.VT_Transaction_Table,
																							 cTransaction_Type         => CFGRec.VT_Transaction_Type,
                                               cTransaction_Class        => vTransaction_Class,
                                               cInternalErrorCode        => vInternalErrorCode);

                  If xxcp_global.Get_Preview_Waiting(cPreview_id => xxcp_global.gCommon(1).Preview_id) = 'Y' then
                     -- Release date override
                     xxcp_global.Set_Release_Date(Null);
                  End If;

                  If xxcp_global.Get_Preview_Waiting(cPreview_id => xxcp_global.gCommon(1).Preview_id) = 'Y' then
                     -- Release date override
                     xxcp_global.Set_Release_Date(Null);
                  End If;

                  -- Check Status
                  If xxcp_global.Get_Release_Date IS NOT NULL THEN
                      Stop_Whole_Transaction;
                  ElsIf nvl(vInternalErrorCode, 0) = 0 then
                    Update XXCP_PV_INTERFACE
                       set vt_status              = 'TRANSACTION',
                           vt_internal_error_code = Null,
                           vt_date_processed      = xxcp_global.SystemDate
                     where rowid = CFGRec.Source_Rowid
                     and vt_preview_id  = xxcp_global.gCommon(1).Preview_id;
              
                  ELSIf vInternalErrorCode = -1 then
                    Update XXCP_PV_INTERFACE
                       set vt_status              = 'SUCCESSFUL',
                           vt_internal_error_code = Null,
                           vt_date_processed      = xxcp_global.SystemDate,
                           vt_status_code         = 7001
                     where rowid = CFGRec.Source_Rowid
                     and vt_preview_id  = xxcp_global.gCommon(1).Preview_id;
                  ElsIf  vInternalErrorCode  BETWEEN 7021 and 7028 then
									  Pass_Through_Whole_Trx(CFGRec.Source_Rowid, vInternalErrorCode);
                  ElsIf nvl(vInternalErrorCode, 0) <> 0 then
                    Update XXCP_PV_INTERFACE
                       set vt_status              = 'ERROR',
                           vt_internal_error_code = 12000,
                           vt_date_processed      = xxcp_global.SystemDate
                     where rowid = CFGRec.Source_Rowid
                     and vt_preview_id  = xxcp_global.gCommon(1).Preview_id;

                  End If;
                
                End If; -- End configuration
              
                -- ##
                -- ## Commit Configured rows
                -- ##
              
                If i >= 500 then
                  xxcp_global.SystemDate    := Sysdate;
                  Commit;
                  i           := 0;
                  -- !! Dont test for termination
                  Exit when vJob_Status > 0; -- Controlled Exit
                End If;
    			
    			    END Loop;
 
          xxcp_dynamic_sql.Close_Session;
          -- #
          -- # Clear Common vars
          -- #
          xxcp_global.gCommon(1).current_transaction_table := Null;
          xxcp_global.gCommon(1).current_transaction_id    := Null;
          xxcp_global.gCommon(1).current_parent_trx_id     := Null;
					xxcp_global.gcommon(1).current_interface_ref     := Null;
					xxcp_global.gcommon(1).current_interface_ref     := Null; 
       
          Commit; -- Final Configuration Commit.
        
          --
          If xxcp_global.gCommon(1).Custom_events = 'Y' then
            xxcp_custom_events.AFTER_ASSIGNMENT_PROCESSING(vSource_id,vSource_assignment_id);
          End If;
        
          vTiming(vTimingCnt).CF := xxcp_reporting.Get_Seconds_Diff(vTiming(vTimingCnt).CF_last,xxcp_reporting.Get_Seconds);
          vTiming(vTimingCnt).CF_End_Date := Sysdate;
        
          -- ***********************************************************************************************************
          --
          --  Set associated Transactions to error if anyone of the Transactions is a set has errored in configuration
          --
          -- ***********************************************************************************************************
          --
          xxcp_foundation.show('Reporting Configuration errors....');
          xxcp_global.SystemDate := Sysdate;
          vInternalErrorCode := 0;
          For ConfErrRec in ConfErr(vSource_Assignment_id, vRequest_id) loop
          
            Begin
              update XXCP_pv_interface
                 set vt_status              = 'ERROR',
                     vt_internal_Error_code = 12824, -- Associated errors
                     vt_date_processed      = xxcp_global.SystemDate
               where vt_request_id           = vRequest_id
                 and vt_Source_Assignment_id = vSource_Assignment_id
                 and vt_status               IN ('ASSIGNMENT', 'TRANSACTION')
                 and vt_parent_trx_id        = ConfErrRec.vt_parent_trx_id
                 and vt_transaction_table    = ConfErrRec.vt_Transaction_table;
            
            Exception
              when OTHERS then
                vInternalErrorCode := 12819; -- Update Failed.
            
            End;
          End Loop;
        
          --## ***********************************************************************************************************
          --##
          --##                                    Transaction Engine
          --##
          --## ***********************************************************************************************************
        
          vTiming(vTimingCnt).TE_Last    := xxcp_reporting.Get_Seconds;
          vTiming(vTimingCnt).TE_Records := 0;
          vTiming(vTimingCnt).TE_Start_Date := Sysdate;
        
          vExtraLoop    := False;
          vExtraClass   := Null;
					vISExtraClass := Null;
					vExtraCount   := 0;
          i             := 0;
          xxcp_global.SystemDate := Sysdate;

          If vInternalErrorCode = 0 and vJob_Status = 0 then

            xxcp_foundation.show('Transactions....');
            xxcp_wks.Reset_Source_Segments;
            gBalancing_Array      := xxcp_wks.Clear_Segments;
            ClearWorkingStorage;
            
            For  GLIRec in  GLI(vSOURCE_ID, vSOURCE_ASSIGNMENT_ID, vRequest_id) Loop
            
                  vInternalErrorCode := 0;
              
                  vTiming(vTimingCnt).TE_Records := vTiming(vTimingCnt).TE_Records + 1;
                  --
                  -- ***********************************************************************************************************
                  --        Set the balancing Setments to be written to the Column Rules Array
                  -- ***********************************************************************************************************
                  --
                  -- Poplulate the Balancing Array
                  xxcp_wks.Source_Balancing(1) := Null;

                  xxcp_global.gcommon(1).preview_parent_trx_id   := glirec.vt_parent_trx_id;
                  xxcp_global.gcommon(1).preview_source_rowid    := glirec.vt_source_rowid;
								  xxcp_global.gcommon(1).current_interface_ref   := glirec.vt_transaction_ref; 
                  xxcp_global.New_Transaction := 'N';
                  --
                  -- ***********************************************************************************************************
                  --         When a New Transaction is detected then flush the current buffer
                  -- ***********************************************************************************************************
                  --
                  If (vCurrent_Parent_Trx_id <> GLIRec.vt_parent_Trx_id) then
                  
                    vTransaction_Error := False;

                    --
                    -- !! ******************************************************************************************************
                    --                             FLUSH TRANSACTION
                    -- !! ******************************************************************************************************
                    --
                    If vCurrent_Parent_Trx_id <> 0 then
                      CommitCnt := CommitCnt + 1; -- Number of records process since last commit.

                      k := FlushRecordsToTarget(vGlobal_Rounding_Tolerance,
                                                vGlobalPrecision,
                                                vTransaction_Type,
                                                vInternalErrorCode);
                    End If;

                    --
                    -- ***********************************************************************************************************
                    --            Store Variables that are only set once per Parent Trx Id
                    -- ***********************************************************************************************************
                    --
                    xxcp_wks.Trace_Log     := Null;
                    xxcp_global.gCommon(1).current_transaction_table := GLIRec.vt_Transaction_Table;
                    xxcp_global.gCommon(1).current_parent_trx_id     := GLIRec.vt_Parent_Trx_id;
								    xxcp_global.gcommon(1).current_interface_ref     := GLIRec.vt_transaction_ref;
								    xxcp_global.gCommon(1).current_transaction_id    := GLIRec.vt_Transaction_id;
                    xxcp_global.gCommon(1).current_source_rowid      := GLIRec.Source_Rowid;

                    xxcp_global.New_Transaction := 'Y';
                    vCurrent_Parent_Trx_id := GLIRec.vt_Parent_Trx_id;
                    vTransaction_Type      := GLIRec.vt_Transaction_Type;
                    --
                    -- ***********************************************************************************************************
                    --                         Extra Loops for different Classes
                    -- ***********************************************************************************************************
                    --
                    vExtraLoop    := False;
                    vExtraClass   := Null;
										vISExtraClass := Null;
										vExtraCount   := 0;

                    --
                    -- !! ******************************************************************************************************
                    --                             TRANSACTION_ENGINE_HEADER
                    -- !! ******************************************************************************************************
                    --
										If xxcp_global.gCommon(1).Custom_events = 'Y' then
/*										  If xxcp_te_base.gSabrix_Engine = 'Y' then
										    xxcp_wks.gTaxCodes.delete;
											End If;*/
										  vInternalErrorCode :=  nvl(xxcp_custom_events.TRANSACTION_ENGINE_HEADER(vSource_id, vSource_assignment_id, GLIRec.vt_Transaction_table,GLIRec.vt_Transaction_Type, GLIRec.Source_Rowid, Null,  xxcp_te_base.gMasterTradingSetId),0);
										  If vInternalErrorCode != 0 then
											  vTransaction_Error := True;
                        Error_Whole_Transaction(vInternalErrorCode);
                      End If;
                    End If; 

                    -- Check to See if an HDR Class is required
                    For j in 1 .. xxcp_global.gSRE.Count loop
                      If GLIRec.vt_Transaction_Table = xxcp_global.gSRE(j).Transaction_table then
                        vISExtraClass := xxcp_global.gSRE(j).Extra_Record_type;
                        Exit;
                      End If;
                    End Loop;

                    -- Phase 1 Check
										If nvl(GLIRec.Parent_Entered_Amount,0) = 0 then
										   -- Check to see if any of the records are ! zero
												vExtraClass := vISExtraClass;
											  vExtraLoop  := TRUE;
												vExtraCount := 1; -- Dont do it again for this Trx
										End If;

                  Else
                    vExtraLoop  := FALSE; -- !!!!
                    vExtraClass := Null;
                  End If;

									-- New Extra HDR Control
                  -- Phase 2 Check
                  If vExtraCount = 0 then
										If nvl(GLIRec.Parent_Entered_Amount,0) != 0 then
										  If vISExtraClass is not null then
												vExtraClass := vISExtraClass;
											  vExtraLoop  := TRUE;
												vExtraCount := 1; -- Dont do it again for this Trx
                      End If;
										End If;
                  End If;

                  --
                  -- ***********************************************************************************************************
                  --                                   Store Working Variables
                  -- ***********************************************************************************************************
                  --
                  xxcp_global.gCommon(1).current_Interface_id    := GLIRec.vt_interface_id;
                  xxcp_global.gCommon(1).current_Accounting_date := GLIRec.Accounting_Date;
                  --
                  -- ***********************************************************************************************************
                  --                                   Call the Transaction Engine
                  -- ***********************************************************************************************************
                  --
                  If vTransaction_Error = False then
                    vInternalErrorCode := 0; -- Reset for each row

                    i := i + 1; -- Count the number of rows processed.

                    xxcp_wks.Source_CNT  := xxcp_wks.SOURCE_CNT + 1;
                    xxcp_wks.Source_rowid(xxcp_wks.source_cnt)   := GLIRec.Source_Rowid;

                    xxcp_wks.SOURCE_SUB_CODE(xxcp_wks.source_cnt):= 0;
                    xxcp_wks.SOURCE_STATUS(xxcp_wks.source_cnt)  := '?';

                    -- *********************************************************************
                    -- Class Mapping
                    -- *********************************************************************
										If GLIRec.class_mapping_req = 'Y' then
  									    vClass_Mapping_Name := xxcp_te_base.class_mapping(
												                                   cSource_id              => vSource_id,
										                                       cClass_Mapping_Required => GLIRec.class_mapping_req,
																													 cTransaction_Table      => GLIRec.vt_transaction_table,
																													 cTransaction_Type       => GLIRec.vt_transaction_type,
																													 cTransaction_Class      => GLIRec.vt_transaction_class,
																													 cTransaction_id         => GLIRec.vt_transaction_id,
																													 cCode_Combination_id    => GLIRec.code_combination_id,
																													 cAmount                 => GLIRec.Transaction_Cost);
                    End If;
                    -- *********************************************************************
                    -- End Class Mapping
                    -- *********************************************************************

       	            xxcp_global.gCommon(1).current_Source_rowid := GLIRec.Source_Rowid;

                    XXCP_TE_ENGINE.Control(cSource_assignment_id    => vSource_Assignment_id,
                                           cSource_Table_id         => GLIRec.Source_Table_id,
																					 cSource_Type_id          => GLIRec.Source_Type_id,
                                           cSource_Class_id         => GLIRec.Source_Class_id,
                                           cTransaction_id          => GLIRec.vt_Transaction_id,
                                           cParent_Trx_id           => GLIRec.vt_Parent_Trx_id,
                                           cSourceRowid             => GLIRec.Source_Rowid,
                                           cParent_Entered_Amount   => GLIRec.Parent_Entered_Amount,
                                           cParent_Entered_Currency => GLIRec.Parent_Entered_Currency,
                                           cClass_Mapping           => GLIRec.class_mapping_req,
                                           cClass_Mapping_Name      => vClass_Mapping_Name,
                                           --
                                           cInterface_ID            => GLIRec.VT_Interface_id,
                                           cExtraLoop               => vExtraLoop,
                                           cExtraClass              => vExtraClass,
                                           cTransaction_set_id      => GLIRec.Transaction_Set_id,
																					 --
																					 cTransaction_Table       => GLIRec.VT_Transaction_Table,
																					 cTransaction_Type        => GLIRec.VT_Transaction_Type,
                                           cTransaction_Class       => GLIRec.vt_Transaction_Class,
                                           cInternalErrorCode       => vInternalErrorCode);


                    xxcp_global.gCommon(1).current_trading_set := Null;

                    -- Report Errors
                    If vInternalErrorCode <> 0 then
                      Error_Whole_Transaction(vInternalErrorCode);
                      vTransaction_Error := True;
                    End If;

                  End If;
                  --
                  -- ***********************************************************************************************************
                  --                     Test to See if Oracle or the User has terminated this process
                  --                                   And Commit after Each 1000 Records processed
                  -- ***********************************************************************************************************
                  --
                  IF CommitCnt >= 100 THEN
                    CommitCnt := 0;
                    xxcp_global.SystemDate    := sysdate;
                    Commit;
                    -- !! Dont test for termination in Preview Mode
                    Exit when vJob_Status > 0; -- Controlled Exit
                  END IF;
                
                 END LOOP;
              
            xxcp_te_base.Close_Cursors;

            --
            --  !!***********************************************************************************************************
            --                     Flush the Buffer for a Final Time
            -- !! ***********************************************************************************************************
            --
            IF vCurrent_Parent_Trx_id <> 0 and vInternalErrorCode = 0 then
              If vInternalErrorCode = 0 then
                k := FlushRecordsToTarget(vGlobal_Rounding_Tolerance,
                                          vGlobalPrecision,
                                          vTransaction_type,
                                          vInternalErrorCode);
              End If;
            End If;
          End If; -- Internal Error Check GLI
        End If; -- End Internal Error Check
      
        vTiming(vTimingCnt).TE := xxcp_reporting.Get_Seconds_Diff(vTiming(vTimingCnt).TE_last,xxcp_reporting.Get_Seconds);

        xxcp_foundation.show('Clear down...');
        xxcp_global.SystemDate := Sysdate;
        -- *******************************************************************
        -- Error out records remaining with a Processing status after the run
        -- *******************************************************************
        Begin
             update XXCP_pv_interface
                 set vt_status              = 'ERROR'
                    ,vt_internal_error_code = 12999
                    ,vt_date_processed      = sysdate
               where rowid                  = any
			              (select g.Rowid
                    from XXCP_pv_interface g
                   where g.vt_status               in ('ASSIGNMENT', 'TRANSACTION')
                     and g.vt_preview_id           = xxcp_global.gCommon(1).preview_id
                     and g.vt_source_assignment_id = vSource_assignment_id);

             Exception when OTHERS then Null;
        End;

        -- #
        -- # Clear Common vars
        -- #
        xxcp_global.gCommon(1).current_transaction_table := Null;
        xxcp_global.gCommon(1).current_transaction_id    := Null;
			  xxcp_global.gcommon(1).current_interface_ref     := Null;
        xxcp_global.gCommon(1).current_parent_trx_id     := Null;
        --
        -- ***********************************************************************************************************
        --                     Clear Down
        -- ***********************************************************************************************************
        --
        ClearWorkingStorage;
        --
        -- ***********************************************************************************************************
        --                     DeActive the Lock in XXCP_ACTIVITY_CONTROL
        -- ***********************************************************************************************************
        --
        Commit;
				
        vTiming(vTimingCnt).TE_End_Date := Sysdate;
				vTimingCnt := vTimingCnt + 1;

      End loop; -- SR1
    

      xxcp_te_base.ClearDown;
      xxcp_memory_pack.ClearDown;
      xxcp_dynamic_sql.Close_Session;
    
      If xxcp_global.gCommon(1).Custom_events = 'Y' then
        xxcp_custom_events.After_processing(cSource_id => vSource_id, cSource_Group_id => cSource_Group_id);
      End If;
      Commit;

    Else
      --
      -- Report If this process was already in use
      --
      xxcp_foundation.show('Engine already in use');
      vJob_Status := 1;
    End If;
  
    xxcp_te_base.Write_Timings(vTiming, vTiming_Start);
    --
    -- ************************************************************************************************
    --                     Show Time Statistics when called from SQL*Plus
    -- ************************************************************************************************
    --
    xxcp_global.gCommon(1).Current_Request_id := Null;
    --
    -- ***********************************************************************************************************
    --                     Return Job Status Code and Finish!!
    -- ***********************************************************************************************************
    --
    Return(vJob_Status);
  End Control;


Begin
  -- Get System Date once
  xxcp_global.SystemDate := Sysdate;

END XXCP_PV_AR_ENGINE;
/
