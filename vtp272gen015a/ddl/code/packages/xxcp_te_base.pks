 CREATE OR REPLACE PACKAGE XXCP_TE_BASE AS
/******************************************************************************
                    V I R T U A L  T R A D E R  L I M I T E D
          (Copyright 2003-2012 Virtual Trader Ltd. All rights reserved)

   NAME:       XXCP_TE_BASE 
   PURPOSE:    Engine Base Calls

   Version [03.06.17] Build Date [21-SEP-2012] Name [XXCP_TE_BASE]
   
******************************************************************************/

   gSabrix_Engine      Varchar2(1) := 'N';     
   gMasterTradingSetId pls_integer := 0;
   
   --- Tax Lookup
   Type gTax_Record_Type IS RECORD(
       Tax_Engine_id             pls_integer,
       Element_Count             Number(3),
       Taxable_Amount            Number,
       Taxable_Currency          varchar2(15),
       Tax_Gross_Amount          Number,
       Tax_Rate                  Number,
       Tax_Amount                Number,
       Recovery_Amount           Number,
       Tax_Minus_Recovery_Amount Number
      );
      
   TYPE gTax_Record IS TABLE OF gTax_Record_Type INDEX BY BINARY_INTEGER;

   gTax_Record_Set gTax_Record;   

   --- Tax Element
   Type gTax_Element_Type IS RECORD(
       Tax_Element_Number    number(3),
       Tax_Rate_Code         varchar2(100),
       Tax_Rate              number,
       Tax_Lookup_id         number(15),
       Tax_Regime_Name       varchar2(100),
       Tax_Amount            number,
       Tax_Rate_id           number,
       Tax_Jurisdiction      varchar2(100),
       Imposition            varchar2(100),
       Recovery_Rate         number,  -- % to Recover
       Recovery_Amount       number,  -- % of Tax Amount
       Recovery_Rate_Code    varchar2(100),
       Tax_Minus_Recovery_Amount Number
      );
      
   TYPE gTax_Element IS TABLE OF gTax_Element_Type INDEX BY BINARY_INTEGER;

   gTax_Element_Set gTax_Element;   
   
   
   Function Software_Version RETURN VARCHAR2;

   Function Ignore_Class(cTransaction_Table in varchar2, cTransaction_Class in varchar2) return boolean;
   
   Function GetPassThroughActive return varchar2;
   
   Function GetPassThroughIgnore return varchar2;

   Procedure SetPassThroughIgnore(cPassThroughIgnore in varchar2);
 
   Function GetPassThroughPrvCode return number;
 
   Procedure SetPassThroughPrvCode(cPassThroughPrvCode in number);
  
   Function TransactionPathTitles(cType in varchar2, cLines in number) return long;

   Procedure DumpTransactionPath(cFE_cnt in integer, cInternalErrorCode in number);

   Procedure Account_Rounding(
                           cGlobal_Rounding_Tolerance in number
                          ,cGlobalPrecision           in number
                          ,cTransaction_Type          in varchar2
                          ,cEntered_DR               out number
                          ,cEntered_CR               out Number
                          ,cInternalErrorCode     in out number);

   Function Zero_Suppression_Rule(j in number) return number;

   Procedure Get_Engine_Dates(cStart_Partition_Date       out date, 
                              cEnd_Partition_Date         out date,
                              cRemove_Filtered_Date       out date,
                              cIgnored_Start_Removal_Date out date,
                              cIgnored_End_Removal_Date   out date,
                              cRequest_Id_Range           out number
                             );
                             
   PROCEDURE Engine_Init ( cSource_id               in number
                          ,cSource_Group_id         in number
                          ,cInternalErrorCode   in out NOCOPY number);
   Procedure ClearDown;

   Procedure Init_Cursors(cTarget_Table in varchar2);

   Procedure Reset_Package;

   Procedure Close_Cursors;
   
   Procedure Set_Master_Trading_Set(cSource_id in number); 
   
   Function Get_Global_Tolerance(cSource_id in number) return number;

   Procedure Custom_Event_Insert_Row (cPos in number, cInternalErrorCode in out number);
   
   Function ElapsedTimeFunction(cStart_Date in date, cEnd_Date in date) return varchar2;


   Procedure Write_Timings(cTiming in out xxcp_global.gTiming_rec, cTiming_Start in number);

   Procedure Curr_Conv_Set_init(cTarget_Currency in varchar2, cExchange_Date in date, cExchange_Type in varchar2);
     
   Procedure Curr_Conv_Set(cPricing_Method_id    in Number, 
                           cOwner_Tax_Reg_id     in Number,
                           cPartner_Tax_Reg_id   in Number,
                           cTransaction_Currency in Varchar2,
                           cPricing_Method_Type  in varchar2,
                           cAction               in varchar2
                          );

   Function Utils_Curr_Conv(cAmount              in number
                          , cCurrency_Code       in varchar2
                          , cPrecision_Indicator in varchar2
                          , cExchange_Date       in Date     default null
                          , cExchange_Type       in varchar2 default null
                          , cTarget_Currency     in varchar2 default null
                          , cLabel               in varchar2 default null
   ) return number;

  Function Utils_Curr_ConvRate(cCurrency_Code       in varchar2
                             , cPrecision_Indicator in varchar2
                             , cExchange_Date       in Date     default null
                             , cExchange_Type       in varchar2 default null
                             , cTarget_Currency     in varchar2 default null
   ) return number;
                          
                          
   Function Class_Mapping(
                        cSource_id              in number,
                        cClass_Mapping_Required in varchar2,
                        cTransaction_Table      in varchar2,
                        cTransaction_Type       in varchar2,
                        cTransaction_Class      in varchar2,
                        cTransaction_id         in Number,
                        cCode_Combination_id    in Number default 0,
                        cAmount                 in Number default 0,
                        cSystem_Link_id         in number default 0 
                        ) return varchar2;
 
   

   Procedure Preview_Mode_Restrictions(cCalling_Wrapper      in varchar2, 
                                       cSource_assignment_id in number, 
                                       cSource_Rowid         in Rowid );

   Procedure Add_Alert_to_Queue(cSource_id            in number,
                                cSource_Group_id      in number,
                                cSource_Assignment_id in number,
                                cRequest_id           in number,
                                cAlert_Type           in number default 0
                                );
                               
  Procedure Remove_Alert_from_Queue;

  Function Get_Cost_Category (cCompany              in varchar2,
                              cDepartment           in varchar2,
                              cAccount              in varchar2,
                              cPeriod_End_Date      in date,
                              cCost_Category_Id     out number,
                              cCategory_Data_Source out varchar2,
                              cCost_Category        out varchar2,
                              cCost_Plus_Set_Id     in number) return number;

  Function Get_Payer_Details (cPayee            in  varchar2,
                              cTerritory        in  varchar2,
                              cCost_Category_id in  number,
                              cData_Source      in  varchar2,
                              cTransaction_Date in  date,
                              cPayer1           out varchar2,
                              cPayer1_Percent   out number,
                              cPayer2           out varchar2,
                              cPayer2_Percent   out number,
                              cInter_Payer      out varchar2,
                              cAccount_Type     out varchar2) return number;
  
  Function Get_Trading_Relationship(cAction_Type               IN Varchar2,
                                    cSource_id                 IN Number, 
                                    cOwner_Tax_Reg_id          IN Number, 
                                    cPartner_Tax_Reg_Id        IN Number, 
                                    cTransaction_Date          IN Date,
                                    cCreate_IC_Trx             IN Varchar2,
                                    cDflt_Trading_Relationship IN Varchar2,
                                    cEntity_Partnership_id     out Number) Return Number;
          
  -- 03.06.15                          
  function Get_Classification_Attribute return varchar2;                                    
  -- 03.06.16
  function Get_Unmatched_Attribute return varchar2;                                 
  function Get_Quantity_Attribute return varchar2;  
  -- 03.06.18
  function Get_Approve_Restricted_Att return varchar2;   

  function Get_CPA_Attributes(cProfile_Name IN VARCHAR2) return varchar2;                                    
           
  Procedure Calc_Tax_Rates(  
                        cSourceRowid            IN ROWID,
                        cTransaction_id         IN NUMBER,
                        cTransaction_Date       IN DATE,
                        cOwnerTaxid             IN NUMBER,
                        cPartnerTaxId           IN NUMBER,
                        cIC_Control             IN VARCHAR2, 
                        cTransaction_Set_id     IN NUMBER,
                        cExchange_Date          IN DATE,
                        cTaxable_Amount         IN NUMBER,
                        cTaxable_Currency       IN VARCHAR2,
                        cInternalErrorCode  IN OUT NUMBER);  
         
                             
End XXCP_TE_BASE;
/
