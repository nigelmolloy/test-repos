Create Or Replace Package XXCP_DATA_COLLECTOR As
   /******************************************************************************
                           V I R T A L  T R A D E R  L I M I T E D 
               (Copyright 2012 Virtual Trader Ltd. All rights Reserved) 
   
      NAME:       XXCP_DATA_COLLECTOR
      PURPOSE:    These functions are used by the Data Collection Engine
   
      Version [03.03.02] Build Date [30/06/2012] NAME [XXCP_DATA_COLLECTOR]
   
   ******************************************************************************/

   Function Software_Version Return Varchar2;

   Function Get_Value( cPosition In Number) Return Varchar2;

   Function Control( cSource_id         In Number,
                     cCol_ctl_id        In Number,
                     cOracle_Request_id In Number,
                     cRecsProcessed     Out Number,
                     cRecsInserted      Out Number,
                     cException         Out Varchar) Return Number;

End XXCP_DATA_COLLECTOR;
/
