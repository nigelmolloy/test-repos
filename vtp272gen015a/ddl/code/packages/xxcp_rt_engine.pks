CREATE OR REPLACE PACKAGE XXCP_RT_ENGINE AS
/******************************************************************************
                    V I R T A L  T R A D E R  L I M I T E D 
		  (Copyright 2006-2010 Virtual Trader Ltd. All rights Reserved)

   NAME:       XXCP_RT_ENGINE
   PURPOSE:    RT Engine 
   
   Version [03.06.07] Build Date [02-JAN-2013] Name [XXCP_RT_ENGINE]

******************************************************************************/

  Function Software_Version RETURN VARCHAR2;
   
	Function NextSequenceId Return Number;

  Procedure Open_Session(cSource_Assignment_Name in varchar2,
                         cSource_Assignment_Id  out number,
                         cSource_id              in number default 17);

	Procedure Close_Session;
  
  Function Get_Source_Table_id(cTransaction_Table in varchar2, cSource_id in number default 17) return number;
  
  Function Get_Trading_Set_id(cTrading_Set in varchar2, cSource_id in number default 17) return number;
  
  Function Set_Frozen_Flag(cSource_assignment_id in number,
                           cSource_table_id      in number,
                           cTrading_Set_id       in number,
                           cParent_trx_id        in number,
                           cTransaction_Date     in date) return boolean;

  Function Remove_Transaction_Set(
	                 cSource_Assignment_Id   IN NUMBER,
                   cUnique_Request_Id      IN NUMBER
									 ) return number;


	Function Remove_Transactions_In_Error(cSource_Assignment_id   IN NUMBER,
                                        cUnique_Request_id      IN NUMBER
									                    ) return number;


	Procedure Show(cMessage in Varchar2);

  Procedure Report_Error_Details(cInternalErrorCode in number, cError_Message in varchar2, cLong_Message in long);

	Procedure SummarizeInterface(cSource_Assignment_id in number, cRequest_id in number);

  Function OLS_Trx_By_Source_Group(cSource_Group_id    in number,
                                   cTransaction_Table  in varchar2,
                                   cParent_Trx_id      in number
                                  ) Return number;

  Function Control(cSource_Assignment_Id   IN NUMBER,
                   cUnique_Request_Id      IN NUMBER,
									 cTable_Group_id         IN NUMBER   Default 0,
									 cRemove_Prior_Records   IN VARCHAR2 Default 'N',
                   cInvestigate_Errors     IN VARCHAR2 Default 'Y',
                   cIC_Price_Mandatory     IN VARCHAR2 Default 'N'
                  ) return number;




END XXCP_RT_ENGINE;
/
