CREATE OR REPLACE PACKAGE BODY XXCP_PV_GL_ENGINE AS
/******************************************************************************
                        V I R T A L  T R A D E R  L I M I T E D 
  		      (Copyright 2005-2012 Virtual Trader Ltd. All rights Reserved)

   NAME:       XXCP_PV_GL_ENGINE
   PURPOSE:    GL Engine in Preview Mode

   REVISIONS:
   Ver        Date      Author  Description
   ---------  --------  ------- ------------------------------------
   02.01.00   28/06/02  Keith   First Coding
   02.01.01   07/01/04  Keith   Restructure for VT 2.1
   02.01.02   20/05/04  Keith   ransaction Stamp by vt_transaction_group
   02.01.03   28/05/04  Keith   Fixed Rounding Amount Issue
   02.01.05   29/09/04  Keith   Added debug on Entered DR, CR Not matching
   02.01.07   20/10/04  Keith   Added debug on Entered DR, CR Not matching
   02.02.00   07/02/05  Keith   Adding Inventory Logic
   02.02.03   12/05/05  Keith   Moved Rounding out to TE_BASE
	 02.03.00   27/01/06  Keith   Added BEFORE_ASSIGNMENT 
	 02.03.01   08/02/06  Keith   Moved .init upwards
	 02.03.02   17/04/07  Keith   Added Decode for class_transaction_id
   02.03.04   03/09/09  Keith   Removed the for update command
   02.06.01   29/09/09  Simon   Improved Pass Through Logic.
   02.06.02   14/10/09  Simon   Grouping now done by transaction_table not assignment_id..
   02.06.03   28/09/09  Keith   Added calc_legal_exch_rate  
   02.06.04   16/05/11  Simon   Added Reset_HAC_Tables and Reset_Parent_Trx
   02.06.05SW 14/06/11  Keith   Added Waiting to Reset_HAC_Tables 
   03.06.06   27/03/12  Keith   Added Exclude Rule and fixed latch issue
   03.06.07   27/06/12  Nigel   Added ICS integrity check
   03.06.08   03/07/12  Nigel   Split out ICS DML packages to one per perview_ctl
   03.06.09   10/07/12  Nigel   Changed the integrity check, and removed check for classification
   03.06.10   09/08/12  Keith   Dynamic Explosion
******************************************************************************/

  vTiming          xxcp_global.gTiming_rec;
	vTimingCnt       pls_integer;
	
	gSource_Rowid    Rowid;
	
  -- Current Records
  gBalancing_Array XXCP_DYNAMIC_ARRAY := XXCP_DYNAMIC_ARRAY(' ',' ',' ',' ',' ',' ',' ',' ',' ',' ');


  TYPE gCursors_Rec is RECORD(
    v_cursorId     pls_Integer := 0,
    v_Target_Table XXCP_column_rules.transaction_table%type,
    v_usage        varchar2(1),
    v_OpenCur      varchar2(1) := 'N',
    v_LastCur      pls_integer := -1);

  TYPE gCUR_REC is TABLE of gCursors_Rec INDEX by BINARY_INTEGER;
  gCursors gCUR_REC;


  -- *************************************************************
  --                     Software Version Control
  --                   -----------------------------
  -- This package can be checked with SQL to ensure it's installed
  -- select packagename.software_version from dual;
  -- *************************************************************
  Function Software_Version RETURN VARCHAR2 IS
   BEGIN
     RETURN('Version [03.06.10] Build Date [09-AUG-2012] Name [XXCP_PV_GL_ENGINE]');
   END Software_Version;
	 
	 
  -- !! ***********************************************************************************************************
  -- !!
  -- !!                                             Fetch_Source_Rowid 
  -- !!
  -- !! ***********************************************************************************************************
	Function Fetch_Source_Rowid(cRowid in rowid) Return Rowid is

   Cursor SX (pSource_Rowid in rowid) is
      Select vt_source_rowid
        from XXCP_PV_INTERFACE l
       where rowid            = pSource_Rowid
         and l.vt_preview_id  = xxcp_global.gCommon(1).Preview_id;
				 
  vReturn_rowid rowid;
				 
  Begin
	  For SXRec in SX(cRowid) Loop
      vReturn_rowid := SXRec.vt_source_rowid;
    End Loop;
		
		Return(vReturn_rowid);
                 
  End Fetch_Source_Rowid; 	 

  -- !! ***********************************************************************************************************
  -- !!
  -- !!                                             Open_Cursors
  -- !!
  -- !! ***********************************************************************************************************
  Function Open_Cursor(cUsage in varchar2, cTarget_Table in varchar2) Return pls_integer is

    i    pls_integer;
    vPos pls_integer := 0;

  Begin
    For i in 1 .. gCursors.count loop
      If (gCursors(i).v_Target_Table = cTarget_Table) AND (gCursors(i).v_usage = cUsage) then
        vPos := i;
        If gCursors(i).v_OpenCur = 'N' then
          gCursors(i).v_cursorId := DBMS_SQL.OPEN_CURSOR;
          gCursors(i).v_OpenCur := 'Y';
          gCursors(i).v_LastCur := -1;
          exit;
        End If;
      End If;
    End Loop;

    Return(vPos);
  End Open_Cursor;

  -- *************************************************************
  --
  --                  ClearWorkingStorage
  --
  -- *************************************************************
  Procedure ClearWorkingStorage IS
 

  begin
    -- Processed Records
    xxcp_wks.WORKING_CNT := 0;
    xxcp_wks.WORKING_RCD.Delete;

    -- Current Records before processing
    xxcp_wks.SOURCE_CNT := 0;
    XXCP_WKS.SOURCE_ROWID.Delete;
    XXCP_WKS.SOURCE_STATUS.Delete;
    XXCP_WKS.SOURCE_SUB_CODE.Delete;
    -- Balancing
    xxcp_wks.BALANCING_CNT := 0;
    XXCP_WKS.BALANCING_RCD.Delete;

  End ClearWorkingStorage;

 --  !! ***********************************************************************************************************
 --  !!   Stop_Whole_Transaction
 --  !! ***********************************************************************************************************
 Procedure Stop_Whole_Transaction is

 Begin
   -- Set all transactions in group to error status
   Begin
    Update XXCP_pv_interface
      set vt_status              = 'WAITING'
         ,vt_internal_Error_code = 0 -- Associated errors
         ,VT_DATE_PROCESSED  = xxcp_global.SystemDate
       WHERE vt_preview_id             = xxcp_global.gCommon(1).Preview_id
         AND vt_Source_Assignment_id = xxcp_global.gCommon(1).current_assignment_id
         AND vt_parent_trx_id = xxcp_global.gCommon(1).current_parent_Trx_id
         AND NOT vt_status IN ('TRASH', 'IGNORED');
   End;
   -- Update Record that actually caused the problem.
   Begin
     Update XXCP_pv_interface
        set vt_status          = 'WAITING'
           ,VT_DATE_PROCESSED  = xxcp_global.SystemDate
      where Rowid          = xxcp_global.gCommon(1).current_source_rowid
        and vt_preview_id  = xxcp_global.gCommon(1).Preview_id;
   End;

   ClearWorkingStorage;

 End Stop_Whole_Transaction;
--
--   !! ***********************************************************************************************************
--   !!
--   !!                                   Error_Whole_Gl_Transaction
--   !!                If one part of the wrapper finds a problem whilst processing a record,
--   !!                          then we need to error the whole transaction.
--   !!
--   !! ***********************************************************************************************************
--
  Procedure Error_Whole_Transaction(cInternalErrorCode  in out Number) is

 vVT_Source_Rowid Rowid;

 -- Set all transactions in group to error status
 Begin
   If nvl(xxcp_global.gCommon(1).explosion_id,0) = 0 then
       -- Standard
       Update xxcp_pv_interface
         set vt_status = 'ERROR'
            ,vt_internal_Error_code    = 12823 -- Associated errors
       where vt_Source_Assignment_id   = xxcp_global.gCommon(1).current_Assignment_id
         and vt_Parent_Trx_id          = xxcp_global.gCommon(1).current_Parent_Trx_id
         and vt_preview_id             = xxcp_global.gCommon(1).Preview_id
         and vt_Transaction_Table      = xxcp_global.gCommon(1).current_Transaction_Table;
   Else
       -- Explosion
       Update xxcp_pv_interface
         set vt_status = 'ERROR'
            ,vt_internal_Error_code    = 12823 -- Associated errors
       where vt_Source_Assignment_id   = xxcp_global.gCommon(1).current_Assignment_id
         and vt_Parent_Trx_id          = xxcp_global.gCommon(1).current_Parent_Trx_id
         and vt_preview_id             = xxcp_global.gCommon(1).Preview_id
         and vt_Transaction_Table      = xxcp_global.gCommon(1).current_Transaction_Table
         and vt_status                != 'EXPLOSION';
   End if;
   -- Update Record that actually caused the problem.
   Begin
     If nvl(xxcp_global.gCommon(1).explosion_id,0) = 0 then
       -- Standard
       Update xxcp_pv_interface
          set VT_STATUS              = 'ERROR'
             ,VT_INTERNAL_ERROR_CODE = cInternalErrorCode
             ,VT_DATE_PROCESSED      = Sysdate
        where Rowid                  = xxcp_global.gCommon(1).current_source_rowid
          and vt_preview_id          = xxcp_global.gCommon(1).Preview_id;
     else
       -- Explosion
       Update xxcp_pv_interface
          set VT_STATUS_CODE         = 7060
             ,VT_INTERNAL_ERROR_CODE = cInternalErrorCode
             ,VT_DATE_PROCESSED      = Sysdate
        where vt_transaction_id      = xxcp_global.gCommon(1).Explosion_Transaction_id
          and vt_preview_id          = xxcp_global.gCommon(1).Preview_id;
     end if;
   End;

   If xxcp_global.gCommon(1).Custom_events = 'Y' then

		  vVT_Source_Rowid := fetch_source_rowid(xxcp_global.gCommon(1).current_source_rowid);

      xxcp_custom_events.ON_TRANSACTION_ERROR(xxcp_global.gCommon(1).Source_id,
                                              xxcp_global.gCommon(1).current_assignment_id,
                                              vVT_Source_Rowid,
                                              xxcp_global.gCommon(1).current_Transaction_Table,
                                              xxcp_global.gCommon(1).current_Parent_Trx_id,
                                              xxcp_global.gCommon(1).current_transaction_id,
                                              cInternalErrorCode);
   End If;

   ClearWorkingStorage;

 End Error_Whole_Transaction;
--
--   !! ***********************************************************************************************************
--   !!
--   !!                                     No_Action_GL_Transaction
--   !!                                If there are no records to process.
--   !!
--   !! ***********************************************************************************************************
--
Procedure No_Action_Transaction(cSource_Rowid in rowid) is
Begin
  -- Update Record that actually had not action required
  Begin
    Update XXCP_PV_INTERFACE
       set VT_STATUS              = 'SUCCESSFUL'
          ,VT_INTERNAL_ERROR_CODE = Null
          ,VT_DATE_PROCESSED      = xxcp_global.SystemDate
          ,VT_STATUS_CODE         = 7003
     where Rowid                  = cSource_Rowid
       and vt_preview_id          = xxcp_global.gCommon(1).Preview_id;
  End;
End No_Action_Transaction;


--   !! ***********************************************************************************************************
--   !!
--   !!                                     Pass_Through_Whole_Trx 
--   !!                                If there are no records to process.
--   !!
--   !! ***********************************************************************************************************
Procedure Pass_Through_Whole_Trx(cSource_Rowid in Rowid, cPassThroughCode in number) is


  vStatus XXCP_pv_interface.vt_status%type;
 
 Begin
 
   If cPassThroughCode between 7021 AND 7024 then
	   vStatus := 'IGNORED';
	 Else
	   vStatus := 'PASSTHROUGH';
	 End If;
 
   If cPassThroughCode in (7022, 7026) then -- Parent level   
     xxcp_global.gPassThroughCode := cPassThroughCode;
	 End if;
      
   Update XXCP_pv_interface p
      set p.vt_status               = vStatus
         ,p.vt_internal_Error_code  = Null
         ,p.vt_status_code          = cPassThroughCode
         ,p.vt_date_processed       = xxcp_global.SystemDate
    where p.rowid = cSource_Rowid
      and p.vt_request_id           = xxcp_global.gCommon(1).Preview_id;
	 
 End Pass_Through_Whole_Trx;

/*
  !! ***********************************************************************************************************
  !!
  !!                                   Transaction_Group_Stamp
  !!                 Stamp Incomming record with parent Trx to create groups of
  ||                    transactions that resemble on document transaction.
  !!
  !! ***********************************************************************************************************
*/
Procedure Transaction_Group_Stamp( cSource_assignment_id in number, cSource_Rowid in Rowid, cUser_id in number, cInternalErrorCode out number) is



 vStatement          varchar2(2000);
 vInternalErrorCode  Number(8) := 0;


    -- Distinct list of Transaction Tables
    -- populated in Set_Running_Status
    CURSOR cTrxTbl is
      SELECT transaction_table,
             nvl(grouping_rule_id,0) grouping_rule_id
      FROM   XXCP_sys_source_tables b
      where  source_id = xxcp_global.gCommon(1).Source_id
      and    transaction_table in (select * from TABLE(CAST(xxcp_global.gTrans_Table_Array AS XXCP_DYNAMIC_ARRAY)));


 Cursor c1 (pSource_assignment_id in number,
            pTransaction_Table    IN VARCHAR2) IS
      select distinct 'Update XXCP_pv_interface j
                          set vt_parent_trx_id = (select '||t.Parent_trx_column|| ' from XXCP_gl_interface k where k.rowid = j.vt_source_rowid)
                        where vt_status = ''ASSIGNMENT''
                          and vt_preview_id  = '||to_char(xxcp_global.gCommon(1).Preview_id)||'
                          and vt_source_assignment_id = '||to_char(pSource_assignment_id)||'
                          and vt_transaction_table = '''||g.vt_transaction_table||'''' Statement_line
           ,t.parent_trx_column
           ,t.Transaction_Table
        from XXCP_pv_interface g
            ,XXCP_sys_source_tables t
  	        ,XXCP_source_assignments x
       where g.vt_transaction_table    = t.transaction_table
         and t.source_id               = x.source_id
         and g.vt_preview_id           = xxcp_global.gCommon(1).preview_id
         and x.source_assignment_id    = psource_assignment_id
         and g.vt_source_assignment_id = x.source_assignment_id
         and g.vt_transaction_table    = pTransaction_Table;     -- 02.06.02

    Cursor er1(psource_assignment_id in number,
            pTransaction_Table    IN VARCHAR2) IS
      select g.vt_interface_id, g.rowid source_rowid, g.vt_transaction_table
        from XXCP_pv_interface g
       where g.vt_status = 'ASSIGNMENT'
         and g.vt_source_assignment_id = psource_assignment_id
         and g.vt_preview_id           = xxcp_global.gCommon(1).preview_id
         and g.vt_transaction_table    = pTransaction_Table     -- 02.06.02
         and g.vt_parent_trx_id is null;


 Begin
 
   For cTblRec in cTrxTbl Loop  -- Loop for all Transaction Tables within SA.

      If cTblRec.Grouping_Rule_Id > 0 THEN  --02.06.02
        XXCP_DYNAMIC_SQL.Apply_Grouping_Rules(cInternalErrorCode => vInternalErrorCode,
                                              cTransaction_Table => cTblRec.Transaction_Table);

	      -- Error Checking for Null Parent Trx id
        For er1Rec in er1(cSource_assignment_id, cTblRec.transaction_table) LOOP
	       update XXCP_pv_interface l
		        set vt_status                = 'ERROR'
		           ,vt_parent_trx_id         = Null
		           ,l.vt_internal_error_code = 11090
		      where l.vt_transaction_table = er1rec.vt_transaction_table
            and l.vt_preview_id        = xxcp_global.gCommon(1).preview_id;
        End loop;
		  
      Else

        -- Mass update of Parent Trx id
        For rec in c1(cSource_assignment_id, cTblRec.transaction_table) LOOP

           If rec.parent_trx_column is null then
              vInternalErrorCode := 10665;
              xxcp_foundation.FndWriteError(vInternalErrorCode,'Transaction Table <'||Rec.Transaction_Table||'>');
           Else
             vStatement := rec.Statement_line;
	           Begin
     	        Execute Immediate vStatement;

    	        Exception when OTHERS then
  	             vInternalErrorCode := 10667;
                  xxcp_foundation.FndWriteError(vInternalErrorCode,SQLERRM,vStatement);
             End;
           End If;
        End loop;
      End If;

      xxcp_te_base.Preview_Mode_Restrictions(cCalling_Wrapper      => 'XXCP_PV_GL_ENGINE', 
			                                       cSource_assignment_id => cSource_assignment_id, 
																             cSource_Rowid         => cSource_Rowid  
																             ); 

    End Loop; 
      
    cInternalErrorCode := vInternalErrorCode;

  End Transaction_Group_Stamp;
 

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
 --                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
 -- !! ***********************************************************************************************************                                                                                                                                                                                                                                                                                                                                                                                                  
 -- !!                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
 -- !!                                   FlushRecordsToTarget
 -- !!     Control The process of moving the Array elements throught to creating the new output records
 -- !!
 -- !! ***********************************************************************************************************
  Function FlushRecordsToTarget(cGlobal_Rounding_Tolerance in number,
                                cGlobalPrecision           in number,
                                cTransaction_Type          in varchar2,
                                cInternalErrorCode         in out number)
    return number is

    j                  pls_integer := 0;
    fe                 pls_integer := xxcp_wks.WORKING_CNT;
    c                  pls_integer := 0;
    vCnt               pls_integer := 0;
    vEntered_DR        Number      := 0;
    vEntered_CR        Number      := 0;
    vRC                pls_integer := xxcp_wks.SOURCE_CNT;
    vRC_Records        pls_integer := 0;
    vRC_Error          pls_integer := 0;
  
    vStatus XXCP_gl_interface.vt_status%type; 
  
    vErrorMessage      varchar2(3000);
    UDI_Cnt            pls_integer := 0; 
    vVT_Source_Rowid   Rowid;
    vZero_Action       pls_integer := 0;
    
    -- 03.06.07
    vICS_Cnt       PLS_INTEGER := 0;  
	
  Begin
    If fe > 0 then
      xxcp_te_base.Account_Rounding(cGlobal_Rounding_Tolerance,
                                    cGlobalPrecision,
                                    cTransaction_Type,
                                    vEntered_DR,
                                    vEntered_CR,
                                    cInternalErrorCode);
    End If;
  
    -- Check that for each attribute record we have a records for output.
    For c in 1 .. vRC Loop
      vRC_Error := 0;
      FOR j in 1 .. fe LOOP
        If xxcp_wks.Source_rowid(c) = xxcp_wks.WORKING_RCD(j).Source_rowid then
          xxcp_wks.WORKING_RCD(j).Source_Pos := c;
          xxcp_wks.SOURCE_STATUS(c) := 'FLUSH';
          vRC_Error   := 1;
          vRC_Records := vRC_Records + 1;
        end if;
      End loop;
      IF vRC_Error = 0 THEN
          xxcp_wks.SOURCE_SUB_CODE(c) := 7003;
          xxcp_wks.SOURCE_STATUS(c) := 'NO RULES';
          No_Action_Transaction(xxcp_wks.Source_rowid(c));
      END IF;
    END LOOP;
   
    Savepoint TARGET_INSERT;
  
    If cInternalErrorCode = 0 and vRC_Records > 0 then
 
      For j in 1 .. fe Loop
      
        Exit when cInternalErrorCode <> 0;
      
        vCnt := vCnt + 1;
				
        -- Custom Events 
        If xxcp_global.gCommon(1).Custom_events = 'Y' then
   				xxcp_te_base.Custom_Event_Insert_Row(j,cInternalErrorCode);	        
        End If;

        -- Tag Zero Account Value rows
      
        If cInternalErrorCode = 0 then
        
		      xxcp_global.gCommon(1).current_source_rowid := xxcp_wks.Source_rowid(xxcp_wks.WORKING_RCD(j).Source_Pos);
					
					vZero_Action := xxcp_te_base.Zero_Suppression_Rule(j);
        
          IF vZero_Action > 0 then
					
                 Select XXCP_PREVIEW_HIST_SEQ.nextval into xxcp_wks.WORKING_RCD(j).Process_History_id from dual;
               
                 vVT_Source_Rowid := fetch_source_rowid(xxcp_wks.WORKING_RCD(j).Source_rowid);
 
                 If cInternalErrorCode = 0 then
                    ---
                    --- XXCP_PROCESS_HISTORY
                    ---        
                    XXCP_PV_PROCESS_HIST_GL.Process_History_Insert(                                                              
                                                              cSource_Rowid            => vVT_Source_Rowid,  -- Preview Mode only,
																											        cParent_Rowid            => Null,
                                                              cTarget_Table            => xxcp_wks.WORKING_RCD(j).Target_Table,
                                                              cTarget_Instance_id      => xxcp_wks.WORKING_RCD(j).Target_Instance_id,
                                                              cProcess_History_id      => xxcp_wks.WORKING_RCD(j).Process_History_id,
                                                              cTarget_assignment_id    => xxcp_wks.WORKING_RCD(j).Target_Assignment_id,
                                                              cAttribute_id            => xxcp_wks.WORKING_RCD(j).Attribute_id,
                                                              cRecord_type             => xxcp_wks.WORKING_RCD(j).Record_type,
                                                              cRequest_id              => xxcp_global.gCommon(1).current_request_id,
                                                              cStatus                  => xxcp_wks.WORKING_RCD(j).Record_Status,
                                                              cInterface_id            => xxcp_wks.WORKING_RCD(j).Interface_id,
                                                              cModel_ctl_id            => xxcp_wks.WORKING_RCD(j).Model_ctl_id,
                                                              cRule_id                 => xxcp_wks.WORKING_RCD(j).Rule_id,
                                                              cTax_registration_id     => xxcp_wks.WORKING_RCD(j).Tax_Registration_id,
																															cEntered_rounding_amount => xxcp_wks.WORKING_RCD(j).Entered_Rnd_Amount,
																															cAccount_rounding_amount => xxcp_wks.WORKING_RCD(j).Accounted_Rnd_Amount,
                                                              cD1001_Array             => xxcp_wks.WORKING_RCD(j).D1001_Array,
                                                              cD1002_Array             => xxcp_wks.WORKING_RCD(j).D1002_Array,
                                                              cD1003_Array             => xxcp_wks.WORKING_RCD(j).D1003_Array,
                                                              cD1004_Array             => xxcp_wks.WORKING_RCD(j).D1004_Array,
																											        cD1005_Array             => xxcp_wks.WORKING_RCD(j).D1005_Array,
                                                              cD1006_Array             => xxcp_wks.WORKING_RCD(j).D1006_Array,
                                                              cI1009_Array             => xxcp_wks.WORKING_RCD(j).I1009_Array,
                                                              cErrorMessage            => vErrorMessage,
                                                              cInternalErrorCode       => cInternalErrorCode);

                     If cInternalErrorCode = 0 then
                       vTiming(vTimingCnt).Flush_Records := vTiming(vTimingCnt).Flush_Records + 1;
										 End If;
                  End If;
                  
                  -- 03.06.06
                  IF cInternalErrorCode = 0 THEN
          
                    -- Check the report settlement rule.
                    If xxcp_global.gCommon(1).ic_settlements = 'Y' and xxcp_wks.WORKING_RCD(j).I1009_Array(151) is not null then   -- If ICS Rule
                    
                      if cInternalErrorCode = 0 then                                                               
                        XXCP_PV_PROCESS_DML_ICS_GL.Process_Oracle_Insert (cSource_Rowid        => vVT_Source_Rowid,
                                                                          cParent_Rowid        => NULL,
                                             	                            cTarget_Table        => xxcp_wks.WORKING_RCD(j).Target_Table,
                                                                          cTarget_Instance_id  => xxcp_wks.WORKING_RCD(j).Target_Instance_id,
                                                                          cProcess_History_id  => xxcp_wks.WORKING_RCD(j).Process_History_id,
                                                                          cRule_id             => xxcp_wks.WORKING_RCD(j).Rule_id,
                                                                          cD1001_Array         => xxcp_wks.WORKING_RCD(j).D1001_Array,
                                                                          cD1002_Array         => xxcp_wks.WORKING_RCD(j).D1002_Array,
                                                                          cD1003_Array         => xxcp_wks.WORKING_RCD(j).D1003_Array,
                                                                          cD1004_Array         => xxcp_wks.WORKING_RCD(j).D1004_Array,
                                                                          cD1005_Array         => xxcp_wks.WORKING_RCD(j).D1005_Array,
                                                                          cD1006_Array         => xxcp_wks.WORKING_RCD(j).D1006_Array,
                                                                          cI1009_Array         => xxcp_wks.WORKING_RCD(j).I1009_Array,
                                                                          cErrorMessage        => vErrorMessage,
                                                                          cInternalErrorCode   => cInternalErrorCode);             
                      end if;
                                                                  
                      vICS_Cnt := vICS_Cnt + 1;                                                         
                    end if;          
                  end if;   

                  If cInternalErrorCode = 0 then
                     vStatus := 'SUCCESSFUL';
				          Else
                     vStatus := 'ERROR';
                     xxcp_wks.SOURCE_SUB_CODE(xxcp_wks.WORKING_RCD(j).Source_Pos):= Null;
                  End If;

				          xxcp_wks.SOURCE_STATUS(xxcp_wks.WORKING_RCD(j).Source_Pos) := vStatus;
            Else
              -- ZAV
              xxcp_wks.SOURCE_SUB_CODE(xxcp_wks.WORKING_RCD(j).Source_Pos):= 7008;
			        xxcp_wks.SOURCE_STATUS(xxcp_wks.WORKING_RCD(j).Source_Pos) := 'SUCCESSFUL';
            End If;
            
			      UDI_Cnt := UDI_Cnt + 1;
          END IF;
      END LOOP;
      
      -- 03.06.06
      -- validate the records inserted into the interface table.                                           
      IF cInternalErrorCode = 0 and xxcp_global.gCommon(1).ic_settlements = 'Y' and vICS_Cnt > 0  then -- If ICS Rule and processed one or more settlement rules
        if xxcp_global.gCommon(1).current_Transaction_table != 'PAYMENTS' then  -- don't perform the integrity check for intercompany payments
          xxcp_ic_applications.integrity_check(cParent_Trx_id       => xxcp_global.gCommon(1).current_parent_trx_id,
                                               cPreview_id          => xxcp_global.gCommon(1).Preview_id,
                                               cErrorMessage        => vErrorMessage,
                                               cInternalErrorCode   => cInternalErrorCode);                                                          
        end if;                                               
      end if;        
    
      If cInternalErrorCode = 0 and UDI_Cnt > 0 then
        FORALL j in 1 .. xxcp_wks.SOURCE_CNT
          Update XXCP_pv_interface
             Set vt_Status              = xxcp_wks.SOURCE_STATUS(j),
                 vt_internal_Error_code = Null,
                 vt_date_processed      = xxcp_global.SystemDate,
                 vt_Status_Code         = xxcp_wks.SOURCE_SUB_CODE(j)
           where rowid = xxcp_wks.Source_rowid(j);
      End If;
    
    END IF;
	
	 -- Source_Pos
  
    If cInternalErrorCode <> 0 then
      -- Rollback
      If vCnt > 0 then
        Rollback To TARGET_INSERT;
        xxcp_foundation.FndWriteError(cInternalErrorCode, vErrorMessage);
      End If;
    
      Error_Whole_Transaction(cInternalErrorCode);
    
      If cInternalErrorCode = 12800 then
        xxcp_foundation.FndWriteError(cInternalErrorCode,
          'Entered DR <' ||to_char(vEntered_DR) ||'> Entered CR <' ||to_char(vEntered_CR) || '>');
      End If;
    
    End If;
  
    -- Remove Array Elements
    ClearWorkingStorage;
  
    Return(vCnt);
  End FlushRecordsToTarget;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
  --  !! ***********************************************************************************************************
  --  !!
  --  !!                                     Reset_Parent_Trx
  --  !!                      Reset Parent_Trx for non Successful Transactions
  --  !!
  --  !! ***********************************************************************************************************
  PROCEDURE Reset_Parent_Trx( cSource_assignment_id IN NUMBER,
                             cTransaction_Table    IN VARCHAR2) IS

    
  Begin

      Update XXCP_pv_interface
      set    vt_parent_trx_id        = Null
      where  vt_source_assignment_id = cSource_assignment_id
      and    vt_status              <> 'SUCCESSFUL'
      and    vt_transaction_Table    =  cTransaction_Table
      and    vt_preview_id           = xxcp_global.gCommon(1).Preview_id;

    
  End Reset_Parent_Trx;

  --  !! ***********************************************************************************************************
  --  !!
  --  !!                                     Reset_HAC_Tables
  --  !!                      Reset Headers, Attributes, Cache for Errored Transactions
  --  !!
  --  !! ***********************************************************************************************************
  PROCEDURE Reset_HAC_Tables(cSource_assignment_id IN NUMBER,
                             cTransaction_Table    IN VARCHAR2) IS
  BEGIN

      -- Clear HAC tables
      
      -- Header
      DELETE from XXCP_pv_transaction_header
      WHERE  preview_id           = xxcp_global.gCommon(1).Preview_id
      AND    header_id in (select header_id 
                           from   XXCP_pv_transaction_attributes
                           where  source_assignment_id = cSource_assignment_id
                           and    preview_id        = xxcp_global.gCommon(1).Preview_id
                           and    interface_id in (select vt_interface_id
                                                   from   XXCP_pv_interface
                                                   where  vt_source_assignment_id = cSource_assignment_id
                                                   and   (vt_status = 'ERROR' or vt_status = 'WAITING')
                                                   and    vt_transaction_table = cTransaction_Table
                                                   and    vt_preview_id        = xxcp_global.gCommon(1).Preview_id
                                                  )
                          ); 
      
      -- Cache
      DELETE from XXCP_pv_transaction_cache
      WHERE  preview_id = xxcp_global.gCommon(1).Preview_id
      AND    attribute_id in (select attribute_id 
                              from   XXCP_pv_transaction_attributes
                              where  source_assignment_id = cSource_assignment_id
                              and    preview_id           = xxcp_global.gCommon(1).Preview_id
                              and    interface_id in (select vt_interface_id
                                                      from   XXCP_pv_interface
                                                      where  vt_source_assignment_id = cSource_assignment_id
                                                      and   (vt_status = 'ERROR' or vt_status = 'WAITING')
                                                      and    vt_transaction_table = cTransaction_Table
                                                      and    vt_preview_id        = xxcp_global.gCommon(1).Preview_id
                                                     )
                             ); 
        
      -- Attributes
      DELETE from XXCP_pv_transaction_attributes
      where  source_assignment_id = cSource_assignment_id
      and    preview_id           = xxcp_global.gCommon(1).Preview_id
      and    interface_id in (select vt_interface_id
                              from   XXCP_pv_interface
                              where  vt_source_assignment_id = cSource_assignment_id
                              and    (vt_status = 'ERROR' or vt_status = 'WAITING')
                              and    vt_transaction_table = cTransaction_Table
                              and    vt_preview_id        = xxcp_global.gCommon(1).Preview_id
                             );
    
  END Reset_HAC_Tables;

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
  --  !! ***********************************************************************************************************
  --  !!
  --  !!                                   Reset_Errored_Transactions                                                                                                                                                                                                                                                                                                                                                                                                                                                   
  --  !!                              Reset Transactions from a previous run.                                                                                                                                                                                                                                                                                                                                                                                                                                           
  --  !!                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
  --  !! ***********************************************************************************************************                                                                                                                                                                                                                                                                                                                                                                                                    
  Procedure Reset_Errored_Transactions(cSource_id            in number,
                                       cSource_assignment_id in number, 
                                       cRequest_id           in number) is

    -- Distinct list of Transaction Tables
    CURSOR c1 is
      SELECT distinct transaction_table,
                      clear_hac,
                      clear_parent_trx,
                      nvl(grouping_rule_id,0) grouping_rule_id
      FROM   XXCP_sys_source_tables
      where  source_id      = cSource_id; 
  
  begin
  
    vTiming(vTimingCnt).Reset_Start := to_char(sysdate, 'HH24:MI:SS');

    For c1_rec in c1 loop -- Loop for all distinct tables in SA

      -- Reset HAC
      If c1_rec.clear_hac = 'Y' then
        Reset_HAC_Tables(cSource_assignment_id => cSource_assignment_id,
                         cTransaction_Table    => c1_rec.Transaction_Table);
      end if;
      
      -- Reset Parent Trx
      If c1_rec.clear_parent_trx = 'Y' and c1_rec.grouping_rule_id <> 0 then
        Reset_Parent_Trx( cSource_assignment_id => cSource_assignment_id,
                         cTransaction_Table    => c1_rec.Transaction_Table);

      End if; 
    End Loop;
  
    -- Update Interface
    Begin
    
      Update XXCP_pv_interface r
         set vt_status              = 'NEW',
             vt_Internal_Error_Code = Null,
             vt_status_code         = Null,
             vt_request_id          = cRequest_id
       where r.vt_source_assignment_id = cSource_assignment_id
			   and r.vt_preview_id           = xxcp_global.gCommon(1).Preview_id
         and (vt_status in
             ('ERROR', 'PROCESSING', 'TRANSACTION', 'ASSIGNMENT','WAITING','PASSTHROUGH'));
    
    exception
      when OTHERS then
        null;
    End;
  
    -- Remove Errors 
    Begin
      Delete from XXCP_pv_errors cx
       where source_assignment_id = cSource_assignment_id
			   and preview_id        = xxcp_global.gCommon(1).Preview_id;

      Exception
        when OTHERS then
          null;
    End;
    Commit;
  End RESET_ERRORED_TRANSACTIONS;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    
 -- !! ***********************************************************************************************************                                                                                                                                                                                                                                                                                                                                                                                                    
 -- !!                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
 -- !!                                  Set_Running_Status                                                                                                                                                                                                                                                                                                                                                                                                                                                            
 -- !!                      Flag the records that are going to be processed.                                                                                                                                                                                                                                                                                                                                                                                                                                          
 -- !!                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
 -- !! ***********************************************************************************************************                                                                                                                                                                                                                                                                                                                                                                                                    
  Procedure Set_Running_Status(cSource_id            in number,
                               cSource_assignment_id in number,
                               cRequest_id           in number) is
  
    cursor rx(pSource_id in number, pSource_Assignment_id in number ) is
      select r.vt_transaction_table,
             r.vt_transaction_id,
             r.rowid source_rowid,
             r.vt_transaction_type,
             t.grouping_rule_id,
						 t.source_table_id
        from xxcp_pv_interface r, xxcp_sys_source_tables t
       where r.vt_preview_id = xxcp_global.gCommon(1).Preview_id
         and vt_source_assignment_id = pSource_assignment_id
         and vt_status = 'NEW'
         and r.vt_transaction_table = t.transaction_table
         and t.source_id = pSource_id
    order by t.transaction_table
         for update;
  
    vGrouping_rule_id XXCP_gl_interface.vt_grouping_rule_id%type;
  
    -- Find records with no vt_grouping_rule_id
    cursor er2(pSource_assignment_id in number) is
      select Distinct g.vt_transaction_table
        from XXCP_pv_interface g, XXCP_sys_source_tables t
       where g.vt_request_id = cRequest_id
         and g.vt_preview_id = xxcp_global.gCommon(1).Preview_id
         and g.vt_status = 'ASSIGNMENT'
         AND g.vt_transaction_table    = t.transaction_table
         and g.vt_source_assignment_id = pSource_assignment_id
         AND t.grouping_rule_id IS NOT NULL  -- 02.06.02
         and g.vt_grouping_rule_id is null;
  
    -- Find records with invalid Grouping rule 
    cursor er3(pSource_assignment_id in number) is
      select Distinct g.vt_transaction_table
        from XXCP_pv_interface g, XXCP_grouping_rules r, XXCP_sys_source_tables t
       where g.vt_preview_id = xxcp_global.gCommon(1).Preview_id
         and g.vt_status = 'ASSIGNMENT'
         AND g.vt_transaction_table    = t.transaction_table
         and g.vt_source_assignment_id = pSource_assignment_id
         and g.vt_grouping_rule_id = r.grouping_rule_id(+)
         AND t.grouping_rule_id IS NOT NULL  -- 02.06.02
         and r.grouping_rule_id is null;
   
    vGroupingRuleExists BOOLEAN     := FALSE;
    vPrevTransaction_Table XXCP_sys_source_tables.transaction_table%TYPE := '~NULL~';
  
  begin
  
    vTiming(vTimingCnt).Set_Running_Start := to_char(sysdate, 'HH24:MI:SS');
  
    For rec in rx(cSource_id, cSource_assignment_id ) Loop
      vGrouping_rule_id := Null;

      -- Populate Transaction_Table Array
      if vPrevTransaction_Table <> rec.vt_transaction_table then
        xxcp_global.gTrans_Table_Array.extend;
        xxcp_global.gTrans_Table_Array(xxcp_global.gTrans_Table_Array.count) := rec.vt_transaction_table;
      End if;

      vPrevTransaction_Table := rec.vt_transaction_table;

      --If xxcp_global.gCommon(1).Grouping_Rule = 'Y' then
      IF Rec.grouping_rule_id is not null then  -- 02.06.02
      
        vGroupingRuleExists := TRUE;

				xxcp_global.gCommon(1).current_transaction_table := Rec.vt_Transaction_Table;
				xxcp_global.gCommon(1).current_Source_Table_id   := Rec.Source_Table_id;

        xxcp_global.gCommon(1).current_transaction_id    := Rec.vt_Transaction_id;

        vGrouping_rule_id := xxcp_custom_events.get_group_rule_id(cSource_id,
                                                                  cSource_assignment_id,
                                                                  rec.source_rowid,
                                                                  rec.vt_transaction_table,
                                                                  rec.vt_transaction_type);

        If nvl(vGrouping_rule_id, 0) = 0 then
          vGrouping_rule_id := Rec.grouping_rule_id; -- Default
        End If;
      End If;
            
      -- Set Processing Status   
      begin
        update XXCP_pv_interface r
           set vt_status           = 'ASSIGNMENT',
               vt_date_processed   = xxcp_global.SystemDate,
               vt_status_code      = Null,
               vt_request_id       = cRequest_id,
               vt_Grouping_rule_id = vGrouping_rule_id
         where r.rowid = rec.source_rowid
				   and r.VT_PREVIEW_ID = xxcp_global.gCommon(1).Preview_id;
      
     -- Exception
     --   when OTHERS then null;
      End;
    End Loop;
  
    --
    -- Error Checking 
    --
    

    If vGroupingRuleExists then  -- 02.06.02 Don't Check If no Grouping Rule used.
      -- Find records with no vt_grouping_rule_id 
      For Rec in er2(cSource_assignment_id) loop
        update XXCP_pv_interface g
           set g.vt_status              = 'ERROR',
               g.vt_date_processed      = xxcp_global.SystemDate,
               g.vt_internal_error_code = 11088
         where g.vt_preview_id           = xxcp_global.gCommon(1).Preview_id
           and g.vt_status     = 'ASSIGNMENT'
           and g.vt_source_assignment_id = cSource_assignment_id
           and g.vt_transaction_table = rec.vt_transaction_table;
      End Loop;
      -- Find records with invalid Grouping rule 
      For Rec in er3(cSource_assignment_id) loop
        update XXCP_pv_interface g
           set g.vt_status              = 'ERROR',
               g.vt_date_processed      = xxcp_global.SystemDate,
               g.vt_internal_error_code = 11089
         where vt_preview_id             = xxcp_global.gCommon(1).Preview_id
           and g.vt_status               = 'ASSIGNMENT'
           and g.vt_source_assignment_id = cSource_assignment_id
           and g.vt_transaction_table = rec.vt_transaction_table;
      End Loop;
    End If;
  
    Commit;
  
  End Set_Running_Status;


 -- !! ***********************************************************************************************************
 -- !!
 -- !!                                    CONTROL
 -- !!                  External Procedure (Entry Point) to start the AR Engine.
 -- !!
 -- !! ***********************************************************************************************************

  Function Control(cSource_Group_id         IN NUMBER,
	                 cSource_Assignment_id    IN NUMBER,
                   cPreview_id              IN NUMBER,
                   cParent_trx_id           IN NUMBER   DEFAULT 0,
                   cUser_id                 IN NUMBER,
                   cLogin_id                IN NUMBER,
									 cPV_Zero_Flag            IN VARCHAR2,
									 cSource_Rowid            IN Rowid default null) RETURN NUMBER is


    -- Loop Controls vars
    i    pls_integer := 0;
    k    number;
    j    integer := 0;
    g    number  := 0;
    xw   number  := 0; 

    -- Assignment
    cursor cfg(pSource_id in number, pSource_Assignment_id in Number, pRequest_id in number) is
                select x.vt_transaction_table,
                       x.vt_transaction_type,
                       x.vt_transaction_class,
                       x.vt_source_rowid,
                       x.vt_transaction_id,
                       x.vt_parent_trx_id,
											 x.vt_transaction_ref,
                       s.set_of_books_id,
                       x.rowid source_rowid,
                       g.currency_code,
                       g.entered_dr,
                       g.entered_cr,
                       s.source_assignment_id,
                       s.set_of_books_id source_set_of_books_id,
                       x.vt_interface_id,
                       w.transaction_set_name,
                       nvl(w.Transaction_Set_id,0) Transaction_Set_Id,
											 x.vt_transaction_date,
						           y.source_table_id,
						           y.source_type_id,
											 cx.source_class_id,
											 x.vt_status_code,
                       t.calc_legal_exch_rate,
                       nvl(y.explosion_id,0) asg_explosion_id
                  from XXCP_pv_interface       x,
                       XXCP_source_assignments s,
                       XXCP_gl_interface       g,
											 XXCP_sys_source_tables       t,
                       XXCP_sys_source_types        y,
                       XXCP_sys_source_classes      cx,   
                       XXCP_source_transaction_sets w
                 where x.vt_status               = 'ASSIGNMENT'
                   and x.vt_source_assignment_id = pSource_assignment_id
                   and x.vt_source_assignment_id = s.source_assignment_id
                   and g.rowid                   = x.vt_source_rowid
                   -- Table
                   and g.vt_transaction_table    = t.transaction_table
                   and t.source_id               = pSource_id
                   -- Type
                   and g.vt_transaction_type     = y.type
									 and y.latch_only             = 'N'
                   and y.source_table_id         = t.source_table_id
                   -- Class
                   and g.vt_transaction_class    = cx.class
              		 and cx.latch_only             = 'N'
                   and y.source_table_id         = cx.source_table_id
                   -- Transaction_Set_id
                   and y.Transaction_Set_id      = w.Transaction_Set_id
                   and x.vt_preview_id = xxcp_global.gCommon(1).Preview_id
                 order by decode(pSource_id,1,g.user_je_source_name,g.vt_transaction_table),
                          decode(pSource_id,1,g.user_je_category_name,null),
													w.sequence,cx.sequence,
                          x.vt_transaction_id;

    CFGRec CFG%rowtype;

    -- Transaction 
    cursor GLI(pSource_id in number, pSource_Assignment_id in Number, pRequest_id in number) is
      select g.vt_transaction_table,
             g.vt_transaction_type,
             g.vt_transaction_class,
             x.vt_source_rowid,
             g.vt_interface_id,
             x.vt_transaction_id,
						 x.vt_transaction_ref,
             s.instance_id vt_instance_id,
             g.code_combination_id,
             g.user_je_category_name,
             --
             -- balancing segment
             g.user_je_source_name   be2,
             g.group_id              be3,
             g.user_je_category_name be4,
             g.reference1            be6,
             g.reference2            be7,
             g.reference5            be8,
             -- Inventory
             decode(g.accounted_dr,
                    null,
                    g.accounted_cr * -1,
                    g.accounted_dr) mtl_amount,
             -- History
             g.group_id,
             g.user_je_category_name category_name,
             g.user_je_source_name source_name,
             g.accounting_date,
             ((nvl(g.entered_cr, 0) * -1) + nvl(g.entered_dr, 0)) parent_entered_amount,
             g.currency_code parent_entered_currency,
             g.accounted_cr,
             g.accounted_dr,
             g.currency_code,
             x.vt_parent_trx_id,
             g.entered_dr,
             g.entered_cr,
             x.rowid source_rowid,
             s.set_of_books_id,
             nvl(g.segment1, c.segment1) segment1,
             nvl(g.segment2, c.segment2) segment2,
             nvl(g.segment3, c.segment3) segment3,
             nvl(g.segment4, c.segment4) segment4,
             nvl(g.segment5, c.segment5) segment5,
             nvl(g.segment6, c.segment6) segment6,
             nvl(g.segment7, c.segment7) segment7,
             nvl(g.segment8, c.segment8) segment8,
             nvl(g.segment9, c.segment9) segment9,
             nvl(g.segment10, c.segment10) segment10,
             nvl(g.segment11, c.segment11) segment11,
             nvl(g.segment12, c.segment12) segment12,
             nvl(g.segment13, c.segment13) segment13,
             nvl(g.segment14, c.segment14) segment14,
             nvl(g.segment15, c.segment15) segment15,
             s.source_assignment_id,
             s.set_of_books_id source_set_of_books_id,
             w.transaction_set_name,
             w.Transaction_Set_id,
						 t.source_table_id,
						 y.source_type_id,						 
						 cx.source_class_id,
						 t.class_mapping_req,
						 g.gl_sl_link_id,
						 decode(g.user_je_source_name,'Inventory',g.Reference23, g.vt_transaction_id) class_transaction_id,
             t.calc_legal_exch_rate,
             nvl(y.explosion_id,0) asg_explosion_id,
             nvl(y.trx_explosion_id,0) trx_explosion_id
        from XXCP_gl_interface       g,
             XXCP_pv_interface       x,
             gl_code_combinations  c,
             XXCP_source_assignments s ,
             -- New
             XXCP_sys_source_tables       t,
             XXCP_sys_source_types        y,
             XXCP_sys_source_classes      cx,
             XXCP_source_transaction_sets w
       where g.rowid                   = x.vt_source_rowid
         and x.vt_preview_id           = xxcp_global.gCommon(1).Preview_id
         and x.vt_status               = 'TRANSACTION'
				 and nvl(x.vt_status_code,0)   = 0
         and x.vt_source_assignment_id = pSource_Assignment_ID
         and x.vt_source_assignment_id = s.source_assignment_id
         -- Table
         and g.vt_transaction_table = t.transaction_table
         and t.source_id            = pSource_id
         -- Type
         and g.vt_transaction_type  = y.type
         and t.source_table_id      = y.source_table_id
         and y.latch_only           = 'N'
         -- Class
         and g.vt_transaction_class    = cx.class
         and y.source_table_id         = cx.source_table_id
         and cx.latch_only             = 'N'
         -- Transaction_Set_id
         and y.Transaction_Set_id   = w.Transaction_Set_id
         and g.code_combination_id  = c.code_combination_id(+)
       order by decode(psource_id,1,g.user_je_source_name,g.vt_transaction_table),
                decode(psource_id, 1, g.user_je_category_name, null),
								w.sequence, 
                x.vt_parent_trx_id, cx.sequence,x.vt_transaction_id;

    GLIRec GLI%Rowtype;
    
    Cursor usr1(pUser_id in number) is
      select nvl(p.pv_zero_flag,'N') Pv_Zero_Flag, nvl(p.pv_none_cache_flag,'N') pv_none_cache_flag
        from xxcp_user_profiles p
        where p.user_id = pUser_id;    
    
    Cursor ConfErr(pSource_Assignment_id in number, pRequest_id in number) is
      select Distinct k.vt_parent_trx_id, k.vt_transaction_table
        from XXCP_pv_interface k
       where k.vt_request_id           = pRequest_id
         and k.vt_source_assignment_id = pSource_Assignment_id
         and k.vt_status               = 'ERROR';

    -- Assignments
    Cursor sr1(pSource_Assignment_id in number) is
      select set_of_books_id,
             source_assignment_id,
             instance_id source_instance_id,
             source_id,
						 t.stamp_parent_trx
        from XXCP_source_assignments t
       where t.Source_Assignment_id = pSource_Assignment_id
         and t.active   = 'Y';

    vInternalErrorCode        XXCP_errors.INTERNAL_ERROR_CODE%type := 0;
    vExchange_Rate_Type       XXCP_tax_registrations.EXCHANGE_RATE_TYPE%type;
    vCommon_Exch_Curr         XXCP_tax_registrations.COMMON_EXCH_CURR%type;

    vGlobalPrecision          pls_integer := 2;
    CommitCnt                 pls_Integer := 0;
    vSource_Activity          varchar2(4) := 'GL';
    vJob_Status               Number(1)   := 0;

    vTransaction_Class        XXCP_sys_source_classes.Class%type;
    vRequest_ID               XXCP_process_history.request_id%type;

    -- NEW PARAMETERS
    vCurrent_Parent_Trx_id     XXCP_transaction_attributes.parent_trx_id%type := 0; 
    vTransaction_Type          XXCP_sys_source_types.type%type;
    vGlobal_Rounding_Tolerance Number  := 0;
    vTransaction_Error         Boolean := False; 

    -- Used for the REC Distribution
    vExtraLoop            Boolean := False;
    vExtraClass           XXCP_sys_source_tables.extra_record_type%type;
    --
    vSource_Assignment_id XXCP_source_assignments.source_assignment_id%type;
    vStamp_Parent_Trx     XXCP_source_assignments.stamp_parent_trx%type := 'N';
    vSource_instance_id   XXCP_source_assignments.instance_id%type;
    vSource_ID            XXCP_sys_sources.source_id%type;

    vClass_Mapping_Name   XXCP_sys_source_classes.Class%type;

    vTiming_Start         number;
	  vDML_Compiled         varchar2(1);

    vStaged_Records       XXCP_sys_sources.staged_records%type;

    Cursor SF(pSource_Group_id in number) is
      select s.Source_Activity,
             s.Source_id,
             s.Preview_ctl,
             s.Timing,
             s.Custom_events,
             s.Cached,
             s.DML_Compiled,
             S.Staged_Records,
             -- 03.06.07
             nvl(s.ics_flag,'N') ic_settlements
        from XXCP_source_assignment_groups g, XXCP_sys_sources s
       where g.source_group_id = pSource_Group_id
         and s.source_id = g.source_id;

    Cursor Tolx is
      Select Numeric_Code Global_Rounding_Tolerance
        from XXCP_lookups
       where lookup_type = 'ROUNDING TOLERANCE'
         and lookup_code = 'GLOBAL'; 

	  Cursor CRL(pSource_id in number) is
     select Distinct Transaction_table, source_id
       from XXCP_column_rules
      where source_id = pSource_id;	

    -- Dynamic Explosion 
    vExplosion_summary      NUMBER;
    vLast_source_type_id    xxcp_sys_source_types.source_type_id%TYPE := 0;
    vExplosion_sql          VARCHAR2(4000);
    vRowsfound              NUMBER;
    jx                      NUMBER;
    vExplosion_trx_id       NUMBER;
    vExplosion_source_rowid VARCHAR2(32);
    vExplosion_trx_type_id  NUMBER(15);
    vExplosion_trx_class_id NUMBER(15);
    			
  BEGIN

  -- Start up 

    xxcp_global.User_id  := cUser_id;
    xxcp_global.Login_id := fnd_global.login_id;
		
		gSource_Rowid := cSource_Rowid;

    xxcp_global.Trace_on := xxcp_reporting.Get_Trace_Mode(cUser_id);
    xxcp_global.Preview_on := 'Y';
    xxcp_global.SystemDate := Sysdate;
    xxcp_global.gCommon(1).Preview_id  := cPreview_id;
		vTimingCnt := 1;
 

    For SFRec in SF(cSource_Group_id) loop
      vSource_Activity := SFRec.Source_Activity;
      vSource_id := SFRec.Source_id;
      xxcp_global.gCommon(1).Source_id := vSource_id;
      xxcp_global.gCommon(1).Preview_ctl := SFRec.Preview_ctl;
      xxcp_global.gCommon(1).Preview_on  := xxcp_global.Preview_on;
      xxcp_global.gCommon(1).Custom_events := SFRec.Custom_events;
      xxcp_global.gCommon(1).current_Source_activity := vSource_Activity;
      xxcp_global.gCommon(1).Cached_Attributes := SFRec.Cached;
      xxcp_global.gCommon(1).Grouping_Rule := vStamp_Parent_Trx;
      -- 03.06.07 
      xxcp_global.gCommon(1).ic_settlements := SFRec.Ic_Settlements;
	    vDML_Compiled := SFRec.DML_Compiled;
      vStaged_Records := SFRec.Staged_Records;
    End Loop;


 
    
    xxcp_global.SET_SOURCE_ID(cSource_id => vSource_id);

    --     SYS.DBMS_SUPPORT.START_TRACE( waits=>true, binds=>true );

    vTiming(vTimingCnt).CF_Records    := 0;
    vTiming(vTimingCnt).Flush_Records := 0;
    vTiming(vTimingCnt).Start_time    := to_char(sysdate, 'HH24:MI:SS');
    vTiming(vTimingCnt).Flush         := 0;
    vTiming_Start            := xxcp_reporting.Get_Seconds;

    If xxcp_global.gCommon(1).Custom_events = 'Y' then
      xxcp_custom_events.Set_system_mode(xxcp_global.gCommon);
    End If;

  	If vDML_Compiled = 'N' then
	  -- Prevent Processing and the column rules have changed and
	  -- they need compiling.
	  vJob_Status := 4;
	  xxcp_foundation.show('ERROR: Column Rules have changed, but not re-generated');
	  xxcp_foundation.FndWriteError(10999,'Column Rules have changed, but not re-generated');

	End If;
    --
    -- ## *******************************************************************************************************
    -- ##
    -- ##                            Check to See the process is already in use
    -- ##
    -- ## *******************************************************************************************************
    --
    
    xxcp_global.gCommon(1).Preview_id := cPreview_id;

    If vJob_Status = 0 then
      vRequest_id := cPreview_id;

      xxcp_foundation.show('Activity locked....' || to_char(vRequest_id));

      xxcp_global.gCommon(1).Current_request_id := vRequest_id;
			xxcp_global.gCommon(1).Preview_Zero_flag  := cPV_Zero_Flag;

      If xxcp_global.gCommon(1).Custom_events = 'Y' then
        vInternalErrorCode := xxcp_custom_events.BEFORE_PROCESSING(vSource_id);
      End If;
      --
      -- ## **************************************************************************************************
      -- ##
      -- ##                                Setup Ready for the Run
      -- ##
      -- ## **************************************************************************************************
      --
      For REC in SR1(cSource_Assignment_id) Loop

        Exit when vJob_Status <> 0;

        vSource_instance_id   := rec.source_instance_id;
        vSource_Assignment_id := rec.Source_Assignment_id; 

				If vTimingCnt = 1 then
          xxcp_foundation.show('Initialization....');
          vTiming(vTimingCnt).Init_Start := to_char(sysdate, 'HH24:MI:SS');
          
          xxcp_te_base.Engine_Init(cSource_id         => vSource_id,
				                           cSource_Group_id   => cSource_Group_id,
																   cInternalErrorCode => vInternalErrorCode);
        End If;
				
				vTiming(vTimingCnt).Source_Assignment_id := vSource_Assignment_id; 				
      
        -- Setup Globals
        xxcp_global.gCommon(1).current_process_name  := 'XXCP_GLENG';
        xxcp_global.gCommon(1).current_assignment_id := vSource_Assignment_ID;
        
        For usrRec in Usr1(pUser_id  => cPreview_id) Loop
          xxcp_global.gCommon(1).Preview_None_Cache_Flag := usrRec.pv_none_cache_flag;
          xxcp_global.gCommon(1).Preview_Zero_flag       := usrRec.Pv_Zero_Flag;
        End Loop;      
        
        -- 02.06.02
        xxcp_global.gTrans_Table_Array.delete;

        --
        -- *********************************************************************************************************
        --                            Populate Column Rules and Initialize Cursors
        -- *********************************************************************************************************
        --
        vTiming(vTimingCnt).Memory_Start := to_char(sysdate, 'HH24:MI:SS');

				For Rec in CRL(vSource_id) Loop
          -- Used for column Attributes and error messages 
          xxcp_memory_Pack.LoadColumnRules(vSource_id, 'XXCP_GL_INTERFACE',vSource_instance_id,Rec.Transaction_table,vRequest_id,vInternalErrorCode);
          xxcp_te_base.Init_Cursors(Rec.Transaction_table);
				End Loop;
      
        --
        -- *******************************************************************************************************
        --     Reset Previosly Errored Transactions and Assign Parent Trx Id to Incomming transactions
        -- *******************************************************************************************************
        --
        Reset_Errored_Transactions(cSource_id            => vSource_id,
                                   cSource_Assignment_id => vSource_Assignment_id, 
                                   cRequest_id           => vRequest_id);
                                   
        Set_Running_Status(vSource_id, vSource_Assignment_id, vRequest_id);
        
        If  vInternalErrorCode = 0 then                               
            Transaction_Group_Stamp(cSource_Assignment_id => vSource_Assignment_id, 
                                    cSource_Rowid         => cSource_Rowid, 
                                    cUser_id              => cUser_id, 
                                    cInternalErrorCode    => vInternalErrorCode);

            vTiming(vTimingCnt).Init_End := to_char(sysdate, 'HH24:MI:SS');
            --
            -- GLOBAL ROUNDING TOLERANCE
            --
            vGlobal_Rounding_Tolerance := 0;
            For Rec in Tolx loop
              vGlobal_Rounding_Tolerance := Rec.Global_Rounding_Tolerance;
            End loop;

            -- Data Staging 
            If nvl(vInternalErrorCode,0) = 0 then
              vInternalErrorCode := xxcp_custom_events.Data_Staging(cSource_id            => vSource_id,
                                                                    cSource_Assignment_id => vSource_Assignment_id,
                                                                    cRequest_id           => Null,
                                                                    cPreview_id           => cPreview_id);
               
            End If;
    								
            If vInternalErrorCode = 0 then
              --
              -- Get Standard Parameters
              --
              vInternalErrorCode := xxcp_foundation.fnd_gl_lookup(cExchange_Rate_Type => vExchange_Rate_Type,
                                                                  cGlobalPrecision    => vGlobalPrecision,
                                                                  cCommon_Exch_Curr   => vCommon_Exch_Curr);
               
            End If;

            Commit;
        
        End If;

        If vInternalErrorCode = 0 then

          --
          -- ## ***********************************************************************************************************
          -- ##
          -- ##                                Assignment
          -- ##
          -- ## ***********************************************************************************************************
          --
          i := 0;

          vTiming(vTimingCnt).CF_last     := xxcp_reporting.Get_Seconds;
          vTiming(vTimingCnt).CF          := 0;
          vTiming(vTimingCnt).CF_Records  := 0;
          vTiming(vTimingCnt).CF_Start_Date := Sysdate;
					
          xxcp_global.SystemDate := SysDate;
          xxcp_global.Set_Release_Date(Null);
          
					vCurrent_Parent_Trx_id := 0;
          --
          -- Assignment Loop
          --
          For CFGRec in CFG(vSource_id, vSource_Assignment_id, vRequest_id) Loop

                i := i + 1;

                vTiming(vTimingCnt).CF_Records := vTiming(vTimingCnt).CF_Records + 1;

                xxcp_global.gCommon(1).current_source_rowid      := CFGRec.Source_Rowid;
                xxcp_global.gCommon(1).current_transaction_date  := CFGRec.vt_Transaction_Date;
                xxcp_global.gCommon(1).current_transaction_table := CFGRec.vt_Transaction_Table;
								xxcp_global.gCommon(1).current_source_table_id   := CFGRec.Source_Table_id;
                xxcp_global.gCommon(1).current_transaction_id    := CFGRec.vt_Transaction_id;
                xxcp_global.gCommon(1).current_parent_trx_id     := CFGRec.vt_Parent_Trx_id;
                xxcp_global.gCommon(1).current_Interface_id      := CFGRec.vt_interface_id;
								xxcp_global.gCommon(1).current_interface_ref     := CFGREC.vt_transaction_ref; 
                xxcp_global.gCOMMON(1).calc_legal_exch_rate      := CFGREC.calc_legal_exch_rate;

                xxcp_global.gCommon(1).preview_parent_trx_id     := CFGRec.vt_parent_trx_id;
                xxcp_global.gCommon(1).preview_source_rowid      := CFGRec.vt_source_rowid;
                xxcp_global.gCommon(1).explosion_id              := CFGRec.ASG_Explosion_Id; 

                vTransaction_Class := CFGRec.VT_Transaction_Class;

                If vCurrent_Parent_Trx_id <> CFGREC.vt_Parent_Trx_id then  -- New Parent Trx Id
                  xxcp_global.gPassThroughCode := 0;
                End if;

                vCurrent_Parent_Trx_id := CFGREC.vt_Parent_Trx_id;
								
                If XXCP_TE_BASE.Ignore_Class(CFGRec.VT_Transaction_Table, vTransaction_Class) then
                  Begin
                    update XXCP_pv_interface
                       set vt_status              = 'IGNORED',
                           vt_internal_error_code = Null,
                           vt_date_processed      = xxcp_global.SystemDate
                     where rowid = CFGRec.Source_Rowid
                       and vt_preview_id  = xxcp_global.gCommon(1).Preview_id;

                  Exception
                    when OTHERS then Null;
                  End;

                -- First/Second Passthrough
                ELSIF xxcp_global.gPassThroughCode > 0 then
              
                     Update XXCP_pv_interface p
                        set p.vt_status               = decode(xxcp_global.gPassThroughCode,7022,'SUCCESSFUL',
                                                                                            7023,'SUCCESSFUL',
                                                                                            7026,'PASSTHROUGH',
                                                                                            7027,'PASSTHROUGH')
                           ,p.vt_internal_Error_code  = Null
                           ,p.vt_status_code          = xxcp_global.gPassThroughCode
                           ,p.vt_date_processed       = xxcp_global.SystemDate
                      where p.rowid                   = CFGREC.SOURCE_ROWID
                      and   vt_preview_id             = xxcp_global.gCommon(1).Preview_id;
                Else
                  
                         -- Explosion Check
            If CFGRec.ASG_Explosion_Id > 0 Then

                 If CFGREC.Source_Type_Id != vLast_Source_Type_id then
                     vInternalErrorCode := 
                      xxcp_dynamic_sql.ReadExplosionDef(cExplosion_id    => CFGRec.ASG_Explosion_Id, 
                                                        cExplosion_sql   => vExplosion_SQL, 
                                                        cSummary_Columns => vExplosion_Summary);

                     vLast_Source_Type_id := CFGREC.SOURCE_TYPE_ID;

                 End If;
 
                 If vInternalErrorCode = 0 then             
                   vInternalErrorCode := 
                      xxcp_dynamic_sql.ExplosionInitProcess(
                               cSource_Type_Id    => CFGREC.Source_Type_Id,
                               cSource_Rowid      => CFGREC.vt_Source_Rowid,
                               cInterface_id      => CFGREC.vt_Interface_Id,
                               cExplosion_SQL     => vExplosion_SQL,
                               cExplosion_Summary => vExplosion_Summary,
                               cColumnCount       => 2, -- not used
                               cRowsFound         => vRowsFound,
                               cTransaction_Type  => CFGRec.VT_Transaction_Type,
                               cTransaction_Class => CFGRec.VT_Transaction_Class
                               );
                 End if; 
                 If vRowsFound > 0 and vInternalErrorCode = 0 then

                    For jx in 1..vRowsFound Loop
 
                     i := i + 1;

                     xxcp_global.gCommon(1).Explosion_Rowid          := xxcp_dynamic_sql.gDataColumn1(jx);
                     xxcp_global.gCommon(1).Explosion_Transaction_id := xxcp_dynamic_sql.gDataColumn2(jx);
                     xxcp_global.gCommon(1).Explosion_RowsFound      := vRowsFound;

                     vTransaction_Type       := xxcp_dynamic_sql.gDataColumn3(jx);
                     vTransaction_Class      := xxcp_dynamic_sql.gDataColumn4(jx);
                     vExplosion_Trx_Type_id  := xxcp_dynamic_sql.gDataColumnTypeId(jx);
                     vExplosion_Trx_Class_id := xxcp_dynamic_sql.gDataColumnClassId(jx);
                  
                     XXCP_TE_CONFIGURATOR.Control(
                                               cSource_Assignment_ID     => vSource_assignment_id,
                                               cSet_of_books_id          => CFGRec.Source_Set_of_books_id,
																							 cTransaction_Date         => CFGRec.VT_Transaction_Date,
																							 cSource_id                => vSource_id,
                                               cSource_Table_id          => CFGRec.Source_table_id,
                                               cSource_Type_id           => vExplosion_Trx_Type_id,
                                               cSource_Class_id          => vExplosion_Trx_Class_id,
                                               cTransaction_id           => CFGRec.VT_Transaction_id,
                                               cSourceRowid              => CFGRec.Source_Rowid,
                                               cParent_Trx_id            => CFGRec.VT_Parent_Trx_id,
                                               cTransaction_Set_id       => CFGRec.Transaction_Set_id,
                                               cSpecial_Array            => 'N',
                                               cKey                      => Null,
																							 cTransaction_Table        => CFGRec.VT_Transaction_Table,
																							 cTransaction_Type         => vTransaction_Type,
																							 cTransaction_Class        => vTransaction_Class,
                                               cInternalErrorCode        => vInternalErrorCode);

                     xxcp_global.gCommon(1).Explosion_transaction_id := CFGRec.VT_Transaction_id;
                     -- Check for Explosion Config Error
                     If nvl(vInternalErrorCode, 0) <> 0 then
                        Update xxcp_pv_interface
                            set vt_status_code = 7060,
                                vt_internal_error_code = vInternalErrorCode, 
                                vt_date_processed = xxcp_global.SystemDate
                          where rowid         = xxcp_dynamic_sql.gDataColumnPvRowid(jx)
                           and vt_preview_id  = xxcp_global.gCommon(1).Preview_id;
                     End if;

                     Exit when vInternalErrorCode > 0;

                    End Loop;

                  ElsIf vInternalErrorCode = 0 then
                       vInternalErrorCode := 3441;
                  End If;
               
   Else

                  XXCP_TE_CONFIGURATOR.Control(
                                               cSource_Assignment_ID     => vSource_assignment_id,
                                               cSet_of_books_id          => CFGRec.Source_Set_of_books_id,
																							 cTransaction_Date         => CFGRec.VT_Transaction_Date,
																							 cSource_id                => vSource_id,
                                               cSource_Table_id          => CFGRec.Source_table_id,
                                               cSource_Type_id           => CFGRec.Source_type_id,
                                               cSource_Class_id           => CFGRec.Source_class_id,
                                               cTransaction_id           => CFGRec.VT_Transaction_ID,
                                               cSourceRowid              => CFGRec.Source_Rowid,
                                               cParent_Trx_id            => CFGRec.VT_Parent_Trx_id,
                                               cTransaction_Set_id       => CFGRec.Transaction_Set_id,
                                               cSpecial_Array            => 'N', 
                                               cKey                      => Null, 
																							 cTransaction_Table        => CFGRec.VT_Transaction_Table,
																							 cTransaction_Type         => CFGRec.VT_Transaction_Type,
																							 cTransaction_Class        => vTransaction_Class,
                                               cInternalErrorCode        => vInternalErrorCode);
            End If;
                  If xxcp_global.Get_Preview_Waiting(cPreview_id => xxcp_global.gCommon(1).Preview_id) = 'Y' then
                     -- Release date override
                     xxcp_global.Set_Release_Date(Null);
                  End If;

                  -- Check Status
                  If xxcp_global.Get_Release_Date IS NOT NULL THEN
                      Stop_Whole_Transaction;
                  ElsIf nvl(vInternalErrorCode, 0) = 0 then
                    Update XXCP_PV_INTERFACE
                       set vt_status              = 'TRANSACTION',
                           vt_internal_error_code = Null,
                           vt_date_processed      = xxcp_global.SystemDate
                     where rowid = CFGRec.Source_Rowid
                     and vt_preview_id  = xxcp_global.gCommon(1).Preview_id;
              
                  ELSIf vInternalErrorCode = -1 then
                    Update XXCP_PV_INTERFACE
                       set vt_status              = 'SUCCESSFUL',
                           vt_internal_error_code = Null,
                           vt_date_processed      = xxcp_global.SystemDate,
                           vt_status_code         = 7001
                     where rowid = CFGRec.Source_Rowid
                     and vt_preview_id  = xxcp_global.gCommon(1).Preview_id;
                  ElsIf  vInternalErrorCode  BETWEEN 7021 and 7028 then
									  Pass_Through_Whole_Trx(CFGRec.Source_Rowid, vInternalErrorCode);
                  ElsIf nvl(vInternalErrorCode, 0) <> 0 then
                    Update XXCP_PV_INTERFACE
                       set vt_status              = 'ERROR',
                           vt_internal_error_code = 12000,
                           vt_date_processed      = xxcp_global.SystemDate
                     where rowid = CFGRec.Source_Rowid
                     and vt_preview_id  = xxcp_global.gCommon(1).Preview_id;

                  End If;

                End If; -- End configuration

                -- ##
                -- ## Commit Configured rows
                -- ##

                If i >= 1000 then
                  xxcp_global.SystemDate    := Sysdate;
                  Commit;
                  i           := 0;
                  -- !! Dont test for termination
                  Exit when vJob_Status > 0; -- Controlled Exit
                End If;


            --
          End loop;
          xxcp_dynamic_sql.Close_Session;
          -- #
          -- # Clear Common vars
          -- #
          xxcp_global.gCommon(1).current_transaction_table := Null;
					xxcp_global.gCommon(1).current_source_table_id   := Null;
          xxcp_global.gCommon(1).current_transaction_id    := Null;
          xxcp_global.gCommon(1).current_parent_trx_id     := Null;
					xxcp_global.gcommon(1).current_interface_ref     := Null; 
		      xxcp_global.Set_New_Trx_Flag('N');

          Commit; -- Final Configuration Commit.

          --
          If xxcp_global.gCommon(1).Custom_events = 'Y' then
            xxcp_custom_events.AFTER_ASSIGNMENT_PROCESSING(vSource_id,
                                                           vSource_assignment_id);
          End If;

          vTiming(vTimingCnt).CF := xxcp_reporting.Get_Seconds_Diff(vTiming(vTimingCnt).CF_last,xxcp_reporting.Get_Seconds);
          vTiming(vTimingCnt).CF_End_Date := Sysdate;

          -- ***********************************************************************************************************
          --
          --  Set associated Transactions to error if anyone of the Transactions is a set has errored in configuration
          --
          -- ***********************************************************************************************************
          --
          xxcp_foundation.show('Reporting Configuration errors....');
          xxcp_global.SystemDate := Sysdate;
          vInternalErrorCode := 0;
          For ConfErrRec in ConfErr(vSource_Assignment_id, vRequest_id) loop

            Begin
              update XXCP_pv_interface
                 set vt_status              = 'ERROR',
                     vt_internal_Error_code = 12824, -- Associated errors
                     vt_date_processed      = xxcp_global.SystemDate
               where vt_request_id           = vRequest_id
                 and vt_Source_Assignment_id = vSource_Assignment_id
                 and vt_status               IN ('ASSIGNMENT', 'TRANSACTION')
                 and vt_parent_trx_id        = ConfErrRec.vt_parent_trx_id
                 and vt_transaction_table    = ConfErrRec.vt_Transaction_table;

            Exception
              when OTHERS then
                vInternalErrorCode := 12819; -- Update Failed.

            End;
          End Loop;

          --## ***********************************************************************************************************
          --##
          --##                                    Transaction Engine
          --##
          --## ***********************************************************************************************************

          vTiming(vTimingCnt).TE_Last       := xxcp_reporting.Get_Seconds;
          vTiming(vTimingCnt).TE_Records    := 0;
          vTiming(vTimingCnt).TE_Start_Date := Sysdate;

          vExtraLoop             := False;
          vExtraClass            := Null;
          xxcp_global.SystemDate := Sysdate;
          i                := 0;

          If vInternalErrorCode = 0 and vJob_Status = 0 then

            xxcp_foundation.show('Transactions....');
            xxcp_wks.Reset_Source_Segments;
            gBalancing_Array      := xxcp_wks.Clear_Segments;
            ClearWorkingStorage;
            vCurrent_Parent_Trx_id := 0;
            
            For  GLIRec in  GLI(vSOURCE_ID, vSOURCE_ASSIGNMENT_ID, vRequest_id) Loop

                  vInternalErrorCode := 0;

                  vTiming(vTimingCnt).TE_Records := vTiming(vTimingCnt).TE_Records + 1;
                  
                  -- Dynamic Explosion at Transaction Level
                  xxcp_global.gCommon(1).Explosion_id     := glirec.Trx_Explosion_id;
                  xxcp_global.gCommon(1).Trx_Explosion_id := glirec.Trx_Explosion_id;
                  --
                  -- ***********************************************************************************************************
                  --        Set the balancing Setments to be written to the Column Rules Array
                  -- ***********************************************************************************************************
                  --
                  -- Poplulate the Balancing Array
                  xxcp_wks.Source_Balancing(1) := Null;
                  xxcp_wks.Source_Balancing(2) := GLIRec.be2; -- User JE Source Name
                  xxcp_wks.Source_Balancing(3) := GLIRec.be3; -- Group Id
                  xxcp_wks.Source_Balancing(4) := GLIRec.be4; -- User JE Category Name
                  xxcp_wks.Source_Balancing(5) := Null;
                  xxcp_wks.Source_Balancing(6) := GLIRec.be6; -- Reference 1
                  xxcp_wks.Source_Balancing(7) := GLIRec.be7; -- Reference 2
                  xxcp_wks.Source_Balancing(8) := GLIRec.be8; -- Reference 3
                  xxcp_wks.Source_Balancing(9) := Null;
                  xxcp_wks.Source_Balancing(10):= Null;
                  xxcp_wks.Source_Balancing(11):= Null;  -- Balancing Segment
                  xxcp_wks.Source_Balancing(12):= Null;  -- Rounding Group
                  
                  --
                  -- ***********************************************************************************************************
                  --         Move Accounting Codes from Master Record to Array
                  -- ***********************************************************************************************************
                  -- Populate Source Account Segments
                  xxcp_wks.Source_Segments(01) := GLIRec.Segment1;
                  xxcp_wks.Source_Segments(02) := GLIRec.Segment2;
                  xxcp_wks.Source_Segments(03) := GLIRec.Segment3;
                  xxcp_wks.Source_Segments(04) := GLIRec.Segment4;
                  xxcp_wks.Source_Segments(05) := GLIRec.Segment5;
                  xxcp_wks.Source_Segments(06) := GLIRec.Segment6;
                  xxcp_wks.Source_Segments(07) := GLIRec.Segment7;
                  xxcp_wks.Source_Segments(08) := GLIRec.Segment8;
                  xxcp_wks.Source_Segments(09) := GLIRec.Segment9;
                  xxcp_wks.Source_Segments(10) := GLIRec.Segment10;
                  xxcp_wks.Source_Segments(11) := GLIRec.Segment11;
                  xxcp_wks.Source_Segments(12) := GLIRec.Segment12;
                  xxcp_wks.Source_Segments(13) := GLIRec.Segment13;
                  xxcp_wks.Source_Segments(14) := GLIRec.Segment14;
                  xxcp_wks.Source_Segments(15) := GLIRec.Segment15;

                  xxcp_global.gCommon(1).preview_parent_trx_id   := GLIRec.VT_PARENT_TRX_ID;
                  xxcp_global.gCommon(1).preview_source_rowid    := GLIRec.vt_source_rowid;
                  xxcp_global.Set_New_Trx_Flag('N');
                  
                  xxcp_global.gCommon(1).explosion_id          := GLIRec.ASG_Explosion_Id; -- new

                  --
                  -- ***********************************************************************************************************
                  --         When a New Transaction is detected then flush the current buffer
                  -- ***********************************************************************************************************
                  --
                  If (vCurrent_Parent_Trx_id <> GLIRec.vt_parent_Trx_id) then

                    vTransaction_Error := False;
                   
                    --
                    -- !! ******************************************************************************************************
                    --                             FLUSH TRANSACTION
                    -- !! ******************************************************************************************************
                    --
                    If vCurrent_Parent_Trx_id <> 0 then
                      CommitCnt := CommitCnt + 1; -- Number of records process since last commit.

                      k := FlushRecordsToTarget(vGlobal_Rounding_Tolerance,
                                                vGlobalPrecision,
                                                vTransaction_Type,
                                                vInternalErrorCode);
                    End If;

                    --
                    -- ***********************************************************************************************************
                    --            Store Variables that are only set once per Parent Trx Id
                    -- ***********************************************************************************************************
                    --
                    xxcp_global.gCommon(1).current_transaction_table := GLIRec.vt_Transaction_Table;
										xxcp_global.gCommon(1).current_source_table_id   := GLIRec.Source_Table_id;
                    xxcp_global.gCommon(1).current_parent_trx_id     := GLIRec.vt_Parent_Trx_id;
										
                    vCurrent_Parent_Trx_id := GLIRec.vt_Parent_Trx_id;
                    vTransaction_Type      := GLIRec.vt_Transaction_Type;

                    xxcp_global.gCommon(1).current_source_rowid    := GLIRec.Source_Rowid;
                    xxcp_global.gCommon(1).current_Interface_id    := GLIRec.vt_interface_id;
                    xxcp_global.gCommon(1).calc_legal_exch_rate    := GLIRec.calc_legal_exch_rate;
                    xxcp_global.gCommon(1).current_Accounting_date := GLIRec.Accounting_Date;
                    xxcp_global.gCommon(1).current_Batch_number    := GLIRec.Group_id;
                    xxcp_global.gCommon(1).current_Category_name   := GLIRec.Category_Name;
                    xxcp_global.gCommon(1).current_Source_name     := GLIRec.Source_name;
										xxcp_global.gcommon(1).current_interface_ref   := GLIRec.vt_transaction_ref; 

                    xxcp_global.Set_New_Trx_Flag('Y');

                    --
                    -- ***********************************************************************************************************
                    --                         Extra Loops for different Classes
                    -- ***********************************************************************************************************
                    --
                    vExtraLoop  := False;
                    vExtraClass := Null;
                    For j in 1 .. xxcp_global.gSRE.Count loop
                      xw := j;
                      If GLIRec.vt_Transaction_Table = xxcp_global.gSRE(j).Transaction_table then
                        vExtraClass := xxcp_global.gSRE(j).Extra_Record_type;
                        vExtraLoop  := TRUE;
                        Exit;
                      End If;
                    End Loop;

                  Else
                    vExtraLoop  := FALSE; -- !!!!
                    vExtraClass := Null;
                  End If;


                  xxcp_global.gCommon(1).current_transaction_id := GLIRec.vt_Transaction_id;
                  --
                  -- ***********************************************************************************************************
                  --                                   Store Working Variables
                  -- ***********************************************************************************************************
                  --
                  xxcp_global.gCommon(1).current_source_rowid    := GLIRec.Source_Rowid;
                  xxcp_global.gCommon(1).current_Interface_id    := GLIRec.vt_interface_id;
                  xxcp_global.gCommon(1).current_Accounting_date := GLIRec.Accounting_Date;
                  xxcp_global.gCommon(1).current_Batch_number    := GLIRec.Group_id;
                  xxcp_global.gCommon(1).current_Category_name   := GLIRec.Category_Name;
                  xxcp_global.gCommon(1).current_Source_name     := GLIRec.Source_name;
                  --
                  -- ***********************************************************************************************************
                  --                                   Call the Transaction Engine
                  -- ***********************************************************************************************************
                  --
                  If vTransaction_Error = False then
                    vInternalErrorCode := 0; -- Reset for each row

                    i := i + 1; -- Count the number of rows processed.

                    xxcp_wks.Source_CNT  := xxcp_wks.SOURCE_CNT + 1;
                    xxcp_wks.Source_rowid(xxcp_wks.source_cnt)   := GLIRec.Source_Rowid;
                    xxcp_wks.SOURCE_SUB_CODE(xxcp_wks.source_cnt):= 0;
                    xxcp_wks.SOURCE_STATUS(xxcp_wks.source_cnt)  := '?';


                    -- *********************************************************************
                    -- Inventory Logic
                    -- *********************************************************************
										If GLIRec.class_mapping_req = 'Y' then
  									    vClass_Mapping_Name := xxcp_te_base.class_mapping(
												                                   cSource_id              => vSource_id,
										                                       cClass_Mapping_Required => GLIRec.class_mapping_req,
																													 cTransaction_Table      => GLIRec.vt_transaction_table,
																													 cTransaction_Type       => GLIRec.vt_transaction_type,
																													 cTransaction_Class      => GLIRec.vt_transaction_class,
																													 cTransaction_id         => GLIRec.class_transaction_id,
																													 cCode_Combination_id    => GLIRec.code_combination_id,
																													 cAmount                 => GLIRec.mtl_amount,
																													 cSystem_Link_id         => GLIRec.gl_sl_link_id);
                    End If;			
										
                    -- *********************************************************************
                    -- End Inventory Logic
                    -- *********************************************************************
                    XXCP_TE_ENGINE.Control(cSource_assignment_id    => vSource_Assignment_id,
                                           cSource_Table_id         => GLIRec.Source_Table_id,
																					 cSource_Type_id          => GLIRec.Source_Type_id,
                                           cSource_Class_id         => GLIRec.Source_Class_id,
                                           cTransaction_id          => GLIRec.vt_Transaction_id,
                                           cParent_Trx_id           => GLIRec.vt_Parent_Trx_id,
                                           cSourceRowid             => GLIRec.Source_Rowid,
                                           cParent_Entered_Amount   => GLIRec.Parent_Entered_Amount,
                                           cParent_Entered_Currency => GLIRec.Parent_Entered_Currency,
																					 cClass_Mapping           => GLIRec.class_mapping_req,
                                           cClass_Mapping_Name      => vClass_Mapping_Name,
                                           -- Segments 1-15
                                           cInterface_ID            => GLIRec.VT_Interface_id,
                                           cExtraLoop               => vExtraLoop,
                                           cExtraClass              => vExtraClass,
                                           cTransaction_set_id      => GLIRec.Transaction_Set_id,
																					 --
																					 cTransaction_Table       => GLIRec.VT_Transaction_Table,
																					 cTransaction_Type        => GLIRec.VT_Transaction_Type,
                                           cTransaction_Class       => GLIRec.vt_Transaction_Class,
                                           cInternalErrorCode       => vInternalErrorCode);
                  
     
                    xxcp_global.gCommon(1).current_trading_set := Null;
                  
                    -- Report Errors
                    If vInternalErrorCode <> 0 then
                      Error_Whole_Transaction(vInternalErrorCode);
                      vTransaction_Error := True;
                    End If;
                  
                  End If;
                  --
                  -- ***********************************************************************************************************
                  --                     Test to See if Oracle or the User has terminated this process
                  --                                   And Commit after Each 1000 Records processed
                  -- ***********************************************************************************************************
                  --
                  IF CommitCnt >= 100 THEN
                    CommitCnt := 0;
                    xxcp_global.SystemDate    := sysdate;
                    Commit;
                    -- !! Dont test for termination in Preview Mode
                    Exit when vJob_Status > 0; -- Controlled Exit
                  END IF;
                 
            END LOOP;
						
            -- Pass Through 
						If vJob_Status = 0 and xxcp_te_base.GetPassThroughActive = 'Y' then
						   Begin
							 
							   update XXCP_pv_interface x
                   set vt_status  = 'SUCCESSFUL'
								  where x.vt_Source_assignment_id = vSource_Assignment_id
										and x.vt_preview_id           = xxcp_global.gCommon(1).Preview_id
									  and x.vt_request_id           = xxcp_global.gCommon(1).Current_request_id
                    and x.vt_status               = 'PASSTHROUGH'
				            and x.vt_status_code          between 7025 and 7028;
										
								 Commit;
								 
								 Exception when OTHERS then NULL;
								 
							 End;
						End If;
														
            xxcp_te_base.Close_Cursors;

            --
            --  !!***********************************************************************************************************
            --                     Flush the Buffer for a Final Time
            -- !! ***********************************************************************************************************
            --
            IF vCurrent_Parent_Trx_id <> 0 and vInternalErrorCode = 0 then
              If vInternalErrorCode = 0 then
                k := FlushRecordsToTarget(vGlobal_Rounding_Tolerance,
                                          vGlobalPrecision,
                                          vTransaction_type,
                                          vInternalErrorCode);
              End If;
            End If;
          End If; -- Internal Error Check GLI
        End If; -- End Internal Error Check
      
        vTiming(vTimingCnt).TE := xxcp_reporting.Get_Seconds_Diff(vTiming(vTimingCnt).TE_last,xxcp_reporting.Get_Seconds);
      
        xxcp_foundation.show('Clear down...');
        xxcp_global.SystemDate := Sysdate;
        -- *******************************************************************
        -- Error out records remaining with a Processing status after the run 
        -- *******************************************************************
        Begin
            Update xxcp_pv_interface
                 set vt_status              = 'ERROR'
                    ,vt_internal_error_code = Decode(vt_status,'TRANSACTION',12999,'ASSIGNMENT',12998)
                    ,vt_date_processed      = sysdate
               where rowid                  = any
			              (select g.Rowid
                    from xxcp_pv_interface g
                   where g.vt_status               in ('ASSIGNMENT', 'TRANSACTION')
                     and g.vt_preview_id           = xxcp_global.gCommon(1).preview_id
                     and g.vt_source_assignment_id = vSource_assignment_id);
                     

            Exception when OTHERS then Null;
        End;

        -- #
        -- # Clear Common vars
        -- #
        xxcp_global.gCommon(1).current_transaction_table := Null;
				xxcp_global.gCommon(1).current_source_table_id   := Null;
        xxcp_global.gCommon(1).current_transaction_id    := Null;
        xxcp_global.gCommon(1).current_parent_trx_id     := Null;
				xxcp_global.gcommon(1).current_interface_ref     := Null; 
        --
        -- ***********************************************************************************************************
        --                     Clear Down
        -- ***********************************************************************************************************
        --
        ClearWorkingStorage;
        --
        -- ***********************************************************************************************************
        --                     DeActive the Lock in XXCP_ACTIVITY_CONTROL
        -- ***********************************************************************************************************
        --
        Commit;
        vTiming(vTimingCnt).TE_End_Date := Sysdate;
				vTimingCnt := vTimingCnt + 1;
      End loop; -- SR1
    
        
      If xxcp_global.gCommon(1).Custom_events = 'Y' then
        xxcp_custom_events.After_processing(cSource_id       => vSource_id,
                                            cSource_Group_id => cSource_Group_id,
																						cRequest_id      => cPreview_id,
																						cJob_Status      => -1); -- < Preview mode >
      End If;
  
      xxcp_te_base.ClearDown;
      xxcp_memory_pack.ClearDown;
      xxcp_dynamic_sql.Close_Session;
		
		  Commit;

    Else
      --
      -- Report If this process was already in use
      --
      xxcp_foundation.show('Engine already in use');
      vJob_Status := 1;
    End If;
  
    xxcp_te_base.Write_Timings(vTiming, vTiming_Start);
    --
    -- ************************************************************************************************
    --                     Show Time Statistics when called from SQL*Plus
    -- ************************************************************************************************
    --
    xxcp_global.gCommon(1).Current_Request_id := Null;
    --
    -- ***********************************************************************************************************
    --                     Return Job Status Code and Finish!!
    -- ***********************************************************************************************************
    --
    Return(vJob_Status);
  End Control;


Begin
  -- Get System Date once
  xxcp_global.SystemDate := Sysdate;

END XXCP_PV_GL_ENGINE;
/
