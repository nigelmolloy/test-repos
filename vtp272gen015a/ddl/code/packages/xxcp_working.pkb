CREATE OR REPLACE PACKAGE BODY XXCP_WKS IS
/******************************************************************************
                        V I R T A L  T R A D E R  L I M I T E D 
  		      (Copyright 2007-2012 Virtual Trader Ltd. All rights Reserved)

   NAME:       XXCP_WKS 
   PURPOSE:    Define Working Storage

   REVISIONS:
   Ver        Date      Author  Description
   ---------  --------  ------- ------------------------------------
   02.02.00   25/02/05  Keith   First Coding
   02.02.01   16/03/05  Keith   Reset Base Array to Internal not Dynamic
	 02.02.02   11/05/05  Keith   Added to balancing
   02.02.03   11/10/05  Keith   Added Summary record
   02.02.04   11/11/05  Keith   fndwrit
	 02.02.05   19/12/05  Keith   Added to BRK_SET_RCD
	 02.02.06   25/12/05  Keith   Added ONCE_RCD
	 02.03.00   22/01/06  Keith   Added Last_Transaction_Table in definition
	                              added Record_status and Target Rounding (WORKING_RCD)
   02.03.01   02/11/06  Keith   added trading_set_seq
	 02.03.02   17/11/06  Keith   Zero_Suppression_rule
	 02.04.00   09/07/07  Keith   Added Tax Record
	 02.04.01   03/10/07  Keith   Increased cache to 25 elements
	 02.04.02   10/10/07  Keith   Increased break points to 25 elements too.
   02.04.03   20/02/09  Keith   Look at Extended values flag.
   02.04.04   10/10/09  Nigel   Added attribute11..30 to gREALTIME_RESULTS_TYPE
   02.04.05   21/04/09  Simon   Added replan.
   02.04.06   01/05/09  Simon   Add category_data_source to gActual_costs_Rec_Type in Spec.
   02.04.07   01/05/09  Simon   Add instance_id to gActual_costs_Rec_Type in Spec.
   02.04.08   02/07/09  Simon   Add I1009_ArrayGLOBAL.
   02.04.09   13/07/09  Simon   Add "VT_REPLAN" columns to gActual_costs_Rec_Type.
   02.06.01   01/07/09  Nigel   Made changes for "Common Assignment"
   02.06.02   27/07/09  Nigel   Synchronize with vt271 Patch10.
   02.06.03   29/09/09  Keith   Added formulas_log
   02.06.04   06/11/09  Keith   Increased Source_Balancing to use xxcp_dynamic_long
   02.06.05   21/12/09  Simon   Added vt_results_attribute31..50 to gREALTIME_RESULTS_TYPE
   02.06.05b  15/04/10  Keith   Added Trading Set Id to Once rule record
   02.06.05d  15/05/10  Keith   Extended Balancing Element to 300
   02.06.06   11/11/10  Keith   Added Event Timing 
   02.06.07   28/02/11  Keith   Added Record number and line number assigned to record type
   02.06.08   14/07/11  Keith   Added Pass_Through_Log
   02.06.09   26/09/11  Keith   Added Target_Suppression_rule
   02.06.10   23/11/11  Keith   Added Owner Tax Reg to Once Rule
   02.06.11   31/03/12  Keith   Added Parent Assignemnt
   03.06.12   17/08/12  Simon   Init now sets Max_Column_Attribute (OOD-247).
   03.06.13   14/09/12  Simon   Added Cost_Plus_Set_Id (OOD-293).
   03.06.14   11/10/12  Mat     OOD-349 Long Trx References; now 20 long_references;
                                Add another 20 Long_references  
   03.06.15   21/11/12  Keith   Removed old Sabrix structure making way for web service call
***********************************************************************************************/
  FUNCTION Software_Version RETURN VARCHAR2 IS
   BEGIN
     RETURN('Version [03.06.15] Build Date [21-NOV-2012] Name [XXCP_WKS]');
   END Software_Version;


  -- **********************************************************
  --       DumpWriteError
  --
  -- This procedure writes a record to the xxcp_errors table
  -- and date stamps it with sysdate.
  -- XXCP_ERRORS now can hold LONG as the ERROR Message
  -- **********************************************************
  Procedure DumpWriteError(cInternalErrorCode in Number,
                          cErrorMessage      in varchar2,
                          cLong_Message      in Long Default Null) is
  
    pragma autonomous_transaction;
  
    gSysdate date;
  
  Begin
  
    gSysdate := Sysdate;
  
    If cErrorMessage is not null then
    
      If nvl(xxcp_global.Preview_on,'N') = 'N' then
        Begin
        
          Insert into xxcp_errors
            (Source_Activity,
             Process_name,
             Error_Message,
             Long_Message,
             Transaction_id,
             Source_Table_id,
             Parent_Trx_id,
             Internal_Error_Code,
             Source_Assignment_id,
             Source_Rowid,
             Error_id,
						 Request_id,
             Trading_set_id,
             created_by,
             creation_date,
             last_update_login)
          Values
            (xxcp_global.gCommon(1).current_source_activity,
             xxcp_global.gCommon(1).current_Process_name,
             cErrorMessage,
             cLong_Message,
             xxcp_global.gCommon(1).current_transaction_id,
             xxcp_global.gCommon(1).current_source_table_id,
             xxcp_global.gCommon(1).current_parent_trx_id,
             cInternalErrorCode,
             xxcp_global.gCommon(1).current_assignment_id,
             xxcp_global.gCommon(1).current_source_rowid,
             XXCP_ERRORS_SEQ.nextval,
						 xxcp_global.gCommon(1).current_request_id,
             xxcp_global.gCommon(1).current_trading_set_id,
             xxcp_global.User_id,
             gSysdate,
             xxcp_global.Login_id);
        
           Exception when Others then Null;
        End;
      Else
        Begin
        
          -- Preview errors
          Insert into xxcp_pv_errors
            (Preview_id,
             Source_Activity,
             Process_name,
             Error_Message,
             Long_Message,
             Transaction_id,
             Source_Table_id,
             Parent_Trx_id,
             Internal_Error_Code,
             Source_Assignment_id,
             Source_Rowid,
             Error_id,
						 Request_id,
             Trading_set_id ,
             created_by,
             creation_date,
             last_update_login)
          values
            (xxcp_global.gCommon(1).Preview_id,
             xxcp_global.gCommon(1).current_source_activity,
             xxcp_global.gCommon(1).current_Process_name,
             cErrorMessage,
             cLong_message,
             xxcp_global.gCommon(1).current_transaction_id,
             xxcp_global.gCommon(1).current_source_table_id,
             xxcp_global.gCommon(1).current_parent_trx_id,
             cInternalErrorCode,
             xxcp_global.gCommon(1).current_assignment_id,
             xxcp_global.gCommon(1).current_source_rowid,
             XXCP_ERRORS_SEQ.nextval,
						 xxcp_global.gCommon(1).current_request_id,
             xxcp_global.gCommon(1).current_trading_set_id,
             xxcp_global.User_id,
             gSysdate,
             xxcp_global.Login_id);
        
        Exception When OTHERS then Null;
          
        End;
      End If;
    
      Commit;
    
    End If;
  End DumpWriteError;

   --- Dynamic 1001
   Procedure Reset_D1001 is
     Begin

	   xxcp_wks.D1001_Array := xxcp_wks.Clear_Dynamic;

     End Reset_D1001;

   Procedure Reset_D1002 is
     Begin

	   xxcp_wks.D1002_Array := xxcp_wks.Clear_Dynamic;

     End Reset_D1002;

   Procedure Reset_D1003 is

     Begin

	     xxcp_wks.D1003_Array := xxcp_wks.Clear_Dynamic;

     End Reset_D1003;

   --- Dynamic 1004
   Procedure Reset_D1004 is
     Begin
	     xxcp_wks.D1004_Array := xxcp_wks.Clear_Dynamic;
     End Reset_D1004;

   Procedure Reset_WKR is
    --
	  -- Reset Working Storage for the current VT Record
	  --
    Begin
	
	  xxcp_wks.Account_Segments(1)  := Null;
	  xxcp_wks.Account_Segments(2)  := Null;
	  xxcp_wks.Account_Segments(3)  := Null;
	  xxcp_wks.Account_Segments(4)  := Null;
	  xxcp_wks.Account_Segments(5)  := Null;
	  xxcp_wks.Account_Segments(6)  := Null;
	  xxcp_wks.Account_Segments(7)  := Null;
	  xxcp_wks.Account_Segments(8)  := Null;
	  xxcp_wks.Account_Segments(9)  := Null;
	  xxcp_wks.Account_Segments(10) := Null;
	  xxcp_wks.Account_Segments(11) := Null;
	  xxcp_wks.Account_Segments(12) := Null;
	  xxcp_wks.Account_Segments(13) := Null;
	  xxcp_wks.Account_Segments(14) := Null;
	  xxcp_wks.Account_Segments(15) := Null;

	End Reset_WKR;
	
 
   Procedure Reset_Account_Segments is
    --
	-- Reset Working Storage for the current VT Record
	--
    Begin
	
	  xxcp_wks.Account_Segments(1)  := Null;
	  xxcp_wks.Account_Segments(2)  := Null;
	  xxcp_wks.Account_Segments(3)  := Null;
	  xxcp_wks.Account_Segments(4)  := Null;
	  xxcp_wks.Account_Segments(5)  := Null;
	  xxcp_wks.Account_Segments(6)  := Null;
	  xxcp_wks.Account_Segments(7)  := Null;
	  xxcp_wks.Account_Segments(8)  := Null;
	  xxcp_wks.Account_Segments(9)  := Null;
	  xxcp_wks.Account_Segments(10) := Null;
	  xxcp_wks.Account_Segments(11) := Null;
	  xxcp_wks.Account_Segments(12) := Null;
	  xxcp_wks.Account_Segments(13) := Null;
	  xxcp_wks.Account_Segments(14) := Null;
	  xxcp_wks.Account_Segments(15) := Null;
	
	End Reset_Account_Segments;
	

    Procedure Reset_Enabled_Segments is	
    --
	-- Reset Working Storage for the current VT Record
	--
    Begin
	
	  xxcp_wks.Enabled_Segments(1)  := Null;
	  xxcp_wks.Enabled_Segments(2)  := Null;
	  xxcp_wks.Enabled_Segments(3)  := Null;
	  xxcp_wks.Enabled_Segments(4)  := Null;
	  xxcp_wks.Enabled_Segments(5)  := Null;
	  xxcp_wks.Enabled_Segments(6)  := Null;
	  xxcp_wks.Enabled_Segments(7)  := Null;
	  xxcp_wks.Enabled_Segments(8)  := Null;
	  xxcp_wks.Enabled_Segments(9)  := Null;
	  xxcp_wks.Enabled_Segments(10) := Null;
	  xxcp_wks.Enabled_Segments(11) := Null;
	  xxcp_wks.Enabled_Segments(12) := Null;
	  xxcp_wks.Enabled_Segments(13) := Null;
	  xxcp_wks.Enabled_Segments(14) := Null;
	  xxcp_wks.Enabled_Segments(15) := Null;
	
	End Reset_Enabled_Segments;
	
	procedure Reset_Internal_Array is
 
	Begin
	  xxcp_wks.I1009_Array := xxcp_wks.Clear_Internal;
	End Reset_Internal_Array;
	
	
	procedure Reset_TIER_Array is
 
	Begin
	  xxcp_wks.D1007_Array := xxcp_wks.Clear_Internal;
	End Reset_TIER_Array;
	
	
	procedure Reset_Column_Segments is
 
	Begin
	  xxcp_wks.D1006_Array := xxcp_wks.Clear_1006_Dynamic;
	End Reset_Column_Segments;
	
	Procedure Reset_8000_Array is
	  i pls_integer;
	 Begin
	   For i in 1..D1008_Array.Count Loop
	    xxcp_wks.D1008_Array(i) := Null;
	   End Loop;
	 End Reset_8000_Array;
		
	procedure Reset_Source_Segments is
	    Begin
	
    	  xxcp_wks.Source_Segments(1)  := Null;
    	  xxcp_wks.Source_Segments(2)  := Null;
    	  xxcp_wks.Source_Segments(3)  := Null;
    	  xxcp_wks.Source_Segments(4)  := Null;
    	  xxcp_wks.Source_Segments(5)  := Null;
    	  xxcp_wks.Source_Segments(6)  := Null;
    	  xxcp_wks.Source_Segments(7)  := Null;
    	  xxcp_wks.Source_Segments(8)  := Null;
    	  xxcp_wks.Source_Segments(9)  := Null;
    	  xxcp_wks.Source_Segments(10)  := Null;
    	  xxcp_wks.Source_Segments(11)  := Null;
    	  xxcp_wks.Source_Segments(12)  := Null;
    	  xxcp_wks.Source_Segments(13)  := Null;
    	  xxcp_wks.Source_Segments(14)  := Null;
    	  xxcp_wks.Source_Segments(15)  := Null;
	
	End Reset_Source_Segments;
	
	Procedure Reset_Dynamic_Results is
	  Begin
	     xxcp_wks.DYNAMIC_RESULTS_CNT := 0;
	     xxcp_wks.DYNAMIC_RESULTS := xxcp_wks.Clear_Dynamic;
	  End Reset_Dynamic_Results;
	

    Procedure Reset_Gain_Loss_Arrays(cAction in varchar2) is
	  Begin
	  
	   If cAction = 'S' then
       D1001_ArrayGL        := D1001_ArrayCH;
       D1002_ArrayGL        := D1002_ArrayCH;
       D1003_ArrayGL        := D1003_ArrayCH;
       D1004_ArrayGL        := D1004_ArrayCH;
       D1005_ArrayGL        := D1005_ArrayCH;
       D1006_ArrayGL        := D1006_ArrayCH;
       D1007_ArrayGL        := D1007_ArrayCH;
       D1008_ArrayGL        := D1008_ArrayCH;
       I1009_ArrayGL        := I1009_ArrayCH;
       vBase_ArrayGL        := I1009_ArrayCH;
  
	  Else -- Control
       D1001_ArrayGL        := xxcp_wks.D1001_Array;
       D1002_ArrayGL        := xxcp_wks.D1002_Array;
       D1003_ArrayGL        := xxcp_wks.D1003_Array;
       D1004_ArrayGL        := xxcp_wks.D1004_Array;
       D1005_ArrayGL        := xxcp_wks.D1005_Array;
       D1006_ArrayGL        := xxcp_wks.D1006_Array;
       D1007_ArrayGL        := xxcp_wks.D1007_Array;
       D1008_ArrayGL        := xxcp_wks.D1008_Array;
       I1009_ArrayGL        := xxcp_wks.I1009_Array;
       vBase_ArrayGL        := xxcp_wks.I1009_Array;

	  End If;
  End Reset_Gain_Loss_Arrays;

  Procedure Reset_Local_Arrays is
	  Begin
       D1001_ArrayLC        := xxcp_wks.D1001_Array;
       D1002_ArrayLC        := xxcp_wks.D1002_Array;
       D1003_ArrayLC        := xxcp_wks.D1003_Array;
       D1004_ArrayLC        := xxcp_wks.D1004_Array;
       D1005_ArrayLC        := xxcp_wks.D1005_Array;
       D1006_ArrayLC        := xxcp_wks.D1006_Array;
       D1007_ArrayLC        := xxcp_wks.D1007_Array;
       D1008_ArrayLC        := xxcp_wks.D1008_Array;
       I1009_ArrayLC        := xxcp_wks.I1009_Array;
       vBase_ArrayLC        := xxcp_wks.I1009_Array;

       I1009_CountLC        := xxcp_wks.I1009_Count;
  End Reset_Local_Arrays;

  Procedure Reset_Child_Arrays is
	  Begin
       D1001_ArrayCH        := xxcp_wks.D1001_Array;
       D1002_ArrayCH        := xxcp_wks.D1002_Array;
       D1003_ArrayCH        := xxcp_wks.D1003_Array;
       D1004_ArrayCH        := xxcp_wks.D1004_Array;
       D1005_ArrayCH        := xxcp_wks.D1005_Array;
       D1006_ArrayCH        := xxcp_wks.D1006_Array;
       D1007_ArrayCH        := xxcp_wks.D1007_Array;
       D1008_ArrayCH        := xxcp_wks.D1008_Array;
       I1009_ArrayCH        := xxcp_wks.I1009_Array;
       vBase_ArrayCH        := xxcp_wks.I1009_Array;
       I1009_CountCH        := xxcp_wks.I1009_Count;
  End Reset_Child_Arrays;

  Procedure Check_Trace_Log(cText in varchar2) is
   Begin
      If Length(xxcp_wks.trace_log) > 28000 then
        If xxcp_global.Trace_on = 'Y' then
          xxcp_wks.DumpWriteError(100,'TRACE <'||cText||'>',xxcp_wks.trace_log);
        End If;
        xxcp_wks.trace_log := Null;
      End If; 
   End Check_Trace_log;
   

  --
  --  INIT
  --
  PROCEDURE INIT IS

    c integer;
    
    Cursor DAS is
     select to_number(column_type) column_type, g.dynamic_name_id, 
            Max(column_id)-((column_type-1000)*1000) max_id
       from xxcp_dynamic_attributes g
       group by column_type, g.dynamic_name_id;
    

  BEGIN

    Max_Dynamic_Attribute := 0;
    For Rec in DAS loop
      If rec.column_type = 1001 then    
        -- Max_Configuration
        Max_Configuration := rec.max_id;
      Elsif rec.column_type = 1002 then
        -- Max_IC_Pricing
        Max_IC_Pricing    := rec.max_id;
      Elsif rec.column_type = 1003 then
        -- Max_Accounting
        Max_Accounting    := rec.max_id;
      Elsif rec.column_type = 1004 then
        -- Max_Pricing
        Max_Pricing    := rec.max_id;
      Elsif rec.column_type = 1006 then
        -- Max_Custom_Attributes
        Max_Custom_Attributes    := rec.max_id;
        Max_Column_Attribute     := rec.max_id;
      Elsif rec.column_type = 1009 then
        -- Max_Pricing
        Max_Internal_Attribute    := rec.max_id;
      End If;

      If rec.dynamic_name_id = 1 and Rec.Max_Id > Max_Dynamic_Attribute then
        Max_Dynamic_Attribute := Rec.Max_Id;
      End If;

    End Loop;
    
    If nvl(Max_Accounting,0) = 0 then
      Max_Accounting := 2;
    End If;

    --------------------------------------------------------
    -- SET CLEAR_DYNAMIC
    --------------------------------------------------------
    xxcp_wks.Clear_Dynamic.delete;
	  xxcp_wks.Clear_Dynamic.Extend(Max_Dynamic_Attribute);

	  For c in 1..xxcp_wks.Clear_Dynamic.Count Loop
	   xxcp_wks.Clear_Dynamic(c) := Null;
    End loop;
    --------------------------------------------------------
    -- SET CLEAR_INTERNAL
    --------------------------------------------------------
    xxcp_wks.Clear_Internal.delete;
	  xxcp_wks.Clear_Internal.Extend(Max_Internal_Attribute);
	  For c in 1..xxcp_wks.Clear_Internal.Count Loop
	   xxcp_wks.Clear_Internal(c) := Null;
    End loop;
    --------------------------------------------------------

	  -- Processed Records
	  xxcp_wks.SOURCE_CNT := 0;

	  xxcp_wks.WORKING_HIGH_TIDE := 0;
	  xxcp_wks.WORKING_CNT       := 0;

	  xxcp_wks.BALANCING_CNT        := 0;
	  xxcp_wks.BALANCING_HIGH_TIDE  := 0;

	  xxcp_wks.D1001_Array := xxcp_wks.Clear_Dynamic;
	  xxcp_wks.D1002_Array := xxcp_wks.Clear_Dynamic;
	  xxcp_wks.D1003_Array := xxcp_wks.Clear_Dynamic;
	  xxcp_wks.D1004_Array := xxcp_wks.Clear_Dynamic;
	  xxcp_wks.D1005_Array := xxcp_wks.Clear_Dynamic;
	  xxcp_wks.D1006_Array := xxcp_wks.Clear_Dynamic;
	  xxcp_wks.D1007_Array := xxcp_wks.Clear_Dynamic;
	  xxcp_wks.D1008_Array := xxcp_wks.Clear_Dynamic;

	  vBase_ArrayCH        := xxcp_wks.Clear_Dynamic;
	  vBase_ArrayLC        := xxcp_wks.Clear_Dynamic;
	  vBase_ArrayGL        := xxcp_wks.Clear_Dynamic;

    xxcp_wks.Source_Balancing.Delete;
	  xxcp_wks.Source_Balancing.Extend(12);

	  xxcp_wks.Source_Balancing(1) := Null;
	  xxcp_wks.Source_Balancing(2) := Null;
	  xxcp_wks.Source_Balancing(3) := Null;
	  xxcp_wks.Source_Balancing(4) := Null;
	  xxcp_wks.Source_Balancing(5) := Null;
	  xxcp_wks.Source_Balancing(6) := Null;
	  xxcp_wks.Source_Balancing(7) := Null;
	  xxcp_wks.Source_Balancing(8) := Null;
	  xxcp_wks.Source_Balancing(9) := Null;
	  xxcp_wks.Source_Balancing(10):= Null;

     -- Internal Variables
	  xxcp_wks.I1009_Count := Max_Internal_Attribute;
    xxcp_wks.I1009_Array.delete;
	  xxcp_wks.I1009_Array.Extend(Max_Internal_Attribute);

	  xxcp_wks.Clear_Internal     := xxcp_wks.I1009_Array;
	  xxcp_wks.Clear_1006_Dynamic := xxcp_wks.D1006_Array;
	  xxcp_wks.I1009_ArrayTEMP    := xxcp_wks.Clear_Internal;
	  xxcp_wks.I1009_ArrayGLOBAL  := xxcp_wks.Clear_Internal; -- CHG20090702SJS
	  xxcp_wks.I1006_ArrayTEMP    := xxcp_wks.Clear_Dynamic;
    --
	  vPrev_ArrayCH               := xxcp_wks.Clear_Dynamic;
	  -- Used in Dynamic SQL
	  xxcp_wks.DYNAMIC_RESULTS_CNT   := 0;
	  xxcp_wks.DYNAMIC_RESULTS       := xxcp_wks.Clear_Dynamic;
	  xxcp_wks.CLEAR_DYNAMIC_RESULTS := xxcp_wks.DYNAMIC_RESULTS;

	  xxcp_wks.gENABLED_SEGS_Cnt := 1;
	  xxcp_wks.gENABLED_SEGS(1)  := Null;

	  -- Account Segments
	  xxcp_wks.Account_Segments := xxcp_wks.Clear_Segments;
	  xxcp_wks.Enabled_Segments := xxcp_wks.Clear_Segments;
	  xxcp_wks.Source_Segments  := xxcp_wks.Clear_Segments;

	  xxcp_wks.BALANCING_RCD(1) := Null;

		xxcp_wks.Trace_Log           := NULL;
    xxcp_wks.Values_Log          := NULL;
		xxcp_wks.Segments_Log        := NULL;
	  xxcp_wks.ColumnAttrbutes_Log := NULL;
    -- 28-JUL-2009 KSP
    xxcp_wks.Pricing_Ladder_Log  := NULL;
    xxcp_wks.Ownership_Log       := NULL;
    xxcp_wks.Formulas_Log        := NULL;
    xxcp_wks.Pass_Through_Log    := NULL;
    -- Clear Line Count table
    gLineCount.delete; -- new

  END INIT;
	 
Begin

   Init;

END XXCP_WKS;
/
