CREATE OR REPLACE PACKAGE XXCP_REPORTING AS
  /******************************************************************************
                          V I R T A L  T R A D E R  L I M I T E D
           (Copyright 2004-2012 Virtual Trader Ltd. All rights Reserved)
  
     NAME:       XXCP_REPORTING
     PURPOSE:    This packages is called from concurrent requests and packages
   
     Version [03.02.10] Build Date [01-NOV-2012] NAME [XXCP_REPORTING]

  ******************************************************************************/
  Function Software_Version return varchar2;

  Function Get_Source_Assignment_id(cSource_Assignment_Name in varchar2) return number;

  Function Get_Trace_Mode(cUser_id in number) Return varchar2;

  Function Get_Timing_Mode(cUser_id in number) Return varchar2;

  Procedure Period_Date_Range(cPeriod_Name in varchar2,
                              cStart_Date  out Date,
                              cEnd_Date    out Date,
                              cCalendar    in varchar2 default null);

  Function Current_Period_Name(cDate       in Date,
                               cStart_Date out Date,
                               cEnd_Date   out Date,
                               cPeriodSetID in number default 1,
                               cInstanceID in number default 0) return varchar2;

  Procedure Set_of_books_Info(cSet_of_books_id      in Number,
                              cDescription          out varchar2,
                              cShort_name           out varchar2,
                              cChart_of_accounts_id out number,
                              cCurrency_code        out varchar2,
                              cPeriod_set_name      out varchar2);

  Function Get_Seconds return number;
  Function Get_Seconds_Diff(cSec1 in number, cSec2 in number) return number;

  Function Currency_Conversions(cTransaction_Currency          in varchar2
                                 ,cExchange_Currency             in varchar2
                                 ,cDefault_Common_Exch_Curr      in varchar2
                                 ,cExchange_Date                 in date
                                 ,cExchange_Type                 in varchar2
                                ) return number;

  Function Get_Period_id_details ( cPeriod_id          in number,
                                   cPeriod_Set_Name_id in number,
                                   cPeriods_Offset     in number,
                                   cInstance_id        in number,
                                   cPeriod_Name       out varchar2,
                                   cPeriod_Year       out number,
                                   cPeriod_Num        out number,
                                   cStart_Date        out date,
                                   cEnd_Date          out date,
                                   cPeriod_Type       out varchar2) return number;

  Function Get_Period_Offset ( cPeriod_id          in number,
                               cPeriod_Set_Name_id in number,
                               cPeriods_Offset     in number,
                               cInstance_id        in number) return number;

  Function Get_Period_Offset_Name
                             ( cPeriod_id          in number,
                               cPeriod_Set_Name_id in number,
                               cPeriods_Offset     in number,
                               cInstance_id        in number) return varchar2;


  Function Get_Bill_To_Address(cTax_Reg_id In Number) Return Varchar2;
 
  Function Get_Ship_To_Address(cTax_Reg_id In Number) Return Varchar2;
  
  Function Get_Legal_Text(cTax_Reg_id In Number) Return Varchar2;

  Function Get_Bill_To_Zip(cTax_Reg_id In Number) Return Varchar2;

End XXCP_REPORTING;
/
