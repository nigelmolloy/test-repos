CREATE OR REPLACE PACKAGE XXCP_CPA_FCAST_ENGINE AS
  /******************************************************************************
                      V I R T A L  T R A D E R  L I M I T E D
        (Copyright 2008-2012 Virtual Trader Ltd. All rights Reserved)

     NAME:       XXCP_CPA_FCAST_ENGINE
     PURPOSE:    Cost Plus Forecast Engine.


     Version [03.06.08A] Build Date [07-DEC-2012] Name [XXCP_CPA_FCAST_ENGINE

  ******************************************************************************/

  Function Software_Version RETURN VARCHAR2;

  Function Commit_Forecast(cSource_Group_ID    IN NUMBER,
                           cCost_Plus_Set_id   IN NUMBER DEFAULT NULL) RETURN number;
  
  Procedure Delete_FC_Tables(cSource_Assignment_id IN NUMBER,
                             cCost_Plus_Set_id     IN NUMBER DEFAULT NULL);

  Function Control(cSource_Group_id    IN NUMBER,
                   cConc_Request_Id    IN NUMBER,
                   cTable_Group_id     IN NUMBER   DEFAULT 0, 
                   cCalendar_id        IN NUMBER   DEFAULT 0,
                   cPeriod_Name        IN VARCHAR2 DEFAULT Null,
                   cRestart            IN VARCHAR2 DEFAULT 'N',
                   cCost_Plus_Set_id   IN NUMBER   DEFAULT Null) return number;

END XXCP_CPA_FCAST_ENGINE;
/
