CREATE OR REPLACE PACKAGE XXCP_AR_ENGINE AS
/******************************************************************************
                    V I R T A L  T R A D E R  L I M I T E D 
		  (Copyright 2005-2012 Virtual Trader Ltd. All rights Reserved) 

   NAME:       XXCP_AR_ENGINE 
   PURPOSE:    AR Engine 

   Version [03.06.10] Build Date [21-NOV-2012] Name [XXCP_AR_ENGINE]

******************************************************************************/

  Function Software_Version RETURN VARCHAR2;
	
  Function Control(cSource_Group_ID    IN NUMBER,
                   cConc_Request_Id    IN NUMBER,
									 cTable_Group_id     IN NUMBER   DEFAULT 0,
                   cClearDownFrequency IN NUMBER   DEFAULT 0,
                   cDebug_on           IN VARCHAR2 DEFAULT 'N'
									 ) return number;

END XXCP_AR_ENGINE;
/
