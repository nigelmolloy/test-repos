CREATE OR REPLACE PACKAGE BODY XXCP_FORMS AS
  /************************************************************************************************
                          V I R T A L  T R A D E R  L I M I T E D
              (Copyright 2003-2012 Virtual Trader Ltd. All rights Reserved)

     NAME:       XXCP_FORMS
     PURPOSE:    This functions and procedures are used by the Forms

     REVISIONS:
     Ver        Date      Author  Description
     ---------  --------  ------- -------------------------------------------------------------
     02.01.00   05/04/03  Keith   First Release
     02.01.01   11/01/04  Keith   Revised for VT 2.1
     02,01.02   01/07/04  Keith   Adjustment Validation Added
     02.01.03   31/08/04  Keith   Changed Convert_column_definition to cope with no attributes
     02.01.04   07/02/05  Keith   Added Security for forms.
     02.01.05   21/02/05  Keith   Added Execute_immediate_clob
     02.01.06   18/10/05  Keith   Added xxcp_user_profiles control
     02.03.03   05/05/06  Keith   Changed Edit_interface for external tables
     02.03.04   06/06/06  Keith   Added Write_Ed_Audit
     02.03.05   23/08/06  Keith   Format of WHO columns on xxcp_audit changed
     02.03.06   20/12/06  Keith   Added new Where Used logic and Security.
     02.03.07   28/12.06  Keith   Added Edit_Interface _V logic
     02.03.08   02/05/07  Keith   Added PV_TRX_REMOVAL
     02.03.09   21/05/07  Keith   Added refresh names/set names
     02.04.00   17/07/07  Keith   Added New Write_Edit parameters
     02.04.01   06/10/07  Keith   Increase Transaction Cache to 32K
     02.04.02   12/11/07  Keith   Change User Profile update to use 1,2,3 params.
     02.04.03   29/11/07  Keith   Added auto update to sys profile for db name
     02.04.04   15/02/08  Keith   Ensure Edit Columns do not exceed 300 char
     02.04.05   16/03/08  Keith   Added Adminstor_User_Profile
     02.04.06   21/03/08  Keith   Removed Reliance on XXCP_FOUNDATION package
     02.04.07   09/07/08  Keith   Added Refresh_Cross_Source_Mapping for Shared Pricing
     02.04.08   03/09/08  Keith   Security check now checks the responsibility you are using
     02.04.08   21/01/08  Nigel   Changed get_transaction_Chache for cost plus 
     02.04.09   02/03/09  Nigel   Fixed source_security to check user_profiles 
     02.04.10   04/03/09  Nigel   Overloaded edit_record_contents for preview mode.
     02.04.11   08/06/09  Dave    Added get_sys_profile and get_default_effectivity_dates
     02.05.00   08/07/09  Keith   added di_column_definitions
     02.05.01   08/12/09  Simon   Get_Transaction_Cache to return distinct records. Ammend Display of brackets.
     02.05.02   15/12/09  Simon   Fix DI_Column_Definition for colon attributes.
     02.05.03   27/05/10  Simon   Update Where Attribute Used Tool to reflect latest changes.
     02.05.04   18/08/10  Keith   Modify edit interface for nulls
     02.05.05   20/09/10  Nigel   Fixed an issue with overloaded edit_record_contents passing -1 for user_id.
     02.05.05A  29/06/11  Keith   Added R12 Clob fix     
     02.05.06   21/07/11  Simon   Merged File Load changes.
     02.05.07   25/08/11  Mark    Add Write_Ed_Audit_Parent.
     02.05.08   03/09/11  Keith   Addd Submit_Journal_Entry
     02.05.09   05/09/11  Mark    Bug fix to send the interface id to the index_id for auditing at parent level.
     02.05.10   09/11/11  Nigel   Added dbms_sql.execute wrapper. 
     02.05.11   17/11/11  Nigel   Added Flat File alias view functions
     02.05.12   06/12/11  Keith   Put Trucated Journal Name into vt_transaction_ref
     02.05.13   04/01/12  Keith   Added Line_id to Journal Lines 
     02.05.14   10/01/12  Mark    Added extra columns to submit journal and merge patch
     03.05.15   01/02/12  Mat     Replace ref to XXCP_AUDIT with XXCP_AUDIT_B
     03.05.16   21/02/12  Nigel   Change reference from v$reserverd_words to gv$reserverd_words
     03.05.17   07/03/12  Nigel   Removed reference to v$reserverd_words or gv$reserverd_words
                                  Created Clear_Queue_Def_Clob and Append_Queue_Def_Clob. 
     03.05.18   17/02/12  Nigel   Remove record_type predicate
     03.05.19   26/03/12  Mark    Submit_Journal_Entry now uses transaction_type for vt_transaction_type
     03.05.20   02/04/12  Keith   Added Alias to Edit Record Contents
     03.05.21   13/04/12  Simon   Overloaded Execute_Immediate_Clob
                                  Added Filter Criteria and Trim/Trunc to Excel_Insert. Line From To.
     03.05.22   27/04/12  Mark    Re-map reference fields for submit_journal_entry.
     03.06.01   25/06/12  Simon   Modify Refresh_Engines_Names request groups (OOD-157).
     03.06.02   02/07/12  Mat     Added the PUs: Clear_Queue_Def_Insert_Clob, Append_Queue_Def_Insert_Clob, Read_Queue_Def_Select_Clob, 
                                  Read_Queue_Def_Insert_Clob for reference from XCPQUEDEF
     03.06.03   09/07/12  Simon   Fix Edit Interface issue (OOD-198).
     03.06.04   11/07/12  Mat     Edit_record_Contents - qx cursor - added source_assignment_id restriction (OOD-153)
     03.06.05   20/08/12  Simon   Added Read_Errors and Read_PV_Errors (OOD-252).
     03.06.06   11/09/12  Mat     Added check to submit_journal_entry to ensure that Journal has not already been Submitted (OOD-240)
     03.06.07   27/09/12  Mark    Move the logic from the Sources form for renaming the engine to this package
     03.06.08   05/10/12  Simon   Increased Dyn Attr length from 100 to 250 (OOD-195).
     03.06.09   03/10/12  Simon   Enhance function calls on filter criteria (OOD-342).
     03.06.10   15/10/12  Mat     OOD-349 Added Create_Edit_Ref_Temp
     03.06.11   16/10/12  Simon   Read_WS_Clob, Clear_WS_Clob, Append_WS_Clob added (OOD-357).
     03.06.12   17/10/12  Mat     OOD-358 Changed alias view generation to include VT_FILE_EXPORT_ID
     03.06.13   09/11/12  Simon   Added Clob_Replace.
  ***************************************************************************************/

  gSecurity_Type_Resp varchar2(1) := 'N';

  -- *************************************************************
  --                     Software Version Control
  -- This package can be checked with SQL to ensure it's installed
  -- select packagename.software_version from dual;
  -- *************************************************************
  FUNCTION Software_Version RETURN VARCHAR2 IS
  Begin
    Return('Version [03.06.13] Build Date [09-NOV-2012] NAME [XXCP_FORMS]');
  END SOFTWARE_VERSION;

  -- **********************************************************
  --
  --        Show
  --
  -- This procedure writes to screen and ensures that the
  -- length of the string is not too high. 
  -- It is here as well as in the foundation package because 
  -- of installation processes.
  -- **********************************************************
  Procedure Show(cMessage in varchar2) is
  begin
    dbms_output.put_line(substr(cMessage, 1, 255));
  end Show;

  -- **********************************************************
  --       FndWriteError
  --
  -- This procedure writes a record to the xxcp_errors table
  -- and date stamps it with sysdate.
  -- XXCP_ERRORS now can hold LONG as the ERROR Message
  -- It is here as well as in the foundation package because 
  -- of installation processes.
  -- **********************************************************
  Procedure FndWriteError(cInternalErrorCode in Number,
                          cErrorMessage      in varchar2,
                          cLong_Message      in Long Default Null) is

    pragma autonomous_transaction;

    gSysdate date;

  Begin

    gSysdate := Sysdate;

    If cErrorMessage is not null then

      If nvl(xxcp_global.Preview_on,'N') = 'N' then
        Begin

          Insert into xxcp_errors
            (Source_Activity,
             Process_name,
             Error_Message,
             Long_Message,
             Transaction_id,
             Source_Table_id,
             Parent_Trx_id,
             Internal_Error_Code,
             Source_Assignment_id,
             Table_Group_id,
             Trx_Group_id,
             Source_Rowid,
             Error_id,
             Request_id,
             Trading_set_id,
             created_by,
             creation_date,
             last_update_login)
          Values
            (xxcp_global.gCommon(1).current_source_activity,
             xxcp_global.gCommon(1).current_Process_name,
             cErrorMessage,
             cLong_Message,
             xxcp_global.gCommon(1).current_transaction_id,
             xxcp_global.gCommon(1).current_source_table_id,
             xxcp_global.gCommon(1).current_parent_trx_id,
             cInternalErrorCode,
             xxcp_global.gCommon(1).current_assignment_id,
             nvl(xxcp_global.gCommon(1).current_table_group_id,0),
             nvl(xxcp_global.gCommon(1).current_trx_group_id,0),
             xxcp_global.gCommon(1).current_source_rowid,
             XXCP_ERRORS_SEQ.nextval,
             xxcp_global.gCommon(1).current_request_id,
             xxcp_global.gCommon(1).current_trading_set_id,
             xxcp_global.User_id,
             gSysdate,
             xxcp_global.Login_id);

          Exception WHEN OTHERS then Null;

        End;
      Else
        Begin

          -- Preview errors
          Insert into xxcp_pv_errors
            (Preview_id,
             Source_Activity,
             Process_name,
             Error_Message,
             Long_Message,
             Transaction_id,
             Source_Table_id,
             Parent_Trx_id,
             Internal_Error_Code,
             Source_Assignment_id,
             Table_Group_id,
             Trx_Group_id,
             Source_Rowid,
             Error_id,
             Request_id,
             Trading_set_id ,
             created_by,
             creation_date,
             last_update_login)
          values
            (xxcp_global.gCommon(1).Preview_id,
             xxcp_global.gCommon(1).current_source_activity,
             xxcp_global.gCommon(1).current_Process_name,
             cErrorMessage,
             cLong_message,
             xxcp_global.gCommon(1).current_transaction_id,
             xxcp_global.gCommon(1).current_source_table_id,
             xxcp_global.gCommon(1).current_parent_trx_id,
             cInternalErrorCode,
             xxcp_global.gCommon(1).current_assignment_id,
             nvl(xxcp_global.gCommon(1).current_table_group_id,0),
             nvl(xxcp_global.gCommon(1).current_trx_group_id,0),
             xxcp_global.gCommon(1).current_source_rowid,
             XXCP_ERRORS_SEQ.nextval,
             xxcp_global.gCommon(1).current_request_id,
             xxcp_global.gCommon(1).current_trading_set_id,
             xxcp_global.User_id,
             gSysdate,
             xxcp_global.Login_id);

          Exception WHEN OTHERS then Null;

        End;
      End If;

      Commit;

    End If;
  End FndWriteError;

  -- **********************************************************
  --    Set_InternalErrorCode
  --
  -- This procedure only sets the Internal Error code if it is
  -- not already set.
  -- **********************************************************
  Procedure Set_InternalErrorCode(cInternalErrorCode in out number,
                                  cRecentErrorCode   in number default 0) is
  Begin
    If nvl(cInternalErrorCode, 0) = 0 then
      cInternalErrorCode := cRecentErrorCode;
    End If;
  End;

  -- *************************************************************
  --
  --      Get_Attribute_Name
  --
  -- *************************************************************
  Function Get_Attribute_Name(cColumn_id in number, cColumn_Type in number) Return varchar2 is

    Cursor AttName(pColumn_id in number) is
      select Column_name Attribute_name
        from XXCP_DYNAMIC_ATTRIBUTES d
       where d.column_id = pColumn_id
         and decode(d.source_id, 0, xxcp_global.get_source_id,d.source_id) = xxcp_global.get_source_id;

    vResult xxcp_dynamic_attributes.column_name%type;

  Begin
    For AttRec in AttName(cColumn_id) Loop
      vResult := AttRec.Attribute_name;
    End Loop;
    Return(vResult);
  End Get_Attribute_Name;

   -- *************************************************************
  --
  --    Get_Attribute_Name_All
  --
  -- *************************************************************
  Function Get_Attribute_Name_All(cDynamic_Name_id in number,
                                  cColumn_id       in number,
                                  cColumn_Type     in number) return varchar2 is

    Cursor AttName(pDynamic_Name_id in number, pColumn_id in number, pColumn_Type in number) is
      select x.Attribute_name
        from xxcp_dynamic_attributes_v x
       where x.dynamic_name_id = pDynamic_Name_id
         and x.activity_id     = pColumn_Type
         and x.id              = pColumn_id;

    vResult xxcp_dynamic_attributes.column_name%type;

  begin

    For AttRec in AttName(cDynamic_Name_id, cColumn_id, cColumn_Type) loop
      vResult := AttRec.Attribute_name;
    End Loop;
    Return(vResult);

  End Get_Attribute_Name_ALL;


  -- *************************************************************
  --                           IsNumber
  --                   -----------------------------
  -- *************************************************************
  Function IsNumber(cStr in varchar2) return varchar2 is
    vResult Char := 'N';
    vNum    Number;

   Begin
      Begin
        vNum := to_number(cStr);
        vResult := 'Y';
        Exception when OTHERS then vResult := 'N';
      End;
      Return(vResult);
   End IsNumber;

  -- *************************************************************
  --                           IsDate
  -- *************************************************************
  Function IsDate(cStr in varchar2) return varchar2 is
    vResult Char := 'N';
    vNum    Date;

   Begin
      Begin
        vNum := to_date(cStr);
        vResult := 'Y';
        Exception when OTHERS then vResult := 'N';
      End;
      Return(vResult);
   End IsDate;

  -- *************************************************************
  --                         SetDate
  -- *************************************************************
  Procedure SetDate(cDateName in varchar2, cDate in date) is
   Begin
      If Substr(cDateName,1,1) = 'S' then
       gStart_Date := cDate;
      Else
       gEnd_Date := cDate;
      End If;
   End SetDate;

  -- *************************************************************
  --                         GetDate
  -- *************************************************************
  Function GetDate(cDateName in varchar2, cDefaultDate in date) return date is
    vDate date;
   Begin
      If Substr(cDateName,1,1) = 'S' then
       vDate := nvl(gStart_Date,cDefaultDate);
      Else
       vDate := nvl(gEnd_Date,cDefaultDate);
      End If;
    return(vDate);
   End GetDate;
  -- *************************************************************
  --                     Adjustment Comparision
  --                   -----------------------------
  -- This function check to see if the adjustment rate is in
  -- conflict with any other adjustment rate.
  -- *************************************************************
  FUNCTION Adjustment_Comparision(cNew_Rate_Set_id         in number,
                                  cNew_Owner               in varchar2,
                                  cNew_Partner             in varchar2,
                                  cNew_Qualifier1          in varchar2,
                                  cNew_Qualifier2          in varchar2,
                                  cNew_Effective_from_Date in date,
                                  cNew_Effective_to_Date   in date,
                                  cOld_Rate_Set_id         in number,
                                  cOld_Owner               in varchar2,
                                  cOld_Partner             in varchar2,
                                  cOld_Qualifier1          in varchar2,
                                  cOld_Qualifier2          in varchar2,
                                  cOld_Effective_from_Date in date,
                                  cOld_Effective_to_Date   in date,
                                  cInternalErrorCode       in out Number)
    RETURN BOOLEAN IS

    vNew_Start Date;
    vNew_End   Date;
    vOld_Start Date;
    vOld_End   Date;

    vValid     Boolean := False;

  Begin

    vNew_Start := nvl(trunc(cNew_Effective_from_Date), '01-JAN-1990');
    vNew_End   := nvl(trunc(cNew_Effective_to_Date), '31-DEC-2999');

    vOld_Start := nvl(trunc(cOld_Effective_from_Date), '01-JAN-1990');
    vOld_End   := nvl(trunc(cOld_Effective_to_Date), '31-DEC-2999');

    If ((nvl(cNew_Rate_Set_id, 0) = nvl(cOld_Rate_Set_id, 0)) and
       (nvl(cNew_Owner, '~null~') = nvl(cOld_Owner, '~null~')) and
       (nvl(cNew_Partner, '~null~') = nvl(cOld_Partner, '~null~')) and
       (nvl(cNew_Qualifier1, '~null~') = nvl(cOld_Qualifier1, '~null~')) and
       (nvl(cNew_Qualifier2, '~null~') = nvl(cOld_Qualifier2, '~null~'))) then

      If vNew_Start between vOld_Start and vOld_End then
        vValid             := TRUE;
        cInternalErrorCode := 1081;
      End If;

      If vNew_End between vOld_Start and vOld_End then
        vValid             := TRUE;
        cInternalErrorCode := 1082;
      End If;

      Return(vValid);

    Else
      Return(False);
    End If;
  END Adjustment_Comparision;

  -- *************************************************************
  --                     Adjustment Validation
  -- This function check to see if the adjustment rate is in
  -- conflict with any other adjustment rate.
  -- *************************************************************
  FUNCTION Adjustment_Validation(cRate_Set_id         in number,
                                 cAdjustment_Rate_id  in number,
                                 cOwner               in varchar2,
                                 cPartner             in varchar2,
                                 cQualifier1          in varchar2,
                                 cQualifier2          in varchar2,
                                 cEffective_from_Date in date,
                                 cEffective_to_Date   in date,
                                 cInternalErrorCode   in out Number)
    RETURN NUMBER IS

    Cursor VX(pRate_Set_id in number, pAdjustment_Rate_Id in number, pOwner in varchar2, pPartner in varchar2, pQualifier1 in varchar2, pQualifier2 in varchar2) is
      Select r.ADJUSTMENT_RATE_ID,
             trunc(nvl(r.EFFECTIVE_FROM_DATE, '01-JAN-1990')) From_Date,
             trunc(nvl(r.EFFECTIVE_TO_DATE, '31-DEC-2999')) To_Date
        from xxcp_adjustment_rates r
       where rate_set_id = pRate_Set_id
         and nvl(owner, '~null~') = nvl(pOwner, '~null~')
         and nvl(partner, '~null~') = nvl(pPartner, '~null~')
         and nvl(ar_qualifier1, '~null~') = nvl(pQualifier1, '~null~')
         and nvl(ar_qualifier2, '~null~') = nvl(pQualifier2, '~null~')
         and Adjustment_Rate_Id <> pAdjustment_rate_id;

    vStart Date;
    vEnd   Date;

    vValid            boolean := False;
    vMatching_Rate_id Number := 0;

  Begin

    vStart := nvl(cEffective_from_Date, '01-JAN-1990');
    vEnd   := nvl(cEffective_to_Date, '31-DEC-2999');

    If vStart > vEnd then
      cInternalErrorCode := 1080;

      vMatching_Rate_id  := -1;
    Else

      For vxRec in vx(cRate_Set_id,
                      cAdjustment_Rate_id,
                      cOwner,
                      cPartner,
                      cQualifier1,
                      cQualifier2) Loop

        If vStart between vxRec.From_Date and vxRec.To_Date then
          vMatching_Rate_id  := vxrec.adjustment_Rate_id;
          vValid             := True;
          cInternalErrorCode := 1081;
        End If;

        If vEnd between vxRec.From_Date and vxRec.To_Date then
          vMatching_Rate_id  := vxrec.adjustment_Rate_id;
          vValid             := True;
          cInternalErrorCode := 1082;
        End If;

      End Loop;
    End If;
    Return(vMatching_Rate_id);
  END Adjustment_Validation;

  --
  -- MODEL_CTL_VALIDATION
  --
  Function Model_Ctl_Validation( cInsert_or_Update      in varchar2,
                                 cSource_id             in number,
                                 cModel_Ctl_id          in number,
                                 cEntity_Partnership_id in number,
                                 cTransaction_Set_id    in number,
                                 cModel_Name_id         in number,
                                 cTrading_Set_id        in Number,
                                 cQualifier1            in varchar2,
                                 cQualifier2            in varchar2,
                                 cQualifier3            in varchar2,
                                 cQualifier4            in varchar2,
                                 cTrx_Qualifier1        in varchar2,
                                 cTrx_Qualifier2        in varchar2,
                                 cEffective_from_Date   in date,
                                 cEffective_to_Date     in date,
                                 cInternalErrorCode     in out Number)
    return Number is

    Cursor VX(pInsert_or_Update in varchar2,
              pModel_Ctl_id     in number,
              pSource_id        in number,
              pEP_ID            in number,
              pTS_ID            in number,
              pModel_Name_id    in number,
              pTrading_Set_id   in number,
              --
              pQualifier1       in varchar2,
              pQualifier2       in varchar2,
              pQualifier3       in varchar2,
              pQualifier4       in varchar2,
              pTrx_Qualifier1   in varchar2,
              pTrx_Qualifier2   in varchar2
    ) is
      Select r.model_ctl_id,
             trunc(nvl(r.effective_from_date, '01-JAN-1990')) From_Date,
             trunc(nvl(r.effective_to_date, '31-DEC-2999')) To_Date
        from xxcp_model_ctl r
       where r.Entity_Set_id              = pEP_ID
         and r.Source_id                  = pSource_id
         and r.Transaction_Set_id         = pTS_ID
         and r.Model_Name_id              = pModel_Name_id
         and r.Trading_set_id             = pTrading_Set_id
         and nvl(mc_qualifier1, '~null~') = nvl(pQualifier1, '~null~')
         and nvl(mc_qualifier2, '~null~') = nvl(pQualifier2, '~null~')
         and nvl(mc_qualifier3, '~null~') = nvl(pQualifier3, '~null~')
         and nvl(mc_qualifier4, '~null~') = nvl(pQualifier4, '~null~')
         and nvl(trx_qualifier1, '~null~') = nvl(pTrx_Qualifier1, '~null~')
         and nvl(trx_qualifier2, '~null~') = nvl(pTrx_Qualifier2, '~null~')
         and r.Model_ctl_id <> Decode(pInsert_or_Update,'I',-1,pModel_ctl_id);

    vStart Date;
    vEnd   Date;

    vValid            boolean := False;
    vMatching_Rate_id Number := 0;

  Begin

    vStart := nvl(cEffective_from_Date, '01-JAN-1990');
    vEnd   := nvl(cEffective_to_Date, '31-DEC-2999');

    If vStart > vEnd then
      cInternalErrorCode := 1080;

      vMatching_Rate_id  := -1;
    Else

      For vxRec in vx(cInsert_or_Update,
                      cModel_Ctl_id,
                      cSource_id   ,
                      cEntity_Partnership_id ,
                      cTransaction_Set_id    ,
                      cModel_Name_id         ,
                      cTrading_Set_id        ,
                      cQualifier1            ,
                      cQualifier2            ,
                      cQualifier3            ,
                      cQualifier4            ,
                      cTrx_Qualifier1        ,
                      cTrx_Qualifier2) Loop

        If vStart between vxRec.From_Date and vxRec.To_Date then
          vMatching_Rate_id  := vxrec.model_ctl_id;
          vValid             := True;
          cInternalErrorCode := 1081;
        End If;

        If vEnd between vxRec.From_Date and vxRec.To_Date then
          vMatching_Rate_id  := vxrec.model_ctl_id;
          vValid             := True;
          cInternalErrorCode := 1082;
        End If;

      End Loop;
    End If;
    Return(vMatching_Rate_id);
  End Model_Ctl_Validation;

  -- *************************************************************
  --                     Tax Code Validation
  -- This function check to see if the tax code is in
  -- conflict with any other adjustment rate.
  -- *************************************************************
  FUNCTION Tax_Code_Validation(cIC_Trad_Tax_Id      in number,
                               cOwner_tax_reg_id    in number,
                               cPartner_tax_reg_id  in number,
                               cTax_Qualifier1      in varchar2,
                               cTax_Qualifier2      in varchar2,
                               cAR_tax_code_id      in number,
                               cAP_tax_code_id      in number,
                               cEffective_from_date in date,
                               cEffective_to_date   in date,
                               cInternalErrorCode   in out Number)
    RETURN NUMBER IS

    Cursor VX(pIC_Trad_Tax_Id in number, pOwner_tax_reg_id in number, pPartner_tax_reg_id in number, pAR_tax_code_id in number
            , pAP_tax_code_id in number, pTax_Qualifier1 in varchar2, pTax_Qualifier2 varchar2) is
      Select r.IC_Trad_Tax_Id,
             trunc(nvl(r.EFFECTIVE_FROM_DATE, '01-JAN-1990')) From_Date,
             trunc(nvl(r.EFFECTIVE_TO_DATE, '31-DEC-2999')) To_Date
        from xxcp_tax_codes r
       where nvl(Owner_tax_reg_id, 0) = nvl(pOwner_tax_reg_id, 0)
         and nvl(partner_tax_reg_id, 0) = nvl(pPartner_tax_reg_id, 0)
         and nvl(r.TAX_QUALIFIER1,'~Null~') = nvl(pTax_Qualifier1,'~Null~')
         and nvl(r.TAX_QUALIFIER2,'~Null~') = nvl(pTax_Qualifier2,'~Null~')
         and nvl(AR_tax_code_id, 0) = nvl(pAR_tax_code_id, 0)
         and nvl(AP_tax_code_id, 0) = nvl(pAP_tax_code_id, 0)
         and IC_Trad_Tax_Id <> pIC_Trad_Tax_Id;

    vStart Date;
    vEnd   Date;

    vMatching_Rate_id Number := 0;

  Begin

    vStart := nvl(cEffective_from_Date, '01-JAN-1990');
    vEnd   := nvl(cEffective_to_Date, '31-DEC-2999');

    If vStart > vEnd then
      cInternalErrorCode := 1085;
      vMatching_Rate_id  := -1;
    Else

      For vxRec in vx(cIC_Trad_Tax_Id,
                      cOwner_tax_reg_id,
                      cPartner_tax_reg_id,
                      cAR_tax_code_id,
                      cAP_tax_code_id,
                      cTax_Qualifier1,
                      cTax_Qualifier2) Loop

        If vStart between vxRec.From_Date and vxRec.To_Date then
          vMatching_Rate_id  := vxrec.IC_Trad_Tax_Id;
          cInternalErrorCode := 1086;
        End If;

        If vEnd between vxRec.From_Date and vxRec.To_Date then
          vMatching_Rate_id  := vxrec.IC_Trad_Tax_Id;
          cInternalErrorCode := 1087;
        End If;

      End Loop;
    End If;
    Return(vMatching_Rate_id);
  END Tax_Code_Validation;

  Procedure COA_Structure(cCOA_ID      in number,
                          cInstance_id in number,
                          cMaxSegments out number,
                          cSegmentMap  out varchar2
                          ) is


  vMaxSegments   number(2) := 0;
  vEnSegments    varchar2(30);
  k              integer := 0;

 Begin
    -- Not used 
    vEnSegments := 'NNNNNNNNNNNNNNNNNNNNNNNNNNNNNN';
  End COA_Structure;

  -- *************************************************************
  --                     Online Help
  -- *************************************************************
  FUNCTION Online_Help(cWeb_page in varchar2,
                       cProfile  in varchar2 default Null) RETURN VARCHAR2 IS

    vHelpPage varchar2(600) := 'http://www.virtualtradet.net/support/';

    Cursor c1 is
      Select meaning HelpPage
        from xxcp_lookups
       where lookup_type = 'ONLINE HELP'
         and lookup_code = 'DEFAULT'
         and Decode(cProfile, Null, '1', lookup_type_subset) =
             nvl(cProfile, '1');
  BEGIN
    For Rec in C1 loop
      vHelpPage := Rec.HelpPage;
    End loop;

    If cWeb_Page is not null then
      vHelpPage := vHelpPage || cWeb_Page;
    Else
      vHelpPage := vHelpPage || 'index.html';
    End If;

    Return(vHelpPage);
  END Online_Help;

  -- *************************************************************
  --                    Add Sys Profile
  -- *************************************************************
  PROCEDURE Add_Sys_Profile is

   cursor qt is
   select max(profile_value)+1 MaxId
     from xxcp_sys_profile
    where profile_category = 'DB';

   cursor dn is
   select count(*) cnt
    from xxcp_sys_profile p
   where profile_category = 'DB'
     and p.PROFILE_NAME = (select name from v$database);

   vCnt    Number;
   vNextId Number := 1;

  Begin

    -- Check to see if database already in file
    For rec in dn loop
      vCnt := Rec.cnt;
    End Loop;

    If vCnt = 0 then
       -- find next id
       For rec in qt loop
         vNextId := rec.maxid;
       End Loop;
       -- Create Record
       Insert into xxcp_sys_profile p(
        PROFILE_NAME,PROFILE_VALUE,PROFILE_CATEGORY,MANDATORY,PREFILLED,DATA_TYPE,
        CREATED_BY,CREATION_DATE,LAST_UPDATED_BY,LAST_UPDATE_DATE,LAST_UPDATE_LOGIN
        )
       Select d.name, nvl(vNextId,1), 'DB',null,'N','C',-1,sysdate,-1,sysdate,-1 from v$database d;

       Commit;

    End If;
  End Add_Sys_Profile;

  -- *************************************************************
  --                    Get_Interface_Security
  -- *************************************************************
  Function Get_Interface_Security(cSource_id in number, cInterface_Table in varchar2, cUser_id in number) return varchar2 is

    cursor c1 (pSource_id in number, pInterface_Table in varchar2, pUser_id in number) is
      select es.security_id
        from xxcp_edit_user_sets es,
             xxcp_edit_security_names_v n
       where es.security_id = n.edit_security_id
         and n.source_id         = pSource_id
         and n.source_base_table = pInterface_table
         and es.user_id          = pUser_id
         and es.usage            = 'S';

    cursor c2 (pParent_Security_id in number) is
      select c.edit_security_id, Decode(substr(c.data_type,1,1),'V','Y','N') char_field, c.COLUMN_NAME
        from xxcp_edit_security_cols_v c
       where c.parent_security_id = pParent_Security_id;

    cursor c3 (pParent_Security_id in number, pCF in varchar2) is
      select decode(pCF,'Y','''',Null)||v.column_value||decode(pCF,'Y','''',Null) value_name
        from xxcp_edit_security_vals_v v
       where v.parent_security_id = pParent_Security_id;

  vStr  varchar2(32000);
  vCols varchar2(16000);
  a     Number := 0;
  b     Number := 0;

  Begin

    For Rec1 in C1(cSource_id, cInterface_Table, cUser_id) Loop

      If a > 0 then
        vStr := vStr ||' OR (';
      Else
        vStr := 'AND (';
        a := 1;
      End If;

      b := 0;

      For Rec2 in C2(Rec1.Security_id) Loop
        vCols := Null;

        For Rec3 in C3(Rec2.edit_security_id, Rec2.char_field) Loop
          vCols := vCols ||Rec3.value_name||',';
        End Loop;

        If vCols is not null then
          If b = 0 then
            vStr := vStr||' '||Rec2.column_name||' IN ('||substr(vCols,1,Length(vCols)-1)||')';
          Else
            vStr := vStr||' AND '||Rec2.column_name||' IN ('||substr(vCols,1,Length(vCols)-1)||')';
          End If;
        End If;

        b := 1;

      End Loop;

       vStr := vStr ||')';

    End Loop; -- c1

    vStr := ltrim(rtrim(vStr));

    Return(vStr);

  End Get_Interface_Security;

  -- Show
  Function Show_Interface_Security(cSecurity_Name in varchar2) return varchar2 is

    cursor c1 (pSecurity_Name in varchar2) is
      select n.edit_security_id
        from xxcp_edit_security_names_v n
       where n.SECURITY_NAME     = pSecurity_Name;

    cursor c2 (pParent_Security_id in number) is
      select c.edit_security_id, Decode(substr(c.data_type,1,1),'V','Y','N') char_field, c.COLUMN_NAME
        from xxcp_edit_security_cols_v c
       where c.parent_security_id = pParent_Security_id
       order by c.column_name;

    cursor c3 (pParent_Security_id in number, pCF in varchar2) is
      select decode(pCF,'Y','''',Null)||v.column_value||decode(pCF,'Y','''',Null) value_name
        from xxcp_edit_security_vals_v v
       where v.parent_security_id = pParent_Security_id;

  vStr  varchar2(32000);
  vCols varchar2(16000);

  Begin

    For Rec1 in C1(cSecurity_Name) Loop

      For Rec2 in C2(Rec1.edit_security_id) Loop
        vCols := Null;

        For Rec3 in C3(Rec2.edit_security_id, Rec2.char_field) Loop
          vCols := vCols||Rec3.value_name||',';
        End Loop;

        If vCols is not null then
          vStr := vStr||' AND '||Rec2.column_name||' IN ('||substr(vCols,1,Length(vCols)-1)||')';
        End If;

      End Loop;

    End Loop; -- c1

    vStr := ltrim(rtrim(vStr));

    Return(vStr);

  End Show_Interface_Security;

  -- *************************************************************
  --                     GET_RECORD_CONTENTS
  --  This procedure gets the contents of a record in a table
  --  that has a vt_transaction_id column.
  -- *************************************************************
  Procedure Get_Record_Contents(cTransaction_Table in varchar2,
                                cVT_transaction_id in number,
                                cAll_Data_Columns  in varchar2,
                                cResults           out varchar2,
                                cInternalErrorCode in out number) IS

    vColumn_List       varchar2(32000);
    vInternalErrorCode Integer := 0;

    Cursor cl(p_table_name in varchar2) is
      select column_name
        from xxcp_table_columns
       where table_name = p_table_name
       order by column_name;

    w integer := 0;
    g integer := 0;

    vColumn_Results XXCP_DYNAMIC_ARRAY := XXCP_DYNAMIC_ARRAY(' ');
    vColumn_Array   XXCP_DYNAMIC_ARRAY := XXCP_DYNAMIC_ARRAY(' ');

    --
    v_cursorId integer;
    v_dummy    integer;

    v_selectsmt varchar2(32000);
    vSQLERRM    varchar2(512);
    j           integer := 0;

    vResults_Str varchar2(32000);

  BEGIN

    vColumn_Results.Delete;
    vColumn_Array.Delete;

    for clRec in cl(cTransaction_Table) loop
      vColumn_list := vColumn_list || clRec.column_name || ',';
      g            := g + 1;

      vColumn_Results.extend(1);
      vColumn_Array.extend(1);

      vColumn_Array(g) := clRec.column_name;

    end loop;
    j            := length(vColumn_list) - 1;
    vColumn_list := substr(vColumn_list, 1, j);

    v_selectsmt := 'select ' || vColumn_list || ' from ' ||
                   cTransaction_Table || ' where vt_transaction_id = :M1';

    Begin
      -- Fetch Rows

      v_cursorid := DBMS_SQL.Open_Cursor;
      DBMS_SQL.Parse(v_cursorId, v_selectsmt, DBMS_SQL.Native);

      DBMS_SQL.Bind_Variable(v_cursorId, ':M1', cVT_Transaction_id);

      vInternalErrorCode := 1010;

      for w in 1 .. g loop

        DBMS_SQL.Define_Column(v_cursorid, w, vColumn_Results(w), 150);

      end loop;

      v_dummy := DBMS_SQL.Execute(v_cursorid);

      loop
        If DBMS_SQL.Fetch_Rows(v_cursorid) = 0 then
          exit;
        End if;
        vInternalErrorCode := 0;
        for w in 1 .. g loop
          DBMS_SQL.Column_Value(v_cursorid, w, vColumn_Results(w));
        end loop;

      End Loop;
      DBMS_SQL.Close_Cursor(v_CursorId);

    Exception
      when OTHERS then
        vSQLERRM := SQLERRM; -- New to trap Oracle's Reason for not working.
        Set_InternalErrorCode(vInternalErrorCode, 1009); -- Change to Say BAD SQL
        If DBMS_SQl.IS_OPEN(v_CursorId) then
          DBMS_SQL.CLOSE_CURSOR(v_CursorId);
        End If;

    End;

    If vInternalErrorCode = 0 then
      for w in 1 .. g loop
        If ((vColumn_Results(w) is not null) or (cAll_Data_Columns = 'Y')) then
          vResults_Str := vResults_Str || vColumn_Array(w) || ' <' ||
                          vColumn_Results(w) || '>' || Chr(10);
        end If;
        cResults := vResults_Str;
      end loop;

    Else
      Show(to_char(vInternalErrorCode) || vSQLERRM);
      FndWriteError(vInternalErrorCode,vSQLERRM,v_selectsmt);
    end If;

    cInternalErrorCode := vInternalErrorCode;

  END Get_Record_Contents;

  -- *************************************************************
  --                     GET_RECORD_CONTENTS
  -- *************************************************************
  Procedure Get_Record_Contents(cTransaction_Table in varchar2,
                                cvt_transaction_id in number,
                                cResults           out varchar2,
                                cInternalErrorCode in out number) is

  BEGIN
    Get_Record_Contents(cTransaction_Table,
                        cvt_transaction_id,
                        'Y',
                        cResults,
                        cInternalErrorCode);

  END Get_Record_Contents;

  -- *************************************************************
  --                     Edit_Record_Contents
  -- *************************************************************
  Procedure Edit_Record_Contents(cTable_Name           in varchar2,
                                 cTransaction_table    in varchar2,
                                 cTransaction_type     in varchar2,
                                 cTransaction_id       in number,
                                 cSource_Rowid         in rowid,
                                 cForm_Context         in varchar2,
                                 cInterface_id         in varchar2,
                                 -- 02.04.10
                                 cPreview_id           in number,
                                 cData_Only            in varchar2,
                                 cSource_id            in number,
                                 cSource_Assignment_id in number,
                                 cUser_id              in number default -1,
                                 cLogin_id             in number default -1,
                                 cInternalErrorCode in out number) IS

    vInternalErrorCode Integer := 0;
    vColumn_List       varchar2(32000);
    vSource_Rowid      Rowid;
    -- 02.04.10
    vPreview_table_name varchar2(30) := 'XXCP_PV_INTERFACE';
    vTable_name         varchar2(30);
    vUser_id            number(15);

    Cursor cl(pTable_name in varchar2, pInstance_id in number, pSource_id in number) is
        select distinct t.column_name, Decode(t.data_type,'DATE',11,t.data_length) data_length, t.data_type, t.required, t.instance_id,
             q.field_name description , q.qc_id
        from xxcp_table_columns t,
             xxcp_sys_qual_context q
       where t.table_name  = pTable_name
         and t.instance_id = pinstance_id
         and t.column_name = q.field_name(+)
         and t.table_name  = q.qualification_name(+)
         and NOT t.column_name = any(select distinct ec.column_name
                                      from xxcp_edit_user_sets     up,
                                           xxcp_edit_columns       ec,
                                           xxcp_edit_profiles      ep,
                                           xxcp_edit_tables_v    s
                                      where up.user_id          = fnd_global.user_id
                                        and up.profile_id       = ec.edit_profile_id
                                        and up.profile_id       = ep.edit_profile_id
                                        and up.usage            = 'P'
                                        and ep.source_id        = s.source_id
                                        and s.source_base_table = ep.table_name
                                        and s.SOURCE_BASE_TABLE = t.table_name
                                        and up.source_id        = pSource_id
                                        and (nvl(ep.Active,'Y')  = 'Y'
                                        and nvl(ec.Hide_column,'N') = 'Y'))
       -- 02.04.10
       -- if called in preview mode then find all columns on interface
       -- that exist on the preview interface.
       and (1 = case when cPreview_id is not null then
                  (select 1
                     from all_tab_cols p
                    where p.column_name = t.column_name
                      and p.table_name = vPreview_table_name
                      and p.owner = 'XXCP') 
                 else 
                   1 
                 end)                                        
       order by column_name;

     Cursor qx(pQC_ID in number, pForm_Context in varchar2, pSource_Assignment_id in number) is
     select nvl(c.description,sc.field_name) description
       from xxcp_qual_context c,
            xxcp_sys_qual_context sc
      where sc.qc_id               = pQC_id
        and sc.qc_id               = c.qc_id(+)
        and sc.USAGE               = 'E'
        and c.form_context         in (pForm_Context,'*')
        AND c.source_assignment_id = pSource_Assignment_id        
      order by c.form_context desc;

     Cursor Secur(pTable_name in varchar2, pColumn_Name in varchar2, pSource_id in number, pUser_id in number) is
     select Distinct ec.column_name
          from xxcp_edit_user_sets up,
               xxcp_edit_columns       ec,
               xxcp_edit_profiles      ep,
               xxcp_edit_tables_v    s
          where up.user_id          = pUser_id
            and ec.column_name      = pColumn_Name
            and up.usage            = 'P'
            and up.profile_id       = ec.edit_profile_id
            and up.profile_id       = ep.edit_profile_id
            and ep.source_id        = s.source_id
            and s.source_base_table = ep.table_name
            and s.source_base_table = pTable_name
            and up.source_id        = pSource_id
            and nvl(ep.ACTIVE,'Y')  = 'Y'
            and (nvl(ec.hide_column,'N') != 'Y'
                 or nvl(ec.edit_column,'N') = 'Y');
                 
     -- 02.05.07                 
     Cursor tsal(pSource_id in number, pTransaction_Table in varchar2, pTransaction_Type in varchar2, pColumn_Name in varchar2) is
      select a.source_id, a.transaction_set_id, a.table_name, a.column_name, a.user_alias, t.transaction_table, y.type
        from xxcp_table_column_alias a,
             xxcp_sys_source_tables  t,
             xxcp_sys_source_types   y
       where t.source_table_id    = y.source_table_id
         and t.source_id          = pSource_id
         and a.column_name        = pColumn_Name
         and y.transaction_set_id = a.transaction_set_id
         and t.transaction_table  = pTransaction_table
         and y.type = pTransaction_Type;                  

    w integer := 0;
    g integer := 0;

    vColumn_Results  XXCP_DYNAMIC_ARRAY_LONG := XXCP_DYNAMIC_ARRAY_LONG(' ');
    vColumn_Names    XXCP_DYNAMIC_ARRAY := XXCP_DYNAMIC_ARRAY(' ');
    vColumn_Length   XXCP_DYNAMIC_ARRAY := XXCP_DYNAMIC_ARRAY(' ');
    vColumn_Type     XXCP_DYNAMIC_ARRAY := XXCP_DYNAMIC_ARRAY(' ');
    vColumn_Req      XXCP_DYNAMIC_ARRAY := XXCP_DYNAMIC_ARRAY(' ');
    vColumn_Desc     XXCP_DYNAMIC_ARRAY := XXCP_DYNAMIC_ARRAY(' ');
    vColumn_QC_ID    XXCP_DYNAMIC_ARRAY := XXCP_DYNAMIC_ARRAY(' ');
    vColumn_Security XXCP_DYNAMIC_ARRAY := XXCP_DYNAMIC_ARRAY(' ');

    --
    v_CursorId  integer;
    v_dummy     integer;

    v_Selectsmt varchar2(32000);
    vSQLERRM    varchar2(512);

    vDescription varchar2(400);
    vIntSecStr   varchar2(32000);
    vSource_id   number(15);
    vSecurityOn  varchar2(1) := 'Y';

   Cursor SA(pSource_Assignment_id in number) is
    Select source_id
    from xxcp_source_assignments
    where source_assignment_id = pSource_Assignment_id;

  vColumn_size number(4);

  BEGIN

    vUser_Id  := fnd_global.user_id;
    If vUser_id = -1 then
      vUser_id := cUser_id;
    End If;
    
    -- 02.04.10
    -- if in preview mode the substitute the real interface table for the preview interface table.
    if cPreview_id is not null then
      vTable_name := vPreview_table_name;
    else
      vTable_name := cTable_name;
    end if;

    For Rec in SA(cSource_Assignment_id) loop
      vSource_id := Rec.Source_id;
    End Loop;

    vColumn_Results.Delete;
    vColumn_Names.Delete;
    vColumn_Length.Delete;
    vColumn_Type.Delete;
    vColumn_Req.Delete;
    vColumn_Desc.Delete;
    vColumn_QC_ID.Delete;
    vColumn_Security.Delete;

    -- Remove previous data
    Begin
      Delete from xxcp_edit_interface where user_id = vUser_id;
      commit;

      Exception when OTHERS then null;
    End;
    
    dbms_output.put_line(cTable_Name||':'||cSource_id||':'||vUser_Id);

    -- Populate table structure details
    For clRec in cl(cTable_Name, 0, cSource_id) loop

      If clRec.Data_length > 300 then
        vColumn_size := 300;
      Else
        vColumn_size := clRec.Data_length;
      End If;

      If g = 0 then
        vColumn_list := 'substr('||clRec.column_name||',1,'||to_char(vColumn_size)||')';
      Else
        vColumn_list := vColumn_list||',substr('||clRec.column_name||',1,'||to_char(vColumn_size)||')';
      End If;

      g  := g + 1;

      vColumn_Results.extend(1);
      vColumn_Names.extend(1);
      vColumn_Length.extend(1);
      vColumn_Type.extend(1);
      vColumn_Req.extend(1);
      vColumn_Desc.extend(1);
      vColumn_QC_ID.extend(1);
      vColumn_Security.extend(1);

      vColumn_Names(g)    := clRec.Column_name;
      vColumn_Length(g)   := clRec.Data_length;
      vColumn_Type(g)     := clRec.Data_type;
      vColumn_Req(g)      := clRec.Required;
      vColumn_Desc(g)     := clRec.Description;
      vColumn_QC_ID(g)    := clRec.QC_ID;
      vColumn_Security(g) := 0;

    End Loop;

    -- Add Rowid
    Begin
      vColumn_Results.extend(1);
      vColumn_Names.extend(1);
      vColumn_Length.extend(1);
      vColumn_Type.extend(1);
      vColumn_Req.extend(1);
      vColumn_Desc.extend(1);
      vColumn_QC_ID.extend(1);
      vColumn_Security.extend(1);
      g := g + 1;

      vColumn_Names(g)    := 'SOURCE ROWID';
      vColumn_Length(g)   := 18;
      vColumn_Type(g)     := 'ROWID';
      vColumn_Req(g)      := 'N';
      vColumn_Desc(g)     := 'SOURCE ROWID';
      vColumn_QC_ID(g)    := 0;
      vColumn_Security(g) := 0;
    End;

    -- 02.04.10 
    -- replaced the cTable_name for vTable_name dependant on which mode is chosen.
    If Substr(vTable_Name,1,5) != 'XXCP_' then
        If Substr(vTable_Name,Length(vTable_Name)-1,2) = '_V' then
          v_selectsmt := 'select ' || vColumn_list || ', NULL Source_Rowid  from ' ||vTable_Name ||' a where vt_transaction_id = :M1';
          vIntSecStr  := 'select count(*) cnt from '||vTable_Name||' a where vt_transaction_id = :M1'||' '||get_interface_security(vSource_id, cTable_Name, vUser_Id);
        Else
          v_selectsmt := 'select ' || vColumn_list || ', a.rowid Source_Rowid  from ' ||vTable_Name ||' a where vt_transaction_id = :M1';
          vIntSecStr  := 'select count(*) cnt from '||vTable_Name||' a where vt_transaction_id = :M1'||' '||get_interface_security(vSource_id, cTable_Name, vUser_Id);
        End If;
    Else
      If cInterface_id is not null then
        v_selectsmt := 'select ' || vColumn_list || ', a.rowid Source_Rowid from ' ||vTable_Name || ' a where vt_interface_id = :M1';
        vIntSecStr  := 'select count(*) cnt from '||vTable_Name||' a where vt_interface_id = :M1'||' '||get_interface_security(vSource_id, cTable_Name, vUser_Id);
      ElsIf cSource_Rowid is not null then
        v_selectsmt := 'select ' || vColumn_list || ', a.rowid Source_Rowid from ' ||vTable_Name || ' a where rowid = :M1';
        vIntSecStr  := 'select count(*) cnt from '||vTable_Name||' a where rowid = :M1'||' '||get_interface_security(vSource_id, cTable_Name, vUser_Id);
      Else
        v_selectsmt := 'select ' || vColumn_list || ', a.rowid Source_Rowid from ' ||vTable_Name || ' a where vt_transaction_id = :M1';
        vIntSecStr  := 'select count(*) cnt from '||vTable_Name||' a where vt_transaction_id = :M1'||' '||get_interface_security(cSource_id, cTable_Name, vUser_Id);
      End If;
    End If;
    
    -- 02.04.10
    -- If preview mode the append the predicate for preview_id
    if cPreview_id is not null then
      v_Selectsmt := v_Selectsmt || ' and vt_preview_id = :M2';
      vIntSecStr  := vIntSecStr  || ' and vt_preview_id = :M2';
    end if;
    
    dbms_output.put_line(vIntSecStr);
    dbms_output.put_line(v_Selectsmt);

    -- Interface Security Check
    Begin


      v_cursorid := DBMS_SQL.Open_Cursor;
      DBMS_SQL.Parse(v_cursorId, vIntSecStr, DBMS_SQL.Native);

      -- 02.04.10 
      -- replaced the cTable_name for vTable_name dependant on which mode is chosen.
      If Substr(vTable_Name,1,5) != 'XXCP_' then
          DBMS_SQL.Bind_Variable(v_cursorId, ':M1', cTransaction_id);
      Else
        If cInterface_id is not null then
          DBMS_SQL.Bind_Variable(v_cursorId, ':M1', cInterface_id);
        ElsIf cSource_Rowid is not null then
          DBMS_SQL.Bind_Variable(v_cursorId, ':M1', cSource_Rowid);
        Else
          DBMS_SQL.Bind_Variable(v_cursorId, ':M1', cTransaction_id);
        End If;
      End If;
      
      -- 02.04.10
      -- If preview mode the append the predicate for preview_id
      if cPreview_id is not null then      
        DBMS_SQL.Bind_Variable(v_cursorId, ':M2', cPreview_id);
      end if;

      DBMS_SQL.Define_Column(v_cursorid, 1, vColumn_Results(1), 50);

      v_dummy := DBMS_SQL.Execute(v_cursorid);

      Loop
        If DBMS_SQL.Fetch_Rows(v_cursorid) = 0 then
          Exit;
        End if;
        vInternalErrorCode := 0;
        DBMS_SQL.Column_Value(v_cursorid, 1, vColumn_Results(1));
      End Loop;
      DBMS_SQL.Close_Cursor(v_CursorId);

      If vColumn_Results(1) = '0' then
        vSecurityOn := 'N';
      Else
        vSecurityOn := 'Y';
      End If;

      Exception
        when OTHERS then
          vSQLERRM := SQLERRM; -- New to trap Oracle's Reason for not working.
          If DBMS_SQl.IS_OPEN(v_CursorId) then
            DBMS_SQL.CLOSE_CURSOR(v_CursorId);
          End If;

    End;
    -- Interface Security Check End.

    Begin

      v_cursorid := DBMS_SQL.Open_Cursor;
      DBMS_SQL.Parse(v_cursorId, v_selectsmt, DBMS_SQL.Native);

      -- 02.04.10 
      -- replaced the cTable_name for vTable_name dependant on which mode is chosen.
      If Substr(vTable_Name,1,5) != 'XXCP_' then
          DBMS_SQL.Bind_Variable(v_cursorId, ':M1', cTransaction_id);
      Else
        If cInterface_id is not null then
          DBMS_SQL.Bind_Variable(v_cursorId, ':M1', cInterface_id);
        ElsIf cSource_Rowid is not null then
          DBMS_SQL.Bind_Variable(v_cursorId, ':M1', cSource_Rowid);
        Else
          DBMS_SQL.Bind_Variable(v_cursorId, ':M1', cTransaction_id);
        End If;
      End If;
      
      -- 02.04.10
      -- If preview mode the append the predicate for preview_id
      if cPreview_id is not null then      
        DBMS_SQL.Bind_Variable(v_cursorId, ':M2', cPreview_id);
      end if;

      vInternalErrorCode := 1010;

      For w in 1 .. g loop
        DBMS_SQL.Define_Column(v_cursorid, w, vColumn_Results(w), 150);
      End loop;

      v_dummy := DBMS_SQL.Execute(v_cursorid);

      Loop
        If DBMS_SQL.Fetch_Rows(v_cursorid) = 0 then
          Exit;
        End if;
        vInternalErrorCode := 0;
        For w in 1 .. g loop
            DBMS_SQL.Column_Value(v_cursorid, w, vColumn_Results(w));
        End loop;

      End Loop;
      DBMS_SQL.Close_Cursor(v_CursorId);

    Exception
      when OTHERS then
        vSQLERRM := SQLERRM; -- New to trap Oracle's Reason for not working.
        Set_InternalErrorCode(vInternalErrorCode, 1009); -- Change to Say BAD SQL
        If DBMS_SQl.IS_OPEN(v_CursorId) then
          DBMS_SQL.CLOSE_CURSOR(v_CursorId);
        End If;

    End;

    If vInternalErrorCode = 0 then

      If vColumn_Results.Count > 0 then
        vSource_Rowid := vColumn_Results(g);
      End If;

      For w in 1 .. g loop
        If ((vColumn_Results(w) is not null) or (nvl(cData_Only,'N') IN ('N','E')))  And vColumn_Names(w) != 'SOURCE ROWID' then
          Begin

            vDescription := nvl(vColumn_Desc(w),vColumn_Names(w));
            
            -- 02.05.07            
            For tsalRec in tsal(cSource_id, cTransaction_Table, cTransaction_Type,vColumn_Names(w)) loop
               vDescription := tsalRec.User_Alias;
            End Loop;            

            For NmRec in qx(vColumn_QC_ID(w), nvl(cForm_Context,'*'), cSource_Assignment_id) Loop
              vDescription := nvl(NmRec.Description,vColumn_Names(w));
              Exit;
            End Loop;

            vColumn_Security(w) := 0;
            For ScRec in Secur(cTable_Name,vColumn_Names(w), cSource_id, cUser_id) Loop
                vColumn_Security(w) := 3;
            End Loop;

            If vSecurityOn = 'N' then
              vColumn_Security(w) := 9;
            End If;

            Insert into xxcp_edit_interface(
                user_id
               ,interface_id
               ,transaction_id
               ,source_rowid
               ,table_name
               ,column_name
               ,data_type
               ,data_length
               ,required
               ,old_null
               ,old_value
               ,new_value
               ,creation_date
               ,qc_id
               ,column_description
               ,security_level
               ,source_assignment_id
               -- 02.04.10
               ,preview_id
               )
              Values(
               vUser_id,
               cInterface_id,
               cTransaction_id,
               nvl(cSOURCE_ROWID, vSource_Rowid),
               vTable_name,
               vColumn_Names(w),
               vColumn_Type(w),
               vColumn_length(w),
               vColumn_Req(w),
               Decode(vColumn_Results(w),Null,'Y','N'),
               vColumn_Results(w), -- old
               vColumn_Results(w), -- new
               Sysdate,
               vColumn_QC_ID(w),
               vDescription,
               vColumn_Security(w),
               nvl(cSource_Assignment_id,0),
               -- 02.04.10
               cPreview_id
              );

              Commit;

            Exception when OTHERS then null;
          End;

        end If;
      End loop;

    Else
      Show(to_char(vInternalErrorCode) || vSQLERRM);
      FndWriteError(vInternalErrorCode,vSQLERRM,v_selectsmt);
    End If;

    cInternalErrorCode := vInternalErrorCode;

  End Edit_Record_Contents;

  -- *************************************************************
  --                     Edit_Record_Contents
  -- *************************************************************
  Procedure Edit_Record_Contents(cTable_Name           in varchar2,
                                 cTransaction_table    in varchar2,
                                 cTransaction_type     in varchar2,
                                 cTransaction_id       in number,
                                 cSource_Rowid         in rowid,
                                 cForm_Context         in varchar2,
                                 cInterface_id         in varchar2,
                                 cData_Only            in varchar2,
                                 cSource_id            in number,
                                 cSource_Assignment_id in number,
                                 cUser_id              in number default -1,
                                 cLogin_id             in number default -1,
                                 cInternalErrorCode in out number) IS
  begin
  
   Edit_Record_Contents(cTable_Name           => cTable_Name,
                        cTransaction_Table    => cTransaction_Table,
                        cTransaction_Type     => cTransaction_Type,
                        cTransaction_id       => cTransaction_id,
                        cSource_Rowid         => cSource_Rowid,     
                        cForm_Context         => cForm_Context,     
                        cInterface_id         => cInterface_id,       
                        cPreview_id           => null,     
                        cData_Only            => cData_Only,           
                        cSource_id            => cSource_id,    
                        cSource_Assignment_id => cSource_Assignment_id,
                        -- 02.05.05
                        cUser_id              => cUser_id,
                        cLogin_id             => cLogin_id,
                        cInternalErrorCode    => cInternalErrorCode);
  
  end Edit_Record_Contents;                                 
 
  --
  -- Get_SQLBuildStatements
  --
  Function Get_SqlBuildStatements(cSQL_build_id     in Number,
                                  cAttribute_set_id in Number,
                                  cActivity_id      in Number)
    RETURN VARCHAR2 IS

    -- You must have set the xxcp_global.set_source_id in your form first.

    cursor sbd(pSQL_Build_id in Number) is
      select distinct sql_build_id,
                      sql_build,
                      transaction_table,
                      optimizer_hint,
                      activity_id, source_id
        from xxcp_sql_build
       where sql_build_id = pSQL_Build_id;

    cursor dya(pAttribute_set_id in number, pActivity_id in number) is
      select sequence_number, Column_Definition
        from xxcp_dynamic_attribute_sets ds
       where ds.attribute_set_id = pAttribute_set_id
         and upper(nvl(ds.column_definition, 'NULL')) <> 'NULL'
      Union
      select id, 'null'
        from xxcp_Dynamic_attributes_v
       where dynamic_name_id = 1
         and column_type = pActivity_id
         and not id = any
       (select sequence_number
                from xxcp_dynamic_attribute_sets ds
               where ds.attribute_set_id = pAttribute_set_id
                 and upper(nvl(ds.column_definition, 'NULL')) <> 'NULL')
       order by 1;

    vSelect_statement varchar2(32676);
    vSource_cols      varchar2(32676);
    vColumn_Count     Integer := 0;

  Begin

    --
    for rec1 in sbd(cSQL_Build_id) loop

      vSelect_statement := Null;
      vSource_cols      := Null;
      vColumn_Count     := 0;

      xxcp_global.SET_SOURCE_ID(rec1.source_id);

      for rec2 in dya(cSQL_build_id, cActivity_id) loop
        vColumn_Count := vColumn_Count + 1;
        vSource_cols  := vSource_Cols ||
                         replace(replace(rec2.column_definition, '}', null),
                                 '{',
                                 ':') || ',';
      end loop;

      if instr(vSource_cols, ',') > 0 then
        vSource_cols := Substr(vSource_cols, 1, Length(vSource_cols) - 1);
      end if;
      If rec1.optimizer_hint is not null then
        vSelect_statement := 'Select ' || rec1.optimizer_hint || ' ' ||
                             vSource_Cols || ' ' || rec1.sql_build;
      Else
        vSelect_statement := 'Select ' || vSource_Cols || ' ' ||
                             rec1.sql_build;
      End If;

    End loop;

    Return(vSelect_statement);

  END Get_SqlBuildStatements;

  --
  -- CONVERT_COLUMN_DEFINITION
  --
  FUNCTION Convert_Column_Definition(cColumn_Definition in varchar2)
    RETURN VARCHAR2 IS

    i integer;
    p integer;

    MI   integer;
    MinD integer;
    MaxD integer;

    vCompName varchar2(60);
    vCompStr  varchar2(1000);
    vColStr   varchar2(1000);

    -- You must have set the xxcp_global.set_source_id in your form first.

  Begin

    vColStr := cColumn_Definition;

    Select Max(id - 1000)
      into mi
      from xxcp_dynamic_attributes_v
     where dynamic_name_id = 2
       and column_type = 1009;

    For i in 1 .. MI loop
      P := 9000 + i;

      vCompStr := '{P' || to_char(P) || '}';

      If Instr(vColStr, vCompStr) > 0 then
        vCompName := get_attribute_name_all(2, P, 1009);
        If vCompName is not null then
          vCompName := '{' || vCompName || '}';
          Select Replace(vColStr, vCompStr, vCompName)
            into vColStr
            from dual;
        End If;
      End If;
    End Loop;

    -- Configuration

    Begin

      Select Min(Id), Max(id)
        into minD, maxD
        from xxcp_dynamic_attributes_v
       where dynamic_name_id = 1
         and column_type = 1001;

      For i in minD .. maxD loop
        P        := i;
        vCompStr := '{D' || to_char(P) || '}';

        If Instr(vColStr, vCompStr) > 0 then
          vCompName := get_attribute_name(P, 1001);
          If vCompName is not null then
            vCompName := '{' || vCompName || '}';
            Select Replace(vColStr, vCompStr, vCompName)
              into vColStr
              from dual;
          End If;
        End If;
      End Loop;

    Exception
      when others then
        null;

    End;

    -- Configuration pricing
    Begin

      Select Min(Id), Max(id)
        into minD, maxD
        from xxcp_dynamic_attributes_v
       where dynamic_name_id = 1
         and column_type = 1002;

      For i in minD .. maxD loop
        P        := i;
        vCompStr := '{D' || to_char(P) || '}';

        If Instr(vColStr, vCompStr) > 0 then
          vCompName := get_attribute_name(P, 1002);
          If vCompName is not null then
            vCompName := '{' || vCompName || '}';
            Select Replace(vColStr, vCompStr, vCompName)
              into vColStr
              from dual;
          End If;
        End If;
      End Loop;

    Exception
      when others then
        null;

    End;

    Begin

      -- Accounting
      Select Min(Id), Max(id)
        into minD, maxD
        from xxcp_dynamic_attributes_v
       where dynamic_name_id = 1
         and column_type = 1003;

      For i in minD .. maxD loop
        P        := i;
        vCompStr := '{D' || to_char(P) || '}';

        If Instr(vColStr, vCompStr) > 0 then
          vCompName := get_attribute_name(P, 1003);
          If vCompName is not null then
            vCompName := '{' || vCompName || '}';
            Select Replace(vColStr, vCompStr, vCompName)
              into vColStr
              from dual;
          End If;
        End If;
      End Loop;

    Exception
      when others then
        null;

    End;

    Begin
      -- Accounting Pricing
      Select Min(Column_Id), Max(Column_id)
        into minD, maxD
        from xxcp_dynamic_attributes
       where dynamic_name_id = 1
         and column_type = 1004;

      For i in minD .. maxD loop
        P        := i;
        vCompStr := '{D' || to_char(P) || '}';

        If Instr(vColStr, vCompStr) > 0 then
          vCompName := get_attribute_name(P, 1004);
          If vCompName is not null then
            vCompName := '{' || vCompName || '}';
            Select Replace(vColStr, vCompStr, vCompName)
              into vColStr
              from dual;
          End If;
        End If;
      End Loop;

    Exception
      when others then
        null;

    End;

    Begin
      -- Column Attributes
      Select Min(Column_Id), Max(Column_id)
        into minD, maxD
        from xxcp_dynamic_attributes
       where dynamic_name_id = 1
         and column_type = 1006;

      For i in minD .. maxD loop
        P        := i;
        vCompStr := '{D' || to_char(P) || '}';

        If Instr(vColStr, vCompStr) > 0 then
          vCompName := get_attribute_name(P, 1006);
          If vCompName is not null then
            vCompName := '{' || vCompName || '}';
            Select Replace(vColStr, vCompStr, vCompName)
              into vColStr
              from dual;
          End If;
        End If;
      End Loop;

    Exception
      when others then
        null;

    End;

    Begin
      -- API  Attributes
      Select Min(Column_Id), Max(Column_id)
        into minD, maxD
        from xxcp_dynamic_attributes
       where dynamic_name_id = 1
         and column_type = 1005;

      For i in minD .. maxD loop
        P        := i;
        vCompStr := '{D' || to_char(P) || '}';

        If Instr(vColStr, vCompStr) > 0 then
          vCompName := get_attribute_name(P, 1005);
          If vCompName is not null then
            vCompName := '{' || vCompName || '}';
            Select Replace(vColStr, vCompStr, vCompName)
              into vColStr
              from dual;
          End If;
        End If;
      End Loop;

    Exception
      when OTHERS then
        null;

    End;
    Return(vColStr);
  END Convert_Column_Definition;

  --
  -- DI_COLUMN_DEFINITION
  --
  Function DI_Column_Definition(cColumn_Definition in varchar2)RETURN VARCHAR2 IS

    i integer;
    p integer;

    MI   integer;
    MinD integer;
    MaxD integer;

    vCompName varchar2(60);
    vCompStr  varchar2(32000);
    vCompStr2 varchar2(32000);
    vColStr   varchar2(32000);

    -- You must have set the xxcp_global.set_source_id in your form first.

  Begin

    vColStr := cColumn_Definition;

    vColStr := replace(vColStr,':M0','{Source Rowid}');
    vColStr := replace(vColStr,':M3','{Transaction Id}');
    vColStr := replace(vColStr,':M5','{Set of Books}');
    vColStr := replace(vColStr,':M7','{Trading Set}');
    vColStr := replace(vColStr,':M8','{Transaction Class}');    
    vColStr := replace(vColStr,':M9','{Previous Attribute id}');
    vColStr := replace(vColStr,':PV0','{Preview Source Rowid}');
    vColStr := replace(vColStr,':MS','{Owner Tax Reg id}');
    vColStr := replace(vColStr,':MP','{Partner Tax Reg id}');
    vColStr := replace(vColStr,':MKEY','{Key Information}');
    vColStr := replace(vColStr,':MA','{Transaction Attribute Id}');
    vColStr := replace(vColStr,':m0','{Source Rowid}');
    vColStr := replace(vColStr,':m3','{Transaction Id}');
    vColStr := replace(vColStr,':m5','{Set of Books}');
    vColStr := replace(vColStr,':m7','{Trading Set}');
    vColStr := replace(vColStr,':m8','{Transaction Class}');    
    vColStr := replace(vColStr,':m9','{Previous Attribute id}');
    vColStr := replace(vColStr,':pv0','{Preview Source Rowid}');
    vColStr := replace(vColStr,':ms','{Owner Tax Reg id}');
    vColStr := replace(vColStr,':mp','{Partner Tax Reg id}');
    vColStr := replace(vColStr,':mkey','{Key Information}');
    vColStr := replace(vColStr,':ma','{Transaction Attribute Id}');

    vColStr := replace(vColStr,'{M0}','{Source Rowid}');
    vColStr := replace(vColStr,'{M3}','{Transaction Id}');
    vColStr := replace(vColStr,'{M5}','{Set of Books}');
    vColStr := replace(vColStr,'{M7}','{Trading Set}');
    vColStr := replace(vColStr,'{M8}','{Transaction Class}');    
    vColStr := replace(vColStr,'{M9}','{Previous Attribute id}');
    vColStr := replace(vColStr,'{PV0}','{Preview Source Rowid}');
    vColStr := replace(vColStr,'{MS}','{Owner Tax Reg id}');
    vColStr := replace(vColStr,'{MP}','{Partner Tax Reg id}');
    vColStr := replace(vColStr,'{MKEY}','{Key Information}');
    vColStr := replace(vColStr,'{MA}','{Transaction Attribute Id}');
    vColStr := replace(vColStr,'{m0}','{Source Rowid}');
    vColStr := replace(vColStr,'{m3}','{Transaction Id}');
    vColStr := replace(vColStr,'{m5}','{Set of Books}');
    vColStr := replace(vColStr,'{m7}','{Trading Set}');
    vColStr := replace(vColStr,'{m8}','{Transaction Class}');    
    vColStr := replace(vColStr,'{m9}','{Previous Attribute id}');
    vColStr := replace(vColStr,'{pv0}','{Preview Source Rowid}');
    vColStr := replace(vColStr,'{ms}','{Owner Tax Reg id}');
    vColStr := replace(vColStr,'{mp}','{Partner Tax Reg id}');
    vColStr := replace(vColStr,'{mkey}','{Key Information}');
    vColStr := replace(vColStr,'{ma}','{Transaction Attribute Id}');

    For P in 1..8 loop
      vColStr := replace(vColStr,':CB'||to_char(p),'{Custom Bind '||to_char(p)||'}');
    End loop;
    
    -- Internals
    Select Max(id - 1000)
      into mi
      from xxcp_dynamic_attributes_v
     where dynamic_name_id = 2
       and column_type = 1009;

    For i in 1 .. MI loop
      P := 9000 + i;

      vCompStr  := '{P' || to_char(P) || '}';
      vCompStr2 := ':P' || to_char(P);

      If Instr(vColStr, vCompStr) > 0 or Instr(vColStr, vCompStr2) > 0 then
        vCompName := get_attribute_name_all(2, P, 1009);
        If vCompName is not null then
          vCompName := '{' || vCompName || '}';
          Select Replace(Replace(vColStr, vCompStr, vCompName),vCompStr2, vCompName)
            into vColStr
            from dual;
        End If;
      End If;
    End Loop;

    -- Configuration

    Begin

      Select Min(Id), Max(id)
        into minD, maxD
        from xxcp_dynamic_attributes_v
       where dynamic_name_id = 1
         and column_type = 1001;

      For i in minD .. maxD loop
        P        := i;
        vCompStr := '{D' || to_char(P) || '}';
        vCompStr2 := ':D' || to_char(P);


        If Instr(vColStr, vCompStr) > 0 or Instr(vColStr, vCompStr2) > 0 then
          vCompName := get_attribute_name(P, 1001);
          If vCompName is not null then
            vCompName := '{' || vCompName || '}';
            Select Replace(Replace(vColStr, vCompStr, vCompName),vCompStr2, vCompName)
              into vColStr
              from dual;
          End If;
        End If;
      End Loop;

    Exception
      when others then
        null;

    End;

    -- Common Assignment 

    Begin

      Select Min(Id), Max(id)
        into minD, maxD
        from xxcp_dynamic_attributes_v
       where dynamic_name_id = 1
         and column_type = 1001;

      For i in minD .. maxD loop
        P        := i;
        vCompStr := '{C' || to_char(P) || '}';
        vCompStr2 := ':C' || to_char(P);

        If Instr(vColStr, vCompStr) > 0 or Instr(vColStr, vCompStr2) > 0 then
          vCompName := get_attribute_name(P, 1001);
          If vCompName is not null then
            vCompName := '{' || vCompName || '}';
            Select Replace(Replace(vColStr, vCompStr, vCompName),vCompStr2, vCompName)
              into vColStr
              from dual;
          End If;
        End If;
      End Loop;

    Exception
      when others then
        null;

    End;

    -- Configuration pricing
    Begin

      Select Min(Id), Max(id)
        into minD, maxD
        from xxcp_dynamic_attributes_v
       where dynamic_name_id = 1
         and column_type = 1002;

      For i in minD .. maxD loop
        P        := i;
        vCompStr := '{D' || to_char(P) || '}';
        vCompStr2 := ':D' || to_char(P);

        If Instr(vColStr, vCompStr) > 0 or Instr(vColStr, vCompStr2) > 0 then
          vCompName := get_attribute_name(P, 1002);
          If vCompName is not null then
            vCompName := '{' || vCompName || '}';
            Select Replace(Replace(vColStr, vCompStr, vCompName),vCompStr2, vCompName)
              into vColStr
              from dual;
          End If;
        End If;
      End Loop;

    Exception
      when others then
        null;

    End;

    Begin

      -- Accounting
      Select Min(Id), Max(id)
        into minD, maxD
        from xxcp_dynamic_attributes_v
       where dynamic_name_id = 1
         and column_type = 1003;

      For i in minD .. maxD loop
        P        := i;
        vCompStr := '{D' || to_char(P) || '}';
        vCompStr2 := ':D' || to_char(P);

        If Instr(vColStr, vCompStr) > 0 or Instr(vColStr, vCompStr2) > 0 then
          vCompName := get_attribute_name(P, 1003);
          If vCompName is not null then
            vCompName := '{' || vCompName || '}';
            Select Replace(Replace(vColStr, vCompStr, vCompName),vCompStr2, vCompName)
              into vColStr
              from dual;
          End If;
        End If;
      End Loop;

    Exception
      when others then
        null;

    End;

    Begin
      -- Accounting Pricing
      Select Min(Column_Id), Max(Column_id)
        into minD, maxD
        from xxcp_dynamic_attributes
       where dynamic_name_id = 1
         and column_type = 1004;

      For i in minD .. maxD loop
        P        := i;
        vCompStr := '{D' || to_char(P) || '}';
        vCompStr2 := ':D' || to_char(P);

        If Instr(vColStr, vCompStr) > 0 or Instr(vColStr, vCompStr2) > 0 then
          vCompName := get_attribute_name(P, 1004);
          If vCompName is not null then
            vCompName := '{' || vCompName || '}';
            Select Replace(Replace(vColStr, vCompStr, vCompName),vCompStr2, vCompName)
              into vColStr
              from dual;
          End If;
        End If;
      End Loop;

    Exception
      when others then
        null;

    End;

    Begin
      -- Column Attributes
      Select Min(Column_Id), Max(Column_id)
        into minD, maxD
        from xxcp_dynamic_attributes
       where dynamic_name_id = 1
         and column_type = 1006;

      For i in minD .. maxD loop
        P        := i;
        vCompStr := '{D' || to_char(P) || '}';
        vCompStr2 := ':D' || to_char(P);

        If Instr(vColStr, vCompStr) > 0 or Instr(vColStr, vCompStr2) > 0 then
          vCompName := get_attribute_name(P, 1006);
          If vCompName is not null then
            vCompName := '{' || vCompName || '}';
            Select Replace(Replace(vColStr, vCompStr, vCompName),vCompStr2, vCompName)
              into vColStr
              from dual;
          End If;
        End If;
      End Loop;

    Exception
      when others then
        null;

    End;

    Begin
      -- API  Attributes
      Select Min(Column_Id), Max(Column_id)
        into minD, maxD
        from xxcp_dynamic_attributes
       where dynamic_name_id = 1
         and column_type = 1005;

      For i in minD .. maxD loop
        P        := i;
        vCompStr := '{D' || to_char(P) || '}';
        vCompStr2 := ':D' || to_char(P);

        If Instr(vColStr, vCompStr) > 0 or Instr(vColStr, vCompStr2) > 0 then
          vCompName := get_attribute_name(P, 1005);
          If vCompName is not null then
            vCompName := '{' || vCompName || '}';
            Select Replace(Replace(vColStr, vCompStr, vCompName),vCompStr2, vCompName)
              into vColStr
              from dual;
          End If;
        End If;
      End Loop;

    Exception
      when OTHERS then
        null;

    End;
    Return(vColStr);
  END DI_Column_Definition;
  --
  -- FETCH_COLUMN
  --
  FUNCTION Fetch_Column(cSQL               in varchar2,
                        cSQLERRM           out varchar2,
                        cInternalErrorCode in out Number) RETURN VARCHAR2 IS

    -- ***********
    --  Variables
    -- ***********
    v_cursorId integer;
    v_dummy    integer;
    rowcnt     number;

    v_SelectSmt    varchar2(16100);
    vResult        varchar2(400);
    vSQLERRM       varchar2(1024);
    vValue1        varchar2(400);

  begin
    -- Initialize Arrays
    vSQLERRM := Null;
    vResult  := Null;
    -- Build SQL
    v_SelectSmt := cSQL;
    Begin
      v_cursorid := DBMS_SQL.OPEN_CURSOR;
      -- Query Database
      DBMS_SQL.PARSE(v_cursorId, v_selectsmt, DBMS_SQL.native);

      /* Std Columns */
      DBMS_SQL.DEFINE_COLUMN(v_cursorid, 1, vValue1, 250);

      v_dummy := DBMS_SQL.Execute(v_cursorid);
      Rowcnt  := DBMS_SQL.FETCH_ROWS(v_CursorId);

      If RowCnt = 1 then
        DBMS_SQL.COLUMN_VALUE(v_cursorid, 1, vValue1);
        vResult := vValue1;
      End if;

      DBMS_SQL.CLOSE_CURSOR(v_CursorId);
      /* Error Notification */
      If RowCnt = 0 then
        Set_InternalErrorCode(cInternalErrorCode, 2003);
      End If;
      If RowCnt > 1 then
        Set_InternalErrorCode(cInternalErrorCode, 2004);
      End If;

    Exception
      when OTHERS then
        vSQLERRM := SQLERRM; -- New to trap Oracle's Reason for not working.
        Set_InternalErrorCode(cInternalErrorCode, 2009); -- Change to Say BAD SQL
        If DBMS_SQl.IS_OPEN(v_CursorId) then
          DBMS_SQL.CLOSE_CURSOR(v_CursorId);
        End If;
    End;

    cSQLERRM := vSQLERRM;
    Return(vResult);
  END Fetch_Column;

  --
  -- DML_STATEMENT
  --
  PROCEDURE DML_Statement(cSQL               in varchar2,
                          cSQLERRM           out varchar2,
                          cTest_only         in varchar2,
                          cInternalErrorCode in out Number) IS

    v_cursorId     integer;
    v_dummy        integer;
    v_SelectSmt    varchar2(16100);
    vSQLERRM       varchar2(1024);

  begin
    -- Initialize Arrays
    vSQLERRM := Null;
    -- Build SQL
    v_SelectSmt := cSQL;
    Begin
      v_cursorid := DBMS_SQL.OPEN_CURSOR;
      -- Query Database
      DBMS_SQL.PARSE(v_cursorId, v_selectsmt, DBMS_SQL.native);
      If cTest_only <> 'Y' then
        /* Std Columns */
        v_dummy := DBMS_SQL.Execute(v_cursorid);
      End If;
      If DBMS_SQl.IS_OPEN(v_CursorId) then
        DBMS_SQL.CLOSE_CURSOR(v_CursorId);
      End If;

    Exception
      when OTHERS then
        vSQLERRM := SQLERRM; -- New to trap Oracle's Reason for not working.
        Set_InternalErrorCode(cInternalErrorCode, 2009); -- Change to Say BAD SQL
        If DBMS_SQl.IS_OPEN(v_CursorId) then
          DBMS_SQL.CLOSE_CURSOR(v_CursorId);
        End If;
    End;

    cSQLERRM := vSQLERRM;
  END DML_Statement;

  --
  -- Execute Immediate
  --
  PROCEDURE Execute_Immediate(cSQL               in varchar2,
                              cSQLERRM           out varchar2,
                              cInternalErrorCode in out Number) IS

    vSQLERRM       varchar2(1024);

  begin
    -- Initialize Arrays
    vSQLERRM := Null;
    Begin
      EXECUTE IMMEDIATE cSQL;
    Exception
      when OTHERS then
        vSQLERRM := SQLERRM; -- New to trap Oracle's Reason for not working.
        Set_InternalErrorCode(cInternalErrorCode, 2009); -- Change to Say BAD SQL
    End;

    cSQLERRM := vSQLERRM;
  End Execute_Immediate;
  --
  -- Execute Immediate
  --
  Function Execute_Immediate_Into(cSQL               in varchar2,
                                  cSQLERRM           out varchar2,
                                  cInternalErrorCode in out Number) return varchar2 IS

    vSQLERRM       varchar2(512);
    vReturn_Value  varchar2(500);

  begin
    Begin
      EXECUTE IMMEDIATE cSQL INTO vReturn_Value;
    Exception
      when OTHERS then
        vSQLERRM := SQLERRM; -- New to trap Oracle's Reason for not working.
    End;

    cSQLERRM := vSQLERRM;
    Return(vReturn_Value);
  End Execute_Immediate_Into;

  --
  -- Execute_Rewind
  --
  PROCEDURE Execute_Rewind(cSID               in number,
                           cSQL               in varchar2,
                           cSQLERRM           out varchar2,
                           cInternalErrorCode in out Number) IS

    vRowcnt  number;
    vSQL     varchar2(32000);
    vSQLERRM varchar2(512);

    cursor c1 is
      select vt_parent_trx_id, vt_transaction_id, vt_transaction_table
        from xxcp_rewind_records r
       where r.SID = cSid;

  begin
    vSQLERRM := Null;
    vRowcnt  := 0;
    for rec in c1 loop
      Begin
        vSQL := cSQL || to_char(rec.vt_transaction_id);
        EXECUTE IMMEDIATE vSQL;
        vRowcnt := vRowcnt + 1;
      Exception
        when others then
          vSQLERRM := SQLERRM; -- New to trap Oracle's Reason for not working.
          Set_InternalErrorCode(cInternalErrorCode, 2019); -- Change to Say BAD SQL
      End;
    End loop;

    If (cInternalErrorCode = 0 and vRowcnt > 0) then
      Commit;
    Elsif (cInternalErrorCode <> 0 and vRowcnt > 0) then
      Rollback;
    End If;
    cSQLERRM := vSQLERRM;
  END Execute_Rewind;

  --
  -- Get_Transaction_Cache
  --
  Function Get_Transaction_Cache(cAttribute_id in number,
                                 cPreview_id   in number,
                                 cOrderBy      in varchar2 default 'ID')  --'ID' or 'NAME'  
                                 Return varchar2 is

    Cursor TAC(pAttribute_id in number) is
       select distinct 
       seq,
       substr(value,5)   value,
       substr(value,1,4) attribute_id,
       xxcp_foundation.get_attribute_name(substr(value,1,4),0) attribute_name
       from
       (
       Select seq,
              decode(r,1,cached_value1,
                       2,cached_value2,
                       3,cached_value3,
                       4,cached_value4,
                       5,cached_value5,
                       6,cached_value6,
                       7,cached_value7,
                       8,cached_value8,
                       9,cached_value9,
                      10,cached_value10,
                      11,cached_value11,
                      12,cached_value12,
                      13,cached_value13,
                      14,cached_value14,
                      15,cached_value15,
                      16,cached_value16,
                      17,cached_value17,
                      18,cached_value18,
                      19,cached_value19,
                      20,cached_value20,
                      21,cached_value21,
                      22,cached_value22,
                      23,cached_value23,
                      24,cached_value24,
                      25,cached_value25,
                      26,cached_value26,
                      27,cached_value27,
                      28,cached_value28,
                      29,cached_value29,
                      30,cached_value30,
                      31,cached_value31,
                      32,cached_value32,
                      33,cached_value33,
                      34,cached_value34,
                      35,cached_value35,
                      36,cached_value36,
                      37,cached_value37,
                      38,cached_value38,
                      39,cached_value39,
                      40,cached_value40,
                      41,cached_value41,
                      42,cached_value42,
                      43,cached_value43,
                      44,cached_value44,
                      45,cached_value45,
                      46,cached_value46,
                      47,cached_value47,
                      48,cached_value48,
                      49,cached_value49,
                      50,cached_value50)  value
        -- NCM 02.04.08
        from xxcp_transaction_cache_v,
             (select rownum r from all_objects where rownum <= 50)
        where attribute_id = pAttribute_id
        and   decode(r,1,cached_value1,
                2,cached_value2,
                3,cached_value3,
                4,cached_value4,
                5,cached_value5,
                6,cached_value6,
                7,cached_value7,
                8,cached_value8,
                9,cached_value9,
               10,cached_value10,
               11,cached_value11,
               12,cached_value12,
               13,cached_value13,
               14,cached_value14,
               15,cached_value15,
               16,cached_value16,
               17,cached_value17,
               18,cached_value18,
               19,cached_value19,
               20,cached_value20,
               21,cached_value21,
               22,cached_value22,
               23,cached_value23,
               24,cached_value24,
               25,cached_value25,
               26,cached_value26,
               27,cached_value27,
               28,cached_value28,
               29,cached_value29,
               30,cached_value30,
               31,cached_value31,
               32,cached_value32,
               33,cached_value33,
               34,cached_value34,
               35,cached_value35,
               36,cached_value36,
               37,cached_value37,
               38,cached_value38,
               39,cached_value39,
               40,cached_value40,
               41,cached_value41,
               42,cached_value42,
               43,cached_value43,
               44,cached_value44,
               45,cached_value45,
               46,cached_value46,
               47,cached_value47,
               48,cached_value48,
               49,cached_value49,
               50,cached_value50) is not null)
    Order By seq,decode(cOrderBy,'NAME',xxcp_foundation.Get_Attribute_Name(attribute_id,0),attribute_id);

    Cursor TAC_Preview(pAttribute_id in number, pPreview_id in number) is
       select distinct
       seq,
       substr(value,5)   value,
       substr(value,1,4) attribute_id,
       xxcp_foundation.get_attribute_name(substr(value,1,4),0) attribute_name
       from
       (
       Select seq,
              decode(r,1,cached_value1,
                       2,cached_value2,
                       3,cached_value3,
                       4,cached_value4,
                       5,cached_value5,
                       6,cached_value6,
                       7,cached_value7,
                       8,cached_value8,
                       9,cached_value9,
                      10,cached_value10,
                      11,cached_value11,
                      12,cached_value12,
                      13,cached_value13,
                      14,cached_value14,
                      15,cached_value15,
                      16,cached_value16,
                      17,cached_value17,
                      18,cached_value18,
                      19,cached_value19,
                      20,cached_value20,
                      21,cached_value21,
                      22,cached_value22,
                      23,cached_value23,
                      24,cached_value24,
                      25,cached_value25,
                      26,cached_value26,
                      27,cached_value27,
                      28,cached_value28,
                      29,cached_value29,
                      30,cached_value30,
                      31,cached_value31,
                      32,cached_value32,
                      33,cached_value33,
                      34,cached_value34,
                      35,cached_value35,
                      36,cached_value36,
                      37,cached_value37,
                      38,cached_value38,
                      39,cached_value39,
                      40,cached_value40,
                      41,cached_value41,
                      42,cached_value42,
                      43,cached_value43,
                      44,cached_value44,
                      45,cached_value45,
                      46,cached_value46,
                      47,cached_value47,
                      48,cached_value48,
                      49,cached_value49,
                      50,cached_value50)  value
        from xxcp_pv_transaction_cache,
             (select rownum r from all_objects where rownum <= 50)
       where attribute_id = pAttribute_id
         and preview_id = pPreview_id
         and   decode(r,1,cached_value1,
                2,cached_value2,
                3,cached_value3,
                4,cached_value4,
                5,cached_value5,
                6,cached_value6,
                7,cached_value7,
                8,cached_value8,
                9,cached_value9,
               10,cached_value10,
               11,cached_value11,
               12,cached_value12,
               13,cached_value13,
               14,cached_value14,
               15,cached_value15,
               16,cached_value16,
               17,cached_value17,
               18,cached_value18,
               19,cached_value19,
               20,cached_value20,
               21,cached_value21,
               22,cached_value22,
               23,cached_value23,
               24,cached_value24,
               25,cached_value25,
               26,cached_value26,
               27,cached_value27,
               28,cached_value28,
               29,cached_value29,
               30,cached_value30,
               31,cached_value31,
               32,cached_value32,
               33,cached_value33,
               34,cached_value34,
               35,cached_value35,
               36,cached_value36,
               37,cached_value37,
               38,cached_value38,
               39,cached_value39,
               40,cached_value40,
               41,cached_value41,
               42,cached_value42,
               43,cached_value43,
               44,cached_value44,
               45,cached_value45,
               46,cached_value46,
               47,cached_value47,
               48,cached_value48,
               49,cached_value49,
               50,cached_value50) is not null)
    Order By seq,decode(cOrderBy,'NAME',xxcp_foundation.Get_Attribute_Name(attribute_id,0),attribute_id);

    vCache           varchar2(32000);
    vNoneCache       varchar2(32000); 
    vNone_cache_flag varchar2(1) Default 'N';
    
    cursor usrPreview(cUser_id in number) is
    select c.pv_none_cache_flag
      from xxcp_user_profiles c
    where c.user_id = cUser_id;

   --
   -- Get_Attribute_None_Cache
   --
   Function Get_Attribute_None_Cache(cColumn_id in number) Return varchar2 is

    Cursor AttName(pColumn_id in number) is
      select nvl(d.none_cache,'N') none_cache_flag
        from XXCP_DYNAMIC_ATTRIBUTES d
       where d.column_id = pColumn_id
         and decode(d.source_id, 0, xxcp_global.get_source_id,d.source_id) = xxcp_global.get_source_id;

    vResult xxcp_dynamic_attributes.column_name%type := 'N';

    Begin
      For AttRec in AttName(pColumn_id => cColumn_id) Loop
        vResult := AttRec.none_cache_flag;
      End Loop;
      Return(vResult);
    End Get_Attribute_None_Cache;
  
    --
    -- Cache
    --
    Function CACHE_LINE(cAttribute_ID   in varchar2,
                        cAttribute_Name in varchar2,
                        cValue          in varchar2) return varchar2 is

      vLine varchar2(500);

    Begin
      
      If cAttribute_ID is not null then
        if cOrderBy = 'ID' then
          vLine := '{' || rpad(cAttribute_ID, 4) || '} ' || rpad(cAttribute_Name,30) || ' <' ||
                   substr(cValue, 1, 250) || '>' || chr(10);
        elsif cOrderBy = 'NAME' then
          vLine := rpad(cAttribute_Name,30) || ' {' || rpad(cAttribute_ID, 4) || '} ' || ' <' ||
                   substr(cValue, 1, 250) || '>' || chr(10);
        end if;        
      End If;
      Return(vLine);
    End Cache_line;

  Begin

    If cPreview_id = 0 then
      
        For Rec in TAC(cAttribute_id) Loop
          vCache := vCache || Cache_Line(rec.attribute_id,
                                         rec.Attribute_Name,
                                         rec.VALUE);
        End Loop;

    ElsIf cPreview_id != 0 then

        For Rec in TAC_Preview(cAttribute_id, cPreview_id) Loop
          If Get_Attribute_None_Cache(rec.attribute_id) = 'N' then
            vCache := vCache || Cache_Line(rec.attribute_id,
                                           rec.Attribute_Name,
                                           rec.VALUE);
           End If;
        End Loop;

        For Rec in TAC_Preview(cAttribute_id, cPreview_id) Loop
          If Get_Attribute_None_Cache(rec.attribute_id) = 'Y' then
            vNoneCache := vNoneCache || Cache_Line(rec.attribute_id,
                                                   rec.Attribute_Name,
                                                   rec.VALUE);
          End If;
        End Loop;
        
        If vNoneCache is not null then
          vNoneCache := '----- Not in Cache ------'||chr(10)||vNoneCache;
          vCache := vCache||vNoneCache; 
        End If;
        
      End If;
 

    vCache := Replace(vCache, '<>  <>' || Chr(10), Null);

    Return(vCache);

  End Get_Transaction_Cache;

  FUNCTION Execute_Immediate_CLOB(cStatement CLOB, cParse_Only in varchar2, cSQLERRM out varchar2)
    Return BOOLEAN IS

    ds_cur    PLS_INTEGER := dbms_sql.open_cursor;
    sql_table dbms_sql.VARCHAR2S;

    c_buf_len CONSTANT BINARY_INTEGER := 256;
    vSQLERRM Varchar2(512);
    v_accum  INTEGER := 0;
    v_beg    INTEGER := 1;
    v_end    INTEGER := 256;
    v_loblen PLS_INTEGER;
    v_RetVal PLS_INTEGER;

    vSuccess Boolean := False;

    FUNCTION next_row(clob_in IN CLOB,
                      len_in  IN INTEGER,
                      off_in  IN INTEGER) RETURN VARCHAR2 IS

    BEGIN

      RETURN DBMS_LOB.SUBSTR(clob_in, len_in, off_in);

    END next_row;

  BEGIN

    Begin

      v_loblen := DBMS_LOB.GETLENGTH(cStatement);

      LOOP

        -- Set the length to the remaining size
        -- if there are < c_buf_len characters remaining.

        IF v_accum + c_buf_len > v_loblen THEN
          v_end := v_loblen - v_accum;
        END IF;

        sql_table(NVL(sql_table.LAST, 0) + 1) := next_row(cStatement,
                                                          v_end,
                                                          v_beg);

        v_beg   := v_beg + c_BUF_LEN;
        v_accum := v_accum + v_end;

        IF v_accum >= v_loblen THEN
          EXIT;
        END IF;

      END LOOP;

      -- Parse the pl/sql and execute it

      dbms_sql.PARSE(ds_cur,
                     sql_table,
                     sql_table.FIRST,

                     sql_table.LAST,
                     FALSE,
                     dbms_sql.NATIVE);

    If cParse_Only = 'N' then
        v_RetVal := dbms_sql.EXECUTE(ds_cur);
      End If;
      dbms_sql.CLOSE_CURSOR(ds_cur);

      vSuccess := True;

    Exception
      when OTHERS then
        cSQLERRM := SQLERRM;

        If DBMS_SQl.IS_OPEN(ds_cur) then
          DBMS_SQL.CLOSE_CURSOR(ds_cur);
        End If;
    End;
    Return(vSuccess);
  END Execute_Immediate_CLOB;

  -- Overloaded to return Rows Processed
  FUNCTION Execute_Immediate_CLOB(cStatement      in  CLOB, 
                                  cParse_Only     in  varchar2, 
                                  cSQLERRM        out varchar2,
                                  cRows_Processed out number)
    Return BOOLEAN IS

    ds_cur    PLS_INTEGER := dbms_sql.open_cursor;
    sql_table dbms_sql.VARCHAR2S;

    c_buf_len CONSTANT BINARY_INTEGER := 256;
    vSQLERRM Varchar2(512);
    v_accum  INTEGER := 0;
    v_beg    INTEGER := 1;
    v_end    INTEGER := 256;
    v_loblen PLS_INTEGER;
    v_RetVal PLS_INTEGER;

    vSuccess Boolean := False;

    FUNCTION next_row(clob_in IN CLOB,
                      len_in  IN INTEGER,
                      off_in  IN INTEGER) RETURN VARCHAR2 IS

    BEGIN

      RETURN DBMS_LOB.SUBSTR(clob_in, len_in, off_in);

    END next_row;

  BEGIN

    Begin

      v_loblen := DBMS_LOB.GETLENGTH(cStatement);

      LOOP

        -- Set the length to the remaining size
        -- if there are < c_buf_len characters remaining.

        IF v_accum + c_buf_len > v_loblen THEN
          v_end := v_loblen - v_accum;
        END IF;

        sql_table(NVL(sql_table.LAST, 0) + 1) := next_row(cStatement,
                                                          v_end,
                                                          v_beg);

        v_beg   := v_beg + c_BUF_LEN;
        v_accum := v_accum + v_end;

        IF v_accum >= v_loblen THEN
          EXIT;
        END IF;

      END LOOP;

      -- Parse the pl/sql and execute it

      dbms_sql.PARSE(ds_cur,
                     sql_table,
                     sql_table.FIRST,

                     sql_table.LAST,
                     FALSE,
                     dbms_sql.NATIVE);

    If cParse_Only = 'N' then
        v_RetVal := dbms_sql.EXECUTE(ds_cur);
      End If;
      dbms_sql.CLOSE_CURSOR(ds_cur);
      
      vSuccess := True;

    Exception
      when OTHERS then
        cSQLERRM := SQLERRM;

        If DBMS_SQl.IS_OPEN(ds_cur) then
          DBMS_SQL.CLOSE_CURSOR(ds_cur);
        End If;
    End;
    -- Set Rows Processed OUT param
    cRows_Processed := v_RetVal;
    Return(vSuccess);
  END Execute_Immediate_CLOB;

  -- *************************************************************
  --                     Execute_Immediate
  -- This calls the PLSQL Execute Immediate command for the
  -- Custom Events from and return any given message
  -- *************************************************************
  Function Execute_Immediate(cStatement in varchar2, cSQLERRM out varchar2)
    Return Boolean is

    vSQLERRM Varchar2(512);
    vSuccess Boolean := False;

  Begin
    Begin
      Execute Immediate cStatement;

      vSuccess := True;

    Exception
      when OTHERS then
        vSQLERRM := SQLERRM;

    End;

    cSQLERRM := vSQLERRM;
    Return(vSuccess);
  End Execute_Immediate;

  --
  -- Check_Security
  --
  Function Check_Security(cCheck_String in Varchar2) Return Boolean is
    vResult Number;

  begin

    select to_number(lpad(cCheck_String, 16), 'XXXXXXXXXXXXXXX')
      into vResult
      from dual;

    If vResult in (140668, 120354) then
      Return(TRUE);
    Else
      Return(FALSE);
    End If;

  End Check_Security;

  --
  -- Set_Security
  --
  Function Set_Security(cInput_number in number) Return Varchar2 is

    vResult varchar2(100);

  Begin
    Select to_char(cInput_number, 'XXXXXXXXXXXXXXX')
      into vResult
      from dual;
    Return(ltrim(vResult));
  End Set_Security;

  --
  -- Preview_Mode_Transaction
  --
  Function Preview_Mode_Transaction(cPreview_id in number, cHeader_id in number) return boolean is
  Begin
    -- Remove old records from Preview Tables
    Begin
       Delete from xxcp_pv_transaction_header
        where preview_id = cPreview_id
          and header_id = cHeader_id;
      Exception when others then null;
    End;
    Begin
       Delete xxcp_pv_transaction_cache x
        where preview_id = cPreview_id;
      Exception when others then null;
    End;
    Begin
       Delete from xxcp_pv_transaction_attributes
        where preview_id = cPreview_id
          and header_id = cHeader_id;
      Exception when others then null;
    End;

    Begin
       Insert into xxcp_pv_transaction_header(
                 preview_id
                ,header_id
                ,source_assignment_id
                ,parent_trx_id
                ,source_table_id
                ,trading_set_id
                ,transaction_date
                ,set_of_books_id
                ,owner_tax_reg_id
                ,owner_legal_curr
                ,owner_legal_exch_rate
                ,owner_assoc_id
                ,owner_assign_rule_id
                ,exchange_date
                ,frozen
                ,internal_error_code
                ,transaction_ref1
                ,transaction_ref2
                ,transaction_ref3
                ,transaction_currency
                ,transaction_exch_rate
                ,created_by
                ,creation_date
                ,last_updated_by
                ,last_update_date
                ,last_update_login
        ) Select
                 cPreview_id
                ,HEADER_ID
                ,SOURCE_ASSIGNMENT_ID
                ,PARENT_TRX_ID
                ,SOURCE_TABLE_ID
                ,TRADING_SET_ID
                ,TRANSACTION_DATE
                ,SET_OF_BOOKS_ID
                ,OWNER_TAX_REG_ID
                ,OWNER_LEGAL_CURR
                ,OWNER_LEGAL_EXCH_RATE
                ,OWNER_ASSOC_ID
                ,OWNER_ASSIGN_RULE_ID
                ,EXCHANGE_DATE
                ,FROZEN
                ,INTERNAL_ERROR_CODE
                ,TRANSACTION_REF1
                ,TRANSACTION_REF2
                ,TRANSACTION_REF3
                ,TRANSACTION_CURRENCY
                ,TRANSACTION_EXCH_RATE
                ,CREATED_BY
                ,CREATION_DATE
                ,LAST_UPDATED_BY
                ,LAST_UPDATE_DATE
                ,LAST_UPDATE_LOGIN
         from xxcp_transaction_header
        where header_id = cHeader_id;

        Exception when NO_DATA_FOUND then null;
      End;

    Begin
       Insert into xxcp_pv_transaction_attributes(
             preview_id
            ,attribute_id
            ,header_id
            ,source_assignment_id
            ,parent_trx_id
            ,transaction_id
            ,trading_set_id
            ,source_table_id
            ,source_type_id
            ,source_class_id
            ,transaction_date
            ,set_of_books_id
            ,partner_required
            ,partner_assoc_id
            ,partner_tax_reg_id
            ,partner_legal_exch_rate
            ,partner_legal_curr
            ,partner_assign_rule_id
            ,exchange_date
            ,tc_qualifier1
            ,tc_qualifier2
            ,mc_qualifier1
            ,mc_qualifier2
            ,ed_qualifier1
            ,ed_qualifier2
            ,frozen
            ,internal_error_code
            ,transaction_ref1
            ,transaction_ref2
            ,transaction_ref3
            ,owner_tax_reg_id
            ,adjustment_rate
            ,adjustment_rate_id
            ,quantity
            ,uom
            ,ic_unit_price
            ,ic_trade_tax_id
            ,ic_currency
            ,ic_control
            ,price_method_id
            ,attribute1
            ,attribute2
            ,attribute3
            ,attribute4
            ,attribute5
            ,attribute6
            ,attribute7
            ,attribute8
            ,attribute9
            ,attribute10
            ,created_by
            ,creation_date
            ,last_updated_by
            ,last_update_date
            ,last_update_login
            ,ic_tax_rate
            ,mc_qualifier3
            ,mc_qualifier4
        ) Select
            cPREVIEW_ID
           ,attribute_id
           ,header_id
           ,source_assignment_id
           ,parent_trx_id
           ,transaction_id
           ,trading_set_id
           ,source_table_id
           ,source_type_id
           ,source_class_id
           ,transaction_date
           ,set_of_books_id
           ,partner_required
           ,partner_assoc_id
           ,partner_tax_reg_id
           ,partner_legal_exch_rate
           ,partner_legal_curr
           ,partner_assign_rule_id
           ,exchange_date
           ,tc_qualifier1
           ,tc_qualifier2
           ,mc_qualifier1
           ,mc_qualifier2
           ,ed_qualifier1
           ,ed_qualifier2
           ,frozen
           ,internal_error_code
           ,transaction_ref1
           ,transaction_ref2
           ,transaction_ref3
           ,owner_tax_reg_id
           ,adjustment_rate
           ,adjustment_rate_id
           ,quantity
           ,uom
           ,ic_unit_price
           ,ic_trade_tax_id
           ,ic_currency
           ,ic_control
           ,price_method_id
           ,attribute1
           ,attribute2
           ,attribute3
           ,attribute4
           ,attribute5
           ,attribute6
           ,attribute7
           ,attribute8
           ,attribute9
           ,attribute10
           ,created_by
           ,creation_date
           ,last_updated_by
           ,last_update_date
           ,last_update_login
           ,ic_tax_rate
           ,mc_qualifier3
           ,mc_qualifier4
        from xxcp_transaction_attributes
        where header_id = cHeader_id;

        Exception when NO_DATA_FOUND then null;
      End;

      Begin
        Insert into xxcp_pv_transaction_cache(
            PREVIEW_ID
           ,attribute_id
           ,seq
           ,cached_qty
           ,cached_value1
           ,cached_value2
           ,cached_value3
           ,cached_value4
           ,cached_value5
           ,cached_value6
           ,cached_value7
           ,cached_value8
           ,cached_value9
           ,cached_value10
           ,cached_value11
           ,cached_value12
           ,cached_value13
           ,cached_value14
           ,cached_value15
           ,cached_value16
           ,cached_value17
           ,cached_value18
           ,cached_value19
           ,cached_value20
           ,cached_value21
           ,cached_value22
           ,cached_value23
           ,cached_value24
           ,cached_value25
           ,cached_value26
           ,cached_value27
           ,cached_value28
           ,cached_value29
           ,cached_value30
           ,cached_value31
           ,cached_value32
           ,cached_value33
           ,cached_value34
           ,cached_value35
           ,cached_value36
           ,cached_value37
           ,cached_value38
           ,cached_value39
           ,cached_value40
           ,cached_value41
           ,cached_value42
           ,cached_value43
           ,cached_value44
           ,cached_value45
           ,cached_value46
           ,cached_value47
           ,cached_value48
           ,cached_value49
           ,cached_value50
           )
        Select
            cPREVIEW_ID
           ,attribute_id
           ,seq
           ,cached_qty
           ,cached_value1
           ,cached_value2
           ,cached_value3
           ,cached_value4
           ,cached_value5
           ,cached_value6
           ,cached_value7
           ,cached_value8
           ,cached_value9
           ,cached_value10
           ,cached_value11
           ,cached_value12
           ,cached_value13
           ,cached_value14
           ,cached_value15
           ,cached_value16
           ,cached_value17
           ,cached_value18
           ,cached_value19
           ,cached_value20
           ,cached_value21
           ,cached_value22
           ,cached_value23
           ,cached_value24
           ,cached_value25
           ,cached_value26
           ,cached_value27
           ,cached_value28
           ,cached_value29
           ,cached_value30
           ,cached_value31
           ,cached_value32
           ,cached_value33
           ,cached_value34
           ,cached_value35
           ,cached_value36
           ,cached_value37
           ,cached_value38
           ,cached_value39
           ,cached_value40
           ,cached_value41
           ,cached_value42
           ,cached_value43
           ,cached_value44
           ,cached_value45
           ,cached_value46
           ,cached_value47
           ,cached_value48
           ,cached_value49
           ,cached_value50
         from xxcp_transaction_cache
          where attribute_id = any(
                  select attribute_id
                    from xxcp_transaction_attributes a,
                         xxcp_transaction_header h
                    where h.header_id = a.header_id);

        Exception when NO_DATA_FOUND then null;
      End;

    Return(True);

  End Preview_Mode_Transaction;

  -- *************************************************************
  --                      make_user_profile
  -- *************************************************************
  Procedure Make_User_Profile(cUser_id in number) is

      pragma autonomous_transaction;

    Cursor nx (pUser_id in number) is
    select user_name from fnd_user where user_id = pUser_id;

    vUser_name varchar2(100);

    -- Create User table
    vCnt integer := 0;
  Begin

     If cUser_id > 0 then

       Select count(*) into vCnt
       from xxcp_user_profiles
       where user_id = cUser_id;

      If vCnt = 0 then

        For Rec in NX(cUser_id) Loop
          vUser_name := Rec.User_name;
        End Loop;

        Insert into XXCP_USER_PROFILES(user_id, creation_date, pv_trx_removal, created_by, active,  user_name,
                                     Language, last_update_login, last_update_date, last_updated_by)
         values(cUser_id,sysdate, 3 , cUser_id, 'Y', vUser_Name,
           'ENGLISH', fnd_global.login_id, sysdate, cUser_id);

        Commit;
      End If;
    End If;
  End Make_User_Profile;


  -- *************************************************************
  --                        make_history_ctl
  -- *************************************************************
  Function Make_History_Ctl(cTable_name in varchar2) return boolean is

  Cursor c1(pTable_name in varchar2) is
  select pTable_name transaction_table, h.history_column_name, h.source_column_name, h.locked, h.mandatory, h.default_value
    from xxcp_history_ctl h
   where transaction_table = 'GL_INTERFACE' -- Default
     and not (pTable_name) = any(select distinct w.transaction_table from xxcp_history_ctl w);

  Cursor c2(pTable_Name in varchar2) is
    select count(*) cnt from xxcp_history_ctl w
      where transaction_table = pTable_Name;

  vCnt   number  := 0;
  vExist Boolean := FALSE;

  Begin

    For Rec in c1(cTable_Name) Loop

      Insert into xxcp_history_ctl h
        (transaction_table, history_column_name, locked, mandatory, default_value)
      Values (cTable_Name, rec.history_column_name, rec.locked, rec.mandatory, rec.default_value);

      Commit;
      vExist := TRUE;

    End Loop;

    If vCnt = 0 then
      For Rec in c2(cTable_Name) Loop

        vExist := TRUE;

      End Loop;
    End If;

    Return(vExist);

  End Make_History_Ctl;

  --
  -- Administer_User_Profile
  --
  Function Administer_User_Profile(cUser_id            in number,
                                   cUser_Name          in varchar2,
                                   cSource_id          in number,
                                   cVerification       in varchar2,
                                   cEffective_From     in date default sysdate,
                                   cEffective_To       in date,
                                   cRemove_User        in varchar2 default 'N'
                                   ) return boolean is


   vResult boolean := False;


   cursor FN(pUser_id in number, pUser_Name in varchar2) is
     select user_name, user_id
     from fnd_user
     where user_id > 0
       and (user_id = pUser_id
       or user_name = pUser_Name);

   vUser_Name fnd_user.user_name%type;
   vUser_id   fnd_user.user_id%type;

  Begin

    If cVerification = '442476236932'
    then

      -- Get User Name
      For Rec in FN(cUser_id, cUser_Name) loop
        vUser_Name := Rec.user_name;
        vUser_id   := rec.user_id;
      End loop;

      Show('User id = '||vUser_id||' User Name = '||vUser_Name);

      If vUser_Name is not null then

        Make_User_Profile(cUser_id => vUser_id);

        If cRemove_User = 'Y' then
        Begin

            If cSource_id is null then
              Delete from xxcp_user_sources where user_id = vUser_id;
              Delete from xxcp_user_profiles where user_id = vUser_id;
            Else

              Delete from xxcp_user_sources where user_id = vUser_id and source_id = cSource_id;
            End If;
            vResult := True;
            Commit;
            Exception when Others then null;
          End;
        Else
           --
           If cSource_id between 0 and 1000 then
              Begin

               Delete from xxcp_user_sources where source_id = cSource_id and user_id = vUser_id;

               Insert into xxcp_user_sources(user_id, source_id, effective_from_date, effective_to_date,
                         active, created_by, creation_date, last_updated_by, last_update_date, last_update_login)
               values(vUser_id, cSource_id, trunc(nvl(cEffective_From,sysdate)), trunc(cEffective_To), 'Y', fnd_global.USER_ID,
                         sysdate,fnd_global.user_id, sysdate, fnd_global.login_id);

               vResult := True;

               Commit;
               Exception when Others then null;
             End;
           End If;
        End If;
      End If;

    End If;

    Return(vResult);

  End Administer_User_Profile;

  --
  -- Administer_Resp_Profile
  --
  Function Administer_Resp_Profile(cResponsibility_id   in number,
                                   cSource_id           in varchar2,
                                   cVerification        in varchar2,
                                   cEffective_From      in date default sysdate,
                                   cEffective_To        in date,
                                   cDisable_User        in varchar2 default 'N',
                                   cRemove_User         in varchar2 default 'N'
                                   ) return boolean is



   vResult boolean := False;
   vCnt    number(10);

  Begin

    If cVerification = '442476236932'
    then

      Show('Resp id = '||to_char(cResponsibility_id));

      If cResponsibility_id is not null then

        If cRemove_User = 'Y' then
        Begin
            Delete
               from xxcp_responsibility_sources t
              where t.responsibility_id = cResponsibility_id
                and source_id = cSource_id;

            Show('Removed');

            vResult := True;
            Commit;
            Exception when Others then null;
          End;
        ElsIf cDisable_User = 'Y' then
          Begin
             Update xxcp_responsibility_sources t
                set t.effective_to_date = sysdate
              where t.responsibility_id = cResponsibility_id
                and source_id = cSource_id;

            Exception when others then null;

          End;
        Else


           -- Add Source
           Select count(*) into vCnt
             from xxcp_responsibility_sources t
              where t.responsibility_id = cResponsibility_id
                and source_id = cSource_id;

            If vCnt > 0 then
              Delete
               from xxcp_responsibility_sources t
              where t.responsibility_id = cResponsibility_id
                and source_id = cSource_id;

             End If;

             Begin

               Insert into xxcp_responsibility_sources(RESPONSIBILITY_ID, responsibility_name, source_id, effective_from_date, effective_to_date,
                         created_by, creation_date, last_updated_by, last_update_date, last_update_login)
               select cResponsibility_id, w.responsibility_name,  cSource_id, trunc(nvl(cEffective_From,sysdate)), trunc(cEffective_To), fnd_global.USER_ID,
                         sysdate,fnd_global.user_id, sysdate, fnd_global.login_id
                         from fnd_responsibility_tl w
                         where w.language = 'US'
                           and w.responsibility_id = cResponsibility_id;

               vResult := True;

               Show('Added/Updated');

               Commit;
               Exception when Others then null;
             End;
        End If;
      End If;

    End If;

    Return(vResult);

  End Administer_Resp_Profile;

  -- *************************************************************
  --                      set_user_profile
  -- *************************************************************
  Procedure Set_User_Profile(cUser_id in number, cField_Name in varchar2, cValue in varchar2 , cType in varchar2, cInternalErrorCode out number) is

    --pragma autonomous_transaction;

   vStatement_code varchar2(4000);
   vSQLERRM        varchar2(512);
   vCnt            integer;

  Begin

    If cUser_id > 0 then

     Select count(*) into vCnt
      from xxcp_user_profiles
     where user_id = cUser_id;
     -- Ensure that a profile exists.
     If vCnt = 0 then
       make_user_profile(cUser_id);
     End If;

     If cField_Name is not null and cValue is not null then
      Begin
        If Upper(cType) = 'C' then
         vStatement_code :=
         'update xxcp_user_profiles set '||cField_Name||' = '''||cValue||''', last_update_date = sysdate, last_updated_by = :1, last_update_login = :2 where active = ''Y'' and user_id = :3';
        Else
         vStatement_code :=
         'update xxcp_user_profiles set '||cField_Name||' = '||cValue||', last_update_date = sysdate, last_updated_by = :1, last_update_login = :2 where active = ''Y'' and user_id = :3';
        End If;

        EXECUTE IMMEDIATE vStatement_code USING cUser_id, fnd_global.login_id , cUser_id;

      Commit;

      Exception
         when OTHERS then
           vSQLERRM := SQLERRM; -- New to trap Oracle's Reason for not working.

      End;

      If vSQLERRM is not null then
        FndWriteError(100,'Set_User_profile failed:'||vSQLERRM,vStatement_code);
      End If;
     End If;
    End If;
  End Set_User_Profile;

  -- *************************************************************
  --                      get_user_profile
  -- *************************************************************
  Function Get_User_Profile(cUser_id in number, cField_Name in varchar2, cInternalErrorCode out number) return varchar2 is

   vStatement_code varchar2(4000);
   vSQLERRM        varchar2(512);
   vReturned_Value varchar2(500);

  Begin

    If cField_Name is not null then
     Begin
      vStatement_code :=
      'select '||cField_Name||' from xxcp_user_profiles where active = ''Y'' and user_id = :1';

      EXECUTE IMMEDIATE vStatement_code INTO vReturned_Value USING cUser_id;

      Exception
         when OTHERS then
           vSQLERRM := SQLERRM; -- New to trap Oracle's Reason for not working.
      End;
    End If;
    Return(vReturned_Value);
  End Get_User_Profile;

  -- *************************************************************
  --                     compile_custom_events
  -- *************************************************************
  Function Compile_Custom_Events Return number is

   vHeader_Statement Long;
   vStatement        Long;
   vSQLERRM          Varchar2(512);

   cursor PKHDR is
   select e.HEADER Code2
   from XXCP_CUSTOM_EVENT_CODE e;

   Cursor PKBDY is
   select e.DEFINITION_BLOCK||chr(10)||
          Decode(e.VERSION_CONTROL,Null,Null,e.VERSION_CONTROL||chr(10))||
          Decode(e.FIXED_VARIABLES,Null,Null,e.FIXED_VARIABLES||chr(10))||
          Decode(e.DECLARE_BLOCK,Null,Null,e.DECLARE_BLOCK||chr(10))||
          chr(10)||
          'BEGIN '||
          chr(10)||
          Decode(Active,'Y',Decode(e.Execution_BLOCK,Null,Decode(code_type,'F',Null,' Null;'||chr(10)),e.Execution_BLOCK||chr(10)),' Null;'||chr(10))||
          Decode(e.RETURN_BLOCK,Null,Null,e.RETURN_BLOCK||chr(10))||
          'END '||e.Code_Name||';' Code1
   from XXCP_CUSTOM_EVENT_CODE e;

   vInternalErrorCode number := 950;

  BEGIN

    Update XXCP_CUSTOM_EVENT_CODE c set Compilied_Code = 'N' where nvl(c.Active,'Y') = 'Y';
    Commit;

    --
    -- HEADER for Custom Events
    --
    vHeader_Statement := vHeader_Statement||'CREATE OR REPLACE PACKAGE XXCP_CUSTOM_EVENTS AS'||chr(10)||chr(10);
    vHeader_Statement := vHeader_Statement||'gNew_Transaction Varchar2(1) := ''N'';'||chr(10);

    FOR REC IN PKHDR LOOP
      vHeader_Statement := vHeader_Statement||REC.CODE2||chr(10)||chr(10);
    END LOOP;

    vHeader_Statement := vHeader_Statement||chr(10)||'END XXCP_CUSTOM_EVENTS;';

    If xxcp_forms.Execute_Immediate(vHeader_Statement, vSQLERRM) then

      vStatement := vStatement||'CREATE OR REPLACE PACKAGE BODY XXCP_CUSTOM_EVENTS AS'||chr(10)||chr(10);

      vStatement := vStatement||
      ' gCommon        xxcp_global.gCOMMON_REC;'||chr(10)||chr(10)||
      ' gGrouping_Rule varchar2(1) := ''N'';'||chr(10)||
      ' gPreview_id    number      := 0;'||chr(10)||
      ' gPreview_Mode  varchar2(1) := ''N'';'||chr(10)||
      ' gSysdate       date;'||chr(10)||
      ' gTrace_Mode    varchar2(1) := ''N'';'||chr(10)||
      ' gRequest_id    number      := 0;'||chr(10)||
      ' gUserId        number      := 0;'||chr(10)||chr(10);

      FOR REC IN PKBDY LOOP
        vStatement := vStatement||REC.CODE1||chr(10);
        If Instr(upper(REC.CODE1),'COMMIT;') > 0 then
          vStatement := vStatement||'** ERROR: Commit is not allowed!!'||chr(10)||chr(10);
        Else
          vStatement := vStatement||chr(10);
        End If;
      END LOOP;

      vStatement := vStatement||chr(10)||'END XXCP_CUSTOM_EVENTS;';

      If xxcp_forms.Execute_Immediate(vHeader_Statement, vSQLERRM) then
        vInternalErrorCode := 0;
        Show('Custom Events Compiled - OK');
        Update XXCP_CUSTOM_EVENT_CODE c set Compilied_Code = 'Y';
        Commit;
      Else
        FndWriteError(950,'Custom Events Body Not Compiled',vSQLERRM);
        Show('Custom Events Body failed:'||vSQLERRM);
      End If;
    Else
      FndWriteError(950,'Custom Events Header Not Compiled',vSQLERRM);
      Show('Custom Events Header failed:'||vSQLERRM);
    End If;

    Return(vInternalErrorCode);

  End Compile_Custom_Events;

  -- *************************************************************
  --                        Condition_Test
  -- *************************************************************
  Function Condition_Test( cField_Value       in  varchar2
                         , cCondition         in  varchar2
                         , cData_Type         in  varchar2
                         , cInternalErrorCode out number) Return boolean is

   vExStr             varchar2(1024);
   vSQLERRM           varchar2(512);
   vReturn_Value      varchar2(500);
   vInternalErrorCode number;
   vField_Value       varchar2(250);

    Begin

     vField_Value := cField_Value;

     If cData_Type in ('VARCHAR2','DATE') then
       vField_Value := ''''||vField_Value||'''';
     End If;

     vExStr := 'Begin if '||vField_Value||' '||cCondition||' then :1 := ''Y''; else :1 := ''N''; End If; End;';

     Begin
       EXECUTE IMMEDIATE vExStr using OUT vReturn_Value;
     Exception
      when OTHERS then
        vSQLERRM := SQLERRM; -- New to trap Oracle's Reason for not working.
     End;

     If vSQLERRM is not null then
       Show('vSQLERRM '||vSQLERRM);
     End If;

     If vReturn_Value = 'Y' then
       Return(TRUE);
     Else
       Return(FALSE);
     End If;

  End Condition_Test;


  -- *************************************************************
  --                    WRITE_ED_AUDIT
  -- *************************************************************
  PROCEDURE Write_ED_Audit(cAudit_Reference in varchar2, cUser_id in number default -1, cSQLERRM OUT Varchar2) IS

  vResult   CLOB;
  vQuery    VARCHAR2(2000);
  vQueryCtx DBMS_XMLQUERY.ctxType;
  vRefCur   SYS_REFCURSOR;

  Cursor c1(pUser_id in number) is
   select e.table_name, e.interface_id
     from xxcp_edit_interface e
    where e.security_level = 3
      and e.last_update_date > e.creation_date
      and e.user_id = pUser_id;

  Cursor c2(pUser_id in number) is
      select a.table_name, a.column_name
      from xxcp_edit_interface a
     where a.security_level = 3
       and a.last_update_date > a.creation_date
       and a.user_id = pUser_id
       and nvl(a.old_value,'~nvl~') != nvl(a.new_value,'~nvl~') ;

  vInT_Table_Name VARCHAR2(30);
  vSQLERRM        VARCHAR2(512);
  vInterface_id   NUMBER;
  vDateTime       Date;
  vUser_id        NUMBER;

  -- Write to table
  PROCEDURE insAuditTable (cTableName       IN  VARCHAR2
                          ,cInterface_id    IN  NUMBER
                          ,cDateTime        IN  DATE
                          ,cChangedRecord   IN  CLOB) IS

--  PRAGMA AUTONOMOUS_TRANSACTION;

  BEGIN


    INSERT INTO XXCP_AUDIT_B
     (
     AUDIT_ID,
     INDEX_KEY_ID,
     TABLE_NAME,
     ACTION,
     CHANGED_RECORD,
     CREATION_DATE,
     CREATED_BY,
     LAST_UPDATE_DATE,
     LAST_UPDATED_BY,
     LAST_UPDATE_LOGIN
     )
     VALUES
       (
        XXCP_AUDIT_ID_SEQ.NEXTVAL,
        cInterface_id,
        cTableName,
        'E',
        XMLTYPE(cChangedRecord),
        cDateTime,
        vUser_id,
        sysdate,
        vUser_id,
        fnd_global.login_id
       );

    End insAuditTable;

  BEGIN
    -- take care of form lossing user_id
    vUser_id := fnd_global.user_id;
    If vUser_id = -1 then
      vUser_id := cUser_id;
    End If;

    -- Get Table Name
    For Rec in c1(vUser_id) loop
      vInt_Table_Name := Rec.Table_name;
      vInterface_id   := Rec.Interface_id;
      Exit;
    End Loop;

   vDateTime := Sysdate; -- This gives use the same date and time for all updates in this action.

   For Rec in c2(vUser_id) loop -- Create one audit record by column so that you can query it back.
    Begin
    -- Build Query
    vQuery :=
    'select a.rowid, a.table_name, a.column_name, a.old_value, a.new_value, a.interface_id, a.transaction_id, a.column_description,
            a.source_assignment_id, a.source_rowid, a.user_id, a.last_updated_by, a.last_update_date, '':SUB3'' audit_reference
      from xxcp_edit_interface a
     where a.security_level = 3
       and a.last_update_date > a.creation_date
       --
       and a.column_name = '':SUB1''
       and a.table_name  = '':SUB2''
       --
       and a.user_id = :SUB4
       and nvl(a.old_value,''~nvl~'') != nvl(a.new_value,''~nvl~'')';

    vQuery := Replace(vQuery,':SUB1',Rec.Column_Name);
    vQuery := Replace(vQuery,':SUB2',Rec.Table_Name);
    vQuery := Replace(vQuery,':SUB3',nvl(cAudit_Reference,'-'));
    vQuery := Replace(vQuery,':SUB4',vUser_id);

    OPEN vRefCur FOR vQuery;

      vQueryCtx := DBMS_XMLQUERY.newContext(vQuery);

      DBMS_XMLQUERY.setDateFormat(vQueryCtx,'dd/MM/yyyy HH:mm:ss');
      DBMS_XMLQUERY.setRaiseNoRowsException( vQueryCtx,FALSE );
      DBMS_LOB.createtemporary( vResult, TRUE, dbms_lob.SESSION );

      vResult := DBMS_XMLQUERY.getXML(vQueryCtx);

    DBMS_XMLGEN.closeContext (vQueryCtx);

    insAuditTable(vInt_Table_Name, vInterface_id, vDateTime, vResult);

    EXCEPTION WHEN OTHERS THEN

      vSQLERRM := SQLERRM;
      DBMS_XMLGEN.closeContext (vQueryCtx);
      Exit;
   End;
  End Loop;

  cSQLERRM := vSQLERRM;

  END Write_ED_Audit;

   -- *************************************************************
  --                    WRITE_ED_AUDIT
  -- *************************************************************
  PROCEDURE Write_ED_Audit(cUser_id in number default -1, cSQLERRM OUT Varchar2) IS
   Begin
     Write_Ed_Audit(Null, cUser_id, cSQLERRM);
   End Write_ED_Audit;
  -- *************************************************************
  --                    WRITE_ED_AUDIT_PARENT
  -- *************************************************************
  PROCEDURE Write_ED_Audit_Parent(cUser_id in number default -1
                                 ,cTable_Name in varchar2
                                 ,cCol_Name in varchar2
                                 ,cOld_Value in varchar2
                                 ,cNew_Value in varchar2
                                 ,cInterface_Id in number
                                 ,cTransaction_Id in number
                                 ,cAudit_Reference in varchar2
                                 ,cSQLERRM OUT Varchar2) IS

    vResult   CLOB;
    vQuery     VARCHAR2(2000);
    vQueryCtx DBMS_XMLQUERY.ctxType;
    vRefCur   SYS_REFCURSOR;

    Cursor c1(pUser_id in number) is
     select e.table_name, e.interface_id
       from xxcp_edit_interface e
      where e.security_level = 3
        and e.last_update_date > e.creation_date
        and e.user_id = pUser_id;

    Cursor c2(pUser_id in number) is
        select a.table_name, a.column_name
        from xxcp_edit_interface a
       where a.security_level = 3
         and a.last_update_date > a.creation_date
         and a.user_id = pUser_id
         and nvl(a.old_value,'~nvl~') != nvl(a.new_value,'~nvl~') ;

    vInT_Table_Name VARCHAR2(30);
    vSQLERRM        VARCHAR2(512);
    vInterface_id   NUMBER;
    vDateTime       Date;
    vUser_id        NUMBER;

  -- Write to table
  PROCEDURE insAuditTable (cTableName       IN  VARCHAR2
                          ,cInterface_id    IN  NUMBER
                          ,cDateTime        IN  DATE
                          ,cChangedRecord   IN  CLOB) IS

--  PRAGMA AUTONOMOUS_TRANSACTION;

  BEGIN


    INSERT INTO XXCP_AUDIT_B
     (
     AUDIT_ID,
     INDEX_KEY_ID,
     TABLE_NAME,
     ACTION,
     CHANGED_RECORD,
     CREATION_DATE,
     CREATED_BY,
     LAST_UPDATE_DATE,
     LAST_UPDATED_BY,
     LAST_UPDATE_LOGIN
     )
     VALUES
       (
        XXCP_AUDIT_ID_SEQ.NEXTVAL,
        cInterface_id,
        cTableName,
        'E',
        XMLTYPE(cChangedRecord),
        cDateTime,
        vUser_id,
        sysdate,
        vUser_id,
        fnd_global.login_id
       );

    End insAuditTable;

  BEGIN

    vUser_id := cuser_id;

     -- Get Table Name
    For Rec in c1(vUser_id) loop
      vInt_Table_Name := Rec.Table_name;
      vInterface_id   := Rec.Interface_id;
      Exit;
    End Loop;

   vDateTime := Sysdate; -- This gives use the same date and time for all updates in this action.

    Begin
    -- Build Query
    vQuery :=
    'select a.rowid, a.table_name, a.column_name, '':SUB5'' old_value,'':SUB6'' new_value, '':SUB7'' interface_id, '':SUB8'' transaction_id, a.column_description,
            a.source_assignment_id, a.source_rowid, a.user_id, a.last_updated_by, a.last_update_date, '':SUB3'' audit_reference
      from xxcp_edit_interface a
     where a.security_level = 3
       and a.last_update_date > a.creation_date
       --
       and a.column_name = '':SUB1''
       and a.table_name  = '':SUB2''
       --
       and a.user_id = :SUB4
       and nvl(a.old_value,''~nvl~'') != nvl(a.new_value,''~nvl~'')';

    vQuery := Replace(vQuery,':SUB1',cCol_Name);
    vQuery := Replace(vQuery,':SUB2',cTable_Name);
    vQuery := Replace(vQuery,':SUB3',nvl(cAudit_Reference,'-'));
    vQuery := Replace(vQuery,':SUB4',vUser_id);
    vQuery := Replace(vQuery,':SUB5',cOld_Value);
    vQuery := Replace(vQuery,':SUB6',cNew_Value);
    vQuery := Replace(vQuery,':SUB7',cInterface_Id);
    vQuery := Replace(vQuery,':SUB8',cTransaction_Id);

    OPEN vRefCur FOR vQuery;

      vQueryCtx := DBMS_XMLQUERY.newContext(vQuery);

      DBMS_XMLQUERY.setDateFormat(vQueryCtx,'dd/MM/yyyy HH:mm:ss');
      DBMS_XMLQUERY.setRaiseNoRowsException( vQueryCtx,FALSE );
      DBMS_LOB.createtemporary( vResult, TRUE, dbms_lob.SESSION );

      vResult := DBMS_XMLQUERY.getXML(vQueryCtx);

    DBMS_XMLGEN.closeContext (vQueryCtx);

    insAuditTable(vInt_Table_Name, cInterface_id, vDateTime, vResult);

    EXCEPTION WHEN OTHERS THEN

      vSQLERRM := SQLERRM;
      DBMS_XMLGEN.closeContext (vQueryCtx);
   End;

  cSQLERRM := vSQLERRM;

  End Write_ED_Audit_Parent;
    

  -- *************************************************************
  --                    UPDATE_EDIT_INTERFACE
  -- This procedure will determine from XXCP_EDIT_INTERFACE table
  -- which values have changed and apply those changes at a parent
  -- level to all of the records found on the interface table.
  -- An audit record is also written away for every interface row
  -- changed.
  -- *************************************************************
  PROCEDURE Update_Edit_Interface(cUser_id              IN NUMBER DEFAULT -1
                                 ,cAudit_Reference      IN VARCHAR2
                                 ,cInterface_Table_Name IN VARCHAR2
                                 ,cSource_Assignment_Id IN NUMBER
                                 ,cTransaction_Table    IN VARCHAR2
                                 ,cParent_Trx_Col       IN VARCHAR2
                                 ,cParent_Trx_Val       IN VARCHAR2
                                 ,cErr                  OUT VARCHAR2) IS
  
    CURSOR b1(pUser_id IN NUMBER) IS
      SELECT e.table_name
            ,e.source_rowid
            ,', ' || e.column_name || ' = ' ||
             decode(e.new_value
                   ,NULL
                   ,'NULL'
                   ,decode(substr(data_type
                                 ,1
                                 ,1)
                          ,'V'
                          ,''''
                          ,NULL) || e.new_value ||
                    decode(substr(data_type
                                 ,1
                                 ,1)
                          ,'V'
                          ,''''
                          ,NULL)) || chr(10) col
            ,e.old_value ccol
            ,e.new_value new_val
            ,e.column_name col_name
        FROM xxcp_edit_interface e
       WHERE e.security_level = 3
         AND e.last_update_date > e.creation_date
         AND e.user_id = pUser_id
         AND NVL(e.old_value
                ,'~nvl~') != NVL(e.new_value
                                ,'~nvl~');
  
    vCnt               NUMBER := 0;
    vStatement         VARCHAR2(32000);
    vSource_Rowid      ROWID;
    vSQLERRM           VARCHAR2(1024);
    vInternalErrorCode NUMBER;
    vAnswer            NUMBER;
    vRowid_Statement   VARCHAR2(1000);
    vRowid             VARCHAR2(18);
    TYPE rowid_cur IS REF CURSOR;
    rowid_row       rowid_cur;
    vCol            VARCHAR2(100);
    vNew_Val        VARCHAR2(4000);
    vUpd_Statement  VARCHAR2(4000);
    vRowid_Append   VARCHAR2(1000);
    vInterface_Id   NUMBER;
    vTransaction_Id NUMBER;
    vCol_Name       VARCHAR2(100);
    vCol_Val        VARCHAR2(4000);
  BEGIN
   
    vStatement := NULL;
    -- Start a cursor for loop for the records on XXCP_EDIT_INTERFACE that have changed.
    FOR Recx IN b1(cUser_id) LOOP
      -- Construct the update statement to update the original interface records.
      IF vCnt = 0 THEN
        vSource_Rowid := Recx.SOURCE_ROWID;
        vStatement    := 'UPDATE ' || Recx.Table_NAME || chr(10) || ' SET ';
        vStatement    := vStatement ||
                         substr(Recx.Col
                               ,2
                               ,Length(Recx.Col));
      ELSE
        vStatement := vStatement || Recx.Col;
      END IF;
    
      vCol     := Recx.cCol;
      vNew_Val := Recx.new_val;
      vCnt     := vCnt + 1;
      vCol_Name:= Recx.Col_Name;
      
      -- Build up a statement for a ref cursor for the intended record set.
      vRowid_Statement := 'select rowid ' ||
                          '      ,vt_interface_id ' ||
                          '      ,vt_transaction_id ' || 
                          '      ,'||vCol_Name||' col_val '||
                          '  from ' || cInterface_Table_Name ||
                          ' where vt_source_assignment_id = ' || cSource_Assignment_Id ||
                          '   and vt_transaction_table    = ''' ||cTransaction_Table || '''' ||
                          '   and ' || cParent_Trx_Col || ' = ''' || cParent_Trx_Val || '''';

      -- For all of the records determined by the ref cursor
      OPEN rowid_row FOR vRowid_Statement;
      LOOP
        vUpd_Statement := NULL;
        vRowid_Append  := NULL;
        FETCH rowid_row
          INTO vRowid
              ,vInterface_Id
              ,vTransaction_Id
              ,vCol_Val;
        EXIT WHEN rowid_row%NOTFOUND;
      
        vRowid_Append := NULL;
        IF vCnt > 0 THEN
        
          SAVEPOINT AX;
          -- Call the procedure to write away audit details
          xxcp_forms.Write_ED_Audit_Parent(cUser_id         => cUser_id
                                          ,cTable_Name      => Recx.Table_NAME
                                          ,cCol_Name        => Recx.Col_Name
                                          ,cOld_Value       => vCol_Val
                                          ,cNew_Value       => vNew_Val
                                          ,cInterface_Id    => vInterface_Id
                                          ,cTransaction_Id  => vTransaction_Id
                                          ,cAudit_reference => cAudit_Reference
                                          ,cSQLERRM         => vSQLERRM);
        
          IF vSQLERRM IS NOT NULL THEN
            -- Send back the error message if there is one.
            cErr := vSQLERRM;
            ROLLBACK;
          ELSE
          
            vRowid_Append  := ' where rowid = ''' || vRowid || '''';
            vUpd_Statement := vStatement || vRowid_Append;
            
            xxcp_forms.Execute_Immediate(vUpd_Statement
                                        ,vSQLERRM
                                        ,vInternalErrorCode);
          
            IF vSQLERRM IS NOT NULL THEN
              -- Send back the error message if there is one.
              cErr := vSQLERRM;
              ROLLBACK;
            ELSE
              COMMIT;
            END IF;
          END IF;
        
        END IF;
      END LOOP;
      CLOSE rowid_row;
    
    END LOOP;
  EXCEPTION
    WHEN OTHERS THEN
      IF vInternalErrorCode IS NOT NULL THEN
        cErr := vInternalErrorCode;
      END IF;
      IF vSQLERRM IS NOT NULL THEN
        cErr := cErr || ' ' || vSQLERRM;
      END IF;
      ROLLBACK;
  END Update_Edit_Interface;
  -- *************************************************************
  --                        Excel_Insert
  -- *************************************************************
  Function Excel_Insert(cSource_Assignment_id in number, 
                        cFileid               in Number,
                        cMapping_Name         in VARCHAR2, 
                        cSQLERRM              OUT VARCHAR2,
                        cCriteria_Set_Id      in number default 0,
                        cRows_Processed       OUT number) return number is


  cursor c1(pSource_Assignment_id in number) is
  select s.source_base_table Interface_Table, s.Source_id, a.set_of_books_id, a.org_id
    from xxcp_source_assignments a,
         xxcp_sys_sources s
   where a.source_assignment_id = pSource_Assignment_id
     and a.source_id = s.source_id;

  cursor c2(pSource_id in number, pMapping_Name in varchar2) is
    select replace(replace(from_column_name,'{',Null),'}',Null) From_Column_Name, 
           to_column_name,
           nvl(trim_data,'N') trim_data,
           nvl(trunc_data,'N') trunc_data,
           data_type,
           data_length
     from xxcp_data_mapping m,
          xxcp_table_set_ctl c
    where m.mapping_set_id  = c.set_id
      and c.source_id       = pSource_id
      and upper(c.set_name) = upper(pMapping_Name)
      and c.set_base_table  = 'CP_DATA_MAPPING'
      and from_column_name is not null
    order by 2;

  cursor c3(pSource_id in number, pMapping_Name in varchar2) is
    select upper(destination_table_name) destination_table_name
    from   XXCP_DATA_MAPPING_TYPE_V
    where  source_id         = pSource_id
    and    upper(mapping_type_name) = upper(pMapping_Name);

  cursor c4(pCriteria_set_id in number) is
    select  case
              when operator <> '* FREETEXT *' then 
                replace(replace(column_name,'{',''),'}','')||' '||operator||' '||replace(replace(criteria,'{',''),'}','')||'' 
              else
                replace(replace(column_name,'{',''),'}','')||' '||replace(replace(criteria,'{',''),'}','')||'' 
            end filter_criteria
    from   xxcp_file_upload_criteria
    where  criteria_set_id = pCriteria_set_id;

  cursor c5 (pCriteria_set_id in number) is
    select attribute2 from_line,
           attribute3 to_line
    from   xxcp_table_set_ctl
    where  set_base_table = 'CP_FILE_UPLOAD_CRITERIA'
    and    set_id = pCriteria_set_id;


  vStr             clob;
  vSQLERRM         varchar2(512);
  vInterface_Table varchar2(30);
  vSet_of_books_id number(15);
  vOrg_id          number(15);
  vSource_id       xxcp_sys_sources.source_id%type;

  vError_Code      Number(8) := 14007; -- No Mapping

  vDestination_Table varchar2(30);
  vIs_Interface       Boolean := TRUE;
  
  vRows_Processed  number := 0;
  vTmp_From_Column varchar2(1000);

  Begin

    For Rec in C1(cSource_Assignment_id) Loop
     vSource_id       := Rec.Source_id;
     vInterface_Table := Rec.Interface_Table;
     vSet_of_books_id := nvl(Rec.Set_of_books_id,0);
     vOrg_id          := nvl(Rec.Org_id,0);
    End Loop;

    Open c3(vSource_id, cMapping_Name);
    Fetch c3 into vDestination_Table;
    Close c3;

    If vInterface_Table <> vDestination_Table then
      vIs_Interface := FALSE;
      vInterface_table := vDestination_Table;
    End if;

    -- Build Dynamic Insert Statement
    vStr := 'Insert into '||vInterface_Table||' g (';
 
    -- Additional columns appended if Table is an Interface Table
    If vIs_Interface then
      vStr := vStr ||' vt_source_assignment_id, '; 

    If upper(vInterface_Table) = 'XXCP_GL_INTERFACE' then
      vStr := vStr||' set_of_books_id,';
    End If;

    If upper(vInterface_Table) = 'XXCP_RA_INTERFACE_LINES' then
      vStr := vStr||' Org_id,';
    End If;
    End If;

    For Rec in C2(vSource_id, cMapping_Name) loop
      vStr := vStr || Rec.to_column_name||',';
    End Loop;
    vStr := Substr(vStr,1,Length(vStr)-1)||')';


    vStr := vStr ||' Select ';

    -- Additional columns appended if Table is an Interface Table
    If vIs_Interface then
      vStr := vStr || to_char(cSource_Assignment_id)||',';

    If upper(vInterface_Table) = 'XXCP_GL_INTERFACE' then
      vStr := vStr||' '||to_char(vSet_of_books_id)||',';
    End If;

    If upper(vInterface_Table) = 'XXCP_RA_INTERFACE_LINES' then
      vStr := vStr||' '||to_char(vOrg_id)||',';
    End If;
    End if;
    

    For Rec in C2(vSource_id, cMapping_Name) loop
      -- Init Tmp Column
      vTmp_From_Column := Rec.from_column_name;

      If rec.trim_data = 'Y' then
        vTmp_From_Column := 'trim('||vTmp_From_Column||')';    
      End if;

      If rec.trunc_data = 'Y' then
        vTmp_From_Column := 'substr(' ||vTmp_From_Column||',1,'||rec.data_length||')';    
      End if;
      
      -- Append Column
      vStr := vStr || vTmp_From_Column ||',';
      vError_Code := 0;
    End Loop;

    vStr := Substr(vStr,1,Length(vStr)-1)||' from XXCP_DATA_LOAD_R_ALL_V where file_id = '||to_char(cFileid);
    -- End Build DI.

    -- Add Filter Criteria
    If cCriteria_Set_id > 0 then
      for c4_rec in c4(cCriteria_set_id) loop
        vStr := vStr ||' and '|| c4_rec.filter_criteria;
      end loop;
      
      for c5_rec in c5(cCriteria_set_id) loop
        -- Line From
        If c5_rec.from_line is not null then
          vStr := vStr || ' and position >= '||c5_rec.from_line;
        End if;
        
        -- Line To 
        If c5_rec.to_line is not null then
          vStr := vStr ||' and position <= '||c5_rec.to_line;
        End if;
      End Loop;
     End if;

    If vError_Code = 0 then
      vError_Code := 14008; -- Insert Failed
      
      If xxcp_forms.Execute_immediate_clob(cStatement      => vStr, 
                                           cParse_Only     => 'N', 
                                           cSQLERRM        => vSQLERRM,
                                           cRows_Processed => vRows_Processed) then
        vError_Code := 0;
      End If;
    End If;

    If nvl(vError_Code,0) = 0 then
      Commit;
    Else
      Rollback;
    End If;

    cSQLERRM := vSQLERRM;
    cRows_Processed := vRows_Processed;
 
    Return(vError_Code);

  End Excel_Insert;


  -- *************************************************************
  --                        Where_Attribute_Used
  --                   -----------------------------
  -- *************************************************************
  Function Where_Attribute_Used(cSource_id in number, cAttribute_id in number) return varchar2 is

  vReturned_value Varchar2(250);

  vSQLERRM   Varchar2(512);
  vErrNum    Number;
  vStatement varchar2(1000);

  cursor q1 is
    Select
    'select count(*) cnt from XXCP_ACCOUNT_RULES where  source_id in (0,~S~) and ACCOUNTED_CURR_ATTRIBUTE_ID = ~A~' QRY , 'Transaction Rules' Descr, 'XXCP_ACCOUNT_RULES' table_name, 'ACCOUNTED_CURR_ATTRIBUTE_ID' column_name, 'Accounted Curr.' Marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_ACCOUNT_RULES where  source_id in (0,~S~) and ACCOUNTED_VALUE_ATTRIBUTE_ID = ~A~' QRY , 'Transaction Rules' Descr, 'XXCP_ACCOUNT_RULES' table_name, 'ACCOUNTED_VALUE_ATTRIBUTE_ID' column_name, 'Accounted Value' Marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_ACCOUNT_RULES where  source_id in (0,~S~) and ENTERED_CURR_ATTRIBUTE_ID = ~A~' QRY , 'Transaction Rules' Descr, 'XXCP_ACCOUNT_RULES' table_name, 'ENTERED_CURR_ATTRIBUTE_ID' column_name, 'Entered Curr.' Marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_ACCOUNT_RULES where  source_id in (0,~S~) and ENTERED_VALUE_ATTRIBUTE_ID = ~A~' QRY , 'Transaction Rules' Descr, 'XXCP_ACCOUNT_RULES' table_name, 'ENTERED_VALUE_ATTRIBUTE_ID' column_name, 'Entered Value' Marker   from dual
    UNION SELECT
    'select count(*) cnt from XXCP_ACCOUNT_RULES where  source_id in (0,~S~) and PAIR_A_CURR_ATTRIBUTE_ID = ~A~' QRY , 'Transaction Rules' Descr, 'XXCP_ACCOUNT_RULES' table_name, 'PAIR_A_CURR_ATTRIBUTE_ID' column_name, 'G/L Curr 1' Marker   from dual
    UNION SELECT
    'select count(*) cnt from XXCP_ACCOUNT_RULES where  source_id in (0,~S~) and PAIR_A_EXCH_DATE_ATTRIBUTE_ID = ~A~' QRY , 'Transaction Rules' Descr, 'XXCP_ACCOUNT_RULES' table_name, 'PAIR_A_EXCH_DATE_ATTRIBUTE_ID' column_name, 'G/L Date 1' Marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_ACCOUNT_RULES where  source_id in (0,~S~) and PAIR_A_EXCH_RATE_ATTRIBUTE_ID = ~A~' QRY , 'Transaction Rules' Descr, 'XXCP_ACCOUNT_RULES' table_name, 'PAIR_A_EXCH_RATE_ATTRIBUTE_ID' column_name, 'G/L Rate 1' Marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_ACCOUNT_RULES where  source_id in (0,~S~) and PAIR_A_EXCH_TYPE_ATTRIBUTE_ID = ~A~' QRY , 'Transaction Rules' Descr, 'XXCP_ACCOUNT_RULES' table_name, 'PAIR_A_EXCH_TYPE_ATTRIBUTE_ID' column_name, 'G/L Type 1' Marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_ACCOUNT_RULES where  source_id in (0,~S~) and PAIR_A_VALUE_ATTRIBUTE_ID = ~A~' QRY , 'Transaction Rules' Descr, 'XXCP_ACCOUNT_RULES' table_name, 'PAIR_A_VALUE_ATTRIBUTE_ID' column_name, 'G/L Value 2' Marker   from dual
    UNION SELECT
    'select count(*) cnt from XXCP_ACCOUNT_RULES where  source_id in (0,~S~) and PAIR_B_CURR_ATTRIBUTE_ID = ~A~' QRY , 'Transaction Rules' Descr, 'XXCP_ACCOUNT_RULES' table_name, 'PAIR_B_CURR_ATTRIBUTE_ID' column_name, 'G/L Curr 2' Marker   from dual
    UNION SELECT
    'select count(*) cnt from XXCP_ACCOUNT_RULES where  source_id in (0,~S~) and PAIR_B_EXCH_DATE_ATTRIBUTE_ID = ~A~' QRY , 'Transaction Rules' Descr, 'XXCP_ACCOUNT_RULES' table_name, 'PAIR_B_EXCH_DATE_ATTRIBUTE_ID' column_name, 'G/L Date 2' Marker   from dual
    UNION SELECT
    'select count(*) cnt from XXCP_ACCOUNT_RULES where  source_id in (0,~S~) and PAIR_B_EXCH_RATE_ATTRIBUTE_ID = ~A~' QRY , 'Transaction Rules' Descr, 'XXCP_ACCOUNT_RULES' table_name, 'PAIR_B_EXCH_RATE_ATTRIBUTE_ID' column_name, 'G/L Rate 2' Marker   from dual
    UNION SELECT
    'select count(*) cnt from XXCP_ACCOUNT_RULES where  source_id in (0,~S~) and PAIR_B_EXCH_TYPE_ATTRIBUTE_ID = ~A~' QRY , 'Transaction Rules' Descr, 'XXCP_ACCOUNT_RULES' table_name, 'PAIR_B_EXCH_TYPE_ATTRIBUTE_ID' column_name, 'G/L Type 2' Marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_ACCOUNT_RULES where  source_id in (0,~S~) and PAIR_B_VALUE_ATTRIBUTE_ID = ~A~' QRY , 'Transaction Rules' Descr, 'XXCP_ACCOUNT_RULES' table_name, 'PAIR_B_VALUE_ATTRIBUTE_ID' column_name, 'Rule_id' Marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_ACCOUNT_SETS where  source_id in (0,~S~) and SEGMENT_ATTRIBUTE_ID = ~A~' QRY , 'Account Set Control' Descr, 'XXCP_ACCOUNT_SETS' table_name, 'SEGMENT_ATTRIBUTE_ID' column_name, 'Segment' marker    from dual
    UNION SELECT
    'select count(*) cnt from XXCP_ACCOUNT_SET_CTL where  source_id in (0,~S~) and ASQ1_ATTRIBUTE_ID = ~A~' QRY , 'Account Set Control' Descr,'XXCP_ACCOUNT_SET_CTL' table_name, 'ASQ1_ATTRIBUTE_ID' column_name, 'Qualifier 1' marker   from dual
    UNION SELECT
    'select count(*) cnt from XXCP_ACCOUNT_SET_CTL where  source_id in (0,~S~) and ASQ2_ATTRIBUTE_ID = ~A~' QRY , 'Account Set Control' Descr,'XXCP_ACCOUNT_SET_CTL' table_name, 'ASQ2_ATTRIBUTE_ID' column_name, 'Qualifier 2' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_ACCOUNT_SET_CTL where  source_id in (0,~S~) and SET_ATTRIBUTE_ID = ~A~' QRY , 'Account Set Control' Descr,'XXCP_ACCOUNT_SET_CTL' table_name, 'SET_ATTRIBUTE_ID' column_name, 'ACCOUNT_SET_ID' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_ASSIGNMENT_QUALIFIERS where  1=1 and ASSIGNMENT_QUALIFIER_ID = ~A~' QRY , 'Assignment Qualifiers' Descr,'XXCP_ASSIGNMENT_QUALIFIERS' table_name, 'ASSIGNMENT_QUALIFIER_ID' column_name, 'ASSIGNMENT_SET_ID' marker   from dual
    UNION SELECT
    'select count(*) cnt from XXCP_ASSIGNMENT_RULES r, xxcp_assignment_ctl c where c.assignment_set_id = r.assignment_set_id and c.source_id in  (0,~S~) and ATTRIBUTE_ID = ~A~' QRY , 'Assignment Rules' Descr,'XXCP_ASSIGNMENT_RULES' table_name, 'ATTRIBUTE_ID' column_name, 'ASSIGNMENT_RULE_ID' marker   from dual
    UNION SELECT
    'select count(*) cnt from XXCP_ASSIGNMENT_RULES r, xxcp_assignment_ctl c where c.assignment_set_id = r.assignment_set_id and c.source_id in  (0,~S~) and TAX_REG_ATTRIBUTE_ID = ~A~' QRY , 'Assignment Rules' Descr,'XXCP_ASSIGNMENT_RULES' table_name, 'TAX_REG_ATTRIBUTE_ID' column_name, 'ASSIGNMENT RULE (Common)' marker   from dual
    UNION SELECT
    'select count(*) cnt from XXCP_COLUMN_RULES where  source_id in (0,~S~) and COLUMN_DEFINITION like ''%~A~%''' QRY , 'Column Sets' Descr,'XXCP_COLUMN_RULES' table_name, 'COLUMN_DEFINITION' column_name, 'Column Definition' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_COLUMN_RULES where  source_id in (0,~S~) and PH_ATTRIBUTE_ID = ~A~' QRY , 'Column Sets' Descr,'XXCP_COLUMN_RULES' table_name, 'PH_ATTRIBUTE_ID' column_name, 'Rule_id' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_COLUMN_SETS where  source_id in (0,~S~) and COLUMN_ATTRIBUTE_ID = ~A~' QRY , 'Column Sets' Descr,'XXCP_COLUMN_SETS' table_name, 'COLUMN_ATTRIBUTE_ID' column_name, 'Rule_id' marker   from dual
    UNION SELECT
    'select count(*) cnt from XXCP_COLUMN_SET_RULES where  source_id in (0,~S~) and COLUMN_ATTRIBUTE_ID = ~A~' QRY , 'Pricing Control' Descr,'XXCP_COLUMN_SET_RULES' table_name, 'COLUMN_ATTRIBUTE_ID' column_name, 'Rule_id' marker from dual
    UNION SELECT
    'select count(*) cnt from XXCP_COLUMN_SET_RULES where  source_id in (0,~S~) and DYNAMIC_ATTRIBUTE_ID = ~A~' QRY , 'Pricing Control' Descr,'XXCP_COLUMN_SET_RULES' table_name, 'DYNAMIC_ATTRIBUTE_ID' column_name, 'Rule_id' marker   from dual
    UNION SELECT
    'select count(*) cnt from XXCP_CONFIGURATION_CTL where  source_id in (0,~S~) and PRICE_ATTRIBUTE_ID = ~A~' QRY , 'Pricing Control' Descr,'XXCP_CONFIGURATION_CTL' table_name, 'PRICE_ATTRIBUTE_ID' column_name, 'CONFIGURATION_CTL_ID' marker   from dual
    UNION SELECT
    'select count(*) cnt from XXCP_CONFIGURATION_CTL where  source_id in (0,~S~) and PRICE_CURR_ATTRIBUTE_ID = ~A~' QRY , 'Pricing Control' Descr,'XXCP_CONFIGURATION_CTL' table_name, 'PRICE_CURR_ATTRIBUTE_ID' column_name, 'CONFIGURATION_CTL_ID' marker   from dual
    UNION SELECT
    'select count(*) cnt from XXCP_CROSS_SOURCE_MAPPING where ~S~ = 40 and FROM_ATTRIBUTE_ID = ~A~' QRY , 'Cross Source Mapping' Descr,'XXCP_CROSS_SOURCE_MAPPING' table_name, 'FROM_ATTRIBUTE_ID' column_name, 'FROM_ATTRIBUTE_ID' marker   from dual
    UNION SELECT
    'select count(*) cnt from XXCP_CROSS_SOURCE_MAPPING where  source_id in (0,~S~) and TO_ATTRIBUTE_ID = ~A~' QRY , 'Cross Source Mapping' Descr,'XXCP_CROSS_SOURCE_MAPPING' table_name, 'TO_ATTRIBUTE_ID' column_name, 'TO_ATTRIBUTE_ID' marker   from dual
    UNION SELECT
    'select count(*) cnt from XXCP_DYNAMIC_ATTRIBUTE_SETS a, XXCP_SQL_BUILD s where a.attribute_set_id=s.sql_build_id and s.source_id in (0,~S~) and a.COLUMN_DEFINITION like ''%~A~%''' QRY , 'Dynamic Attribute Set' Descr,'XXCP_DYNAMIC_ATTRIBUTE_SETS' table_name, 'COLUMN_DEFINITION' column_name, 'Column Definition' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_FORMULAS where  source_id in (0,~S~) and ATTRIBUTE_ID = ~A~' QRY , 'Formula Sets' Descr,'XXCP_FORMULAS' table_name, 'ATTRIBUTE_ID' column_name, 'Formula Definiton' marker   from dual
    UNION SELECT
    'select count(*) cnt from XXCP_FORMULAS where  source_id in (0,~S~) and FORMULA like ''%~A~%''' QRY , 'Formula Sets' Descr,'XXCP_FORMULAS' table_name, 'FORMULA' column_name, 'Formula' marker   from dual
    UNION SELECT
    'select count(*) cnt from XXCP_FORMULA_SEQUENCES where  source_id in (0,~S~) and ADJ_DATE_ATTRIBUTE_ID = ~A~' QRY , 'Formula Sequences' Descr,'XXCP_FORMULA_SEQUENCES' table_name, 'ADJ_DATE_ATTRIBUTE_ID' column_name, 'Adj. Rate Date' marker   from dual
    UNION SELECT
    'select count(*) cnt from XXCP_FORMULA_SEQUENCES where  source_id in (0,~S~) and ADJ_Q1_ATTRIBUTE_ID = ~A~' QRY , 'Formula Sequences' Descr,'XXCP_FORMULA_SEQUENCES' table_name, 'ADJ_Q1_ATTRIBUTE_ID' column_name, 'Adj. Rate Qualifier 1' marker   from dual
    UNION SELECT
    'select count(*) cnt from XXCP_FORMULA_SEQUENCES where  source_id in (0,~S~) and ADJ_Q2_ATTRIBUTE_ID = ~A~' QRY , 'Formula Sequences' Descr,'XXCP_FORMULA_SEQUENCES' table_name, 'ADJ_Q2_ATTRIBUTE_ID' column_name, 'Adj. Rate Qualifier 2' marker   from dual
    UNION SELECT
    'select count(*) cnt from XXCP_FORMULA_SEQUENCES where  source_id in (0,~S~) and ADJ_Q3_ATTRIBUTE_ID = ~A~' QRY , 'Formula Sequences' Descr,'XXCP_FORMULA_SEQUENCES' table_name, 'ADJ_Q3_ATTRIBUTE_ID' column_name, 'Adj. Rate Qualifier 3' marker   from dual
    UNION SELECT
    'select count(*) cnt from XXCP_FORMULA_SEQUENCES where  source_id in (0,~S~) and ADJ_Q4_ATTRIBUTE_ID = ~A~' QRY , 'Formula Sequences' Descr,'XXCP_FORMULA_SEQUENCES' table_name, 'ADJ_Q4_ATTRIBUTE_ID' column_name, 'Adj. Rate Qualifier 4' marker   from dual
    UNION SELECT
    'select count(*) cnt from XXCP_FORMULA_SEQUENCES where  source_id in (0,~S~) and EXCHANGE_DATE_ATTRIBUTE_ID = ~A~' QRY , 'Formula Sequences' Descr,'XXCP_FORMULA_SEQUENCES' table_name, 'EXCHANGE_DATE_ATTRIBUTE_ID' column_name, 'Exchange Rate Date' marker   from dual
    UNION SELECT
    'select count(*) cnt from XXCP_FORMULA_SEQUENCES where  source_id in (0,~S~) and FROM_CURR_ATTRIBUTE_ID = ~A~' QRY , 'Formula Sequences' Descr,'XXCP_FORMULA_SEQUENCES' table_name, 'FROM_CURR_ATTRIBUTE_ID' column_name, 'Exchange From Curr.' marker   from dual
    UNION SELECT
    'select count(*) cnt from XXCP_FORMULA_SEQUENCES where  source_id in (0,~S~) and TO_CURR_ATTRIBUTE_ID = ~A~' QRY , 'Formula Sequences' Descr,'XXCP_FORMULA_SEQUENCES' table_name, 'TO_CURR_ATTRIBUTE_ID' column_name, 'Exchange To Curr.' marker   from dual
    UNION SELECT
    'select count(*) cnt from XXCP_FORMULA_SEQUENCES where  source_id in (0,~S~) and GIVEN_EXCH_RATE_ATTRIBUTE_ID = ~A~' QRY , 'Formula Sequences' Descr,'XXCP_FORMULA_SEQUENCES' table_name, 'GIVEN_EXCH_RATE_ATTRIBUTE_ID' column_name, 'Given Exchange Rate' marker   from dual
    UNION SELECT
    'select count(*) cnt from XXCP_HISTORY_ATTRIBUTES where  1=1 and COLUMN_ID = ~A~' QRY , 'Column Rules (History columns)' Descr,'XXCP_HISTORY_ATTRIBUTES' table_name, 'COLUMN_ID' column_name, 'CONFIGURATION_CTL_ID' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_PASS_THROUGH_RULE_SETS where  source_id in (0,~S~) and PTQ1_ATTRIBUTE_ID = ~A~' QRY , 'Pass Through Rules' Descr,'XXCP_PASS_THROUGH_RULE_SETS' table_name, 'PTQ1_ATTRIBUTE_ID' column_name , 'Qualifier 1' marker from dual
    UNION SELECT
    'select count(*) cnt from XXCP_PASS_THROUGH_RULE_SETS where  source_id in (0,~S~) and PTQ2_ATTRIBUTE_ID = ~A~' QRY , 'Pass Through Rules' Descr,'XXCP_PASS_THROUGH_RULE_SETS' table_name, 'PTQ2_ATTRIBUTE_ID' column_name , 'Qualifier 2' marker from dual
    UNION SELECT
    'select count(*) cnt from XXCP_PASS_THROUGH_RULE_SETS where  source_id in (0,~S~) and PTQ3_ATTRIBUTE_ID = ~A~' QRY , 'Pass Through Rules' Descr,'XXCP_PASS_THROUGH_RULE_SETS' table_name, 'PTQ3_ATTRIBUTE_ID' column_name , 'Qualifier 3' marker from dual
    UNION SELECT
    'select count(*) cnt from XXCP_PASS_THROUGH_RULE_SETS where  source_id in (0,~S~) and PTQ4_ATTRIBUTE_ID = ~A~' QRY , 'Pass Through Rules' Descr,'XXCP_PASS_THROUGH_RULE_SETS' table_name, 'PTQ4_ATTRIBUTE_ID' column_name , 'Qualifier 4' marker from dual
    UNION SELECT
    'select count(*) cnt from XXCP_PRICING_METHOD where  source_id in (0,~S~) and ARQC1_ATTRIBUTE_ID = ~A~' QRY , 'Pricing Methods' Descr,'XXCP_PRICING_METHOD' table_name, 'ARQC1_ATTRIBUTE_ID' column_name , 'Adj. Qualifier 1' marker from dual
    UNION SELECT
    'select count(*) cnt from XXCP_PRICING_METHOD where  source_id in (0,~S~) and ARQC2_ATTRIBUTE_ID = ~A~' QRY , 'Column Rules' Descr,'XXCP_PRICING_METHOD' table_name, 'ARQC2_ATTRIBUTE_ID' column_name, 'Adj. Qualifier 2' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_PRICING_METHOD where  source_id in (0,~S~) and BASE_CURR_ATTRIBUTE_ID = ~A~' QRY , 'Pricing Methods' Descr,'XXCP_PRICING_METHOD' table_name, 'BASE_CURR_ATTRIBUTE_ID' column_name, 'Pricing Currency' marker   from dual
    UNION SELECT
    'select count(*) cnt from XXCP_PRICING_METHOD where  source_id in (0,~S~) and BASE_VAL_ATTRIBUTE_ID = ~A~' QRY , 'Pricing Methods' Descr,'XXCP_PRICING_METHOD' table_name, 'BASE_VAL_ATTRIBUTE_ID' column_name, 'Pricing Value' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_PRICING_METHOD where  source_id in (0,~S~) and EXCHANGE_DATE_ATTRIBUTE_ID = ~A~' QRY , 'Pricing Methods' Descr,'XXCP_PRICING_METHOD' table_name, 'EXCHANGE_DATE_ATTRIBUTE_ID' column_name, 'Exchange Date' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_PRICING_METHOD where  source_id in (0,~S~) and OWNER_ATTRIBUTE_ID = ~A~' QRY , 'Pricing Methods' Descr,'XXCP_PRICING_METHOD' table_name, 'PARTNER_ATTRIBUTE_ID' column_name, 'Owner' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_PRICING_METHOD where  source_id in (0,~S~) and PARTNER_ATTRIBUTE_ID = ~A~' QRY , 'Pricing Methods' Descr,'XXCP_PRICING_METHOD' table_name, 'PARTNER_ATTRIBUTE_ID' column_name, 'Partner' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_PRICING_METHOD where  source_id in (0,~S~) and TO_CURR_ATTRIBUTE_ID = ~A~' QRY , 'Pricing Methods' Descr,'XXCP_PRICING_METHOD' table_name, 'TO_CURR_ATTRIBUTE_ID' column_name, 'Currency Indicator' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_PRICING_METHOD where  source_id in (0,~S~) and ADJ_DATE_ATTRIBUTE_ID = ~A~' QRY , 'Pricing Methods' Descr,'XXCP_PRICING_METHOD' table_name, 'ADJ_DATE_ATTRIBUTE_ID' column_name, 'Adj. Rate Date' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_PRICING_METHOD where  source_id in (0,~S~) and DFT_BASE_CURR_ATTRIBUTE_ID = ~A~' QRY , 'Pricing Methods' Descr,'XXCP_PRICING_METHOD' table_name, 'DFT_BASE_CURR_ATTRIBUTE_ID' column_name, 'Default Currency' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_PRICING_METHOD where  source_id in (0,~S~) and DFT_BASE_VAL_ATTRIBUTE_ID = ~A~' QRY , 'Pricing Methods' Descr,'XXCP_PRICING_METHOD' table_name, 'DFT_BASE_VAL_ATTRIBUTE_ID' column_name, 'Default Value' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_PRICING_TYPES where  source_id in (0,~S~) and UNIT_PRICE_ATTRIBUTE_ID = ~A~' QRY , 'Pricing Types' Descr,'XXCP_PRICING_TYPES' table_name, 'UNIT_PRICE_ATTRIBUTE_ID' column_name, 'Unit Price' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_PRICING_TYPES where  source_id in (0,~S~) and CURRENCY_ATTRIBUTE_ID = ~A~' QRY , 'Pricing Types' Descr,'XXCP_PRICING_TYPES' table_name, 'CURRENCY_ATTRIBUTE_ID' column_name, 'Currency' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_PRICING_TYPES where  source_id in (0,~S~) and PRICE_ATTRIBUTE_ID = ~A~' QRY , 'Pricing Types' Descr,'XXCP_PRICING_TYPES' table_name, 'PRICE_ATTRIBUTE_ID' column_name, 'Price' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_PRICING_TYPES where  source_id in (0,~S~) and ADJ_RATE_ATTRIBUTE_ID = ~A~' QRY , 'Pricing Types' Descr,'XXCP_PRICING_TYPES' table_name, 'ADJ_RATE_ATTRIBUTE_ID' column_name, 'Adj. Rate' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_PRICING_TYPES where  source_id in (0,~S~) and TAX_RATE_ATTRIBUTE_ID = ~A~' QRY , 'Pricing Types' Descr,'XXCP_PRICING_TYPES' table_name, 'TAX_RATE_ATTRIBUTE_ID' column_name, 'Tax Rate' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_SEGMENT_RULES q where q.rule_id in (select ar.rule_id from xxcp_account_rules ar where  source_id in (0,~S~)) and SEGMENT_ATTRIBUTE_ID = ~A~' QRY , 'Segment Rules' Descr,'XXCP_SEGMENT_RULES' table_name, 'SEGMENT_ATTRIBUTE_ID' column_name, 'RULE_ID' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_SUMMARY_BREAK_PTS where source_id in (0,~S~) and GRP1_ATTRIBUTE = ~A~' QRY , 'Cache Summaries' Descr,'XXCP_SUMMARY_BREAK_PTS' table_name, 'GRP1_ATTRIBUTE' column_name, 'Cached Attribute 1' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_SUMMARY_BREAK_PTS where source_id in (0,~S~) and GRP2_ATTRIBUTE = ~A~' QRY , 'Cache Summaries' Descr,'XXCP_SUMMARY_BREAK_PTS' table_name, 'GRP2_ATTRIBUTE' column_name, 'Cached Attribute 2' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_SUMMARY_BREAK_PTS where source_id in (0,~S~) and GRP3_ATTRIBUTE = ~A~' QRY , 'Cache Summaries' Descr,'XXCP_SUMMARY_BREAK_PTS' table_name, 'GRP1_ATTRIBUTE' column_name, 'Cached Attribute 3' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_SUMMARY_BREAK_PTS where source_id in (0,~S~) and GRP4_ATTRIBUTE = ~A~' QRY , 'Cache Summaries' Descr,'XXCP_SUMMARY_BREAK_PTS' table_name, 'GRP1_ATTRIBUTE' column_name, 'Cached Attribute 4' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_SUMMARY_BREAK_PTS where  source_id in (0,~S~) and GRP5_ATTRIBUTE = ~A~' QRY , 'Cache Summaries' Descr,'XXCP_SUMMARY_BREAK_PTS' table_name, 'GRP1_ATTRIBUTE' column_name, 'Cached Attribute 5' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_SUMMARY_BREAK_PTS where source_id in (0,~S~) and COUNT_CACHE = ~A~' QRY , 'Cache Summaries' Descr,'XXCP_SUMMARY_BREAK_PTS' table_name, 'COUNT_CACHE' column_name, 'Record Count' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_SUMMARY_BREAK_PTS where source_id in (0,~S~) and TRX_QUALIFIER1_EXPR = ~A~' QRY , 'Cache Summaries' Descr,'XXCP_SUMMARY_BREAK_PTS' table_name, 'TRX_QUALIFIER1_EXPR' column_name, 'Trx Qual 1 Expression' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_SUMMARY_BREAK_PTS where source_id in (0,~S~) and TRX_QUALIFIER2_EXPR = ~A~' QRY , 'Cache Summaries' Descr,'XXCP_SUMMARY_BREAK_PTS' table_name, 'TRX_QUALIFIER2_EXPR' column_name, 'Trx Qual 2 Expression' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_SUMMARY_CACHE where source_id in (0,~S~) and SUM_ATTRIBUTE = ~A~' QRY , 'Cache Summaries' Descr,'XXCP_SUMMARY_CACHE' table_name, 'SUM_ATTRIBUTE' column_name, 'Cache Summary Total' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_SUMMARY_CACHE where source_id in (0,~S~) and NUMERIC_CACHE = ~A~' QRY , 'Cache Summaries' Descr,'XXCP_SUMMARY_CACHE' table_name, 'NUMERIC_CACHE' column_name, 'Cache Break Total' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_SUMMARY_CACHE where source_id in (0,~S~) and TS_CACHE = ~A~' QRY , 'Cache Summaries' Descr,'XXCP_SUMMARY_CACHE' table_name, 'TS_CACHE' column_name, 'Cache Grand Total' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_SQL_BUILD where  source_id in (0,~S~) and SQL_BUILD like ''%~A~%''' QRY , 'Dynamic Interface' Descr,'XXCP_SQL_BUILD' table_name, 'SQL_BUILD' column_name, 'SQL Select Statement' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_VALIDATIONS where  source_id in (0,15) and CHILD_ATTRIBUTE_ID = ~A~' QRY , 'Validation Rules' Descr,'XXCP_VALIDATIONS' table_name, 'CHILD_ATTRIBUTE_ID' column_name, 'Validations' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_VALIDATIONS where  source_id in (0,15) and DYNAMIC_ATTRIBUTE_ID = ~A~' QRY , 'Validation Rules' Descr,'XXCP_VALIDATIONS' table_name, 'DYNAMIC_ATTRIBUTE_ID' column_name, 'Validations' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_VALIDATIONS where  source_id in (0,15) and REGION_ATTRIBUTE_ID = ~A~' QRY , 'Validation Rules' Descr,'XXCP_VALIDATIONS' table_name, 'REGION_ATTRIBUTE_ID' column_name, 'Validations' marker  from dual
    UNION SELECT
    'select count(*) cnt from XXCP_VALIDATIONS where  source_id in (0,15) and TEST_ATTRIBUTE_ID = ~A~' QRY , 'Validation Rules' Descr,'XXCP_VALIDATIONS' table_name, 'TEST_ATTRIBUTE_ID' column_name, 'Validations' marker   from dual
    UNION SELECT
    'select count(*) cnt from XXCP_VALIDATION_RESULTS where  1=1 and DYNAMIC_ATTRIBUTE_ID = ~A~' QRY , 'Validation Results' Descr,'XXCP_VALIDATION_RESULTS' table_name, 'DYNAMIC_ATTRIBUTE_ID' column_name, 'Validations' marker   from dual
    UNION SELECT
    'select count(*) cnt from XXCP_VALIDATION_SEVERITY_COND where  1=1 and ATTRIBUTE_ID = ~A~' QRY , 'Validation Severity' Descr,'XXCP_VALIDATION_SEVERITY_COND' table_name, 'ATTRIBUTE_ID' column_name, 'Validations' marker    from dual
    UNION SELECT
    'select count(*) cnt from XXCP_VALIDATION_SEVERITY_COND where  1=1 and VALUE_ATTRIBUTE_ID = ~A~' QRY , 'Validation Severity' Descr,'XXCP_VALIDATION_SEVERITY_COND' table_name, 'VALUE_ATTRIBUTE_ID' column_name, 'VALUE_ATTRIBUTE_ID' marker from dual;


  vMsg varchar2(32000);

  vCnt number := 0;
  vAns boolean;

  Cursor SB(cSource_id in number, cAttribute_id in number) is
  select s.sql_name Dynamic_interface
    from xxcp_dynamic_attribute_sets d,
         xxcp_sql_build s
   where s.source_id       = cSource_id
     and s.sql_build_id    = d.attribute_set_id
     and d.sequence_number = cAttribute_id;

Begin

  For Rec in SB(cSource_id, cAttribute_id) Loop
     vCnt := vCnt + 1;
     vMsg := vMsg  ||'Dynamic Interface ['|| rec.dynamic_interface||']'||chr(10);
  End Loop;

  For Rec in Q1 Loop

   vStatement := Rec.Qry|| ' and rownum = 1';

   vStatement := Replace(vStatement,'~S~',to_char(cSource_id));
   vStatement := Replace(vStatement,'~A~',to_char(cAttribute_id));

   If cSource_id != 15 and substr(Rec.table_name,1,5) = 'XXCP_VA' then
      vReturned_value := 0;
   Else
      vReturned_value := nvl(xxcp_forms.execute_immediate_into(vStatement, vSQLERRM, vErrNum),'0');
   End If;

   If to_number(nvl(vReturned_value,'0')) > 0 then
     vCnt := vCnt + 1;
     vMsg := vMsg  ||''|| rec.descr||' ['||rec.table_name||'/'||rec.column_name||'/'||rec.marker||']'||chr(10);
   End If;

  End Loop;

  If vCnt = 0 then
   vMsg := 'Not used';
   vAns := FALSE;
  Else
   vAns := TRUE;
  End If;

  Return(vMsg);


 End Where_Attribute_Used;

  -- *************************************************************
  --                        SOURCE_SECURITY
  --                   -----------------------------
  -- *************************************************************
 Function Source_Security(cSource_id in number) return varchar2 is

  Cursor c1(pSource_id in number) is
  select 'Y' Allowed
    from xxcp_user_sources s,
         xxcp_user_profiles p
   where p.user_id = s.user_id
     -- 02.04.09
     and nvl(p.active,'N') = 'Y'
     and s.user_id = fnd_global.user_id
     and sysdate between trunc(nvl(effective_from_date,'01-JAN-2000')) and trunc(nvl(effective_to_date,'31-DEC-2999'))
     and source_id = pSource_id;

  Cursor c2(pSource_id in number) is
    select 'Y' Allowed
    from xxcp_responsibility_sources w,
         fnd_user_resp_groups g
     where g.user_id = fnd_global.USER_ID
       and w.responsibility_id = fnd_global.resp_id
       and g.RESPONSIBILITY_ID = w.responsibility_id
       and w.source_id = pSource_id
       and sysdate between trunc(nvl(effective_from_date,'01-JAN-2000')) and trunc(nvl(effective_to_date,'31-DEC-2999'));

  vResult varchar2(1) := 'N';

 Begin

   If gSecurity_Type_Resp = 'Y' then
     -- Responsibility
     For Rec in c2(cSource_id) loop
       vResult := Rec.Allowed;
     End Loop;
   Else
     -- User
     For Rec in c1(cSource_id) loop
       vResult := Rec.Allowed;
     End Loop;
   End If;

   Return(vResult);

 End Source_Security;

  -- *************************************************************
  --                      Validate_Apps_Triggers
  --                   -----------------------------
  -- *************************************************************
 Function Validate_Apps_Triggers return number is


 Cursor invalid_views is
 select 'alter trigger '||object_name||' compile ' cmd_line,
        object_name Trigger_Name
   from all_objects
  where object_type = 'TRIGGER'
    and object_name like 'XXCP_%'
    and owner = 'APPS'
    and status = 'INVALID';

  k                  pls_integer := 0;
  vTwice             pls_integer := 2;
  vSQLERRM           varchar2(512);
  vInternalErrorCode number := 0;

 begin

   xxcp_forms.execute_immediate('Alter package XXCP_TRIGGER_EVENTS Compile', vSQLERRM,  vInternalErrorCode);

   xxcp_forms.execute_immediate('Alter package XXCP_TRIGGER_CODE Compile', vSQLERRM,  vInternalErrorCode);

   If vInternalErrorCode = 0 then

        For K in 1..vTwice Loop
          -- Check for invalid views
          For Rec in invalid_views Loop

           xxcp_forms.execute_immediate(Rec.cmd_line, vSQLERRM, vInternalErrorCode);

           If vInternalErrorCode != 0 then
             FndWriteError(100,Rec.Trigger_Name||' Apps Trigger Failed compile ',vSQLERRM);
             Exit; -- loop
           End If;
          End Loop;

        End Loop;
   Else
     FndWriteError(100,'Apps Trigger: '||vSQLERRM,vSQLERRM);

   End If;

   Return(vInternalErrorCode);

 End Validate_Apps_Triggers;

  -- *************************************************************
  --                      Refresh_Engine_Names
  --                   -----------------------------
  -- *************************************************************
 Procedure Refresh_Engine_Names is

  Cursor c1(pApplication_id in number) is
  select ps.CONCURRENT_PROGRAM_NAME Program_name,
         tx.DESCRIPTION Name,
         tx.USER_CONCURRENT_PROGRAM_NAME User_Name, ps.concurrent_program_id
   from FND_CONCURRENT_PROGRAMS_TL tx, FND_CONCURRENT_PROGRAMS ps
  where tx.concurrent_program_id =
      any (select request_unit_id
             from FND_REQUEST_GROUP_UNITS x
            where x.request_group_id =
                any(select request_group_id
                      from FND_REQUEST_GROUPS tx
                     where request_group_code IN ('XXCP_ENGINES','XXCP_ICS_REPORTS','XXCP_COST_PLUS_ENG')
                       and tx.application_id = pApplication_id)
                    )
     and tx.CONCURRENT_PROGRAM_ID = ps.CONCURRENT_PROGRAM_ID
     and tx.language = 'US'
      and tx.application_id = pApplication_id;

  cursor c2 is
   select application_id from fnd_application where application_short_name = 'XXCP';

  vApplication_id  number;

 Begin

   -- Get Application Id
   For Rec2 in C2 loop
     vApplication_id := rec2.application_id;
   End Loop;


   Delete from xxcp_sys_engine_names;

   For Rec in c1(vApplication_id) Loop

     INSERT into xxcp_sys_engine_names(
         PROGRAM_ID
        ,PROGRAM_NAME
        ,PROGRAM_TYPE
        ,ENGINE_NAME
        ,USER_ENGINE_NAME
        ,CREATED_BY
        ,CREATION_DATE
        ,LAST_UPDATED_BY
        ,LAST_UPDATE_DATE
        ,LAST_UPDATE_LOGIN
     )
     VALUES( rec.concurrent_program_id, rec.program_name, 'CONC', rec.name, rec.user_name,
      fnd_global.user_id, sysdate, fnd_global.user_id, sysdate, fnd_global.login_id);

   End Loop;

   Commit;

 End Refresh_Engine_Names;
 
  -- *************************************************************
  --                      Update_Engine_Names
  --                   -----------------------------
  -- *************************************************************
 Procedure Update_Engine_Names (cNew_Name     IN Varchar2
                               ,cCurrent_Name IN Varchar2
                               ,cRetCode     OUT Number
                               ,cSQLERRM     OUT Varchar2) is
   
   vDBLink              xxcp_instance_db_links.db_link%TYPE;
   vSQL                  Varchar2(500);
   vSQLERRM              varchar2(1024);
   vInternalErrorCode    Number;   
   
 Begin
  If instr(cNew_Name, 'Virtual Trader ') = 1 then
    
      vSQL := 'update fnd_concurrent_programs_tl';
      vSQL := vSQL||' set user_concurrent_program_name = '''||cNew_Name||'''';
      vSQL := vSQL||' where user_concurrent_program_name = '''||cCurrent_Name||'''';
    
      xxcp_forms.Execute_Immediate(vSQL, vSQLERRM, vInternalErrorCode);
      
      If vSQLERRM is not null then
        vSQLERRM := 'Error: Invalid command... '||vSQLERRM;
        vInternalErrorCode := 4;
        Rollback;
      Else
        vInternalErrorCode := 0;
        vSQLERRM           := 'Concurrent Program changed. Re-Query to see new name.';        
         Commit;        
      End If;
        
  Else
    vInternalErrorCode := 2;
    vSQLERRM           := 'Invalid Name.'  ;
  End If;   
  
  cRetCode := vInternalErrorCode;
  cSQLERRM := vSQLERRM;

 End Update_Engine_Names;
 
  -- *************************************************************
  --                   Update_All_User_Engine_Names
  --                   -----------------------------
  -- ************************************************************* 
 Procedure Update_All_User_Engine_Names Is
 
   Cursor c1 is
     Select a.program_id
           ,a.user_engine_name
           ,a.engine_name
       from xxcp_sys_engine_names a;
         
 Begin
   
   For x in c1 loop
     If x.engine_name != x.user_engine_name Then
       Set_Engine_Name(cProgram_id => x.program_id
                      ,cUser_Name  => x.user_engine_name);
     End If;
   End Loop;
 
 End;
 
  -- *************************************************************
  --                      Set_Engine_Name
  --                   -----------------------------
  -- *************************************************************
Procedure Set_Engine_Name(cProgram_id in number, cUser_Name in varchar2) is

 cursor c2 is
   select application_id from fnd_application where application_short_name = 'XXCP';

  vApplication_id  number;

 Begin

   -- Get Application Id
   For Rec2 in C2 loop
     vApplication_id := rec2.application_id;
   End Loop;

   If nvl(cProgram_id,0) > 0 then
    Begin

     update FND_CONCURRENT_PROGRAMS_TL tx
       set USER_CONCURRENT_PROGRAM_NAME = cUser_Name
      where application_id = vApplication_id
        and tx.CONCURRENT_PROGRAM_ID = cProgram_id;

      Commit;

      Exception when others then null;
    End;
   End If;

 End Set_Engine_Name;

 --
 -- Generate_Instance_Views
 --
 Procedure Generate_Instance_Views(cLast_View out varchar2, cInternal_Error_Code out number) is

 vStatement   varchar2(32000);
 vReplace_Smt varchar2(32000);
 vSQLERRM     varchar2(512);

 Cursor vc(pView_id in number) is
 select l2.ovr_text, rtrim(nvl(l2.ovr_text,l.std_text)) text
      , k.instance_id, k.instance_name
      , decode(rtrim(k.db_link),null,null,'@'||k.db_link) db_link
 from xxcp_instance_views l,
      xxcp_instance_views_ovr l2,
      xxcp_instance_view_links d,
      xxcp_instance_db_links k
 where l.view_id = pView_id
   and l.view_id = d.view_id
   and l.view_id = l2.view_id(+)
   and l2.ovr_text is null
   and d.included = 'Y'
   and d.db_link_id = k.db_link_id
   and k.active = 'Y'
   and l.view_name = any(select g.view_name from all_views g where g.view_name = l.view_name and g.OWNER = 'APPS' )
   order by l.compile_seq,l.view_id,k.instance_id;

 Cursor vc_ovr(pView_id in number) is
  select l2.ovr_text
  from xxcp_instance_views l,
       xxcp_instance_views_ovr l2
  where l.view_id = pView_id
    and l.view_id = l2.view_id
    and l.view_name = any(select g.view_name from all_views g where g.view_name = l.view_name and g.OWNER = 'APPS' )
  order by l.compile_seq;

   vCnt integer := 0;

   Cursor GA is
    select view_id, rowid source_rowid, view_name, locked,std_text
     from xxcp_instance_views r
         where r.view_name = any(select g.view_name from all_views g where g.view_name = r.view_name and g.OWNER = 'APPS' )
    order by r.compile_seq;

   vErr       Integer := 0;
   vB         Integer := 0;
   vFirst     boolean := True;
   vLast_View xxcp_instance_views.view_name%type;

   vResult    Integer;

Begin

  For GaRec in GA loop

    Exit when vErr <> 0;
    vCnt := 0;

    vB := nvl(vB,0) + 1;
    vLast_View := GaRec.View_Name;

    If GaRec.locked = 'Y' then
      vStatement := 'Create or Replace view '||GaRec.view_name||' as '||chr(10)||GaRec.std_text;
      vCnt := 1;
    Else
      vStatement := 'Create or Replace view '||GaRec.view_name||' as '||chr(10);
      vCnt := 0;
      For Rec in vc(GaRec.view_id) loop
         vReplace_Smt := rec.text;
         vReplace_Smt := Replace(vReplace_Smt,'0/*instance id*/',to_char(rec.instance_id));
         vReplace_Smt := Replace(vReplace_Smt,'/*datalink*/',rec.db_link);
         vCnt := vCnt + 1;

         If vCnt = 1 then
           vStatement := vStatement || vReplace_Smt;
         Else
           vStatement := vStatement ||chr(10)||'Union All /*'||rec.Instance_Name||'*/'||chr(10)||vReplace_Smt;
         End If;
      End Loop;

      If vCnt = 0 then
        For Rec in vc_ovr(GARec.view_id) loop
          vStatement := Rec.ovr_text;
        End Loop;
        If ((Instr(upper(vStatement),'SELECT ') > 0) and (Instr(upper(vStatement),'CREATE ') > 0)) then
          vCnt := 1;
        Else
          vCnt := 0;
          vErr := 2;
        End If;
      End If;
    End If;

   If vCnt = -1 then
      vErr := 2;
   Elsif Execute_Immediate(vStatement,vSQLERRM) and vCnt > 0 then
      vErr := 0;
   Else
      vErr := 1;
   End If;

  End Loop;

  cLast_View           := vLast_View;
  cInternal_Error_Code := vErr;

 End Generate_Instance_views;
 
 -- 
 -- Refresh_Cross_Source_Mapping Table
 --
 Procedure Refresh_Cross_Source_Mapping is
 
 Cursor c1 is 
   select source_id
     from xxcp_sys_sources s
     where s.active = 'Y'
       and not s.source_id in (14,15)
       and s.shared_pricing = 'Y';
 
 begin
 
   Begin
     -- Remove obsolete records
     Delete from xxcp_cross_source_mapping w
      where to_attribute_id is null
        and not exists (select 'x' 
                          from xxcp_dynamic_attributes s
                         where s.source_id = 40
                           and column_type in (1001,1002)
                           and s.column_id = w.from_attribute_id); 
  
      Commit;
 
      Exception when others then null;
   End;
   
   Begin
   
      For Rec in C1 loop
        -- Update cross source mapping table
        insert into xxcp_cross_source_mapping(source_id, from_attribute_id,data_type, from_attribute_name,
                                            creation_date, created_by, last_updated_by, last_update_date, last_update_login) 
        select rec.source_id, s.column_id, s.data_type, s.column_name,
               sysdate, fnd_global.USER_ID, fnd_global.USER_ID, sysdate, fnd_global.LOGIN_ID
          from xxcp_dynamic_attributes s
         where s.source_id = 40
           and column_type in (1001,1002)
           and not exists (select 'x'
                  from xxcp_cross_source_mapping f
                   where f.source_id = rec.source_id
                     and f.from_attribute_id = s.column_id);
 
 
      End Loop;
      
      -- Refresh names incase user has change the name in the dynamic attributes form
      update xxcp_cross_source_mapping d
          set d.to_attribute_name = xxcp_translations.Attribute_Name(d.to_attribute_id  , d.source_id);

      Commit;
      
      Exception when others then null;
      
     End;
     
      
 
 End Refresh_Cross_Source_Mapping;

 -- Init
 Procedure Init is
 -- Added for Cisco project
 cursor c1 is
   select s.profile_value
   from xxcp_sys_profile s
   where s.profile_name = 'USER CONTROL BY RESPONSIBILITY'
     or  s.profile_name = 'USER CONTROL BY REPSONSIBILITY'; -- Spelling mistake

 Begin

     For Rec in c1 loop
       gSecurity_Type_Resp := rec.profile_value;
       Exit;
     End Loop;

 End Init;

  -- **********************************************************
  --
  --        Get Sys Profile
  -- 
  -- Gets the System Profile value for the passed Category and
  -- Name
  -- **********************************************************
 Function Get_Sys_Profile(cProfile_Category in varchar2, cProfile_Name in varchar2) return varchar2 is
   cursor c1 is
    select profile_value from xxcp_sys_profile
                   where profile_category = cProfile_Category
                   and   profile_name = cProfile_Name;
   
   vProfileValue   xxcp_sys_profile.profile_name%type;
   
 Begin
  For Rec in C1 loop
   vProfileValue := Rec.Profile_Value;
  End Loop;
  
  Return(vProfileValue);
 End Get_Sys_Profile; 
 
  -- **********************************************************
  --
  --        Get Default Effectivity Dates
  --
  -- Gets the default Effectivity Dates held as a system
  -- profile. If null, use 1 year for Start and 10 years as end
  -- **********************************************************
 Procedure Get_Default_Effectivity_Dates(cStartDate out date, cEndDate out date) is
   vStartDate  number;
   vEndDate    number;
 Begin
   vStartDate := to_number(nvl(Get_Sys_Profile('EV','EFFECTIVE START DAYS OFFSET'), 365));
   vEndDate := to_number(nvl(Get_Sys_Profile('EV','EFFECTIVE END DAYS OFFSET'), 3650));
   
   cStartDate := trunc(sysdate) - vStartDate;
   cEndDate := trunc(sysdate) + vEndDate;
 End Get_Default_Effectivity_Dates;

  --
  -- Create_Edit_Ref_Temp
  --
  Function Create_Edit_Ref_Temp(cAttribute_id in number, 
                                cUser_id      in number, 
                                cPreview_Flag in varchar2) return number is

    cursor pc1(pAttribute_id in number, pUser_id in number) is
     select r.rowid source_rowid , r.ref_ctl_id
       from xxcp_pv_transaction_references r
       where r.attribute_id = pAttribute_id
         and r.preview_id = pUser_id;
         
    cursor lv1(pAttribute_id in number, pUser_id in number) is
     select r.rowid source_rowid , r.ref_ctl_id
       from xxcp_transaction_references r
       where r.attribute_id = pAttribute_id;

    cursor da(pRef_ctl_id in number) is 
      select h.column_name, h.ref_attribute_id, h.source_id,
             nvl(h.title, xxcp_translations.Attribute_Name(h.ref_attribute_id, h.source_id)) title
      from xxcp_trx_reference_rules h
       where h.ref_ctl_id = pRef_Ctl_id
       order by decode(nvl(h.display_seq,0),0,999,h.display_seq), h.column_number;    

     vSQLData varchar2(1200);
     vData    xxcp_pv_transaction_references.long_reference1%type;
     
     vDisplay_seq number := 0;
     
  Begin
    
     Begin
       Delete from xxcp_edit_reference_temp t
        where t.attribute_id = cAttribute_id
          and t.preview_id   = cUser_id
          and t.user_id      = cUser_id;
     
       Exception when others then null;
     End;
  
     If cPreview_Flag = 'Y' then
     
      For recPC in PC1(pAttribute_id => cAttribute_id, pUser_id => cUser_id) Loop
        For recDa in DA(pRef_Ctl_id => RecPC.Ref_Ctl_Id) Loop        
          vSQLData := 'Select '||recDa.Column_Name||' from  cp_pv_transaction_references where rowid = :1';         
          
          Execute Immediate vSQLData into vData using RecPC.Source_Rowid;       
          
          vDisplay_seq := vDisplay_seq + 1; 
               
          insert into xxcp_edit_reference_temp(
              REF_CTL_ID   ,                        
              USER_ID      ,                          
              PREVIEW_ID   ,                         
              SESSION_ID   ,                         
              ATTRIBUTE_ID ,                         
              COLUMN_NAME  ,                         
              TITLE        ,                         
              DATA_VALUE   ,                        
              USAGE_ID     ,                         
              CREATED_BY   ,                         
              CREATION_DATE,
              DISPLAY_SEQ                         
              )
           Values(recPC.Ref_Ctl_Id,
                  cUser_id,
                  cUser_id,
                  fnd_global.session_id,
                  cAttribute_id,
                  recDa.Column_Name,
                  RecDa.Title,
                  vData,
                  '1',
                  fnd_global.USER_ID,
                  sysdate,
                  vDisplay_seq);
        End Loop;
      End Loop;
     Else

      For recPC in lv1(pAttribute_id => cAttribute_id, pUser_id => cUser_id) Loop
        For recDa in DA(pRef_Ctl_id => RecPC.Ref_Ctl_Id) Loop        
          vSQLData := 'Select '||recDa.Column_Name||' from  cp_transaction_references where rowid = :1';         
          
          Execute Immediate vSQLData into vData using RecPC.Source_Rowid;       

          vDisplay_seq := vDisplay_seq + 1; 
          
          Insert into xxcp_edit_reference_temp(
              REF_CTL_ID   ,                        
              USER_ID      ,                          
              PREVIEW_ID   ,                         
              SESSION_ID   ,                         
              ATTRIBUTE_ID ,                         
              COLUMN_NAME  ,                         
              TITLE        ,                         
              DATA_VALUE   ,                        
              USAGE_ID     ,                         
              CREATED_BY   ,                         
              CREATION_DATE,
              DISPLAY_SEQ                         
              )
           Values(recPC.Ref_Ctl_Id,
                  cUser_id,
                  cUser_id,
                  fnd_global.session_id,
                  cAttribute_id,
                  recDa.Column_Name,
                  RecDa.Title,
                  vData,
                  '2',
                  fnd_global.USER_ID,
                  sysdate,
                  vDisplay_seq);
        End Loop;
      End Loop;
       
     End If;  
     Return(0);
  
  End Create_Edit_Ref_Temp;
 

  -- **********************************************************
  --              Read_TrigDef_Clob
  -- **********************************************************
 Function Read_TrigDef_Clob (cTrigger_Name IN XXCP_TRIGGER_DEFINITIONS.TRIGGER_NAME%TYPE,
                             cIndex        IN NUMBER) Return varchar2 IS
   vString VARCHAR2 (2000);
 BEGIN
   SELECT SUBSTR (DEFAULT_TRIGGER_BODY, 1 + cIndex * 1000, 1000)
     INTO vString
     FROM XXCP_TRIGGER_DEFINITIONS
    WHERE TRIGGER_NAME = cTRIGGER_NAME;
   
   RETURN (vString);
 END Read_TrigDef_Clob;

  -- **********************************************************
  --                      Read_Utils_Clob
  -- **********************************************************
 Function Read_Utils_Clob (cSource_Id IN xxcp_custom_utils_code.source_id%TYPE,
                           cCode_Name IN xxcp_custom_utils_code.code_name%TYPE, 
                           cIndex     IN NUMBER) Return varchar2 IS
   vString VARCHAR2 (2000);
 Begin
   SELECT SUBSTR (execution_block, 1 + cIndex * 1000, 1000)
   INTO   vString
   FROM   xxcp_custom_utils_code
   WHERE  source_id = cSource_id
   AND    code_name = cCode_Name;
   
   RETURN (vString);
 End Read_Utils_Clob;

 -- **********************************************************
 --                     Clear_Utils_Clob
 -- **********************************************************
 Procedure Clear_Utils_Clob (cSource_Id IN xxcp_custom_utils_code.source_id%TYPE,
                             cCode_Name IN xxcp_custom_utils_code.code_name%TYPE) IS
 Begin
   Update xxcp_custom_utils_code 
   Set    execution_block = NULL
   where  source_id = cSource_id
   and    code_name = cCode_Name;
 End Clear_Utils_Clob;

 -- **********************************************************
 --                     Append_Utils_Clob
 -- **********************************************************
 Procedure Append_Utils_Clob (cSource_Id IN xxcp_custom_utils_code.source_id%TYPE,
                              cCode_Name IN xxcp_custom_utils_code.code_name%TYPE, 
                              cString    IN VARCHAR2) IS
 Begin
   Update xxcp_custom_utils_code 
   Set    execution_block = Execution_block || to_clob(cString)
   where  source_id = cSource_id
   and    code_name = cCode_Name;
 End Append_Utils_Clob;

  -- **********************************************************
  --                      Read_Queue_Def_Select_Clob
  -- **********************************************************
 Function Read_Queue_Def_Select_Clob (cSource_Id  IN xxcp_queue_definitions.source_id%TYPE,
                                      cSqlBuildId IN xxcp_queue_definitions.sql_build_id%TYPE, 
                                      cIndex      IN NUMBER) Return varchar2 IS
   vString VARCHAR2 (2000);
 Begin
   SELECT SUBSTR (qd.select_statement, 1 + cIndex * 1000, 1000)
   INTO   vString
   FROM   xxcp_queue_definitions qd
   WHERE  qd.source_id = cSource_id
   AND    qd.sql_build_id = cSqlBuildId;
   
   RETURN (vString);
 End Read_Queue_Def_Select_Clob;

  -- **********************************************************
  --                      Read_Queue_Def_Insert_Clob
  -- **********************************************************
 Function Read_Queue_Def_Insert_Clob (cSource_Id  IN xxcp_queue_definitions.source_id%TYPE,
                                      cSqlBuildId IN xxcp_queue_definitions.sql_build_id%TYPE, 
                                      cIndex      IN NUMBER) Return varchar2 IS
   vString VARCHAR2 (2000);
 Begin
   SELECT SUBSTR (qd.insert_statement, 1 + cIndex * 1000, 1000)
   INTO   vString
   FROM   xxcp_queue_definitions qd
   WHERE  qd.source_id = cSource_id
   AND    qd.sql_build_id = cSqlBuildId;
   
   RETURN (vString);
 End Read_Queue_Def_Insert_Clob;


 -- **********************************************************
 --                     Clear_Queue_Def_Clob (Select)
 -- **********************************************************
 Procedure Clear_Queue_Def_Clob  (cSource_Id    IN xxcp_queue_definitions.source_id%TYPE,
                                  cSQL_Build_Id IN xxcp_queue_definitions.sql_build_id%TYPE) is
                                  
 Begin
   Update xxcp_queue_definitions 
   Set    select_statement = NULL
   where  source_id    = cSource_id
   and    sql_build_id = cSQL_Build_Id;
 End Clear_Queue_Def_Clob;

 -- **********************************************************
 --                     Clear_Queue_Def_Insert_Clob
 -- **********************************************************
 Procedure Clear_Queue_Def_Insert_Clob  (cSource_Id    IN xxcp_queue_definitions.source_id%TYPE,
                                         cSQL_Build_Id IN xxcp_queue_definitions.sql_build_id%TYPE) is
                                  
 BEGIN
   
   Update xxcp_queue_definitions 
   Set    insert_statement = NULL
   where  source_id    = cSource_id
   and    sql_build_id = cSQL_Build_Id;
   
 End Clear_Queue_Def_Insert_Clob;

 -- **********************************************************
 --                     Append_Queue_Def_Clob
 -- **********************************************************
 Procedure Append_Queue_Def_Clob (cSource_Id    IN xxcp_queue_definitions.source_id%TYPE,
                                  cSQL_Build_Id IN xxcp_queue_definitions.sql_build_id%TYPE, 
                                  cString       IN VARCHAR2) is
 Begin

   Update xxcp_queue_definitions 
   Set    select_statement = select_statement || to_clob(cString)
   where  source_id        = cSource_id
   and    SQL_Build_Id     = cSQL_Build_Id;

 End Append_Queue_Def_Clob;
 
  -- **********************************************************
 --                     Append_Queue_Def_Insert_Clob
 -- **********************************************************
 Procedure Append_Queue_Def_Insert_Clob (cSource_Id    IN xxcp_queue_definitions.source_id%TYPE,
                                         cSQL_Build_Id IN xxcp_queue_definitions.sql_build_id%TYPE, 
                                         cString       IN VARCHAR2) is
 Begin
   Update xxcp_queue_definitions 
   Set    insert_statement = insert_statement || to_clob(cString)
   where  source_id        = cSource_id
   and    SQL_Build_Id     = cSQL_Build_Id;
 End Append_Queue_Def_Insert_Clob;

  -- **********************************************************
  --                      Read_PV_Errors_Clob
  -- **********************************************************
 Function Read_Errors_Clob (cError_id  IN number, 
                            cIndex     IN NUMBER) Return varchar2 IS
   vString VARCHAR2 (2000);
 Begin
   SELECT SUBSTR (long_message, 1 + cIndex * 1000, 1000)
   INTO   vString
   FROM   xxcp_errors
   WHERE  error_id = cError_id;
   
   RETURN (vString);
 End Read_Errors_Clob;
 
   -- **********************************************************
  --                      Read_PV_Errors_Clob
  -- **********************************************************
 Function Read_PV_Errors_Clob (cError_id  IN number, 
                          cIndex     IN NUMBER) Return varchar2 IS
   vString VARCHAR2 (2000);
 Begin
   SELECT SUBSTR (long_message, 1 + cIndex * 1000, 1000)
   INTO   vString
   FROM   xxcp_pv_errors
   WHERE  error_id = cError_id;
   
   RETURN (vString);
 End Read_PV_Errors_Clob;

  -- **********************************************************
  --                      Read_WS_Clob
  -- **********************************************************
 Function Read_WS_Clob (cSource_Id IN xxcp_web_service_ctl.source_id%TYPE,
                        cWS_name   IN xxcp_web_service_ctl.ws_name%TYPE, 
                        cIndex     IN NUMBER) Return varchar2 IS
   vString VARCHAR2 (2000);
 Begin
   SELECT SUBSTR (ws_request, 1 + cIndex * 1000, 1000)
   INTO   vString
   FROM   xxcp_web_service_ctl
   WHERE  source_id = cSource_id
   AND    ws_name   = cWS_name;
   
   RETURN (vString);
 End Read_WS_Clob;

 -- **********************************************************
 --                     Clear_WS_Clob
 -- **********************************************************
 Procedure Clear_WS_Clob (cSource_Id IN xxcp_web_service_ctl.source_id%TYPE,
                          cWS_name   IN xxcp_web_service_ctl.ws_name%TYPE) IS
 Begin
   Update xxcp_web_service_ctl 
   Set    ws_request = NULL
   where  source_id = cSource_id
   and    ws_name   = cWS_name;
 End Clear_WS_Clob;

 -- **********************************************************
 --                     Append_Utils_Clob
 -- **********************************************************
 Procedure Append_WS_Clob (cSource_Id IN xxcp_web_service_ctl.source_id%TYPE,
                           cWS_name   IN xxcp_web_service_ctl.ws_name%TYPE, 
                           cString    IN VARCHAR2) IS
 Begin
   Update xxcp_web_service_ctl 
   Set    ws_request = ws_request || to_clob(cString)
   where  source_id = cSource_id
   and    ws_name   = cWS_name;
 End Append_WS_Clob;

 --
 --  Trim_Form_Max_Label
 --
 Function Trim_Form_Max_Label( cText in varchar2, cMax_Size in number) return varchar2 is

  vMaxSize number(4) := 0;
  i        integer;
  j        integer;
  k        integer;
  
  vText        varchar2(500);
  vPrompt_Text varchar2(500);
  
  vLine1 varchar2(50);
  vLine2 varchar2(50);
  
 begin
  
   vPrompt_Text := cText;
   vText        := cText;
   vMaxSize     := cMax_Size;
  
   If Length(vText) > vMaxSize then
    -- First line
    For j in 1..vMaxSize loop
      i := instr(substr(vText,1,vMaxSize),chr(32),vMaxSize-j);
      
      If i > 0 then
        vLine1 := substr(vText,1,i-1);
        -- Second Line
        vLine2 := substr(vText,i+1,100);
        vPrompt_text := vLine1 || chr(10) || vLine2;        

        If length(vLine2) > vMaxSize then

         for k in 1..vMaxSize loop
           i := instr(substr(vLine2,1,vMaxSize),chr(32),vMaxSize-j);
           
            if i > 0 then
             vLine2 := substr(vLine2,1,i-1);
             vPrompt_text := vLine1 || chr(10) || vLine2;        
             Exit;
            End If;
         end loop;
        End If;
        
        Exit;
      End if;
      
    End Loop;
   End If;
  
  Return(vPrompt_Text);
  
  End Trim_Form_Max_Label;

  --
  -- Submit Journals
  --
  Function Submit_Journal_Entry(cJournal_Header_id in number, cUser_id in number) return number is

     vInternalErrorCode    number(8) := 130;

     cursor c1(pJournal_Header_id in number) is
        select y.source_assignment_id, 
               y.responsibility_id, 
               y.source_table_id, 
               t.transaction_table,
               y.status,
               y.reversal_status
          from xxcp_journal_entry_headers y,
               xxcp_sys_source_tables t
         where y.je_header_id    = pJournal_header_id
           and y.source_table_id = t.source_table_id;
            
     vTransaction_Table    varchar2(30);
     vSource_Assignment_id xxcp_source_assignments.source_assignment_id%type;
     vStatus               xxcp_journal_entry_headers.status%TYPE;   

  Begin
  
     For Rec in c1(cJournal_header_id) loop
        vSource_Assignment_id := rec.source_assignment_id;
        vTransaction_Table    := rec.transaction_table;
        vStatus               := rec.status;       
     End Loop;
  
     Begin

        IF UPPER(vStatus) = 'OPEN' THEN
       
           Insert into xxcp_gl_interface(
             vt_status,
             vt_source_assignment_id,
             vt_transaction_table,
             vt_transaction_type,
             vt_transaction_class,
             vt_transaction_ref,
             vt_transaction_id,
             vt_parent_trx_id,
             vt_transaction_date,
             vt_transaction_line,
             status,
             actual_flag,
             ledger_id,
             set_of_books_id,
             entered_dr,
             entered_cr,
             accounted_dr,
             accounted_cr,
             currency_code,
             vt_qualifier1,
             Reference1,
             Reference2,
             Reference3,
             Reference4,
             Reference5,
             Reference6,
             Reference7,
             Reference8,
             Reference9,
             Reference10,
             Reference11,
             Reference12,
             Reference13,
             Segment1,
             Segment2,
             Segment3,
             Segment4,
             Segment5,
             Segment6,
             Segment7,
             Segment8,
             Segment9,
             Segment10,
             Segment11,
             Segment12,
             Segment13,
             Segment14,
             Segment15,
             Accounting_Date,
             date_created,
             created_by,
             user_je_source_name,
             user_je_category_name,
             Attribute1,
             Attribute2,
             Attribute3,
             Attribute4,
             Attribute5,
             Attribute6,
             Attribute7,
             Attribute8,
             Attribute9,
             Attribute10,
             Attribute11,
             Attribute12,
             Attribute13,
             Attribute14,
             Attribute15,
             User_currency_conversion_type,
             Currency_conversion_date )
          select 'NEW'
                ,vSource_Assignment_id
                ,vTransaction_Table
                ,h.transaction_type
                ,l.accounting_class
                ,h.journal_reference
                ,l.je_line_id --new
                ,h.je_header_id
                ,h.accounting_date
                ,l.je_line_num -- new
                ,'NEW'
                ,'A'
                ,l.set_of_books_id -- ledger
                ,-1
                ,l.entered_dr
                ,l.entered_cr
                ,l.accounted_dr
                ,l.accounted_cr
                ,h.currency_code
                ,l.sender_receiver
                ,h.name --reference1
                ,h.description
                ,h.clearing_company
                ,h.name --reference4
                ,h.description --reference5
                ,to_char(l.quantity) --reference6
                ,null --reference7
                ,null --reference8
                ,null --reference9
                ,l.description --reference10
                ,null --reference11
                ,null --reference12
                ,h.exchange_rate
                ,l.Segment1
                ,l.Segment2
                ,l.Segment3
                ,l.Segment4
                ,l.Segment5
                ,l.Segment6
                ,l.Segment7
                ,l.Segment8
                ,l.Segment9
                ,l.Segment10
                ,l.Segment11
                ,l.Segment12
                ,l.Segment13
                ,l.Segment14
                ,l.Segment15
                ,h.Accounting_Date
                ,sysdate
                ,cUser_id
                ,h.je_source
                ,h.je_category
                ,l.Attribute1
                ,l.Attribute2
                ,l.Attribute3
                ,l.Attribute4
                ,l.Attribute5
                ,l.Attribute6
                ,l.Attribute7
                ,l.Attribute8
                ,l.Attribute9
                ,l.Attribute10
                ,l.Attribute11
                ,l.Attribute12
                ,l.Attribute13
                ,l.Attribute14
                ,l.Attribute15
                ,h.exchange_type
                ,h.exchange_date
          from xxcp_journal_entry_headers h,
               xxcp_journal_entry_lines l
         where h.je_header_id = l.je_header_id
           and h.je_header_id = cJournal_Header_id;
      
           vInternalErrorCode := 0;

        ELSE

           vInternalErrorCode := 140; -- Journal Already Submitted

        END IF;

     Exception 
        when others then 
           vInternalErrorCode := 14551;
  
     End;
    
    Return(vInternalErrorCode);
  
  End Submit_Journal_Entry;
  
  -- Check_Segment
  Function Check_Segment(cSeg_Number           in number, 
                          cSegment_Value        in varchar2,
                          cChart_of_Accounts_id in number,
                          cInstance_id          in number,
                          cSeg_Description     out varchar2) return varchar2 IS

  Cursor c4(pSeg_Number in number, pSegment_Value in varchar2) is          
  select Segment_Value, Description, Segment_Value High_Segment
    from XXCP_INSTANCE_COA_SEG_VALS_V w
   where instance_id     = cInstance_id
     and coa_id          = cChart_of_Accounts_id
     and w.SEGMENT_NUM   = pSeg_Number
     and w.segment_value = pSegment_value;

  vResult  varchar2(1) := 'N';
  vMessage varchar2(100);

  BEGIN
  
  For Rec in c4(pSeg_Number => cSeg_Number, pSegment_Value => cSegment_Value) Loop
     vResult := 'Y';
     cSeg_Description := Rec.description;
  End Loop;
  
  If vResult = 'N' then
    vMessage := 'Error Validating Account Segment '||to_char(cSeg_Number)||' <'||cSegment_Value||'>';
  End If;
  
  Return(vMessage);
   
  END Check_Segment;  
  
  -- Validate_Account_String
  Procedure Validate_Account_String(cAccount_String       in varchar2,
                                    cChart_of_Accounts_id in number,
                                    cInstance_id          in number,
                                    cSegment1             in out varchar2,
                                    cSegment2             in out varchar2,
                                    cSegment3             in out varchar2,
                                    cSegment4             in out varchar2,
                                    cSegment5             in out varchar2,
                                    cSegment6             in out varchar2,
                                    cSegment7             in out varchar2,
                                    cSegment8             in out varchar2,
                                    cSegment9             in out varchar2,
                                    cSegment10            in out varchar2,
                                    cSegment11            in out varchar2,
                                    cSegment12            in out varchar2,
                                    cSegment13            in out varchar2,
                                    cSegment14            in out varchar2,
                                    cSegment15            in out varchar2,
                                    cAccount_Description  out varchar2,
                                    cErrorMessage         out varchar2,
                                    cInternalErrorCode    out number) IS

  cursor c2 is
     select w2.max_segments
       from XXCP_INSTANCE_COA_SEG_COUNT_V w2
      where coa_id      = cChart_of_Accounts_id
        and instance_id = cInstance_id;
        
  Cursor c3(pInstance_id in number,
            pCOA_id      in number,
            pSegment1    in varchar2,
            pSegment2    in varchar2,
            pSegment3    in varchar2,
            pSegment4    in varchar2,
            pSegment5    in varchar2,
            pSegment6    in varchar2,
            pSegment7    in varchar2,
            pSegment8    in varchar2,
            pSegment9    in varchar2,
            pSegment10   in varchar2,
            pSegment11   in varchar2,
            pSegment12   in varchar2,
            pSegment13   in varchar2,
            pSegment14   in varchar2,
            pSegment15   in varchar2
           ) is
  select 1 cnt
  from xxcp_instance_code_comb_v w3
  where w3.instance_id          = pInstance_id
    and w3.chart_of_accounts_id = pCOA_id
    and w3.segment1             = pSegment1
    and w3.segment2             = pSegment2
    and w3.segment3             = pSegment3      
    and w3.segment4             = pSegment4      
    and w3.segment5             = pSegment5      
    and w3.segment6             = pSegment6      
    and w3.segment7             = pSegment7      
    and w3.segment8             = pSegment8      
    and w3.segment9             = pSegment9     
    and w3.segment10            = pSegment10     
    and w3.segment11            = pSegment11     
    and w3.segment12            = pSegment12     
    and w3.segment13            = pSegment13     
    and w3.segment14            = pSegment14      
    and w3.segment15            = pSegment15;      
            
             
  vMax_Segments        number(2) := 0; 
  vEntered_Segments    number(2) := 0; 
  vFull_Account        varchar2(1000);
  vAccount_Description varchar2(4000);
  vSeg_Description     varchar2(4000);
  
  vSegments XXCP_DYNAMIC_ARRAY := XXCP_DYNAMIC_ARRAY('','','','','','','','','','','','','','','','','');
  
  x              number(5); 
  w              number := 0;
  vFound boolean := False;


BEGIN
  cInternalErrorCode := 0;

  If cAccount_String is not null then
  
    For Rec in c2 loop
      vMax_Segments := Rec.Max_Segments;
    End Loop;

    select nvl(length(cAccount_String)-length(replace(cAccount_String,'-','')),0)
    into   vEntered_Segments
    from   dual;
    
    If length(cAccount_String) > 0 then
      vEntered_Segments := vEntered_Segments + 1;
    End if;
    
    if vEntered_Segments > vMax_Segments then
      cInternalErrorCode := 7871; -- invalid account
      cErrorMessage := 'Error Validating Account String. Entered Segments <'||vEntered_Segments||'>'||
                       ' greater than Max Segments <'||vMax_Segments||'>';
    End if;
  
    if cInternalErrorCode = 0 then
      vFull_Account := cAccount_String;
     
      If vMax_Segments > 0 then
    
       For x in 1..vMax_Segments Loop
        
        If instr(vFull_Account,'-') > 0 then
          vSegments(x) := substr(vFull_Account,1,instr(vFull_Account,'-')-1);
          vFull_Account := substr(vFull_Account,instr(vFull_Account,'-')+1,200);
        ElsIf instr(vFull_Account,'-') = 0 and vFull_Account is not null then
          vSegments(x) := vFull_Account;
          vFull_Account := Null;
        End If;  
          
        cErrorMessage := Check_Segment(x,
                                       vSegments(x), 
                                       cChart_of_Accounts_id, 
                                       cInstance_id, 
                                       vSeg_Description);
        
        If cErrorMessage is not null then
          cInternalErrorCode := 7871; -- invalid account
          Exit;
        Else
          -- Concat Account Description
          If vAccount_Description is null then
            vAccount_Description := vSeg_Description;
          else
            vAccount_Description := vAccount_Description||'-'||vSeg_Description;
          end if;  
        End If;

       End Loop;
      End If;
    End if;  
   End If;
   -- Return split up segments 
     cSegment1  := vSegments(1);
     cSegment2  := vSegments(2);
     cSegment3  := vSegments(3);
     cSegment4  := vSegments(4);
     cSegment5  := vSegments(5);
     cSegment6  := vSegments(6);
     cSegment7  := vSegments(7);
     cSegment8  := vSegments(8);
     cSegment9  := vSegments(9);
     cSegment10 := vSegments(10);
     cSegment11 := vSegments(11);
     cSegment12 := vSegments(12);
     cSegment13 := vSegments(13);
     cSegment14 := vSegments(14);
     cSegment15 := vSegments(15); 
  
     cAccount_Description := vAccount_Description;
  END Validate_Account_String;
  
  -- dbms_sql_execute
  function dbms_sql_execute(cStmt in dbms_sql.varchar2a) return number is
  
    vCursor integer default dbms_sql.open_cursor;
    vRows   number default 0;
  
  begin
  
    dbms_sql.parse(c             => vCursor,
                   statement     => cStmt,
                   lb            => cStmt.first,
                   ub            => cStmt.last,
                   lfflg         => TRUE,
                   language_flag => dbms_sql.native);
  
    vRows := dbms_sql.execute(vCursor);
  
    dbms_sql.close_cursor(vCursor);
  
    return vRows;
    
  exception when others then 
    
    if dbms_sql.is_open(vCursor) then 
      dbms_sql.close_cursor(vCursor);
    end if;
  
  end dbms_sql_execute;

  -------------------------------------------------------------  
  -- generate_ff_alias_views
  -------------------------------------------------------------
  procedure generate_ff_alias_views(cFile_type_id in number) is
    -- dynamic sql vars.
    vStmt dbms_sql.varchar2a;
    vRows number default 0;

  begin

    for x in (select file_type_name,
                     record_type_name,
                     record_type,
                     source_id,
                     file_type_id,
                     view_name,
                     table_name,
                     case when table_type = 'OUTPUT' then 'Y' else 'N' end staged
                from xxcp_ff_file_types_v
                where file_type_id = cFile_Type_id) loop
    
      vStmt.delete;
    
      vStmt(1) := 'create or replace view ' || x.view_name || ' as ';
      vStmt(vStmt.last + 1) := 'select';
    
      for y in (select stmt
                  from (select 1 seq,
                               rownum row_number,
                               case
                                 when rownum > 1 then
                                  ','
                               end || t.column_name stmt
                          from all_tab_cols t
                         where t.table_name = x.table_name
                           and t.column_name like 'VT%'
--                           and t.column_name not in ('VT_FILE_TYPE_ID', 'VT_RECORD_TYPE')
--                             and t.column_name not in ('VT_FILE_EXPORT_ID') 03.06.12
                        union
                        select 2 seq,
                               rownum row_number,
                               ',' || column_name || ' "' || alias_column_name || '"' stmt
                          from xxcp_gen_ff_alias_views_v
                         where source_id = x.source_id
                           and file_type_id = x.file_type_id
                           and nvl(record_type, 'ALL') =
                               nvl(x.record_type, 'ALL')
                         order by seq, row_number)) loop
      
        -- insert the code.
        vStmt(vStmt.last + 1) := y.stmt;
      
      end loop;
    
      vStmt(vStmt.last + 1) := 'from ' || x.table_name;
    
      vStmt(vStmt.last + 1) := 'where vt_file_type_id = ' || x.file_type_id;
 --     if x.record_type is not null then 
 --       vStmt(vStmt.last + 1) := 'and   vt_record_type  = ''' || x.record_type || '''';
 --     end if;
    
      vRows := xxcp_forms.dbms_sql_execute(cStmt => vStmt);

      -- insert/update then xxcp_table_names
      merge into xxcp_table_names t
      using (select 0 instance_id, 
                    x.view_name view_name
             from dual) z
          on (z.instance_id = t.instance_id
         and z.view_name = t.table_name)
      -- if exists then update the last update columns
      when matched then 
        update set t.last_updated_by = fnd_global.USER_ID, 
                   t.last_update_date = sysdate
      -- otherwise insert the new record                   
      when not matched then 
        insert (instance_id, 
                table_name, 
                system_maintained, 
                staging_table,
                created_by, 
                creation_date)
      values (0, 
              x.view_name, 
              'Y', 
              x.staged,
              fnd_global.USER_ID, 
              sysdate);
    
      dbms_output.put_line('Successfully created ' || x.view_name);
    
    end loop;
    
    commit;

  end generate_ff_alias_views;    
  
  -------------------------------------------------------------  
  -- drop_ff_alias_views
  -------------------------------------------------------------
  procedure drop_ff_alias_views(cFile_type_id in number) is
    
    no_such_table EXCEPTION;
    PRAGMA EXCEPTION_INIT (no_such_table, -942);  
    
  begin
    
    for x in (select 'drop view '||view_name stmt, view_name
                from xxcp_ff_file_types_v
               where file_type_id = cFile_type_id) loop
      
      begin     
        Execute Immediate x.stmt;                     
      exception when no_such_table then 
        null;
      end;

      delete from xxcp_table_names 
      where table_name = x.view_name;      
      
      dbms_output.put_line('Successfully dropped ' || x.view_name);
    
    end loop;
    
  end drop_ff_alias_views;
  
  -------------------------------------------------------------  
  -- check_ff_view_used
  -------------------------------------------------------------
  function check_ff_view_used(cFile_type_id in number) return varchar2 is
    vRetVal varchar2(10);
  begin

  for x in (select 'Config' used_where 
             from (select transaction_table table_name 
                     from xxcp_column_rules_tables 
                    union 
                   select transaction_table table_name 
                     from xxcp_sys_target_tables 
                    union 
                   select target_table_override table_name 
                     from xxcp_sys_target_tables_ovr) t, 
                    xxcp_ff_file_types_v ty 
             where t.table_name = ty.view_name 
               and ty.file_type_id = cFile_type_id
             union 
            select 'Generated' used_where 
              from xxcp_all_generated_code_v, 
                   xxcp_ff_file_types_v 
             where check_column like '%' || view_name || '%' and file_type_id = cFile_type_id) loop 
                     
    vRetVal := x.used_where;
    
  end loop;

    Return(vRetVal);

  end check_ff_view_used;
  
  -------------------------------------------------------------  
  -- isKeyword
  -------------------------------------------------------------  
  function isKeyword(cStr in varchar2) return boolean is
    vBol boolean := false;
  begin

   -- 03.05.17 Removed reference to v$reserved_words as causing too many issues on install
   for x in (select 'Yes' bol
              from dual
             where upper(cStr) in ('ACCESS','ELSE','MODIFY','START','ADD','EXCLUSIVE','NOAUDIT','SELECT',
                                    'ALL','EXISTS','NOCOMPRESS','SESSION','ALTER','FILE','NOT','SET',
                                    'AND','FLOAT','NOTFOUND','SHARE','ANY','FOR','NOWAIT',
                                    'SIZE','ARRAYLEN','FROM','NULL','SMALLINT','AS','GRANT','NUMBER',
                                    'SQLBUF','ASC','GROUP','OF','SUCCESSFUL','AUDIT','HAVING','OFFLINE',
                                    'SYNONYM','BETWEEN','IDENTIFIED','ON','SYSDATE','BY','IMMEDIATE','ONLINE',
                                    'TABLE','CHAR','IN','OPTION','THEN','CHECK','INCREMENT','OR',
                                    'TO','CLUSTER','INDEX','ORDER','TRIGGER','COLUMN','INITIAL','PCTFREE',
                                    'UID','COMMENT','INSERT','PRIOR','UNION','COMPRESS','INTEGER','PRIVILEGES',
                                    'UNIQUE','CONNECT','INTERSECT','PUBLIC','UPDATE','CREATE','INTO','RAW',
                                    'USER','CURRENT','IS','RENAME','VALIDATE','DATE','LEVEL','RESOURCE',
                                    'VALUES','DECIMAL','LIKE','REVOKE','VARCHAR','DEFAULT','LOCK','ROW',
                                    'VARCHAR2','DELETE','LONG','ROWID','VIEW','DESC','MAXEXTENTS','ROWLABEL',
                                    'WHENEVER','DISTINCT','MINUS','ROWNUM','WHERE','DROP','MODE','ROWS','WITH')) loop
             
     vBol := true;             
   end loop; 
   
   return vBol;   
  end isKeyword;
  
  -------------------------------------------------------------  
  -- Get_Instance_Name
  -------------------------------------------------------------  
 Function Get_Instance_Name(cInstance_id in number) Return Varchar2 is
   
   Cursor c1(pInstance_id in number) is
     select db.instance_name
     from xxcp_instance_db_links_v db
     where db.instance_id = pInstance_id;
     
   vResult xxcp_instance_db_links_v.instance_name%type;
 
  Begin
    
     For Rec in c1(pInstance_id => cInstance_id) Loop
     
       vResult := Rec.Instance_Name;  
     
     End Loop;
     
     Return(vResult);
      
  End Get_Instance_Name;

    --
  -- Update_User_Profile_Source
  -- 
  Procedure Update_User_Profile_Source(cUser_id                in number,
                                       cSource_id              in number,
                                       cPV_Hist_Order_Type     in Varchar2,
                                       cPV_Hist_Order_Column1  in varchar2,
                                       cPV_Hist_Order_Column2  in varchar2,
                                       cPV_Hist_Order_Column3  in varchar2,
                                       cPV_Hist_Column_Asc     in varchar2,
                                       cPV_Hist_Order_Trx_Flag in varchar2) is

   pragma autonomous_transaction;
 
 

    -- Create User table
    vCnt integer := 0;
  Begin

     If cUser_id > 0 then

       Select count(*) into vCnt
       from xxcp_user_sources
       where user_id   = cUser_id
         and source_id = cSource_id;

      If vCnt > 0 then
        
        Update xxcp_user_sources w
          set w.pv_hist_order_type     = cPV_Hist_Order_Type,
              w.pv_hist_order_column1  = cPV_Hist_Order_Column1,
              w.pv_hist_order_column2  = cPV_Hist_Order_Column2,
              w.pv_hist_order_column3  = cPV_Hist_Order_Column3,
              w.pv_hist_column_asc     = cPV_Hist_column_asc,
              w.pv_hist_order_trx_flag = cPV_Hist_order_trx_flag,
              w.last_update_date = sysdate,
              w.last_updated_by = fnd_global.user_id
              
        where user_id   = cUser_id
          and source_id = cSource_id;   
 

        Commit;
      End If;
    End If;
  End Update_User_Profile_Source;  

  --
  -- Clob_Replace
  -- 
  FUNCTION Clob_Replace (cClob IN CLOB,
                         cWhat IN VARCHAR2,
                         cWith IN VARCHAR2 ) RETURN CLOB IS

    vWhatLen       CONSTANT PLS_INTEGER := LENGTH(cWhat);
    vWithLen       CONSTANT PLS_INTEGER := LENGTH(cWith);

    vReturn        CLOB;
    vSegment       CLOB;
    vPos           PLS_INTEGER := 1 - vWithLen;
    vOffset        PLS_INTEGER := 1;

  BEGIN

    IF cWhat IS NOT NULL THEN
      WHILE vOffset < DBMS_LOB.GETLENGTH(cClob) LOOP
        vSegment := DBMS_LOB.SUBSTR(cClob,32767,vOffset);
        LOOP
          vPos := DBMS_LOB.INSTR(vSegment, cWhat, vPos + vWithLen);
          EXIT WHEN (NVL(vPos,0) = 0) OR (vPos = 32767 - vWithLen);
          vSegment := TO_CLOB( DBMS_LOB.SUBSTR(vSegment,vPos-1)
                              ||cWith
                              ||DBMS_LOB.SUBSTR(vSegment, 32767 - vWhatLen - vPos - vWhatLen + 1, vPos + vWhatLen));
        END LOOP;
        vReturn := vReturn||vSegment;
        vOffset := vOffset + 32767 - vWhatLen;
      END LOOP;
    END IF;

    RETURN(vReturn);

  END Clob_Replace;
  
Begin

  Init;

END XXCP_FORMS;
/
