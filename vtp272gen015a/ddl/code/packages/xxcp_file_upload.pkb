CREATE OR REPLACE PACKAGE BODY XXCP_FILE_UPLOAD AS
  /******************************************************************************
                          V I R T A L  T R A D E R  L I M I T E D
       (Copyright 2002-2012 Virtual Trader Ltd. All rights Reserved)

     NAME:       XXCP_FILE_UPLOAD
     PURPOSE:    Enable external files to be uploaded into VT.

     REVISIONS:
     Ver        Date      Author  Description
     ---------  --------  ------- ------------------------------------
     02.01.00   11/10/06  Nigel   First Coding
     02.06.01   28/06/10  Simon   Modified to enable csv files to be uploaded.
     02.06.02   09/07/10  Simon   Show warning if file fails.
     02.06.03   26/08/10  Simon   Get XXCPLOAD path from single source.
                                  Insert into XXCP_FILE_UPLOAD_HISTORY to prevent duplicates.
     02.06.04   08/11/10  Simon   Only process .csv files.
     02.06.05   26/10/11  Nigel   Added Fixed format load and export file
     02.06.06   12/01/12  Nigel   Added Qualifiers1..4 to xxcp_staged_flat_file
     02.06.07   19/01/12  Nigel   Added run_sequence1..2 to cp_staged_flat_file
     02.06.08   17/02/12  Nigel   Removed max from returning clause 9i compliant
     02.06.09   21/03/12  Nigel   Added before_file_export custom event
     02.06.10   23/03/12  Nigel   Various bug fixes.
     02.06.11   25/04/12  Nigel   Added system profile for archive directory
     03.06.12 	03/05/12  Nigel	  OOD Fixes
     03.06.13   31/05/12  Nigel   Changes to File Export Archive directory.
     03.06.14   06/06/12  Simon   Add exception handling to Init procedure.
     03.06.15   16/08/12  Nigel   Add updateStagedFlatFile function.
     03.06.16   06/11/12  Mark    Export file procedure c2 cursor to have extra predicate.
	   03.07.01   17/05/12  Simon   Modified getNumberOfRows.
     03.07.02   30/05/12  Simon   Modified getExcelXSL to fix shifting cells.
     03.07.03   16/10/12  Mat     OOD-358 Add updateStagedFlatFile function.     
     03.07.04   12/11/12  Simon   File Loader now ignores blank rows (OOD-447).
     03.07.05   07/01/13  Simon   Fix large xml not uploading (OOD-607).     
  ******************************************************************************/

  TYPE t_column_rec is Record
    (column_name  varchar2(30),
     data_type    varchar2(10),
     data_length  number(5),
     data_value   varchar2(1000),
     trim_data    varchar2(1));

  TYPE t_fixed_rec is record
    (col varchar2(100),
     pos number,
     len number);

  TYPE t_column_tab is table of t_column_rec
    index by binary_integer;

  TYPE t_fixed_tab is table of t_fixed_rec
    index by binary_integer;

  TYPE t_record_type is table of t_fixed_tab
    index by varchar2(30);

  TYPE t_FileName_tab is table of varchar2(1000)
    index by binary_integer;

  TYPE t_Cols_tab is table of varchar2(1000)
    index by binary_integer;

  TYPE t_Cols_tab_2d is table of t_Cols_tab
    index by binary_integer;

  TYPE t_Heading_tab is table of varchar2(1000)
    index by binary_integer;

  TYPE t_Heading_tab_char is table of number
    index by varchar2(100);

  gColumn_tab       t_column_tab;
  gFixed_tab        t_fixed_tab;
  gFixed_rec_tab    t_record_type;
  gFileName_tab     t_FileName_tab;
  gData_row         t_Cols_tab;
  gData_tab         t_Cols_tab_2d;
  gHeading_tab      t_Heading_tab;
  gHeading_tab_char t_Heading_tab_char;
  gBind_tab         t_Heading_tab;

  gWarning          boolean := FALSE;
  gFileRejected     boolean := FALSE;
  gDebug_On         boolean := FALSE;
  gDate_Format      varchar2(30) := 'DD/MON/YY';

  -- 02.06.09
  gArchive_export     varchar2(1)   := 'N';
  gArchive_export_dir varchar2(100);

  gArchive_import     varchar2(1)   := 'N';
  gArchive_import_dir varchar2(100);

  -- *************************************************************
  --                     Software Version Control
  --                   -----------------------------
  -- This package can be checked with SQL to ensure it's installed
  -- select packagename.software_version from dual;
  -- *************************************************************
  FUNCTION Software_Version RETURN VARCHAR2 IS
  Begin
    Return('Version [03.07.05] Build Date [07-JAN-2013] NAME [XXCP_FILE_UPLOAD]');
  END SOFTWARE_VERSION;

  -- 02.06.09
  -- init
  procedure init is

   Cursor SP(pProfile_Category in varchar2, pProfile_Name in varchar2) is
    select p.profile_value
      from xxcp_sys_profile p
     where p.profile_name = pProfile_Name
       and p.profile_category = pProfile_Category;

  begin

    For Rec in SP('EV','FILE EXPORT ARCHIVE ENABLED') loop
       gArchive_export := Rec.profile_value;
    End Loop;

    For Rec in SP('EV','FILE EXPORT ARCHIVE DIRECTORY') loop
       gArchive_export_dir := Rec.profile_value;

       Begin
         -- 03.05.13 Grant write privileges to export archive directory.
         execute immediate 'begin DBMS_JAVA.grant_permission(''APPS'', ''java.io.FilePermission'',''' ||Rec.profile_value || '/-'', ''write''); end;';
       Exception
         When Others then
           null;  -- Catch and ignore any Java Excpeptions
       End;
    End Loop;

    For Rec in SP('EV','FILE IMPORT ARCHIVE ENABLED') loop
       gArchive_import := Rec.profile_value;
    End Loop;

    For Rec in SP('EV','FILE IMPORT ARCHIVE DIRECTORY') loop
       gArchive_import_dir := Rec.profile_value;

       Begin
         -- 03.05.13 Grant write privileges to import archive directory.
         execute immediate 'begin DBMS_JAVA.grant_permission(''APPS'', ''java.io.FilePermission'',''' ||Rec.profile_value || '/-'', ''write''); end;';
       Exception
         When Others then
           null;  -- Catch and ignore any Java Excpeptions
       End;

    End Loop;

  end init;

  --
  --  Writes to Log or Dbms_output
  --  depending on Mode
  --
  Procedure writeOutput(vOutput IN VARCHAR2) is

  begin
    if gDebug_On then
      dbms_output.put_line(vOutput);
    else
      fnd_file.put_line(fnd_file.output,vOutput);
    end if;
  end writeOutput;

-- Displays Error page
PROCEDURE errorPage(cErrorText IN VARCHAR2) IS
  BEGIN

    htp.htmlOpen;
    htp.headOpen;
    htp.title('Error');
    htp.headClose;
    htp.bodyOpen;
    htp.img2('/OA_MEDIA/FNDLOGOS.gif',calign => 'Left',calt => 'Logo');
    htp.br;
    htp.br;
    htp.p('<h4>Error Processing File.</h4>');
    htp.hr;
    htp.p ('<h4>'||substr(cErrorText,1,255)||'</h4>');
    htp.br;
    htp.bodyClose;
    htp.htmlClose;

  END errorPage;

  Function isFileTypeValid(cFile_name in varchar2) return boolean is
    vRetVal boolean := false;
  begin

    -- make sure that the file has an defined extension
    for x in (select distinct '.'||nvl(file_extension,'CSV') file_extension
                from xxcp_ff_file_types_v) loop

      if upper(substr(cFile_Name,-4)) = x.file_extension then
        vRetVal := true;
      end if;
    end loop;

    return vRetVal;

  end isFileTypeValid;

----------------------------------------------------------------
-- This function gets a directory list
-- via a java stored procedure
----------------------------------------------------------------
FUNCTION getDirectoryList(cPath      IN VARCHAR2,
                          cSource_id IN VARCHAR2) return boolean is

  vDirList  varchar2(32000);
  vFileName varchar2(500);
  vSep      varchar2(1)   := ',';
  vDummy    varchar2(1);

  cursor c1(pShort_Code varchar2) is
    select 'X'
    from   xxcp_source_assignments
    where  source_id  = cSource_id
    and    short_code = pShort_Code;

begin
  -- Call Java Stored Procedure
  vDirList := XXCP_File_API.list(cPath);

  While length(vDirList) > 0 loop
    if instr(vDirList,vSep) > 0 then
      vFileName := substr(vDirList,1,instr(vDirList,vSep)-1);
      vDirList  := substr(vDirList,instr(vDirList,vSep)+1);
    else
      vFileName := vDirList;
      vDirList  := null;
    end if;

    -- Add ONLY if .csv file and NOT a directory
    if isFileTypeValid(vFileName) then  -- 02.06.04
          vDummy := null;
          open c1(substr(vFileName,7,3));  -- 3 char Short Code in Filename
          fetch c1 into vDummy;
          close c1;
          if vDummy is not null then
            gFileName_tab(gFileName_tab.count+1) := vFileName;
          end if;
    end if;
  End loop;

  return(TRUE);

end getDirectoryList;


----------------------------------------------------------------
-- This function gets a file loaded into FND_LOBS
-- by XXCPMLFLUPL and returns a CLOB.
----------------------------------------------------------------
FUNCTION getExcelXML(cFileId IN Number) RETURN CLOB IS

CURSOR c1 (cFileId number) IS
  SELECT file_data, oracle_charset
    FROM fnd_lobs
   WHERE file_id           = cFileId
     AND file_content_type = 'text/xml'
     AND program_name      = 'XXCPEXLOAD';

vCharSet       VARCHAR2(1000);
vWarning       NUMBER;
vBlobOffset    NUMBER := 1;
vClobOffset    NUMBER := 1;
vLangContext   NUMBER := 0;
vXmlBlob       BLOB;
vXmlClob       CLOB;

BEGIN

  DBMS_LOB.createtemporary (vXmlClob, TRUE);
  DBMS_LOB.createtemporary (vXmlBlob, TRUE);

  OPEN c1(cFileId);

  FETCH c1 INTO  vXmlBlob, vCharSet;

  IF c1%FOUND THEN

  -- Convert the returned BLOB to CLOB
  DBMS_LOB.convertToClob(dest_lob     => vXmlClob
                        ,src_blob     => vXmlBlob
                        ,amount       => DBMS_LOB.getlength(vXmlBlob)
                        ,blob_csid    => NLS_CHARSET_ID(vCharSet)
                        ,lang_context => vLangContext
                        ,warning      => vWarning
                        ,dest_offset  => vClobOffset
                        ,src_offset   => vBlobOffset);
  END IF;

  CLOSE c1;

  RETURN vXmlClob;

END getExcelXML;

------------------------------------------------------------------------------------
-- This function returns the XSL stylesheet to transform an EXCEL spreadsheet XML
------------------------------------------------------------------------------------
FUNCTION getExcelXSL(cColumn_Number IN Number)  RETURN CLOB IS

-- Define the local variables
vXsl       CLOB;
vXslT      CLOB;
--
i          Number := 0;
vColNumber Number := 10;

BEGIN

vColNumber := nvl(cColumn_Number,10);

vXsl := '<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- Edited by XMLSpy? -->
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet">
<xsl:output method="xml" indent="yes"/>
<xsl:template match="/">
  <xsl:for-each select="ss:Row [position() &gt; 0]">
    <xsl:if test="ss:Cell/ss:Data">
    <ROW>
      <xsl:call-template name="incrementValue">
          <xsl:with-param name="cellnum" select="1"/>
        <xsl:with-param name="lastcell" select="count(ss:Cell)"/>
          <xsl:with-param name="index" select="1"/>
      </xsl:call-template>
    </ROW>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template name="incrementValue">
  <xsl:param name="index"/>
  <xsl:param name="lastcell"/>
  <xsl:param name="cellnum"/>

  <xsl:variable name="col_count">
    <xsl:choose>
      <xsl:when test="ss:Cell[position()=$cellnum]/@ss:Index != ''''">
        <xsl:value-of select="ss:Cell[position()=$cellnum]/@ss:Index"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$index"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:variable name="column_name" select="concat(''COLUMN'', $col_count)"/>

  <xsl:element name="{$column_name}">
    <xsl:value-of select="ss:Cell[$cellnum]/ss:Data" />
  </xsl:element>

  <xsl:choose>
    <xsl:when test="$cellnum &lt; $lastcell">
      <xsl:call-template name="incrementValue">
        <xsl:with-param name="cellnum" select="$cellnum + 1"/>
        <xsl:with-param name="lastcell" select="$lastcell"/>
        <xsl:with-param name="index" select="$col_count + 1"/>

      </xsl:call-template>
    </xsl:when>
  </xsl:choose>

</xsl:template>
</xsl:stylesheet>';

RETURN vXsl;

END getExcelXSL;

--
-- getCSVHeadings
--
Function getCSVData(vFileHandle IN utl_file.file_type,
                          vSep        IN varchar2) Return Number is
  vLine varchar2(32000);
  vRow       number := 0;
  vErrorCode number := 0;
begin

  Loop
    Begin
      utl_file.get_line(vFileHandle, vLine);

      IF vLine IS NULL THEN
        EXIT;
      END IF;

      vRow := vRow+1;
      gData_Row.delete;

      -- Parse Line
      While length(vLine) > 0 loop
        if instr(vLine,vSep) > 0 then
          gData_Row(gData_Row.count+1) := trim(substr(vLine,1,instr(vLine,vSep)-1));
          vLine  := substr(vLine,instr(vLine,vSep)+1);
        else
          gData_Row(gData_Row.count+1) := trim(replace(replace(vLine,CHR(10),null),CHR(13),null));
          vLine        := null;
        end if;
      End loop;
    Exception
      WHEN NO_DATA_FOUND THEN
        EXIT;
    End;

    if gData_Row.count > gHeading_tab.count then
      writeOutput('Error: Data fields exceed number of headings in row <'||vRow||'>. Check for delimiter in data.');
      vErrorCode := -1;
      exit;
    end if;
    -- Assign Data Row to Table
    gData_Tab(vRow) := gData_Row;
  End Loop;

  return(vErrorCode);
Exception
  when others then
    writeOutput('Error: Unable to read data. Check delimiter is correct.');
    raise;

end getCSVData;

Function getFixedData(vFileHandle IN utl_file.file_type,
                      cRecord_type_start number,
                      cRecord_type_length number) Return Number is

  vLine varchar2(32000);
  vRow       number := 0;
  vErrorCode number := 0;
  vRecord_type varchar2(30);

  vFixed_tab t_fixed_tab;
begin

  Loop
    Begin
      utl_file.get_line(vFileHandle, vLine);

      IF vLine IS NULL THEN
        EXIT;
      END IF;

      vRow := vRow+1;
      gData_Row.delete;
      vFixed_tab.delete;

      -- get the record type (if used)
      if cRecord_type_start is not null then
        vRecord_type := rtrim(substr(vLine, cRecord_type_start, cRecord_type_length));

        -- return the the column headings for that record type.
        vFixed_tab := gFixed_rec_tab(vRecord_type);
      else
        vFixed_tab := gFixed_tab;
      end if;

      -- Parse Line
      for x in vFixed_tab.first..vFixed_tab.last loop
        gData_Row(gData_Row.count+1) := substr(vLine, vFixed_tab(x).pos, vFixed_tab(x).len);
      end loop;

    Exception
      WHEN NO_DATA_FOUND THEN
        EXIT;
    End;

    -- Assign Data Row to Table
    gData_Tab(vRow) := gData_Row;
  End Loop;

  return(vErrorCode);
Exception
  when others then
    writeOutput('Error: Unable to read data. Check delimiter is correct.');
    raise;

end getFixedData;
--
-- Function: validateRecord
--
FUNCTION validateColumn (cBind IN NUMBER,
                         cData IN VARCHAR2,
                         cRow  IN NUMBER) return boolean is

  vDummy     number;
  vDummyDate date;
begin
  -- VARCHAR
  if gColumn_tab(cBind).data_type = 'VARCHAR' then
    -- Test Length
    if length(cData) > gColumn_tab(cBind).data_length then
      writeOutput('Error validating data in Record <'||cRow||'>: <'||cData||'> greater than '||gColumn_tab(cBind).data_length||'.');
      return (FALSE);
    end if;
  -- NUMBER
  elsif gColumn_tab(cBind).data_type = 'NUMBER' then
    begin
      vDummy := to_number(cData);
    exception
      when others then
        writeOutput('Error validating data in Record <'||cRow||'>: Unable to convert <'||cData||'> to number.');
        return (FALSE);
    end;
  elsif gColumn_tab(cBind).data_type = 'DATE' then
    begin
      vDummyDate := to_date(cData,gDate_Format);
    exception
      when others then
        writeOutput('Error validating data in Record <'||cRow||'>: Unable to convert <'||cData||'> to date using format <'||gDate_Format||'>.');
        return (FALSE);
    end;
  End if;

  return(TRUE);
end validateColumn;


-----------------------------------------------------------------------
-- getCSVHeadings
-----------------------------------------------------------------------
Function getCSVHeadings(vFileHandle IN utl_file.file_type,
                          vSep        IN varchar2) Return Number is
  vLine varchar2(32000);
begin

  utl_file.get_line(vFileHandle, vLine);

  While length(vLine) > 0 loop
    if nvl(instr(vLine,vSep),0) > 0 then
      gHeading_tab(gHeading_tab.count+1) := replace(substr(vLine,1,instr(vLine,vSep)-1),CHR(15711167),null); -- Strip BOM for UTF8
      vLine  := substr(vLine,instr(vLine,vSep)+1);

      gHeading_tab_char(gHeading_tab(gHeading_tab.count)) := gHeading_tab_char.count + 1;
    else
      gHeading_tab(gHeading_tab.count+1) := replace(replace(vLine,CHR(10),null),CHR(13),null);
      vLine        := null;

      gHeading_tab_char(gHeading_tab(gHeading_tab.count)) := gHeading_tab_char.count + 1;

    end if;

  End loop;

  return(0);

Exception
  when others then
    writeOutput('Error: Unable to read Headings. Check delimiter is correct.');
    raise;

end getCSVHeadings;

-----------------------------------------------------------------------
-- getFixedHeadings
-----------------------------------------------------------------------
Function getFixedHeadings(vFileHandle IN utl_file.file_type) Return Number is
  vLine varchar2(32000);
begin

  for x in gFixed_tab.first..gFixed_tab.last loop
    gHeading_tab(gHeading_tab.count+1) := gFixed_tab(x).col;
    gHeading_tab_char(gHeading_tab(gHeading_tab.count)) := gHeading_tab_char.count + 1;
  end loop;

  return(0);

Exception
  when others then
    writeOutput('Error: Unable to read Headings. Check delimiter is correct.');
    raise;

end getFixedHeadings;

-----------------------------------------------------------------------
-- insertDynamic
-----------------------------------------------------------------------
Function insertDynamic(cMapping_Short_Code IN VARCHAR2,
                       cSA_Short_Code      IN VARCHAR2,
                       cFile_type_id       IN NUMBER) Return Number IS

  vData             VARCHAR2(4000);
  dest_cur          INTEGER;
  ignore            INTEGER;
  vDest_Tab         VARCHAR2(30);
  vInterface_Tab    VARCHAR2(30);
  vColumns          VARCHAR2(32000);
  vValues           VARCHAR2(32000);
  vSQL              VARCHAR2(32000);
  --vBind_cnt         NUMBER := 0;
  vFrom_Column_Name varchar2(100);
  vCol              NUMBER;
  vSource_id        number;
  vErrorCode        number := 0;
  vInserted         number := 0;
  vRejected         number := 0;
  vIs_Interface     boolean := TRUE;
  vSource_assignment_id number;
  vSet_of_Books_id  number;
  vOrg_id           number;
  vColumn_Tab_cnt   number := 0;
  vRowRejected      boolean := false;

  cursor c1 (pMapping_Short_Code in varchar2) is
    select attribute1 dest_table, source_id
    from   xxcp_table_set_ctl
    where  set_base_table  = 'CP_DATA_MAPPING'
    and    attribute3      = pMapping_Short_Code;
    --and    set_name        = pMapping_Set_Name;

  cursor c2(pMapping_Short_Code in varchar2, pInterface_table in varchar2) is
    select 0 seq, From_Column_Name, to_column_name, data_type, data_length, trim_data
     from xxcp_data_mapping m,
          xxcp_table_set_ctl c
    where m.mapping_set_id  = c.set_id
      --and c.set_name        = pMapping_Set_Name
      and c.attribute3      = pMapping_Short_Code
      and c.set_base_table  = 'CP_DATA_MAPPING'
      and from_column_name is not null
    union
   -- only return COLUMNx columns for XXCP_FLAT_FILE_INTERFACE
   select column_id seq, '{'||column_name||'}', column_name, data_type, data_length, null
     from all_tab_cols
    where table_name = pInterface_table
      and column_name like 'COLUMN%'
 order by 1, 3;

  cursor c3(pSA_Short_Code in varchar2) is
    select a.source_assignment_id,
           s.source_base_table Interface_Table,
           a.set_of_books_id,
           a.org_id
      from xxcp_source_assignments a,
           xxcp_sys_sources s
     where a.short_code = pSA_Short_Code
     and a.source_id = s.source_id;

BEGIN

  open c1(cMapping_Short_Code);
  fetch c1 into vDest_Tab, vSource_id;
  close c1;

  open c3(cSA_Short_Code);
  fetch c3 into vSource_assignment_id,
                vInterface_Tab,
                vSet_of_Books_id,
                vOrg_id;
  close c3;

  if vInterface_Tab <> vDest_Tab then
    vIs_Interface := FALSE;
  end if;

  if vDest_Tab is null then
    writeOutput(' Unable to find Mapping <'||cMapping_Short_Code||'>.');
    vErrorCode := -1;
  end if;

  if vSource_assignment_id is null and vIs_Interface then
    writeOutput(' Unable to find Source Assignment Id for Mapping  <'||cMapping_Short_Code||'>.');
    vErrorCode := -1;
  end if;

  -- open cursor on destination table
  dest_cur := dbms_sql.open_cursor;

  -- Build Insert Statement
  if vErrorCode = 0 then

    -- Add Seeded Columns
    -- if destination is an Interface Table.
    If vIs_Interface then
      vColumns := '( vt_source_assignment_id ';
      vValues  := ' VALUES(' ||to_char(vSource_Assignment_id);

      If upper(vInterface_Tab) = 'XXCP_GL_INTERFACE' then
        vColumns := vColumns||' ,set_of_books_id ';
        vValues  := vValues||' ,'||to_char(vSet_of_books_id);
      End If;

      If upper(vInterface_Tab) = 'XXCP_RA_INTERFACE_LINES' then
        vColumns := vColumns||' ,Org_id';
        vValues  := vValues||' ,'||to_char(vOrg_id);
      End If;

      If upper(vInterface_Tab) = 'XXCP_FLAT_FILE_INTERFACE' then
        vColumns := vColumns||' ,vt_file_type_id';
        vValues  := vValues||' ,'||cFile_type_id;
      end if;
    End if;


    for c2_rec in c2(cMapping_Short_Code, vDest_Tab) loop
      if substr(c2_rec.from_column_name,1,1) = '{' and substr(c2_rec.from_column_name,-1,1) = '}' then
        --vBind_cnt := vBind_cnt + 1;

        vFrom_Column_Name := ':' || to_char(gBind_tab.count+1);
        gBind_tab(gBind_tab.count+1) := replace(replace(c2_rec.from_column_name,'{',null),'}',null);

        vColumn_Tab_cnt := vColumn_Tab_cnt + 1;
        gColumn_tab(vColumn_Tab_cnt).column_name := c2_rec.to_column_name;
        gColumn_tab(vColumn_Tab_cnt).data_type   := c2_rec.data_type;
        gColumn_tab(vColumn_Tab_cnt).data_length := c2_rec.data_length;
        gColumn_tab(vColumn_Tab_cnt).trim_data := c2_rec.trim_data;
      else
        vFrom_Column_Name := c2_rec.from_column_name;
      end if;

      -- Column List
      If vColumns is null then
        vColumns := '(' || c2_rec.to_column_name;
      else
        vColumns := vColumns || ',' || c2_rec.to_column_name;
      end if;

      -- Value List
      If vValues is null then
        vValues := ' VALUES(' || vFrom_Column_Name;
      else
        vValues := vValues || ',' || vFrom_Column_Name;
      end if;

    end loop;

    vColumns := vColumns || ')';
    vValues  := vValues || ')';

    vSQL := 'INSERT INTO '||vDest_Tab||' '||vColumns||' '||vValues;
--xxcp_foundation.FndWriteError(5, vSQL);
    -- parse the INSERT statement
    dbms_sql.parse(dest_cur,vSQL,dbms_sql.NATIVE);

    For vRow in 1..gData_Tab.count loop
        vRowRejected := FALSE;
        --
        -- Need to Make this binding dynamic
        --
        for vBind in 1..gBind_tab.count loop
          vData := null;
          Begin
            -- Check if its a COLUMN column, if so the then we don't need the column heading as
            -- colunn 1 = field 1 in the file
            if substr(gBind_tab(vBind),1,6) = 'COLUMN' then
              vCol := substr(gBind_tab(vBind),7,2);
            else
              vCol := gHeading_tab_char(gBind_tab(vBind));
            end if;
          Exception
            when no_data_found then
              writeOutput(' Mapped column <'||gBind_tab(vBind)||'> does not exist in file.');
              vErrorCode := -2;
              gWarning := TRUE;
              exit;
          End;
          begin
            vData := gData_Tab(vRow)(vCol);
          exception
            when others then
              vData := null;
          end;
          --
          --Validate Field
          --
          If not validateColumn(vBind,vData,vRow) then
            -- Failed Validation
            vRowRejected := TRUE;
            vRejected := vRejected + 1;
            exit;
          end if;

          -- 02.06.07
          -- Remove any carridge returns or new line feed
          vData := REPLACE(REPLACE(vData, chr(13), ''), chr(10), '');

          -- bind in the values to be inserted
          If gColumn_tab(vBind).data_type = 'DATE' then
            dbms_sql.bind_variable(dest_cur, ':'||vBind, to_date(vData,gDate_Format));
          else
            If gColumn_tab(vBind).trim_data = 'Y' then
              dbms_sql.bind_variable(dest_cur, ':'||vBind, trim(vData));
            else
              dbms_sql.bind_variable(dest_cur, ':'||vBind, vData);
            end if;
          end if;

        End loop;

        -- Insert
        if vErrorCode = 0 and not vRowRejected then
          begin
            ignore := dbms_sql.execute(dest_cur);
            vInserted := vInserted + 1;
          exception
            when others then
              writeOutput(' Error Inserting Record <'||vRow|| '>. '||substr(SQLERRM,1,200));
              vRejected := vRejected + 1;
          end;
        elsif vErrorCode <> 0 then
          exit;
        end if;
    END LOOP;

    -- Commit and close all cursors
    if vRejected <> 0 then
      ROLLBACK;
      gWarning := TRUE;
    else
      COMMIT;
    end if;
  end if;

  -- Close Cursor
  if dbms_sql.is_open(dest_cur) then
    dbms_sql.close_cursor(dest_cur);
  end if;
  writeOutput('Accepted <'||vInserted||'>. Rejected <'||vRejected||'>.');
  if gWarning then
    writeOutput('File rejected. 0 Records Inserted. Please correct file and re-process.');
  else
    writeOutput('File accepted. '||vInserted||' Records inserted successfully.');
  end if;

  return(vErrorCode);
EXCEPTION
  WHEN OTHERS THEN
    IF dbms_sql.is_open(dest_cur) THEN
      dbms_sql.close_cursor(dest_cur);
    END IF;
    RAISE;
END insertDynamic;

-----------------------------------------------------------------------
-- getNumberOfRows
-----------------------------------------------------------------------
FUNCTION getNumberOfRows(cFileId in Number) RETURN NUMBER IS

vTmpVal NUMBER;

BEGIN

--  SELECT Count(*)
--    INTO vTmpVal
--    FROM TABLE(xmlsequence(extract(vXML, '//ROWSET/ROW'))) d;

  select count(*)
  into   vTmpVal 
  from   xxcp_data_load
         ,table(xmlsequence(extract(xxcp_data_load.data,'//ROWSET/ROW'))) d
  where  file_id = cFileId;

  If vTmpVal > 1 then
    vTmpVal := vTmpVal -1;
  End If;

  RETURN vTmpVal;

END getNumberOfRows;



----------------------------------------------------------------
--
-- isValidFile
--
----------------------------------------------------------------
FUNCTION isValidFile(cFileId IN number) RETURN NUMBER IS

vClob        CLOB;
vRetVal      NUMBER;
eInvalidFile EXCEPTION;

BEGIN

  DBMS_LOB.createtemporary (vClob, TRUE);

  vClob := getExcelXML(cFileId);

  IF dbms_lob.getlength(vClob) > 0 THEN
    vRetVal := cFileId;
  ELSE
    RAISE eInvalidFile;
  END IF;

  RETURN vRetVal;

EXCEPTION WHEN eInvalidFile THEN

  DELETE FROM fnd_lobs
  WHERE file_id = cFileId;

  COMMIT;

  RETURN -1;

END isValidFile;


----------------------------------------------------------------------------------
-- This procedure gets and EXCEL XML file loaded by XXCPMLFLUPL and transforms
-- it into ?
-----------------------------------------------------------------------------------

PROCEDURE parseExcelXML(cFileId             IN NUMBER,
                        cSourceID           IN NUMBER,
                        cSourceAssignmentID IN NUMBER,
                        cMapping_Set_Id     IN NUMBER default null) IS

-- Define the local variables
vXmlData    XMLType;
vXslData    XMLType;
vOutData    XMLType;

vOutputClob   CLOB;
vNumberofRows NUMBER;

BEGIN

  -- Get the XML document using the getXML() function defined in the database.
  -- Since XMLType.transform() method takes XML data as XMLType instance,
  -- use the XMLType.createXML method to convert the XML content received
  -- as CLOB into an XMLType instance.
  vXmlData := XMLType.createXML(getExcelXML(cFileId));

  -- Get the XSL Stylesheet using the getXSL() function defined in the database.
  -- Since XMLType.transform() method takes an XSL stylesheet as XMLType instance,
  -- use the XMLType.createXML method to convert the XSL content received as CLOB
  -- into an XMLType instance.
  vXslData := XMLType.createXML(getExcelXSL(100));

  -- Use the XMLtype.transform() function to get the transformed XML instance.
  -- This function applies the stylesheet to the XML document and returns a transformed
  -- XML instance.


  --vOutData := vXMLData.transform(vXslData);

  -- New Transform (transform each row instead of whole document)

  insert into xxcp_data_load_tmp (xml,xsl) values(vXMLData,vXslData);

  select XMLELEMENT("ROWSET",
                             XMLAGG(d.transform(xxcp_data_load_tmp.xsl)
                                    )
                   )
  into   vOutData
  from   xxcp_data_load_tmp,
         table(xmlsequence(extract(xxcp_data_load_tmp.xml,'//Workbook/Worksheet/Table/Row','xmlns="urn:schemas-microsoft-com:office:spreadsheet"'))) d;


  -- Insert the transformed XML into the XXCPML_ITEMS_LOAD.
  INSERT INTO xxcp_data_load_v (file_id
                              ,Source_id
                              ,Source_Assignment_id
                              ,data
                              ,status
                              ,no_updated
                              ,no_inserted
                              ,no_errored
                              ,no_records
                              ,mapping_set_id
                              ,created_by
                              ,creation_date
                              ,last_updated_by
                              ,last_update_date
                              ,last_update_login)
                          VALUES
                             (cfileId
                             ,cSourceId
                             ,cSourceAssignmentId
                             ,vOutData
                             ,'NEW'
                             ,0
                             ,0
                             ,0
                             ,0                 --vNumberofRows
                             ,cMapping_set_id
                             ,fnd_global.user_id
                             ,SYSDATE
                             ,fnd_global.user_id
                             ,SYSDATE
                             ,fnd_global.login_id);

  -- Returns the number of rows that are in the XLS file.
  vNumberofRows := getNumberOfRows(cFileId => cFileId);

  Update xxcp_data_load
  Set    no_records = vNumberofRows
  Where  file_id = cFileId;  


  COMMIT;

END parseExcelXML;

-- UploadCompleteMessage
--
--    Displays file upload compelte message.
--
PROCEDURE UploadCompleteMessage(cFile                IN VARCHAR2
                               ,cAccessId            IN NUMBER
                               ,cSourceId           IN NUMBER
                               ,cSourceAssignmentID IN NUMBER
                               ) IS
  l_file_id  NUMBER;
  fn        VARCHAR2(256);

BEGIN
IF (Icx_Sec.ValidateSession) THEN
  l_file_id := fnd_gfm.confirm_upload(access_id    => cAccessId
                                     ,file_name     => cFile
                                     ,program_name => 'XXCPEXFLUPL');

  fn := SUBSTR(cFile, INSTR(cFile,'/')+1);
    -- bug 3045375, changed <> to > so <0 l_file_id goes to else.
  IF (isValidFile(l_file_id) > -1) THEN -- File upload completed

    xxcp_file_upload.parseExcelXML(cFileId => l_file_id, cSourceID => cSourceId, cSourceAssignmentID => cSourceAssignmentID);

    htp.htmlOpen;
    htp.headOpen;

  htp.title('File Upload');
  htp.headClose;

    htp.headClose;
    htp.bodyOpen;
    htp.img2('/OA_MEDIA/FNDLOGOS.gif',calign => 'Left',calt => 'Logo');
    htp.br;
    htp.br;
    htp.p('<h4>File Uploaded'||'</h4>');

    htp.hr;
    htp.p (
       htf.bold(Fnd_Message.get_string('FND','FILE-UPLOAD COMPLETED MESSAGE')));
    htp.br;
    htp.p ('<h4>'||'** Press REFRESH on the form to see the loaded file **'||'</h4>');

    htp.p ('<h4>'||Fnd_Message.get_string('FND','FILE-UPLOAD CLOSE WEB BROWSER')||'</h4>');
    htp.br;
    htp.bodyClose;
    htp.htmlClose;

  ELSE -- File upload failed.
    htp.htmlOpen;
    htp.headOpen;
    htp.title(Fnd_Message.get_string('FND','FILE-UPLOAD PAGE TITLE'));
    htp.headClose;
    htp.bodyOpen;
    htp.img2('/OA_MEDIA/FNDLOGOS.gif',calign => 'Left',calt => 'Logo');
    htp.br;
    htp.br;
    htp.p('<h4>'||'ERROR'||'</h4>');
    htp.p('<h4>'||'* Ensure that you do not have the file open in another application and the file is saved in XML format.'||'</h4>');
    htp.p('<h4>'||'* File Format: '||'</h4>');
    htp.p('<h4>'||'  Line 1: Mapping Name, Line 2: Titles, Line 3+: Data'||'</h4>');
    htp.p('<h4>'||'* Max: One Hundred Columns'||'</h4>');
    htp.hr;
    --
    --
    --
    IF xxcp_file_upload.isValidFile(l_file_id) <> -2 THEN
       htp.p (
         htf.bold(Fnd_Message.get_string('FND','FILE-UPLOAD FAILED')));
    ELSE
       htp.p (
         htf.bold(fn||Fnd_Message.get_string('FND','FILE-UPLOAD INVALID FILE NAME')));
    END IF;
    htp.br;
    htp.bodyClose;
    htp.htmlClose;
  END IF;
END IF;

EXCEPTION WHEN OTHERS THEN

  errorPage(SQLERRM);

END UploadCompleteMessage;


-- CancelProcess
--    Displays the file upload cancel message page.
--  IN
--  file_id  - Unique process to autenticate the file upload process.
--

PROCEDURE CancelProcess AS

BEGIN

IF (Icx_Sec.ValidateSession) THEN

  -- Show a message page
  htp.htmlOpen;
  htp.headOpen;
  htp.title(Fnd_Message.get_string('FND','FILE-UPLOAD PAGE TITLE'));
  htp.headClose;
  htp.bodyOpen;
  htp.img2('/OA_MEDIA/FNDLOGOS.gif',calign => 'Left',calt => 'Logo');
  htp.br;
  htp.br;
  htp.p('<h4>'||Fnd_Message.get_string('FND','FILE-UPLOAD PAGE HEADING')
                                                                ||'</h4>');
  htp.hr;
  htp.p (htf.bold(Fnd_Message.get_string('FND','FILE-UPLOAD CANCEL MESSAGE')));
  htp.br;
  htp.p ('<h4>'||Fnd_Message.get_string('FND','FILE-UPLOAD CLOSE WEB BROWSER')
                                                                ||'</h4>');
  htp.p ('<h4>'|| Fnd_Message.get_string('FND','FILE-UPLOAD REJECT UPLOAD')
                                                                ||'</h4>');
  htp.br;
  htp.br;
  htp.br;
  htp.bodyClose;
  htp.htmlClose;

END IF;

END CancelProcess;



--
-- DisplayGFMForm
--   access_id -  Access id for file in fnd_lobs
--   l_server_url - Obsolete, not used anymore
--   cSource_Assignment_id - Source Assignment in VT
--
PROCEDURE DisplayLOADForm(access_id             IN NUMBER
                         ,l_server_url          IN VARCHAR2
                         ,cSource_Assignment_id IN NUMBER)
                         IS

  l_cancel_url  VARCHAR2(255);
  l_start_pos   NUMBER := 1;
  l_length      NUMBER := 0;
  upload_action VARCHAR2(2000);
  l_language    VARCHAR2(80);

  Cursor C1(pSource_Assignment_id in number) is
  select source_id
    from xxcp_source_assignments
   where source_assignment_id = pSource_Assignment_id;

  vSourceID NUMBER;

BEGIN

 --Set Source Assignment
 For Rec in C1(cSource_Assignment_id) Loop
  vSourceID := Rec.Source_id;
 End Loop;

 IF (Icx_Sec.ValidateSession) THEN

  -- Set the upload action
  upload_action := fnd_gfm.construct_upload_url(Fnd_Web_Config.gfm_agent,
                                'xxcp_file_upload.uploadcompletemessage',
                                access_id);

  -- Set page title and toolbar.
  htp.htmlOpen;
  htp.headOpen;
  htp.p( '<SCRIPT LANGUAGE="JavaScript">');
  htp.p( ' function processclick (cancel_url) {
                 IF (confirm('||'"'||
     Fnd_Message.get_string ('FND','FILE-UPLOAD CONFIRM CANCEL')
     ||'"'||'))
                 {
                        parent.location=cancel_url
                 }
              }');
  htp.print(  '</SCRIPT>' );
  htp.title(Fnd_Message.get_string('FND','FILE-UPLOAD PAGE TITLE'));  htp.headClose;
  htp.bodyOpen;
  htp.img2('/OA_MEDIA/FNDLOGOS.gif',calign => 'Left',calt => 'Logo');
  htp.br;
  htp.br;
  htp.p('<h4>Upload Excel File'||'</h4>');
  htp.hr;
  htp.br;
  htp.print( '</LEFT>' );

  htp.formOpen( curl => upload_action, cmethod => 'POST',
    cenctype=> 'multipart/form-data');
  htp.tableOpen(  cattributes => ' border=0 cellpadding=2 cellspacing=0' );

  htp.tableRowOpen( cvalign => 'TOP' );
  htp.tableData( '<label> '||Fnd_Message.get_string('FND','ATCHMT-FILE-PROMPT')||' </label>');

  htp.tableData( '<INPUT TYPE="File" NAME="cFile" SIZE="60" ACCEPT="text/xml">',
                                                        calign => 'left');
  htp.tableRowClose;

  -- Get ML Languages.
  htp.tableClose;

  -- Send access is as a hidden value
  htp.formHidden ( cname =>'cAccessId', cvalue=> TO_CHAR(access_id) );
  htp.formHidden ( cname =>'cSourceID', cvalue=> TO_CHAR(vSourceID) );
  htp.formHidden ( cname =>'cSourceAssignmentID', cvalue=> TO_CHAR(cSource_Assignment_id) );
  l_cancel_url := RTRIM(Fnd_Web_Config.plsql_agent, '/') ||
                  '/xxcp_ml_file_upload.cancelprocess';

  htp.br;
  htp.tableOpen(  cattributes => ' border=0 cellpadding=2 cellspacing=0' );
  htp.tableRowOpen( cvalign => 'TOP' );
  htp.tableData( '<INPUT TYPE="Submit" VALUE="' ||
    Fnd_Message.get_string('FND','OK')||
    '" SIZE="50">', calign => 'left');
  htp.tableData( '<INPUT TYPE="Button" NAME="cancel" VALUE="'||
         Fnd_Message.get_string('FND','FILE-UPLOAD CANCEL BUTTON TEXT')||
         '"' || '" onClick="javascript:window.close();" SIZE="50"> ' , calign => 'left');
  htp.tableRowClose;
  htp.tableClose;
  htp.formClose;

  htp.bodyClose;
  htp.htmlClose;
 END IF;
END DisplayLOADForm;

-----------------------------------------------------------------------
-- loadFile
-----------------------------------------------------------------------
Function loadFile(cSource_id IN NUMBER   default null,
                  cDebug     IN VARCHAR2 default null) return number is
  -- 02.06.03
  cursor c1 is select directory_path path
               from all_directories
               where directory_name = 'XXCPLOAD';

  cursor c2 (pMapping_Short_Code in varchar2) is
    select f.attribute1 delimiter,
           f.attribute2 date_format,
           f.set_id file_type_id,
           f.attribute6 record_type_start,
           f.attribute7 - f.attribute6 + 1 record_type_length,
           m.attribute1 dest_table,
           case when f.attribute1 = 'FIX' then 'FIXED' else 'CSV' end file_format
    from   xxcp_table_set_ctl m,
           xxcp_table_set_ctl f
    where  m.set_base_table  = 'CP_DATA_MAPPING'
    and    m.attribute3      = pMapping_Short_Code
    and    f.set_base_table  = 'CP_FILE_TYPE_HEADINGS'
    and    to_char(f.set_id) = m.attribute4
    and    m.enabled_flag = 'Y';

  cursor c3 (pFile_Name in varchar2) is
    select upload_date
    from   xxcp_file_upload_history
    where  file_name = pFile_Name
    and    loaded = 'Y'
    order by upload_date desc;

  cursor c4 (pFile_type_id in number, pRecord_type in varchar2) is
    select heading_name,
           start_position,
           end_position - start_position + 1 field_length
    from   xxcp_file_type_headings
    where  file_type_id = pFile_type_id
      and  nvl(record_type,'ALL') = nvl(pRecord_type,'ALL')
    order by start_position;

  vPath               varchar2(1000);  -- Get From System Profile
  vCurrentFileName    varchar2(500);
  vRenamedFile        varchar2(500);
  vFileHandle         utl_file.file_type;
  vSep                varchar2(1) := ',';
  vSep_Temp           varchar2(10);
  vMapping_Short_Code varchar2(5);
  vSA_Short_Code      varchar2(3);
  vReturn             number := 0;
  vErrorCode          number := 0;
  vDate_Format        varchar2(30);
  vRunWarning         boolean := FALSE;
  vFile_Upload_Id     number := 0;
  vRecord_type_start  number;
  vRecord_type_length number;
  vDest_table_name    varchar2(30);
  vFile_format        varchar2(10);

  vFile_type_id       number;
  y number;

  ex_nodirlist exception;
  ex_nomapping exception;
  ex_nopath    exception;

Begin

  gFileName_tab.delete;
  gHeading_tab.delete;
  gHeading_tab_char.delete;
  gBind_tab.delete;

  If cDebug = 'Y' then
    gDebug_On := TRUE;
  end if;

  -- Get Directory Path
  open c1;
  fetch c1 into vPath;
  close c1;

  if nvl(vPath,'NULL') = 'NULL' then
    raise ex_nopath;
  end if;

  writeOutput('Path <'||vPath||'>');

  --
  -- Get Directory List
  --
  if not getDirectoryList(vPath,cSource_id) then
    -- Error Retrieving Directory Listing
    raise ex_nodirlist;
  end if;

  writeOutput('Found <'||gFileName_tab.count ||'> file(s) to process...');

  For i in 1..gFileName_tab.count loop
    vCurrentFileName := gFileName_tab(i);
    gHeading_tab.delete;
    gHeading_tab_char.delete;
    gBind_tab.delete;
    gData_Row.delete;
    gData_Tab.delete;
    gColumn_tab.delete;
    gWarning      := FALSE;
    gFileRejected := FALSE;
    vErrorCode := 0;
    vFile_Upload_Id := 0;

    y := 0;
    gFixed_tab.delete;
    gFixed_rec_tab.delete;

    writeOutput(' ');
    writeOutput('Processing Filename <'||vCurrentFileName||'>');
    -- Get Mapping from Filename (First 5 Chars)
    vMapping_Short_Code := substr(vCurrentFileName,1,5);

    -- Get Source Assignment Id (3 chars from)
    vSA_Short_Code := substr(vCurrentFileName,7,3);

    -- Check if File has already been loaded
    for c3_rec in c3(vCurrentFileName) loop
      -- File Exists. Reject
      WriteOutput('File Rejected. File already processed <'||to_char(c3_rec.upload_date,'DD-MON-YYYY HH24:MI:SS')||'>');
      vErrorCode := -4;
      vRunWarning := TRUE;
    end loop;

    if vErrorCode = 0 then
      -- Make File Upload Entry
      insert into xxcp_file_upload_history(FILE_UPLOAD_ID,
                                         FILE_NAME,
                                         FILE_PATH,
                                         UPLOAD_DATE,
                                         LOADED,
                                         CREATED_BY,
                                         CREATION_DATE)
                                 values (xxcp_file_upload_id_seq.nextval,
                                         vCurrentFileName,
                                         vPath,
                                         sysdate,
                                         'N',
                                         -1,
                                         sysdate) returning file_upload_id into vFile_Upload_Id;


      if vMapping_Short_Code is null then
        raise ex_nomapping;
      else
        -- Get Delimiter
        open  c2(vMapping_Short_Code);
        fetch c2 into vSep_Temp,vDate_Format, vFile_type_id, vRecord_type_start, vRecord_type_length, vDest_table_name, vFile_format;

        -- if a mapping is found
        if c2%found then
          if vDate_Format is not null then
            gDate_Format := vDate_format;
          end if;

          -- Open File
          vFileHandle := utl_file.fopen('XXCPLOAD', vCurrentFileName, 'R');

          if vFile_format = 'CSV' then

            if vSep_Temp = 'COM' then
              vSep := ',';
            elsif vSep_Temp = 'TAB' then
              vSep := chr(9);
            elsif vSep_Temp = 'SPA' then
              vSep := chr(32);
            elsif length(vSep_Temp) = 1 then
              vSep := vSep_Temp;
            else
              vSep := ',';
            end if;

            vErrorCode := getCSVHeadings(vFileHandle, vSep);
          else

            -- get file type headings
            for z in (select attribute1 record_type
                      from XXCP_Table_Set_Ctl r
                      where r.set_base_table = 'CP_FILE_TYPE_RECORDS'
                      and   r.attribute_set_id = vFile_type_id
                      -- Get headings for file types that have no record_type
                      union
                      select null
                      from dual) loop

              gFixed_tab.delete;

              for x in c4(vFile_type_id,z.record_type) loop

                y := gFixed_tab.count+1;

                gFixed_tab(y).col := x.heading_name;
                gFixed_tab(y).pos := x.start_position;
                gFixed_tab(y).len := x.field_length;

              end loop;

              -- If the file type is not null then populate headings
              if y > 0 and z.record_type is not null then
                gFixed_rec_tab(z.record_type) := gFixed_tab;
              end if;

            end loop;

            -- if record types are being used always get the "All Records" headings.
            vErrorCode := getFixedHeadings(vFileHandle);
          end if;

          -- Get Data
          if vErrorCode = 0 then
            if vFile_format = 'CSV' then
              vErrorCode := getCSVData(vFileHandle, vSep);
            else
              vErrorCode := getFixedData(vFileHandle, vRecord_type_start, vRecord_type_length);
            end if;
          end if;

          --
          -- Insert
          --
          if vErrorCode = 0 then
              vErrorCode := insertDynamic(vMapping_Short_Code,
                                          vSA_Short_Code,
                                          vFile_type_id);
          end if;

          -- Close File
          utl_file.fclose(vFileHandle);

          if not gWarning and vErrorCode = 0 then
            -- Rename and Move Files
            vRenamedFile := replace(vCurrentFileName,'.','_'||to_char(sysdate,'YYYYMMDDHH24MI')||'.');

            -- 03.06.13
            -- if the import archive directory is set then
            if gArchive_import = 'Y' then

              vReturn := xxcp_file_api.renameTo(vPath||'/'||vCurrentFileName
                                               ,gArchive_import_dir||'/'||vRenamedFile);

              if vReturn = 0 then
                writeOutput('Unable to rename and move file <'||vCurrentFileName||'> to <'||gArchive_import_dir||'>');
              elsif vReturn = 1 then
                writeOutput('Moved file <'||vCurrentFileName||'>.');
              end if;
            -- original code to move it to old directory
            else
              vReturn := xxcp_file_api.renameTo(vPath||'/'||vCurrentFileName
                                               ,vPath||'/old/'||vRenamedFile);

              if vReturn = 0 then
                writeOutput('Unable to rename and move file <'||vCurrentFileName||'> to <'||vPath||'/old>');
              elsif vReturn = 1 then
                writeOutput('Moved file <'||vCurrentFileName||'>.');
              end if;
            end if;

            -- Mark File Upload Entry as Loaded
            Update xxcp_file_upload_history
            set    loaded = 'Y'
            where  file_upload_id =  vFile_Upload_Id;

          else
            vRunWarning := TRUE;
          end if;

        else
          writeOutput('Mapping not found for file <'||vCurrentFileName||'>');
          vRunWarning := TRUE;
        end if;

        close c2;
      end if;
    end if;
  end loop;

  if gFileName_tab.count > 0 then
    writeOutput(' ');
    writeOutput('Processed '||gFileName_tab.count||' file(s).');
  end if;

  If vRunWarning then -- File(s) rejected
    vErrorCode := -3;
  end if;

  commit;

  return(vErrorCode);
Exception
  when ex_nodirlist then
    raise_application_error(-20001, 'Error retrieving directory listing.');
  when ex_nomapping then
    raise_application_error(-20002, 'Mapping not found for file <'||vCurrentFileName||'>');
  when ex_nopath then
    raise_application_error(-20003, 'Path not found. Please check the XXCPLOAD directory.');

end loadFile;

-- **************************
-- loadCSV
-- -------
-- Main entry point.
-- **************************
Function loadCSV(cSource_id IN NUMBER   default null,
                 cDebug     IN VARCHAR2 default null) return number IS
begin

  return loadFile(cSource_id => cSource_id,
                  cDebug     => cDebug);

End;

-----------------------------------------------------------------------
-- exportFile
-----------------------------------------------------------------------
Function exportFile(cSource_id  IN NUMBER default null,
                    cDebug      IN VARCHAR2 default null,
                    cRequest_id IN NUMBER default null) return number IS

  -- 02.06.03
  cursor c1 is
    select directory_path path
      from all_directories
     where directory_name = 'XXCPEXPORT';

  cursor c2(pSource_id in number, pRequest_id in number) is
    select distinct f.delimiter,
                    f.date_format,
                    f.file_type_id,
                    f.file_type_name,
                    f.overwrite,
                    f.load_headings,
                    f.file_extension,
                    f.id_in_filename,
                    f.instance_name,
                    f.instance_id,
                    -- 02.06.06
                    st.vt_qualifier1,
                    st.vt_qualifier2,
                    st.vt_qualifier3,
                    st.vt_qualifier4
      from (select f.delimiter,
                   f.date_format,
                   f.file_type_id,
                   f.file_type_name,
                   f.overwrite,
                   f.load_headings,
                   f.file_extension,
                   f.id_in_filename,
                   db.instance_name,
                   db.instance_id,
                   ov.target_instance_id
              from xxcp_ff_file_types_v     f,
                   xxcp_sys_target_tables     ta,
                   XXCP_Sys_Target_Tables_Ovr ov,
                   xxcp_instance_db_links     db
             where ta.transaction_table = f.view_name
               and ov.target_table_id = ta.target_table_id
               and ov.source_id = ta.source_id
--               and ov.target_instance_id = db.instance_id
               and f.table_type = 'OUTPUT'
               and ta.source_id = pSource_id
               and ov.staged_table = 'Y'
               and db.active = 'Y'
                  -- if cRequest_id is not null then make sure that online table is set to Y,
                  -- so that the job is run on the back to the engine run.
               and ov.online_table =
                   decode(cRequest_id, null, ov.online_table, 'Y')) f,
           xxcp_staged_flat_file st
    -- Outer Join to staged flat file to pickup any populated qualifiers
     where st.vt_instance_id(+) = f.instance_id
       and st.vt_file_type_id(+) = f.file_type_id
       and st.vt_file_export_id(+) is null
       and st.vt_source_assignment_id = any(select source_assignment_id 
                                              from xxcp_source_assignments w 
                                              where w.source_id = pSource_id);

  cursor c3(pFile_type_id in number,
            pInstance_id  in number,
            pSep          in varchar2,
            pNum_of_recs  in number,
            -- 02.06.06
            pQualifier1   in varchar2,
            pQualifier2   in varchar2,
            pQualifier3   in varchar2,
            pQualifier4   in varchar2) is
    select column1 || case when pNum_of_recs >= 2 then pSep end || column2 ||
           case when pNum_of_recs >= 3 then pSep end || column3 ||
           case when pNum_of_recs >= 4 then pSep end || column4 ||
           case when pNum_of_recs >= 5 then pSep end || column5 ||
           case when pNum_of_recs >= 6 then pSep end || column6 ||
           case when pNum_of_recs >= 7 then pSep end || column7 ||
           case when pNum_of_recs >= 8 then pSep end || column8 ||
           case when pNum_of_recs >= 9 then pSep end || column9 ||
           case when pNum_of_recs >= 10 then pSep end || column10 ||
           case when pNum_of_recs >= 11 then pSep end || column11 ||
           case when pNum_of_recs >= 12 then pSep end || column12 ||
           case when pNum_of_recs >= 13 then pSep end || column13 ||
           case when pNum_of_recs >= 14 then pSep end || column14 ||
           case when pNum_of_recs >= 15 then pSep end || column15 ||
           case when pNum_of_recs >= 16 then pSep end || column16 ||
           case when pNum_of_recs >= 17 then pSep end || column17 ||
           case when pNum_of_recs >= 18 then pSep end || column18 ||
           case when pNum_of_recs >= 19 then pSep end || column19 ||
           case when pNum_of_recs >= 20 then pSep end || column20 ||
           case when pNum_of_recs >= 21 then pSep end || column21 ||
           case when pNum_of_recs >= 22 then pSep end || column22 ||
           case when pNum_of_recs >= 23 then pSep end || column23 ||
           case when pNum_of_recs >= 24 then pSep end || column24 ||
           case when pNum_of_recs >= 25 then pSep end || column25 ||
           case when pNum_of_recs >= 26 then pSep end || column26 ||
           case when pNum_of_recs >= 27 then pSep end || column27 ||
           case when pNum_of_recs >= 28 then pSep end || column28 ||
           case when pNum_of_recs >= 29 then pSep end || column29 ||
           case when pNum_of_recs >= 30 then pSep end || column30 ||
           case when pNum_of_recs >= 31 then pSep end || column31 ||
           case when pNum_of_recs >= 32 then pSep end || column32 ||
           case when pNum_of_recs >= 33 then pSep end || column33 ||
           case when pNum_of_recs >= 34 then pSep end || column34 ||
           case when pNum_of_recs >= 35 then pSep end || column35 ||
           case when pNum_of_recs >= 36 then pSep end || column36 ||
           case when pNum_of_recs >= 37 then pSep end || column37 ||
           case when pNum_of_recs >= 38 then pSep end || column38 ||
           case when pNum_of_recs >= 39 then pSep end || column39 ||
           case when pNum_of_recs >= 40 then pSep end || column40 ||
           case when pNum_of_recs >= 41 then pSep end || column41 ||
           case when pNum_of_recs >= 42 then pSep end || column42 ||
           case when pNum_of_recs >= 43 then pSep end || column43 ||
           case when pNum_of_recs >= 44 then pSep end || column44 ||
           case when pNum_of_recs >= 45 then pSep end || column45 ||
           case when pNum_of_recs >= 46 then pSep end || column46 ||
           case when pNum_of_recs >= 47 then pSep end || column47 ||
           case when pNum_of_recs >= 48 then pSep end || column48 ||
           case when pNum_of_recs >= 49 then pSep end || column49 ||
           case when pNum_of_recs = 50 then pSep end || column50 file_line,
           vt_filename_infix
      from xxcp_staged_flat_file
     where vt_file_type_id = pFile_type_id
       and vt_instance_id = pInstance_id
       and vt_request_id = nvl(cRequest_id, vt_request_id)
       -- 02.06.06
       and nvl(vt_qualifier1,'~NULL~') = nvl(pQualifier1, '~NULL~')
       and nvl(vt_qualifier2,'~NULL~') = nvl(pQualifier2, '~NULL~')
       and nvl(vt_qualifier3,'~NULL~') = nvl(pQualifier3, '~NULL~')
       and nvl(vt_qualifier4,'~NULL~') = nvl(pQualifier4, '~NULL~')
       and vt_file_export_id is null
     order by vt_order_sequence1,
              vt_order_sequence2;


  cursor c4(pFile_type_id in number) is
    select heading_name
      from xxcp_file_type_headings
     where file_type_id = pFile_type_id
     order by display_seq;

  vPath               varchar2(1000);
  vCurrentFileName    varchar2(500);
  vRenamedFile        varchar2(500);
  vFileHandle         utl_file.file_type;
  vSep                varchar2(1) := ',';
  vSep_Temp           varchar2(10);
  vMapping_Short_Code varchar2(5);
  vFilename_infix     varchar2(10);
  vReturn             number := 0;
  vErrorCode          number := 0;
  vDate_Format        varchar2(30);
  vRunWarning         boolean := FALSE;
  vFile_Upload_Id     number := 0;
  vFile_type_name     varchar2(30);
  vId_in_filename     varchar2(1);

  -- defaults
  vLoadHeadings  varchar2(1) := 'Y';
  vOverwrite     varchar2(1) := 'Y';
  vExtension     varchar2(50) := 'CSV';
  vInstance_name varchar2(50);
  vInstance_id   number;
  vQualifier1    varchar2(30);
  vQualifier2    varchar2(30);
  vQualifier3    varchar2(30);
  vQualifier4    varchar2(30);
  vRequest_id    number;

  vHeadings       varchar2(4000);
  vFile_type_id   number;
  vFile_line      varchar2(32676);
  vFile_export_id number;
  y               number;
  vFiles_processed number := 0;
  vNum_of_headings number := 0;

  ex_file_exists exception;
  ex_nopath exception;
  ex_rename exception;

  vJob_Status number := 0;


Begin

  -- 02.06.09 if called outside the engine run check if custom events are enabled.
  if xxcp_global.gCommon(1).Custom_events is null then

    select s.custom_events
      into xxcp_global.gCommon(1).Custom_events
     from  xxcp_sys_sources s
     where source_id = cSource_id;
  end if;

  -- check if debug is on
  If cDebug = 'Y' then
    gDebug_On := TRUE;
  end if;

  writeOutput('Flat File Export <'||to_char(sysdate,'DD-MON-YYYY HH24:MI:SS')||'>');

  -- Get Directory Path
  open c1;
  fetch c1
    into vPath;
  close c1;

  if nvl(vPath, 'NULL') = 'NULL' then
    raise ex_nopath;
  end if;

  writeOutput('Path <' || vPath || '>');

  -- get the file details
  open c2(cSource_id, cRequest_id);

  loop
    fetch c2
      into vSep_temp,
           vDate_Format,
           vFile_type_id,
           vFile_type_name,
           vOverwrite,
           vLoadHeadings,
           vExtension,
           vId_in_filename,
           vInstance_name,
           vInstance_id,
           -- 02.06.06
           vQualifier1,
           vQualifier2,
           vQualifier3,
           vQualifier4;

    exit when c2%notfound;

    -- Set the seperator for the file
    if vSep_Temp = 'COM' then
      vSep := ',';
    elsif vSep_Temp = 'TAB' then
      vSep := chr(9);
    elsif vSep_Temp = 'SPA' then
      vSep := chr(32);
    elsif vSep_Temp = 'FIX' then
      vSep := null;
    elsif length(vSep_Temp) = 1 then
      vSep := vSep_Temp;
    else
      vSep := ',';
    end if;

    -- reset the headings variable
    vHeadings := null;

    -- work out the number of columns in the file type.
    for x in c4(vFile_type_id) loop
      vHeadings := vHeadings || case
                     when x.heading_name is not null then
                      vSep
                   end || x.heading_name;
      -- count the number of headings
      vNum_of_headings := vNum_of_headings + 1;
    end loop;

    -- 02.06.09
    IF xxcp_global.gCommon(1).Custom_events = 'Y' THEN
      xxcp_custom_events.before_file_export(cSource_id       => cSource_id,
                                            cRequest_id      => cRequest_id,
                                            cFile_type_id    => vFile_type_id,
                                            cInstance_id     => vInstance_id,
                                            cQualifier1      => vQualifier1,
                                            cQualifier2      => vQualifier2,
                                            cQualifier3      => vQualifier3,
                                            cQualifier4      => vQualifier4,
                                            cJob_Status      => vJob_Status);
    END IF;

    if nvl(vJob_Status,0) = 0 then

    -- see if there is any data for the file
    open c3(vFile_type_id,
            vInstance_id,
            vSep,
              vNum_of_headings,
            vQualifier1,
            vQualifier2,
            vQualifier3,
            vQualifier4);

    fetch c3
        into vFile_line, vFilename_infix;

    -- if so check to see if the file_name exists
    if c3%found then

      -- get the file export id
      select xxcp_file_export_seq.nextval
      into vFile_export_id
      from dual;

      -- Set the filename
        vCurrentFileName := upper(vFile_type_name) || '_' ||
                            -- 02.06.09
                            case
                              when vFilename_infix is not null then
                                upper(vFilename_infix) || '_'
                            end ||
                          upper(vInstance_name) ||
                         -- if the flag is set then inlcude the file_export_id in the file_name
                          case
                            when vId_in_filename = 'Y' then
                             '_'||vFile_export_id
                            else
                             null
                          end || '.' || upper(vExtension);

      writeOutput(' ');
      writeOutput('Processing Filename <'||vCurrentFileName||'>');

      -- check to see if file already exists
      if XXCP_File_API.exists(vPath || '/' || vCurrentFileName) = 1 then

        -- If so the override then rename file
        if vOverwrite = 'Y' then

          vRenamedFile := replace(vCurrentFileName,
                                  '.',
                                  '_' || to_char(sysdate, 'YYYYMMDDHH24MI') || '.');

          if XXCP_File_API.exists(vPath || '/' || vRenamedFile) = 0 then

            vReturn := xxcp_file_api.renameTo(vPath || '/' ||
                                              vCurrentFileName,
                                              vPath || '/' || vRenamedFile);

            if vReturn = 0 then
              raise ex_rename;
            end if;
          else
            raise ex_rename;
          end if;

        else
          -- otherwise raise error
          raise dup_val_on_index;
        end if;
      end if;

      -- open the file for writing
      vFileHandle := utl_file.fopen('XXCPEXPORT', vCurrentFileName, 'w');

      -- write the headings
      if vLoadHeadings = 'Y' then
        utl_file.put_line(vFileHandle, vHeadings);
      end if;

      -- write the first line
      begin
        utl_file.put_line(vFileHandle, vFile_line);
      end;

      -- record that the file is being written to, so that we can delete the
      -- file if needed.
      gFileName_tab(gFileName_tab.count+1) := vCurrentFileName;

      -- write the rest of the file
      loop
        fetch c3
            into vFile_line, vFilename_infix;

        exit when c3%notfound;

        -- extract the data from the table.
        begin
          utl_file.put_line(vFileHandle, vFile_line);
        end;
      end loop;

      utl_file.fclose(vFileHandle);

      -- 02.06.08
      if gArchive_export = 'Y' then

        if gArchive_export_dir is not null then

          -- Move the exported file to archive or old directory
          vReturn := xxcp_file_api.copy(vPath||'/'||vCurrentFileName
                                       ,gArchive_export_dir||'/'||vCurrentFileName);

          if vReturn = 0 then
            writeOutput('Unable to move file <'||vCurrentFileName||'> to <'||gArchive_export_dir||'>');
          elsif vReturn = 1 then
            writeOutput('Moved file <'||vCurrentFileName||'> to <'||gArchive_export_dir||'>');
          end if;
        else
          writeOutput('Warning: FILE EXPORT ARCHIVE DIRECTORY System Profile not defined.');
        end if;
      end if;

      -- 02.06.08
      -- record the details of the exported file.
     insert into xxcp_file_export_history
       (SOURCE_ID,
        FILE_EXPORT_ID,
        FILE_NAME,
        FILE_PATH,
        EXPORTED_DATE,
        EXPORTED,
        REQUEST_ID,
        CREATED_BY,
        CREATION_DATE,
        LAST_UPDATED_BY,
        LAST_UPDATE_DATE,
        LAST_UPDATE_LOGIN)
       select distinct
              cSource_id,
              vFile_export_id,
              vCurrentFileName,
              vPAth,
              sysdate,
              'Y',
              nvl(cRequest_id, vt_Request_id),
              fnd_global.user_id,
              sysdate,
              fnd_global.user_id,
              sysdate,
              fnd_global.login_id
         from xxcp_staged_flat_file
        where vt_file_type_id = vFile_type_id
          and vt_instance_id = vInstance_id
          and vt_request_id = nvl(cRequest_id, vt_request_id)
          and nvl(vt_qualifier1, '~NULL~') = nvl(vQualifier1, '~NULL~')
          and nvl(vt_qualifier2, '~NULL~') = nvl(vQualifier2, '~NULL~')
          and nvl(vt_qualifier3, '~NULL~') = nvl(vQualifier3, '~NULL~')
            and nvl(vt_qualifier4, '~NULL~') = nvl(vQualifier4, '~NULL~')
            -- 02.06.10
            and vt_file_export_id is null;

        -- mark the file as being exported
        update xxcp_staged_flat_file
           set vt_file_export_id       = vFile_export_id,
               vt_source_creation_date = sysdate
         where vt_file_type_id = vFile_type_id
           and vt_instance_id = vInstance_id
           and vt_request_id = nvl(cRequest_id, vt_request_id)
           -- 02.06.06
           and nvl(vt_qualifier1,'~NULL~') = nvl(vQualifier1, '~NULL~')
           and nvl(vt_qualifier2,'~NULL~') = nvl(vQualifier2, '~NULL~')
           and nvl(vt_qualifier3,'~NULL~') = nvl(vQualifier3, '~NULL~')
           and nvl(vt_qualifier4,'~NULL~') = nvl(vQualifier4, '~NULL~')
           -- 02.06.10
           and vt_file_export_id is null;


      vFiles_processed := vFiles_processed + nvl(sql%rowcount,0);

    end if;

    close c3;

    -- If custom event has error then do nothing and return the error.
    else
      vErrorCode := vJob_Status;
    end if;

  end loop;

  close c2;

  writeOutput(' ');
  writeOutput('Processed '||vFiles_processed||' file(s).');

  writeOutput('End Flat File Export <'||to_char(sysdate,'DD-MON-YYYY HH24:MI:SS')||'>');

  commit;

  return(vErrorCode);
Exception
  when dup_val_on_index then
    raise_application_error(-20010,
                            'File ' || vPath || '/' || vCurrentFileName ||
                            ' already exists.');
    return(-3);
  when ex_rename then
    raise_application_error(-20011,
                            'Unable to rename file ' || vPath || '/' ||
                            vCurrentFileName || ' to ' || vPath || '/' ||
                            vRenamedFile || '.');
    return(-3);
  when ex_nopath then
    raise_application_error(-20012,
                            'Path not found. Please check the XXCPEXPORT directory.');
    return(-3);

  when others then

    writeOutput('Error occured: Rolling back.');

    -- delete any files created
    For i in 1..gFileName_tab.count loop
      -- check to see if the file exists in the output directory.
      if XXCP_File_API.exists(vPath || '/' || gFileName_tab(i)) = 1 then
        -- Delete the file
        if XXCP_File_API.delete(vPath || '/' ||gFileName_tab(i)) = 1 then
          writeOutput('Successfully deleted:'||gFileName_tab(i));
        else
          writeOutput('Error: Unable to delete file: '||gFileName_tab(i));
        end if;
      else
        writeOutput('Error: Unable find file: '||gFileName_tab(i));
      end if;
    end loop;

    -- close any files that are open
    if utl_file.is_open(vFileHandle) then
      utl_file.fclose(vFileHandle);
    end if;

    -- rollback any data
    rollback;

    --re-raise the error.
    raise;

end exportFile;

-----------------------------------------------------------------------
-- insert_staged_flat_file
-----------------------------------------------------------------------
function insertStagedFlatFile(cStaged_flat_file_rec xxcp_staged_flat_file%rowtype,
                              cErr_msg out varchar2)
  return number is

  vError_code number := 0;

begin

  insert into xxcp_staged_flat_file
    (vt_request_id,
     vt_instance_id,
     vt_source_assignment_id,
     vt_process_history_id,
     vt_target_assignment_id,
     vt_transaction_date,
     vt_source_creation_date,
     vt_file_type_id,
     vt_record_type,
     vt_file_export_id,
     column1,
     column2,
     column3,
     column4,
     column5,
     column6,
     column7,
     column8,
     column9,
     column10,
     column11,
     column12,
     column13,
     column14,
     column15,
     column16,
     column17,
     column18,
     column19,
     column20,
     column21,
     column22,
     column23,
     column24,
     column25,
     column26,
     column27,
     column28,
     column29,
     column30,
     column31,
     column32,
     column33,
     column34,
     column35,
     column36,
     column37,
     column38,
     column39,
     column40,
     column41,
     column42,
     column43,
     column44,
     column45,
     column46,
     column47,
     column48,
     column49,
     column50,
     vt_qualifier1,
     vt_qualifier2,
     vt_qualifier3,
     vt_qualifier4,
     vt_order_sequence1,
     vt_order_sequence2,
     -- 02.06.10
     vt_filename_infix)
  values
    (cStaged_flat_file_rec.vt_request_id,
     cStaged_flat_file_rec.vt_instance_id,
     cStaged_flat_file_rec.vt_source_assignment_id,
     cStaged_flat_file_rec.vt_process_history_id,
     cStaged_flat_file_rec.vt_target_assignment_id,
     cStaged_flat_file_rec.vt_transaction_date,
     cStaged_flat_file_rec.vt_source_creation_date,
     cStaged_flat_file_rec.vt_file_type_id,
     cStaged_flat_file_rec.vt_record_type,
     cStaged_flat_file_rec.vt_file_export_id,
     cStaged_flat_file_rec.column1,
     cStaged_flat_file_rec.column2,
     cStaged_flat_file_rec.column3,
     cStaged_flat_file_rec.column4,
     cStaged_flat_file_rec.column5,
     cStaged_flat_file_rec.column6,
     cStaged_flat_file_rec.column7,
     cStaged_flat_file_rec.column8,
     cStaged_flat_file_rec.column9,
     cStaged_flat_file_rec.column10,
     cStaged_flat_file_rec.column11,
     cStaged_flat_file_rec.column12,
     cStaged_flat_file_rec.column13,
     cStaged_flat_file_rec.column14,
     cStaged_flat_file_rec.column15,
     cStaged_flat_file_rec.column16,
     cStaged_flat_file_rec.column17,
     cStaged_flat_file_rec.column18,
     cStaged_flat_file_rec.column19,
     cStaged_flat_file_rec.column20,
     cStaged_flat_file_rec.column21,
     cStaged_flat_file_rec.column22,
     cStaged_flat_file_rec.column23,
     cStaged_flat_file_rec.column24,
     cStaged_flat_file_rec.column25,
     cStaged_flat_file_rec.column26,
     cStaged_flat_file_rec.column27,
     cStaged_flat_file_rec.column28,
     cStaged_flat_file_rec.column29,
     cStaged_flat_file_rec.column30,
     cStaged_flat_file_rec.column31,
     cStaged_flat_file_rec.column32,
     cStaged_flat_file_rec.column33,
     cStaged_flat_file_rec.column34,
     cStaged_flat_file_rec.column35,
     cStaged_flat_file_rec.column36,
     cStaged_flat_file_rec.column37,
     cStaged_flat_file_rec.column38,
     cStaged_flat_file_rec.column39,
     cStaged_flat_file_rec.column40,
     cStaged_flat_file_rec.column41,
     cStaged_flat_file_rec.column42,
     cStaged_flat_file_rec.column43,
     cStaged_flat_file_rec.column44,
     cStaged_flat_file_rec.column45,
     cStaged_flat_file_rec.column46,
     cStaged_flat_file_rec.column47,
     cStaged_flat_file_rec.column48,
     cStaged_flat_file_rec.column49,
     cStaged_flat_file_rec.column50,
     cStaged_flat_file_rec.vt_qualifier1,
     cStaged_flat_file_rec.vt_qualifier2,
     cStaged_flat_file_rec.vt_qualifier3,
     cStaged_flat_file_rec.vt_qualifier4,
     cStaged_flat_file_rec.vt_order_sequence1,
     cStaged_flat_file_rec.vt_order_sequence2,
     -- 02.06.10
     cStaged_flat_file_rec.vt_filename_infix);

  return vError_code;

exception
  when others then

    cErr_msg := substr(sqlerrm,1,200);
    return -1;

end insertStagedFlatFile;

-----------------------------------------------------------------------
-- update_staged_flat_file
-----------------------------------------------------------------------
function updateStagedFlatFile(cColumn_name  in varchar2,
                              cColumn_value in varchar2,
                              cRequest_id   in number,
                              cFile_type_id in number,
                              cInstance_id  in number,
                              cQualifier1   in varchar2,
                              cQualifier2   in varchar2,
                              cQualifier3   in varchar2,
                              cQualifier4   in varchar2,
                              cErr_msg out varchar2)
  return number is

  vError_code number := 0;

begin

  update xxcp_staged_flat_file
     set column1 = decode(cColumn_name, 'column1', cColumn_value, column1),
         column2 = decode(cColumn_name, 'column2', cColumn_value, column2),
         column3 = decode(cColumn_name, 'column3', cColumn_value, column3),
         column4 = decode(cColumn_name, 'column4', cColumn_value, column4),
         column5 = decode(cColumn_name, 'column5', cColumn_value, column5),
         column6 = decode(cColumn_name, 'column6', cColumn_value, column6),
         column7 = decode(cColumn_name, 'column7', cColumn_value, column7),
         column8 = decode(cColumn_name, 'column8', cColumn_value, column8),
         column9 = decode(cColumn_name, 'column9', cColumn_value, column9),
         column10 = decode(cColumn_name, 'column10', cColumn_value, column10),
         -- 11..20
         column11 = decode(cColumn_name, 'column11', cColumn_value, column11),
         column12 = decode(cColumn_name, 'column12', cColumn_value, column12),
         column13 = decode(cColumn_name, 'column13', cColumn_value, column13),
         column14 = decode(cColumn_name, 'column14', cColumn_value, column14),
         column15 = decode(cColumn_name, 'column15', cColumn_value, column15),
         column16 = decode(cColumn_name, 'column16', cColumn_value, column16),
         column17 = decode(cColumn_name, 'column17', cColumn_value, column17),
         column18 = decode(cColumn_name, 'column18', cColumn_value, column18),
         column19 = decode(cColumn_name, 'column19', cColumn_value, column19),
         column20 = decode(cColumn_name, 'column20', cColumn_value, column20),
         -- 21..30
         column21 = decode(cColumn_name, 'column21', cColumn_value, column21),
         column22 = decode(cColumn_name, 'column22', cColumn_value, column22),
         column23 = decode(cColumn_name, 'column23', cColumn_value, column23),
         column24 = decode(cColumn_name, 'column24', cColumn_value, column24),
         column25 = decode(cColumn_name, 'column25', cColumn_value, column25),
         column26 = decode(cColumn_name, 'column26', cColumn_value, column26),
         column27 = decode(cColumn_name, 'column27', cColumn_value, column27),
         column28 = decode(cColumn_name, 'column28', cColumn_value, column28),
         column29 = decode(cColumn_name, 'column29', cColumn_value, column29),
         column30 = decode(cColumn_name, 'column30', cColumn_value, column30),
         -- 31..40
         column31 = decode(cColumn_name, 'column31', cColumn_value, column31),
         column32 = decode(cColumn_name, 'column32', cColumn_value, column32),
         column33 = decode(cColumn_name, 'column33', cColumn_value, column33),
         column34 = decode(cColumn_name, 'column34', cColumn_value, column34),
         column35 = decode(cColumn_name, 'column35', cColumn_value, column35),
         column36 = decode(cColumn_name, 'column36', cColumn_value, column36),
         column37 = decode(cColumn_name, 'column37', cColumn_value, column37),
         column38 = decode(cColumn_name, 'column38', cColumn_value, column38),
         column39 = decode(cColumn_name, 'column39', cColumn_value, column39),
         column40 = decode(cColumn_name, 'column40', cColumn_value, column40),
         -- 41..50
         column41 = decode(cColumn_name, 'column41', cColumn_value, column41),
         column42 = decode(cColumn_name, 'column42', cColumn_value, column42),
         column43 = decode(cColumn_name, 'column43', cColumn_value, column43),
         column44 = decode(cColumn_name, 'column44', cColumn_value, column44),
         column45 = decode(cColumn_name, 'column45', cColumn_value, column45),
         column46 = decode(cColumn_name, 'column46', cColumn_value, column46),
         column47 = decode(cColumn_name, 'column47', cColumn_value, column47),
         column48 = decode(cColumn_name, 'column48', cColumn_value, column48),
         column49 = decode(cColumn_name, 'column49', cColumn_value, column49),
         column50 = decode(cColumn_name, 'column50', cColumn_value, column50)
   where vt_file_type_id = cFile_type_id    
     and vt_instance_id = cInstance_id
     and vt_request_id = nvl(cRequest_id, vt_request_id)
     and nvl(vt_qualifier1,'~NULL~') = nvl(cQualifier1, '~NULL~')
     and nvl(vt_qualifier2,'~NULL~') = nvl(cQualifier2, '~NULL~')
     and nvl(vt_qualifier3,'~NULL~') = nvl(cQualifier3, '~NULL~')
     and nvl(vt_qualifier4,'~NULL~') = nvl(cQualifier4, '~NULL~') 
     -- Make sure that you don't update any exported files                     
     and vt_file_export_id is null;
  
  return vError_code;

exception
  when others then
  
    cErr_msg := substr(sqlerrm,1,200);
    return -1;
  
end updateStagedFlatFile;

begin
  init;

END XXCP_FILE_UPLOAD;
/
