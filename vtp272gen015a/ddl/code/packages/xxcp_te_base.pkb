CREATE OR REPLACE PACKAGE BODY XXCP_TE_BASE AS
-- !! *********************************************************************************************
-- !!                         V I R T U A L  T R A D E R  L I M I T E D 
-- !!             (Copyright 2003-2012 Virtual Trader Ltd. All rights Reserved)
-- !!
-- !!    NAME:       XXCP_TE_BASE 
-- !!    PURPOSE:    Common code calls for Wrappers
-- !!
-- !!    REVISIONS:
-- !!    Ver        Date      Author  Description
-- !!    ---------  --------  ------- ------------------------------------
-- !!    02.01.00   03/07/03  Keith   First Coding
-- !!    02.01.01   30/12/03  Keith   Changed structure to remove unwanted code from 2.1.1
-- !!    02.01.02   01/03/04  Keith   Added Enforce_Accounting
-- !!    02.01.03   25/04/04  Keith   Added DumpTransactionPath to a common place
-- !!    02.01.04   29/06/04  Keith   Removed the loading of adjustment rates into memory
-- !!    02.01.05   01/07/04  Keith   Added 3 function
-- !!    02.01.06   12/10/04  Keith   Display Array data
-- !!    02.01.06   25/02/05  Keith   Reduced the use of XXCP_DYNAMIC_ARRAY
-- !!    02.02.07   12/05/05  Keith   Moved Account Rounding into this package
-- !!                                 Changed Minutes timing issue in report
-- !!    02.02.09   03/09/05  Keith   Cross Validation Override in Preview Mode added
-- !!    02.03.00   10/01/06  Keith   Added xxcp_global.set_source_id to the INIT.
-- !!    02.03.01   22/01/06  Keith   Added xxcp_wks.BRK_SET_RCD(1).sum_qty := 0 to
-- !!                                 make sure that a record exists to prevent fall over
-- !!                                 in xxcp_te_engine.
-- !!    02.03.02   21/02/06  Keith   Added Target Set of books id to Transaction path
-- !!    02.03.03   18/04/06  Keith   Changed Init( to Engine_Init(
-- !!    02.03.04   25/08/06  Keith   Added Trx1-3 + Trx Attributes to Transaction Path
-- !!    02.03.05   19/12/06  Keith   Added gAcct_Exted_Bal, added zero suppression function
-- !!    02.04.01   04/01/07  Keith   Default to params in utils_curr_conv. and rounding
-- !!    02.04.02   24/01/07  Keith   Removed CCUR logic.
-- !!    02.04.03   11/02/07  Keith   Added Tax flags
-- !!    02.04.04   25/06/07  Keith   Changed Zero logic suppression to take into acocunt Entered value.
-- !!    02.04.05   11/10/07  Keith   ValueAttributesUsed
-- !!    02.04.06   15/10/07  Keith   Added Pass Through Flag logic
-- !!    02.04.07   16/11/07  Keith   Improved Rounding Logic to look at 21/23 or 81/83 values.
-- !!    02.04.08   26/12/07  Keith   Timing reporting modified
-- !!    02.05.01   01/10/08  Keith   Added gBalancing_Trace
-- !!    02.05.02   19/11/08  Nigel   Added default trading relationship change
-- !!    02.05.03   16/06/09  Simon   Added Get_Cost_Category and Get_Payer_Details
-- !!    02.05.04   26/06/09  Simon   Changed Get_Payer_Details to use boolean algebra to hit index.
-- !!    02.05.05   09/07/09  Simon   Change datatypes of Get_Payer_Details.
-- !!    02.05.06   28/07/09  Simon   Add cost_category_id and data_source to Get_Payer_Details.
-- !!    02.05.07   30/07/09  Simon   Modify Payee to varchar2 in Get_Payer_Details.
-- !!    02.05.08   17/08/09  Simon   Added w.data_source='VT' when category_id = 0 in Get_Payer_Details.
-- !!    02.05.09   29/08/09  Keith   Added error codes to get_cost_category and get_payer_details
-- !!    02.06.01   09/11/09  Simon   Get_Cost_Category to check account_from and account_to.
-- !!    02.06.02   17/02/10  Simon   Added Balancing Segment 11 Override system profile.
-- !!    02.06.03   26/05/10  Simon   Fix Leading zero trim on segments.
-- !!    02.06.04   04/11/10  Nigel   Fixed issue with timeElapsedFunction and added Custom Event trace
-- !!    02.06.05   12/11/10  Nigel   Added addtional Custom Event trace when in trace mode
-- !!    02.06.06   28/02/11  Keith   Line Numbering
-- !!    02.06.07   26/09/11  Keith   Target Suppress output and Round Suppressed Rules flag
-- !!    03.06.08   06/02/12  Mark    Change FndWriteTimings to stop cMessage to 4k
-- !!    03.06.09   17/02/12  Keith   Centralized Trading Relationships call
-- !!    03.06.10   01/03/12  Mat     Retrieve Default Trading Relationship from init
-- !!                                 Get_Trading_Relationship - change to EP cursor 
-- !!    03.06.11   23/03/12  Keith   Added Remove_Unwanted_Records to centralize things
-- !!    03.06.12   24/04/12  Keith   Assignment Count and Transaction Count for activity control
-- !!    03.06.13   25/06/12  Simon   Account_Rounding CROSS CURRENCY rounding Fix merged.
-- !!    03.06.14   11/07/12  Simon   Fix Preview_on value passed to LoadSQLBuildStatements.
-- !!    03.06.15   16/07/12  Nigel   Moved ICS Classification Attribute population from xxcp_te_engine.init to engine_init
-- !!    03.06.16   12/09/12  Nigel   Added Quantity + Unmatched Attribute population
-- !!    03.06.16a  14/09/12  Simon   Added Cost_Plus_Set_Id (OOD-293).
-- !!    03.06.17   21/10/12  Simon   Get_Tax_Reg_Group now returns Null if no group exists (OOD-304).
-- !!    03.06.18   28/09/12  Nigel   Added new Dynamic Attribute population
-- !! ********************************************************************************************************/

 gCur_cnt                 pls_integer := 0;
 gDumpCnt                 pls_integer := 0;
 gPathHeaders             varchar2(1) := 'Y';
 gBalancing_Trace         varchar2(1) := 'N';
 gEngineStartDate         date;
 
 -- NCM 02.04.09 Added for default trading relationship change
 gDefault_trading_relationship  xxcp_table_set_ctl.set_name%type := 0; 
 
 -- 03.06.15
 gClassificationAttribute number(4) := 0;
 -- 03.06.18
 gApproveRestrictedAttribute number(4) := 0;
 -- 03.06.16
 gUnmatchedAttribute      number(4) := 0; 
 gQuantityAttribute       number(4) := 0;


 -- 03.06.16 (Cost Plus Attributes)
 gUpliftRateAttribute        number(4) := 0;
 gGrowthRateAttribute        number(4) := 0;
 gAvgPeriodCostAttribute     number(4) := 0;
 gTotalCostAttribute         number(4) := 0;
 gConsideredPeriodsAttribute number(4) := 0;
 gCurrencyCodeAttribute      number(4) := 0;
 gCostCategoryIdAttribute    number(4) := 0;
 gUpliftedAttribute          number(4) := 0;
 gTrueUpAttribute            number(4) := 0;
 gPeriodsRequiredAttribute   number(4) := 0;
 gPayer1Attribute            number(4) := 0;
 gPayer1PercentAttribute     number(4) := 0;
 gPayer2Attribute            number(4) := 0;
 gPayer2PercentAttribute     number(4) := 0;
 gIntermediatePayerAttribute number(4) := 0;
 gAccountAttribute           number(4) := 0;

 -- Currency Conversion
 gCUCO_INUSE              varchar2(1) := 'N';
 gCUCO_Common_Currency    varchar2(3);
 gCUCO_Target_Currency    varchar2(3);
 gCUCO_Exchange_Type      varchar2(30);
 gCUCO_Exchange_Date      Date;
 gCUCO_Dft_Exchange_Type  varchar2(30);
 
 gJobStartTime            Date;
 
 -- Pass Through Control 
 gPassThroughRules        Varchar2(1) := 'N';
 gPassThroughIgnore       Varchar2(1) := 'N';
 gPassThroughPrvCode      Number(6);
 gPassThroughPrvParent    Number;

 TYPE gCursors_Rec is RECORD(
     v_cursorId           pls_Integer := 0,
     v_Target_Table       xxcp_column_rules.transaction_table%type,
     v_usage              varchar2(38),
     v_pharsed            varchar2(1) := 'N',
     v_OpenCur            varchar2(1) := 'N',
     v_LastCur            pls_integer := -1);

 TYPE gCUR_REC    is TABLE of gCursors_Rec    INDEX by BINARY_INTEGER;
 gCursors  gCUR_REC;

 -- !! ***********************************************************************************************************
 -- !!
 -- !!                                  Software_Version
 -- !!
 -- !! ***********************************************************************************************************
  Function Software_Version RETURN VARCHAR2 IS
 Begin
   Return('Version [03.06.18] Build Date [28-SEP-2012] Name [XXCP_TE_BASE]');
 End Software_Version;

 -- !! ***********************************************************************************************************
 -- !!
 -- !!                                   Ignore_Class
 -- !!
 -- !! ***********************************************************************************************************
 Function Ignore_Class(cTransaction_Table in varchar2, cTransaction_Class in varchar2) return boolean is

    vResult Boolean := False;

    c pls_integer := xxcp_global.gCLA_Cnt;
    j pls_integer;

  Begin

    -- Ignore some classes
    For j in 1..c loop
     If (xxcp_global.gCLA(j).transaction_table = cTransaction_Table and
           xxcp_global.gCLA(j).transaction_class = cTransaction_Class) then
       vResult := TRUE;
       Exit;
     End If;
    End Loop;
                               
    Return(vResult);
 END Ignore_Class;

 -- !! ***********************************************************************************************************
 -- !!                            InitPassThrough 
 -- !! ***********************************************************************************************************
 Procedure InitPassThrough(cSource_id in number) is
 
    Cursor PT(pSource_id in number) is
     select 'X' Rules_On
       from xxcp_assignment_ctl c,
            xxcp_assignment_qualifiers q
       where c.source_id = pSource_id
         and c.ASSIGNMENT_SET_ID = q.ASSIGNMENT_SET_ID
         and nvl(q.PASS_THROUGH_SET_ID,0) > 0;

 
 Begin
 
    gPassThroughRules := 'N';
    For Rec in PT(cSource_id) Loop
      gPassThroughRules := 'Y';
    End Loop;
 
    gPassThroughIgnore    := 'N';
    gPassThroughPrvCode   := Null;
    gPassThroughPrvParent := Null; 
   
 End InitPassThrough;
 
 
 -- !! ***********************************************************************************************************
 -- !!                            GetPassThroughActive
 -- !! ***********************************************************************************************************
 Function GetPassThroughActive return varchar2 is
 Begin
   Return(gPassThroughRules);
 End GetPassThroughActive;
  

  -- !! ***********************************************************************************************************
 -- !!                            GetPassThroughIgnore
 -- !! ***********************************************************************************************************
 Function GetPassThroughIgnore return varchar2 is
 Begin
   Return(gPassThroughIgnore);
 End GetPassThroughIgnore;

 -- !! ***********************************************************************************************************
 -- !!                            SetPassThroughIgnore
 -- !! ***********************************************************************************************************
 Procedure SetPassThroughIgnore(cPassThroughIgnore in varchar2) is
 Begin
   gPassThroughIgnore := cPassThroughIgnore;
 End SetPassThroughIgnore;
 
  -- !! ***********************************************************************************************************
 -- !!                            GetPassThroughPrvCode
 -- !! ***********************************************************************************************************
 Function GetPassThroughPrvCode return number is
 Begin
   Return(gPassThroughPrvCode);
 End GetPassThroughPrvCode;
 
 -- !! ***********************************************************************************************************
 -- !!                            SetPassThroughPrvCode
 -- !! ***********************************************************************************************************
 Procedure SetPassThroughPrvCode(cPassThroughPrvCode in number) is
 Begin
   gPassThroughPrvCode := cPassThroughPrvCode;
 End SetPassThroughPrvCode;
 
 -- !! ***********************************************************************************************************
 -- !!                            get_dflt_trading_relationship_name
 -- !! ***********************************************************************************************************
 function Get_Dflt_Trading_Relationship return varchar2 is
 begin
  return (gDefault_trading_relationship);
 end Get_Dflt_Trading_Relationship;  
 
 -- !! ***********************************************************************************************************
 -- !!                            get_classification_attribute
 -- !! ***********************************************************************************************************
 function Get_Classification_Attribute return varchar2 is
 begin
  return (gClassificationAttribute);
 end Get_Classification_Attribute;  

-- !! ***********************************************************************************************************
 -- !!                            Get_Matching_Attribute
 -- !! ***********************************************************************************************************
 function Get_Unmatched_Attribute return varchar2 is
 begin
  return (gUnmatchedAttribute);
 end Get_Unmatched_Attribute;   

 -- !! ***********************************************************************************************************
 -- !!                            Get_Matching_Attribute
 -- !! ***********************************************************************************************************
 function Get_Quantity_Attribute return varchar2 is
 begin
  return (gQuantityAttribute);
 end Get_Quantity_Attribute; 
   
 -- !! ***********************************************************************************************************
 -- !!                            Get_Approve_Restricted_Attribute
 -- !! ***********************************************************************************************************
 function Get_Approve_Restricted_Att return varchar2 is
 begin
  return (gApproveRestrictedAttribute);
 end Get_Approve_Restricted_Att; 

 -- !! ***********************************************************************************************************
 -- !!                            get_classification_attribute
 -- !! ***********************************************************************************************************
 function Get_CPA_Attributes(cProfile_Name IN VARCHAR2) return varchar2 is
 begin
   If upper(cProfile_Name) = 'UPLIFT RATE ATTRIBUTE' then
     return(gUpliftRateAttribute);
   elsif upper(cProfile_Name) = 'GROWTH RATE ATTRIBUTE' then
     return(gGrowthRateAttribute);
   elsif upper(cProfile_Name) = 'AVG PERIOD COST ATTRIBUTE' then
     return(gAvgPeriodCostAttribute);
   elsif upper(cProfile_Name) = 'TOTAL COST ATTRIBUTE' then
     return(gTotalCostAttribute);
   elsif upper(cProfile_Name) = 'CONSIDERED PERIODS ATTRIBUTE' then
     return(gConsideredPeriodsAttribute);
   elsif upper(cProfile_Name) = 'CURRENCY CODE ATTRIBUTE' then
     return(gCurrencyCodeAttribute);
   elsif upper(cProfile_Name) = 'COST CATEGORY ID ATTRIBUTE' then
     return(gCostCategoryIdAttribute);
   elsif upper(cProfile_Name) = 'UPLIFTED AMOUNT ATTRIBUTE' then
     return(gUpliftedAttribute);
   elsif upper(cProfile_Name) = 'TRUE UP AMOUNT ATTRIBUTE' then
     return(gTrueUpAttribute);
   elsif upper(cProfile_Name) = 'PERIODS REQUIRED ATTRIBUTE' then
     return(gPeriodsRequiredAttribute);
   elsif upper(cProfile_Name) = 'PAYER1 ATTRIBUTE' then
     return(gPayer1Attribute);
   elsif upper(cProfile_Name) = 'PAYER1 PERCENT ATTRIBUTE' then
     return(gPayer1PercentAttribute);
   elsif upper(cProfile_Name) = 'PAYER2 ATTRIBUTE' then
     return(gPayer2Attribute);
   elsif upper(cProfile_Name) = 'PAYER2 PERCENT ATTRIBUTE' then
     return(gPayer2PercentAttribute);
   elsif upper(cProfile_Name) = 'INTERMEDIATE PAYER ATTRIBUTE' then
     return(gIntermediatePayerAttribute);
   elsif upper(cProfile_Name) = 'ACCOUNT ATTRIBUTE' then
     return(gAccountAttribute);
   else
     return(null);
   end if;

 end Get_CPA_Attributes;                    

  --  !! ***********************************************************************************************************
  --  !!
  --  !!                                  Get_Cost_Category (CPA)
  --  !!           Gets the latest Cost Category from xxcp_instance_cpa_collector_v.
  --  !!
  --  !! ***********************************************************************************************************
  Function Get_Cost_Category (cCompany              in varchar2,
                              cDepartment           in varchar2,
                              cAccount              in varchar2,
                              cPeriod_End_Date      in date,
                              cCost_Category_Id     out number,
                              cCategory_Data_Source out varchar2,
                              cCost_Category        out varchar2,
                              cCost_Plus_Set_Id     in number) return number is

    vAccount_from varchar2(30);
    vAccount_to   varchar2(30);
    vInternalErrorCode xxcp_errors.internal_error_code%type := 0;

    Cursor c1 is select cost_category_id,
                        category_data_source,
                        cost_category
                 from   xxcp_instance_cpa_collector_v
                 where  cCompany         between company_from and company_to
                 and    cDepartment      between department_from and department_to
                 and    vAccount_from    between account_from and account_to
                 and    vAccount_to      between account_from and account_to
                 and    cPeriod_end_date between nvl(effective_from_date,to_date('01-JAN-1900','DD-MON-YYYY')) and nvl(effective_to_date,to_date('01-JAN-2900','DD-MON-YYYY'))
                 and    active = 'Y'
                 and    cost_plus_set_id = cCost_Plus_Set_Id
                 order by collector_id desc;


  Begin

    vAccount_from := substr(cAccount,1,instr(cAccount,'-')-1);  -- 02.06.01
    vAccount_to   := substr(cAccount,instr(cAccount,'-')+1);    -- 02.06.01
  
    Begin

     Open  c1;
     fetch c1 into cCost_Category_Id,
                   cCategory_Data_Source,
                   cCost_Category;
     close c1;
    
     If cCost_Category_Id is null or cCategory_Data_Source is Null then
      vInternalErrorCode := 7013;
     End if;
  
     Exception When OTHERS then vInternalErrorCode := 7013;
   
    End;
   
    If xxcp_global.Trace_on = 'Y' or vInternalErrorCode != 0 Then
     xxcp_foundation.FndWriteError(vInternalErrorCode,'Company <'||cCompany||'> Department <'||cDepartment||'> Account <'||cAccount||'> Date <'||cPeriod_End_Date||'> '||chr(10)||  
                                                      'Cost Category Id <'||to_char(cCost_Category_Id)||'> Data Source '||cCategory_Data_Source||'> Cost Category <'||cCost_Category||'>');
    End If;

   Return(vInternalErrorCode);
    
  End Get_Cost_Category;

  --  !! ***********************************************************************************************************
  --  !!
  --  !!                                  Get_Payer_Details (CPA)
  --  !!                    Gets the Payer Details using Best Match Logic.
  --  !!
  --  !! ***********************************************************************************************************
  Function Get_Payer_Details (cPayee            in  varchar2,
                              cTerritory        in  varchar2,
                              cCost_Category_id in  number,
                              cData_source      in  varchar2,
                              cTransaction_Date in  date,
                              cPayer1           out varchar2,
                              cPayer1_Percent   out number,
                              cPayer2           out varchar2,
                              cPayer2_Percent   out number,
                              cInter_Payer      out varchar2,
                              cAccount_Type     out varchar2) return number is
    vPayer_ctl_id number;

    cursor c1 is
       Select
                 Payer1,
                 Payer1_Percent,
                 Payer2,
                 Payer2_Percent,
                 Inter_Payer,
                 Account_Type,
                 Payer_Ctl_Id
                 from
                  (select
                      w.*
                    from xxcp_instance_cpa_payer_v w
                   where cTransaction_Date Between nvl(w.EFFECTIVE_FROM_DATE,to_date('01-JAN-2000','DD-MON-YY')) and nvl(w.EFFECTIVE_TO_DATE,to_date('31-DEC-2999','DD-MON-YY'))
                     and (
                          (w.payee = cPayee and w.Territory = cTerritory and w.Cost_Category_id = cCost_Category_id and w.data_source = cData_Source)
                           or
                          (w.payee = cPayee and w.Territory = cTerritory and w.Cost_Category_id = 0 and w.data_source = 'VT')
                           or
                          (w.payee = cPayee and w.Territory = '*'        and w.Cost_Category_id = cCost_Category_id and w.data_source = cData_Source)
                           or
                          (w.payee = cPayee and w.Territory = '*'        and w.Cost_Category_id = 0 and w.data_source = 'VT')
                           or
                          (w.payee = '*'    and w.Territory = cTerritory and w.Cost_Category_id = cCost_Category_id and w.data_source = cData_Source)
                           or
                          (w.payee = '*'    and w.Territory = cTerritory and w.Cost_Category_id = 0 and w.data_source = 'VT')
                           or
                          (w.payee = '*'    and w.Territory = '*'        and w.Cost_Category_id = cCost_Category_id and w.data_source = cData_Source)
                           or
                          (w.payee = '*'    and w.Territory = '*'        and w.Cost_Category_id = 0 and w.data_source = 'VT')
                         )
                  )
                 order by Decode(Payee,cPayee,                4000,'*',1,0)+
                          Decode(Territory,cTerritory,        2000,'*',1,0)+
                          Decode(Cost_Category_id,cCost_Category_id,1000,0,1,0) desc,   -- If Tied order by Only Exact Match
                          data_source,
                          Payer_ctl_id desc;
                          
   vInternalErrorCode xxcp_errors.internal_error_code%type := 0;                       
                          
  Begin
    Begin
      Open c1;
      fetch c1 into cPayer1,
                    cPayer1_Percent,
                    cPayer2,
                    cPayer2_Percent,
                    cInter_Payer,
                    cAccount_Type,
                    vPayer_ctl_id;
      close c1;
  
      Exception
        when OTHERS then vInternalErrorCode := 7012;
   End;
   
   If xxcp_global.Trace_on = 'Y' or vInternalErrorCode != 0 Then
     xxcp_foundation.FndWriteError(vInternalErrorCode,
                                  'Payee <'||cPayee||'> Territory <'||cTerritory||'> Cost Category Id <'||cCost_Category_id||'> '||
                                  'Data Source <'||cData_source||'> Transaction Date <'||cTransaction_Date||'> '||chr(10)||
                                  'Payer 1 <'||cPayer1||'> Payer 1 % <'||cPayer1_Percent||'> '||chr(10)||
                                  'Payer 2 <'||cPayer2||'> Payer 2 % <'||cPayer2_Percent||'> '||chr(10)||
                                  'Int. Payer <'||cInter_Payer||'> Account Type <'||cAccount_Type||'> '||chr(10)||
                                  'Payer ctl id <'||vPayer_ctl_id||'>');
   End If;

   Return(vInternalErrorCode);

  End Get_Payer_Details;
  
  --  !! ***********************************************************************************************************
  --  !!
  --  !!                                    REPLAN_CONTROL
  --  !!                  External Procedure (Entry Point) to start the RE-PLAN Engine.
  --  !!
  --  !! ***********************************************************************************************************
 
 --
 --  TransactionPathTitles
 --
 -- Called by this package and Preview forms too.
 --
 Function TransactionPathTitles(cType in varchar2, cLines in number) return long is

    vText       Long; 

    cursor SZ(pSource_id in number, pSource_Assignment_id in number) is
     select s.source_name, a.source_assignment_name
       from xxcp_sys_sources s,
            xxcp_source_assignments a
       where s.source_id = pSource_id
         and s.source_id = a.source_id
         and a.source_assignment_id = pSource_Assignment_id;

   Begin

     If upper(cType) = 'HEADER' then

         vText := Null;

         For Rec in SZ(xxcp_global.gCommon(1).Source_id, xxcp_global.gCommon(1).current_assignment_id) loop
           vText := 'Source Name:'||chr(9)||Rec.Source_name||chr(9)||'Assignment Name:'||chr(9)||rec.source_assignment_name||chr(10);
           Exit;
         End Loop;

         vText := vText||'Trad. Seq.'||
             chr(9)||'Model Seq.'||
             chr(9)||'Rule Seq.'||
             chr(9)||'Run Seq.'||
             chr(9)||'Set of books'||
             chr(9)||'Transaction ID'||
             chr(9)||'Interface Ref'||
             chr(9)||'Transaction Table'||
             chr(9)||'Class'||
             chr(9)||'Rule ID'||
             chr(9)||'Ent. DR'||
             chr(9)||'Ent. CR'||
             chr(9)||'Ent. Curr'||
             chr(9)||'Ent. Rnd'||
             chr(9)||'Acc. DR'||
             chr(9)||'Acc. CR'||
             chr(9)||'Acc.Curr'||
             chr(9)||'Acc. Rnd'||
             chr(9)||'Account'||
             chr(9)||'Acc. Date'||
             chr(9)||'Trading Set'||
             chr(9)||'Model Name'||
             chr(9)||'Rule Name'||
             chr(9)||'Rule Type'||
             chr(9)||'Rounding Group'||
             chr(9)||'Rule Cat.1'||
             chr(9)||'Rule Cat.2'||
             chr(9)||'Instance'||
             chr(9)||'Table Code'||
             chr(9)||'Change Sign'||
             chr(9)||'Parent ID'||
             chr(9)||'Attribute ID'||
             chr(9)||'Transaction Ref 1'||
             chr(9)||'Transaction Ref 2'||
             chr(9)||'Transaction Ref 3'||
             chr(9)||'Qty'||
             chr(9)||'Target Suppression'||
             chr(9)||
             chr(9)||'Bal 1'||
             chr(9)||'Bal 2'||
             chr(9)||'Bal 3'||
             chr(9)||'Bal 4'||
             chr(9)||'Bal 5'||
             chr(9)||'Bal 6'||
             chr(9)||'Bal 7'||
             chr(9)||'Bal 8'||
             chr(9)||'Bal 9'||
             chr(9)||'Bal 10'||
             chr(9)||'Bal 11'||
             chr(9)||'Bal 12'||
             chr(9)||
             chr(9)||'Seg 1'||
             chr(9)||'Seg 2'||
             chr(9)||'Seg 3'||
             chr(9)||'Seg 4'||
             chr(9)||'Seg 5'||
             chr(9)||'Seg 6'||
             chr(9)||'Seg 7'||
             chr(9)||'Seg 8'||
             chr(9)||'Seg 9'||
             chr(9)||'Seg 10'||
             chr(9)||'Seg 11'||
             chr(9)||'Seg 12'||
             chr(9)||'Seg 13'||
             chr(9)||'Seg 14'||
             chr(9)||'Seg 15'||
             chr(9)||
             chr(9)||'Trx. Attribute 1'||
             chr(9)||'Trx. Attribute 2'||
             chr(9)||'Trx. Attribute 3'||
             chr(9)||'Trx. Attribute 4'||
             chr(9)||'Trx. Attribute 5'||
             chr(9)||'Trx. Attribute 6'||
             chr(9)||'Trx. Attribute 7'||
             chr(9)||'Trx. Attribute 8'||
             chr(9)||'Trx. Attribute 9'||
             chr(9)||'Trx. Attribute 10'||
             chr(9)||
             chr(9)||'Date/Time '||to_char(sysdate,'DD-MON-YYYY HH24:MI:SS');

     End If;

     Return(vText);

   End TransactionPathTitles;

  -- !! ***********************************************************************************************************
  -- !!
  -- !!                                   DumpTransactionPath
  -- !!                 Format the Internal Data Structure and push out to errors table
  -- !!                 This creates the XLS Transaction Path
  -- !! ***********************************************************************************************************
  PROCEDURE DumpTransactionPath(cFE_cnt in integer, cInternalErrorCode in number) is

    vExcel_Path            Long;

    vRule_Name             xxcp_account_rules.rule_name%type;
    vModel_Name            xxcp_sys_model_names.Model_Name%type;
    vChange_Sign           xxcp_account_rules.change_sign%type;
    vRule_Display_Sequence xxcp_account_rules.rule_display_sequence%type;
    vRounding_group        xxcp_account_rules.Rounding_group%type;
    vMod_Display_Sequence  xxcp_sys_model_names.model_display_sequence%type;
    vTransaction_Table     xxcp_account_rules.transaction_table%type;
    vRule_Type             xxcp_account_rules.rule_type%type;
    vRule_category_1       xxcp_account_rules.Rule_category_1%type;
    vRule_category_2       xxcp_account_rules.Rule_category_2%type;

    cursor RN(pRule_id in number, pModel_Ctl_id in number) is
     select m.model_name, r.rule_name, r.change_sign, r.rule_display_sequence
           ,r.rounding_group
           ,m.model_display_sequence
           ,r.transaction_table
           ,decode(r.rule_type,'C','S',r.rule_type) Rule_Type
           ,r.rule_category_1
           ,r.rule_category_2
       from xxcp_sys_model_names m,
            xxcp_account_rules r,
            xxcp_model_ctl x
      where m.model_name_id = r.model_name_id
        and r.rule_id       = pRule_id
        and x.model_name_id = r.model_name_id
        and x.model_ctl_id  = pmodel_ctl_id;

    j   pls_integer;
    k   pls_integer;
    g   pls_integer := 0;
    c1  pls_integer := 0;

    -- Excel
    vSE  varchar2(20);
    e1   pls_integer := 0; 

    vEntDR        Varchar2(50);
    vEntCR        Varchar2(50);
    vAcctDR       Varchar2(50);
    vAcctCR       Varchar2(50);
    vAccount_Code varchar2(2000);
    vMessage      varchar2(100);

    vEntered_Amount    Number;
    vAccounted_Amount  Number;
    vInternalErrorCode number(8);
    vAccounted_Amounts varchar2(1) := 'N';

  Begin

    c1 := cFE_cnt;
    gDumpCnt := 1;


    vInternalErrorCode := cInternalErrorCode;

    If cInternalErrorCode = 100 then
      vMessage := 'TRACE <Transaction Path>';
      vInternalErrorCode := 101;
    Else
      vMessage := 'Transaction Path';
    End If;

    If nvl(cFE_cnt,0) > 0 then

      If gPathHeaders = 'Y' then
        vExcel_Path := xxcp_te_base.TransactionPathTitles('HEADER',0)||CHR(10);
      End If;

     If cFE_Cnt > 0 then  
       vAccounted_Amounts := xxcp_wks.WORKING_RCD(1).Accounted_Amounts;
     End If;

      For j in 1..cFE_cnt Loop
 

          g := g + 1;

          For RNRec in RN(xxcp_wks.WORKING_RCD(j).rule_id, xxcp_wks.WORKING_RCD(j).Model_Ctl_id)
          Loop
            vRule_Name             := RNRec.Rule_Name;
            vModel_Name            := RNRec.Model_Name;
            vChange_Sign           := RNRec.Change_Sign;
            vRule_Display_Sequence := RNRec.Rule_Display_Sequence;
            vMod_display_sequence  := RNRec.Model_display_sequence;
            vRounding_Group        := RNRec.Rounding_Group;
            vTransaction_Table     := RNRec.transaction_table;
            vRule_Type             := RNRec.Rule_Type;
            vRule_category_1       := RNRec.Rule_category_1;
            vRule_category_2       := RNRec.Rule_category_2;
          End Loop;

          -- build vAccount_Code
          vAccount_Code := Null;
          For k in 1..10 Loop
            vAccount_Code := vAccount_Code||xxcp_wks.WORKING_RCD(j).I1009_Array(k+10)||'-';
          End loop;

          vAccount_Code := rtrim(vAccount_Code,'-');

          vEntered_Amount   := nvl(xxcp_wks.WORKING_RCD(j).Entered_Amount, 0);
          vAccounted_Amount := nvl(xxcp_wks.WORKING_RCD(j).Accounted_Amount, 0);

          -- Entered
          If vEntered_Amount < 0 then
            vEntCR := abs(vEntered_Amount);
            vEntDR := Null;
          Else
            vEntDR := abs(vEntered_Amount);
            vEntCR := Null;
          End If;
          
          -- Accounted
          If vAccounted_Amount < 0 then
            vAcctCR := abs(vAccounted_Amount);
            vAcctDR := Null;
          Else
            vAcctDR := abs(vAccounted_Amount);
            vAcctCR := Null;
          End If;
 
          vExcel_Path := vExcel_Path||to_char(xxcp_wks.working_rcd(j).Trading_Set_Seq)||
                      chr(9)||to_char(vMod_display_sequence)||
                      chr(9)||to_char(vRule_display_sequence)||
                      chr(9)||lpad(j,4,0)||
                      chr(9)||xxcp_wks.working_rcd(j).set_of_books_id||
                      chr(9)||xxcp_wks.working_rcd(j).transaction_id||
                      chr(9)||xxcp_wks.working_rcd(j).D1001_array(3)||
                      chr(9)||vTransaction_table||
                      chr(9)||xxcp_wks.working_rcd(j).i1009_array(40)||
                      chr(9)||to_char(xxcp_wks.working_rcd(j).rule_id)||
                      chr(9)||vEntdr||chr(9)||vEntcr||
                      chr(9)||xxcp_wks.working_rcd(j).entered_currency||
                      chr(9)||xxcp_wks.working_rcd(j).entered_rnd_amount;
                      
          -- Don't show Accounted values if accounted not used            
          If vAccounted_Amounts = 'Y' then            
            vExcel_Path := vExcel_Path||                                  
                      chr(9)||vAcctDr||chr(9)||vAcctCr||
                      chr(9)||xxcp_wks.working_rcd(j).accounted_currency||
                      chr(9)||xxcp_wks.working_rcd(j).accounted_rnd_amount;
          Else
            vExcel_Path := vExcel_Path||
                      chr(9)||NULL||chr(9)||NULL||
                      chr(9)||NULL||
                      chr(9)||NULL;
          End If;
                      
          vExcel_Path := vExcel_Path||
                      chr(9)||vAccount_code||
                      chr(9)||xxcp_wks.working_rcd(j).i1009_array(52)||
                      chr(9)||xxcp_wks.working_rcd(j).trading_set||
                      chr(9)||vModel_name||
                      chr(9)||vRule_name||
                      chr(9)||vRule_type||
                      chr(9)||vRounding_group||
                      chr(9)||vRule_category_1||
                      chr(9)||vRule_category_2||
                      chr(9)||xxcp_wks.working_rcd(j).target_instance_id||
                      chr(9)||xxcp_wks.working_rcd(j).record_status||
                      chr(9)||vChange_sign||
                      chr(9)||xxcp_wks.working_rcd(j).I1009_array(36)||
                      chr(9)||xxcp_wks.working_rcd(j).I1009_array(35)||
                      chr(9)||xxcp_wks.working_rcd(j).D1001_array(3)||
                      chr(9)||xxcp_wks.working_rcd(j).D1001_array(4)||
                      chr(9)||xxcp_wks.working_rcd(j).D1001_array(5)||
                      chr(9)||xxcp_wks.working_rcd(j).D1001_array(8)||
                      chr(9)||xxcp_wks.working_rcd(j).i1009_array(145)||
                      chr(9)||
                      chr(9)||'="'||xxcp_wks.working_rcd(j).balancing_elements(1)||'"'||
                      chr(9)||'="'||xxcp_wks.working_rcd(j).balancing_elements(2)||'"'||
                      chr(9)||'="'||xxcp_wks.working_rcd(j).balancing_elements(3)||'"'||
                      chr(9)||'="'||xxcp_wks.working_rcd(j).balancing_elements(4)||'"'||
                      chr(9)||'="'||xxcp_wks.working_rcd(j).balancing_elements(5)||'"'||
                      chr(9)||'="'||xxcp_wks.working_rcd(j).balancing_elements(6)||'"'||
                      chr(9)||'="'||xxcp_wks.working_rcd(j).balancing_elements(7)||'"'||
                      chr(9)||'="'||xxcp_wks.working_rcd(j).balancing_elements(8)||'"'||
                      chr(9)||'="'||xxcp_wks.working_rcd(j).balancing_elements(9)||'"'||
                      chr(9)||'="'||xxcp_wks.working_rcd(j).balancing_elements(10)||'"'||
                      chr(9)||'="'||xxcp_wks.working_rcd(j).balancing_elements(11)||'"'||
                      chr(9)||'="'||xxcp_wks.working_rcd(j).balancing_elements(12)||'"'||
                      chr(9)||
                      chr(9)||'="'||xxcp_wks.working_rcd(j).I1009_array(11)||'"'||
                      chr(9)||'="'||xxcp_wks.working_rcd(j).I1009_array(12)||'"'||
                      chr(9)||'="'||xxcp_wks.working_rcd(j).I1009_array(13)||'"'||
                      chr(9)||'="'||xxcp_wks.working_rcd(j).I1009_array(14)||'"'||
                      chr(9)||'="'||xxcp_wks.working_rcd(j).I1009_array(15)||'"'||
                      chr(9)||'="'||xxcp_wks.working_rcd(j).I1009_array(16)||'"'||
                      chr(9)||'="'||xxcp_wks.working_rcd(j).I1009_array(17)||'"'||
                      chr(9)||'="'||xxcp_wks.working_rcd(j).I1009_array(18)||'"'||
                      chr(9)||'="'||xxcp_wks.working_rcd(j).I1009_array(19)||'"'||
                      chr(9)||'="'||xxcp_wks.working_rcd(j).I1009_array(20)||'"'||
                      chr(9)||'="'||xxcp_wks.working_rcd(j).I1009_array(41)||'"'||
                      chr(9)||'="'||xxcp_wks.working_rcd(j).I1009_array(42)||'"'||
                      chr(9)||'="'||xxcp_wks.working_rcd(j).I1009_array(43)||'"'||
                      chr(9)||'="'||xxcp_wks.working_rcd(j).I1009_array(44)||'"'||
                      chr(9)||'="'||xxcp_wks.working_rcd(j).I1009_array(45)||'"'||
                      chr(9)||
                      chr(9)||xxcp_wks.working_rcd(j).i1009_array(71)||
                      chr(9)||xxcp_wks.working_rcd(j).i1009_array(72)||
                      chr(9)||xxcp_wks.working_rcd(j).i1009_array(73)||
                      chr(9)||xxcp_wks.working_rcd(j).i1009_array(74)||
                      chr(9)||xxcp_wks.working_rcd(j).i1009_array(75)||
                      chr(9)||xxcp_wks.working_rcd(j).i1009_array(76)||
                      chr(9)||xxcp_wks.working_rcd(j).i1009_array(77)||
                      chr(9)||xxcp_wks.working_rcd(j).i1009_array(78)||
                      chr(9)||xxcp_wks.working_rcd(j).i1009_array(79)||
                      chr(9)||xxcp_wks.working_rcd(j).i1009_array(80);
                      
                      If j < cFE_cnt then
                        vExcel_Path := vExcel_Path||Chr(10);
                      End If;

            -- Exel output (28K Limit)
            If Length(vExcel_Path) > (28*1024) then
              e1 := e1 + 1;
              vSE := ' ('||to_char(e1)||')';
              xxcp_foundation.fndwriteError(vInternalErrorCode,vMessage||vSE,vExcel_Path);
              vExcel_Path := Null;
            End If;

       End Loop;
 

       -- Tidy up Excel output
       If  vExcel_Path is not null then
         e1 := e1 + 1;
         vSE := ' ('||to_char(e1)||')';

         xxcp_foundation.fndwriteError(vInternalErrorCode,vMessage||vSE,vExcel_Path);
         vExcel_Path  := Null;
         e1 := 0;
       End If;
    End If;

  End DumpTransactionPath;

 --  !! ***********************************************************************************************************
 --  !!
 --  !!                                   Account_Rounding
 --  !!     Make sure that the transaction balances and apply Rounding Tolerances
 --  !!
 -- !! ***********************************************************************************************************
 PROCEDURE Account_Rounding(
                           cGlobal_Rounding_Tolerance in number
                          ,cGlobalPrecision           in number
                          ,cTransaction_Type          in varchar2
                          ,cEntered_DR               out number
                          ,cEntered_CR               out Number
                          ,cInternalErrorCode     in out number) IS

    k                     integer := 0;
    w                     integer := 0;
    fe                    number  := xxcp_wks.WORKING_CNT;
    j                     Integer := 0;
    Cy                    Integer := 0;
    bl                    pls_integer := 0;
    vPrecision            pls_integer := 2;
    vRow_Found            Boolean := False;

    vEntered_dr_cr          xxcp_process_history.entered_dr%type := 0;
    vAccounted_dr_cr        xxcp_process_history.entered_dr%type := 0;

    vRoundingAmountEnt       xxcp_process_history.entered_rounding_amount%type := 0;
    vRoundingAmount          xxcp_process_history.entered_rounding_amount%type := 0;

    vEntered_Tolerance_Amt   xxcp_rounding_tolerances.entered_tolerence%type := 0;
    vAccounted_Tolerance_Amt xxcp_rounding_tolerances.entered_tolerence%type := 0;
    vEntered_DR              xxcp_process_history.entered_dr%type := 0;
    vEntered_CR              xxcp_process_history.entered_cr%type := 0;
    vSource_Name             xxcp_gl_interface.user_je_source_name%type;
    vCategory_Name           xxcp_gl_interface.user_je_category_name%type;
    vCurrency_Code           xxcp_gl_interface.currency_code%type;
    vEntered_Currency        xxcp_gl_interface.currency_code%type;
    vGroup_id                xxcp_process_history.batch_number%type;
    vTarget_Assignment_id    xxcp_target_assignments.Target_Assignment_id%type;

    vEngine_Path             xxcp_errors.long_message%type;
    vRule_id                 xxcp_account_rules.rule_id%type;

    vHoldErrorCode           xxcp_errors.internal_error_code%type;
    vTolerance_Check         number := 0;
    -- Default
    vEnt_Pointer             number := 21;
    vAcc_Pointer             number := 23; 
    vEntered_Amounts         varchar2(1) := 'N';
    vAccounted_Amounts       varchar2(1) := 'N';

 Begin

   xxcp_wks.BALANCING_CNT := 0;
   gDumpCnt := 0;

   -- Check to see if Entered_DR or Accounted_DR fields are used.
   --AnyValueUsed(vEntered_Used, vAccounted_Used);

   If fe > 0 then -- switch
     vEntered_Amounts   := xxcp_wks.WORKING_RCD(1).Entered_Amounts;
     vAccounted_Amounts := xxcp_wks.WORKING_RCD(1).Accounted_Amounts;
   End If;
   
   -- For each record ...
   For j in 1 .. fe Loop

    If xxcp_wks.WORKING_RCD(j).Target_Rounding = 'Y' and  (xxcp_wks.WORKING_RCD(j).Target_Suppression_Rule = 'N' or xxcp_wks.WORKING_RCD(j).Round_Suppressed_Rules = 'Y') then

       vEntered_dr_cr   := nvl(xxcp_wks.WORKING_RCD(j).Entered_Amount, 0);
       vAccounted_dr_cr := nvl(xxcp_wks.WORKING_RCD(j).Accounted_Amount, 0);
       vRule_id          := xxcp_wks.WORKING_RCD(j).Rule_id;

       vRow_Found := False;

      -- Balancing Elements
      -- 01 : Target Assignment id
      -- 02 : User Je Source Name
      -- 03 : Group Id
      -- 04 : User Je Category Name
      -- 05 : Currency Code
      -- 06 : Not Used
      -- 07 : Not Used
      -- 08 : Not Used
      -- 09 : Target Instance id
      -- 10 : Entered Currency
      -- 11 : Balancing Segment
      -- 12 : Rounding Group

        If upper(xxcp_wks.WORKING_RCD(j).balancing_elements(4)) = 'CROSS CURRENCY' then
          vEntered_dr_cr   := 0;
          vAccounted_dr_cr := 0;  -- 02.06.06a
        End If;

        For k in 1 .. bl loop
            If nvl(xxcp_wks.BALANCING_RCD(k).balancing_element1, '~#~')   =  nvl(xxcp_wks.WORKING_RCD(j).balancing_elements(1), '~#~') and
               nvl(xxcp_wks.BALANCING_RCD(k).balancing_element2, '~#~')   =  nvl(xxcp_wks.WORKING_RCD(j).balancing_elements(2), '~#~') and
               nvl(xxcp_wks.BALANCING_RCD(k).balancing_element3, '~#~')   =  nvl(xxcp_wks.WORKING_RCD(j).balancing_elements(3), '~#~') and
               nvl(xxcp_wks.BALANCING_RCD(k).balancing_element4, '~#~')   =  nvl(xxcp_wks.WORKING_RCD(j).balancing_elements(4), '~#~') and
               nvl(xxcp_wks.BALANCING_RCD(k).balancing_element5, '~#~')   =  nvl(xxcp_wks.WORKING_RCD(j).balancing_elements(5), '~#~') and
               nvl(xxcp_wks.BALANCING_RCD(k).balancing_element9, '~#~')   =  nvl(xxcp_wks.WORKING_RCD(j).balancing_elements(9), '~#~') and
               nvl(xxcp_wks.BALANCING_RCD(k).balancing_element10,'~#~')   =  nvl(xxcp_wks.WORKING_RCD(j).balancing_elements(10),'~#~') and
               nvl(xxcp_wks.BALANCING_RCD(k).balancing_element11, '~#~')  =  nvl(xxcp_wks.WORKING_RCD(j).balancing_elements(11), '~#~') and
               nvl(xxcp_wks.BALANCING_RCD(k).balancing_element12, '~#~')  =  nvl(xxcp_wks.WORKING_RCD(j).balancing_elements(12), '~#~') and
               nvl(xxcp_wks.BALANCING_RCD(k).Target_Type_id, 0)           =  nvl(xxcp_wks.WORKING_RCD(j).Target_Type_id, 0) and
               nvl(xxcp_wks.BALANCING_RCD(k).Target_Record_Status, '~#~') =  nvl(xxcp_wks.WORKING_RCD(j).Record_Status, '~#~')
               then

              xxcp_wks.BALANCING_CNT := xxcp_wks.BALANCING_CNT  + 1;
              xxcp_wks.BALANCING_RCD(k).record_count := xxcp_wks.BALANCING_RCD(k).record_count +1;

              If vAccounted_dr_cr <> 0 then
                 -- Accounted Amount
                 xxcp_wks.BALANCING_RCD(k).Accounted_sum := xxcp_wks.BALANCING_RCD(k).Accounted_sum + vAccounted_dr_cr;

                 If ABS(vAccounted_dr_cr) < abs(xxcp_wks.BALANCING_RCD(k).Lowest_Accounted_Value) AND
                   xxcp_wks.WORKING_RCD(j).Record_type <> 'L' then
                      xxcp_wks.BALANCING_RCD(k).Lowest_Accounted_Pointer := j;
                      xxcp_wks.BALANCING_RCD(k).Lowest_Accounted_Value   := vAccounted_dr_cr;
                 End If;
              End If;

              If vEntered_dr_cr <> 0 then
                 -- Entered Amount
                 xxcp_wks.BALANCING_RCD(k).Entered_sum   := xxcp_wks.BALANCING_RCD(k).Entered_sum + vEntered_dr_cr;
                 If ABS(vEntered_dr_cr) < abs(xxcp_wks.BALANCING_RCD(k).Lowest_Entered_Value) AND
                   xxcp_wks.WORKING_RCD(j).Record_type <> 'L' then
                      xxcp_wks.BALANCING_RCD(k).Lowest_Entered_Pointer := j;
                      xxcp_wks.BALANCING_RCD(k).Lowest_Entered_Value   := vEntered_dr_cr;
                 End If;
              End If;

              If xxcp_wks.WORKING_RCD(j).No_Rounding_Rule = 'N' and xxcp_wks.WORKING_RCD(j).Record_type <> 'L' then
                 -- Accounted
                 If ABS(vAccounted_dr_cr) > abs(xxcp_wks.BALANCING_RCD(k).Highest_Accounted_Value) then
                   xxcp_wks.BALANCING_RCD(k).Highest_Accounted_Pointer := j;
                   xxcp_wks.BALANCING_RCD(k).Highest_Accounted_Value := vAccounted_dr_cr;
                 End If;
                 -- Entered Amount
                 If ABS(vEntered_dr_cr) > abs(xxcp_wks.BALANCING_RCD(k).Highest_Entered_Value) then
                     xxcp_wks.BALANCING_RCD(k).Highest_Entered_Pointer := j;
                     xxcp_wks.BALANCING_RCD(k).Highest_Entered_Value := vEntered_dr_cr;
                 End If;
              End If;
              vRow_Found := True;
              Exit;
            End If;
        End Loop;

        -- New group
        If vRow_Found = False then
          bl := bl + 1;
          xxcp_wks.BALANCING_RCD(bl).balancing_element1 := xxcp_wks.WORKING_RCD(j).balancing_elements(1);
          xxcp_wks.BALANCING_RCD(bl).balancing_element2 := xxcp_wks.WORKING_RCD(j).balancing_elements(2);
          xxcp_wks.BALANCING_RCD(bl).balancing_element3 := xxcp_wks.WORKING_RCD(j).balancing_elements(3);
          xxcp_wks.BALANCING_RCD(bl).balancing_element4 := xxcp_wks.WORKING_RCD(j).balancing_elements(4);
          xxcp_wks.BALANCING_RCD(bl).balancing_element5 := xxcp_wks.WORKING_RCD(j).balancing_elements(5);
          xxcp_wks.BALANCING_RCD(bl).balancing_element6 := xxcp_wks.WORKING_RCD(j).balancing_elements(6);
          xxcp_wks.BALANCING_RCD(bl).balancing_element7 := xxcp_wks.WORKING_RCD(j).balancing_elements(7);
          xxcp_wks.BALANCING_RCD(bl).balancing_element8 := xxcp_wks.WORKING_RCD(j).balancing_elements(8);
          xxcp_wks.BALANCING_RCD(bl).balancing_element9 := xxcp_wks.WORKING_RCD(j).balancing_elements(9);
          xxcp_wks.BALANCING_RCD(bl).balancing_element10:= xxcp_wks.WORKING_RCD(j).balancing_elements(10);
          xxcp_wks.BALANCING_RCD(bl).balancing_element11:= xxcp_wks.WORKING_RCD(j).balancing_elements(11);
          xxcp_wks.BALANCING_RCD(bl).balancing_element12:= xxcp_wks.WORKING_RCD(j).balancing_elements(12);
          -- New
          xxcp_wks.BALANCING_RCD(bl).Target_Type_id      := xxcp_wks.WORKING_RCD(j).Target_Type_id;
          xxcp_wks.BALANCING_RCD(bl).Target_Record_Status:= xxcp_wks.WORKING_RCD(j).Record_Status;
          --
          --
          xxcp_wks.BALANCING_RCD(bl).Accounted_sum := vAccounted_dr_cr;
          xxcp_wks.BALANCING_RCD(bl).Entered_sum   := vEntered_dr_cr;
          xxcp_wks.BALANCING_RCD(bl).Highest_Accounted_Pointer := 0;
          xxcp_wks.BALANCING_RCD(bl).Highest_Accounted_Value   := 0;
          xxcp_wks.BALANCING_RCD(bl).Highest_Entered_Pointer   := 0;
          xxcp_wks.BALANCING_RCD(bl).Highest_Entered_Value     := 0;
          --
          -- Do not do rounding if the flag is set.
          If xxcp_wks.WORKING_RCD(j).No_Rounding_Rule <> 'Y' then
            xxcp_wks.BALANCING_RCD(bl).Highest_Accounted_Pointer := j;
            xxcp_wks.BALANCING_RCD(bl).Highest_Accounted_Value   := vAccounted_dr_cr;
            xxcp_wks.BALANCING_RCD(bl).Highest_Entered_Pointer   := j;
            xxcp_wks.BALANCING_RCD(bl).Highest_Entered_Value     := vEntered_dr_cr;
          End If;

          xxcp_wks.BALANCING_RCD(bl).Lowest_Accounted_Pointer := j;
          xxcp_wks.BALANCING_RCD(bl).Lowest_Accounted_Value   := vAccounted_dr_cr;
          xxcp_wks.BALANCING_RCD(bl).Lowest_Entered_Pointer   := j;
          xxcp_wks.BALANCING_RCD(bl).Lowest_Entered_Value     := vEntered_dr_cr;

          xxcp_wks.BALANCING_CNT := 1;
          xxcp_wks.BALANCING_RCD(bl).record_count := 1;
        End If;

     End If; -- New
    End Loop;

    -- !!************************************************************************
    -- !! Entered Amount Rounding/Tolerance
    --!! ************************************************************************

    For k in 1 .. xxcp_wks.BALANCING_RCD.count Loop

        -- ENTERED BALANCING
        If (nvl(xxcp_wks.BALANCING_RCD(k).Entered_sum, 0) <> 0 and vEntered_Amounts   = 'Y') then
          vCurrency_Code := xxcp_wks.BALANCING_RCD(k).balancing_element10; -- Entered Currency

          -- Tolerance Calculation
          vPrecision := xxcp_foundation.Get_CurrencyPrecision_memory(vCurrency_Code, cGlobalPrecision, 'N');

          If vPrecision is null then
            cInternalErrorCode :=  12757;
            XXCP_FOUNDATION.FndWriteError(cInternalErrorCode,'Balancing Entered Currency Code <'||vCurrency_Code||'>');
            Exit; -- Get out of loop
          End If;

          -- Look for any Tolerance override.
          vEntered_Tolerance_Amt := Null;
          For Cy in 1 .. xxcp_global.gCY_Cnt Loop
            If vCurrency_Code = xxcp_global.gCY(cy).Currency_Code then
              vEntered_Tolerance_Amt := xxcp_global.gCY(Cy).Entered_Tolerence;
              Exit;
            End If;
          End loop;

          If vEntered_Tolerance_Amt is null then
            -- Get the Global if there is not specific tolerance
            Select to_number(Decode(vPrecision,0,1,'.' || lpad(1, vPrecision, '0'))) *
                   cGlobal_Rounding_Tolerance
              into vEntered_Tolerance_Amt
              from dual;
          End If;

          If ABS(nvl(xxcp_wks.BALANCING_RCD(k).Entered_sum, 0)) > Round(vEntered_Tolerance_Amt,2) then
            cInternalErrorCode := 12751; -- Value not within Tolerance
            vTarget_Assignment_id := xxcp_wks.BALANCING_RCD(k).balancing_element1;
            vSource_Name          := xxcp_wks.BALANCING_RCD(k).balancing_element2;
            vGroup_id             := xxcp_wks.BALANCING_RCD(k).balancing_element3;
            vCategory_Name        := xxcp_wks.BALANCING_RCD(k).balancing_element4;

            XXCP_TE_BASE.DumpTransactionPath(xxcp_wks.WORKING_CNT,cInternalErrorCode);

            XXCP_FOUNDATION.FndWriteError(cInternalErrorCode,
                                          'Entered Rounding <' ||to_char(xxcp_wks.BALANCING_RCD(k).Entered_sum) ||
                                          '> Tolerance <' ||to_char(vEntered_Tolerance_Amt) ||  '> Currency <' || vCurrency_Code || '> Source <' || vSource_Name ||
                                          '> Category <' || vCategory_Name || '> Group id <' ||to_char(vGroup_id) ||
                                          '> Set of books Id <' ||to_char(vTarget_Assignment_id) || '> ');
            Exit; -- Get out of Loop
          Else

            If xxcp_wks.BALANCING_RCD(k).Highest_Entered_Pointer > 0 then
                w := xxcp_wks.BALANCING_RCD(k).Highest_Entered_Pointer;
            Else
                w := xxcp_wks.BALANCING_RCD(k).Lowest_Entered_Pointer;
            End If;
            
            If xxcp_wks.WORKING_RCD(w).extended_values = 'N' then
              vEnt_Pointer := 81;
              vAcc_Pointer := 83;
            End If;
              
            vAccounted_Amounts  := xxcp_wks.WORKING_RCD(w).Accounted_Amounts;
            
            vRoundingAmountEnt := xxcp_wks.BALANCING_RCD(k).Entered_sum;

            xxcp_wks.WORKING_RCD(w).Entered_Amount  := nvl(xxcp_wks.WORKING_RCD(w).Entered_Amount, 0) - vRoundingAmountEnt;
            xxcp_wks.WORKING_RCD(w).I1009_Array(vEnt_Pointer) := xxcp_wks.WORKING_RCD(w).Entered_Amount;
            xxcp_wks.WORKING_RCD(w).Entered_Rnd_Amount := vRoundingAmountEnt;

            If xxcp_global.Trace_on = 'Y' then
              xxcp_foundation.FndWriteError(100,
                  'TRACE (Ent Rnd Amt) : New Entered Amt <' ||to_char(nvl(xxcp_wks.WORKING_RCD(w).Entered_Amount,0)) ||
                  '> Rnd Amt <' ||to_char(vRoundingAmountEnt) || '> Target Assignment <'||to_char(vTarget_Assignment_id)||'>');
            End If;
          End If;
        End If;

        -- ACCOUNTED RND
        If (nvl(xxcp_wks.BALANCING_RCD(k).Accounted_sum, 0) <> 0 and vAccounted_Amounts = 'Y') then

          vCurrency_Code := xxcp_wks.BALANCING_RCD(k).balancing_element5;

          -- Tolerance Calculation
          vPrecision := xxcp_foundation.Get_CurrencyPrecision_memory(vCurrency_Code, cGlobalPrecision,'N');

          If vPrecision is null then
            cInternalErrorCode :=  12758;
            XXCP_FOUNDATION.FndWriteError(cInternalErrorCode,'Balancing Accounted Currency Code <'||vCurrency_Code||'>');
            Exit; -- Get out of loop
          End If;

          -- Look for any Tolerance override.
          vAccounted_Tolerance_Amt := Null;
          For Cy in 1 .. xxcp_global.gCY_Cnt Loop
            If vCurrency_Code = xxcp_global.gCY(cy).Currency_Code then
              vAccounted_Tolerance_Amt := xxcp_global.gCY(Cy).Accounted_Tolerence;
              Exit;
            End If;
          End loop;

          If vAccounted_Tolerance_Amt is null then
            -- Get the Global if there is not specific tolerance
            Select to_number(Decode(vPrecision,0,1,'.' || lpad(1, vPrecision, '0'))) *
                   cGlobal_Rounding_Tolerance
              into vAccounted_Tolerance_Amt
              from dual;
          End If;

          If abs(nvl(xxcp_wks.BALANCING_RCD(k).Accounted_sum, 0)) > vAccounted_Tolerance_Amt then

            cInternalErrorCode := 12750; -- Value not within Tolerance

            vTarget_Assignment_id := xxcp_wks.BALANCING_RCD(k).balancing_element1;
            vSource_Name          := xxcp_wks.BALANCING_RCD(k).balancing_element2;
            vGroup_id             := xxcp_wks.BALANCING_RCD(k).balancing_element3;
            vCategory_Name        := xxcp_wks.BALANCING_RCD(k).balancing_element4;

            XXCP_TE_BASE.DumpTransactionPath(xxcp_wks.WORKING_CNT,cInternalErrorCode);

            XXCP_FOUNDATION.FndWriteError(cInternalErrorCode,
                                          'Accounted Rounding <' ||to_char(xxcp_wks.BALANCING_RCD(k).Accounted_sum) ||
                                          '> Tolerance <' ||to_char(vAccounted_Tolerance_Amt) || '> Currency <' || vCurrency_Code ||
                                          '> Source <' || vSource_Name || '> Category <' || vCategory_Name ||
                                          '> Group id <' ||to_char(vGroup_id) || '> Set of books Id <' ||to_char(vTarget_Assignment_id) || '> ');
            Exit; -- Get out of Loop

          Else

            If xxcp_wks.BALANCING_RCD(k).Highest_Accounted_Pointer > 0 then
              w := xxcp_wks.BALANCING_RCD(k).Highest_Accounted_Pointer;
            Else
              w := xxcp_wks.BALANCING_RCD(k).Lowest_Accounted_Pointer;
            End If;

            If xxcp_wks.WORKING_RCD(w).Extended_values = 'N' then
              vEnt_Pointer := 81;
              vAcc_Pointer := 83;
            End If;
              
            vAccounted_Amounts := xxcp_wks.WORKING_RCD(w).Accounted_Amounts;
            vRoundingAmount  := xxcp_wks.BALANCING_RCD(k).Accounted_sum;

            xxcp_wks.WORKING_RCD(w).Accounted_Amount          := nvl(xxcp_wks.WORKING_RCD(w).Accounted_Amount, 0) - vRoundingAmount;
            xxcp_wks.WORKING_RCD(w).Accounted_Rnd_Amount      := vRoundingAmount;
            xxcp_wks.WORKING_RCD(w).I1009_Array(vAcc_Pointer) := xxcp_wks.WORKING_RCD(w).Accounted_Amount;

            If xxcp_global.Trace_on = 'Y' then
              xxcp_foundation.FndWriteError(100,
                  'TRACE (Rnd Amt) : New Accnt Amt <' ||to_char(nvl(xxcp_wks.WORKING_RCD(w).Accounted_Amount,0)) ||
                  '> Rnd Amt <' ||to_char(vRoundingAmount) || '> Target Assignment <'||to_char(vTarget_Assignment_id)||'>');
            End If;
          End If;
        End If;

    End Loop;

    If cInternalErrorCode <> 0 then
        vEngine_Path := Null;
    End If;

    --
    -- Balancing Groups Error output
    --
    If (cInternalErrorCode > 0 or gBalancing_Trace = 'Y') and bl > 0 then

        For k in 1 .. bl Loop
          vEngine_Path := vEngine_Path || lpad(k, 4, 0) ||' Set of Books ID       <' || xxcp_wks.BALANCING_RCD(k).balancing_element1 || '>' || chr(10);
          vEngine_Path := vEngine_Path || lpad(k, 4, 0) ||' Source Name           <' || xxcp_wks.BALANCING_RCD(k).balancing_element2 || '>' || chr(10);
          vEngine_Path := vEngine_Path || lpad(k, 4, 0) ||' Group Id              <' || xxcp_wks.BALANCING_RCD(k).balancing_element3 || '>' || chr(10);
          vEngine_Path := vEngine_Path || lpad(k, 4, 0) ||' Category Name         <' || xxcp_wks.BALANCING_RCD(k).balancing_element4 || '>' || chr(10);
          vEngine_Path := vEngine_Path || lpad(k, 4, 0) ||' Accounted Currency    <' || xxcp_wks.BALANCING_RCD(k).balancing_element5 || '>' || chr(10);
          vEngine_Path := vEngine_Path || lpad(k, 4, 0) ||' Reference 1           <' || xxcp_wks.BALANCING_RCD(k).balancing_element6 || '>' || chr(10);
          vEngine_Path := vEngine_Path || lpad(k, 4, 0) ||' Instance ID           <' || xxcp_wks.BALANCING_RCD(k).balancing_element9 || '>' || chr(10);
          vEngine_Path := vEngine_Path || lpad(k, 4, 0) ||' Entered Currency      <' || xxcp_wks.BALANCING_RCD(k).balancing_element10 || '>' || chr(10);
          vEngine_Path := vEngine_Path || lpad(k, 4, 0) ||' Balancing Segment     <' || xxcp_wks.BALANCING_RCD(k).balancing_element11 || '>' || chr(10);
          vEngine_Path := vEngine_Path || lpad(k, 4, 0) ||' Rounding Group        <' || xxcp_wks.BALANCING_RCD(k).balancing_element12 || '>' || chr(10);
          vEngine_Path := vEngine_Path || lpad(k, 4, 0) ||' Target Type ID        <' || xxcp_wks.BALANCING_RCD(k).Target_Type_id || '>' || chr(10);
          vEngine_Path := vEngine_Path || lpad(k, 4, 0) ||' Target Table Code     <' || xxcp_wks.BALANCING_RCD(k).Target_Record_Status || '>' || chr(10);
          vEngine_Path := vEngine_Path || lpad(k, 4, 0) ||' Entered Amount Sum    <' || xxcp_wks.BALANCING_RCD(k).Entered_sum || '>' || chr(10);
          vEngine_Path := vEngine_Path || lpad(k, 4, 0) ||' Highest Entered Trx   <' || xxcp_wks.BALANCING_RCD(k).Highest_Entered_Value || '>' || chr(10);
          vEngine_Path := vEngine_Path || lpad(k, 4, 0) ||' Accounted Amount Sum  <' || xxcp_wks.BALANCING_RCD(k).Accounted_sum || '>' || chr(10);
          vEngine_Path := vEngine_Path || lpad(k, 4, 0) ||' Highest Accounted Trx <' || xxcp_wks.BALANCING_RCD(k).Highest_Accounted_Value || '>' || chr(10) ;
          vEngine_Path := vEngine_Path || lpad(k, 4, 0) ||' Records in group      <' || to_char(xxcp_wks.BALANCING_RCD(k).Record_Count) || '>' || chr(10) || chr(10);
        End Loop;

        IF vEngine_Path IS NOT NULL THEN
          xxcp_foundation.FndWriteError(cInternalErrorCode,'Balancing Groups', vEngine_Path);
        END IF;

    End If;

    -- Transaction Path output
    If xxcp_global.Preview_on = 'Y' then
      If (xxcp_global.trx_path = 'Y' and gDumpCnt = 0) then -- Dont do it if it has already been done
          vHoldErrorCode :=  cInternalErrorCode; -- switch error codes (temp)
          cInternalErrorCode := 100;
          xxcp_te_base.DumpTransactionPath(xxcp_wks.WORKING_CNT,cInternalErrorCode);
          IF vEngine_Path IS NOT NULL THEN
            xxcp_foundation.FndWriteError(100,'TRACE (Balancing Groups)', vEngine_Path);
          END IF;
          cInternalErrorCode := vHoldErrorCode; -- Put error code back
       End If;
    End If;

    gDumpCnt := 0;

  End Account_Rounding;

 
 -- !! ***********************************************************************************************************
 -- !!
 -- !!                                      Zero_Suppression_Rule 
 -- !!                 This funciton works out if zero account records should be written or not
 -- !!
 -- !! ***********************************************************************************************************
 Function Zero_Suppression_Rule(j in number) return Number is

    vZero_Action Number := 3;
   
    -- E = Entity - Do what Target Assignments say to do
    -- N = None - Do not put out zero records
    -- H = Only put zeros out to history
    -- T = Always put out zeros.
   
   begin

        xxcp_global.gCommon(1).current_source_rowid := xxcp_wks.Source_rowid(xxcp_wks.WORKING_RCD(j).Source_Pos);
        -- Find the Action to take for Zero Accounted values  
        
        IF (nvl(xxcp_wks.WORKING_RCD(j).Entered_Amount, 0) + nvl(xxcp_wks.WORKING_RCD(j).Accounted_Amount, 0)) <> 0 then
            vZero_Action := 3; -- Write Process History and Transaction
        Else 
            -- Target
            If xxcp_wks.WORKING_RCD(j).zero_suppression_rule = 'T' then
               vZero_Action := 3;
            -- None suppress if zero 
            Else 
            
            If xxcp_wks.WORKING_RCD(j).zero_suppression_rule = 'N' then
            -- History  suppress if zero 
               If xxcp_global.gCOMMON(1).preview_zero_flag = 'Y' then
                 vZero_Action := 2;
                 xxcp_wks.SOURCE_SUB_CODE(xxcp_wks.WORKING_RCD(j).Source_Pos):= 7011;
               Else
                 vZero_Action := 0;
               End If;
            ElsIf xxcp_wks.WORKING_RCD(j).zero_suppression_rule = 'H' then
              vZero_Action := 2;
              xxcp_wks.SOURCE_SUB_CODE(xxcp_wks.WORKING_RCD(j).Source_Pos):= 7011;            
            Else  -- Entity 
            
             -- Use Normal Top level rules
             If (xxcp_wks.WORKING_RCD(j).Target_Zero_Flag = 'N') then
               vZero_Action := 0;
              ElsIf (xxcp_wks.WORKING_RCD(j).Target_Zero_Flag = 'H') then
               vZero_Action := 2;
               xxcp_wks.SOURCE_SUB_CODE(xxcp_wks.WORKING_RCD(j).Source_Pos):= 7011;
              ElsIf (xxcp_wks.WORKING_RCD(j).Target_Zero_Flag = 'T') then
               vZero_Action := 3;
              End If;
            
            End If;
            
            If xxcp_global.gCOMMON(1).preview_zero_flag = 'Y' and vZero_Action < 2 then
              vZero_Action := 2;
              xxcp_wks.SOURCE_SUB_CODE(xxcp_wks.WORKING_RCD(j).Source_Pos):= 7011;            
            End If;
          End If; 
        End If; 
            
        Return(vZero_Action);
  End Zero_Suppression_Rule;
  
 -- !! *******************************************
 -- !!                 Create_Lookup
 -- !! *******************************************
 Procedure Create_Lookup(cSabrix in varchar2 default 'N') is

   vCnt number;
 
  Begin
  
    If cSabrix = 'Y' then
    
       Select count(*) into vCnt
         from xxcp_lookups
         where lookup_type = 'SABRIX TAX ENGINE';
         
         If vCnt = 0 then
           Insert into xxcp_lookups(
              lookup_type,
              lookup_code, 
              numeric_code, 
              creation_date, 
              meaning, 
              enabled_flag)
           values('SABRIX TAX ENGINE'
                  ,'Y' -- default to on so that not mistakes happen.
                  ,0
                  ,sysdate,
                  'Sabrix Tax Engine','Y');
                  
        End If;
   End If;
  End Create_Lookup;
  
  --  Get_Engine_Dates
  Procedure Get_Engine_Dates(cStart_Partition_Date       out date, 
                             cEnd_Partition_Date         out date,
                             cRemove_Filtered_Date       out date,
                             cIgnored_Start_Removal_Date out date,
                             cIgnored_End_Removal_Date   out date,
                             cRequest_Id_Range           out number
                             ) is

    Cursor SP(pProfile_Category in varchar2, pProfile_Name in varchar2) is
      select p.profile_value
        from xxcp_sys_profile p
       where p.profile_name = pProfile_Name
         and p.profile_category = pProfile_Category;  
         
    vMonths                     xxcp_sys_profile.profile_value%type;       
 
    vPartition_Months_start     number := -6; -- This should be a negative number
    vPartition_Months_end       number := 1;  -- This should be a positive number
 
    vStart_Partition_Date       date;
    vEnd_Partition_Date         date := Sysdate;
    vRemove_Filtered_Date       date := Sysdate-40;
    vRequest_Id_Range           number := 1000;
    vIgnored_Removal_Flag       Varchar2(1) := 'Y';
    vIgnored_Delete_Months      Number := -12; -- This should be a negative number
    vIgnored_Start_Removal_Date date;
    vIgnored_End_Removal_Date   date := trunc(Add_Months(Sysdate,vIgnored_Delete_Months)); 
    
  Begin    
  
    -- Partition Months Start
    For Rec in SP('EV','PARTITIONING MONTHS START') loop
       vMonths := Rec.profile_value;
       vPartition_Months_start := ABS(to_number(vMonths))*-1;
    End Loop;   
    
    -- 03.06.13
    -- Partition Months End
    For Rec in SP('EV','PARTITIONING MONTHS END') loop
       vMonths := Rec.profile_value;
       vPartition_Months_end := ABS(to_number(vMonths));
    End Loop;   

    -- Ignored Status Months
    For Rec in SP('EV','IGNORED STATUS REMOVAL MONTHS') loop
       vMonths := Rec.profile_value;
       vIgnored_Delete_Months := ABS(to_number(vMonths))*-1;
    End Loop;   
    
    -- Ignored Removal Flag
    For Rec in SP('EV','IGNORED STATUS REMOVAL ON') loop
       vIgnored_Removal_Flag := Rec.profile_value; 
    End Loop;    

    -- Previous Request Id Range 
    For Rec in SP('EV','INTERFACE VT REQUEST ID RANGE') loop
       vRequest_Id_Range := Rec.profile_value; 
    End Loop;   
    
    -- Partition Months Start
    For Rec in SP('EV','PARTITIONING MONTHS START') loop
     vMonths := Rec.profile_value;
     vPartition_Months_start := ABS(to_number(vMonths))*-1;
    End Loop;   
    
    -- 03.06.13
    -- Partition Months End
    For Rec in SP('EV','PARTITIONING MONTHS END') loop
      vMonths := Rec.profile_value;
      vPartition_Months_end := ABS(to_number(vMonths));
    End Loop;  
    
    -- Remove Filtered Record Days
    For Rec in SP('EV','REMOVE FILTERED RECORD DAYS') loop
      If to_number(nvl(rec.profile_value,'0')) >= 0 then
        vRemove_Filtered_Date := sysdate-to_number(Rec.profile_value); 
      End If;
    End Loop;   
     
    cRequest_Id_Range           := vRequest_Id_Range;       
    cIgnored_Start_Removal_Date := vIgnored_Start_Removal_Date;
    cIgnored_End_Removal_Date   := vIgnored_End_Removal_Date;
    
    cRemove_Filtered_Date := vRemove_Filtered_Date;    
    cStart_Partition_Date := trunc(add_months(sysdate,vPartition_Months_start)); 
    cEnd_Partition_Date   := trunc(add_months(sysdate,vPartition_Months_end));  
       
  End Get_Engine_Dates;
  --
  --   !! ***********************************************************************************************************
  --   !!                                     Remove_Unwanted_Records
  --   !! ***********************************************************************************************************
  PROCEDURE Remove_Unwanted_Records(cSource_Group_id in number ) is
                                    
  vCmdStatement varchar2(10000);
  
  cursor c1(pSource_Group_id in number) is
    select s.source_base_table Interface_Table
      from xxcp_sys_sources s,
           xxcp_source_assignment_groups r
       where s.source_id       = r.source_id
         and r.source_group_id = pSource_Group_id
         and s.interfaced      = 'Y'; 
         
   Cursor SP(pProfile_Category in varchar2, pProfile_Name in varchar2) is
      select p.profile_value
        from xxcp_sys_profile p
       where p.profile_name = pProfile_Name
         and p.profile_category = pProfile_Category; 

   vStart_Partition_Date       date; 
   vEnd_Partition_Date         date;   
   vRemove_Filtered_Date       date;  
   vIgnored_Start_Removal_Date date;
   vIgnored_End_Removal_Date   date;
   
   vIgnored_Removal_Flag       varchar2(1) := 'Y';
   vRequest_Id_Range           number := 1000;
        
  Begin

    If nvl(xxcp_global.preview_on,'N') = 'N' then
      -- Get Engine Dates
      Get_Engine_Dates(cStart_Partition_Date       => vStart_Partition_Date, 
                       cEnd_Partition_Date         => vEnd_Partition_Date,
                       cRemove_Filtered_Date       => vRemove_Filtered_Date,
                       cIgnored_Start_Removal_Date => vIgnored_Start_Removal_Date,
                       cIgnored_End_Removal_Date   => vIgnored_End_Removal_Date,
                       cRequest_Id_Range           => vRequest_Id_Range
                       );
 
      -- Ignored Removal Flag
      For Rec in SP('EV','IGNORED STATUS REMOVAL ON') loop
       vIgnored_Removal_Flag := Rec.profile_value; 
      End Loop;  
              
      -- Filtered Records
      For Rec in c1(cSource_Group_id) Loop
        -- Ignored
        If vIgnored_Removal_Flag = 'Y' then
         Begin
          vCmdStatement :=
           'Delete from '||rec.Interface_Table||' gm '||chr(10)||
           '   where gm.vt_status = ''IGNORED''
               and gm.vt_transaction_date between :1 and :2 
               and gm.vt_request_id < :3
               and gm.vt_source_assignment_id = any(select source_assignment_id
                                                      from xxcp_source_assignments s
                                                      where s.source_group_id = :5)';
                                                
           Execute Immediate vCmdStatement 
             using vIgnored_Start_Removal_Date,
                   vIgnored_End_Removal_Date,
                   xxcp_global.gCommon(1).Current_request_id, 
                   cSource_Group_id;                                           
                                                
           Exception when no_data_found then Null;
                   when others then  xxcp_foundation.fndwriteerror(2322,'Delete Ignored Failed',vCmdStatement);                                       
           
          End;
         End if;
      
        -- Filtered
        Begin
         vCmdStatement :=
          'Delete from '||rec.Interface_Table||' gm '||chr(10)||
          '   where gm.vt_status = ''FILTERED''
               and gm.vt_transaction_date between :1 and :2
               and gm.vt_request_id < :3 
               and gm.vt_date_processed <= :4
               and gm.vt_source_assignment_id = any(select source_assignment_id
                                                 from xxcp_source_assignments s
                                                where s.source_group_id = :5)';
                                                
           Execute Immediate vCmdStatement 
             using vStart_Partition_Date,
                   vEnd_Partition_Date,
                   xxcp_global.gCommon(1).Current_request_id, 
                   vRemove_Filtered_Date, 
                   cSource_Group_id;                                           
                                                
           Exception when no_data_found then Null;
                   when others then  xxcp_foundation.fndwriteerror(2322,'Filter Delete Failed',vCmdStatement);                                       
        End;
      End Loop;
     End If;
  End Remove_Unwanted_Records;  

 -- !! ***********************************************************************************************************
 -- !!
 -- !!                                      Engine_Init
 -- !!                 This procedure reads data into memory read for use by the Engines
 -- !!
 -- !! ***********************************************************************************************************
  PROCEDURE Engine_Init (  cSource_id               in number
                          ,cSource_Group_id         in number
                          ,cInternalErrorCode   in out NOCOPY number) is


    -- This procedure is very important. You call this procedure from
    -- each transactional wrapper. The code loads the core setup tables
    -- into memory for performance purposes.

   Cursor c1(pUser_id in number) is
    select pv_trx_path
      from xxcp_user_profiles
     where user_id = pUser_id;

     Cursor c2 is
     Select Count(*) Compiled_Count
       from xxcp_custom_event_code c
      where nvl(c.Compilied_code,'N') = 'N';

     vCompile_Event_Code Boolean := True;
 
     
     Cursor c4(pSource_id in number) is
     select 'Y' CurrConvUsed
       from xxcp_dynamic_attribute_sets a,
            xxcp_sql_build b
     where b.source_id = pSource_id
       and b.SQL_BUILD_ID = a.ATTRIBUTE_SET_ID
       and ((upper(a.COLUMN_DEFINITION) like '%XXCP_GWU.CURRCONV%') OR (upper(a.COLUMN_DEFINITION) like '%XXCP_GATEWAY_UTILS.CURRCONV%'));
       
       
     Cursor c5(pSource_id in number) is
       select lookup_code Sabrix_tax, Enabled_flag
       from xxcp_lookups
       where lookup_type = 'SABRIX TAX ENGINE'
        and nvl(numeric_code,0) in (0,pSource_id);
        
    -- NCM 02.05.02 Added for default trading relationship change      
    Cursor c6 is
     select profile_value Default_trading_relationship
       from xxcp_sys_profile
      where Profile_Name = 'DEFAULT TRADING RELATIONSHIP'
        and profile_category = 'EV';    
        
     -- 02.06.02 Added to override Balancing Segment 11 (per source)        
    Cursor c7(pSource_id in number) is
     select 'Y' override
       from xxcp_sys_profile
      where Profile_Name = 'BALANCING SEGMENT 11 OVERRIDE'
        and profile_category = 'EV'
        and profile_value = pSource_id;   

     vSabrix_lookup varchar2(1) := 'N'; 
     
     -- 02.06.05 Custom Event Trace
     Cursor c8(pSource_id in number) is
       select w.timing_advanced_mode
        from xxcp_sys_sources w
        where source_id = pSource_id;
        
     -- 03.06.15
     Cursor c9 is
          select column_id
          from xxcp_dynamic_attributes
         where source_id = xxcp_global.gCommon(1).Source_id
            and column_name in
                (select Profile_Value
                                 from xxcp_sys_profile
                                where profile_name = 'CLASSIFICATION ATTRIBUTE'
                                  and profile_category = 'IS');          
             
     -- 03.06.16
     Cursor c10 is
         select column_id
           from xxcp_dynamic_attributes
          where source_id = xxcp_global.gCommon(1).Source_id
            and column_name in
                (select Profile_Value
                   from xxcp_sys_profile
                  where profile_name = 'UNMATCHED ATTRIBUTE'
                    and profile_category = 'MA');

Cursor c11 is
         select column_id
           from xxcp_dynamic_attributes
          where source_id = xxcp_global.gCommon(1).Source_id
            and column_name in
                (select Profile_Value
                   from xxcp_sys_profile
                  where profile_name = 'QUANTITY ATTRIBUTE'
                    and profile_category = 'MA');     
                    
     -- 03.06.18
     Cursor c12 is
         select column_id
           from xxcp_dynamic_attributes
          where source_id = xxcp_global.gCommon(1).Source_id
            and column_name in
                (select Profile_Value
                   from xxcp_sys_profile
                  where profile_name = 'APPROVE RESTRICTED ATTRIBUTE'
                    and profile_category = 'IS');      
   
     Cursor c13 (pProfile_Name in varchar2)is
          select column_id
          from xxcp_dynamic_attributes
         where source_id = xxcp_global.gCommon(1).Source_id
         and   column_name in (select Profile_Value
                                 from xxcp_sys_profile
                                where profile_name = pProfile_Name
                                  and profile_category = 'CP');          
          

        
   Begin

       xxcp_global.set_source_id(cSource_id); -- Set the source
       xxcp_te_base.Reset_Package;
       gJobStartTime := SysDate;
       -- Is Tranaction Path required
       xxcp_global.trx_path := 'N';
       For Rec in c1(xxcp_global.User_id)
       Loop
         xxcp_global.trx_path := rec.pv_trx_path;
       End Loop;

       -- Check to See if Custom Code is compiled
        For Rec in c2 loop
          If Rec.Compiled_Count > 0 then
             vCompile_Event_Code := False;
          End If;
        End Loop;

        If xxcp_global.gCommon(1).Cached_Attributes is null then
          cInternalErrorCode := 999;
        End If;
        --        
        -- Pass Through Control
        --
        InitPassThrough(cSource_id);

        -- Currency Conversion in Use 
        gCUCO_INUSE := 'N';
        For Rec in c4(cSource_id) loop
          gCUCO_INUSE := Rec.CurrConvUsed;
        End Loop;
        
        -- Set Sabrix Tax engine status
        Create_lookup(cSabrix => 'Y');
        
        For Rec in c5(cSource_id) loop 
         If Rec.Sabrix_tax = 'Y' and Rec.Enabled_Flag = 'Y' then
          gSabrix_Engine := 'Y';
         End If;        
        End Loop;
        
        -- NCM 02.05.02 Added for default trading relationship change    
        For Rec in C6 loop
          gDefault_trading_relationship := rec.default_trading_relationship;
        End Loop;         

        -- 02.06.02 Added for default trading relationship change    
        xxcp_global.gBalancing_Segment11_Override := 'N';
        For Rec in C7(cSource_id) loop
          xxcp_global.gBalancing_Segment11_Override := rec.override;
        End Loop;         
        
        -- 02.06.05 Additional Custom Event Trace when in Trace mode
        xxcp_global.Timing_Adv_Mode := 'N';
        If xxcp_global.Trace_on = 'Y' then
          For Rec in C8(cSource_id) loop
            xxcp_global.Timing_Adv_Mode := rec.timing_advanced_mode;
          End Loop;
        End If;        

        -- 03.06.15 
        For Rec in c9 loop
          gClassificationAttribute := rec.column_id;
        End Loop;

        -- 03.06.16
        For Rec in c10 loop
          gUnmatchedAttribute := rec.column_id;
        End Loop;         

        For Rec in c11 loop
          gQuantityAttribute := rec.column_id;
        End Loop;     
        
        -- 03.06.18
        For Rec in c12 loop
          gApproveRestrictedAttribute := rec.column_id;
        End Loop; 
        
        -- Only populate for Cost Plus Source
        If xxcp_global.gCommon(1).Source_id = 31 then
          for rec in c13('UPLIFT RATE ATTRIBUTE') loop
            gUpliftRateAttribute := rec.column_id;
          end loop;
          for rec in c13('GROWTH RATE ATTRIBUTE') loop
            gGrowthRateAttribute := rec.column_id;
          end loop;
          for rec in c13('AVG PERIOD COST ATTRIBUTE') loop
            gAvgPeriodCostAttribute := rec.column_id;
          end loop;
          for rec in c13('TOTAL COST ATTRIBUTE') loop
            gTotalCostAttribute := rec.column_id;
          end loop;
          for rec in c13('CONSIDERED PERIODS ATTRIBUTE') loop
            gConsideredPeriodsAttribute := rec.column_id;
          end loop;
          for rec in c13('CURRENCY CODE ATTRIBUTE') loop
            gCurrencyCodeAttribute := rec.column_id;
          end loop;
          for rec in c13('COST CATEGORY ID ATTRIBUTE') loop
            gCostCategoryIdAttribute := rec.column_id;
          end loop;
          for rec in c13('UPLIFTED AMOUNT ATTRIBUTE') loop
            gUpliftedAttribute := rec.column_id;
          end loop;
          for rec in c13('TRUE UP AMOUNT ATTRIBUTE') loop
            gTrueUpAttribute := rec.column_id;
          end loop;          
          for rec in c13('PERIODS REQUIRED ATTRIBUTE') loop
            gPeriodsRequiredAttribute := rec.column_id;
          end loop;
          for rec in c13('PAYER1 ATTRIBUTE') loop
            gPayer1Attribute := rec.column_id;
          end loop;
          for rec in c13('PAYER1 PERCENT ATTRIBUTE') loop
            gPayer1PercentAttribute := rec.column_id;
          end loop;
          for rec in c13('PAYER2 ATTRIBUTE') loop
            gPayer2Attribute := rec.column_id;
          end loop;
          for rec in c13('PAYER2 PERCENT ATTRIBUTE') loop
            gPayer2PercentAttribute := rec.column_id;
          end loop;
          for rec in c13('INTERMEDIATE PAYER ATTRIBUTE') loop
            gIntermediatePayerAttribute := rec.column_id;
          end loop;
          for rec in c13('ACCOUNT ATTRIBUTE') loop
            gAccountAttribute := rec.column_id;
          end loop;
          
        End if;
            
        
        Set_Master_Trading_Set(cSource_id);

        -- Set up Working Storage
        xxcp_wks.init;
        -- Preload memory tables
        xxcp_global.gCR_CNT := 0;
        xxcp_memory_pack.ClearColumnRules;
        xxcp_memory_pack.Clear_Column_Rule_Array;

        -- NCM 02/04/09 Removed for testing of build
        -- xxcp_memory_pack.LoadPricingMethods(cSource_id,'N');
        If xxcp_global.Preview_on = 'Y' then
          xxcp_memory_pack.LoadSQLBuildStatements(cSource_id,'N','Y');
        Else
          xxcp_memory_pack.LoadSQLBuildStatements(cSource_id,'N','N');
        End If;
        xxcp_memory_pack.LoadSegmentRules(cSource_id,'N');
   
        -- Validations Source     
        If cSource_id = 15 then
           xxcp_memory_pack.LoadTaxRegInfoNoTarget('N');
        End If;
        
        xxcp_memory_pack.LoadTaxRegInfo('N');
        xxcp_memory_pack.LoadTaxRegOnlyInfo('N');
        xxcp_memory_pack.LoadExtraRecordType(cSource_id,'N');
        xxcp_memory_pack.LoadCurrencies('N');
        xxcp_memory_pack.LoadEnabledSegments('N',cSource_id);
        xxcp_memory_pack.LoadCurrTolerences(cSource_id, 'N');
        xxcp_memory_pack.LoadIgnoreClass('N',cSource_id);

        xxcp_dynamic_sql.Open_Session;

        -- New Session
        xxcp_global.Set_New_Trx_Flag('N');
        -- Cache Summaries. Ensure their is one record in array.
        xxcp_wks.BRK_SET_RCD.Delete;
        xxcp_wks.BRK_SET_RCD(1).sum_qty := 0;

        xxcp_global.gCommon(1).Current_Trx_Group_id := xxcp_global.Get_Interface_Parameters('GPRM_JOURNAL_GROUP_ID');

        Remove_Unwanted_Records(cSource_Group_id => cSource_Group_id  );

  End Engine_Init;

  -- #
  -- # Cleardown all tables is memory
  -- #
  Procedure ClearDown is
   -- This procedure clears down the tables that are held in memory
   -- for VT actions.
  Begin
        -- NCM 02/04/09 Removed for testing of build
        -- xxcp_memory_pack.LoadPricingMethods(0,'Y');
        xxcp_memory_pack.LoadSQLBuildStatements(0,'Y','N');
        xxcp_memory_pack.LoadSegmentRules(0,'Y');
        xxcp_memory_pack.LoadExtraRecordType(0,'Y');
        xxcp_memory_pack.LoadTaxRegInfo('Y');
        xxcp_memory_pack.LoadTaxRegOnlyInfo('Y');
        xxcp_memory_pack.LoadCurrencies('Y');
        xxcp_memory_pack.LoadEnabledSegments('Y',0);
        xxcp_memory_pack.LoadCurrTolerences(0,'Y');
        xxcp_memory_pack.LoadIgnoreClass('Y',0);

        xxcp_dynamic_sql.Close_Session;

        Close_Cursors;
  End ClearDown;

  -- !! ***********************************************************************************************************
  -- !!
  -- !!                                             Make_Cursor
  -- !!
  -- !! ***********************************************************************************************************
  Procedure Make_Cursor(cTarget_Table in varchar2) is

    vCnt      pls_integer := gCur_cnt;
    
  Begin
    -- Cursor
    vCnt           := vCnt + 1;
    gCur_cnt       := gCur_cnt + 1;
    gCursors(vCnt).v_Target_Table := cTarget_Table;
    gCursors(vCnt).v_OpenCur := 'N';
    gCursors(vCnt).v_LastCur := -1;
    gCursors(vCnt).v_usage   := 'C';
    -- History
    vCnt           := vCnt + 1;
    gCur_cnt       := gCur_cnt + 1;
    gCursors(vCnt).v_Target_Table := cTarget_Table;
    gCursors(vCnt).v_OpenCur := 'N';
    gCursors(vCnt).v_LastCur := -1;
    gCursors(vCnt).v_usage   := 'H';

  End Make_Cursor;

  -- !! ***********************************************************************************************************
  -- !!
  -- !!                                             Init_Cursors
  -- !!
  -- !! ***********************************************************************************************************
  Procedure Init_Cursors(cTarget_Table in varchar2) is

    i         pls_integer;
    vCnt      pls_integer := gCur_cnt;
    vExists   boolean     := False;

  Begin

    For i in 1..vCnt loop
      If (gCursors(i).v_Target_Table = cTarget_Table) then
       vExists := True;
      End If;
    End Loop;

    If NOT vExists then
      Make_Cursor(cTarget_Table);
    End If;

  End Init_Cursors;
  
  -- !! ***********************************************************************************************************
  -- !!
  -- !!                                   Add_Cursor
  -- !!
  -- !! ***********************************************************************************************************
  Procedure Add_Cursor(cTarget_Table in varchar2, cUsage in varchar2) is

  Begin
    gCur_cnt := gCur_cnt + 1;
  
    gCursors(gCur_cnt).v_Target_Table := cTarget_Table;
    gCursors(gCur_cnt).v_OpenCur := 'N';
    gCursors(gCur_cnt).v_LastCur := -1;
    gCursors(gCur_cnt).v_Pharsed := 'N';
    gCursors(gCur_cnt).v_usage   := cUsage;
  
  End Add_Cursor;
  
  -- !! ***********************************************************************************************************
  -- !!
  -- !!                                             Open_Cursors
  -- !!
  -- !! ***********************************************************************************************************
  Function Open_Cursor(cUsage in varchar2, cTarget_Table in varchar2,cStatement varchar2) return pls_integer is

    i         pls_integer;
    vPos      pls_integer := 0;

  Begin
  
     For i in 1..gCursors.count loop
       -- Find existing cursor
       If (gCursors(i).v_Target_Table = cTarget_Table) AND (gCursors(i).v_usage = cUsage) then
        vPos := i;
        If gCursors(i).v_OpenCur = 'N' then
          gCursors(i).v_cursorId := DBMS_SQL.Open_cursor;
          gCursors(i).v_OpenCur := 'Y';
          gCursors(i).v_LastCur := -1;

          gCursors(i).v_pharsed := 'Y';
          dbms_sql.Parse(gCursors(i).v_cursorId, cStatement, DBMS_SQL.native);
          Exit;
        End If;
       End If;
     End Loop;

     If vPos = 0 then
        -- add another cursor
        Add_Cursor(cTarget_Table, cUsage);
        i := gCur_cnt;
        gCursors(i).v_cursorId := DBMS_SQL.Open_cursor;
        gCursors(i).v_OpenCur := 'Y';
        gCursors(i).v_LastCur := -1;
        gCursors(i).v_pharsed := 'Y';
        dbms_sql.Parse(gCursors(i).v_cursorId, cStatement, DBMS_SQL.native);
        vPos := i;
     End If;

     Return(vPos);
  End Open_Cursor;

  -- !! ***********************************************************************************************************
  -- !!
  -- !!                                             Reset_Package
  -- !!
  -- !! ***********************************************************************************************************
  Procedure Reset_Package is

    i pls_integer;

  Begin
  
    xxcp_global.gCSA.delete;
    xxcp_global.gCSA_Cnt := 0;
   
    For i in 1..gCur_cnt loop
      If DBMS_SQl.IS_OPEN(gCursors(i).v_CursorId) then
        DBMS_SQL.CLOSE_CURSOR(gCursors(i).v_CursorId);
      End If;

      gCursors(i).v_OpenCur := 'N';
      gCursors(i).v_LastCur := -1;
    End Loop;
  End Reset_Package;

  -- !! ***********************************************************************************************************
  -- !!
  -- !!                                             Close_Cursors
  -- !!
  -- !! ***********************************************************************************************************
  Procedure Close_Cursors is

    i pls_integer;

  Begin
    For i in 1..gCursors.Count loop
     If DBMS_SQl.IS_OPEN(gCursors(i).v_CursorId) then
       DBMS_SQL.CLOSE_CURSOR(gCursors(i).v_CursorId);
     End If;
     gCursors(i).v_OpenCur := 'N';
     gCursors(i).v_LastCur := -1;
    End Loop;
  End Close_Cursors;


  -- **********************************************************
  --       FndWriteTimings
  --
  -- This procedure writes the timings from the process to the
  -- errors table, but makes sure that it does not belong to a
  -- particular transaction.
  -- **********************************************************
  Procedure FndWriteTimings(cMessage in varchar2, cAssignment_Count in number, cTransaction_Count in number) is

    pragma     autonomous_transaction;
    vMessage   varchar2(4000) := cMessage;
    
  Begin

    xxcp_global.SystemDate := Sysdate;

    If cMessage is not null then

      If xxcp_global.Preview_on = 'Y' then
        Begin

          -- Preview errors
          Insert into xxcp_pv_errors
            (Preview_id,
             Source_Activity,
             Process_name,
             Error_Message,
             Transaction_id,
             Source_Table_id,
             Parent_Trx_id,
             Internal_Error_Code,
             Source_Assignment_id,
             Source_Rowid,
             Error_id,
             Trading_set_id ,
             created_by,
             creation_date)
          values
            (xxcp_global.gCommon(1).Preview_id,
             xxcp_global.gCommon(1).current_source_activity,
             xxcp_global.gCommon(1).current_Process_name,
             cMessage,
             0,
             xxcp_global.gCommon(1).current_Source_table_id,
             0,
             -7,
             0,
             Null,
             XXCP_ERRORS_SEQ.nextval,
             Null,
             xxcp_global.User_id,
             xxcp_global.SystemDate);

        Exception when Others then Null;

        End;
      Else
        Begin
          if length(vMessage) > 4000 then
            vMessage := substr(vMessage,1,3990)||' TOO LONG';
          end if;
                  
          update xxcp_activity_control c
             set c.TIMING_DATA       = cMessage
               , c.assignment_count  = cAssignment_Count
               , c.transaction_count = cTransaction_Count
           where request_id = xxcp_global.gCommon(1).Current_Request_id;

          Exception when OTHERS then null;
        End;
      End If;
    End If;

    Commit;

  End FndWriteTimings;
  
 -- !! ****************************************************************************
 -- !!                      Set_Master_Trading_Set
 -- !! ****************************************************************************
  Procedure Set_Master_Trading_Set(cSource_id in number) is 

   Cursor MTS(pSource_id in number) is
      select Trading_Set_id
      from xxcp_trading_sets s
      where s.source_id = pSource_id
       order by s.master desc, s.sequence, s.trading_set_id;
       
  Begin
  
     gMasterTradingSetId := 0;
     
     For Rec in MTS(cSource_id) Loop
       gMasterTradingSetId := Rec.Trading_Set_id;
       Exit;
     End Loop;
  
  End Set_Master_Trading_Set;
  
 -- !! ****************************************************************************
 -- !!                      Get_Global_Tolerance
 -- !! **************************************************************************** 
  Function Get_Global_Tolerance(cSource_id in number) return number is
   Cursor Tolx(pSource_id in number) is
      Select Numeric_Code Global_Rounding_Tolerance
        from xxcp_lookups
       where lookup_type = 'ROUNDING TOLERANCE'
         and lookup_type_subset in (to_char(pSource_id),'GLOBAL')
         order by lookup_type_subset;
         
    vGlobal_Tolerance Number := 0;

  begin
  
    For Rec in Tolx(nvl(cSource_id,0)) loop
     vGlobal_Tolerance :=  Rec.Global_Rounding_Tolerance;
     Exit;
    End Loop;
    
    Return(vGlobal_Tolerance);
   

  End Get_Global_Tolerance;
  
  
 -- !! ***********************************************************************************************************
 -- !!
 -- !!                                  Custom_Event_Insert_Row 
 -- !!
 -- !! ***********************************************************************************************************
  Procedure Custom_Event_Insert_Row (cPos in number, cInternalErrorCode in out number) is
    
    vD1005_Array       xxcp_dynamic_array;
    vCustom_Attributes xxcp_dynamic_array;

  Begin

          vD1005_Array := xxcp_wks.Clear_Dynamic;
          
          If cPos = 1 then
            xxcp_global.Set_New_Trx_Flag('Y');
          Else
            xxcp_global.Set_New_Trx_Flag('N');
          End If;

          xxcp_global.gCommon(1).current_source_rowid      := xxcp_wks.WORKING_RCD(cPos).Source_rowid;
          xxcp_global.gCommon(1).current_Transaction_id    := xxcp_wks.WORKING_RCD(cPos).Transaction_id;
          xxcp_global.gCommon(1).current_Transaction_Table := xxcp_wks.WORKING_RCD(cPos).Transaction_Table;
          xxcp_global.gCommon(1).current_Trading_Set       := xxcp_wks.WORKING_RCD(cPos).Trading_Set;
          
          -- Note: This code line is very important. It lets the common array tell the user which record 
          -- it is working with.
          xxcp_global.gCommon(1).record_number             := cPos; -- new
          --

          vCustom_Attributes := xxcp_wks.Clear_Dynamic;
          vCustom_Attributes := xxcp_wks.WORKING_RCD(cPos).D1006_Array;

          cInternalErrorCode := 
           xxcp_custom_events.Before_insert_row(cSource_id            => xxcp_global.gCommon(1).source_id,
                                                cSource_Assignment_id => xxcp_global.gCommon(1).current_assignment_id,
                                                cTransaction_Table    => xxcp_global.gCommon(1).current_Transaction_Table,
                                                cParent_Trx_id        => xxcp_global.gCommon(1).current_Parent_Trx_id,
                                                cSource_Rowid         => xxcp_wks.WORKING_RCD(cPos).Source_rowid,
                                                cAttribute_id         => xxcp_wks.WORKING_RCD(cPos).Attribute_id,
                                                cOwner_Tax_Reg_id     => xxcp_wks.WORKING_RCD(cPos).I1009_Array(1),
                                                cPartner_Tax_Reg_id   => xxcp_wks.WORKING_RCD(cPos).I1009_Array(2),
                                                cTrading_Set          => xxcp_global.gCommon(1).current_trading_Set,
                                                cColumn_Attributes    => vCustom_Attributes,
                                                cReturned_Attributes  => vD1005_Array);
        
          cInternalErrorCode := nvl(cInternalErrorCode, 0);
          xxcp_wks.WORKING_RCD(cPos).D1005_Array := vD1005_Array;
          
          If cInternalErrorCode <> 0 then
             xxcp_wks.SOURCE_SUB_CODE(xxcp_wks.WORKING_RCD(cPos).Source_Pos):= 7005;
             xxcp_wks.SOURCE_STATUS(xxcp_wks.WORKING_RCD(cPos).Source_Pos) := 'ERROR';
          End If;

  End Custom_Event_Insert_Row;
  
  --
  -- TimeFunction
  --
  Function ElapsedTimeFunction(cStart_Date in date, cEnd_Date in date) return varchar2 is
    
    vHH      number; -- Hours
    vMI      number; -- Minutes
    vSS      number; -- Seconds
    vDD      number; -- Days;


    vElapsed varchar2(50); 

    J1     number;
    J2     number;
    vDiff  number;
    
    

  Begin
  
    vDD := (to_number(to_char(cEnd_Date,'J')) - to_number(to_char(cStart_Date,'J')));
    
    If vDD > 0 then
     vDD := vDD -1;
     J1 := to_number(to_char(cStart_Date,'SSSSS'));
     J2 := to_number(to_char(cEnd_Date,'SSSSS'))+86400;
    Else
     J1 := to_number(to_char(cStart_Date,'SSSSS'));
     J2 := to_number(to_char(cEnd_Date,'SSSSS'));    
    End If;      
    
    
    vDiff := J2 - J1; -- All Seconds Remaining
    
    -- 02.06.04 Changed round to floor as it was rounding the value up. 
    vHH   := floor(vDiff/3600); -- Hours
    vDiff := Mod(vDiff,3600); -- All Seconds Remaining
    -- 02.06.04 Changed round to floor as it was rounding the value up.
    vMI   := floor(vDiff/60); -- All Seconds Remaining
    vSS   := Mod(vDiff,60);     
    
    If vHH > 23 then
      vDD := vDD + 1;
      vHH := vHH-24;
    End If;
  
    If vDD > 0 then
      vElapsed := to_char(vDD)||' Day '||lpad(to_char(vHH),2,'0')||':'||lpad(to_char(vMI),2,'0')||':'||lpad(to_char(vSS),2,'0');      
    Else
      vElapsed := lpad(to_char(vHH),2,'0')||':'||lpad(to_char(vMI),2,'0')||':'||lpad(to_char(vSS),2,'0');
    End If;
    
    Return(vElapsed); 
        
  End ElapsedTimeFunction;
  
 -- !! ***********************************************************************************************************
 -- !!
 -- !!                                   Write_Timings
 -- !!
 -- !! ***********************************************************************************************************
 Procedure Write_Timings(cTiming in out xxcp_global.gTiming_rec, cTiming_Start in number) is


  cursor ifa(pSource_id in number) is
    select c.interfaced
      from xxcp_sys_sources c
      where source_id = pSource_id
        and not source_id in (14,15);

  vTiming_End  Number;
  vTiming_Long Long;

  vCE_Seconds  Varchar2(50);
  vTE_Seconds  Varchar2(50);
  vElapsed     Varchar2(50);
  vCnt         pls_integer;

  vInterfaced  xxcp_sys_sources.interfaced%type; 
  
  vAssignment_Count  number := 0;
  vTransaction_Count number := 0;

  Begin
    -- Record the final end time.
    cTiming(1).End_time := to_char(sysdate,'HH24:MI:SS');
    vTiming_End := xxcp_reporting.Get_Seconds;
    vTiming_End := vTiming_End - cTiming_Start;

    vTiming_Long :=              'Request id.           '||xxcp_global.gCommon(1).Current_Request_id||Chr(10);
    vTiming_Long := vTiming_Long ||'Start Date            '||to_char(gEngineStartDate,'DD-MON-YYYY')||Chr(10);
    vTiming_Long := vTiming_Long ||'Start Time            '||cTiming(1).Start_Time||Chr(10);
    
    For Rec in IFA(xxcp_global.gCOMMON(1).Source_id) loop
      -- Work out if to include the Transaction leg of timing.
      vInterfaced := Rec.Interfaced;
    End Loop;

    For vCnt in 1..cTiming.Count Loop
      vTiming_Long := vTiming_Long ||' Assignment Name      '||xxcp_translations.Source_Assignments(cTiming(vCnt).Source_Assignment_id)||chr(10);
      vTiming_Long := vTiming_Long ||' Init Start Time      '||cTiming(vCnt).Init_Start||Chr(10);
      vTiming_Long := vTiming_Long ||'  Load Memory         '||cTiming(vCnt).Memory_Start||Chr(10);
      vTiming_Long := vTiming_Long ||'  Reset Transactions  '||cTiming(vCnt).Reset_Start||Chr(10);
      vTiming_Long := vTiming_Long ||'  Set Running Status  '||cTiming(vCnt).Set_Running_Start||Chr(10);
      vTiming_Long := vTiming_Long ||'  Grouping Rules      '||cTiming(vCnt).Group_Stamp_Start||Chr(10);
      vTiming_Long := vTiming_Long ||' Init End Time        '||cTiming(vCnt).Init_End||Chr(10);

      vCE_Seconds := ElapsedTimeFunction(cTiming(vCnt).CF_Start_Date, cTiming(vCnt).CF_End_Date);
      
      vTiming_Long := vTiming_Long ||' Assignment Start     '||to_char(cTiming(vCnt).CF_Start_Date,'HH24:MI:SS')||Chr(10);
      vTiming_Long := vTiming_Long ||'  Records             '||trim(to_char(cTiming(vCnt).CF_Records,'999,999,990'))||Chr(10);
      
      vAssignment_Count := vAssignment_Count + nvl(cTiming(vCnt).CF_Records,0);
      
      vTiming_Long := vTiming_Long ||'  Seconds             '||vCE_Seconds||Chr(10);
      vTiming_Long := vTiming_Long ||' Assignment End       '||to_char(cTiming(1).CF_End_Date,'HH24:MI:SS')||Chr(10);

      If vInterfaced = 'Y' and xxcp_global.gCOMMON(1).current_Source_activity != 'VARC' then

        vTE_Seconds := ElapsedTimeFunction(cTiming(vCnt).TE_Start_Date, cTiming(vCnt).TE_End_Date);
        
        vTiming_Long := vTiming_Long ||' Transaction Start    '||to_char(cTiming(vCnt).TE_Start_Date,'HH24:MI:SS')||Chr(10);
        vTiming_Long := vTiming_Long ||'  Records             '||trim(to_char(cTiming(vCnt).TE_Records,'999,999,990'))||Chr(10);
        vTiming_Long := vTiming_Long ||'  Seconds             '||vTE_Seconds||Chr(10);
        vTiming_Long := vTiming_Long ||'  Generated Records   '||trim(to_char(cTiming(vCnt).Flush_Records,'999,999,990'))||Chr(10);
        vTiming_Long := vTiming_Long ||' Transaction End      '||to_char(cTiming(vCnt).TE_End_Date,'HH24:MI:SS')||Chr(10);

      End If;

      vTransaction_Count := vTransaction_Count + nvl(cTiming(vCnt).TE_Records,0);
    
     End Loop;
     
     vTiming_Long := vTiming_Long ||'End Time              '||cTiming(1).End_Time||Chr(10);
    
     vElapsed := ElapsedTimeFunction(gJobStartTime, Sysdate);
 
     If xxcp_global.Preview_on = 'Y' then
       vTiming_Long := vTiming_Long ||'Note:Preview does not write to Oracle';
     End If;

     FndWriteTimings(vTiming_Long, vAssignment_Count, vTransaction_Count);
    
     Exception When OTHERS Then Null;
    
  End Write_Timings;

  
-- !! ***********************************************************************************************************
-- !!
-- !!                                Set Currency Conversion Parameters 
-- !! ! Under review for delete in vt273
-- !! ***********************************************************************************************************
  Procedure Curr_Conv_Set_init(cTarget_Currency in varchar2, cExchange_Date in date, cExchange_Type in varchar2) is
  Begin

    gCUCO_Target_Currency := cTarget_Currency;
    gCUCO_Exchange_Type   := cExchange_type;
    gCUCO_Exchange_Date   := cExchange_Date;
  
  End Curr_Conv_Set_init;
  
  
-- !! ***********************************************************************************************************
-- !!
-- !!                                  Curr_Conv_Set 
-- !!
-- !! This Procedure is called from the Engines to solve a rounding issue for Tektronix
-- !! You must be using the gateway view function in the dynnamic interface before hand
-- !! ***********************************************************************************************************
  Procedure Curr_Conv_Set(cPricing_Method_id    in Number, 
                          cOwner_Tax_Reg_id     in Number,
                          cPartner_Tax_Reg_id   in Number,
                          --
                          cTransaction_Currency in Varchar2,
                          cPricing_Method_Type  in varchar2,
                          cAction               in varchar2
                          ) is
                          
    Cursor PM(pPricing_Method_id in number, pPricing_Method_Type in varchar2) is
     Select m.exchange_rate_type ,
            Decode(
              sign(nvl(m.exchange_date_attribute_id,99999)-decode(pPricing_Method_Type,'C',2000,4000)),
              -1, exchange_date_attribute_id, 0) exchange_date_id,
            substr(nvl(m.to_curr_indicator,'S'),1,1) Owner_Ind,
            substr(m.to_curr_indicator,2,2) Owner_Curr_Ind
     from xxcp_pricing_method m
     where m.price_method_id = ppricing_method_id;    
     
    Cursor TR(pTax_Reg_id in number, pInd in varchar2) is
        Select decode(pInd,'BM',t.target_acct_currency,
                           'LM',r.legal_currency,
                           'TM',r.Trade_currency) Currency_Code          
            ,r.Exchange_Rate_Type
        from xxcp_tax_registrations  r,
             xxcp_target_assignments t,
             xxcp_sys_target_types   x
       where r.reg_id              = t.reg_id
         and r.Tax_Registration_id = pTax_Reg_id
         and nvl(r.Active,'Y')     = 'Y'
         and nvl(t.Active,'Y')     = 'Y'
         and t.target_type_id      = x.target_type_id
         and x.target_id           = any(select distinct tt.target_id 
                                           from xxcp_sys_target_tables tt 
                                          where tt.source_id = xxcp_global.gCommon(1).Source_id
                                             ) 
    order by x.master desc,t.target_type_id; 
     
    vExchange_Type Varchar2(30);  
    vCurrency_Code Varchar2(15);
    vExchange_Date varchar2(30);
    vTax_Reg_Id    Number;                  
                          
  Begin 

    If gCUCO_INUSE = 'Y' then
  
      For PMRec in PM(cPricing_Method_id, cPricing_Method_Type) Loop

        If PMRec.Owner_Ind in ('O','P') then        
          --
          -- Get the data from the Tax Registration
          -- Owner
          If PMRec.Owner_Ind = 'O' then
            vTax_Reg_Id := cOwner_Tax_Reg_id;
          Else
            -- Partner
            vTax_Reg_Id := cPartner_Tax_Reg_id;
          End If;
          --
          -- Fetch 
          --
          For TRRec in TR(vTax_Reg_Id, PMRec.Owner_Curr_Ind) Loop
            vExchange_Type := nvl(PMRec.Exchange_Rate_Type,TRRec.Exchange_Rate_Type);
            vCurrency_Code := TRRec.Currency_Code;
            Exit;
          End Loop;
        
        Else
          --  Take Type of pricing method and if not there, use default for lookups.
          vExchange_Type := nvl(PMRec.Exchange_Rate_Type,gCUCO_Dft_Exchange_Type);
          -- Take the Transaction Currency 
          vCurrency_Code := cTransaction_Currency;
        End IF;
        
        If PMRec.exchange_date_id > 0 then
          if cPricing_Method_Type = 'C' then
             vExchange_Date := xxcp_foundation.Find_DynamicAttribute(PMRec.Exchange_Date_id);
          Else
            vExchange_Date := xxcp_foundation.Find_DynamicAttribute(PMRec.Exchange_Date_id, cAction);
          End If;
        End If;

        vExchange_Date := nvl(vExchange_Date,xxcp_wks.D1001_Array(7));
        
      End Loop;

      gCUCO_Target_Currency := vCurrency_Code;
      gCUCO_Exchange_Type   := vExchange_Type;
      gCUCO_Exchange_Date   := vExchange_Date;
            
    End If;
             
  End Curr_Conv_Set;


-- !! ***********************************************************************************************************
-- !!
-- !!                                   Currency Conversion 
-- !!
-- !! ***********************************************************************************************************
  Function Utils_Curr_Conv(cAmount              in number
                         , cCurrency_Code       in varchar2
                         , cPrecision_Indicator in varchar2
                         , cExchange_Date       in Date     default null
                         , cExchange_Type       in varchar2 default null
                         , cTarget_Currency     in varchar2 default null
                         , cLabel               in varchar2 default null
   ) return number is
  
   vER                  Number(8) := 0;
   vRet_Common_Currency Varchar2(3);
   vExchange_Rate       Number;
   vPrecision           Number;
   vAmount              Number;
   vTrace_Msg           varchar2(2000);
  
  Begin
  
    If cPrecision_Indicator = 'E' then
      -- Extended Precision 
      vPrecision := xxcp_foundation.Fnd_CurrencyPrecision(nvl(cTarget_Currency, gCUCO_Target_Currency), 2,'Y');
    Else
      vPrecision := xxcp_foundation.Fnd_CurrencyPrecision(nvl(cTarget_Currency, gCUCO_Target_Currency), 2,'N');
    End If;
        
    vExchange_Rate := 
       xxcp_foundation.Currency_Conversions(cTransaction_Currency     => cCurrency_Code
                                           ,cTag                      => 'Gateway'
                                           ,cExchange_Currency        => nvl(cTarget_Currency, gCUCO_Target_Currency)
                                           ,cDefault_Common_Exch_Curr => gCUCO_Common_Currency                     -- Common Exchange Currency
                                           ,cExchange_Date            => nvl(cExchange_Date, gCUCO_Exchange_Date)   -- Exchange Date
                                           ,cExchange_Type            => nvl(cExchange_Type, gCUCO_Exchange_Type)   -- Exchange Rate Type
                                           ,cOnFailure_ErrorCode      => 3033
                                           ,cCommon_Exch_Currency     => vRet_Common_Currency
                                           ,cInternalErrorCode        => vER);

    vAmount := Round(cAmount  * vExchange_Rate,vPrecision);
        
    If xxcp_global.Extra_Trace_on = 'Y' then
    
      vTrace_Msg := 'TRACE: GWU CURR. CONV. :'||cLabel||' Original Value <'||to_char(cAmount)||'> Currency <'||cCurrency_Code||
                     '> New Value <'||to_char(vAmount)||'> Currency <'||nvl(cTarget_Currency, gCUCO_Target_Currency)||
                     '> Rate <'||to_char(vExchange_Rate)||'> Precision <'||vPrecision||'>  Type <'||nvl(cExchange_Type, gCUCO_Exchange_Type) ||
                     '> Date <'||nvl(cExchange_Date, gCUCO_Exchange_Date)||'>';
                                        
      xxcp_foundation.FndWriteError(100,vTrace_Msg);
      xxcp_foundation.show(vTrace_Msg);                                 
    
    End If;   
        
    If vER != 0 then
      vAmount := Null;
    End If;
    
    Return(vAmount);
  End Utils_Curr_Conv;
  

-- !! ***********************************************************************************************************
-- !!
-- !!                                   Currency Conversion Rate
-- !!
-- !! ***********************************************************************************************************
  Function Utils_Curr_ConvRate(cCurrency_Code       in varchar2
                             , cPrecision_Indicator in varchar2
                             , cExchange_Date       in Date     default null
                             , cExchange_Type       in varchar2 default null
                             , cTarget_Currency     in varchar2 default null
   ) return number is

   vER                  Number(8) := 0;
   vRet_Common_Currency Varchar2(3);
   vExchange_Rate       Number;
   vPrecision           Number; 

  Begin

    If cPrecision_Indicator = 'E' then
      -- Extended Precision
      vPrecision := xxcp_foundation.Fnd_CurrencyPrecision(nvl(cTarget_Currency, gCUCO_Target_Currency), 2,'Y');
    Else
      vPrecision := xxcp_foundation.Fnd_CurrencyPrecision(nvl(cTarget_Currency, gCUCO_Target_Currency), 2,'N');
    End If;

    vExchange_Rate :=
       xxcp_foundation.Currency_Conversions(cTransaction_Currency     => cCurrency_Code
                                           ,cTag                      => 'Gateway'
                                           ,cExchange_Currency        => nvl(cTarget_Currency, gCUCO_Target_Currency)
                                           ,cDefault_Common_Exch_Curr => gCUCO_Common_Currency                      -- Common Exchange Currency
                                           ,cExchange_Date            => nvl(cExchange_Date, gCUCO_Exchange_Date)   -- Exchange Date
                                           ,cExchange_Type            => nvl(cExchange_Type, gCUCO_Exchange_Type)   -- Exchange Rate Type
                                           ,cOnFailure_ErrorCode      => 3033
                                           ,cCommon_Exch_Currency     => vRet_Common_Currency
                                           ,cInternalErrorCode        => vER);

    Return(vExchange_Rate);
  End Utils_Curr_ConvRate;

  -- !! ***********************************************************************************************************
  -- !!
  -- !!                                   Class_Mapping 
  -- !!
  -- !! ***********************************************************************************************************
  Function Class_Mapping(cSource_id              in number,
                         cClass_Mapping_Required in varchar2,
                         cTransaction_Table      in varchar2,
                         cTransaction_Type       in varchar2,
                         cTransaction_Class      in varchar2,
                         --
                         cTransaction_id         in Number,
                         cCode_Combination_id    in Number default 0,
                         cAmount                 in Number default 0, 
                         cSystem_Link_id         in number default 0 
                        ) return varchar2 is

  vClass_Mapping_Name xxcp_sys_source_classes.class%type;
      
  vClass_SystemLatch  xxcp_sys_source_classes.class%type;
  
  Cursor MTLWIP(pTransaction_Table in varchar2, pSystem_Latch in varchar2) is
      select c.Class New_Class, c.SOURCE_CLASS_ID
        from xxcp_sys_source_tables t, 
             xxcp_sys_source_classes c
       where t.transaction_table = ptransaction_table
         and c.source_table_id   = t.source_table_id
         and t.class_mapping_req = 'Y'
         and (c.system_latch     = pSystem_latch or c.default_class = 'Y')
       order by Decode(System_latch, Null, 99, 0);  
                        
  Begin             
                      
    If cSource_id = 5 then
    
      If cTransaction_Class in ('MTL','WIP') then
        -- *********************************************************************
        -- Inventory Logic
        -- *********************************************************************
        If cTransaction_Class = 'MTL' then
          Begin
          Select to_char(nvl(mta.Cost_Element_id, 0)) || '-' ||
                 to_char(mta.Accounting_line_type)
            into vClass_SystemLatch
            from mtl_material_transactions mt,
                 mtl_transaction_accounts  mta
           where mt.transaction_id     = cTransaction_id
             and mta.transaction_id    = mt.transaction_id
             and mta.reference_account = cCode_Combination_id
             and mta.gl_sl_link_id     = cSystem_Link_id;

           Exception when OTHERS then vClass_SystemLatch := Null;
          End;

          vClass_Mapping_Name := vClass_SystemLatch;
          
        End If;

        For MTLWIPRec in MTLWIP(cTransaction_Table, vClass_SystemLatch) Loop
            vClass_Mapping_Name := MTLWIPRec.New_Class;
           Exit;
        End Loop;

        If xxcp_global.Trace_on = 'Y' then
             xxcp_foundation.FndWriteError(100,
                              'TRACE (Class Mapping)            :Transaction Class <' ||vClass_Mapping_Name||
                              '> Class System Latch <' ||vClass_SystemLatch ||
                              '> Link ID <' ||to_char(cSystem_Link_id) ||
                             '> Code Comb. <' ||to_char(cCode_Combination_id) || '>');
        End If;
              End If;
   Else 
        
     For MTLWIPRec in MTLWIP(cTransaction_Table, cTransaction_Class) Loop
       vClass_Mapping_Name := MTLWIPRec.New_Class;
       Exit;
     End Loop;
 


   End If;    
                  
   Return(vClass_Mapping_Name);       
                    
  End Class_Mapping;
  
  -- !! ***********************************************************************************************************
  -- !!
  -- !!                                   Preview_Mode_Restrictions 
  -- !!
  -- !! ***********************************************************************************************************
  Procedure Preview_Mode_Restrictions(cCalling_Wrapper      in varchar2,
                                      cSource_assignment_id in number,
                                      cSource_Rowid         in Rowid ) is
  
           
  Cursor pf(pUser_id in number) is
   select f.pv_restrict_parent
   from xxcp_user_profiles f
   where f.user_id = puser_id;    
     
   vParent_trx_id        xxcp_pv_interface.vt_parent_trx_id%type := 0;
   vTransdaction_Table   xxcp_pv_interface.vt_transaction_table%type;
   vSource_assignment_id xxcp_pv_interface.vt_source_assignment_id%Type;
   vTransaction_ref      xxcp_pv_interface.vt_transaction_ref%type;  
  
  Begin
  
        
      For Rec in pf(xxcp_global.gCommon(1).preview_id) Loop
       -- Restrictions 
       If Rec.pv_restrict_parent = 'Y' then
        Begin
          Select vt_parent_trx_id, vt_transaction_table, vt_transaction_ref, vt_source_assignment_id
            into vParent_trx_id, vTransdaction_Table, vTransaction_ref, vSource_assignment_id
            from xxcp_pv_interface p
           where p.vt_source_rowid = cSource_Rowid
             and vt_preview_id     = xxcp_global.gCommon(1).preview_id;

          Begin

            Delete xxcp_pv_interface
            where vt_preview_id           = xxcp_global.gCommon(1).preview_id
              and vt_transaction_table    = vTransdaction_Table
              and vt_transaction_ref      = vTransaction_ref
              and vt_source_assignment_id = vSource_assignment_id
              and vt_parent_Trx_id       != vParent_trx_id;

            Exception when OTHERS then Null;
          End;
          
          Exception when OTHERS then Null;

        End;
       End If;
       Exit;
      End Loop;
  End Preview_Mode_Restrictions;
  
  --
  -- Add_Alert_to_Queue
  --
  Procedure Add_Alert_to_Queue(cSource_id            in number,
                               cSource_Group_id      in number,
                               cSource_Assignment_id in number,
                               cRequest_id           in number,
                               cAlert_Type           in number default 0
                               ) is

    pragma autonomous_transaction;
  
    gSysdate date;
                               
  Begin

    gSysdate := Sysdate;

    -- Alert Queue
    Insert into xxcp_alert_queue(
      source_id,
      source_group_id,
      source_assignment_id,
      request_id,
      alert_type, 
      alert_priority,
      created_by,
      creation_date,
      last_updated_by,
      last_update_date,
      last_update_login
    )
    values
    (
     cSource_id,
     cSource_Group_id,
     cSource_Assignment_id,
     cRequest_id,
     cAlert_Type,
     5,
     fnd_global.user_id,
     gSysdate,
     fnd_global.user_id,
     gSysdate,
     fnd_global.LOGIN_ID
     );
  
  
   Commit;

    
--    Exception when Others then Null;
      
  End Add_Alert_to_Queue;
  --
  -- Remove_Alert_to_Queue
  --
  Procedure Remove_Alert_from_Queue is

    pragma autonomous_transaction;
  
  Begin
    Delete from xxcp_alert_queue 
     where (alert_complete = 'Y' or creation_date < (sysdate-15));
    Exception when Others then Null;
  End Remove_Alert_from_Queue;
   
  --
  -- Get_Partner_Tax_Reg_Group
  --
  Function Get_Tax_Reg_Group(cTax_Registration_id in number) return number is
  
    cursor c1(pTax_Registration_id in number) is
      select w.tax_reg_group_id
       from xxcp_tax_registrations w
       where w.tax_registration_id = pTax_Registration_id;  
       
       vTax_Reg_Group_id xxcp_tax_registrations.tax_reg_group_id%type;
  
    Begin
      
        for Rec in c1(pTax_Registration_id => cTax_Registration_id) Loop
            vTax_Reg_Group_id := Rec.Tax_Reg_Group_Id;
        end loop;
        
        Return(vTax_Reg_Group_id);
    
    End Get_Tax_Reg_Group;

  --
  -- Get_Trading_Relationship
  --
  Function Get_Trading_Relationship(cAction_Type               IN Varchar2,
                                    cSource_id                 IN Number, 
                                    cOwner_Tax_Reg_id          IN Number, 
                                    cPartner_Tax_Reg_Id        IN Number, 
                                    cTransaction_Date          IN Date,
                                    cCreate_IC_Trx             IN Varchar2,
                                    cDflt_Trading_Relationship IN Varchar2,
                                    cEntity_Partnership_id     out Number ) Return Number is
              
      CURSOR EP(pSource_id IN NUMBER, pOwnerTaxid IN NUMBER, pPartnerTaxId IN NUMBER, pTransaction_Date in DATE,
              pOwnerTaxRegGroupId IN NUMBER, pPartnerTaxRegGroupId IN NUMBER) IS
      SELECT Entity_Set_id Entity_Partnership_id, tc.set_name Trading_Relationship_Name
        FROM xxcp_entity_partnerships ep,
             xxcp_table_set_ctl tc
       WHERE ep.source_id  IN (0,pSource_id)
         AND ((ep.Owner_Tax_Reg_id        = pOwnerTaxid 
         AND   ep.Partner_Tax_Reg_id      = pPartnerTaxId)
          OR  (ep.Owner_Tax_Reg_Group_id  = pOwnerTaxRegGroupId
         AND   ep.Partner_Tax_Reg_Group_id= pPartnerTaxRegGroupId))
         AND pTransaction_Date between NVL(ep.effective_from_date, '01-JAN-1900') AND NVL(ep.effective_to_date, '31-DEC-2999')
         AND ep.active         = 'Y'
         AND Decode(tc.attribute1,'Y',Sign(pOwnerTaxId-pPartnerTaxId),1) != 0
         AND ep.entity_set_id  = tc.set_id
         AND tc.set_base_table = 'CP_ENTITY_PARTNERSHIPS'
       ORDER BY ep.source_id DESC, 
           decode(ep.Owner_Tax_Reg_id,pOwnerTaxid,10,0)+decode(ep.Partner_Tax_Reg_id,pPartnerTaxId,10,0) desc,
           decode(ep.Owner_Tax_Reg_id,0,0,10), decode(ep.Partner_Tax_Reg_id,0,0,10);

  -- NCM 02.05.04
   -- This cursor gets the default trading relationship if specified.
   CURSOR EPD (pDflt_trading_rel_name in varchar2) IS
      SELECT set_id Entity_Partnership_id, tc.set_name Trading_Relationship_Name
        FROM xxcp_table_set_ctl tc
       WHERE tc.set_name       = pDflt_trading_rel_name
         AND tc.set_base_table = 'CP_ENTITY_PARTNERSHIPS'
         AND tc.source_id      = 0;
         
    vInternalErrorCode         number(8) := 0;     
    vTrading_Relationship_Name xxcp_table_set_ctl.set_name%type;
    vOwner_Tax_Reg_Group_Id    xxcp_tax_registrations.tax_reg_group_id%type;
    vPartner_Tax_Reg_Group_Id  xxcp_tax_registrations.tax_reg_group_id%type;
         
   Begin
     
        vOwner_Tax_Reg_Group_Id := Get_Tax_Reg_Group(cOwner_Tax_Reg_id);
        vPartner_Tax_Reg_Group_Id := Get_Tax_Reg_Group(cPartner_Tax_Reg_Id);

        IF cAction_Type = 'A'  and (cCreate_IC_Trx = 'Y' AND (cOwner_Tax_Reg_Id <> cPartner_Tax_Reg_Id)) THEN
          vInternalErrorCode := 3092;
        ElsIf cAction_Type = 'T' then
          vInternalErrorCode := 10508;
        END IF;
             -- NCM 02.05.04 if the trading relationship is set to N then do normal processing otherwise get the default.
        IF nvl(cDflt_Trading_Relationship,'N') = 'N' THEN
          FOR Rec IN EP(xxcp_global.gCOMMON(1).Source_id, cOwner_Tax_Reg_Id, cPartner_Tax_Reg_Id, 
                        cTransaction_Date, 
                        vOwner_Tax_Reg_Group_Id, 
                        vPartner_Tax_Reg_Group_Id) LOOP
            cEntity_Partnership_id   := Rec.Entity_Partnership_id;
            xxcp_wks.I1009_Array(98) := Rec.Trading_Relationship_Name;
            vInternalErrorCode       := 0;

            IF cEntity_Partnership_id IS NULL THEN
              vInternalErrorCode := 3094;
              EXIT;
            END IF;
            EXIT;
          END LOOP;
        -- Otherwise
        Else
          FOR Rec IN EPD(Get_dflt_trading_relationship()) LOOP
                --
                cEntity_Partnership_id   := Rec.Entity_Partnership_id;
                xxcp_wks.I1009_Array(98) := Rec.Trading_Relationship_Name;
                vInternalErrorCode       := 0;

                IF cEntity_Partnership_id IS NULL THEN
                  vInternalErrorCode := 3094;
                  EXIT;
                END IF;
            EXIT;
          END LOOP;
        End If;
        
        IF vInternalErrorCode <> 0 then -- and cAction_Type = 'T' THEN
          xxcp_foundation.FndWriteError(vInternalErrorCode,
               'Owner Tax Registration <' ||TO_CHAR(cOwner_Tax_Reg_id) || '> Partner Tax Registration <' ||TO_CHAR(cPartner_Tax_Reg_id) || '> Transaction Date <'||to_char(cTransaction_Date)||
                   '> Owner Tax Reg Group <'|| vOwner_Tax_Reg_Group_id||'> Partner Tax Reg Group <'||vPartner_Tax_Reg_Group_id||'>' );
        END IF;
        
        Return(vInternalErrorCode);
        
   End Get_Trading_Relationship;
  
  -- RoundX
  Function RoundX(cAmount in number, cPrecision in number, cRounding_Type in varchar2) return number is
    
   vResult number;
   
   vPwrPlus number;
   vPwrMinus number;
  
  Begin
    
     vResult := cAmount;
     
     If vResult != 0 then
       Case cRounding_Type 
         When 'Floor' then
            vPwrPlus  := power(10,cPrecision);  
            vPwrMinus := power(10,(cPrecision*-1));  
         
            vResult := floor(cAmount*vPwrPlus)*vPwrMinus;     
         When 'Ceiling' then
            vPwrPlus  := power(10,cPrecision);  
            vPwrMinus := power(10,(cPrecision*-1));  
         
            vResult := ceil(cAmount*vPwrPlus)*vPwrMinus;     
         Else
            vResult := round(cAmount, cPrecision);  
       End Case;     
     End If;
  
     Return(vResult);
  End RoundX;

  -- Tax Rates
  Procedure Calc_Tax_Rates(  
                        cSourceRowid            IN ROWID,
                        cTransaction_id         IN NUMBER,
                        cTransaction_Date       IN DATE,
                        cOwnerTaxid             IN NUMBER,
                        cPartnerTaxId           IN NUMBER,
                        cIC_Control             IN VARCHAR2, 
                        cTransaction_Set_id     IN NUMBER,
                        cExchange_Date          IN DATE,
                        cTaxable_Amount         IN NUMBER,
                        cTaxable_Currency       IN VARCHAR2,
                        cInternalErrorCode  IN OUT NUMBER) IS  
  
    Cursor TEG(pTax_Reg_id in number) is
     select r.tax_authority, w.tax_authority_id, w.tax_engine_id
     from xxcp_tax_registrations r ,
          xxcp_tax_authorities_v w
     where r.tax_authority = w.tax_authority
       and r.tax_registration_id = pTax_Reg_id
       and w.active = 'Y';                        
       
    Cursor TCAT(pTax_Authority_id in number, pTransaction_Set_id in Number) is   
     select hw.qualifier1_attribute_id,
            hw.qualifier2_attribute_id,
            hw.qualifier3_attribute_id,
            hw.qualifier4_attribute_id,
            hw.qualifier5_attribute_id,
            hw.qualifier6_attribute_id,
            r.tax_regime_id,
            r.tax_regime_code,
            r.tax_currency_code,
            r.exchange_rate_type,
            r.rounding_type_code,
            r.tax_element_number,
            r.compound_tax
     from xxcp_tax_rate_qualifiers hw,
          xxcp_tax_regimes r
    where hw.tax_regime_id      = r.tax_regime_id
      and r.tax_authority_id    = pTax_Authority_id
      and hw.transaction_set_id = pTransaction_Set_id
      and hw.source_id = xxcp_global.gCommon(1).Source_id
    order by r.tax_element_number;
      
    Cursor TQUAL(  pTax_Regime_id      in number,
                   pTransaction_Date   in date,
                   pQualifier1_Attr_id in number, 
                   pQualifier2_Attr_id in number, 
                   pQualifier3_Attr_id in number, 
                   pQualifier4_Attr_id in number, 
                   pQualifier5_Attr_id in number, 
                   pQualifier6_Attr_id in number) is
      Select rt2.tax_lookup_id,
             rt2.tax_regime_id,
             rt2.tax_rate_code,
             rt2.tax,
             rt2.tax_rate_description,
             rt2.tax_status_code,
             rt2.tax_jurisdiction
      from xxcp_tax_rate_lookups rt2
      where rt2.tax_regime_id = pTax_Regime_id
        AND pTransaction_Date BETWEEN nvl(rt2.Effective_From_Date, TO_DATE('01-JAN-2000','DD-MON-YYYY')) AND nvl(rt2.Effective_To_Date, TO_DATE('31-DEC-2999','DD-MON-YYYY'))
         AND nvl(rt2.qualifier1, '~null~') =
             DECODE(rt2.qualifier1,
                    '*',
                    nvl(rt2.qualifier1, '~null~'),
                    nvl(xxcp_foundation.MC_DynamicAttribute(pQualifier1_Attr_id), '~null~'))
        AND nvl(rt2.qualifier2, '~null~') =
             DECODE(rt2.qualifier2,
                    '*',
                    nvl(rt2.qualifier2, '~null~'),
                    nvl(xxcp_foundation.MC_DynamicAttribute(pQualifier2_Attr_id), '~null~'))
         AND nvl(rt2.qualifier3, '~null~') =
             DECODE(rt2.qualifier3,
                    '*',
                    nvl(rt2.qualifier3, '~null~'),
                    nvl(xxcp_foundation.MC_DynamicAttribute(pQualifier3_Attr_id), '~null~'))
         AND nvl(rt2.qualifier4, '~null~') =
             DECODE(rt2.qualifier4,
                    '*',
                    nvl(rt2.qualifier4, '~null~'),
                    nvl(xxcp_foundation.MC_DynamicAttribute(pQualifier4_Attr_id), '~null~'))
         AND nvl(rt2.qualifier5, '~null~') =
             DECODE(rt2.qualifier5,
                    '*',
                    nvl(rt2.qualifier5, '~null~'),
                    nvl(xxcp_foundation.MC_DynamicAttribute(pQualifier5_Attr_id), '~null~'))
         AND nvl(rt2.qualifier6, '~null~') =
             DECODE(rt2.qualifier6,
                    '*',
                    nvl(rt2.qualifier6, '~null~'),
                    nvl(xxcp_foundation.MC_DynamicAttribute(pQualifier6_Attr_id), '~null~'))
         Order by Rank;

    Cursor TRATES(pTax_Regime_Code       in varchar2,
                  pTax_Rate_Code         in varchar2,
                  pTax                   in varchar2,
                  pTax_Jurisdiction_code in varchar2,
                  pTax_Status_Code       in varchar2,
                  pTransaction_date      in date
                 ) is
      select mx.PERCENTAGE_RATE, mx.TAX_RATE_ID, mx.recovery_rate, mx.recovery_type_code
      from XXCP_INSTANCE_TAX_RATES_V mx
      where mx.TAX_REGIME_CODE = pTax_Regime_Code
       and mx.TAX_RATE_CODE    = pTax_Rate_Code
       and mx.TAX              = pTax
       and mx.TAX_JURISDICTION_CODE = pTax_Jurisdiction_code
       and mx.TAX_STATUS_CODE = pTax_Status_Code
       and mx.ACTIVE_FLAG = 'Y'
       and cTransaction_Date between nvl(mx.EFFECTIVE_FROM,'01-JAN-2000') and nvl(mx.EFFECTIVE_TO,'31-DEC-2099');
    
    Cursor tADD(pTax_Registration_id in number) is
        select bill_to_country,
               bill_to_postal_code
        from   xxcp_address_details
        where  tax_registration_id = pTax_Registration_id;
    
    vTax_OnBehalfOf   xxcp_tax_registrations.tax_registration_id%type;
    vTax_Authority    xxcp_tax_registrations.tax_authority%type;
    vTax_Authority_id xxcp_table_set_ctl.set_id%type;
    vTax_Engine_id    integer := 0;
    
    vPercentage          number;
    vNew_Tax_Amount      number;
    vTax_Rate_id         number; 
    vTax_Code            varchar2(100);
     
    vTaxCnt               pls_integer := 0; 
    vErrMsg               varchar2(4000);                            
    
    vTax_Amount            number := 0;
    vTax_Gross_Amount      number := 0;
    vRecovery_Amount       number := 0;
    vLast_Recovery_Amount  number := 0;
    vLast_Recovery_Rate    number :=0;
    vTax_Rate              number := 0;
    vBase_Taxable_Amount    number := 0;
    vNew_Taxable_Amount    number := 0;
    vNew_Taxable_Currency  varchar2(15);
    vNew_Exchange_Rate     Number := 1;
    vPrecision             pls_integer := 2;
    vRunning_Total         Number := 0;    
    vCommon_Exch_Currency  varchar2(15);
    
    w pls_integer;

    vCheck_Element_Array   XXCP_DYNAMIC_ARRAY := XXCP_DYNAMIC_ARRAY(0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
    vCheck_Currency        varchar2(15);    
    vTax_Lookup_Trace      varchar2(32000);
    vRounding_Type         varchar2(20) := 'Nearest';
    
    vOwner_Postcode        varchar2(100);
    vOwner_Country         varchar2(100);
    vPartner_Postcode      varchar2(100);
    vPartner_Country       varchar2(100);
    
    vDummy_Base_Amount     number := 100;
    vDummy_Running_Total   number := 0;
    vDummy_Taxable_Amount  number := 100;
    vDummy_Tax_Amount      number := 0;
    vDummy_New_Tax_Amount  number := 0;
        
  Begin
    
    xxcp_te_base.gTax_Record_Set.delete;
    xxcp_te_base.gTax_Element_Set.delete;
    
    If substr(cIC_Control,1,1) in ('O','P') then  
      If substr(cIC_Control,1,1) = 'O' then
         vTax_OnBehalfOf := cOwnerTaxId;
      Else
         vTax_OnBehalfOf := cPartnerTaxId;
      End If;
    
      For Rec in TEG(pTax_Reg_id => vTax_OnBehalfOf) loop
        vTax_Authority    := rec.tax_authority;
        vTax_Authority_id := rec.tax_authority_id;  
        vTax_Engine_id    := rec.tax_engine_id;         
      End Loop;

      vTax_Lookup_Trace := 'Tax Control <'||cIC_Control||'> Tax Reg <'||to_char(vTax_OnBehalfOf)||'> Tax Authority <'||vTax_Authority||'> Tax Engine id <'||vTax_Engine_id||
                           '> Date <'||to_char(cTransaction_Date,'DD-MON-YYYY')||'>'||Chr(10);
      
      If vTax_Authority is null then
        cInternalErrorCode := 3170; -- No Tax Authority defined
      Else     
        If vTax_Engine_id = 2 then
           -- Vertex Call
           
           -- Get Owner Country and Postcode
           Open  tADD (pTax_Registration_id => cOwnerTaxid);
           Fetch tAdd into vOwner_Country,
                           vOwner_Postcode;
           Close tAdd;
           
           -- Get Partner Country and Postcode
           Open  tADD (pTax_Registration_id => cPartnerTaxid);
           Fetch tAdd into vPartner_Country,
                           vPartner_Postcode;
           Close tAdd;
           
           cInternalErrorCode := xxcp_vertex_ws.control(cTaxable_Amount   => cTaxable_Amount,
                                                        cTaxable_Currency => cTaxable_Currency,
                                                        cTransaction_Date => cTransaction_Date,
                                                        cSeller_Postcode  => vOwner_Postcode,
                                                        cSeller_Country   => vOwner_Country,
                                                        cCust_Postcode    => vPartner_Postcode,
                                                        cCust_Country     => vPartner_Country);
                                  
           
        ElsIf vTax_Engine_id = 3 then
           Null; -- Sabrix Call
        ElsIf vTax_Engine_id = 1 then
          
          vCheck_Currency := Null;
          -- VT Tax Engine 
          -- Find which Tax Regimes to search
          -- and which parameters         
          For RecCAT in TCAT(pTax_Authority_id   => vTax_Authority_id, 
                             pTransaction_Set_id => cTransaction_Set_id
                             ) Loop 
                             
             If vCheck_Currency is null then
               vCheck_Currency := RecCat.Tax_Currency_Code;
             ElsIf vCheck_Currency != RecCat.Tax_Currency_Code then
               cInternalErrorCode := 3174;
               Exit;
             End If;                

             vRounding_Type        := RecCat.Rounding_Type_Code;

             vNew_Taxable_Currency := RecCat.Tax_Currency_Code;
             vPrecision            := xxcp_foundation.Fnd_CurrencyPrecision(vNew_Taxable_Currency, 2,'N');

             If cTaxable_Currency != RecCat.Tax_Currency_Code then
                vNew_Exchange_Rate :=
                   xxcp_foundation.Currency_Conversions(cTransaction_Currency     => cTaxable_Currency,
                                                        cTag                      => 'Tax Currency',
                                                        cExchange_Currency        => RecCat.Tax_Currency_Code,
                                                        cDefault_Common_Exch_Curr => 'USD', 
                                                        cExchange_Date            => cExchange_Date,
                                                        cExchange_Type            => RecCAT.Exchange_Rate_Type,
                                                        cOnFailure_ErrorCode      => 3028,
                                                        cCommon_Exch_Currency     => vCommon_Exch_Currency,
                                                        cInternalErrorCode        => cInternalErrorCode);

               vNew_Taxable_Amount   := cTaxable_Amount * vNew_Exchange_Rate;
             Else
               vNew_Exchange_Rate    := 1;
               vNew_Taxable_Amount   := cTaxable_Amount;    
             End If;
             
             vNew_Taxable_Amount := RoundX(vNew_Taxable_Amount,vPrecision,vRounding_Type);
             vBase_Taxable_Amount := vNew_Taxable_Amount;
             -- Using Qualifiers, find which Tax Rates are to be looked up
             --              
             For RecQual in TQUAL(
                   pTax_Regime_id      => RecCAT.Tax_Regime_Id,
                   pTransaction_Date   => cTransaction_Date,
                   pQualifier1_Attr_id => RecCAT.Qualifier1_Attribute_Id, 
                   pQualifier2_Attr_id => RecCAT.Qualifier2_Attribute_Id, 
                   pQualifier3_Attr_id => RecCAT.Qualifier3_Attribute_Id, 
                   pQualifier4_Attr_id => RecCAT.Qualifier4_Attribute_Id, 
                   pQualifier5_Attr_id => RecCAT.Qualifier5_Attribute_Id, 
                   pQualifier6_Attr_id => RecCAT.Qualifier6_Attribute_Id
                  ) Loop

                vCheck_Element_Array(RecCat.Tax_Element_Number) := vCheck_Element_Array(RecCat.Tax_Element_Number)+1;
                    
                If vCheck_Element_Array(RecCat.Tax_Element_Number) > 1 then
                  cInternalErrorCode := 3172;
                  Exit;
                End If;
                
                vTax_Rate       := Null;
                vTax_Rate_id    := Null;
                vNew_Tax_Amount := Null;

                -- 
                -- Using the Instance view (Tax Lookups) fetch the right tax code and rate
                --
                cInternalErrorCode    := 3171; 
                vLast_Recovery_Amount := 0;
                
                For RecRAT in TRATES(
                      pTax_Regime_Code       => RecCAT.Tax_Regime_Code,
                      pTax_Rate_Code         => RecQual.Tax_Rate_Code,
                      pTax                   => RecQual.Tax,
                      pTax_Jurisdiction_code => RecQual.Tax_Jurisdiction,
                      pTax_Status_Code       => RecQual.Tax_Status_Code,
                      pTransaction_date      => cTransaction_Date
                      ) Loop
           
                    vTaxCnt := vTaxCnt+1;
                    cInternalErrorCode := 0;

                    vPercentage       := RecRat.Percentage_Rate;
                    vTax_Rate_id      := RecRat.Tax_Rate_Id;
                    vTax_Code         := RecQual.Tax_Rate_Code;
                    
                    If vTaxCnt > 1 and RecCat.Compound_Tax = 'Y' then
                      vNew_Taxable_Amount := vRunning_Total;
                      vDummy_Taxable_Amount := vDummy_Running_Total;
                    Else
                      vNew_Taxable_Amount := vBase_Taxable_Amount;
                      vRunning_Total      := 0; 
                      vDummy_Running_Total := 0; 
                      vDummy_Taxable_Amount := vDummy_Base_Amount;
                    End If;
                 
                    vNew_Tax_Amount   := RoundX(vNew_Taxable_Amount * (vPercentage*0.01),vPrecision,vRounding_Type);
                    vTax_Amount       := vTax_Amount + vNew_Tax_Amount;
                    vRunning_Total    := (vNew_Taxable_Amount+vNew_Tax_Amount);

                    vDummy_New_Tax_Amount  := RoundX(vDummy_Taxable_Amount * (vPercentage*0.01),vPrecision,vRounding_Type);
                    vDummy_Tax_Amount      := vDummy_Tax_Amount + vDummy_New_Tax_Amount;
                    vDummy_Running_Total := (vDummy_Taxable_Amount+vDummy_New_Tax_Amount);
                    
                    -- Taxable
                    xxcp_te_base.gTax_Element_Set(vTaxCnt).Tax_Element_Number := RecCat.Tax_Element_Number;                  
                    xxcp_te_base.gTax_Element_Set(vTaxCnt).Tax_Rate_Code      := vTax_Code;
                    xxcp_te_base.gTax_Element_Set(vTaxCnt).Tax_Lookup_id      := RecQual.Tax_Lookup_Id;
                    xxcp_te_base.gTax_Element_Set(vTaxCnt).Tax_Regime_Name    := RecCAT.Tax_Regime_Code;
                    xxcp_te_base.gTax_Element_Set(vTaxCnt).Tax_Rate_id        := vTax_Rate_id;
                    xxcp_te_base.gTax_Element_Set(vTaxCnt).Tax_Rate           := (vPercentage*0.01);
                    xxcp_te_base.gTax_Element_Set(vTaxCnt).Tax_Amount         := vNew_Tax_Amount; 
                    
                    -- Recovery
                    vLast_Recovery_Rate := nvl((RecRAT.Recovery_Rate*0.01),100);
                    xxcp_te_base.gTax_Element_Set(vTaxCnt).Recovery_Rate      := vLast_Recovery_Rate;
                    xxcp_te_base.gTax_Element_Set(vTaxCnt).Recovery_Rate_Code := RecRat.Recovery_Type_Code;
                    
                    If xxcp_te_base.gTax_Element_Set(vTaxCnt).Recovery_Rate = 100 then
                      xxcp_te_base.gTax_Element_Set(vTaxCnt).Recovery_Amount := 0;
                    Else
                      xxcp_te_base.gTax_Element_Set(vTaxCnt).Recovery_Amount := RoundX(vNew_Tax_Amount*(xxcp_te_base.gTax_Element_Set(vTaxCnt).Recovery_Rate),vPrecision,vRounding_Type);
                    End If;
                    
                    vLast_Recovery_Amount := roundX(nvl(xxcp_te_base.gTax_Element_Set(vTaxCnt).Recovery_Amount,0),vPrecision,vRounding_Type);

                    vRecovery_Amount := vRecovery_Amount + vLast_Recovery_Amount;
                    
                    xxcp_te_base.gTax_Element_Set(vTaxCnt).Tax_Jurisdiction  := RecQual.Tax_Jurisdiction;                    
                    vTax_Rate   := nvl(vTax_Rate,0) + xxcp_te_base.gTax_Element_Set(vTaxCnt).Tax_Rate;                 
          
                    xxcp_te_base.gTax_Element_Set(vTaxCnt).Tax_Minus_Recovery_Amount := nvl(vNew_Tax_Amount,0)-nvl(xxcp_te_base.gTax_Element_Set(vTaxCnt).Recovery_Amount,0);          

                    Exit; -- Only the first first show work                 
 
                End Loop;              

            End Loop;
            
            vTax_Gross_Amount  := vNew_Taxable_Amount + vNew_Tax_Amount;
                  
            If xxcp_global.Trace_on = 'Y' then
                vTax_Lookup_Trace := vTax_Lookup_Trace ||
                '  Tax Regime <'||RecCat.Tax_Regime_Code ||'> Element <'||to_char(RecCAT.Tax_Element_Number)||'>'||Chr(10)||
                '   Q1 <'||xxcp_foundation.MC_DynamicAttribute(RecCAT.Qualifier1_Attribute_Id)||'> '||
                '   Q2 <'||xxcp_foundation.MC_DynamicAttribute(RecCAT.Qualifier2_Attribute_Id)||'> '||
                '   Q3 <'||xxcp_foundation.MC_DynamicAttribute(RecCAT.Qualifier3_Attribute_Id)||'> '||
                '   Q4 <'||xxcp_foundation.MC_DynamicAttribute(RecCAT.Qualifier4_Attribute_Id)||'> '||
                '   Q5 <'||xxcp_foundation.MC_DynamicAttribute(RecCAT.Qualifier5_Attribute_Id)||'> '||
                '   Q6 <'||xxcp_foundation.MC_DynamicAttribute(RecCAT.Qualifier6_Attribute_Id)||'> '|| Chr(10);
                      
                vTax_Lookup_Trace := vTax_Lookup_Trace || '      Tax Amount <'||vNew_Tax_Amount||'> Tax Rate <'||vTax_Rate||'> Tax Rate id <'||vTax_Rate_id||'> Rec. Amt <'||vLast_Recovery_Amount||'> Rec. Rate <'||vLast_Recovery_Rate||'>'||chr(10);

            End If;
                  
            -- Tax Rate required but not found
            If cInternalErrorCode = 3171 then 
                         vErrMsg := 'Q1 <'||xxcp_foundation.MC_DynamicAttribute(RecCAT.Qualifier1_Attribute_Id)||'> '||
                                    'Q2 <'||xxcp_foundation.MC_DynamicAttribute(RecCAT.Qualifier2_Attribute_Id)||'> '||
                                    'Q3 <'||xxcp_foundation.MC_DynamicAttribute(RecCAT.Qualifier3_Attribute_Id)||'> '||
                                    'Q4 <'||xxcp_foundation.MC_DynamicAttribute(RecCAT.Qualifier4_Attribute_Id)||'> '||
                                    'Q5 <'||xxcp_foundation.MC_DynamicAttribute(RecCAT.Qualifier5_Attribute_Id)||'> '||
                                    'Q6 <'||xxcp_foundation.MC_DynamicAttribute(RecCAT.Qualifier6_Attribute_Id)||'> ';

                         xxcp_foundation.FndWriteError(cInternalErrorCode,'Tax Regime <'||RecCat.Tax_Regime_Code ||'>',vErrMsg);
                         
                         Exit;
                    
                    ElsIf cInternalErrorCode = 3172 then 
                         vErrMsg := 'Q1 <'||xxcp_foundation.MC_DynamicAttribute(RecCAT.Qualifier1_Attribute_Id)||'> '||
                                    'Q2 <'||xxcp_foundation.MC_DynamicAttribute(RecCAT.Qualifier2_Attribute_Id)||'> '||
                                    'Q3 <'||xxcp_foundation.MC_DynamicAttribute(RecCAT.Qualifier3_Attribute_Id)||'> '||
                                    'Q4 <'||xxcp_foundation.MC_DynamicAttribute(RecCAT.Qualifier4_Attribute_Id)||'> '||
                                    'Q5 <'||xxcp_foundation.MC_DynamicAttribute(RecCAT.Qualifier5_Attribute_Id)||'> '||
                                    'Q6 <'||xxcp_foundation.MC_DynamicAttribute(RecCAT.Qualifier6_Attribute_Id)||'> ';

                         xxcp_foundation.FndWriteError(cInternalErrorCode,'Tax Regime <'||RecCat.Tax_Regime_Code||
                                                                          '> Tax Rate id <'||vTax_Rate_id||
                                                                          '> Clashing Regime Id <'||RecCAT.Tax_Regime_Id||'>',vErrMsg);
                         
                         Exit;
                    End If;                  
          End Loop;
      
          xxcp_te_base.gTax_Record_Set(1).Tax_Engine_id         := 1;                  
          xxcp_te_base.gTax_Record_Set(1).Element_Count         := vTaxCnt;                  
          -- Starting Point
          xxcp_te_base.gTax_Record_Set(1).Taxable_Amount        := vBase_Taxable_Amount; --vNew_Taxable_Amount;
          xxcp_te_base.gTax_Record_Set(1).Taxable_Currency      := vNew_Taxable_Currency;
          
          If nvl(vDummy_Tax_Amount,0) != 0 then   
            vTax_Rate := vDummy_Tax_Amount / vDummy_Base_Amount;
          End If;  
          
          -- Results
          xxcp_te_base.gTax_Record_Set(1).Tax_Rate              := vTax_Rate;
          xxcp_te_base.gTax_Record_Set(1).Tax_Amount            := vTax_Amount;
          xxcp_te_base.gTax_Record_Set(1).Tax_Gross_Amount      := vBase_Taxable_Amount+nvl(vTax_Amount,0);
          xxcp_te_base.gTax_Record_Set(1).Recovery_Amount       := vRecovery_Amount;                    
          xxcp_te_base.gTax_Record_Set(1).Tax_Minus_Recovery_Amount := nvl(vTax_Amount,0)-nvl(vRecovery_Amount,0);          
          -- internals
          xxcp_wks.I1009_Array(92) := vTax_Amount;
          xxcp_wks.I1009_Array(93) := vTax_Rate;
          xxcp_wks.I1009_Array(94) := vBase_Taxable_Amount;
          xxcp_wks.I1009_Array(95) := xxcp_te_base.gTax_Record_Set(1).Tax_Gross_Amount;
          xxcp_wks.I1009_Array(160):= vNew_Taxable_Currency;
          -- Trace
          vTax_Lookup_Trace := vTax_Lookup_Trace||chr(10)||
          ' Taxable Amount <'||to_char(vBase_Taxable_Amount)||'> Currency <'|| vNew_Taxable_Currency||'> Tax <'||to_char(vTax_Amount)||
          '> Gross Amount <'||to_char(xxcp_te_base.gTax_Record_Set(1).Tax_Gross_Amount)||'> Recovery <'||to_char( vRecovery_Amount)||'>';
           
          -- clear tax record
          If cInternalErrorCode != 0 then
            xxcp_te_base.gTax_Record_Set.delete;  -- write data          
          End If;
        End If;      
      End If;
   End If;

   If xxcp_global.Trace_on = 'Y' then
    xxcp_foundation.FndWriteError(100,'TRACE (Tax Engine Regime)',vTax_Lookup_Trace);
   End If;
      
  End Calc_Tax_Rates;                      
 
-- !! ***********************************************************************************************************
-- !!
-- !!                                   PACKAGE INITIALIZATION
-- !!
-- !! ***********************************************************************************************************
  Procedure Init is
  
  Cursor c1 is
   select substr(Lookup_Code,1,3) Currency_Code
     from xxcp_lookups
    where lookup_type = 'COMMON EXCHANGE CURRENCY'
      and enabled_flag = 'Y';

  Cursor c2 is
   select substr(Lookup_Code,1,30) Exhange_Type
     from xxcp_lookups
    where lookup_type = 'DEFAULT EXCHANGE RATE TYPE'
      and enabled_flag = 'Y';


  Cursor c3 is
   select substr(Lookup_Code,1,1) PathHeaders
     from xxcp_lookups
    where lookup_type = 'TRANSACTION PATH HEADERS'
      and enabled_flag = 'Y';

  Cursor c4 is
   select substr(Lookup_Code,1,1) Balancing_Trace
     from xxcp_lookups
    where lookup_type = 'BALANCING TRACE'
      and enabled_flag = 'Y';
      
  Cursor c5 is 
     select profile_value Default_trading_relationship 
       from xxcp_sys_profile 
      where Profile_Name = 'DEFAULT TRADING RELATIONSHIP' 
        and profile_category = 'EV';

  Begin

    xxcp_global.SystemDate := Sysdate;
    gEngineStartDate       := xxcp_global.SystemDate;
    gCursors.delete;
    gCur_cnt               := 0;

    -- Currency Conversion
    For Rec in C1 loop
      gCUCO_Common_Currency := rec.currency_code;
    End Loop;

    -- Exchange Type
    For Rec in C2 loop
      gCUCO_Dft_Exchange_Type := rec.Exhange_Type;
    End Loop;

    -- DumpHeaders
    For Rec in C3 loop
      gPathHeaders := rec.PathHeaders;
    End Loop;

    -- Balancing Trace
    For Rec in C4 loop
      gBalancing_Trace := rec.Balancing_Trace;
    End Loop;
    
    -- Default Trading Relationship
    For Rec in C5 Loop 
      gDefault_trading_relationship := rec.default_trading_relationship; 
    End Loop;

   End Init;

Begin
  Init;
End XXCP_TE_BASE;
/
