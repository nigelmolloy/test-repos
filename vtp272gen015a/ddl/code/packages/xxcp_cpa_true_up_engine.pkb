CREATE OR REPLACE PACKAGE BODY XXCP_CPA_TRUE_UP_ENGINE AS
  /******************************************************************************
                          V I R T A L  T R A D E R  L I M I T E D
              (Copyright 2008-2009 Virtual Trader Ltd. All rights Reserved)

     NAME:       XXCP_CPA_TRUE_UP_ENGINE
     PURPOSE:    Wrapper for CPA True Up

     REVISIONS:
     Ver        Date      Author  Description
     ---------  --------  ------- ------------------------------------
     02.04.00   10/12/08  Simon   First Coding
     02.04.01   11/03/09  Simon   Added 7015 Category logic
     02.04.02   02/04/09  Simon   Add data_source (cost category).
     02.04.03   01/05/09  Simon   Add Category_data_source.
     02.04.04   02/05/09  Simon   Fix TU_Category_data_source.
     02.04.05   18/05/09  Simon   Add Instance_id.
     02.04.06   21/05/09  Simon   Replan Modifications.
     02.04.07   01/06/09  Simon   Accounting Date now based on Posting Period
                                  rather than vt_transaction_date.
     02.04.08   04/06/09  Simon   Added Trial True Up.
                                  Added Trial Re-plan.
     02.04.09   11/06/09  Simon   Add Transaction Types STD,REV,REP.
     02.04.10   26/06/09  Simon   Auto run replan after true-up if required.
     02.04.11   02/07/09  Simon   Add I1009_ArrayGLOBAL.
                                  Add Attributes 21..30 in Create_Replan.
                                  Add Partner_Tax_Reg_Id and Owner_Tax_Reg_id into Create_Replan.
                                  Add Mc_Qualifier1 and Old Cost_Category_Id to Create_Replan.
                                  Add Old Data_Source to Create_Replam.
     02.04.12   13/07/09  Simon   Replace XXCP_cost_plus_interface attributes with meaningful columns..
     02.04.13   21/07/09  Simon   REV transactions to stamp reversal_process_history_id on history.
     02.04.14   24/07/09  Simon   REV transactions to stamp reversal_interface_id on history.
     02.04.15   28/07/09  Simon   Modify Get_Payer_Details.
     02.04.16   21/09/09  Simon   Check for gDefCostCatUsed.
     02.04.17   10/10/09  Simon   Fix Forecast_id for header and attributes during replan.
                                  REV records now populate vt_replan_amount from XXCP_process_history values.
     02.06.01   11/08/09  Simon   Moved gReplan_Reprocess from xxcp_cpa_true_up_engine.
     02.06.02   20/10/09  Simon   Grouping now done by transaction_table not assignment_id..
     02.06.03   22/10/09  Simon   Add new VT_GROUPING_COLUMNs to commit process.
     02.06.04   28/12/09  Keith   Added Calc_legal_exch_rate
     03.06.05   14/03/12  Simon   Add facility to optionally end in warning/error (OOD-33). 
     03.06.06   20/04/12  Simon   Fixed TER cursor to use company_number instead of Tax_ragistration_id (OOD-111). 
     03.06.07   14/09/12  Simon   Added Cost_Plus_Set_Id (OOD-293). 
     03.06.07A  10/12/12  Keith   Dynamic Explosion
  ******************************************************************************/

	-- Editor Setting: 2 space tab stop

  gEngine_Trace       varchar2(1);

  vTiming             xxcp_global.gTiming_rec;
	vTimingCnt          pls_integer;

  gBalancing_Array    XXCP_DYNAMIC_ARRAY := XXCP_DYNAMIC_ARRAY(' ',' ',' ',' ',' ',' ',' ',' ',' ',' ');
  gTrans_Table_Array  XXCP_DYNAMIC_ARRAY := XXCP_DYNAMIC_ARRAY();

  gReplan_Created     varchar2(1) := 'N';

  t_forecast_id       XXCP_DYNAMIC_ARRAY := XXCP_DYNAMIC_ARRAY();  -- Hold Forecast_ids used during STD processing. 

  gEngine_Error_Found         boolean := FALSE;
  gForce_Job_Error            boolean := FALSE;
  gForce_Job_Warning          boolean := FALSE;
  gCPA_Attr_Override_exists   boolean := FALSE;

  -- *************************************************************
  --
  --                     Software Version Control
  --
  -- This package can be checked with SQL to ensure it's installed
  -- select packagename.software_version from dual;
  -- *************************************************************
  Function Software_Version RETURN VARCHAR2 IS
  BEGIN
    RETURN('Version [03.06.07A] Build Date [10-DEC-2012] Name [XXCP_CPA_TRUE_UP_ENGINE]');
  END Software_Version;

  -- *************************************************************
  --
  --                  ClearWorkingStorage
  --
  -- *************************************************************
  Procedure ClearWorkingStorage IS

  begin
    -- Processed Records
	  xxcp_wks.WORKING_CNT := 0;
	  xxcp_wks.WORKING_RCD.Delete;
    -- Current Records before processing
    xxcp_wks.SOURCE_CNT := 0;
	  XXCP_WKS.SOURCE_ROWID.Delete;
    XXCP_WKS.SOURCE_STATUS.Delete;
    XXCP_WKS.SOURCE_SUB_CODE.Delete;
    -- Balancing
    xxcp_wks.BALANCING_CNT := 0;
    XXCP_WKS.BALANCING_RCD.Delete;

  End ClearWorkingStorage;

  --  !! ***********************************************************************************************************
  --  !!
  --  !!                                        Create_Replan
  --  !!                If one part of the wrapper finds a change whilst processing a record,
  --  !!                          then we need to replan the transaction.
  --  !!
  --  !! ***********************************************************************************************************
  Procedure Create_Replan(cInternalErrorCode in out Number,
                          cCalendar_id       in     Number,
                          cInstance_id       in     Number) IS
  
    vStart_of_Year_id number(8) := 0;

    -- Forecast_Period  - 1
    vPrv_Forecast_Period_id number := xxcp_reporting.Get_Period_Offset(xxcp_wks.gActual_Costs_Rec(1).Period_id,
                                                                       cCalendar_id,
                                                                       -1,
                                                                       cInstance_id);
    -- True_Up_Period  - 1
    vPrv_True_Up_Period_id  number := xxcp_reporting.Get_Period_Offset(xxcp_wks.gActual_Costs_Rec(1).Collection_Period_id,
                                                                       cCalendar_id,
                                                                       -1,
                                                                       cInstance_id);
    
    Cursor TU_Rev is
    select 
       h.transaction_id,
       h.accounting_date,
       h.period_set_name_id,
       h.period_id,
       h.process_history_id, -- Process History id
       h.cost_category_id,
       h.trading_set_id,
       Nvl(ph.Entered_cr,0) - Nvl(ph.Entered_dr,0) true_up_amount, --h.true_up_amount,
       p.End_Date,
       h.payer1,
       h.payer1_percent,
       h.payer2,
       h.payer2_percent,
       h.Inter_payer,
       h.Account_Type,
       h.currency_code,
       h.partner_tax_reg_id,
       h.owner_tax_reg_id,
       a.mc_qualifier1,
       h.data_source,
       h.cost_plus_set_id
    from XXCP_true_up_history h,
         xxcp_instance_gl_periods_v p,
         XXCP_process_history ph,
         XXCP_transaction_attributes a
    where h.period_id between vStart_of_Year_id and vPrv_True_Up_Period_id
      and h.company            = xxcp_wks.gActual_Costs_Rec(1).Company
      and h.department         = xxcp_wks.gActual_Costs_Rec(1).Department
      and h.account            = xxcp_wks.gActual_Costs_Rec(1).Account
      and h.currency_code      = xxcp_wks.gActual_Costs_Rec(1).Currency_Code
      and h.period_set_name_id = xxcp_wks.gActual_Costs_Rec(1).Period_Set_Name_id
      and h.cost_plus_set_id   = xxcp_wks.gActual_Costs_Rec(1).cost_plus_set_id
      and h.active             = 'Y'
      and p.instance_id        = cInstance_id
      and p.period_set_name_id = h.period_set_name_id
      and h.period_id          = (p.period_year*100)+p.period_num
      and h.process_history_id = ph.process_history_id
      and ph.attribute_id      = a.attribute_id;

    Cursor TU_Rep is
    select 
       h.transaction_id,
       h.accounting_date,
       h.period_set_name_id,
       h.period_id,
       h.process_history_id, -- Process History id
       h.cost_category_id,
       h.trading_set_id,
       h.true_up_amount,
       p.End_Date,
       h.payer1,
       h.payer1_percent,
       h.payer2,
       h.payer2_percent,
       h.Inter_payer,
       h.Account_Type,
       h.currency_code,
       h.partner_tax_reg_id,
       h.owner_tax_reg_id,
       h.data_source,
       h.cost_plus_set_id
    from XXCP_true_up_history h,
         xxcp_instance_gl_periods_v p 
    where h.period_id between vStart_of_Year_id and vPrv_True_Up_Period_id
      and h.company            = xxcp_wks.gActual_Costs_Rec(1).Company
      and h.department         = xxcp_wks.gActual_Costs_Rec(1).Department
      and h.account            = xxcp_wks.gActual_Costs_Rec(1).Account
      and h.currency_code      = xxcp_wks.gActual_Costs_Rec(1).Currency_Code
      and h.period_set_name_id = xxcp_wks.gActual_Costs_Rec(1).Period_Set_Name_id
      and h.cost_plus_set_id   = xxcp_wks.gActual_Costs_Rec(1).cost_plus_set_id
      and h.active             = 'Y'
      and p.instance_id        = cInstance_id
      and p.period_set_name_id = h.period_set_name_id
      and h.period_id          = (p.period_year*100)+p.period_num
      and h.trading_set_id     = to_number(xxcp_global.gCommon(1).current_trading_set_id);
  
    -- Also need Forecast History
    Cursor FC_Rev is
    select 
       h.transaction_id,
       h.accounting_date,
       h.period_set_name_id,
       h.period_id,
       h.process_history_id, -- Process History id
       h.cost_category_id,
       h.trading_set_id,
       Nvl(ph.Entered_cr,0) - Nvl(ph.Entered_dr,0) uplifted_amount,
       p.end_date,
       h.payer1,
       h.payer1_percent,
       h.payer2,
       h.payer2_percent,
       h.Inter_payer,
       h.Account_Type,
       h.currency_code,
       h.partner_tax_reg_id,       
       h.owner_tax_reg_id,
       a.mc_qualifier1,
       h.data_source,
       h.cost_plus_set_id
    from XXCP_forecast_history h,
         xxcp_instance_gl_periods_v p,
         XXCP_process_history ph,
         XXCP_transaction_attributes a
    where h.period_id between vStart_of_Year_id and vPrv_Forecast_Period_id
      and h.company            = xxcp_wks.gActual_Costs_Rec(1).Company
      and h.department         = xxcp_wks.gActual_Costs_Rec(1).Department
      and h.account            = xxcp_wks.gActual_Costs_Rec(1).Account
      and h.currency_code      = xxcp_wks.gActual_Costs_Rec(1).Currency_Code
      and h.period_set_name_id = xxcp_wks.gActual_Costs_Rec(1).Period_Set_Name_id
      and h.cost_plus_set_id   = xxcp_wks.gActual_Costs_Rec(1).cost_plus_set_id
      and h.active             = 'Y'
      and p.instance_id        = cInstance_id
      and p.period_set_name_id = h.period_set_name_id
      and h.period_id           = (p.period_year*100)+p.period_num
      and h.process_history_id = ph.process_history_id
      and ph.attribute_id      = a.attribute_id;

    Cursor FC_Rep is
    select 
       h.transaction_id,
       h.accounting_date,
       h.period_set_name_id,
       h.period_id,
       h.process_history_id, -- Process History id
       h.cost_category_id,
       h.trading_set_id,
       h.uplifted_amount,
       p.end_date,
       h.payer1,
       h.payer1_percent,
       h.payer2,
       h.payer2_percent,
       h.Inter_payer,
       h.Account_Type,
       h.currency_code,
       h.partner_tax_reg_id,
       h.owner_tax_reg_id,
       h.data_source,
       h.cost_plus_set_id
    from XXCP_forecast_history h, 
         xxcp_instance_gl_periods_v p 
    where h.period_id between vStart_of_Year_id and vPrv_Forecast_Period_id
      and h.company            = xxcp_wks.gActual_Costs_Rec(1).Company
      and h.department         = xxcp_wks.gActual_Costs_Rec(1).Department
      and h.account            = xxcp_wks.gActual_Costs_Rec(1).Account
      and h.currency_code      = xxcp_wks.gActual_Costs_Rec(1).Currency_Code
      and h.period_set_name_id = xxcp_wks.gActual_Costs_Rec(1).Period_Set_Name_id
      and h.cost_plus_set_id   = xxcp_wks.gActual_Costs_Rec(1).cost_plus_set_id
      and h.active             = 'Y'
      and p.instance_id        = cInstance_id
      and p.period_set_name_id = h.period_set_name_id
      and h.period_id           = (p.period_year*100)+p.period_num
      and h.trading_set_id     = to_number(xxcp_global.gCommon(1).current_trading_set_id);
    
  BEGIN
     
    --
    -- Create new Cost Category Change Interface Records
    --
    vStart_of_Year_id := nvl(xxcp_te_engine.Current_Start_of_Year_id,0);

    For Rec in TU_Rev Loop
      Begin
              
        -- Replan True-up Reversal
        Insert into XXCP_cost_plus_interface(
          vt_status,
          vt_source_assignment_id,
          vt_transaction_table,
          vt_transaction_type ,
          vt_transaction_class,    
          vt_transaction_ref,       
          vt_transaction_id,        
          vt_transaction_date,    
          vt_request_id,           
          Creation_Date,
          Created_by,         
          Period_Set_Name_id,
          Posting_Period_id,    
          Collection_Period_id,
          Vt_replan_company,             
          Vt_replan_department,          
          Vt_replan_account,             
          Vt_replan_transaction_id,      
          Vt_replan_trading_set_id,      
          Vt_replan_amount,              
          Vt_replan_currency_code,       
          Vt_replan_owner_tax_reg_id,    
          Vt_replan_partner_tax_reg_id,  
          Vt_replan_mc_qualifier1,       
          Vt_replan_cost_category_id,    
          Vt_replan_category_data_source,
          Vt_replan_process_history_id,  
          Vt_replan_period_id,           
          Vt_replan_payer1,              
          Vt_replan_payer1_percent,      
          Vt_replan_payer2,              
          Vt_replan_payer2_percent,      
          Vt_replan_inter_payer,         
          Vt_replan_account_type,
          cost_plus_set_id
          )
        Values( 
          'NEW',
           xxcp_global.gCommon(1).Current_Assignment_id,
          'TRUE-UP',
          'REV',
          'ALL',
           xxcp_global.gCommon(1).Current_Interface_Ref,
           xxcp_cost_plus_trx_id.nextval, --rec.transaction_id,
           rec.end_date, --xxcp_global.gCommon(1).current_Accounting_date, -- rec.accounting_date,
           0,
           xxcp_global.Systemdate,
           xxcp_global.User_id,
           rec.period_set_name_id,
           xxcp_wks.gActual_Costs_Rec(1).Period_id,
           rec.period_id,
           -- Meaningful Columns
           xxcp_wks.gActual_Costs_Rec(1).Company,
           xxcp_wks.gActual_Costs_Rec(1).Department,
           xxcp_wks.gActual_Costs_Rec(1).Account,
           rec.transaction_id,
           rec.trading_set_id,
           rec.true_up_amount,
           rec.currency_code,
           rec.Owner_Tax_Reg_id,
           rec.Partner_Tax_Reg_id,
           rec.mc_qualifier1,
           rec.cost_category_id,
           rec.data_source,
           rec.process_history_id,
           rec.period_id,
           null, --rec.payer1,
           null, --rec.payer1_percent,
           null, --rec.payer2,
           null, --rec.payer2_percent,
           null, --rec.Inter_payer,
           null, --rec.Account_Type,
           rec.cost_plus_set_id
          );

        Exception When Others Then Null;    
      End;
    End Loop;



    For Rec in TU_Rep Loop
      Begin
              
        -- Replan True-up Replacement
        Insert into XXCP_cost_plus_interface(
          vt_status,
          vt_source_assignment_id,
          vt_transaction_table,
          vt_transaction_type ,
          vt_transaction_class,    
          vt_transaction_ref,       
          vt_transaction_id,        
          vt_transaction_date,    
          vt_request_id,           
          Creation_Date,
          Created_by,         
          Period_Set_Name_id,
          Posting_Period_id,    
          Collection_Period_id,
          Vt_replan_company,             
          Vt_replan_department,          
          Vt_replan_account,             
          Vt_replan_transaction_id,      
          Vt_replan_trading_set_id,      
          Vt_replan_amount,              
          Vt_replan_currency_code,       
          Vt_replan_owner_tax_reg_id,    
          Vt_replan_partner_tax_reg_id,  
          Vt_replan_mc_qualifier1,       
          Vt_replan_cost_category_id,    
          Vt_replan_category_data_source,
          Vt_replan_process_history_id,  
          Vt_replan_period_id,           
          Vt_replan_payer1,              
          Vt_replan_payer1_percent,      
          Vt_replan_payer2,              
          Vt_replan_payer2_percent,      
          Vt_replan_inter_payer,         
          Vt_replan_account_type,
          cost_plus_set_id
          )
        Values( 
          'NEW',
           xxcp_global.gCommon(1).Current_Assignment_id,
          'TRUE-UP',
          'REP',
          'ALL',
           xxcp_global.gCommon(1).Current_Interface_Ref,
           xxcp_cost_plus_trx_id.nextval, --rec.transaction_id,
           rec.end_date, --xxcp_global.gCommon(1).current_Accounting_date, -- rec.accounting_date,
           0,
           xxcp_global.Systemdate,
           xxcp_global.User_id,
           rec.period_set_name_id,
           xxcp_wks.gActual_Costs_Rec(1).Period_id,
           rec.period_id,
           -- Meaningful Columns
           xxcp_wks.gActual_Costs_Rec(1).Company,
           xxcp_wks.gActual_Costs_Rec(1).Department,
           xxcp_wks.gActual_Costs_Rec(1).Account,
           rec.transaction_id,
           rec.trading_set_id,
           rec.true_up_amount,
           rec.currency_code,
           rec.Owner_Tax_Reg_id,
           rec.Partner_Tax_Reg_id,
           null, --rec.mc_qualifier1,
           rec.cost_category_id,
           rec.data_source,
           rec.process_history_id,
           rec.period_id,
           null, --rec.payer1,
           null, --rec.payer1_percent,
           null, --rec.payer2,
           null, --rec.payer2_percent,
           null, --rec.Inter_payer,
           null, --rec.Account_Type
           rec.cost_plus_set_id           
          );

        Exception When Others Then Null;    
      End;
    End Loop;
 
    For Rec in FC_Rev Loop
      Begin
        -- Replan Forecast Reversal
        Insert into XXCP_cost_plus_interface(
          vt_status,
          vt_source_assignment_id,
          vt_transaction_table,
          vt_transaction_type ,
          vt_transaction_class,    
          vt_transaction_ref,       
          vt_transaction_id,        
          vt_transaction_date,    
          vt_request_id,           
          Creation_Date,
          Created_by,         
          Period_Set_Name_id,    
          Posting_Period_id,    
          Collection_Period_id,
          Vt_replan_company,             
          Vt_replan_department,          
          Vt_replan_account,             
          Vt_replan_transaction_id,      
          Vt_replan_trading_set_id,      
          Vt_replan_amount,              
          Vt_replan_currency_code,       
          Vt_replan_owner_tax_reg_id,    
          Vt_replan_partner_tax_reg_id,  
          Vt_replan_mc_qualifier1,       
          Vt_replan_cost_category_id,    
          Vt_replan_category_data_source,
          Vt_replan_process_history_id,  
          Vt_replan_period_id,           
          Vt_replan_payer1,              
          Vt_replan_payer1_percent,      
          Vt_replan_payer2,              
          Vt_replan_payer2_percent,      
          Vt_replan_inter_payer,         
          Vt_replan_account_type,
          cost_plus_set_id
          )
        Values( 
          'NEW',
           xxcp_global.gCommon(1).Current_Assignment_id,
          'FORECAST',
          'REV',
          'ALL',
           xxcp_global.gCommon(1).Current_Interface_Ref,
           xxcp_cost_plus_trx_id.nextval, --rec.transaction_id,
           rec.end_date,--xxcp_global.gCommon(1).current_Accounting_date, --rec.accounting_date,
           0,
           xxcp_global.Systemdate,
           xxcp_global.User_id,
           rec.period_set_name_id,
           xxcp_wks.gActual_Costs_Rec(1).Period_id,
           rec.period_id,
           -- Meaningful Columns
           xxcp_wks.gActual_Costs_Rec(1).Company,
           xxcp_wks.gActual_Costs_Rec(1).Department,
           xxcp_wks.gActual_Costs_Rec(1).Account,
           rec.transaction_id,
           rec.trading_set_id,
           rec.uplifted_amount,
           rec.currency_code,
           rec.Owner_Tax_Reg_id,
           rec.Partner_Tax_Reg_id,
           rec.mc_qualifier1,
           rec.cost_category_id,
           rec.data_source,
           rec.process_history_id,
           rec.period_id,
           null, --rec.payer1,
           null, --rec.payer1_percent,
           null, --rec.payer2,
           null, --rec.payer2_percent,
           null, --rec.Inter_payer,
           null,  --rec.Account_Type
           rec.cost_plus_set_id           
          );
        Exception When Others Then Null;    
      End;
    End Loop;

    For Rec in FC_Rep Loop
      Begin
        -- Replan Forecast Replacement
        Insert into XXCP_cost_plus_interface(
          vt_status,
          vt_source_assignment_id,
          vt_transaction_table,
          vt_transaction_type ,
          vt_transaction_class,    
          vt_transaction_ref,       
          vt_transaction_id,        
          vt_transaction_date,    
          vt_request_id,           
          Creation_Date,
          Created_by,         
          Period_Set_Name_id,    
          Posting_Period_id,    
          Collection_Period_id,
          Vt_replan_company,             
          Vt_replan_department,          
          Vt_replan_account,             
          Vt_replan_transaction_id,      
          Vt_replan_trading_set_id,      
          Vt_replan_amount,              
          Vt_replan_currency_code,       
          Vt_replan_owner_tax_reg_id,    
          Vt_replan_partner_tax_reg_id,  
          Vt_replan_mc_qualifier1,       
          Vt_replan_cost_category_id,    
          Vt_replan_category_data_source,
          Vt_replan_process_history_id,  
          Vt_replan_period_id,           
          Vt_replan_payer1,              
          Vt_replan_payer1_percent,      
          Vt_replan_payer2,              
          Vt_replan_payer2_percent,      
          Vt_replan_inter_payer,         
          Vt_replan_account_type,
          cost_plus_set_id
          )
        Values( 
          'NEW',
           xxcp_global.gCommon(1).Current_Assignment_id,
          'FORECAST',
          'REP',
          'ALL',
           xxcp_global.gCommon(1).Current_Interface_Ref,
           xxcp_cost_plus_trx_id.nextval, --rec.transaction_id,
           rec.end_date,--xxcp_global.gCommon(1).current_Accounting_date, --rec.accounting_date,
           0,
           xxcp_global.Systemdate,
           xxcp_global.User_id,
           rec.period_set_name_id,
           xxcp_wks.gActual_Costs_Rec(1).Period_id,
           rec.period_id,
           -- Meaningful Columns
           xxcp_wks.gActual_Costs_Rec(1).Company,
           xxcp_wks.gActual_Costs_Rec(1).Department,
           xxcp_wks.gActual_Costs_Rec(1).Account,
           rec.transaction_id,
           rec.trading_set_id,
           rec.uplifted_amount,
           rec.currency_code,
           rec.Owner_Tax_Reg_id,
           rec.Partner_Tax_Reg_id,
           null, --rec.mc_qualifier1,
           rec.cost_category_id,
           rec.data_source,
           rec.process_history_id,
           rec.period_id,
           null, --rec.payer1,
           null, --rec.payer1_percent,
           null, --rec.payer2,
           null, --rec.payer2_percent,
           null, --rec.Inter_payer,
           null, --rec.Account_Type
           rec.cost_plus_set_id           
          );
        Exception When Others Then Null;    
      End;
    End Loop;
    
    gReplan_Created := 'Y';
   
  --  ClearWorkingStorage;

  END Create_Replan;


  --  !! ***********************************************************************************************************
  --  !!   Stop_Whole_Transaction
  --  !! ***********************************************************************************************************
  PROCEDURE Stop_Whole_Transaction is
  Begin
   -- Set all transactions in group to error status
   Begin
     Update XXCP_cost_plus_interface
      set vt_status              = 'WAITING'
         ,vt_internal_Error_code = 0 -- Associated errors 
         ,VT_DATE_PROCESSED  = xxcp_global.SystemDate
       WHERE vt_request_id = xxcp_global.gCommon(1).Current_request_id
         AND vt_Source_Assignment_id = xxcp_global.gCommon(1).current_assignment_id
         AND vt_parent_trx_id = xxcp_global.gCommon(1).current_parent_Trx_id
         AND NOT vt_status IN ('TRASH', 'IGNORED');
     End;
     -- Update Record that actually caused the problem.
     Begin
     Update XXCP_cost_plus_interface
        set vt_status          = 'WAITING'
           ,VT_DATE_PROCESSED  = sysdate 
       WHERE ROWID = xxcp_global.gCommon(1).current_source_rowid
         AND vt_request_id = xxcp_global.gCommon(1).current_request_id;
     End;

    ClearWorkingStorage;

  End Stop_Whole_Transaction;

  --  !! ***********************************************************************************************************
  --  !!
  --  !!                                   Error_Whole_Transaction
  --  !!                If one part of the wrapper finds a problem whilst processing a record,
  --  !!                          then we need to error the whole transaction.
  --  !!
  --  !! ***********************************************************************************************************
  Procedure Error_Whole_Transaction(cInternalErrorCode in out Number) IS
  BEGIN

    -- Flag Error
    gEngine_Error_Found := TRUE;  

    -- Set all transactions in group to error status
    Begin
      Update XXCP_cost_plus_interface
         set vt_status = 'ERROR', vt_internal_Error_code = 12823 -- Associated errors
       where vt_request_id = xxcp_global.gCommon(1).Current_request_id
         and vt_Source_Assignment_id = xxcp_global.gCommon(1).current_assignment_id
         and vt_transaction_table = 'TRUE-UP'                      -- CHG20081212SJS
         and vt_parent_trx_id = xxcp_global.gCommon(1).current_parent_Trx_id
         and NOT vt_status in ('TRASH', 'IGNORED');
    End;
    -- Update Record that actually caused the problem.
    Begin
      update XXCP_cost_plus_interface
         set vt_status              = 'ERROR',
             vt_internal_error_code = cInternalErrorCode,
             vt_date_processed      = xxcp_global.SystemDate
       where rowid = xxcp_global.gCommon(1).current_source_rowid
         and vt_transaction_table = 'TRUE-UP'                      -- CHG20081212SJS
         and vt_request_id = xxcp_global.gCommon(1).current_request_id;
    End;

    If xxcp_global.gCommon(1).Custom_events = 'Y' then

      xxcp_custom_events.ON_TRANSACTION_ERROR(xxcp_global.gCommon(1).Source_id,
                                              xxcp_global.gCommon(1).current_assignment_id,
                                              xxcp_global.gCommon(1).current_source_rowid,
                                              xxcp_global.gCommon(1).current_Transaction_Table,
                                              xxcp_global.gCommon(1).current_Parent_Trx_id,
                                              xxcp_global.gCommon(1).current_transaction_id,
                                              cInternalErrorCode);
    End If;

    ClearWorkingStorage;

  END Error_Whole_Transaction;

  --
  --   !! ***********************************************************************************************************
  --   !!
  --   !!                                     No_Action_GL_Transaction
  --   !!                                If there are no records to process.
  --   !!
  --   !! ***********************************************************************************************************
  --
  Procedure No_Action_GL_Transaction(cSource_Rowid in rowid) IS
  BEGIN
    -- Update Record that actually had not action required
    Begin
      update XXCP_cost_plus_interface
--         set vt_status              = 'SUCCESSFUL',
         set vt_status              = 'TRIAL',
             vt_internal_error_code = Null,
             vt_date_processed      = xxcp_global.SystemDate,
             vt_status_code         = 7003
       where rowid = csource_rowid;
    End;
  END NO_Action_GL_Transaction;

  --  !! ***********************************************************************************************************
  --  !!
  --  !!                                   Transaction_Group_Stamp
  --  !!                 Stamp Incomming record with parent Trx to create groups of
  --  ||                    transactions that resemble on document transaction.
  --  !!
  --  !! ***********************************************************************************************************
  Procedure Transaction_Replan_Group_Stamp(cStamp_Parent_Trx     in varchar2,
                                    cSource_assignment_id in number,
                                    cInternalErrorCode    out number) IS


    vInternalErrorCode number(8) := 0;
    vStatement         varchar2(6000);

    -- Distinct list of Transaction Tables
    -- populated in Set_Running_Status
    CURSOR cTrxTbl is
      SELECT transaction_table,
             nvl(grouping_rule_id,0) grouping_rule_id
      FROM   XXCP_sys_source_tables
      where  source_id = xxcp_global.gCommon(1).Source_id
      and    transaction_table in (select * from TABLE(CAST(gTrans_Table_Array AS XXCP_DYNAMIC_ARRAY)));

    Cursor c1(pSource_assignment_id in number,
              pTransaction_Table    IN VARCHAR2) IS
      select distinct 'update XXCP_cost_plus_interface ' ||
                      '  set vt_parent_trx_id = ' || t.Parent_trx_column || ' ' ||
                      'where vt_status = :1 ' ||
                      '  and vt_source_assignment_id = :2 ' ||
                      '  and vt_transaction_table = :3 ' ||
                      '  and vt_request_id = :4 ' Statement_line,
                      t.Parent_trx_column,
                      t.Transaction_Table
        from XXCP_cost_plus_interface       g,
             XXCP_sys_source_tables  t,
             XXCP_source_assignments x
       where g.vt_request_id           = xxcp_global.gCommon(1).Current_request_id
         and g.vt_status               = 'GROUPING'
         and g.vt_transaction_Table    = t.TRANSACTION_TABLE
         and t.source_id               = x.Source_id
         and g.vt_transaction_table  in ('TRUE-UP','FORECAST')
         and g.vt_transaction_type   in ('REP','REV')
         and x.source_assignment_id    = pSource_assignment_id
         and g.vt_source_assignment_id = x.source_assignment_id
         and g.vt_transaction_table    = pTransaction_Table;     -- 02.06.02

    Cursor er1(pSource_assignment_id in number,
               pTransaction_Table    IN VARCHAR2) IS
      select g.vt_interface_id,
             g.rowid source_rowid,
             g.vt_transaction_table
        from XXCP_cost_plus_interface g
       where g.vt_request_id           = xxcp_global.gCommon(1).Current_request_id
         and g.vt_status               = 'GROUPING'
         and g.vt_source_assignment_id = psource_assignment_id
         and g.vt_transaction_table  in ('TRUE-UP','FORECAST')
         and g.vt_transaction_type   in ('REP','REV')
         and g.vt_transaction_table    = pTransaction_Table      -- 02.06.02
         and g.vt_parent_trx_id is null;

  Begin

    vTiming(vTimingCnt).Group_Stamp_Start := to_char(sysdate, 'HH24:MI:SS');

    For cTblRec in cTrxTbl Loop  -- Loop for all Transaction Tables in Array within SA.
      
      If cTblRec.Grouping_Rule_Id > 0 THEN  --02.06.02
        XXCP_DYNAMIC_SQL.Apply_Grouping_Rules(cNew_Status        => 'GROUPING', 
                                              cInternalErrorCode => vInternalErrorCode,
                                              cTransaction_Table => cTblRec.Transaction_Table);

        -- Error Checking for Null Parent Trx id
        For er1Rec in er1(cSource_assignment_id,
                          cTblRec.transaction_table) LOOP

          update XXCP_cost_plus_interface l
             set vt_status                = 'ERROR',
                 vt_parent_trx_id         = null,
                 l.vt_internal_error_code = 11090
           where l.vt_request_id = xxcp_global.gCommon(1).Current_request_id
             and l.vt_transaction_table = er1rec.vt_transaction_table;

        End Loop;

      Else
        -- Mass update of Parent Trx id
        For rec in c1(cSource_assignment_id,
                      cTblRec.transaction_table) LOOP
          If rec.Parent_trx_column is null then
            vInternalErrorCode := 10665;
            xxcp_foundation.FndWriteError(vInternalErrorCode,'Transaction Table <' || Rec.Transaction_Table || '>');
          Else
            vStatement := rec.Statement_line;
            Begin

              Execute Immediate vStatement
                Using 'GROUPING', cSource_Assignment_id, rec.transaction_table, xxcp_global.gCommon(1).current_request_id;

            Exception
              when OTHERS then
                vInternalErrorCode := 10667;
                xxcp_foundation.FndWriteError(vInternalErrorCode, SQLERRM,vStatement);
            End;
          End If;
        End loop;
      End If;
    End Loop;
    
    cInternalErrorCode := vInternalErrorCode;

  End Transaction_Replan_Group_Stamp;
  --  !! ***********************************************************************************************************
  --  !!
  --  !!                                   Transaction_Group_Stamp
  --  !!                 Stamp Incomming record with parent Trx to create groups of
  --  ||                    transactions that resemble on document transaction.
  --  !!
  --  !! ***********************************************************************************************************
  Procedure Transaction_Group_Stamp(cStamp_Parent_Trx     in varchar2,
                                    cSource_assignment_id in number,
                                    cInternalErrorCode    out number) IS


    vInternalErrorCode number(8) := 0;
    vStatement         varchar2(6000);

    -- Distinct list of Transaction Tables
    -- populated in Set_Running_Status
    CURSOR cTrxTbl is
      SELECT transaction_table,
             nvl(grouping_rule_id,0) grouping_rule_id
      FROM   XXCP_sys_source_tables
      where  source_id = xxcp_global.gCommon(1).Source_id
      and    transaction_table in (select * from TABLE(CAST(gTrans_Table_Array AS XXCP_DYNAMIC_ARRAY)));

    Cursor c1(pSource_assignment_id in number,
              pTransaction_Table    IN VARCHAR2) IS
      select distinct 'update XXCP_cost_plus_interface ' ||
                      '  set vt_parent_trx_id = ' || t.Parent_trx_column || ' ' ||
                      'where vt_status = :1 ' ||
                      '  and vt_source_assignment_id = :2 ' ||
                      '  and vt_transaction_table = :3 ' ||
                      '  and vt_request_id = :4 ' Statement_line,
                      t.Parent_trx_column,
                      t.Transaction_Table
        from XXCP_cost_plus_interface       g,
             XXCP_sys_source_tables  t,
             XXCP_source_assignments x
       where g.vt_request_id           = xxcp_global.gCommon(1).Current_request_id
         and g.vt_status               = 'GROUPING'
         and g.vt_transaction_Table    = t.TRANSACTION_TABLE
         and t.source_id               = x.Source_id
         and g.vt_transaction_table    = 'TRUE-UP'                      -- CHG20081212SJS
         and g.vt_transaction_type     = 'STD'
         and x.source_assignment_id    = pSource_assignment_id
         and g.vt_source_assignment_id = x.source_assignment_id
         and g.vt_transaction_table    = pTransaction_Table;     -- 02.06.02

    Cursor er1(pSource_assignment_id in number,
              pTransaction_Table    IN VARCHAR2) IS
      select g.vt_interface_id,
             g.rowid source_rowid,
             g.vt_transaction_table
        from XXCP_cost_plus_interface g
       where g.vt_request_id           = xxcp_global.gCommon(1).Current_request_id
         and g.vt_status               = 'GROUPING'
         and g.vt_source_assignment_id = psource_assignment_id
         and g.vt_transaction_table    = 'TRUE-UP'                      -- CHG20081212SJS
         and g.vt_transaction_type     = 'STD'
         and g.vt_transaction_table    = pTransaction_Table     -- 02.06.02
         and g.vt_parent_trx_id is null;

  Begin

    vTiming(vTimingCnt).Group_Stamp_Start := to_char(sysdate, 'HH24:MI:SS');

    For cTblRec in cTrxTbl Loop  -- Loop for all Transaction Tables in Array within SA.
      
      If cTblRec.Grouping_Rule_Id > 0 THEN  --02.06.02
        XXCP_DYNAMIC_SQL.Apply_Grouping_Rules(cNew_Status        => 'GROUPING', 
                                              cInternalErrorCode => vInternalErrorCode,
                                              cTransaction_Table => cTblRec.Transaction_Table);

        -- Error Checking for Null Parent Trx id
        For er1Rec in er1(cSource_assignment_id,
                          cTblRec.transaction_table) LOOP

          update XXCP_cost_plus_interface l
             set vt_status                = 'ERROR',
                 vt_parent_trx_id         = null,
                 l.vt_internal_error_code = 11090
           where l.vt_request_id = xxcp_global.gCommon(1).Current_request_id
             and l.vt_transaction_table = er1rec.vt_transaction_table;

        End Loop;

      ElsIf cStamp_Parent_Trx = 'N' then
        -- Mass update of Parent Trx id
        For rec in c1(cSource_assignment_id,
                      cTblRec.transaction_table) LOOP
          If rec.Parent_trx_column is null then
            vInternalErrorCode := 10665;
            xxcp_foundation.FndWriteError(vInternalErrorCode,'Transaction Table <' || Rec.Transaction_Table || '>');
          Else
            vStatement := rec.Statement_line;
            Begin

              Execute Immediate vStatement
                Using 'GROUPING', cSource_Assignment_id, rec.transaction_table, xxcp_global.gCommon(1).current_request_id;

            Exception
              when OTHERS then
                vInternalErrorCode := 10667;
                xxcp_foundation.FndWriteError(vInternalErrorCode, SQLERRM,vStatement);
            End;
          End If;
        End loop;
      End If;
    End loop;
    
    cInternalErrorCode := vInternalErrorCode;

  End Transaction_Group_Stamp;

  -- ************************************************************************
  --
  --       FIND_DYNAMICATTRIBUTE 
  --
  -- Returns the correct value depending on which array is being pointed at
  -- ************************************************************************
  Function Find_DynamicAttribute(cPosition in number,
                                 cRecord   in number) Return Varchar2 is

    vAttribute varchar2(100);

  Begin

   If nvl(cPosition,0) != 0 then
    If cPosition between 9000 and 9999 then
        vAttribute := RTRIM(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(cPosition - 9000));
    ElsIf cPosition between 1000 and 1999 then
      -- Assignment
        vAttribute := RTRIM(xxcp_wks.WORKING_RCD(cRecord).D1001_Array(cPosition - 1000));
    ElsIf cPosition between 2000 and 2999 then
      -- IC Pricing
        vAttribute := RTRIM(xxcp_wks.WORKING_RCD(cRecord).D1002_Array(cPosition - 2000));
    ElsIf cPosition between 3000 and 3999 then
      -- Transaction
        vAttribute := RTRIM(xxcp_wks.WORKING_RCD(cRecord).D1003_Array(cPosition - 3000));
    ElsIf cPosition between 4000 and 4999 then
      -- Transaction Pricing
        vAttribute := RTRIM(xxcp_wks.WORKING_RCD(cRecord).D1004_Array(cPosition - 4000));
    ElsIf cPosition between 6000 and 6999 then
      -- Column Attributes
        vAttribute := RTRIM(xxcp_wks.WORKING_RCD(cRecord).D1006_Array(cPosition - 6000));
    ElsIf cPosition between 5000 and 5999 then
      -- Custom Events
        vAttribute := RTRIM(xxcp_wks.WORKING_RCD(cRecord).D1005_Array(cPosition - 5000));
    End If;

   End If;

   Return(vAttribute);
  
  Exception
    When others then
      Return(null); 
  
  End Find_DynamicAttribute;

  --
  -- Check_CPA_Attribute_Override
  --
  Procedure Check_CPA_Attribute_Override is
    Cursor c1 is select 'Y'
                 from   xxcp_sys_profile
                 where  profile_name in ('UPLIFT RATE ATTRIBUTE',
                                         'GROWTH RATE ATTRIBUTE',
                                         'AVG PERIOD COST ATTRIBUTE',
                                         'TOTAL COST ATTRIBUTE',
                                         'CONSIDERED PERIODS ATTRIBUTE',
                                         'CURRENCY CODE ATTRIBUTE',
                                         'COST CATEGORY ID ATTRIBUTE',
                                         'UPLIFTED AMOUNT ATTRIBUTE',
                                         'TRUE UP AMOUNT ATTRIBUTE',
                                         'PERIODS REQUIRED ATTRIBUTE',
                                         'PAYER1 ATTRIBUTE',
                                         'PAYER1 PERCENT ATTRIBUTE',
                                         'PAYER2 ATTRIBUTE',
                                         'PAYER2 PERCENT ATTRIBUTE',
                                         'INTERMEDIATE PAYER ATTRIBUTE',
                                         'ACCOUNT ATTRIBUTE')
                 and    profile_category = 'CP';  
  Begin
    for c1_rec in c1 loop
      gCPA_Attr_Override_exists := TRUE;
      exit;
    end loop; 
  End Check_CPA_Attribute_Override;

  --
  -- Set_CPA_Internal_Attributes
  --
  Procedure Set_CPA_Internal_Attributes (cRecord IN NUMBER) is
    
  Begin
     
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(106) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(106),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('UPLIFT RATE ATTRIBUTE'),cRecord));
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(107) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(107),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('GROWTH RATE ATTRIBUTE'),cRecord));
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(108) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(108),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('AVG PERIOD COST ATTRIBUTE'),cRecord));
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(109) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(109),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('TOTAL COST ATTRIBUTE'),cRecord));
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(110) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(110),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('CONSIDERED PERIODS ATTRIBUTE'),cRecord));
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(111) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(111),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('CURRENCY CODE ATTRIBUTE'),cRecord));
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(112) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(112),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('COST CATEGORY ID ATTRIBUTE'),cRecord));
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(114) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(114),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('UPLIFTED AMOUNT ATTRIBUTE'),cRecord));
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(116) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(116),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('TRUE UP AMOUNT ATTRIBUTE'),cRecord));
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(122) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(122),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('PERIODS REQUIRED ATTRIBUTE'),cRecord));
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(123) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(123),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('PAYER1 ATTRIBUTE'),cRecord));
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(124) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(124),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('PAYER1 PERCENT ATTRIBUTE'),cRecord));
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(125) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(125),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('PAYER2 ATTRIBUTE'),cRecord));
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(126) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(126),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('PAYER2 PERCENT ATTRIBUTE'),cRecord));
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(127) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(127),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('INTERMEDIATE PAYER ATTRIBUTE'),cRecord));
    xxcp_wks.WORKING_RCD(cRecord).I1009_Array(128) := nvl(xxcp_wks.WORKING_RCD(cRecord).I1009_Array(128),Find_DynamicAttribute(xxcp_te_base.Get_CPA_Attributes('ACCOUNT ATTRIBUTE'),cRecord));

  End Set_CPA_Internal_Attributes;

  --
  -- Replan History
  --
  Procedure Replan_history(cSource_Rowid            in  Rowid
                          ,cParent_Rowid            in  Rowid
                          ,cPeriod_Set_Name_id      in  number
                          ,cTarget_table            in  varchar2
                          ,cTarget_Instance_id      in  number
                          ,cProcess_history_id      in  number
                          ,cTarget_assignment_id    in  number
                          ,cAttribute_id            in  number
                          ,cRecord_type             in  varchar2
                          ,cRequest_id              in  number
                          ,cStatus                  in  varchar2
                          ,cInterface_id            in  number
                          ,cModel_Ctl_id            in  number
                          ,cRule_id                 in  number
                          ,cTrading_Set_id          in  number
                          ,cTax_registration_id     in  number
                          ,cEntered_Rounding_Amount in  number
                          ,cAccount_Rounding_amount in  number
                          ,cTransaction_Type        in  varchar2
                          ,cI1009_Array             in  xxcp_dynamic_array
                          ,cErrorMessage           out  NOCOPY varchar2
                          ,cInternalErrorCode   in out  NOCOPY number) is
  begin

    if xxcp_wks.gActual_Costs_Rec(1).Transaction_Table = 'TRUE-UP' then

      if cTransaction_Type = 'REV' then
      --
      -- Mark Current True-Up as Inactive
      --
      update XXCP_true_up_history
        set    trial_ind = 'Y',
               reversal_process_history_id = cProcess_history_id,
               reversal_interface_id       = cInterface_id
        where  process_history_id = xxcp_wks.gActual_Costs_Rec(1).vt_replan_process_history_id
        and    period_id          = xxcp_wks.gActual_Costs_Rec(1).vt_replan_period_id
        and    transaction_id     = xxcp_wks.gActual_Costs_Rec(1).vt_replan_transaction_id
        and    trading_set_id     = xxcp_wks.gActual_Costs_Rec(1).vt_replan_trading_set_id
        and    cost_plus_set_id   = xxcp_wks.gActual_Costs_Rec(1).cost_plus_set_id; 
      
      elsif cTransaction_Type = 'REP' then

            
      --
      -- Insert True Up
      --
       Insert into XXCP_fc_forecast_history
                    ( FORECAST_ID,
                     CPA_TYPE,
                      PROCESS_HISTORY_ID,
                      PERIOD_ID,
                      SOURCE_ASSIGNMENT_ID,
                      TRANSACTION_ID,
                      TARGET_ASSIGNMENT_ID,
                      TRADING_SET_ID,
                      INTERFACE_ID,
                      ACCOUNTING_DATE,
                      POSTING_PERIOD_ID,
                      --
                      COMPANY   ,
                      DEPARTMENT,
                      ACCOUNT   ,
                      COST_PLUS_SET_ID,
                      --
                      UPLIFT_RATE       ,
                      GROWTH_RATE       ,
                      AVG_PERIOD_COST   ,
                      TOTAL_COST        ,
                      TRUE_UP_AMOUNT,
                      COST_CATEGORY_ID  ,
                      DATA_SOURCE       ,        -- 02.04.02
                      CURRENCY_CODE     ,
                      CONSIDERED_PERIODS,
                      PERIOD_SET_NAME_ID,
         --            ACTIVE            ,        -- 02.04.02
                      OWNER_TAX_REG_ID,
                      PARTNER_TAX_REG_ID,
                   --
                      PAYER1            ,         
                      PAYER1_PERCENT    ,
                      PAYER2            ,         
                      PAYER2_PERCENT    ,
                      INTER_PAYER       ,
                      ACCOUNT_TYPE      ,    
                     --
                      CREATION_DATE     ,
                      CREATED_BY        ,
                      LAST_UPDATE_LOGIN
                    )
                    ( select
                      xxcp_global.gCommon(1).forecast_id,
                     'RT',
                      cProcess_history_id,
                     PERIOD_ID, -- Period Id
                     xxcp_global.gCommon(1).current_assignment_id,
                     xxcp_wks.gActual_Costs_Rec(1).vt_replan_transaction_id,
                     cTarget_assignment_id,
                     cTrading_Set_id,
                     cInterface_id,
                     ACCOUNTING_DATE,
                      POSTING_PERIOD_ID,
                      --
                     xxcp_wks.gActual_Costs_Rec(1).Company, -- Company
                     xxcp_wks.gActual_Costs_Rec(1).Department, -- Department
                     xxcp_wks.gActual_Costs_Rec(1).Account, -- Account
                     xxcp_wks.gActual_Costs_Rec(1).Cost_Plus_Set_id,
                      --
                     UPLIFT_RATE,
                     GROWTH_RATE,
                     AVG_PERIOD_COST,
                     TOTAL_COST,
                      TRUE_UP_AMOUNT,
                     xxcp_wks.gActual_Costs_Rec(1).attribute1,  -- Cost Category id
                     xxcp_wks.gActual_Costs_Rec(1).attribute2,  -- 02.04.03
                     CURRENCY_CODE,      -- Currency Code
                     CONSIDERED_PERIODS, -- Considered Periods
                     cPeriod_Set_Name_id,
                     cI1009_Array(1),    -- Owner Tax Reg Id
                     cI1009_Array(2),    -- Partner Tax Reg Id
                     --
                     xxcp_wks.gActual_Costs_Rec(1).vt_replan_payer1,         -- Payer1
                     xxcp_wks.gActual_Costs_Rec(1).vt_replan_payer1_percent, -- Payer1_Percent
                     xxcp_wks.gActual_Costs_Rec(1).vt_replan_payer2,         -- Payer2
                     xxcp_wks.gActual_Costs_Rec(1).vt_replan_payer2_percent, -- Payer2_Percent
                     xxcp_wks.gActual_Costs_Rec(1).vt_replan_inter_payer,    -- Intermediate_Payer
                     xxcp_wks.gActual_Costs_Rec(1).vt_replan_account_type,   -- Account_Type
                    --
                     xxcp_global.Systemdate,
                     xxcp_global.User_id,
                     xxcp_global.Login_id
                      from  XXCP_true_up_history
                     where process_history_id = xxcp_wks.gActual_Costs_Rec(1).vt_replan_process_history_id
                     and   period_id          = xxcp_wks.gActual_Costs_Rec(1).vt_replan_period_id
                     and   transaction_id     = xxcp_wks.gActual_Costs_Rec(1).vt_replan_transaction_id
                     and   trading_set_id     = xxcp_wks.gActual_Costs_Rec(1).vt_replan_trading_set_id    
                     and   cost_plus_set_id   = xxcp_wks.gActual_Costs_Rec(1).cost_plus_set_id    
                    );

       end if;


    elsif xxcp_wks.gActual_Costs_Rec(1).Transaction_Table = 'FORECAST' then

      if cTransaction_Type = 'REV' then
      --
      -- Mark Current Forecast as Inactive
      --
      update XXCP_forecast_history
        set    trial_ind = 'Y',
               reversal_process_history_id = cProcess_history_id,
               reversal_interface_id       = cInterface_id
        where  process_history_id = xxcp_wks.gActual_Costs_Rec(1).vt_replan_process_history_id
        and    period_id          = xxcp_wks.gActual_Costs_Rec(1).vt_replan_period_id 
        and    transaction_id     = xxcp_wks.gActual_Costs_Rec(1).vt_replan_transaction_id  
        and    trading_set_id     = xxcp_wks.gActual_Costs_Rec(1).vt_replan_trading_set_id
        and    cost_plus_set_id   = xxcp_wks.gActual_Costs_Rec(1).cost_plus_set_id;    

      elsif cTransaction_Type = 'REP' then
            
      --
      -- Insert Forecast
      --
       Insert into XXCP_fc_forecast_history
                    ( FORECAST_ID,
                     CPA_TYPE,
                      PROCESS_HISTORY_ID,
                     PERIOD_ID,
                      SOURCE_ASSIGNMENT_ID,
                      TRANSACTION_ID,
                      TARGET_ASSIGNMENT_ID,
                      TRADING_SET_ID,
                      INTERFACE_ID,
                      ACCOUNTING_DATE,
                      POSTING_PERIOD_ID,
                      --
                      COMPANY   ,
                      DEPARTMENT,
                      ACCOUNT   ,
                      COST_PLUS_SET_ID,
                      --
                      UPLIFT_RATE       ,
                      GROWTH_RATE       ,
                      AVG_PERIOD_COST   ,
                      TOTAL_COST       ,
                      UPLIFTED_AMOUNT,
                      COST_CATEGORY_ID  ,
                     DATA_SOURCE       ,        -- 02.04.02
                      CURRENCY_CODE     ,
                      CONSIDERED_PERIODS,
                     PERIOD_SET_NAME_ID,
         --            ACTIVE            ,        -- 02.04.02
                      OWNER_TAX_REG_ID,
                      PARTNER_TAX_REG_ID,
                   --
                      PAYER1            ,         
                      PAYER1_PERCENT    ,
                      PAYER2            ,         
                      PAYER2_PERCENT    ,
                      INTER_PAYER       ,    
                      ACCOUNT_TYPE      ,    
                      --
                      CREATION_DATE     ,
                      CREATED_BY        ,
                      LAST_UPDATE_LOGIN
                    )
                    ( select
                      xxcp_global.gCommon(1).forecast_id,
                     'RF',
                      cProcess_history_id,
                     PERIOD_ID, -- Period Id
                     xxcp_global.gCommon(1).current_assignment_id,
                     xxcp_wks.gActual_Costs_Rec(1).vt_replan_transaction_id,
                     cTarget_assignment_id,
                     cTrading_Set_id,
                     cInterface_id,
                     ACCOUNTING_DATE,
                      POSTING_PERIOD_ID,
                      --
                     xxcp_wks.gActual_Costs_Rec(1).Company, -- Company
                     xxcp_wks.gActual_Costs_Rec(1).Department, -- Department
                     xxcp_wks.gActual_Costs_Rec(1).Account, -- Account
                     xxcp_wks.gActual_Costs_Rec(1).cost_plus_set_id,
                      --
                     UPLIFT_RATE,
                     GROWTH_RATE,
                     AVG_PERIOD_COST,
                     TOTAL_COST,
                      UPLIFTED_AMOUNT,
                     xxcp_wks.gActual_Costs_Rec(1).attribute1,  -- Cost Category id
                     xxcp_wks.gActual_Costs_Rec(1).attribute2,  -- 02.04.03
                     CURRENCY_CODE,  -- Currency Code
                     CONSIDERED_PERIODS, -- Considered Periods
                     cPeriod_Set_Name_id,
                     cI1009_Array(1),    -- Owner Tax Reg Id
                     cI1009_Array(2),    -- Partner Tax Reg Id
                     --
                     xxcp_wks.gActual_Costs_Rec(1).vt_replan_payer1,         -- Payer1
                     xxcp_wks.gActual_Costs_Rec(1).vt_replan_payer1_percent, -- Payer1_Percent
                     xxcp_wks.gActual_Costs_Rec(1).vt_replan_payer2,         -- Payer2
                     xxcp_wks.gActual_Costs_Rec(1).vt_replan_payer2_percent, -- Payer2_Percent
                     xxcp_wks.gActual_Costs_Rec(1).vt_replan_inter_payer,    -- Intermediate_Payer
                     xxcp_wks.gActual_Costs_Rec(1).vt_replan_account_type,   -- Account_Type
                      --
                     xxcp_global.Systemdate,
                     xxcp_global.User_id,
                     xxcp_global.Login_id
                      from  XXCP_forecast_history
                     where process_history_id = xxcp_wks.gActual_Costs_Rec(1).vt_replan_process_history_id
                     and   period_id          = xxcp_wks.gActual_Costs_Rec(1).vt_replan_period_id
                     and   transaction_id     = xxcp_wks.gActual_Costs_Rec(1).vt_replan_transaction_id
                     and   trading_set_id     = xxcp_wks.gActual_Costs_Rec(1).vt_replan_trading_set_id    
                     and   cost_plus_set_id   = xxcp_wks.gActual_Costs_Rec(1).cost_plus_set_id   
                    );

             end if;
             end if;

  end Replan_History;

  --
  -- Process_True_Up_Hist_Insert
  --
  Procedure Process_True_Up_Hist_Insert(cSource_Rowid            in  Rowid
                                       ,cParent_Rowid            in  Rowid
                                       ,cPeriod_Set_Name_id      in  number
                                       ,cTarget_table            in  varchar2
                                       ,cTarget_Instance_id      in  number
                                       ,cProcess_history_id      in  number
                                       ,cTarget_assignment_id    in  number
                                       ,cAttribute_id            in  number
                                       ,cRecord_type             in  varchar2
                                       ,cRequest_id              in  number
                                       ,cStatus                  in  varchar2
                                       ,cInterface_id            in  number
                                       ,cModel_Ctl_id            in  number
                                       ,cRule_id                 in  number
                                       ,cTrading_Set_id          in  number
                                       ,cTax_registration_id     in  number
                                       ,cEntered_Rounding_Amount in  number
                                       ,cAccount_Rounding_amount in  number
                                       ,cI1009_Array             in  xxcp_dynamic_array
                                       ,cErrorMessage           out  NOCOPY varchar2
                                       ,cInternalErrorCode   in out  NOCOPY number) is

  Begin

--        Insert into XXCP_true_up_history
        Insert into XXCP_fc_forecast_history
                   ( FORECAST_ID,
                     CPA_TYPE,
                     PROCESS_HISTORY_ID,
                     PERIOD_ID,
                     SOURCE_ASSIGNMENT_ID,
                     TRANSACTION_ID,
                     TARGET_ASSIGNMENT_ID,
                     TRADING_SET_ID,
                     INTERFACE_ID,
                     ACCOUNTING_DATE,
                     POSTING_PERIOD_ID,
                     --
                     COMPANY   ,
                     DEPARTMENT,
                     ACCOUNT   ,
                     COST_PLUS_SET_ID,
                     --
                     UPLIFT_RATE       ,
                     GROWTH_RATE       ,
                     AVG_PERIOD_COST   ,
                     TOTAL_COST       ,
                     TRUE_UP_AMOUNT,
                     COST_CATEGORY_ID  ,
                     DATA_SOURCE       ,        -- 02.04.02
                     CURRENCY_CODE     ,
                     CONSIDERED_PERIODS,
                     PERIOD_SET_NAME_ID,
         --            ACTIVE            ,        -- 02.04.02
                     OWNER_TAX_REG_ID,
                     PARTNER_TAX_REG_ID,
                   --
                     PAYER1            ,         
                     PAYER1_PERCENT    ,
                     PAYER2            ,
                     PAYER2_PERCENT    ,
                     INTER_PAYER       ,
                     ACCOUNT_TYPE      ,
                     TAX_AGREEMENT_NUMBER,
                   --
                     CREATION_DATE     ,
                     CREATED_BY        ,
                     LAST_UPDATE_LOGIN
                   )
                   VALUES
                   ( xxcp_global.gCommon(1).forecast_id,
                     xxcp_global.gCommon(1).CPA_Type,
                     cProcess_History_ID,
                     xxcp_wks.gActual_Costs_Rec(1).Collection_Period_id, -- Period Id
                     xxcp_global.gCommon(1).current_assignment_id,
                     cI1009_Array(28),
                     cTarget_assignment_id,
                     cTrading_Set_id, --to_number(xxcp_global.gCommon(1).current_trading_set_id),
                     cInterface_id,
                     cI1009_Array(52), -- Accounting Date
                     xxcp_wks.gActual_Costs_Rec(1).Period_id,
                     --
                     xxcp_wks.gActual_Costs_Rec(1).Company, -- Company
                     xxcp_wks.gActual_Costs_Rec(1).Department, -- Department
                     xxcp_wks.gActual_Costs_Rec(1).Account, -- Account
                     xxcp_wks.gActual_Costs_Rec(1).Cost_Plus_Set_id, -- Account
                     --
                     cI1009_Array(106), -- Uplift
                     cI1009_Array(107), -- Growth Rate
                     cI1009_Array(108), -- Avg Period Cost
                     cI1009_Array(109), -- Total Cost
                     cI1009_Array(116), -- True Up Amount
                     cI1009_Array(112), -- Cost Category id
                     xxcp_wks.gActual_Costs_Rec(1).TU_Category_Data_Source,  -- 02.04.03
             --        xxcp_wks.gActual_Costs_Rec(1).Data_Source,  -- 02.04.02
                     cI1009_Array(111), -- Currency Code
                     cI1009_Array(110), -- Considered Periods
                     cPeriod_Set_Name_id,
             --        'Y',                                        -- 02.04.02
                     cI1009_Array(1),   -- Owner Tax Reg Id
                     cI1009_Array(2), --xxcp_wks.gActual_Costs_Rec(1).Payer, --cI1009_Array(2),   -- Partner Tax Reg Id
                     --
                     cI1009_Array(123),  -- Payer1
                     cI1009_Array(124),  -- Payer1_Percent
                     cI1009_Array(125),  -- Payer2
                     cI1009_Array(126),  -- Payer2_Percent
                     cI1009_Array(127),  -- Intermediate_Payer
                     cI1009_Array(128),  -- Account_Type  
                     cI1009_Array(171),  -- Tax Agreement Number                 
                     --
                     xxcp_global.Systemdate,
                     xxcp_global.User_id,
                     xxcp_global.Login_id
                    );



  Exception when OTHERS then
--    cErrorMessage := 'Error Inserting into XXCP_TRUE_UP_HISTORY - '||SQLERRM;
    cErrorMessage := 'Error Inserting into XXCP_FC_FORECAST_HISTORY - '||SQLERRM;
    cInternalErrorCode := 12808;
  End Process_True_Up_Hist_Insert;


  -- !! ***********************************************************************************************************
  -- !!
  -- !!                                   FlushRecordsToTarget
  -- !!     Control The process of moving the Array elements throught to creating the new output records
  -- !!
  -- !! ***********************************************************************************************************
  Function FlushRecordsToTarget(cPeriod_Set_Name_id        in number,
                                cGlobal_Rounding_Tolerance in number,
                                cGlobalPrecision           in number,
                                cTransaction_Type          in varchar2,
                                cInternalErrorCode         in out number,
                                cReplan                    in varchar2 default 'N')
    return number is

    vCnt           pls_integer := 0;
    c              pls_integer := 0;

    vEntered_DR    Number := 0;
    vEntered_CR    Number := 0;
    j              pls_integer := 0;
    fe             pls_integer := xxcp_wks.WORKING_CNT;

    vRC            pls_integer := xxcp_wks.SOURCE_CNT;
    vRC_Records    pls_integer := 0;
    vRC_Error      pls_integer := 0;

    vStatus XXCP_cost_plus_interface.vt_status%type;


    vCustom_Attributes xxcp_dynamic_array;

    vErrorMessage  varchar2(3000);
		vZero_Action   pls_integer := 0;

    UDI_Cnt        pls_integer := 0;
    vReplanned     boolean := FALSE;
  BEGIN
    If fe > 0 then
      xxcp_te_base.Account_Rounding(cGlobal_Rounding_Tolerance,
                                    cGlobalPrecision,
                                    cTransaction_Type,
                                    vEntered_DR,
                                    vEntered_CR,
                                    cInternalErrorCode);
    End If;

    -- Check that for each attribute record we have a records for output.
    For c in 1 .. vRC Loop
      vRC_Error := 0;
      FOR j in 1 .. fe LOOP
        If xxcp_wks.Source_rowid(c) = xxcp_wks.WORKING_RCD(j).Source_rowid then
          xxcp_wks.WORKING_RCD(j).Source_Pos := c;
          xxcp_wks.SOURCE_STATUS(c) := 'FLUSH';
          vRC_Error   := 1;
          vRC_Records := vRC_Records + 1;
        end if;
      End loop;
      IF vRC_Error = 0 THEN
		    xxcp_wks.SOURCE_SUB_CODE(c) := 7003;
		    xxcp_wks.SOURCE_STATUS(c) := 'NO RULES';
		    No_Action_GL_Transaction(xxcp_wks.Source_rowid(c));
      END IF;
    END LOOP;

    Savepoint TARGET_INSERT;

    If cInternalErrorCode = 0 and vRC_Records > 0 then


      For j in 1 .. fe Loop
        Exit when cInternalErrorCode <> 0;

        vCnt := vCnt + 1;

        -- Custom Events
        If xxcp_global.gCommon(1).Custom_events = 'Y' then
   				xxcp_te_base.Custom_Event_Insert_Row(j,cInternalErrorCode);
        End If;

        -- Override CPA Internal Attributes?
        If gCPA_Attr_Override_exists then
          Set_CPA_Internal_Attributes(j);
        End if;
        
        -- Tag Zero Account Value rows
        If cInternalErrorCode = 0 then

		      xxcp_global.gCommon(1).current_source_rowid := xxcp_wks.Source_rowid(xxcp_wks.WORKING_RCD(j).Source_Pos);

					-- Find the Action to take for Zero Accounted values
          vZero_Action := xxcp_te_base.Zero_Suppression_Rule(j);

          IF vZero_Action > 0 then

               Select xxcp_process_history_seq.nextval into xxcp_wks.WORKING_RCD(j) .Process_History_id from dual;
               -- don't post ZAV

                 If vZero_Action = 3 then
             --       XXCP_PROCESS_DML_CPA.Process_Oracle_Insert(
                    XXCP_FC_PROCESS_DML_CPA.Process_Oracle_Insert(
										                                  cSource_Rowid        => xxcp_wks.WORKING_RCD(j).Source_rowid,
																											cParent_Rowid        => Null,
                                                      cTarget_Table        => xxcp_wks.WORKING_RCD(j).Target_Table,
                                                      cTarget_Instance_id  => xxcp_wks.WORKING_RCD(j).Target_Instance_id,
                                                      cProcess_History_id  => xxcp_wks.WORKING_RCD(j).Process_History_id,
                                                      cRule_id             => xxcp_wks.WORKING_RCD(j).Rule_id,
                                                      cD1001_Array         => xxcp_wks.WORKING_RCD(j).D1001_Array,
                                                      cD1002_Array         => xxcp_wks.WORKING_RCD(j).D1002_Array,
                                                      cD1003_Array         => xxcp_wks.WORKING_RCD(j).D1003_Array,
                                                      cD1004_Array         => xxcp_wks.WORKING_RCD(j).D1004_Array,
																											cD1005_Array         => xxcp_wks.WORKING_RCD(j).D1005_Array,
                                                      cD1006_Array         => xxcp_wks.WORKING_RCD(j).D1006_Array,
                                                      cI1009_Array         => xxcp_wks.WORKING_RCD(j).I1009_Array,
                                                      cErrorMessage        => vErrorMessage,
                                                      cInternalErrorCode   => cInternalErrorCode);
                 End If;

								 IF cInternalErrorCode = 0 then
                    ---
                    --- XXCP_PROCESS_HISTORY
                    ---
          --          XXCP_PROCESS_HIST_CPA.Process_History_Insert(
                    XXCP_FC_PROCESS_HIST_CPA.Process_History_Insert(
                                                              cSource_Rowid            => xxcp_wks.WORKING_RCD(j).Source_rowid,
																											        cParent_Rowid            => Null,
                                                              cTarget_Table            => xxcp_wks.WORKING_RCD(j).Target_Table,
                                                              cTarget_Instance_id      => xxcp_wks.WORKING_RCD(j).Target_Instance_id,
                                                              cProcess_History_id      => xxcp_wks.WORKING_RCD(j).Process_History_id,
                                                              cTarget_assignment_id    => xxcp_wks.WORKING_RCD(j).Target_Assignment_id,
                                                              cAttribute_id            => xxcp_wks.WORKING_RCD(j).Attribute_id,
                                                              cRecord_type             => xxcp_wks.WORKING_RCD(j).Record_type,
                                                              cRequest_id              => xxcp_global.gCommon(1).current_request_id,
                                                              cStatus                  => xxcp_wks.WORKING_RCD(j).Record_Status,
                                                              cInterface_id            => xxcp_wks.WORKING_RCD(j).Interface_id,
                                                              cModel_ctl_id            => xxcp_wks.WORKING_RCD(j).Model_ctl_id,
                                                              cRule_id                 => xxcp_wks.WORKING_RCD(j).Rule_id,
                                                              cTax_registration_id     => xxcp_wks.WORKING_RCD(j).Tax_Registration_id,
																															cEntered_rounding_amount => xxcp_wks.WORKING_RCD(j).Entered_Rnd_Amount,
																															cAccount_rounding_amount => xxcp_wks.WORKING_RCD(j).Accounted_Rnd_Amount,
                                                              cD1001_Array             => xxcp_wks.WORKING_RCD(j).D1001_Array,
                                                              cD1002_Array             => xxcp_wks.WORKING_RCD(j).D1002_Array,
                                                              cD1003_Array             => xxcp_wks.WORKING_RCD(j).D1003_Array,
                                                              cD1004_Array             => xxcp_wks.WORKING_RCD(j).D1004_Array,
																											        cD1005_Array             => xxcp_wks.WORKING_RCD(j).D1005_Array,
                                                              cD1006_Array             => xxcp_wks.WORKING_RCD(j).D1006_Array,
                                                              cI1009_Array             => xxcp_wks.WORKING_RCD(j).I1009_Array,
                                                              cErrorMessage            => vErrorMessage,
                                                              cInternalErrorCode       => cInternalErrorCode);
                                           
                     If xxcp_wks.WORKING_RCD(j).cost_plus_rule = 'Y' then   -- If Cost-Plus Rule
                     --  If cReplan = 'Y' then
                       If cReplan = 'Y' then
                         if (XXCP_GLOBAL.GCOMMON(1).CURRENT_TRANSACTION_TYPE = 'REP')
                             or
                             (XXCP_GLOBAL.GCOMMON(1).CURRENT_TRANSACTION_TYPE = 'REV' and
                             not vReplanned) then
                             -- and xxcp_global.Get_New_Trx_Flag = 'Y') then
                          Replan_History(cSource_Rowid            => xxcp_wks.WORKING_RCD(j).Source_rowid,
                                         cParent_Rowid            => Null,
                                         cPeriod_Set_Name_id      => cPeriod_Set_Name_id,
                                         cTarget_Table            => xxcp_wks.WORKING_RCD(j).Target_Table,
                                         cTarget_Instance_id      => xxcp_wks.WORKING_RCD(j).Target_Instance_id,
                                         cProcess_History_id      => xxcp_wks.WORKING_RCD(j).Process_History_id,
                                         cTarget_assignment_id    => xxcp_wks.WORKING_RCD(j).Target_Assignment_id,
                                         cAttribute_id            => xxcp_wks.WORKING_RCD(j).Attribute_id,
                                         cRecord_type             => xxcp_wks.WORKING_RCD(j).Record_type,
                                         cRequest_id              => xxcp_global.gCommon(1).current_request_id,
                                         cStatus                  => xxcp_wks.WORKING_RCD(j).Record_Status,
                                         cInterface_id            => xxcp_wks.WORKING_RCD(j).Interface_id,
                                         cModel_ctl_id            => xxcp_wks.WORKING_RCD(j).Model_ctl_id,
                                         cRule_id                 => xxcp_wks.WORKING_RCD(j).Rule_id,
                                         cTrading_Set_id          => xxcp_wks.WORKING_RCD(j).Trading_Set_id,
                                         cTax_registration_id     => xxcp_wks.WORKING_RCD(j).Tax_Registration_id,
                                         cEntered_rounding_amount => xxcp_wks.WORKING_RCD(j).Entered_Rnd_Amount,
                                         cAccount_rounding_amount => xxcp_wks.WORKING_RCD(j).Accounted_Rnd_Amount,
                                         cI1009_Array             => xxcp_wks.WORKING_RCD(j).I1009_Array,
                                         cTransaction_Type        => XXCP_GLOBAL.GCOMMON(1).CURRENT_TRANSACTION_TYPE,
                                         cErrorMessage            => vErrorMessage,
                                         cInternalErrorCode       => cInternalErrorCode);
                           vReplanned := TRUE;
                         End if;
                       Else
                         Process_True_Up_Hist_Insert(cSource_Rowid            => xxcp_wks.WORKING_RCD(j).Source_rowid,
                                                     cParent_Rowid            => Null,
                                                     cPeriod_Set_Name_id      => cPeriod_Set_Name_id,
                                                     cTarget_Table            => xxcp_wks.WORKING_RCD(j).Target_Table,
                                                     cTarget_Instance_id      => xxcp_wks.WORKING_RCD(j).Target_Instance_id,
                                                     cProcess_History_id      => xxcp_wks.WORKING_RCD(j).Process_History_id,
                                                     cTarget_assignment_id    => xxcp_wks.WORKING_RCD(j).Target_Assignment_id,
                                                     cAttribute_id            => xxcp_wks.WORKING_RCD(j).Attribute_id,
                                                     cRecord_type             => xxcp_wks.WORKING_RCD(j).Record_type,
                                                     cRequest_id              => xxcp_global.gCommon(1).current_request_id,
                                                     cStatus                  => xxcp_wks.WORKING_RCD(j).Record_Status,
                                                     cInterface_id            => xxcp_wks.WORKING_RCD(j).Interface_id,
                                                     cModel_ctl_id            => xxcp_wks.WORKING_RCD(j).Model_ctl_id,
                                                     cRule_id                 => xxcp_wks.WORKING_RCD(j).Rule_id,
                                                     cTrading_Set_id          => xxcp_wks.WORKING_RCD(j).Trading_Set_id,
                                                     cTax_registration_id     => xxcp_wks.WORKING_RCD(j).Tax_Registration_id,
                                                     cEntered_rounding_amount => xxcp_wks.WORKING_RCD(j).Entered_Rnd_Amount,
                                                     cAccount_rounding_amount => xxcp_wks.WORKING_RCD(j).Accounted_Rnd_Amount,
                                                     cI1009_Array             => xxcp_wks.WORKING_RCD(j).I1009_Array,
                                                     cErrorMessage            => vErrorMessage,
                                                     cInternalErrorCode       => cInternalErrorCode);
                       End if;
                     end if;
                     
                     If cInternalErrorCode = 0 then
                       vTiming(vTimingCnt).Flush_Records := vTiming(vTimingCnt).Flush_Records + 1;
                     Else
                       xxcp_foundation.FndWriteError(cInternalErrorCode, vErrorMessage);
					           End If;
                  Else
                    xxcp_foundation.FndWriteError(cInternalErrorCode, vErrorMessage);
				  End If;

          If cInternalErrorCode = 0 then
--             vStatus := 'SUCCESSFUL';
             vStatus := 'TRIAL';
          Else
             vStatus := 'ERROR';
             xxcp_wks.SOURCE_SUB_CODE(xxcp_wks.WORKING_RCD(j).Source_Pos):= 7008;
          End If;

				  xxcp_wks.SOURCE_STATUS(xxcp_wks.WORKING_RCD(j).Source_Pos) := vStatus;
        Else
          -- ZAV
          xxcp_wks.SOURCE_SUB_CODE(xxcp_wks.WORKING_RCD(j).Source_Pos):= 7008;
--          xxcp_wks.SOURCE_STATUS(xxcp_wks.WORKING_RCD(j).Source_Pos) := 'SUCCESSFUL';
          xxcp_wks.SOURCE_STATUS(xxcp_wks.WORKING_RCD(j).Source_Pos) := 'TRIAL';
        End If;

      UDI_Cnt := UDI_Cnt + 1;


      END IF;

      END LOOP;

      If cInternalErrorCode = 0 and UDI_Cnt > 0 then
        FORALL j in 1 .. xxcp_wks.SOURCE_CNT
          Update XXCP_cost_plus_interface
             Set vt_Status              = xxcp_wks.SOURCE_STATUS(j),
                 vt_internal_Error_code = Null,
                 vt_date_processed      = xxcp_global.SystemDate,
                 vt_Status_Code         = decode(xxcp_global.gDefCostCatUsed,'Y',7016,xxcp_wks.SOURCE_SUB_CODE(j))
           where rowid = xxcp_wks.Source_rowid(j);
      End If;

    END IF;

	 -- Source_Pos

    If cInternalErrorCode <> 0 then
      -- Rollback
      If vCnt > 0 then
        Rollback To TARGET_INSERT;
        xxcp_foundation.FndWriteError(cInternalErrorCode, vErrorMessage);
      End If;

      Error_Whole_Transaction(cInternalErrorCode => cInternalErrorCode);

      If cInternalErrorCode = 12800 then
        xxcp_foundation.FndWriteError(cInternalErrorCode,
          'Entered DR <' ||to_char(vEntered_DR) ||'> Entered CR <' ||to_char(vEntered_CR) || '>');
      End If;

    End If;

    -- Remove Array Elements
    ClearWorkingStorage;

    Return(vCnt);
  End FlushRecordsToTarget;

  --  !! ***********************************************************************************************************
  --  !!
  --  !!                                   Reset_Errored_Transactions
  --  !!                              Reset Transactions from a previous run.
  --  !!
  --  !! ***********************************************************************************************************
  Procedure Reset_Rep_Errored_Transactions(cSource_assignment_id in number, 
                                           cRequest_id           in number) is

  begin

    vTiming(vTimingCnt).Reset_Start := to_char(sysdate, 'HH24:MI:SS');

    -- Remove Errors
    --Begin
    --  Delete from XXCP_errors cx
    --   Where source_assignment_id = cSource_assignment_id;

    --  Exception When OTHERS then Null;
    --End;

    -- Update Interface
    Begin

		  -- Three updates are used rather than one so that we hit the vt_status
			-- index. This is important when you have 30million rows.
      Update XXCP_cost_plus_interface r
         set vt_status              = 'NEW',
             vt_Internal_Error_Code = Null,
             vt_status_code         = Null,
             vt_request_id          = cRequest_id
       where vt_source_assignment_id = cSource_assignment_id
         and vt_transaction_table  in ('TRUE-UP','FORECAST')
         and vt_transaction_type   in ('REP','REV')
         and vt_status in ('ERROR','GROUPING','ASSIGNMENT','TRANSACTION');

      Exception when OTHERS then Null;
    End;

    Commit;
  End RESET_REP_ERRORED_TRANSACTIONS;

  --  !! ***********************************************************************************************************
  --  !!
  --  !!                                   Reset_Errored_Transactions
  --  !!                              Reset Transactions from a previous run.
  --  !!
  --  !! ***********************************************************************************************************
  Procedure Reset_Errored_Transactions(cSource_assignment_id in number, 
                                       cRequest_id           in number,
                                       cCost_Plus_Set_id     in number) is

  begin

    vTiming(vTimingCnt).Reset_Start := to_char(sysdate, 'HH24:MI:SS');

    -- Remove Errors
    Begin
      Delete from XXCP_errors cx
       Where source_assignment_id = cSource_assignment_id;

      Exception When OTHERS then Null;
    End;

    -- Update Interface
    Begin

		  -- Three updates are used rather than one so that we hit the vt_status
			-- index. This is important when you have 30million rows.
      Update XXCP_cost_plus_interface r
         set vt_status              = 'NEW',
             vt_Internal_Error_Code = Null,
             vt_status_code         = Null,
             vt_request_id          = cRequest_id
       where vt_source_assignment_id = cSource_assignment_id
         and vt_transaction_table = 'TRUE-UP'
         and vt_transaction_type  = 'STD'
         and decode(xxcp_global.gReplan_Reprocess,'Y',vt_internal_error_code,7015) = 7015 -- Only pickup 7015 for Reprocess
         and vt_status in ('ERROR','GROUPING','ASSIGNMENT','TRANSACTION')
         and cost_plus_set_id        = nvl(cCost_Plus_Set_id,Cost_Plus_Set_id);

      Exception when OTHERS then Null;
    End;

    Commit;
  End RESET_ERRORED_TRANSACTIONS;




  --  !! ***********************************************************************************************************
  --  !!
  --  !!                                  Set_Running_Status
  --  !!                      Flag the records that are going to be processed.
  --  !!
  --  !! ***********************************************************************************************************
Procedure Set_Replan_Running_Status(cSource_id            in number,
                               cSource_assignment_id in number,
                               cRequest_id           in number,
                               cPeriod_Set_Name_id   in number,
                               cPosting_Period_id    in number) is
                          --     cStart_Date           in date,
                          --     cEnd_Date             in date) is

    cursor rx(pSource_id in number, pSource_Assignment_id in number, pPeriod_Set_Name_id in number) is
      select r.vt_transaction_table,
             r.vt_transaction_id,
             r.rowid source_rowid,
             r.vt_transaction_type,
             nvl(t.grouping_rule_id,0) grouping_rule_id
        from XXCP_cost_plus_interface r, XXCP_sys_source_tables t
       where vt_source_assignment_id = pSource_assignment_id
         and vt_status               = 'NEW'
         and r.vt_transaction_table  = t.transaction_table
         and r.vt_transaction_table  in ('TRUE-UP','FORECAST')
         and r.vt_transaction_type   in ('REP','REV')
         and t.source_id             = pSource_id
         and r.period_set_name_id    = pPeriod_Set_Name_id
         and r.posting_period_id     = cPosting_period_id
--         and r.vt_transaction_date between cStart_Date and cEnd_Date  -- Replan runs for all periods
    order by t.transaction_table
         for update;

    vGrouping_rule_id XXCP_cost_plus_interface.vt_grouping_rule_id%type;

    -- Find records with no vt_grouping_rule_id
    cursor er2(pSource_assignment_id in number, pPeriod_Set_Name_id in number) is
      select Distinct g.vt_transaction_table
        from XXCP_cost_plus_interface g, XXCP_sys_source_tables t
       where g.vt_request_id           = cRequest_id
         and g.period_set_name_id      = pPeriod_Set_Name_id
         and g.vt_status               = 'GROUPING'
         AND g.vt_transaction_table    = t.transaction_table
         and g.vt_transaction_table  in ('TRUE-UP','FORECAST')
         and g.vt_transaction_type   in ('REP','REV')
         and g.vt_source_assignment_id = pSource_assignment_id
         AND t.grouping_rule_id IS NOT NULL  -- 02.06.02
         and g.vt_grouping_rule_id is null;

    -- Find records with invalid Grouping rule
    cursor er3(pSource_assignment_id in number, pPeriod_Set_Name_id in number) is
      select Distinct g.vt_transaction_table
        from XXCP_cost_plus_interface g, 
             XXCP_grouping_rules r, 
             XXCP_sys_source_tables t
       where g.vt_request_id           = cRequest_id
         and g.period_set_name_id      = pPeriod_Set_Name_id
         and g.vt_status               = 'GROUPING'
         AND g.vt_transaction_table    = t.transaction_table
         and g.vt_transaction_table  in ('TRUE-UP','FORECAST')
         and g.vt_transaction_type   in ('REP','REV')
         and g.vt_source_assignment_id = pSource_assignment_id
         and g.vt_grouping_rule_id     = r.grouping_rule_id(+)
         AND t.grouping_rule_id IS NOT NULL  -- 02.06.02
         and r.grouping_rule_id is null;

    vCurrent_request_id    number;
		vAction_flag           varchar2(1) := 'N';
    vGroupingRuleExists    BOOLEAN     := FALSE;
    vPrevTransaction_Table XXCP_sys_source_tables.transaction_table%TYPE := '~NULL~';

  begin

    vTiming(vTimingCnt).Set_Running_Start := to_char(sysdate, 'HH24:MI:SS');

    vCurrent_Request_id := nvl(cRequest_id, 0);

    For rec in rx(cSource_id, cSource_assignment_id, cPeriod_Set_Name_id) Loop
      vGrouping_rule_id := Null;

      -- Populate Transaction_Table Array
      if vPrevTransaction_Table <> rec.vt_transaction_table then
        gTrans_Table_Array.extend;
        gTrans_Table_Array(gTrans_Table_Array.count) := rec.vt_transaction_table;
      End if;
      
      vPrevTransaction_Table := rec.vt_transaction_table;
			
      IF Rec.grouping_rule_id > 0 then  -- 02.06.02

        vGroupingRuleExists := TRUE;

				xxcp_global.gCommon(1).current_transaction_table := Rec.vt_Transaction_Table;
        xxcp_global.gCommon(1).current_transaction_id    := Rec.vt_Transaction_id;

        vGrouping_rule_id := xxcp_custom_events.get_group_rule_id(cSource_id,
                                                                  cSource_assignment_id,
                                                                  rec.source_rowid,
                                                                  rec.vt_transaction_table,
                                                                  rec.vt_transaction_type);

        If nvl(vGrouping_rule_id, 0) = 0 then
          vGrouping_rule_id := Rec.Grouping_rule_id; -- Default;
        End If;
      End If;
      -- Set Processing Status
      begin
        update XXCP_cost_plus_interface r
           set vt_status           = 'GROUPING',
               vt_date_processed   = xxcp_global.SystemDate,
               vt_status_code      = Null,
               vt_request_id       = cRequest_id,
               vt_Grouping_rule_id = vGrouping_rule_id
         where r.rowid = rec.source_rowid;

      Exception
        when OTHERS then null;
      End;

    End Loop;

    -- Error Checking
    IF vGroupingRuleExists then  -- 02.06.02 Don't Check If no Grouping Rule used.
      -- Find records with no vt_grouping_rule_id
      For Rec in er2(cSource_assignment_id, cPeriod_Set_Name_id) loop
        update XXCP_cost_plus_interface g
           set g.vt_status              = 'ERROR',
               g.vt_date_processed      = xxcp_global.SystemDate,
               g.vt_internal_error_code = 11088
         where g.vt_request_id           = cRequest_id
           and g.vt_status               = 'GROUPING'
           and g.vt_transaction_table    = 'TRUE-UP'                      -- CHG20081212SJS
           and g.vt_source_assignment_id = cSource_assignment_id
           and g.vt_transaction_table = rec.vt_transaction_table;
      End Loop;
      -- Find records with invalid Grouping rule
      For Rec in er3(cSource_assignment_id, cPeriod_Set_Name_id) loop
        update XXCP_cost_plus_interface g
           set g.vt_status              = 'ERROR',
               g.vt_date_processed      = xxcp_global.SystemDate,
               g.vt_internal_error_code = 11089
         where g.vt_request_id           = cRequest_id
           and g.vt_status               = 'GROUPING'
           and g.vt_transaction_table    = 'TRUE-UP'                      -- CHG20081212SJS
           and g.vt_source_assignment_id = cSource_assignment_id
           and g.vt_transaction_table = rec.vt_transaction_table;
      End Loop;
    End If;

    Commit;

  End Set_Replan_Running_Status;


--  !! ***********************************************************************************************************
  --  !!
  --  !!                                  Set_Running_Status
  --  !!                      Flag the records that are going to be processed.
  --  !!
  --  !! ***********************************************************************************************************
  Procedure Set_Running_Status(cSource_id            in number,
                               cSource_assignment_id in number,
                               cRequest_id           in number,
                               cPeriod_Set_Name_id   in number,
                               cPosting_Period_id    in number,
                               cCost_Plus_Set_id     in number) is


    cursor rx(pSource_id in number, pSource_Assignment_id in number, pPeriod_Set_Name_id in number) is
      select r.vt_transaction_table,
             r.vt_transaction_id,
             r.rowid source_rowid,
             r.vt_transaction_type,
             nvl(t.grouping_rule_id,0) grouping_rule_id
        from XXCP_cost_plus_interface r, XXCP_sys_source_tables t
       where vt_source_assignment_id = pSource_assignment_id
         and vt_status               = 'NEW'
         and r.vt_transaction_table  = t.transaction_table
         and r.vt_transaction_table  = 'TRUE-UP'                      -- CHG20081212SJS
         and r.vt_transaction_type   = 'STD'
         and t.source_id             = pSource_id
         and r.period_set_name_id    = pPeriod_Set_Name_id
         and r.posting_period_id     = cPosting_period_id
         and r.Cost_Plus_Set_id      = nvl(cCost_Plus_Set_id,r.Cost_Plus_Set_id)
    order by t.transaction_table
         for update;

    vGrouping_rule_id XXCP_cost_plus_interface.vt_grouping_rule_id%type;

    -- Find records with no vt_grouping_rule_id
    cursor er2(pSource_assignment_id in number, pPeriod_Set_Name_id in number) is
      select Distinct g.vt_transaction_table
        from XXCP_cost_plus_interface g, XXCP_sys_source_tables t
       where g.vt_request_id           = cRequest_id
         and g.period_set_name_id      = pPeriod_Set_Name_id
         and g.vt_status               = 'GROUPING'
         AND g.vt_transaction_table    = t.transaction_table
         and g.vt_transaction_table    = 'TRUE-UP'                      -- CHG20081212SJS
         and g.vt_transaction_type     = 'STD'
         and g.vt_source_assignment_id = pSource_assignment_id
         AND t.grouping_rule_id IS NOT NULL                             -- 02.06.02
         and g.vt_grouping_rule_id is null
         and g.Cost_Plus_Set_id     = nvl(cCost_Plus_Set_id,g.Cost_Plus_Set_id);


    -- Find records with invalid Grouping rule
    cursor er3(pSource_assignment_id in number, pPeriod_Set_Name_id in number) is
      select Distinct g.vt_transaction_table
        from XXCP_cost_plus_interface g, 
             XXCP_grouping_rules r, 
             XXCP_sys_source_tables t
       where g.vt_request_id           = cRequest_id
         and g.period_set_name_id      = pPeriod_Set_Name_id
         and g.vt_status               = 'GROUPING'
         AND g.vt_transaction_table    = t.transaction_table
         and g.vt_transaction_table    = 'TRUE-UP'                      -- CHG20081212SJS
         and g.vt_transaction_type     = 'STD'
         and g.vt_source_assignment_id = pSource_assignment_id
         and g.vt_grouping_rule_id     = r.grouping_rule_id(+)
         AND t.grouping_rule_id IS NOT NULL                             -- 02.06.02
         and r.grouping_rule_id is null
         and g.Cost_Plus_Set_id     = nvl(cCost_Plus_Set_id,g.Cost_Plus_Set_id);


    vCurrent_request_id    number;
		vAction_flag           varchar2(1) := 'N';
    vGroupingRuleExists    BOOLEAN     := FALSE;
    vPrevTransaction_Table XXCP_sys_source_tables.transaction_table%TYPE := '~NULL~';

  begin

    vTiming(vTimingCnt).Set_Running_Start := to_char(sysdate, 'HH24:MI:SS');

    vCurrent_Request_id := nvl(cRequest_id, 0);

    For rec in rx(cSource_id, cSource_assignment_id, cPeriod_Set_Name_id) Loop
      vGrouping_rule_id := Null;
      -- Populate Transaction_Table Array
      if vPrevTransaction_Table <> rec.vt_transaction_table then
        gTrans_Table_Array.extend;
        gTrans_Table_Array(gTrans_Table_Array.count) := rec.vt_transaction_table;
      End if;
      
      vPrevTransaction_Table := rec.vt_transaction_table;
			
      IF Rec.grouping_rule_id > 0 then  -- 02.06.02

        vGroupingRuleExists := TRUE;

				xxcp_global.gCommon(1).current_transaction_table := Rec.vt_Transaction_Table;
        xxcp_global.gCommon(1).current_transaction_id    := Rec.vt_Transaction_id;

        vGrouping_rule_id := xxcp_custom_events.get_group_rule_id(cSource_id,
                                                                  cSource_assignment_id,
                                                                  rec.source_rowid,
                                                                  rec.vt_transaction_table,
                                                                  rec.vt_transaction_type);

        If nvl(vGrouping_rule_id, 0) = 0 then
          vGrouping_rule_id := Rec.Grouping_rule_id; -- Default;
        End If;
      End If;
      -- Set Processing Status
      begin
        update XXCP_cost_plus_interface r
           set vt_status           = 'GROUPING',
               vt_date_processed   = xxcp_global.SystemDate,
               vt_status_code      = Null,
               vt_request_id       = cRequest_id,
               vt_Grouping_rule_id = vGrouping_rule_id
         where r.rowid = rec.source_rowid;

      Exception
        when OTHERS then null;
      End;

    End Loop;

    -- Error Checking
    IF vGroupingRuleExists then  -- 02.06.02 Don't Check If no Grouping Rule used.
      -- Find records with no vt_grouping_rule_id
      For Rec in er2(cSource_assignment_id, cPeriod_Set_Name_id) loop
        update XXCP_cost_plus_interface g
           set g.vt_status              = 'ERROR',
               g.vt_date_processed      = xxcp_global.SystemDate,
               g.vt_internal_error_code = 11088
         where g.vt_request_id           = cRequest_id
           and g.vt_status               = 'GROUPING'
           and g.vt_transaction_table    = 'TRUE-UP'                      -- CHG20081212SJS
           and g.vt_transaction_type     = 'STD'
           and g.vt_source_assignment_id = cSource_assignment_id
           and g.vt_transaction_table = rec.vt_transaction_table
           and g.Cost_Plus_Set_id     = nvl(cCost_Plus_Set_id,g.Cost_Plus_Set_id);
      End Loop;
      -- Find records with invalid Grouping rule
      For Rec in er3(cSource_assignment_id, cPeriod_Set_Name_id) loop
        update XXCP_cost_plus_interface g
           set g.vt_status              = 'ERROR',
               g.vt_date_processed      = xxcp_global.SystemDate,
               g.vt_internal_error_code = 11089
         where g.vt_request_id           = cRequest_id
           and g.vt_status               = 'GROUPING'
           and g.vt_transaction_table    = 'TRUE-UP'                      -- CHG20081212SJS
           and g.vt_transaction_type     = 'STD'
           and g.vt_source_assignment_id = cSource_assignment_id
           and g.vt_transaction_table = rec.vt_transaction_table
           and g.Cost_Plus_Set_id     = nvl(cCost_Plus_Set_id,g.Cost_Plus_Set_id);
      End Loop;
    End If;

    Commit;

  End Set_Running_Status;

  --  !! ***********************************************************************************************************
  --  !!
  --  !!                                  Set_Assignment_Status
  --  !!                      Flag the records that are going to be processed.
  --  !!
  --  !! ***********************************************************************************************************
  Procedure Set_Assignment_Status(cSource_id            in number,
                                  cSource_assignment_id in number,
                                  cRequest_id           in number,
                                  cCost_Plus_Set_id     in number,
															    cInternalErrorCode    in out number) is

    vCurrent_request_id number;
		vDV_Statement       varchar2(32000);
		vSQLERRM            varchar2(512);

  begin
	  cInternalErrorCode := 0;


    vCurrent_Request_id := nvl(cRequest_id, 0);

	  vDV_Statement := xxcp_dynamic_sql.Build_Interface_Selection(cSource_id, cSource_assignment_id, cRequest_id);
 
		If vDV_Statement is not null then
		 Execute Immediate vDV_Statement;
    End If;

		Begin
        update XXCP_cost_plus_interface g
           set g.vt_status              = 'NEW',
               g.vt_date_processed      = xxcp_global.SystemDate,
               g.VT_STATUS_CODE = 7004
         where g.vt_request_id = cRequest_id
           and g.vt_status     = 'GROUPING'
           and g.vt_transaction_table    = 'TRUE-UP'                      -- CHG20081212SJS
           and g.vt_transaction_type     = 'STD'
           and g.vt_source_assignment_id = cSource_assignment_id
           and g.Cost_Plus_Set_id = nvl(cCost_Plus_Set_id,g.Cost_Plus_Set_id);
		End;

    Commit;


		Exception
      when OTHERS then
        vSQLERRM := SQLERRM; -- New to trap Oracle's Reason for not working.
        xxcp_foundation.Set_InternalErrorCode(cInternalErrorCode, 10884);
        xxcp_foundation.FndWriteError(10884, vSQLERRM);


  End Set_Assignment_Status;

  --  !! ***********************************************************************************************************
  --  !!
  --  !!                                  Set_Replan_Assignment_Status
  --  !!                      Flag the records that are going to be processed.
  --  !!
  --  !! ***********************************************************************************************************
  Procedure Set_Replan_Assignment_Status(cSource_id            in number,
                                         cSource_assignment_id in number,
                                         cRequest_id           in number,
															           cInternalErrorCode    in out number) is

    vCurrent_request_id number;
		vDV_Statement       varchar2(32000);
		vSQLERRM            varchar2(512);

  begin
	  cInternalErrorCode := 0;


    vCurrent_Request_id := nvl(cRequest_id, 0);

	  vDV_Statement := xxcp_dynamic_sql.Build_Interface_Selection(cSource_id, cSource_assignment_id, cRequest_id);
 
		If vDV_Statement is not null then
		 Execute Immediate vDV_Statement;
    End If;

		Begin
        update XXCP_cost_plus_interface g
           set g.vt_status              = 'NEW',
               g.vt_date_processed      = xxcp_global.SystemDate,
               g.VT_STATUS_CODE = 7004
         where g.vt_request_id = cRequest_id
           and g.vt_status     = 'GROUPING'
           and g.vt_transaction_table  in ('TRUE-UP','FORECAST')
           and g.vt_transaction_type   in ('REP','REV')
           and g.vt_source_assignment_id = cSource_assignment_id;
		End;

    Commit;


		Exception
      when OTHERS then
        vSQLERRM := SQLERRM; -- New to trap Oracle's Reason for not working.
        xxcp_foundation.Set_InternalErrorCode(cInternalErrorCode, 10884);
        xxcp_foundation.FndWriteError(10884, vSQLERRM);


  End Set_Replan_Assignment_Status;

  --  !! ***********************************************************************************************************
  --  !!
  --  !!                                    DELETE_FC_TABLES
  --  !!                  Cleardown the Trial CPA Tables for TTUE_UP ready for new run.
  --  !!
  --  !! ***********************************************************************************************************
  Procedure Delete_FC_Tables(cSource_Assignment_id in number,
                             cCost_Plus_Set_id     IN NUMBER DEFAULT NULL) is
  
  Begin
      --
      -- Cleardown Forecast Tables
      --
      Delete from XXCP_fc_forecast_history   
      where  source_assignment_id = cSource_Assignment_id
      and    cpa_type in ('T','RT','RF')
      and    cost_plus_set_id = nvl(cCost_Plus_Set_id,cost_plus_set_id);

      Delete from XXCP_fc_transaction_header 
      where source_assignment_id = cSource_Assignment_id
      and    cpa_type in ('T','RT','RF')
      and    cost_plus_set_id = nvl(cCost_Plus_Set_id,cost_plus_set_id);

      Delete from XXCP_fc_transaction_journals j 
      where vt_source_assignment_id = cSource_Assignment_id
      and    cpa_type in ('T','RT','RF')
      and    cost_plus_set_id = nvl(cCost_Plus_Set_id,cost_plus_set_id);

      Delete from XXCP_fc_transaction_cache c
      where  cpa_type in ('T','RT','RF')
      and    cost_plus_set_id = nvl(cCost_Plus_Set_id,cost_plus_set_id)
      and  c.attribute_id = any(select attribute_id
                                    from XXCP_fc_transaction_attributes  
                                    where source_assignment_id = cSource_Assignment_id);

      Delete from XXCP_fc_process_history c
      where  cpa_type in ('T','RT','RF')
      and    cost_plus_set_id = nvl(cCost_Plus_Set_id,cost_plus_set_id)
      and    c.interface_id = any(select  vt_interface_id
                                    from  XXCP_cost_plus_interface w
                                    where w.vt_source_assignment_id = cSource_Assignment_id);

      -- remove records that are in the target interface
      Delete from XXCP_fc_process_history c
      where  cpa_type in ('T','RT','RF')
      and    cost_plus_set_id = nvl(cCost_Plus_Set_id,cost_plus_set_id)
      and    c.process_history_id = any(select j.vt_process_history_id
                                          from XXCP_transaction_journals j
                                         where j.vt_source_assignment_id = cSource_Assignment_id);

      Delete from XXCP_fc_transaction_attributes 
      where source_assignment_id = cSource_Assignment_id
      and    cost_plus_set_id = nvl(cCost_Plus_Set_id,cost_plus_set_id)
      and cpa_type in ('T','RT','RF');

      -- Remove trial replan records
      Delete from XXCP_cost_plus_interface
      where vt_source_assignment_id = cSource_Assignment_id
      and   vt_transaction_table in ('TRUE-UP','FORECAST')
      and   vt_transaction_type  in ('REP','REV')
      and   vt_status != 'SUCCESSFUL' 
      and   cost_plus_set_id = nvl(cCost_Plus_Set_id,cost_plus_set_id);

      -- Reset Trial_ind on history tables
      update XXCP_forecast_history
      set    trial_ind                   = null,
             reversal_process_history_id = null,
             reversal_interface_id       = null
      where  source_assignment_id = cSource_Assignment_id
      and    cost_plus_set_id     = nvl(cCost_Plus_Set_id,cost_plus_set_id)
      and    active = 'Y';

      update XXCP_true_up_history
      set    trial_ind                   = null,
             reversal_process_history_id = null,
             reversal_interface_id       = null
      where  source_assignment_id = cSource_Assignment_id
      and    cost_plus_set_id     = nvl(cCost_Plus_Set_id,cost_plus_set_id)
      and    active = 'Y';

      Commit;

 End Delete_FC_Tables;    


  Function Replan_Control(cSource_Group_id    IN NUMBER,
                   cConc_Request_Id    IN NUMBER,
                   cTable_Group_id     IN NUMBER   DEFAULT 0, 
                   cCalendar_id        IN NUMBER   DEFAULT 0,
                   cPeriod_Name        IN VARCHAR2 DEFAULT Null,
                   cRequest_Id         IN NUMBER   DEFAULT 0,
                   cPrev_Forecast_id   IN NUMBER   DEFAULT 0,
                   cCost_Plus_Set_id   IN NUMBER   DEFAULT NULL) return number is  -- cPeriod_name is the forecast period (accounting period)

    -- Assignment
    cursor cfg(pSource_id in number, pSource_Assignment_id in Number, pRequest_id in number) is
      select g.vt_transaction_table,
             g.vt_transaction_type,
             g.vt_transaction_class,
             g.vt_transaction_id,
             g.vt_parent_trx_id,
             g.vt_transaction_ref,
             s.set_of_books_id,
             g.rowid source_rowid,
             c.currency_code currency_code,                 -- ???
             c.period_net_dr Entered_DR,                    -- ???
             c.period_net_cr Entered_CR,
             s.source_assignment_id,
             s.set_of_books_id source_set_of_books_id,
             g.vt_interface_id,
             w.transaction_set_name,
             nvl(w.Transaction_Set_id,0) Transaction_Set_Id,
             g.vt_transaction_date,
             --
             y.source_table_id,
             y.source_type_id,
             cx.source_class_id,
             g.vt_replan_company    company,
             g.vt_replan_department department,
             g.vt_replan_account    account,
             g.cost_plus_set_id
        from XXCP_source_assignments     s,
             XXCP_cost_plus_interface    g,
             XXCP_actual_costs           c,
             -- New
             XXCP_sys_source_tables       t,
             XXCP_sys_source_types        y,
             XXCP_sys_source_classes      cx,
             XXCP_source_transaction_sets w
       where g.vt_request_id           = pRequest_id
         and g.vt_status               = 'ASSIGNMENT'
         and g.vt_source_assignment_id = pSource_Assignment_ID
         and g.vt_source_assignment_id = s.source_assignment_id
         -- Actual Cost
         and g.vt_replan_transaction_id = c.transaction_id
         and g.vt_source_assignment_id = c.source_assignment_id
         -- Table
         and g.vt_transaction_table    = t.transaction_table
         and t.source_id               = pSource_id
         -- Type
         and g.vt_transaction_type     in ('REV','REP')
         and g.vt_transaction_type     = y.type
         and y.latch_only             = 'N'
         and t.source_table_id         = y.source_table_id
         -- Class
         and g.vt_transaction_class    = decode(t.class_mapping_req,'Y',cx. system_latch,cx.class)
         and cx.latch_only             = 'N'
         and t.source_table_id         = cx.source_table_id
         -- Transaction_Set_id
         and y.Transaction_Set_id      = w.Transaction_Set_id
         --
         order by vt_transaction_table, w.sequence,  g.vt_parent_trx_id, cx.sequence, vt_transaction_id;

    CFGRec CFG%Rowtype;



    -- Transactions
    cursor GLI(pSource_id in number, pSource_Assignment_id in Number, pRequest_id in number) is
      SELECT g.vt_transaction_table,
             g.vt_transaction_type,
             g.vt_transaction_class,
             g.vt_interface_id,
             g.vt_transaction_id,
             g.vt_transaction_ref,
             s.instance_id vt_instance_id,
             Null Code_Combination_id,
             -- Balancing segment
             'Cost-Plus'      be2, --  USER_JE_SOURCE_NAME
             0                be3, -- GROUP_ID
             'Cost-Plus'      be4, -- USER_JE_CATEGORY_NAME
             g.vt_transaction_Ref be6,
             Null             be7, -- Reference2
             Null             be8, -- Reference5
             -- Inventory
             0 Transaction_Cost,              
             -- History
             'Cost-Plus' Category_name,
             'Cost-Plus' Source_name,
             g.vt_transaction_date  accounting_date,
             --
             Null parent_entered_currency,    
             ac.currency_code currency_code,   
             g.vt_parent_trx_id,
             -- Fixed Values for the engine. The pricing engine should would out cost
             0 Group_id,
             0 Parent_Entered_Amount,  
             ac.period_net_dr Entered_DR,
             ac.period_net_cr Entered_CR,
             0    Accounted_DR,
             Null Accounted_CR,
             -- End Fixed Values
             g.rowid source_rowid,
             s.set_of_books_id,
             Null Segment1,
             Null Segment2,
             Null Segment3,
             Null Segment4,
             Null Segment5,
             Null Segment6,
             Null Segment7,
             Null Segment8,
             Null Segment9,
             Null Segment10,
             Null Segment11,
             Null Segment12,
             Null Segment13,
             Null Segment14,
             Null Segment15,
             s.source_assignment_id,
             s.set_of_books_id source_set_of_books_id,
             w.transaction_set_name,
             w.Transaction_Set_id,
             g.creation_date  source_creation_date,
						 --
						 t.source_table_id,
						 y.source_type_id,
						 cx.source_class_id,
						 t.class_mapping_req,
             -- Cost-Plus
             g.Collection_Period_id,
             ac.company,
             ac.department,
             ac.account,
             ac.data_source,
             ac.collector_id,
             ac.period_set_name_id,
             g.attribute1,
             g.attribute2,
             g.attribute3,
             g.attribute4,
             g.attribute5,
             g.attribute6,
             g.attribute7,
             g.attribute8,
             g.attribute9,
             g.attribute10,
             g.attribute11,
             g.attribute12,
             g.attribute13,
             g.attribute14,
             g.attribute15,
             g.attribute16,
             g.attribute17,
             g.attribute18,
             g.attribute19,
             g.attribute20,
             g.vt_replan_company,             
             g.vt_replan_department,          
             g.vt_replan_account,             
             g.vt_replan_transaction_id,      
             g.vt_replan_trading_set_id,      
             g.vt_replan_amount,              
             g.vt_replan_currency_code,       
             g.vt_replan_owner_tax_reg_id,    
             g.vt_replan_partner_tax_reg_id,  
             g.vt_replan_mc_qualifier1,       
             g.vt_replan_cost_category_id,    
             g.vt_replan_category_data_source,
             g.vt_replan_process_history_id,  
             g.vt_replan_period_id,           
             g.vt_replan_payer1,              
             g.vt_replan_payer1_percent,      
             g.vt_replan_payer2,              
             g.vt_replan_payer2_percent,      
             g.vt_replan_inter_payer,         
             g.vt_replan_account_type,
             g.attribute1 cost_category_id,      -- Stamped onto record in Configuration stage
             g.attribute2 category_data_source,  -- Stamped onto record in Configuration stage
             g.attribute3 cost_category,         -- Stamped onto record in Configuration stage
             g.cost_plus_set_id
			  from XXCP_cost_plus_interface     g,
             XXCP_actual_costs            ac,
             XXCP_source_assignments      s,
             -- New
             XXCP_sys_source_tables       t,
             XXCP_sys_source_types        y,
             XXCP_sys_source_classes      cx,
             XXCP_source_transaction_sets w
       where g.vt_status               = 'TRANSACTION'
         and g.vt_source_assignment_id = pSource_Assignment_ID
         and g.vt_source_assignment_id = s.source_assignment_id
         and s.source_id               = pSource_id
         and g.vt_replan_transaction_id = ac.transaction_id
         and g.vt_source_assignment_id = ac.source_assignment_id
         -- Table
         and g.vt_transaction_table    = t.transaction_table
         and t.source_id               = pSource_id
         -- Type
         and g.vt_transaction_type     in ('REV','REP')
         and g.vt_transaction_type     = y.type
         and t.source_table_id         = y.source_table_id
         -- Class
         and g.vt_transaction_class    = decode(t.class_mapping_req,'Y',cx. system_latch,cx.class)
         and y.source_table_id         = cx.source_table_id
         -- Transaction_Set_id
         and y.Transaction_Set_id      = w.Transaction_Set_id
         -- GL Code Combinations
         and g.vt_request_id           = pRequest_id
       order by vt_transaction_table, w.sequence, vt_parent_trx_id,cx.sequence,vt_transaction_id;

 

    GLIRec GLI%Rowtype;

    Cursor ConfErr(pSource_Assignment_id in number, pRequest_id in number) is
      select Distinct k.vt_parent_trx_id, k.vt_transaction_table
        from XXCP_cost_plus_interface k
       where k.vt_request_id           = pRequest_id
         and k.vt_source_assignment_id = pSource_Assignment_id
         and k.vt_status               = 'ERROR';

    -- Assignments
    Cursor sr1(pSource_Group_id in number) is
      select set_of_books_id,
             Source_Assignment_id,
             instance_id source_instance_id,
             Source_id,
						 stamp_parent_trx
        from XXCP_source_assignments g
       where g.Source_Group_id = pSource_Group_id
         and g.active          = 'Y';

    vInternalErrorCode         XXCP_errors.internal_error_code%type := 0;
    vExchange_Rate_Type        XXCP_tax_registrations.exchange_rate_type%type;
    vCommon_Exch_Curr          XXCP_tax_registrations.common_exch_curr%type;

    i                          pls_integer := 0;
    j                          Integer := 0;
    k                          Number;
	  vTiming_Start              number;
    vDML_Compiled              varchar2(1);
    vGlobal_Rounding_Tolerance Number := 0;
    vTransaction_Abort         Boolean := False;

    vGlobalPrecision           pls_integer := 2;
    CommitCnt                  pls_Integer := 0;
    vJob_Status                pls_integer := 0;
    vSource_Activity           XXCP_sys_sources.source_activity%type := 'GL';
    vStaged_Records            XXCP_sys_sources.staged_records%type;

    vTransaction_Class         XXCP_sys_source_classes.Class%type;
    vRequest_ID                XXCP_process_history.request_id%type;

    -- NEW PARAMETERS
    vCurrent_Parent_Trx_id     XXCP_transaction_attributes.parent_trx_id%type := 0;
    vTransaction_Type          XXCP_sys_source_types.Type%type;
    --
    -- Used for the REC Distribution
    vExtraLoop                 Boolean := False;
    vExtraClass                XXCP_sys_source_classes.class%type;
    --
    vSource_Assignment_id      XXCP_source_assignments.source_assignment_id%type;
    vStamp_Parent_Trx          XXCP_source_assignments.stamp_parent_trx%type := 'N';
    vSource_instance_id        XXCP_source_assignments.instance_id%type;
    vSource_ID                 XXCP_sys_sources.source_id%type;

    vClass_Mapping_Name        XXCP_sys_source_classes.Class%type;
    
    vStart_Date                 date;
    vEnd_Date                   date;
    
    vTransaction_Table         varchar2(100);
    vCost_Category             varchar2(100);
    vTerritory                 varchar2(15);
    
    Cursor SF(pSource_Group_id in number) is
      Select s.source_activity,
             s.Source_id,
             Preview_ctl,
             Timing,
             Custom_events,
             s.Cached,
			       s.DML_Compiled,
             Staged_Records
        from XXCP_source_assignment_groups g, XXCP_sys_sources s
       where source_group_id = pSource_Group_id
         and s.source_id = g.source_id;

    Cursor Tolx is
      Select Numeric_Code Global_Rounding_Tolerance
        from XXCP_lookups
       where lookup_type = 'ROUNDING TOLERANCE'
         and lookup_code = 'GLOBAL';

	  Cursor CRL(pSource_id in number) is
     select Distinct Transaction_table, source_id
       from XXCP_column_rules
      where source_id = pSource_id;
      
    Cursor pdt(pPeriod_Set_Name_id in number, pPeriod_name in varchar2) is
      select w.Start_Date, w.End_Date, (w.period_year*100)+w.period_num period_id
      from xxcp_instance_gl_periods_v w
      where w.period_set_name_id = pPeriod_Set_Name_id
        and w.period_name        = pPeriod_name;    
        
    Cursor PayAtt is
      select Profile_Value
      from   XXCP_SYS_PROFILE
      where  profile_name = 'PAYER ATTRIBUTE';

    Cursor RetProAtt is
      select Profile_Value
      from   XXCP_SYS_PROFILE
      where  profile_name = 'PAYER RET/PRO ATTRIBUTE';

    Cursor TER(pCompany_number in varchar2) is
      select territory
      from   XXCP_tax_registrations
      where  company_number = pCompany_number
      and    cost_plus      = 'Y';

   vPeriod_id            Number    := 0;          
   vPayerAttribute       number(4) := 0;
   vPayerRetProAttribute number(4) := 0;


  BEGIN

    xxcp_global.Trace_on   := nvl(gEngine_Trace,'N');
    xxcp_global.Preview_on := 'N';
    xxcp_global.SystemDate := Sysdate;
		vTimingCnt := 1;
    xxcp_global.Forecast_On := 'N';
    xxcp_global.True_up_On  := 'N';
    xxcp_global.Replan_on   := 'Y';

    For SFRec in SF(cSource_Group_id) loop
      xxcp_global.gCommon(1).Source_id     := SFRec.Source_id;
      xxcp_global.gCommon(1).Preview_ctl   := SFRec.Preview_ctl;
      xxcp_global.gCommon(1).Preview_on    := xxcp_global.Preview_on;
      xxcp_global.gCommon(1).Custom_events := SFRec.Custom_events;
      xxcp_global.gCommon(1).current_Source_activity := SFRec.Source_Activity;
      xxcp_global.gCommon(1).Cached_Attributes := SFRec.Cached;

      vSource_Activity := SFRec.Source_Activity;
      vSource_id       := SFRec.Source_id;
      vDML_Compiled    := SFRec.DML_Compiled;
      vStaged_Records  := SFRec.Staged_Records;
    End Loop;

    -- Get dates
    For Rec in pdt(cCalendar_id, cPeriod_Name ) loop -- Passing in Forecast Period.
       vStart_Date := rec.start_date;
       vEnd_Date   := rec.end_date;
       vPeriod_id  := rec.period_id;
    End loop;

    xxcp_global.set_source_id(cSource_id => vSource_id);

    --     SYS.DBMS_SUPPORT.START_TRACE( waits=>true, binds=>true );

    vTiming(vTimingCnt).CF_Records    := 0;
    vTiming(vTimingCnt).Flush_Records := 0;
    vTiming(vTimingCnt).Start_time    := to_char(sysdate, 'HH24:MI:SS');
    vTiming(vTimingCnt).Flush         := 0;
    vTiming_Start            := xxcp_reporting.Get_Seconds;

    If xxcp_global.gCommon(1).Custom_events = 'Y' then
      xxcp_custom_events.Set_system_mode(xxcp_global.gCommon);
    End If;

    --
    -- ## *******************************************************************************************************
    -- ##
    -- ##                            Check to See the process is already in use
    -- ##
    -- ## *******************************************************************************************************
    --
	  If vDML_Compiled = 'N' then
	   -- Prevent Processing and the column rules have changed and
	   -- they need compiling.
	   vJob_Status := 4;
	   xxcp_foundation.show('ERROR: Column Rules have changed, but not re-generated');
	   xxcp_foundation.FndWriteError(10999,'Column Rules have changed, but not re-generated');

	  End If;


    --If NOT xxcp_management.Locked_Process(vSource_Activity, cSource_Group_id) and vJob_Status = 0 then
      --vRequest_id := xxcp_management.Activate_Lock_Process(vSource_Activity,cSource_Group_id,cConc_Request_Id);
      vRequest_id := cRequest_Id;

      --xxcp_foundation.show('Activity locked....' || to_char(vRequest_id));

      xxcp_global.User_id  := fnd_global.user_id;
      xxcp_global.Login_id := fnd_global.login_id;

      xxcp_global.gCommon(1).Current_request_id := vRequest_id;

      If xxcp_global.gCommon(1).Custom_events = 'Y' then
        vInternalErrorCode := xxcp_custom_events.Before_Processing(vSource_id);
      End If;
      --
      -- ## **************************************************************************************************
      -- ##
      -- ##                                Setup Ready for the Run
      -- ##
      -- ## **************************************************************************************************
      --
      
      
      For REC in sr1(cSource_Group_id) Loop

        --
        -- Get ID for run
        --
        select xxcp_forecast_seq.nextval
        into   xxcp_global.gCommon(1).forecast_id
        from   dual;

        Exit when vJob_Status <> 0;

        vSource_instance_id   := rec.source_instance_id;
        vSource_Assignment_id := rec.Source_Assignment_id;

				If vTimingCnt = 1 then
				    xxcp_foundation.show('Initialization....');
            vTiming(vTimingCnt).Init_Start := to_char(sysdate, 'HH24:MI:SS');

            xxcp_te_base.Engine_Init(cSource_id         => vSource_id,
				                             cSource_Group_id   => cSource_Group_id,
																     cInternalErrorCode => vInternalErrorCode);
        End If;

				vTiming(vTimingCnt).Source_Assignment_id := vSource_Assignment_id;

        -- Setup Globals
        xxcp_global.gCommon(1).current_process_name  := 'XXCP_CPAENG';
        xxcp_global.gCommon(1).current_assignment_id := vSource_Assignment_ID;

        -- 02.06.02
        gTrans_Table_Array.delete;

        --
        -- *********************************************************************************************************
        --                            Populate Column Rules and Initialize Cursors
        -- *********************************************************************************************************
        --
        vTiming(vTimingCnt).Memory_Start := to_char(sysdate, 'HH24:MI:SS');

				For Rec in CRL(pSource_id => vSource_id) Loop
          -- Used for column Attributes and error messages
					--
					xxcp_memory_Pack.LoadColumnRules(cSource_id          => vSource_id,
					                                 cSource_Table       => 'XXCP_COST_PLUS_INTERFACE',
																					 cSource_Instance_id => vSource_instance_id,
																					 cTarget_Table       => Rec.Transaction_table,
																					 cRequest_id         => vRequest_id,
																					 cInternalErrorCode  => vInternalErrorCode);

          xxcp_te_base.Init_Cursors(cTarget_Table => Rec.Transaction_table);
				End Loop;


        --
        -- *******************************************************************************************************
        --     Reset Previosly Errored Transactions and Assign Parent Trx Id to Incomming transactions
        -- *******************************************************************************************************
        --
				vInternalErrorCode := 0;

        Reset_Rep_Errored_Transactions(cSource_assignment_id => vSource_Assignment_id,
				                               cRequest_id           => vRequest_id);
 
        Set_Replan_Running_Status(        cSource_id            => vSource_id,
				                                  cSource_assignment_id => vSource_Assignment_id,
													                cRequest_id           => vRequest_id,
                                          cPeriod_Set_Name_id   => cCalendar_id,
                                          cPosting_Period_id    => vPeriod_id);
                             --      cStart_Date           => vStart_Date,
                             --      cEnd_Date             => vEnd_Date);

        Transaction_Replan_Group_Stamp(   cStamp_Parent_Trx     => vStamp_Parent_Trx,
                                   cSource_Assignment_id => vSource_Assignment_id,
																   cInternalErrorCode    => vInternalErrorCode);

				-- Set Assignment Status -- (Not used in Preview)
				If vInternalErrorCode = 0 then
				  Set_Replan_Assignment_Status(cSource_id            => vSource_id,
                                       cSource_Assignment_id => vSource_assignment_id,
                                       cRequest_id           => vRequest_id,
															         cInternalErrorCode    => vInternalErrorCode);

				End If;

				-- Data Staging
				If nvl(vInternalErrorCode,0) = 0 then
				  vInternalErrorCode :=
					  xxcp_custom_events.Data_Staging(cSource_id            => vSource_id,
						                                cSource_Assignment_id => vSource_Assignment_id,
																						cRequest_id           => vRequest_id,
																						cPreview_id           => Null);
        End If;

        vTiming(vTimingCnt).Init_End := to_char(sysdate, 'HH24:MI:SS');
        --
        -- GLOBAL ROUNDING TOLERANCE
        --
        vGlobal_Rounding_Tolerance := 0;
        For Rec in Tolx loop
          vGlobal_Rounding_Tolerance := Rec.Global_Rounding_Tolerance;
        End loop;

        If vInternalErrorCode = 0 then
          --
          -- Get Standard Parameters
          --
					vInternalErrorCode :=
					  xxcp_foundation.fnd_gl_lookup(cExchange_Rate_Type => vExchange_Rate_Type,
						                              cGlobalPrecision    => vGlobalPrecision,
																					cCommon_Exch_Curr   => vCommon_Exch_Curr);
        End If;

        Commit;

        If vInternalErrorCode = 0 then 

          --
          -- ## ***********************************************************************************************************
          -- ##
          -- ##                                Assignment 
          -- ##
          -- ## ***********************************************************************************************************
          --
          i := 0;

          vTiming(vTimingCnt).CF_last       := xxcp_reporting.Get_Seconds;
          vTiming(vTimingCnt).CF            := 0;
          vTiming(vTimingCnt).CF_Records    := 0;
          vTiming(vTimingCnt).CF_Start_Date := Sysdate;
          xxcp_global.SystemDate := sysdate;

          Open CFG(pSource_id             => vSource_id,
					         pSource_Assignment_id  => vSource_Assignment_id,
									 pRequest_id            => vRequest_id);
					Loop
					      Fetch CFG into CFGRec;
								Exit when CFG%Notfound;


                i := i + 1;

                vTiming(vTimingCnt).CF_RECORDS := vTiming(vTimingCnt).CF_RECORDS + 1;

                XXCP_GLOBAL.GCOMMON(1).CURRENT_SOURCE_ROWID      := CFGREC.SOURCE_ROWID;
                XXCP_GLOBAL.GCOMMON(1).CURRENT_TRANSACTION_TABLE := CFGREC.VT_TRANSACTION_TABLE;
                XXCP_GLOBAL.GCOMMON(1).CURRENT_TRANSACTION_ID    := CFGREC.VT_TRANSACTION_ID;
                XXCP_GLOBAL.GCOMMON(1).CURRENT_PARENT_TRX_ID     := CFGREC.VT_PARENT_TRX_ID;
                XXCP_GLOBAL.GCOMMON(1).CURRENT_INTERFACE_ID      := CFGREC.VT_INTERFACE_ID;
								XXCP_GLOBAL.GCOMMON(1).CURRENT_TRANSACTION_DATE  := CFGREC.VT_TRANSACTION_DATE;
								XXCP_GLOBAL.GCOMMON(1).CURRENT_INTERFACE_REF     := CFGREC.VT_TRANSACTION_REF;

                XXCP_GLOBAL.GCOMMON(1).PREVIEW_PARENT_TRX_ID     := NULL;
                XXCP_GLOBAL.GCOMMON(1).PREVIEW_SOURCE_ROWID      := NULL;

                VTRANSACTION_CLASS := CFGREC.VT_TRANSACTION_CLASS;

                IF XXCP_TE_BASE.IGNORE_CLASS(CFGREC.VT_TRANSACTION_TABLE, VTRANSACTION_CLASS) THEN
                  BEGIN
                    UPDATE XXCP_COST_PLUS_INTERFACE
                       SET VT_STATUS              = 'IGNORED',
                           VT_INTERNAL_ERROR_CODE = NULL,
                           VT_DATE_PROCESSED      = XXCP_GLOBAL.SYSTEMDATE
                     WHERE ROWID = CFGREC.SOURCE_ROWID;

                     EXCEPTION WHEN OTHERS THEN NULL;
                  END;
                ELSE

                --
                -- Set Trial Type to True Up
                --
                if CFGREC.VT_TRANSACTION_TABLE = 'FORECAST' then
                  xxcp_global.gCommon(1).cpa_type := 'RF';
                elsif CFGREC.VT_TRANSACTION_TABLE = 'TRUE-UP' then
                  xxcp_global.gCommon(1).cpa_type := 'RT';
                end if;

                --
                -- Get Cost Category
                --
                vInternalErrorCode := xxcp_te_base.Get_Cost_Category(CFGRec.company,
                                                        CFGRec.department,
                                                        CFGRec.account,
                                                        CFGRec.vt_transaction_date,
                                                        xxcp_wks.gActual_Costs_Rec(1).tu_Cost_Category_id,
                                                        xxcp_wks.gActual_Costs_Rec(1).tu_Category_Data_Source,
                                                        vCost_Category,
                                                        CFGRec.cost_plus_set_id);
                --
                -- Territory
                --
                open  TER(CFGRec.company);
                fetch TER into vTerritory;
                close TER;

                --
                -- Get Payee Control Info
                --
                xxcp_wks.Reset_Internal_Array;  -- Clear Internal Array

                vInternalErrorCode := xxcp_te_base.Get_Payer_Details (cPayee            => CFGRec.company,
                                                         cTerritory        => vTerritory,
                                                         cCost_Category_id => xxcp_wks.gActual_Costs_Rec(1).tu_Cost_Category_id, --vCost_Category,
                                                         cData_Source      => xxcp_wks.gActual_Costs_Rec(1).tu_Category_Data_Source,
                                                         cTransaction_Date => CFGRec.vt_transaction_date,
                                                         cPayer1           => xxcp_wks.I1009_Array(123),
                                                         cPayer1_Percent   => xxcp_wks.I1009_Array(124),
                                                         cPayer2           => xxcp_wks.I1009_Array(125),
                                                         cPayer2_Percent   => xxcp_wks.I1009_Array(126),
                                                         cInter_Payer      => xxcp_wks.I1009_Array(127),
                                                         cAccount_Type     => xxcp_wks.I1009_Array(128));

                  xxcp_wks.gActual_Costs_Rec(1).cost_plus_set_id := CFGRec.Cost_Plus_Set_Id;

                  -- Hold Internals for Use in configurator
                  xxcp_wks.I1009_ArrayGLOBAL := xxcp_wks.Clear_Internal;
                  xxcp_wks.I1009_ArrayGLOBAL := xxcp_wks.I1009_Array;
                  XXCP_TE_CONFIGURATOR.Control(
                                               cSource_Assignment_ID     => vSource_assignment_id,
                                               cSet_of_books_id          => CFGRec.Source_Set_of_books_id,
																							 cTransaction_Date         => CFGRec.VT_Transaction_Date,
																							 cSource_id                => vSource_id,
                                               cSource_Table_id          => CFGRec.Source_table_id,
                                               cSource_Type_id           => CFGRec.Source_type_id,
                                               cSource_Class_id          => CFGRec.Source_Class_id,
                                               cTransaction_id           => CFGRec.VT_Transaction_ID,
                                               cSourceRowid              => CFGRec.Source_Rowid,
                                               cParent_Trx_id            => CFGRec.VT_Parent_Trx_id,
                                               cTransaction_Set_id       => CFGRec.Transaction_Set_id,
                                               cSpecial_Array            => 'N',
                                               cKey                      => Null,
																							 cTransaction_Table        => CFGRec.VT_Transaction_Table,
		                                           																					 cTransaction_Type         => CFGRec.VT_Transaction_Type,
                                               cTransaction_Class        => vTransaction_Class,
                                               cInternalErrorCode        => vInternalErrorCode);


                  -- check status
                  If vInternalerrorcode = -1 then
                    update XXCP_cost_plus_interface
                    --   set vt_status              = 'SUCCESSFUL',
                       set vt_status              = 'TRIAL',
                           vt_internal_error_code = null,
                           vt_date_processed      = xxcp_global.systemdate,
                           vt_status_code         = 7001
                     where rowid = cfgrec.source_rowid;
                  Elsif nvl(vInternalErrorCode, 0) <> 0 then
                    update XXCP_cost_plus_interface
                       set vt_status              = 'ERROR',
                           vt_internal_error_code = 12000,
                           vt_date_processed      = xxcp_global.systemdate
                     where rowid = cfgrec.source_rowid;

                  ELSIF nvl(vInternalErrorCode, 0) = 0 THEN

                    update XXCP_cost_plus_interface
                       set vt_status              = 'TRANSACTION',
                           vt_internal_error_code = null,
                           vt_date_processed        = xxcp_global.systemdate,
                           attribute1               = xxcp_wks.gActual_Costs_Rec(1).tu_Cost_Category_id,
                           attribute2               = xxcp_wks.gActual_Costs_Rec(1).tu_Category_Data_Source,
                           vt_replan_payer1         = xxcp_wks.I1009_Array(123),
                           vt_replan_payer1_percent = xxcp_wks.I1009_Array(124),
                           vt_replan_payer2         = xxcp_wks.I1009_Array(125),
                           vt_replan_payer2_percent = xxcp_wks.I1009_Array(126),
                           vt_replan_inter_payer    = xxcp_wks.I1009_Array(127),
                           vt_replan_account_type   = xxcp_wks.I1009_Array(128)                           
                     where rowid = cfgrec.source_rowid;

                  End if;

									-- emergency exit (out of disk space)
									if vinternalerrorcode between 3550 and 3559 then
									  rollback;
										vjob_status := 6;
										exit;
									end if;

                End if; -- end Assignment

                -- ##
                -- ## COMMIT ASSIGNMENT ROWS
                -- ##

                If i >= 2000 then
                  xxcp_global.systemdate    := sysdate;
                  commit;
                  i           := 0;

                  vjob_status :=
									  xxcp_management.Has_Oracle_Been_Terminated(
                                         cRequest_id      => vRequest_id,
                                         cSource_Activity => vSource_activity,
                                         cSource_Group_id => cSource_group_id,
                                         cConc_Request_Id => cConc_request_id);
                  exit when vjob_status > 0; -- controlled exit
                End if;

          END Loop;
          CLOSE CFG;

          xxcp_dynamic_sql.Close_Session;
          -- #
          -- # Clear Common vars
          -- #
          xxcp_global.gCommon(1).current_transaction_table := Null;
          xxcp_global.gCommon(1).current_transaction_id    := Null;
          xxcp_global.gCommon(1).current_parent_trx_id     := Null;
					xxcp_global.gcommon(1).current_interface_ref     := Null;

          Commit; -- Final Configuration Commit.

          --
          If xxcp_global.gCommon(1).Custom_events = 'Y' then
            xxcp_custom_events.After_assignment_processing(cSource_id            => vSource_id,
                                                           cSource_assignment_id => vSource_assignment_id);
          End If;

          vTiming(vTimingCnt).CF          := xxcp_reporting.Get_Seconds_Diff(cSec1 => vTiming(vTimingCnt).CF_last, cSec2 => xxcp_reporting.Get_Seconds);
          vTiming(vTimingCnt).CF_End_Date := Sysdate;

          -- ***********************************************************************************************************
          --
          --  Set associated Transactions to error if anyone of the Transactions is a set has errored in configuration
          --
          -- ***********************************************************************************************************
          --
          xxcp_foundation.show('Reporting Configuration errors....');
          xxcp_global.SystemDate := Sysdate;
          vInternalErrorCode := 0;
          For ConfErrRec in ConfErr(vSource_Assignment_id, vRequest_id) loop

            Begin
              update XXCP_cost_plus_interface
                 set vt_status              = 'ERROR',
                     vt_internal_Error_code = 12824, -- Associated errors
                     vt_date_processed      = xxcp_global.SystemDate
               where vt_request_id = vRequest_id
                 and vt_Source_Assignment_id = vSource_Assignment_id
                 and vt_status IN ('ASSIGNMENT', 'TRANSACTION')
                 and vt_parent_trx_id = ConfErrRec.vt_parent_trx_id
                 and vt_transaction_table = ConfErrRec.vt_Transaction_table;

            Exception
              When OTHERS then
                vInternalErrorCode := 12819; -- Update Failed.

            End;
          End Loop;
           

          --## ***********************************************************************************************************
          --##
          --##                                    Transaction Engine
          --##
          --## ***********************************************************************************************************

          vTiming(vTimingCnt).TE_Last       := xxcp_reporting.Get_Seconds;
          vTiming(vTimingCnt).TE_Records    := 0;
          vTiming(vTimingCnt).TE_Start_Date := Sysdate;

          vExtraLoop             := False;
          vExtraClass            := Null;
          xxcp_global.SystemDate := Sysdate;
          i                := 0;

          If vInternalErrorCode = 0 and vJob_Status = 0 then

            xxcp_foundation.show('Transactions....');
            xxcp_wks.Reset_Source_Segments;
            gBalancing_Array      := xxcp_wks.Clear_Segments;
            ClearWorkingStorage;
 
						Open GLI(pSource_id            => vSource_id,
						         pSource_Assignment_id => vSource_assignment_id,
										 pRequest_id           => vRequest_id);
						Loop

							    Fetch GLI into GLIRec;
							    Exit when GLI%Notfound;

                  vInternalErrorCode := 0;
                  Exit when vJob_Status > 0;
                  vTiming(vTimingCnt).TE_Records := vTiming(vTimingCnt).TE_Records + 1;
                  --
                  -- ***********************************************************************************************************
                  --        Set the balancing Setments to be written to the Column Rules Array
                  -- ***********************************************************************************************************
                  --
                  -- Poplulate the Balancing Array
                  xxcp_wks.Source_Balancing(1) := Null;
                  xxcp_wks.Source_Balancing(2) := GLIRec.be2; -- User_Je_Source_Name
                  xxcp_wks.Source_Balancing(3) := GLIRec.be3; -- Group Id
                  xxcp_wks.Source_Balancing(4) := GLIRec.be4; -- User Je Category Name
                  xxcp_wks.Source_Balancing(5) := Null;
                  xxcp_wks.Source_Balancing(6) := GLIRec.be6; -- Reference 1
                  xxcp_wks.Source_Balancing(7) := GLIRec.be7; -- Reference 2
                  xxcp_wks.Source_Balancing(8) := GLIRec.be8; -- Reference 3
                  xxcp_wks.Source_Balancing(9) := Null;
                  xxcp_wks.Source_Balancing(10):= Null;
                  --
                  -- ***********************************************************************************************************
                  --         Move Accounting Codes from Master Record to Array
                  -- ***********************************************************************************************************
                  -- Populate Source Account Segments
                  xxcp_wks.Source_Segments(01) := GLIRec.Segment1;
                  xxcp_wks.Source_Segments(02) := GLIRec.Segment2;
                  xxcp_wks.Source_Segments(03) := GLIRec.Segment3;
                  xxcp_wks.Source_Segments(04) := GLIRec.Segment4;
                  xxcp_wks.Source_Segments(05) := GLIRec.Segment5;
                  xxcp_wks.Source_Segments(06) := GLIRec.Segment6;
                  xxcp_wks.Source_Segments(07) := GLIRec.Segment7;
                  xxcp_wks.Source_Segments(08) := GLIRec.Segment8;
                  xxcp_wks.Source_Segments(09) := GLIRec.Segment9;
                  xxcp_wks.Source_Segments(10) := GLIRec.Segment10;
                  xxcp_wks.Source_Segments(11) := GLIRec.Segment11;
                  xxcp_wks.Source_Segments(12) := GLIRec.Segment12;
                  xxcp_wks.Source_Segments(13) := GLIRec.Segment13;
                  xxcp_wks.Source_Segments(14) := GLIRec.Segment14;
                  xxcp_wks.Source_Segments(15) := GLIRec.Segment15;

                  --xxcp_wks.Source_Segments(GLIRec.company_seg)    := GLIRec.Company;
                  --xxcp_wks.Source_Segments(GLIRec.Department_Seg) := GLIRec.Department;
                  --xxcp_wks.Source_Segments(GLIRec.Account_Seg)    := GLIRec.Account;

                  xxcp_global.New_Transaction := 'N';

                  --
                  -- ***********************************************************************************************************
                  --         When a New Transaction is detected then flush the current buffer
                  -- ***********************************************************************************************************
                  --
                  If (vCurrent_Parent_Trx_id <> GLIRec.vt_parent_Trx_id)
                      or
                     (xxcp_wks.gActual_Costs_Rec(1).Transaction_Table <> GLIRec.vt_transaction_table) then  -- SJS

                    vTransaction_Abort := False;

                    --
                    -- !! ******************************************************************************************************
                    --                             FLUSH TRANSACTION
                    -- !! ******************************************************************************************************
                    --
                    If vCurrent_Parent_Trx_id <> 0 then
                      CommitCnt := CommitCnt + 1; -- Number of records process since last commit.

                      k := FlushRecordsToTarget(cPeriod_Set_Name_id        => cCalendar_id,
                                                cGlobal_Rounding_Tolerance => vGlobal_Rounding_Tolerance,
                                                cGlobalPrecision           => vGlobalPrecision,
                                                cTransaction_Type          => vTransaction_Type,
                                                cInternalErrorCode         => vInternalErrorCode,
                                                cReplan                    => 'Y');
                    End If;

									  xxcp_global.New_Transaction := 'Y';

									  --
                    -- ***********************************************************************************************************
                    --            Store Variables that are only set once per Parent Trx Id
                    -- ***********************************************************************************************************
                    --
                    xxcp_wks.Trace_Log     := Null;
                    xxcp_global.gCommon(1).current_transaction_table := GLIRec.vt_Transaction_Table;
                    xxcp_global.gCommon(1).current_transaction_type  := GLIRec.vt_Transaction_Type;
                    xxcp_global.gCommon(1).current_parent_trx_id     := GLIRec.vt_Parent_Trx_id;
                    vCurrent_Parent_Trx_id := GLIRec.vt_Parent_Trx_id;
                    vTransaction_Type      := GLIRec.vt_Transaction_Type;
										ClearWorkingStorage;
                    --
                    -- ***********************************************************************************************************
                    --                         Extra Loops for different Classes
                    -- ***********************************************************************************************************
                    --
                    vExtraLoop  := False;
                    vExtraClass := Null;
                    For j in 1 .. xxcp_global.gSRE.Count loop

                      If GLIRec.vt_Transaction_Table = xxcp_global.gSRE(j).Transaction_table then
                        vExtraClass := xxcp_global.gSRE(j).Extra_Record_type;
                        vExtraLoop  := TRUE;
                        Exit;
                      End If;
                    End Loop;

                  Else
                    vExtraLoop  := FALSE; -- !!!!
                    vExtraClass := Null;
                  End If;

                  xxcp_global.gCommon(1).current_transaction_id := GLIRec.vt_Transaction_id;
                  --
                  -- ***********************************************************************************************************
                  --                                   Store Working Variables
                  -- ***********************************************************************************************************
                  --
                  xxcp_global.gCommon(1).current_source_rowid    := GLIRec.Source_Rowid;
                  xxcp_global.gCommon(1).current_Interface_id    := GLIRec.vt_interface_id;
                  --xxcp_global.gCommon(1).current_Accounting_date := GLIRec.Accounting_Date;
                  xxcp_global.gCommon(1).current_Accounting_date := vEnd_Date;
                  xxcp_global.gCommon(1).current_Batch_number    := GLIRec.Group_id;
                  xxcp_global.gCommon(1).current_Category_name   := GLIRec.Category_Name;
                  xxcp_global.gCommon(1).current_Source_name     := GLIRec.Source_name;
								  xxcp_global.gcommon(1).current_interface_ref   := GLIRec.vt_transaction_ref;
                  xxcp_global.gcommon(1).current_creation_date   := GLIRec.source_creation_date;

                  --
                  -- **************************************************************************************************
                  -- Cost-Plus Values
                  -- **************************************************************************************************
                  -- 
                  --xxcp_wks.gActual_Costs_Rec(1).TU_Cost_Category_id   := GLIRec.TU_Cost_Category_id;              
                  xxcp_wks.gActual_Costs_Rec(1).Company               := GLIRec.Company;              
                  xxcp_wks.gActual_Costs_Rec(1).Department            := GLIRec.Department;              
                  xxcp_wks.gActual_Costs_Rec(1).Account               := GLIRec.Account;              
                  xxcp_wks.gActual_Costs_Rec(1).Collection_Period_id  := GLIRec.Collection_Period_id;              
                  xxcp_wks.gActual_Costs_Rec(1).Currency_Code         := GLIRec.Currency_Code;              
                  xxcp_wks.gActual_Costs_Rec(1).Transaction_id        := GLIRec.VT_Transaction_id;              
                  xxcp_wks.gActual_Costs_Rec(1).Collector_id          := GLIRec.Collector_id;
                  xxcp_wks.gActual_Costs_Rec(1).period_set_name_id    := GLIRec.period_set_name_id; 
                  xxcp_wks.gActual_Costs_Rec(1).data_source           := GLIRec.data_source;         -- 02.04.02
                  xxcp_wks.gActual_Costs_Rec(1).Period_id             := xxcp_translations.Period_id(cPeriod_Name => cPeriod_Name,
                                                                                                     cInstance_id => GLIRec.vt_instance_id);               
--                  xxcp_wks.gActual_Costs_Rec(1).Payer                 := xxcp_foundation.Find_DynamicAttribute(vPayerAttribute);
--                  xxcp_wks.gActual_Costs_Rec(1).Payer_ret_pro_ind     := xxcp_foundation.Find_DynamicAttribute(vPayerRetProAttribute);
                  xxcp_wks.gActual_Costs_Rec(1).Replan                := 'N';
                  xxcp_wks.gActual_Costs_Rec(1).Attribute1            := GLIRec.Attribute1;
                  xxcp_wks.gActual_Costs_Rec(1).Attribute2            := GLIRec.Attribute2;
                  xxcp_wks.gActual_Costs_Rec(1).Attribute3            := GLIRec.Attribute3;
                  xxcp_wks.gActual_Costs_Rec(1).Attribute4            := GLIRec.Attribute4;
                  xxcp_wks.gActual_Costs_Rec(1).Attribute5            := GLIRec.Attribute5;
                  xxcp_wks.gActual_Costs_Rec(1).Attribute6            := GLIRec.Attribute6;
                  xxcp_wks.gActual_Costs_Rec(1).Attribute7            := GLIRec.Attribute7;
                  xxcp_wks.gActual_Costs_Rec(1).Attribute8            := GLIRec.Attribute8;
                  xxcp_wks.gActual_Costs_Rec(1).Attribute9            := GLIRec.Attribute9;
                  xxcp_wks.gActual_Costs_Rec(1).Attribute10           := GLIRec.Attribute10;
                  xxcp_wks.gActual_Costs_Rec(1).Attribute11           := GLIRec.Attribute11;
                  xxcp_wks.gActual_Costs_Rec(1).Attribute12           := GLIRec.Attribute12;
                  xxcp_wks.gActual_Costs_Rec(1).Attribute13           := GLIRec.Attribute13;
                  xxcp_wks.gActual_Costs_Rec(1).Attribute14           := GLIRec.Attribute14;
                  xxcp_wks.gActual_Costs_Rec(1).Attribute15           := GLIRec.Attribute15;
                  xxcp_wks.gActual_Costs_Rec(1).Attribute16           := GLIRec.Attribute16;
                  xxcp_wks.gActual_Costs_Rec(1).Attribute17           := GLIRec.Attribute17;
                  xxcp_wks.gActual_Costs_Rec(1).Attribute18           := GLIRec.Attribute18;
                  xxcp_wks.gActual_Costs_Rec(1).Attribute19           := GLIRec.Attribute19;
                  xxcp_wks.gActual_Costs_Rec(1).Attribute20           := GLIRec.Attribute20;

                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_COMPANY              := GLIRec.VT_REPLAN_COMPANY;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_DEPARTMENT           := GLIRec.VT_REPLAN_DEPARTMENT;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_ACCOUNT              := GLIRec.VT_REPLAN_ACCOUNT; 
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_TRANSACTION_ID       := GLIRec.VT_REPLAN_TRANSACTION_ID;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_TRADING_SET_ID       := GLIRec.VT_REPLAN_TRADING_SET_ID;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_AMOUNT               := GLIRec.VT_REPLAN_AMOUNT;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_CURRENCY_CODE        := GLIRec.VT_REPLAN_CURRENCY_CODE;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_OWNER_TAX_REG_ID     := GLIRec.VT_REPLAN_OWNER_TAX_REG_ID;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_PARTNER_TAX_REG_ID   := GLIRec.VT_REPLAN_PARTNER_TAX_REG_ID;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_MC_QUALIFIER1        := GLIRec.VT_REPLAN_MC_QUALIFIER1;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_COST_CATEGORY_ID     := GLIRec.VT_REPLAN_COST_CATEGORY_ID;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_CATEGORY_DATA_SOURCE := GLIRec.VT_REPLAN_CATEGORY_DATA_SOURCE;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_PROCESS_HISTORY_ID   := GLIRec.VT_REPLAN_PROCESS_HISTORY_ID;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_PERIOD_ID            := GLIRec.VT_REPLAN_PERIOD_ID; 
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_PAYER1               := GLIRec.VT_REPLAN_PAYER1;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_PAYER1_PERCENT       := GLIRec.VT_REPLAN_PAYER1_PERCENT;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_PAYER2               := GLIRec.VT_REPLAN_PAYER2;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_PAYER2_PERCENT       := GLIRec.VT_REPLAN_PAYER2_PERCENT; 
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_INTER_PAYER          := GLIRec.VT_REPLAN_INTER_PAYER;
                  xxcp_wks.gActual_Costs_Rec(1).VT_REPLAN_ACCOUNT_TYPE         := GLIRec.VT_REPLAN_ACCOUNT_TYPE;

                  xxcp_wks.gActual_Costs_Rec(1).tu_Cost_Category_id     := GLIRec.Cost_Category_Id;
                  xxcp_wks.gActual_Costs_Rec(1).tu_Category_Data_Source := GLIRec.Category_Data_Source;

                  xxcp_wks.gActual_Costs_Rec(1).Transaction_Table     := GLIRec.vt_transaction_table;
                  xxcp_wks.gActual_Costs_Rec(1).Interface_id          := GLIRec.vt_interface_id;
                  xxcp_wks.gActual_Costs_Rec(1).cost_plus_set_id      := GLIRec.cost_plus_set_id;

                  --
                  -- Set Trial Type to True Up
                  --
                  if GLIREC.VT_TRANSACTION_TABLE = 'FORECAST' then
                    xxcp_global.gCommon(1).cpa_type := 'RF';
                  elsif GLIREC.VT_TRANSACTION_TABLE = 'TRUE-UP' then
                    xxcp_global.gCommon(1).cpa_type := 'RT';
                  end if;

                  --
                  -- ***********************************************************************************************************
                  --                                   Call the Transaction Engine
                  -- ***********************************************************************************************************
                  --
                  If vTransaction_Abort = False then
                    vInternalErrorCode := 0; -- Reset for each row

                    i := i + 1; -- Count the number of rows processed.

                    xxcp_wks.SOURCE_CNT  := xxcp_wks.SOURCE_CNT + 1;
                    xxcp_wks.SOURCE_ROWID(xxcp_wks.source_cnt)   := GLIRec.Source_Rowid;
                    xxcp_wks.SOURCE_SUB_CODE(xxcp_wks.source_cnt):= 0;
                    xxcp_wks.SOURCE_STATUS(xxcp_wks.source_cnt)  := '?';

                    -- *********************************************************************
                    -- Class Mapping
                    -- *********************************************************************
										If GLIRec.class_mapping_req = 'Y' then
  									    vClass_Mapping_Name := xxcp_te_base.class_mapping(
												                                   cSource_id              => vSource_id,
										                                       cClass_Mapping_Required => GLIRec.class_mapping_req,
																													 cTransaction_Table      => GLIRec.vt_transaction_table,
																													 cTransaction_Type       => GLIRec.vt_transaction_type,
																													 cTransaction_Class      => GLIRec.vt_transaction_class,
																													 cTransaction_id         => GLIRec.vt_transaction_id,
																													 cCode_Combination_id    => GLIRec.code_combination_id,
																													 cAmount                 => GLIRec.Transaction_Cost);
                    End If;
                    -- *********************************************************************
                    -- End Class Mapping
                    -- *********************************************************************


                    XXCP_TE_ENGINE.Control(cSource_assignment_id    => vSource_Assignment_id,
                                           cSource_Table_id         => GLIRec.Source_Table_id,
																					 cSource_Type_id          => GLIRec.Source_Type_id,
                                           cSource_Class_id         => GLIRec.Source_Class_id,
                                           cTransaction_id          => GLIRec.vt_Transaction_id,
                                           cParent_Trx_id           => GLIRec.vt_Parent_Trx_id,
                                           cSourceRowid             => GLIRec.Source_Rowid,
                                           cParent_Entered_Amount   => GLIRec.Parent_Entered_Amount,
                                           cParent_Entered_Currency => GLIRec.Parent_Entered_Currency,
                                           cClass_Mapping           => GLIRec.class_mapping_req,
                                           cClass_Mapping_Name      => vClass_Mapping_Name,
                                           --
                                           cInterface_ID            => GLIRec.VT_Interface_id,
                                           cExtraLoop               => vExtraLoop,
                                           cExtraClass              => vExtraClass,
                                           cTransaction_set_id      => GLIRec.Transaction_Set_id,
																					 --
																					 cTransaction_Table       => GLIRec.VT_Transaction_Table,
																					 cTransaction_Type        => GLIRec.VT_Transaction_Type,
                                           cTransaction_Class       => GLIRec.vt_Transaction_Class,
                                           cInternalErrorCode       => vInternalErrorCode);



                    xxcp_global.gCommon(1).current_trading_set := Null;

                    -- Report Errors
/*                    If xxcp_wks.gActual_Costs_Rec(1).Replan = 'Y' then
                         Category_Change_Transaction(cInternalErrorCode => vInternalErrorCode,
                                                     cCalendar_id       => cCalendar_id,
                                                     cInstance_id       => GLIRec.VT_Instance_id);                    
                         xxcp_foundation.FndWriteError(999,'SJS : Category Change');
                         xxcp_wks.gActual_Costs_Rec(1).Replan := 'N';
                    End if;
*/

-- Now need to mark original as inactive.

                    If vInternalErrorCode <> 0 then
                        Error_Whole_Transaction(cInternalErrorCode => vInternalErrorCode);
                        vTransaction_Abort := True;
                    End If;

                  End If;
                  --
                  -- ***********************************************************************************************************
                  --                     Test to See if Oracle or the User has terminated this process
                  --                                   And Commit after Each 1000 Records processed
                  -- ***********************************************************************************************************
                  --
                  IF CommitCnt >= 2000 THEN
                    CommitCnt := 0;
                    xxcp_global.SystemDate    := sysdate;
                    Commit;

                    vJob_Status :=
										 xxcp_management.Has_Oracle_been_Terminated(cRequest_id      => vRequest_id,
                                                                cSource_Activity => vSource_activity,
                                                                cSource_Group_id => cSource_group_id,
                                                                cConc_Request_Id => cConc_request_id);
                    Exit when vJob_Status > 0; -- Controlled Exit
                  END IF;

            END LOOP;

			      Close GLI;

            xxcp_te_base.Close_Cursors;

            --
            --  !!***********************************************************************************************************
            --                     Flush the Buffer for a Final Time
            -- !! ***********************************************************************************************************
            --
            IF vCurrent_Parent_Trx_id <> 0 and vInternalErrorCode = 0 then
              If vInternalErrorCode = 0 then


                k := FlushRecordsToTarget(cPeriod_Set_Name_id        => cCalendar_id, 
                                          cGlobal_Rounding_Tolerance => vGlobal_Rounding_Tolerance,
                                          cGlobalPrecision           => vGlobalPrecision,
                                          cTransaction_Type          => vTransaction_type,
                                          cInternalErrorCode         => vInternalErrorCode,
                                          cReplan                    => 'Y');
              End If;
            End If;
          End If; -- Internal Error Check GLI
        End If; -- End Internal Error Check

        vTiming(vTimingCnt).TE := xxcp_reporting.Get_Seconds_Diff(cSec1 => vTiming(vTimingCnt).TE_last, cSec2 => xxcp_reporting.Get_Seconds);

        xxcp_foundation.show('Clear down...');
        xxcp_global.SystemDate := Sysdate;
        -- *******************************************************************
        -- Error out records remaining with a Processing status after the run
        -- *******************************************************************

        Begin
          update XXCP_cost_plus_interface
             set vt_status              = 'ERROR',
                 vt_internal_error_code = Decode(vt_status,'TRANSACTION',12999,'ASSIGNMENT',12998),
                 vt_date_processed      = xxcp_global.SystemDate
           where Rowid = any
           (select g.rowid Source_Rowid
                    from XXCP_cost_plus_interface g
                   where g.vt_request_id = xxcp_global.gCommon(1)
                  .current_request_id
                     and g.vt_source_assignment_id = vSource_Assignment_id
                     and g.vt_status in ('ASSIGNMENT', 'TRANSACTION'));

           Exception when OTHERS then Null;
        End;

        -- #
        -- # Clear Common vars
        -- #
        xxcp_global.gCommon(1).current_transaction_table := Null;
        xxcp_global.gCommon(1).current_transaction_id    := Null;
        xxcp_global.gCommon(1).current_parent_trx_id     := Null;
				xxcp_global.gcommon(1).current_interface_ref     := Null;

        --
        -- ***********************************************************************************************************
        --                     Clear Down
        -- ***********************************************************************************************************
        --
        ClearWorkingStorage;
        --
        -- ***********************************************************************************************************
        --                     DeActive the Lock in XXCP_ACTIVITY_CONTROL
        -- ***********************************************************************************************************
        --
        Commit;
        vTiming(vTimingCnt).TE_End_Date := Sysdate;
				vTimingCnt := vTimingCnt + 1;
      End loop; -- SR1


       --
       -- Re-process the original STD True-Up Records
       -- which created replan.
       --
       
       xxcp_foundation.show('True-Up Re-process Starting...');
       
       vInternalErrorCode := control(cSource_group_id  => cSource_group_id,
                                     cConc_request_id  => cConc_request_id,
                                     cTable_group_id   => cTable_group_id,
                                     cCalendar_id      => cCalendar_id,
                                     cPeriod_name      => cPeriod_name,
                                     cRestart          => 'N',
                                     cReplan_reprocess => 'Y',
                                     cRequest_id       => cRequest_id);
                                     --cPrev_Forecast_id => cPrev_Forecast_id);

       xxcp_foundation.show('True-Up Re-process Finishing...');

      xxcp_te_base.ClearDown;
      xxcp_memory_pack.ClearDown;
      xxcp_dynamic_sql.Close_Session;


      Commit;

    --Else
      --
      -- Report If this process was already in use
      --
    --  xxcp_foundation.show('Engine already in use');
    --  vJob_Status := 1;
    --End If;


    xxcp_te_base.Write_Timings(vTiming, vTiming_Start);

    -- ************************************************************************************************
    --                     Show Time Statistics when called from SQL*Plus
    -- ************************************************************************************************
    --
    xxcp_global.gCommon(1).Current_Request_id := Null;
    --
    -- ***********************************************************************************************************
    --                     Return Job Status Code and Finish!!
    -- ***********************************************************************************************************
    --
    Return(vJob_Status);

  End Replan_Control;

  --  !! ***********************************************************************************************************
  --  !!
  --  !!                                    Commit True-Up
  --  !!                  External Function to commit the True-Up and Replan
  --  !!                               to the standard VT Tables.
  --  !!
  --  !! ***********************************************************************************************************
  Function Commit_True_Up(cSource_Group_ID  IN NUMBER,
                          cCost_Plus_Set_id IN NUMBER DEFAULT NULL) return Number is
  
    cursor c1(pSource_Assignment_id in number) is 
        select distinct forecast_id 
          from XXCP_fc_process_history f,
               XXCP_cost_plus_interface x
         where f.interface_id = x.vt_interface_id
           and x.vt_source_assignment_id = pSource_Assignment_id
           and x.vt_status in ('ERROR', 'TRIAL')
           and x.vt_transaction_table in ('TRUE-UP','FORECAST')
           and x.vt_transaction_type  in ('STD','REV','REP')
           and f.cpa_type in ('T','RT','RF')
           and x.cost_plus_set_id = nvl(cCost_Plus_Set_id,x.Cost_Plus_Set_id);
                 
    vForecast_ID number;
    vHdrCnt      number;
    vAttCnt      number;
    vCacheCnt    number;
    vHistoryCnt  number;
    vJournalCnt  number;
    vForecastCnt number;
    vTrueUpCnt   number;
    
    cursor c2(pSource_Group_id in number) is
     select source_assignment_id
     from XXCP_source_assignments g
     where g.source_group_id = pSource_Group_id;
    
  Begin
  
   For SGRec in c2(cSource_Group_ID) Loop
   
     For fcRec in c1(SGRec.Source_Assignment_id) Loop
        vForecast_ID := fcRec.Forecast_Id;
    
        If vForecast_ID is null then
           xxcp_foundation.show('Error. No True-Up found to process - aborting...');
        Else
    
          xxcp_foundation.show('Committing True Up...');
      
          -- Move Header
          insert into XXCP_transaction_header (HEADER_ID,              
                                         SOURCE_ASSIGNMENT_ID,   
                                         PARENT_TRX_ID,          
                                         TRADING_SET_ID,         
                                         SOURCE_TABLE_ID,        
                                         TRANSACTION_DATE,       
                                         SET_OF_BOOKS_ID,        
                                         OWNER_TAX_REG_ID,       
                                         OWNER_LEGAL_CURR,       
                                         OWNER_LEGAL_EXCH_RATE,
                                         OWNER_ASSOC_ID,         
                                         OWNER_ASSIGN_RULE_ID,   
                                         EXCHANGE_DATE,          
                                         FROZEN,                 
                                         INTERNAL_ERROR_CODE,    
                                         TRANSACTION_REF1,       
                                         TRANSACTION_REF2,       
                                         TRANSACTION_REF3,       
                                         TRANSACTION_CURRENCY,   
                                         TRANSACTION_EXCH_RATE,
                                         CREATED_BY,             
                                         CREATION_DATE,          
                                         LAST_UPDATED_BY,        
                                         LAST_UPDATE_DATE,       
                                         LAST_UPDATE_LOGIN)
                                 (select HEADER_ID,              
                                         SOURCE_ASSIGNMENT_ID,   
                                         PARENT_TRX_ID,          
                                         TRADING_SET_ID,         
                                         SOURCE_TABLE_ID,        
                                         TRANSACTION_DATE,       
                                         SET_OF_BOOKS_ID,        
                                         OWNER_TAX_REG_ID,       
                                         OWNER_LEGAL_CURR,       
                                         OWNER_LEGAL_EXCH_RATE,
                                         OWNER_ASSOC_ID,         
                                         OWNER_ASSIGN_RULE_ID,   
                                         EXCHANGE_DATE,          
                                         FROZEN,                 
                                         INTERNAL_ERROR_CODE,    
                                         TRANSACTION_REF1,       
                                         TRANSACTION_REF2,       
                                         TRANSACTION_REF3,       
                                         TRANSACTION_CURRENCY,   
                                         TRANSACTION_EXCH_RATE,
                                         CREATED_BY,             
                                         CREATION_DATE,          
                                         LAST_UPDATED_BY,        
                                         LAST_UPDATE_DATE,       
                                         LAST_UPDATE_LOGIN           
                                  from   XXCP_fc_transaction_header
                                  where  forecast_id    = vForecast_id
                                  and    cpa_type in ('T','RT','RF')
                                  and    cost_plus_set_id = nvl(cCost_Plus_Set_id,Cost_Plus_Set_id));
            vHdrCnt := SQL%ROWCOUNT;
                                            
             -- Move Process History
            Insert into XXCP_process_history (PROCESS_HISTORY_ID,             
                                      TARGET_ASSIGNMENT_ID,           
                                      ATTRIBUTE_ID,                   
                                      RECORD_TYPE,                    
                                      SOURCE_ID,                      
                                      REQUEST_ID,                     
                                      STATUS,                         
                                      INTERFACE_ID,                   
                                      RULE_ID,                        
                                      MODEL_ID,                       
                                      SET_OF_BOOKS_ID,                
                                      ACCOUNTING_DATE,                
                                      TRANSACTION_CLASS,              
                                      CATEGORY,                       
                                      SOURCE,                         
                                      CURRENCY_CODE,                  
                                      SEGMENT1,                       
                                      SEGMENT2,                       
                                      SEGMENT3,                       
                                      SEGMENT4,                       
                                      SEGMENT5,                       
                                      SEGMENT6,                      
                                      SEGMENT7,                       
                                      SEGMENT8,                       
                                      SEGMENT9,                       
                                      SEGMENT10,                      
                                      SEGMENT11,                      
                                      SEGMENT12,                      
                                      SEGMENT13,                      
                                      SEGMENT14,                      
                                      SEGMENT15,                      
                                      ENTERED_DR,                     
                                      ENTERED_CR,                     
                                      ENTERED_EXCH_RATE,              
                                      ACCOUNTED_DR,                   
                                      ACCOUNTED_CR,                   
                                      ACCOUNTED_EXCH_RATE,            
                                      BATCH_NUMBER,                   
                                      MODEL_CTL_ID,                   
                                      TAX_REGISTRATION_ID,            
                                      ROUNDING_AMOUNT,                
                                      ENTERED_ROUNDING_AMOUNT,        
                                      TARGET_INSTANCE_ID,             
                                      SOURCE_ACTIVITY,                
                                      ATTRIBUTE1,                     
                                      ATTRIBUTE2,                     
                                      ATTRIBUTE3,                     
                                      ATTRIBUTE4,                     
                                      ATTRIBUTE5,                     
                                      ATTRIBUTE6,                     
                                      ATTRIBUTE7,                     
                                      ATTRIBUTE8,                     
                                      ATTRIBUTE9,                     
                                      ATTRIBUTE10,                    
                                      PROCESS_SUMMARY_ID,             
                                      ICS_FLAG,                       
                                      CREATION_DATE,                  
                                      CREATED_BY,                     
                                      LAST_UPDATE_DATE,               
                                      LAST_UPDATED_BY,                
                                      LAST_UPDATE_LOGIN)              
                               select PROCESS_HISTORY_ID,             
                                      TARGET_ASSIGNMENT_ID,           
                                      ATTRIBUTE_ID,                   
                                      RECORD_TYPE,                    
                                      SOURCE_ID,                      
                                      REQUEST_ID,                     
                                      STATUS,                         
                                      INTERFACE_ID,                   
                                      RULE_ID,                        
                                      MODEL_ID,                       
                                      SET_OF_BOOKS_ID,                
                                      ACCOUNTING_DATE,                
                                      TRANSACTION_CLASS,              
                                      CATEGORY,                       
                                      SOURCE,                         
                                      CURRENCY_CODE,                  
                                      SEGMENT1,                       
                                      SEGMENT2,                       
                                      SEGMENT3,                       
                                      SEGMENT4,                       
                                      SEGMENT5,                       
                                      SEGMENT6,                      
                                      SEGMENT7,                       
                                      SEGMENT8,                       
                                      SEGMENT9,                       
                                      SEGMENT10,                      
                                      SEGMENT11,                      
                                      SEGMENT12,                      
                                      SEGMENT13,                      
                                      SEGMENT14,                      
                                      SEGMENT15,                      
                                      ENTERED_DR,                     
                                      ENTERED_CR,                     
                                      ENTERED_EXCH_RATE,              
                                      ACCOUNTED_DR,                   
                                      ACCOUNTED_CR,                   
                                      ACCOUNTED_EXCH_RATE,            
                                      BATCH_NUMBER,                   
                                      MODEL_CTL_ID,                   
                                      TAX_REGISTRATION_ID,            
                                      ROUNDING_AMOUNT,                
                                      ENTERED_ROUNDING_AMOUNT,        
                                      TARGET_INSTANCE_ID,             
                                      SOURCE_ACTIVITY,                
                                      ATTRIBUTE1,                     
                                      ATTRIBUTE2,                     
                                      ATTRIBUTE3,                     
                                      ATTRIBUTE4,                     
                                      ATTRIBUTE5,                     
                                      ATTRIBUTE6,                     
                                      ATTRIBUTE7,                     
                                      ATTRIBUTE8,                     
                                      ATTRIBUTE9,                     
                                      ATTRIBUTE10,                    
                                      PROCESS_SUMMARY_ID,             
                                      ICS_FLAG,                       
                                      CREATION_DATE,                  
                                      CREATED_BY,                     
                                      LAST_UPDATE_DATE,               
                                      LAST_UPDATED_BY,                
                                      LAST_UPDATE_LOGIN
                                from  XXCP_fc_process_history fph
                                where forecast_id = vForecast_id
                                and   fph.cpa_type in ('T','RT','RF')
                                and   fph.cost_plus_set_id = nvl(cCost_Plus_Set_id,fph.Cost_Plus_Set_id)
                                and   fph.interface_id = any(select fta.interface_id
                                                              from XXCP_fc_transaction_attributes fta
                                                              where fta.source_assignment_id = SGRec.Source_Assignment_id); 

           vHistoryCnt := SQL%ROWCOUNT;

      
           -- Transaction Journals
           Insert into XXCP_transaction_journals  (VT_PROCESS_HISTORY_ID,        
                                            VT_REQUEST_ID,                
                                            VT_INSTANCE_ID,               
                                            VT_SOURCE_ASSIGNMENT_ID,      
                                            VT_SUMMARY_ID,                
                                            VT_ACCOUNTED_CURRENCY_CODE,   
                                            VT_TRANSACTION_DATE,          
                                            VT_SOURCE_CREATION_DATE,      
                                            STATUS,                       
                                            SET_OF_BOOKS_ID,              
                                            ACCOUNTING_DATE,              
                                            CURRENCY_CODE,                
                                            DATE_CREATED,                 
                                            CREATED_BY,                   
                                            ACTUAL_FLAG,                  
                                            USER_JE_CATEGORY_NAME,        
                                            USER_JE_SOURCE_NAME,          
                                            CURRENCY_CONVERSION_DATE,     
                                            ENCUMBRANCE_TYPE_ID,          
                                            BUDGET_VERSION_ID,            
                                            USER_CURRENCY_CONVERSION_TYPE,
                                            CURRENCY_CONVERSION_RATE,     
                                            SEGMENT1,                     
                                            SEGMENT2,                     
                                            SEGMENT3,                     
                                            SEGMENT4,                     
                                            SEGMENT5,                     
                                            SEGMENT6,                     
                                            SEGMENT7,                     
                                            SEGMENT8,                     
                                            SEGMENT9,                     
                                            SEGMENT10,                    
                                            SEGMENT11,                    
                                            SEGMENT12,                    
                                            SEGMENT13,                    
                                            SEGMENT14,                    
                                            SEGMENT15,                    
                                            SEGMENT16,                    
                                            SEGMENT17,                    
                                            SEGMENT18,                    
                                            SEGMENT19,                    
                                            SEGMENT20,                    
                                            SEGMENT21,                    
                                            SEGMENT22,                    
                                            SEGMENT23,                    
                                            SEGMENT24,                    
                                            SEGMENT25,                    
                                            SEGMENT26,                    
                                            SEGMENT27,                    
                                            SEGMENT28,                    
                                            SEGMENT29,                    
                                            SEGMENT30,                    
                                            ENTERED_DR,                   
                                            ENTERED_CR,                   
                                            ACCOUNTED_DR,                 
                                            ACCOUNTED_CR,                 
                                            TRANSACTION_DATE,             
                                            REFERENCE1,                   
                                            REFERENCE2,                   
                                            REFERENCE3,                   
                                            REFERENCE4,                   
                                            REFERENCE5,                   
                                            REFERENCE6,                   
                                            REFERENCE7,                   
                                            REFERENCE8,                   
                                            REFERENCE9,                   
                                            REFERENCE10,                  
                                            REFERENCE11,                  
                                            REFERENCE12,                  
                                            REFERENCE13,                  
                                            REFERENCE14,                  
                                            REFERENCE15,                  
                                            REFERENCE16,                  
                                            REFERENCE17,                  
                                            REFERENCE18,                  
                                            REFERENCE19,                  
                                            REFERENCE20,                  
                                            REFERENCE21,                  
                                            REFERENCE22,                  
                                            REFERENCE23,                  
                                            REFERENCE24,                  
                                            REFERENCE25,                  
                                            REFERENCE26,                  
                                            REFERENCE27,                  
                                            REFERENCE28,                  
                                            REFERENCE29,                  
                                            REFERENCE30,                  
                                            JE_BATCH_ID,                  
                                            PERIOD_NAME,                  
                                            JE_HEADER_ID,                 
                                            JE_LINE_NUM,                  
                                            CHART_OF_ACCOUNTS_ID,         
                                            FUNCTIONAL_CURRENCY_CODE,     
                                            CODE_COMBINATION_ID,          
                                            DATE_CREATED_IN_GL,           
                                            WARNING_CODE,                 
                                            STATUS_DESCRIPTION,           
                                            STAT_AMOUNT,                  
                                            GROUP_ID,                     
                                            REQUEST_ID,                   
                                            SUBLEDGER_DOC_SEQUENCE_ID,    
                                            SUBLEDGER_DOC_SEQUENCE_VALUE, 
                                            ATTRIBUTE1,                   
                                            ATTRIBUTE2,                   
                                            ATTRIBUTE3,                   
                                            ATTRIBUTE4,                   
                                            ATTRIBUTE5,                   
                                            ATTRIBUTE6,                   
                                            ATTRIBUTE7,                   
                                            ATTRIBUTE8,                   
                                            ATTRIBUTE9,                   
                                            ATTRIBUTE10,                  
                                            ATTRIBUTE11,                  
                                            ATTRIBUTE12,                  
                                            ATTRIBUTE13,                  
                                            ATTRIBUTE14,                  
                                            ATTRIBUTE15,                  
                                            ATTRIBUTE16,                  
                                            ATTRIBUTE17,                  
                                            ATTRIBUTE18,                  
                                            ATTRIBUTE19,                  
                                            ATTRIBUTE20,                  
                                            CONTEXT,                      
                                            CONTEXT2,                     
                                            INVOICE_DATE,                 
                                            TAX_CODE,                     
                                            INVOICE_IDENTIFIER,           
                                            INVOICE_AMOUNT,               
                                            CONTEXT3,                     
                                            USSGL_TRANSACTION_CODE,       
                                            DESCR_FLEX_ERROR_MESSAGE,     
                                            JGZZ_RECON_REF,               
                                            AVERAGE_JOURNAL_FLAG,         
                                            ORIGINATING_BAL_SEG_VALUE,    
                                            GL_SL_LINK_ID,                
                                            GL_SL_LINK_TABLE,             
                                            REFERENCE_DATE,               
                                            LEDGER_ID,
                                            VT_GROUPING_COLUMNS,    
                                            VT_GROUPING_COLUMN1,    
                                            VT_GROUPING_COLUMN2,    
                                            VT_GROUPING_COLUMN3,    
                                            VT_GROUPING_COLUMN4,    
                                            VT_GROUPING_COLUMN5,    
                                            VT_GROUPING_COLUMN6,    
                                            VT_GROUPING_COLUMN7,    
                                            VT_GROUPING_COLUMN8,    
                                            VT_GROUPING_COLUMN9,    
                                            VT_GROUPING_COLUMN10,
                                            VT_GROUPING_COLUMN11,
                                            VT_GROUPING_COLUMN12,
                                            VT_GROUPING_COLUMN13,
                                            VT_GROUPING_COLUMN14,
                                            VT_GROUPING_COLUMN15,
                                            VT_GROUPING_COLUMN16,
                                            VT_GROUPING_COLUMN17,
                                            VT_GROUPING_COLUMN18,
                                            VT_GROUPING_COLUMN19,
                                            VT_GROUPING_COLUMN20)
                                    select  VT_PROCESS_HISTORY_ID,        
                                            VT_REQUEST_ID,                
                                            VT_INSTANCE_ID,               
                                            VT_SOURCE_ASSIGNMENT_ID,      
                                            VT_SUMMARY_ID,                
                                            VT_ACCOUNTED_CURRENCY_CODE,   
                                            VT_TRANSACTION_DATE,          
                                            VT_SOURCE_CREATION_DATE,      
                                            STATUS,                       
                                            SET_OF_BOOKS_ID,              
                                            ACCOUNTING_DATE,              
                                            CURRENCY_CODE,                
                                            DATE_CREATED,                 
                                            CREATED_BY,                   
                                            ACTUAL_FLAG,                  
                                            USER_JE_CATEGORY_NAME,        
                                            USER_JE_SOURCE_NAME,          
                                            CURRENCY_CONVERSION_DATE,     
                                            ENCUMBRANCE_TYPE_ID,          
                                            BUDGET_VERSION_ID,            
                                            USER_CURRENCY_CONVERSION_TYPE,
                                            CURRENCY_CONVERSION_RATE,     
                                            SEGMENT1,                     
                                            SEGMENT2,                     
                                            SEGMENT3,                     
                                            SEGMENT4,                     
                                            SEGMENT5,                     
                                            SEGMENT6,                     
                                            SEGMENT7,                     
                                            SEGMENT8,                     
                                            SEGMENT9,                     
                                            SEGMENT10,                    
                                            SEGMENT11,                    
                                            SEGMENT12,                    
                                            SEGMENT13,                    
                                            SEGMENT14,                    
                                            SEGMENT15,                    
                                            SEGMENT16,                    
                                            SEGMENT17,                    
                                            SEGMENT18,                    
                                            SEGMENT19,                    
                                            SEGMENT20,                    
                                            SEGMENT21,                    
                                            SEGMENT22,                    
                                            SEGMENT23,                    
                                            SEGMENT24,                    
                                            SEGMENT25,                    
                                            SEGMENT26,                    
                                            SEGMENT27,                    
                                            SEGMENT28,                    
                                            SEGMENT29,                    
                                            SEGMENT30,                    
                                            ENTERED_DR,                   
                                            ENTERED_CR,                   
                                            ACCOUNTED_DR,                 
                                            ACCOUNTED_CR,                 
                                            TRANSACTION_DATE,             
                                            REFERENCE1,                   
                                            REFERENCE2,                   
                                            REFERENCE3,                   
                                            REFERENCE4,                   
                                            REFERENCE5,                   
                                            REFERENCE6,                   
                                            REFERENCE7,                   
                                            REFERENCE8,                   
                                            REFERENCE9,                   
                                            REFERENCE10,                  
                                            REFERENCE11,                  
                                            REFERENCE12,                  
                                            REFERENCE13,                  
                                            REFERENCE14,                  
                                            REFERENCE15,                  
                                            REFERENCE16,                  
                                            REFERENCE17,                  
                                            REFERENCE18,                  
                                            REFERENCE19,                  
                                            REFERENCE20,                  
                                            REFERENCE21,                  
                                            REFERENCE22,                  
                                            REFERENCE23,                  
                                            REFERENCE24,                  
                                            REFERENCE25,                  
                                            REFERENCE26,                  
                                            REFERENCE27,                  
                                            REFERENCE28,                  
                                            REFERENCE29,                  
                                            REFERENCE30,                  
                                            JE_BATCH_ID,                  
                                            PERIOD_NAME,                  
                                            JE_HEADER_ID,                 
                                            JE_LINE_NUM,                  
                                            CHART_OF_ACCOUNTS_ID,         
                                            FUNCTIONAL_CURRENCY_CODE,     
                                            CODE_COMBINATION_ID,          
                                            DATE_CREATED_IN_GL,           
                                            WARNING_CODE,                 
                                            STATUS_DESCRIPTION,           
                                            STAT_AMOUNT,                  
                                            GROUP_ID,                     
                                            REQUEST_ID,                   
                                            SUBLEDGER_DOC_SEQUENCE_ID,    
                                            SUBLEDGER_DOC_SEQUENCE_VALUE, 
                                            ATTRIBUTE1,                   
                                            ATTRIBUTE2,                   
                                            ATTRIBUTE3,                   
                                            ATTRIBUTE4,                   
                                            ATTRIBUTE5,                   
                                            ATTRIBUTE6,                   
                                            ATTRIBUTE7,                   
                                            ATTRIBUTE8,                   
                                            ATTRIBUTE9,                   
                                            ATTRIBUTE10,                  
                                            ATTRIBUTE11,                  
                                            ATTRIBUTE12,                  
                                            ATTRIBUTE13,                  
                                            ATTRIBUTE14,                  
                                            ATTRIBUTE15,                  
                                            ATTRIBUTE16,                  
                                            ATTRIBUTE17,                  
                                            ATTRIBUTE18,                  
                                            ATTRIBUTE19,                  
                                            ATTRIBUTE20,                  
                                            CONTEXT,                      
                                            CONTEXT2,                     
                                            INVOICE_DATE,                 
                                            TAX_CODE,                     
                                            INVOICE_IDENTIFIER,           
                                            INVOICE_AMOUNT,               
                                            CONTEXT3,                     
                                            USSGL_TRANSACTION_CODE,       
                                            DESCR_FLEX_ERROR_MESSAGE,     
                                            JGZZ_RECON_REF,               
                                            AVERAGE_JOURNAL_FLAG,         
                                            ORIGINATING_BAL_SEG_VALUE,    
                                            GL_SL_LINK_ID,                
                                            GL_SL_LINK_TABLE,             
                                            REFERENCE_DATE,               
                                            LEDGER_ID,
                                            VT_GROUPING_COLUMNS,    
                                            VT_GROUPING_COLUMN1,    
                                            VT_GROUPING_COLUMN2,    
                                            VT_GROUPING_COLUMN3,    
                                            VT_GROUPING_COLUMN4,    
                                            VT_GROUPING_COLUMN5,    
                                            VT_GROUPING_COLUMN6,    
                                            VT_GROUPING_COLUMN7,    
                                            VT_GROUPING_COLUMN8,    
                                            VT_GROUPING_COLUMN9,    
                                            VT_GROUPING_COLUMN10,
                                            VT_GROUPING_COLUMN11,
                                            VT_GROUPING_COLUMN12,
                                            VT_GROUPING_COLUMN13,
                                            VT_GROUPING_COLUMN14,
                                            VT_GROUPING_COLUMN15,
                                            VT_GROUPING_COLUMN16,
                                            VT_GROUPING_COLUMN17,
                                            VT_GROUPING_COLUMN18,
                                            VT_GROUPING_COLUMN19,
                                            VT_GROUPING_COLUMN20
                                      from  XXCP_fc_transaction_journals j
                                      where vt_forecast_id = vForecast_id
                                      and   j.vt_source_assignment_id = SGRec.Source_Assignment_id
                                      and   j.cost_plus_set_id = nvl(cCost_Plus_Set_id,j.Cost_Plus_Set_id)
                                      and   j.cpa_type in ('T','RT','RF');                  

              vJournalCnt := SQL%ROWCOUNT;

           Insert into XXCP_true_up_history
                   ( PROCESS_HISTORY_ID,
                     SOURCE_ASSIGNMENT_ID,
                     PERIOD_SET_NAME_ID,
                     PERIOD_ID,
                     TRANSACTION_ID,
                     TARGET_ASSIGNMENT_ID,
                     TRADING_SET_ID,
                     INTERFACE_ID,
                     ACCOUNTING_DATE,
                     POSTING_PERIOD_ID,
                     COMPANY   ,
                     DEPARTMENT,
                     ACCOUNT   ,
                     UPLIFT_RATE       ,
                     GROWTH_RATE       ,
                     AVG_PERIOD_COST   ,
                     TOTAL_COST       ,
                     TRUE_UP_AMOUNT    ,
                     COST_CATEGORY_ID  ,
                     DATA_SOURCE       ,
                     CURRENCY_CODE     ,
                     CONSIDERED_PERIODS,
                     ACTIVE            ,          -- 02.04.02
                     OWNER_TAX_REG_ID  ,
                     PARTNER_TAX_REG_ID,
                     PAYER1            ,         
                     PAYER1_PERCENT    ,
                     PAYER2            ,
                     PAYER2_PERCENT    ,
                     INTER_PAYER       ,
                     ACCOUNT_TYPE      ,
                     REVERSAL_PROCESS_HISTORY_ID,
                     COST_PLUS_SET_ID  ,
                     TAX_AGREEMENT_NUMBER,
                     CREATION_DATE     ,
                     CREATED_BY        ,
                     LAST_UPDATE_LOGIN
                   )
               Select
                     PROCESS_HISTORY_ID,
                     SOURCE_ASSIGNMENT_ID,
                     PERIOD_SET_NAME_ID,
                     PERIOD_ID,
                     TRANSACTION_ID,
                     TARGET_ASSIGNMENT_ID,
                     TRADING_SET_ID,
                     INTERFACE_ID,
                     ACCOUNTING_DATE,
                     POSTING_PERIOD_ID,
                     COMPANY   ,
                     DEPARTMENT,
                     ACCOUNT   ,
                     UPLIFT_RATE       ,
                     GROWTH_RATE       ,
                     AVG_PERIOD_COST   ,
                     TOTAL_COST        ,
                     TRUE_UP_AMOUNT    ,
                     COST_CATEGORY_ID  ,
                     DATA_SOURCE       ,
                     CURRENCY_CODE     ,
                     CONSIDERED_PERIODS,
                     'Y'               ,       -- 02.04.02
                     OWNER_TAX_REG_ID  ,
                     PARTNER_TAX_REG_ID,
                     PAYER1            ,         
                     PAYER1_PERCENT    ,
                     PAYER2            ,
                     PAYER2_PERCENT    ,
                     INTER_PAYER       ,
                     ACCOUNT_TYPE      ,
                     REVERSAL_PROCESS_HISTORY_ID,
                     COST_PLUS_SET_ID  ,
                     TAX_AGREEMENT_NUMBER,
                     CREATION_DATE     ,
                     CREATED_BY        ,
                     LAST_UPDATE_LOGIN
              from   XXCP_fc_forecast_history x
              where  forecast_id = vForecast_id
              and    x.cpa_type in ('T','RT')
              and    x.cost_plus_set_id = nvl(cCost_Plus_Set_id,x.Cost_Plus_Set_id)
              and    x.INTERFACE_ID = any(select fta.INTERFACE_ID
                                          from XXCP_fc_transaction_attributes fta
                                          where fta.source_assignment_id = SGRec.Source_Assignment_id);

           vTrueUpCnt := SQL%ROWCOUNT;

           Insert into XXCP_forecast_history
                   ( PROCESS_HISTORY_ID,
                     SOURCE_ASSIGNMENT_ID,
                     PERIOD_SET_NAME_ID,
                     PERIOD_ID,
                     TRANSACTION_ID,
                     TARGET_ASSIGNMENT_ID,
                     TRADING_SET_ID,
                     INTERFACE_ID,
                     ACCOUNTING_DATE,
                     POSTING_PERIOD_ID,
                     COMPANY   ,
                     DEPARTMENT,
                     ACCOUNT   ,
                     UPLIFT_RATE       ,
                     GROWTH_RATE       ,
                     AVG_PERIOD_COST   ,
                     TOTAL_COST       ,
                     UPLIFTED_AMOUNT    ,
                     COST_CATEGORY_ID  ,
                     DATA_SOURCE       ,
                     CURRENCY_CODE     ,
                     CONSIDERED_PERIODS,
                     ACTIVE            ,          -- 02.04.02
                     OWNER_TAX_REG_ID  ,
                     PARTNER_TAX_REG_ID,
                     PAYER1            ,         
                     PAYER1_PERCENT    ,
                     PAYER2            ,
                     PAYER2_PERCENT    ,
                     INTER_PAYER       ,
                     ACCOUNT_TYPE      ,
                     REVERSAL_PROCESS_HISTORY_ID,
                     COST_PLUS_SET_ID  ,
                     TAX_AGREEMENT_NUMBER,
                     CREATION_DATE     ,
                     CREATED_BY        ,
                     LAST_UPDATE_LOGIN
                   )
               Select
                     PROCESS_HISTORY_ID,
                     SOURCE_ASSIGNMENT_ID,
                     PERIOD_SET_NAME_ID,
                     PERIOD_ID,
                     TRANSACTION_ID,
                     TARGET_ASSIGNMENT_ID,
                     TRADING_SET_ID,
                     INTERFACE_ID,
                     ACCOUNTING_DATE,
                     POSTING_PERIOD_ID,
                     COMPANY   ,
                     DEPARTMENT,
                     ACCOUNT   ,
                     UPLIFT_RATE       ,
                     GROWTH_RATE       ,
                     AVG_PERIOD_COST   ,
                     TOTAL_COST        ,
                     UPLIFTED_AMOUNT    ,
                     COST_CATEGORY_ID  ,
                     DATA_SOURCE       ,
                     CURRENCY_CODE     ,
                     CONSIDERED_PERIODS,
                     'Y'               ,       -- 02.04.02
                     OWNER_TAX_REG_ID  ,
                     PARTNER_TAX_REG_ID,
                     PAYER1            ,         
                     PAYER1_PERCENT    ,
                     PAYER2            ,
                     PAYER2_PERCENT    ,
                     INTER_PAYER       ,
                     ACCOUNT_TYPE      ,
                     REVERSAL_PROCESS_HISTORY_ID,
                     COST_PLUS_SET_ID  ,
                     TAX_AGREEMENT_NUMBER,
                     CREATION_DATE     ,
                     CREATED_BY        ,
                     LAST_UPDATE_LOGIN
              from   XXCP_fc_forecast_history x
              where  forecast_id = vForecast_id
              and    x.cpa_type = 'RF'
              and    x.cost_plus_set_id = nvl(cCost_Plus_Set_id,x.Cost_Plus_Set_id)
              and    x.INTERFACE_ID = any(select fta.INTERFACE_ID
                                          from XXCP_fc_transaction_attributes fta
                                          where fta.source_assignment_id = SGRec.Source_Assignment_id);

           vForecastCnt := SQL%ROWCOUNT;

           -- Inactivate History Tables if Trial_ind = Y
           update XXCP_forecast_history
           set    active = 'N',
                  trial_ind = null
           where  source_assignment_id = SGRec.Source_Assignment_id
           and    trial_ind = 'Y'
           and    cost_plus_set_id = nvl(cCost_Plus_Set_id,Cost_Plus_Set_id);

           update XXCP_true_up_history
           set    active = 'N',
                  trial_ind = null
           where  source_assignment_id = SGRec.Source_Assignment_id
           and    trial_ind = 'Y'
           and    cost_plus_set_id = nvl(cCost_Plus_Set_id,Cost_Plus_Set_id);

            -- Move Attributes
            Insert into XXCP_transaction_attributes(ATTRIBUTE_ID,                   
                                            HEADER_ID,                      
                                            SOURCE_ASSIGNMENT_ID,           
                                            PARENT_TRX_ID,                  
                                            TRANSACTION_ID,                 
                                            TRADING_SET_ID,                 
                                            SOURCE_TABLE_ID,                
                                            SOURCE_TYPE_ID,                 
                                            SOURCE_CLASS_ID,                
                                            TRANSACTION_DATE,               
                                            SET_OF_BOOKS_ID,                
                                            PARTNER_REQUIRED,               
                                            PARTNER_ASSOC_ID,               
                                            PARTNER_TAX_REG_ID,             
                                            PARTNER_LEGAL_EXCH_RATE,        
                                            PARTNER_LEGAL_CURR,             
                                            PARTNER_ASSIGN_RULE_ID,         
                                            EXCHANGE_DATE,                  
                                            TC_QUALIFIER1,                  
                                            TC_QUALIFIER2,                  
                                            MC_QUALIFIER1,                  
                                            MC_QUALIFIER2,                  
                                            MC_QUALIFIER3,                  
                                            MC_QUALIFIER4,                  
                                            ED_QUALIFIER1,                  
                                            ED_QUALIFIER2,                  
                                            ED_QUALIFIER3,                  
                                            ED_QUALIFIER4,                  
                                            FROZEN,                         
                                            INTERNAL_ERROR_CODE,            
                                            TRANSACTION_REF1,               
                                            TRANSACTION_REF2,               
                                            TRANSACTION_REF3,               
                                            OWNER_TAX_REG_ID,               
                                            ADJUSTMENT_RATE,                
                                            ADJUSTMENT_RATE_ID,             
                                            DOCUMENT_CURRENCY,              
                                            DOCUMENT_EXCH_RATE,             
                                            QUANTITY,                       
                                            UOM,                            
                                            IC_UNIT_PRICE,                  
                                            IC_PRICE,                       
                                            IC_TRADE_TAX_ID,                
                                            IC_TAX_RATE,                    
                                            IC_CURRENCY,                    
                                            IC_CONTROL,                     
                                            PRICE_METHOD_ID,                
                                            ATTRIBUTE1,                     
                                            ATTRIBUTE2,                     
                                            ATTRIBUTE3,                     
                                            ATTRIBUTE4,                     
                                            ATTRIBUTE5,                     
                                            ATTRIBUTE6,                     
                                            ATTRIBUTE7,                     
                                            ATTRIBUTE8,                     
                                            ATTRIBUTE9,                     
                                            ATTRIBUTE10,                    
                                            TAX_QUALIFIER1,                 
                                            TAX_QUALIFIER2,                 
                                            INTERFACE_ID,                   
                                            STEP_NUMBER,                    
                                            OLS_DATE,                       
                                            CREATED_BY,                     
                                            CREATION_DATE,                  
                                            LAST_UPDATED_BY,                
                                            LAST_UPDATE_DATE,               
                                            LAST_UPDATE_LOGIN)
                                    (select ATTRIBUTE_ID,                   
                                            HEADER_ID,                      
                                            SOURCE_ASSIGNMENT_ID,           
                                            PARENT_TRX_ID,                  
                                            TRANSACTION_ID,                 
                                            TRADING_SET_ID,                 
                                            SOURCE_TABLE_ID,                
                                            SOURCE_TYPE_ID,                 
                                            SOURCE_CLASS_ID,                
                                            TRANSACTION_DATE,               
                                            SET_OF_BOOKS_ID,                
                                            PARTNER_REQUIRED,               
                                            PARTNER_ASSOC_ID,               
                                            PARTNER_TAX_REG_ID,             
                                            PARTNER_LEGAL_EXCH_RATE,        
                                            PARTNER_LEGAL_CURR,             
                                            PARTNER_ASSIGN_RULE_ID,         
                                            EXCHANGE_DATE,                  
                                            TC_QUALIFIER1,                  
                                            TC_QUALIFIER2,                  
                                            MC_QUALIFIER1,                  
                                            MC_QUALIFIER2,                  
                                            MC_QUALIFIER3,                  
                                            MC_QUALIFIER4,                  
                                            ED_QUALIFIER1,                  
                                            ED_QUALIFIER2,                  
                                            ED_QUALIFIER3,                  
                                            ED_QUALIFIER4,                  
                                            FROZEN,                         
                                            INTERNAL_ERROR_CODE,            
                                            TRANSACTION_REF1,               
                                            TRANSACTION_REF2,               
                                            TRANSACTION_REF3,               
                                            OWNER_TAX_REG_ID,               
                                            ADJUSTMENT_RATE,                
                                            ADJUSTMENT_RATE_ID,             
                                            DOCUMENT_CURRENCY,              
                                            DOCUMENT_EXCH_RATE,             
                                            QUANTITY,                       
                                            UOM,                            
                                            IC_UNIT_PRICE,                  
                                            IC_PRICE,                       
                                            IC_TRADE_TAX_ID,                
                                            IC_TAX_RATE,                    
                                            IC_CURRENCY,                    
                                            IC_CONTROL,                     
                                            PRICE_METHOD_ID,                
                                            ATTRIBUTE1,                     
                                            ATTRIBUTE2,                     
                                            ATTRIBUTE3,                     
                                            ATTRIBUTE4,                     
                                            ATTRIBUTE5,                     
                                            ATTRIBUTE6,                     
                                            ATTRIBUTE7,                     
                                            ATTRIBUTE8,                     
                                            ATTRIBUTE9,                     
                                            ATTRIBUTE10,                    
                                            TAX_QUALIFIER1,                 
                                            TAX_QUALIFIER2,                 
                                            INTERFACE_ID,                   
                                            STEP_NUMBER,                    
                                            OLS_DATE,                       
                                            CREATED_BY,                     
                                            CREATION_DATE,                  
                                            LAST_UPDATED_BY,                
                                            LAST_UPDATE_DATE,               
                                            LAST_UPDATE_LOGIN
                                      from  XXCP_fc_transaction_attributes
                                      where forecast_id = vForecast_id
                                      and   source_assignment_id = SGRec.Source_Assignment_id
                                      and   cpa_type in ('T','RT','RF')
                                      and   cost_plus_set_id = nvl(cCost_Plus_Set_id,Cost_Plus_Set_id));
           vAttCnt := SQL%ROWCOUNT;

            -- Move Cache
            Insert into XXCP_transaction_cache (ATTRIBUTE_ID,
                                                SEQ,
                                                CACHED_QTY,
                                                CACHED_VALUE1,
                                                CACHED_VALUE2,
                                                CACHED_VALUE3,
                                                CACHED_VALUE4,
                                                CACHED_VALUE5,
                                                CACHED_VALUE6,
                                                CACHED_VALUE7,
                                                CACHED_VALUE8,
                                                CACHED_VALUE9,
                                                CACHED_VALUE10,
                                                CACHED_VALUE11,
                                                CACHED_VALUE12,
                                                CACHED_VALUE13,
                                                CACHED_VALUE14,
                                                CACHED_VALUE15,
                                                CACHED_VALUE16,
                                                CACHED_VALUE17,
                                                CACHED_VALUE18,
                                                CACHED_VALUE19,
                                                CACHED_VALUE20,
                                                CACHED_VALUE21,
                                                CACHED_VALUE22,
                                                CACHED_VALUE23,
                                                CACHED_VALUE24,
                                                CACHED_VALUE25,
                                                CACHED_VALUE26,
                                                CACHED_VALUE27,
                                                CACHED_VALUE28,
                                                CACHED_VALUE29,
                                                CACHED_VALUE30,
                                                CACHED_VALUE31,
                                                CACHED_VALUE32,
                                                CACHED_VALUE33,
                                                CACHED_VALUE34,
                                                CACHED_VALUE35,
                                                CACHED_VALUE36,
                                                CACHED_VALUE37,
                                                CACHED_VALUE38,
                                                CACHED_VALUE39,
                                                CACHED_VALUE40,
                                                CACHED_VALUE41,
                                                CACHED_VALUE42,
                                                CACHED_VALUE43,
                                                CACHED_VALUE44,
                                                CACHED_VALUE45,
                                                CACHED_VALUE46,
                                                CACHED_VALUE47,
                                                CACHED_VALUE48,
                                                CACHED_VALUE49,
                                                CACHED_VALUE50,
                                                TRANSACTION_DATE)
                                        (select ATTRIBUTE_ID,
                                                SEQ,
                                                CACHED_QTY,
                                                CACHED_VALUE1,
                                                CACHED_VALUE2,
                                                CACHED_VALUE3,
                                                CACHED_VALUE4,
                                                CACHED_VALUE5,
                                                CACHED_VALUE6,
                                                CACHED_VALUE7,
                                                CACHED_VALUE8,
                                                CACHED_VALUE9,
                                                CACHED_VALUE10,
                                                CACHED_VALUE11,
                                                CACHED_VALUE12,
                                                CACHED_VALUE13,
                                                CACHED_VALUE14,
                                                CACHED_VALUE15,
                                                CACHED_VALUE16,
                                                CACHED_VALUE17,
                                                CACHED_VALUE18,
                                                CACHED_VALUE19,
                                                CACHED_VALUE20,
                                                CACHED_VALUE21,
                                                CACHED_VALUE22,
                                                CACHED_VALUE23,
                                                CACHED_VALUE24,
                                                CACHED_VALUE25,
                                                CACHED_VALUE26,
                                                CACHED_VALUE27,
                                                CACHED_VALUE28,
                                                CACHED_VALUE29,
                                                CACHED_VALUE30,
                                                CACHED_VALUE31,
                                                CACHED_VALUE32,
                                                CACHED_VALUE33,
                                                CACHED_VALUE34,
                                                CACHED_VALUE35,
                                                CACHED_VALUE36,
                                                CACHED_VALUE37,
                                                CACHED_VALUE38,
                                                CACHED_VALUE39,
                                                CACHED_VALUE40,
                                                CACHED_VALUE41,
                                                CACHED_VALUE42,
                                                CACHED_VALUE43,
                                                CACHED_VALUE44,
                                                CACHED_VALUE45,
                                                CACHED_VALUE46,
                                                CACHED_VALUE47,
                                                CACHED_VALUE48,
                                                CACHED_VALUE49,
                                                CACHED_VALUE50,
                                                TRANSACTION_DATE
                                      from  XXCP_fc_transaction_cache
                                      where attribute_id in (select attribute_id
                                                             from   xxcp_fc_transaction_attributes
                                                             where source_assignment_id = SGRec.Source_Assignment_id
                                                             and   cpa_type in ('T','RT','RF')
                                                             and   cost_plus_set_id = nvl(cCost_Plus_Set_id,Cost_Plus_Set_id)));
           vCacheCnt := SQL%ROWCOUNT;
           
           -- update interface
           Update XXCP_cost_plus_interface g
             set vt_status = 'SUCCESSFUL'
           where vt_status               = 'TRIAL'
             and vt_Source_Assignment_id = SGRec.Source_Assignment_id
             and cost_plus_set_id        = nvl(cCost_Plus_Set_id,Cost_Plus_Set_id)
             and ((vt_transaction_table    = 'TRUE-UP'
                   and vt_transaction_type     in ('STD','REV','REP'))
                 or (vt_transaction_table    = 'FORECAST'
                     and vt_transaction_type  in ('REV','REP'))
                 );

     
           xxcp_foundation.show('True-Up ID :'||vForecast_ID||' Source Assignment Id '||to_char(SGRec.Source_Assignment_Id) );
           xxcp_foundation.show(vHdrCnt||' Transaction Header records committed.');
           xxcp_foundation.show(vAttCnt||' Transaction Attribute records committed.');
           xxcp_foundation.show(vHistoryCnt||' Process History records committed.');
           xxcp_foundation.show(vCacheCnt||' Transaction Cache records committed.');
           xxcp_foundation.show(vJournalCnt||' Transaction Journal records committed.');
           xxcp_foundation.show(vTrueUpCnt||' True-Up History records committed.');
           xxcp_foundation.show(vForecastCnt||' Forecast History records committed.');

        End if; 
     
      End Loop;

    End Loop;

    --
    -- Cleardown
    --
    for clear_rec in c2(cSource_Group_ID) loop
      Delete_FC_Tables(clear_rec.Source_Assignment_Id,cCost_Plus_Set_id);
    end loop;

    Commit;

    xxcp_foundation.show('True-Up Commit complete...');

    return(0);    
  
  Exception
    When Others Then
        xxcp_foundation.show('Error on True-Up Commit - '||SQLERRM);
        Rollback;
        return(-1);
  End Commit_True_Up;
 
 -- 
 -- Record_Checks
 --
 Function Record_Checks( cSource_Rowid      in rowid, 
                         cTransaction_Table in varchar2,
                         cTransaction_Class in varchar2,
                         cDuplicate_check   in varchar2,
                         cDuplicate_found   in varchar2
                         ) Return Number is

  Begin

      IF XXCP_TE_BASE.Ignore_Class(cTransaction_Table, cTransaction_Class) THEN
          UPDATE XXCP_cost_plus_interface r
             SET r.VT_STATUS = 'IGNORED',
                 r.VT_INTERNAL_ERROR_CODE = NULL,
                 r.VT_DATE_PROCESSED = sysdate
           WHERE r.ROWID = cSource_Rowid;

      -- If it is a duplicate then it will be marked as DUPLICATE and not processed.
      ELSIF cDuplicate_check = 'Y' and cDuplicate_found = 'Y' then
        UPDATE XXCP_cost_plus_interface r
             Set r.VT_STATUS = 'DUPLICATE',
                 r.VT_INTERNAL_ERROR_CODE = NULL,
                 r.VT_DATE_PROCESSED      = Sysdate
             Where r.ROWID = cSource_Rowid;
      -- First/Second Passthrough
      ELSIF xxcp_global.gPassThroughCode > 0 then
        Update XXCP_cost_plus_interface  r
              set r.vt_status = decode(xxcp_global.gPassThroughCode,7022,'SUCCESSFUL',
                                                                    7023,'SUCCESSFUL',
                                                                    7026,'PASSTHROUGH',
                                                                    7027,'PASSTHROUGH')
                 ,r.vt_internal_Error_code  = Null
                 ,r.vt_status_code          = xxcp_global.gPassThroughCode
                 ,r.vt_date_processed       = Sysdate
            where r.rowid                   = cSource_Rowid;

       END IF;
    Return(0);
  End Record_Checks;  
  
  
 
  --  !! ***********************************************************************************************************
  --  !!
  --  !!                                    CONTROL
  --  !!                  External Procedure (Entry Point) to start the True-Up Engine.
  --  !!
  --  !! ***********************************************************************************************************
  Function Control(cSource_Group_id    IN NUMBER,
                   cConc_Request_Id    IN NUMBER,
                   cTable_Group_id     IN NUMBER   DEFAULT 0, 
                   cCalendar_id        IN NUMBER   DEFAULT 0,
                   cPeriod_Name        IN VARCHAR2 DEFAULT Null,
                   cRestart            IN VARCHAR2 DEFAULT 'N',
                   cReplan_Reprocess   IN VARCHAR2 DEFAULT 'N',
                   cRequest_id         IN NUMBER   DEFAULT 0,
                   cPrev_Forecast_id   IN NUMBER   DEFAULT 0,
                   cCost_Plus_Set_id   IN NUMBER   DEFAULT NULL) return number is  -- cPeriod_name is the forecast period (accounting period)

    -- Assignment
    cursor cfg(pSource_id in number, pSource_Assignment_id in Number, pRequest_id in number) is
      select g.vt_transaction_table,
             g.vt_transaction_type,
             g.vt_transaction_class,
             g.vt_transaction_id,
             g.vt_parent_trx_id,
             g.vt_transaction_ref,
             s.set_of_books_id,
             g.rowid source_rowid,
             c.currency_code currency_code,                 -- ???
             nvl(c.period_net_dr,0) Entered_DR,                    -- ???
             nvl(c.period_net_cr,0) Entered_CR,
             s.source_assignment_id,
             s.set_of_books_id source_set_of_books_id,
             g.vt_interface_id,
             w.transaction_set_name,
             nvl(w.Transaction_Set_id,0) Transaction_Set_Id,
             g.vt_transaction_date,
             --
             y.source_table_id,
             y.source_type_id,
             cx.source_class_id,
             nvl(c.company,
                 substr(g.vt_transaction_ref,1,instr(g.vt_transaction_ref,'-')-1)) company,
             nvl(c.department,'~NULL~') department,
             nvl(c.account,'~NULL~') account,
             t.calc_legal_exch_rate,
             g.cost_plus_set_id,
             -- Explosion
             nvl(y.explosion_id,0)     ASG_Explosion_id,
             nvl(y.trx_explosion_id,0) TRX_EXPLOSION_ID,
             g.Tax_Agreement_Number
        from XXCP_source_assignments     s,
             XXCP_cost_plus_interface    g,
             XXCP_actual_costs           c,
             -- New
             XXCP_sys_source_tables       t,
             XXCP_sys_source_types        y,
             XXCP_sys_source_classes      cx,
             XXCP_source_transaction_sets w
       where g.vt_request_id           = pRequest_id
         and g.vt_status               = 'ASSIGNMENT'
         and g.vt_source_assignment_id = pSource_Assignment_ID
         and g.vt_source_assignment_id = s.source_assignment_id
         -- Actual Cost
         and g.vt_transaction_id       = c.transaction_id (+)
         and g.vt_source_assignment_id = c.source_assignment_id (+)
         -- Table
         and g.vt_transaction_table    = t.transaction_table
         and t.source_id               = pSource_id
         -- Type
         and g.vt_transaction_type     = y.type
				 and y.latch_only             = 'N'
         and t.source_table_id         = y.source_table_id
         -- Class
         and g.vt_transaction_class    = decode(t.class_mapping_req,'Y',cx. system_latch,cx.class)
				 and cx.latch_only             = 'N'
         and t.source_table_id         = cx.source_table_id
         -- Transaction_Set_id
         and y.Transaction_Set_id      = w.Transaction_Set_id
         --
      Order by vt_transaction_table, w.sequence,  g.vt_parent_trx_id, cx.sequence, vt_transaction_id;

    CFGRec CFG%Rowtype;

    -- Transactions
    cursor GLI(pSource_id in number, pSource_Assignment_id in Number, pRequest_id in number) is
      SELECT g.vt_transaction_table,
             g.vt_transaction_type,
             g.vt_transaction_class,
             g.vt_interface_id,
             g.vt_transaction_id,
						 g.vt_transaction_ref,
             s.instance_id vt_instance_id,
		         Null Code_Combination_id,
             -- Balancing segment
             'Cost-Plus'      be2, --  USER_JE_SOURCE_NAME
             0                be3, -- GROUP_ID
             'Cost-Plus'      be4, -- USER_JE_CATEGORY_NAME
             g.vt_transaction_Ref be6,
             Null             be7, -- Reference2
             Null             be8, -- Reference5
             -- Inventory
             0 Transaction_Cost,              
             -- History
             'Cost-Plus' Category_name,
             'Cost-Plus' Source_name,
             g.vt_transaction_date  accounting_date,
             --
             Null parent_entered_currency,    
             ac.currency_code currency_code,   
             g.vt_parent_trx_id,
             -- Fixed Values for the engine. The pricing engine should would out cost
             0 Group_id,
             0 Parent_Entered_Amount,  
             ac.period_net_dr Entered_DR,
             ac.period_net_cr Entered_CR,
             0    Accounted_DR,
             Null Accounted_CR,
						 -- End Fixed Values
             g.rowid source_rowid,
             s.set_of_books_id,
             Null Segment1,
             Null Segment2,
             Null Segment3,
             Null Segment4,
             Null Segment5,
             Null Segment6,
             Null Segment7,
             Null Segment8,
             Null Segment9,
             Null Segment10,
             Null Segment11,
             Null Segment12,
             Null Segment13,
             Null Segment14,
             Null Segment15,
             s.source_assignment_id,
             s.set_of_books_id source_set_of_books_id,
             w.transaction_set_name,
             w.Transaction_Set_id,
             g.creation_date  source_creation_date,
						 --
						 t.source_table_id,
						 y.source_type_id,
						 cx.source_class_id,
						 t.class_mapping_req,
             -- Cost-Plus
             g.Collection_Period_id,
             nvl(ac.company,
                 substr(g.vt_transaction_ref,1,instr(g.vt_transaction_ref,'-')-1)) company,
             nvl(ac.department,'~NULL~') department,
             nvl(ac.account,'~NULL~') account,
             ac.data_source,
             ac.collector_id,
             ac.period_set_name_id,
             g.vt_transaction_date,
             g.attribute1 cost_category_id,      -- Stamped onto record in Configuration stage
             g.attribute2 category_data_source,  -- Stamped onto record in Configuration stage
             g.attribute3 cost_category,          -- Stamped onto record in Configuration stage
             g.cost_plus_set_id,
             -- Explosion
             nvl(y.explosion_id,0)     ASG_Explosion_id,
             nvl(y.trx_explosion_id,0) TRX_EXPLOSION_ID,
             g.Tax_Agreement_Number
			  from XXCP_cost_plus_interface     g,
             XXCP_actual_costs            ac,
             XXCP_source_assignments      s,
             -- New
             XXCP_sys_source_tables       t,
             XXCP_sys_source_types        y,
             XXCP_sys_source_classes      cx,
             XXCP_source_transaction_sets w
       where g.vt_status               = 'TRANSACTION'
         and g.vt_source_assignment_id = pSource_Assignment_ID
         and g.vt_source_assignment_id = s.source_assignment_id
         and s.source_id               = pSource_id
         -- Actuals table link
         and g.vt_transaction_id       = ac.transaction_id (+)
         and g.vt_source_assignment_id = ac.source_assignment_id (+)
         -- Table
         and g.vt_transaction_table    = t.transaction_table
         and t.source_id               = pSource_id
         -- Type
         and g.vt_transaction_type     = y.type
         and t.source_table_id         = y.source_table_id
         -- Class
         and g.vt_transaction_class    = decode(t.class_mapping_req,'Y',cx. system_latch,cx.class)
         and y.source_table_id         = cx.source_table_id
         -- Transaction_Set_id
         and y.Transaction_Set_id      = w.Transaction_Set_id
         -- GL Code Combinations
         and g.vt_request_id           = pRequest_id
       order by vt_transaction_table, w.sequence, vt_parent_trx_id,cx.sequence,vt_transaction_id;

 
    GLIRec GLI%Rowtype;

    Cursor ConfErr(pSource_Assignment_id in number, pRequest_id in number) is
      select Distinct k.vt_parent_trx_id, k.vt_transaction_table
        from XXCP_cost_plus_interface k
       where k.vt_request_id           = pRequest_id
         and k.vt_source_assignment_id = pSource_Assignment_id
         and k.vt_status               = 'ERROR';

    -- Assignments
    Cursor sr1(pSource_Group_id in number) is
      select set_of_books_id,
             Source_Assignment_id,
             instance_id source_instance_id,
             Source_id,
						 stamp_parent_trx
        from XXCP_source_assignments g
       where g.Source_Group_id = pSource_Group_id
         and g.active          = 'Y'
    order by source_assignment_id;

    vInternalErrorCode         XXCP_errors.internal_error_code%type := 0;
    vExchange_Rate_Type        XXCP_tax_registrations.exchange_rate_type%type;
    vCommon_Exch_Curr          XXCP_tax_registrations.common_exch_curr%type;

    i                          pls_integer := 0;
    j                          Integer := 0;
    k                          Number;
	  vTiming_Start              number;
    vDML_Compiled              varchar2(1);
    vGlobal_Rounding_Tolerance Number := 0;
    vTransaction_Abort         Boolean := False;

    vGlobalPrecision           pls_integer := 2;
    CommitCnt                  pls_Integer := 0;
    vJob_Status                pls_integer := 0;
    vSource_Activity           XXCP_sys_sources.source_activity%type := 'GL';
    vStaged_Records            XXCP_sys_sources.staged_records%type;

    vTransaction_Class         XXCP_sys_source_classes.Class%type;
    vRequest_ID                XXCP_process_history.request_id%type;

    -- NEW PARAMETERS
    vCurrent_Parent_Trx_id     XXCP_transaction_attributes.parent_trx_id%type := 0;
    vTransaction_Type          XXCP_sys_source_types.Type%type;
    --
    -- Used for the REC Distribution
    vExtraLoop                 Boolean := False;
    vExtraClass                XXCP_sys_source_classes.class%type;
    --
    vSource_Assignment_id      XXCP_source_assignments.source_assignment_id%type;
    vStamp_Parent_Trx          XXCP_source_assignments.stamp_parent_trx%type := 'N';
    vSource_instance_id        XXCP_source_assignments.instance_id%type;
    vSource_ID                 XXCP_sys_sources.source_id%type;

    vClass_Mapping_Name        XXCP_sys_source_classes.Class%type;
    
    vStart_Date                date;
    vEnd_Date                  date;
    vSuccess                   boolean;
    vPeriod_id                 Number    := 0;          
    vCost_Category             varchar2(100);
    vTerritory                 varchar2(15);

    vError_Completion_Status   xxcp_source_assignment_groups.error_completion_status%TYPE := 'N';                                     

    Cursor SF(pSource_Group_id in number) is
      Select s.source_activity,
             s.Source_id,
             Preview_ctl,
             Timing,
             Custom_events,
             s.Cached,
			       s.DML_Compiled,
             Staged_Records,
             nvl(g.error_completion_status,'N') error_completion_status ,
             s.Duplicate_Check
        from XXCP_source_assignment_groups g, XXCP_sys_sources s
       where source_group_id = pSource_Group_id
         and s.source_id = g.source_id;

    Cursor Tolx is
      Select Numeric_Code Global_Rounding_Tolerance
        from XXCP_lookups
       where lookup_type = 'ROUNDING TOLERANCE'
         and lookup_code = 'GLOBAL';

	  Cursor CRL(pSource_id in number) is
     select Distinct Transaction_table, source_id
       from XXCP_column_rules
      where source_id = pSource_id;
      
    Cursor pdt(pPeriod_Set_Name_id in number, pPeriod_name in varchar2) is
      select w.Start_Date, w.End_Date, (w.period_year*100)+w.period_num period_id
      from xxcp_instance_gl_periods_v w
      where w.period_set_name_id = pPeriod_Set_Name_id
        and w.period_name        = pPeriod_name;    
        
    Cursor TER(pCompany_number in varchar2) is
      select territory
      from   XXCP_tax_registrations
      where  company_number = pCompany_number
      and    cost_plus      = 'Y';


    -- Dynamic Explosion 
    vExplosion_summary      NUMBER;
    vLast_source_type_id    xxcp_sys_source_types.source_type_id%TYPE := 0;
    vExplosion_sql          VARCHAR2(4000);
    vRowsfound              NUMBER;
    jx                      NUMBER;
    vExplosion_trx_id       NUMBER;
    vExplosion_source_rowid VARCHAR2(32);
    vExplosion_trx_type_id  NUMBER(15);
    vExplosion_trx_class_id NUMBER(15);      

    -- 02.05.02
    vDuplicate_check           xxcp_sys_sources.duplicate_check%type := 'N';
    vDuplicate_found           varchar2(1);
    
  BEGIN

    xxcp_global.Trace_on   := nvl(gEngine_Trace,'N');
    xxcp_global.Forecast_on := 'N';         -- Set Forecast Off
    xxcp_global.True_Up_on  := 'Y';         -- Set True Up On
    xxcp_global.Replan_on   := 'N';
    xxcp_global.Preview_on  := 'N';
    xxcp_global.gReplan_Reprocess       := cReplan_Reprocess;
    xxcp_global.SystemDate := Sysdate;
		vTimingCnt             := 1;
    gEngine_Error_Found    := FALSE;
    gForce_Job_Error       := FALSE;
    gForce_Job_Warning     := FALSE;



    For SFRec in SF(cSource_Group_id) loop
      xxcp_global.gCommon(1).Source_id     := SFRec.Source_id;
      xxcp_global.gCommon(1).Preview_ctl   := SFRec.Preview_ctl;
      xxcp_global.gCommon(1).Preview_on    := xxcp_global.Preview_on;
      xxcp_global.gCommon(1).Custom_events := SFRec.Custom_events;
      xxcp_global.gCommon(1).current_Source_activity := SFRec.Source_Activity;
      xxcp_global.gCommon(1).Cached_Attributes := SFRec.Cached;

      vSource_Activity := SFRec.Source_Activity;
      vSource_id       := SFRec.Source_id;
      vDML_Compiled    := SFRec.DML_Compiled;
      vStaged_Records  := SFRec.Staged_Records;
      --
      vError_Completion_Status := SFRec.Error_Completion_Status;
      -- 02.05.02
      vDuplicate_check    := SFRec.Duplicate_Check;

    End Loop;

    -- Get dates
    For Rec in pdt(cCalendar_id, cPeriod_Name ) loop -- Passing in Forecast Period.
       vStart_Date := rec.start_date;
       vEnd_Date   := rec.end_date;
       vPeriod_id  := rec.period_id;
    End loop;

    xxcp_global.set_source_id(cSource_id => vSource_id);

    --     SYS.DBMS_SUPPORT.START_TRACE( waits=>true, binds=>true );

    vTiming(vTimingCnt).CF_Records    := 0;
    vTiming(vTimingCnt).Flush_Records := 0;
    vTiming(vTimingCnt).Start_time    := to_char(sysdate, 'HH24:MI:SS');
    vTiming(vTimingCnt).Flush         := 0;
    vTiming_Start            := xxcp_reporting.Get_Seconds;

    If xxcp_global.gCommon(1).Custom_events = 'Y' then
      xxcp_custom_events.Set_system_mode(xxcp_global.gCommon);
    End If;

    --
    -- ## *******************************************************************************************************
    -- ##
    -- ##                            Check to See the process is already in use
    -- ##
    -- ## *******************************************************************************************************
    --
	  If vDML_Compiled = 'N' then
	   -- Prevent Processing and the column rules have changed and
	   -- they need compiling.
	   vJob_Status := 4;
	   xxcp_foundation.show('ERROR: Column Rules have changed, but not re-generated');
	   xxcp_foundation.FndWriteError(10999,'Column Rules have changed, but not re-generated');

	  End If;


    If (NOT xxcp_management.Locked_Process(vSource_Activity, cSource_Group_id) and vJob_Status = 0)
        or cReplan_Reprocess = 'Y' then
      
      If cReplan_Reprocess = 'N' then
        vRequest_id := xxcp_management.Activate_Lock_Process(vSource_Activity,cSource_Group_id,cConc_Request_Id);
        xxcp_foundation.show('Activity locked....' || to_char(vRequest_id));
      Else
        vRequest_id := cRequest_id;
      End if;

      xxcp_global.User_id  := fnd_global.user_id;
      xxcp_global.Login_id := fnd_global.login_id;

      xxcp_global.gCommon(1).Current_request_id := vRequest_id;

      If xxcp_global.gCommon(1).Custom_events = 'Y' then
        vInternalErrorCode := xxcp_custom_events.Before_Processing(vSource_id);
      End If;


      -- Check for Override of Internal Attributes      
      Check_CPA_Attribute_Override;

      --
      -- ## **************************************************************************************************
      -- ##
      -- ##                                Setup Ready for the Run
      -- ##
      -- ## **************************************************************************************************
      --
      
      
      For REC in sr1(cSource_Group_id) Loop

        --
        -- Get ID for run
        --
        select xxcp_forecast_seq.nextval
        into   xxcp_global.gCommon(1).forecast_id
        from   dual;

        -- Add Forecast_Id to Array for STD processing
        if cReplan_Reprocess = 'N' then
          t_forecast_id.extend;
          t_forecast_id(t_forecast_id.count) := xxcp_global.gCommon(1).forecast_id;
        end if;

        --
        -- Set Trial Type to True Up
        --
        xxcp_global.gCommon(1).cpa_type := 'T';
        
        Exit when vJob_Status <> 0;

        vSource_instance_id   := rec.source_instance_id;
        vSource_Assignment_id := rec.Source_Assignment_id;

        --
        -- Restart Process
        --
        If cRestart = 'Y' then
      
          Update XXCP_cost_plus_interface r
            set r.vt_status = 'NEW'
           where r.vt_request_id = any(
                                      select request_id
                                        from XXCP_activity_control
                                       where source_activity = 'CPA'
                                         and start_date > (sysdate-100)
                                         and vt_status = 'TRIAL'                                        
                                     )
           and r.vt_source_assignment_id = vSource_Assignment_id
           and r.vt_status               = 'TRIAL'
           and r.period_set_name_id      = cCalendar_id
           and r.vt_transaction_table    = 'TRUE-UP'
           and r.vt_transaction_type     = 'STD'
           and r.Cost_Plus_Set_id        = nvl(cCost_Plus_Set_id,r.Cost_Plus_Set_id);
          
          xxcp_foundation.show('delete TRUE-UP for '||to_char(vSource_Assignment_id)||'/'||cCost_Plus_Set_id);
          
          Delete_FC_Tables(vSource_Assignment_id,cCost_Plus_Set_id);
          
          Commit;
          
        End If;

        --
        -- Delete First Runs Headers and Attributes if Re-Process
        --
        --If cPrev_Forecast_id > 0 then
        If t_Forecast_id.count > 0 then
        
        
          Delete from XXCP_fc_transaction_header h 
          where  h.source_assignment_id = vSource_Assignment_id
          and    h.forecast_id in (select * from TABLE(CAST(t_forecast_id AS XXCP_DYNAMIC_ARRAY)))  -- = cPrev_Forecast_id
          and    h.cpa_type in ('T','RT','RF')
          and    h.header_id in (select a.header_id
                                 from   XXCP_fc_transaction_attributes a
                                 where  a.source_assignment_id = h.Source_Assignment_id
                                 and    a.forecast_id = h.forecast_id
                                 and    a.cpa_type in ('T','RT','RF')
                                 and    a.interface_id in (select i.vt_interface_id
                                                           from   XXCP_cost_plus_interface i
                                                           where  i.vt_source_assignment_id = a.source_assignment_id
                                                           and    i.vt_status = 'ERROR'
                                                           and    i.vt_internal_error_code = 7015));
        
          Delete from XXCP_fc_transaction_attributes a 
          where  a.source_assignment_id = vSource_Assignment_id
          and    a.forecast_id in (select * from TABLE(CAST(t_forecast_id AS XXCP_DYNAMIC_ARRAY)))  -- = cPrev_Forecast_id
          and    a.cpa_type in ('T','RT','RF')
          and    a.interface_id in (select i.vt_interface_id
                                    from   XXCP_cost_plus_interface i
                                    where  i.vt_source_assignment_id = a.source_assignment_id
                                    and    i.vt_status = 'ERROR'
                                    and    i.vt_internal_error_code = 7015);

        

        End if;

				If vTimingCnt = 1 then
				    xxcp_foundation.show('Initialization....');
            vTiming(vTimingCnt).Init_Start := to_char(sysdate, 'HH24:MI:SS');

            xxcp_te_base.Engine_Init(cSource_id         => vSource_id,
				                             cSource_Group_id   => cSource_Group_id,
																     cInternalErrorCode => vInternalErrorCode);
        End If;

				vTiming(vTimingCnt).Source_Assignment_id := vSource_Assignment_id;

        -- 02.06.02
        gTrans_Table_Array.delete;

        -- Setup Globals
        xxcp_global.gCommon(1).current_process_name  := 'XXCP_CPAENG';
        xxcp_global.gCommon(1).current_assignment_id := vSource_Assignment_ID;
        --
        -- *********************************************************************************************************
        --                            Populate Column Rules and Initialize Cursors
        -- *********************************************************************************************************
        --
        vTiming(vTimingCnt).Memory_Start := to_char(sysdate, 'HH24:MI:SS');

				For Rec in CRL(pSource_id => vSource_id) Loop
          -- Used for column Attributes and error messages
					--
					xxcp_memory_Pack.LoadColumnRules(cSource_id          => vSource_id,
					                                 cSource_Table       => 'XXCP_COST_PLUS_INTERFACE',
																					 cSource_Instance_id => vSource_instance_id,
																					 cTarget_Table       => Rec.Transaction_table,
																					 cRequest_id         => vRequest_id,
																					 cInternalErrorCode  => vInternalErrorCode);

          xxcp_te_base.Init_Cursors(cTarget_Table => Rec.Transaction_table);
				End Loop;


        --
        -- *******************************************************************************************************
        --     Reset Previosly Errored Transactions and Assign Parent Trx Id to Incomming transactions
        -- *******************************************************************************************************
        --
				vInternalErrorCode := 0;

        Reset_Errored_Transactions(cSource_assignment_id => vSource_Assignment_id,
				                           cRequest_id           => vRequest_id,
                                   cCost_Plus_Set_id     => cCost_Plus_Set_id);
 
        Set_Running_Status(        cSource_id            => vSource_id,
				                           cSource_assignment_id => vSource_Assignment_id,
													         cRequest_id           => vRequest_id,
                                   cPeriod_Set_Name_id   => cCalendar_id,
                                   cPosting_Period_id    => vPeriod_id,
                                   cCost_Plus_Set_id     => cCost_Plus_Set_id);

        Transaction_Group_Stamp(   cStamp_Parent_Trx     => vStamp_Parent_Trx,
                                   cSource_Assignment_id => vSource_Assignment_id,
																   cInternalErrorCode    => vInternalErrorCode);

				-- Set Assignment Status -- (Not used in Preview)
				If vInternalErrorCode = 0 then
				  Set_Assignment_Status(cSource_id            => vSource_id,
                                cSource_Assignment_id => vSource_assignment_id,
                                cRequest_id           => vRequest_id,
															  cInternalErrorCode    => vInternalErrorCode,
                                cCost_Plus_Set_id     => cCost_Plus_Set_id);

				End If;

				-- Data Staging
				If nvl(vInternalErrorCode,0) = 0 then
				  vInternalErrorCode :=
					  xxcp_custom_events.Data_Staging(cSource_id            => vSource_id,
						                                cSource_Assignment_id => vSource_Assignment_id,
																						cRequest_id           => vRequest_id,
																						cPreview_id           => Null);
        End If;

        vTiming(vTimingCnt).Init_End := to_char(sysdate, 'HH24:MI:SS');
        --
        -- GLOBAL ROUNDING TOLERANCE
        --
        vGlobal_Rounding_Tolerance := 0;
        For Rec in Tolx loop
          vGlobal_Rounding_Tolerance := Rec.Global_Rounding_Tolerance;
        End loop;

        If vInternalErrorCode = 0 then
          --
          -- Get Standard Parameters
          --
					vInternalErrorCode :=
					  xxcp_foundation.fnd_gl_lookup(cExchange_Rate_Type => vExchange_Rate_Type,
						                              cGlobalPrecision    => vGlobalPrecision,
																					cCommon_Exch_Curr   => vCommon_Exch_Curr);
        End If;

        Commit;

        If vInternalErrorCode = 0 then 

          --
          -- ## ***********************************************************************************************************
          -- ##
          -- ##                                Assignment 
          -- ##
          -- ## ***********************************************************************************************************
          --
          i := 0;

          vTiming(vTimingCnt).CF_last       := xxcp_reporting.Get_Seconds;
          vTiming(vTimingCnt).CF            := 0;
          vTiming(vTimingCnt).CF_Records    := 0;
          vTiming(vTimingCnt).CF_Start_Date := Sysdate;
          xxcp_global.SystemDate := sysdate;
          xxcp_global.gPassThroughCode      := 0;
          
          Open CFG(pSource_id             => vSource_id,
					         pSource_Assignment_id  => vSource_Assignment_id,
									 pRequest_id            => vRequest_id);
					Loop
					      Fetch CFG into CFGRec;
								Exit when CFG%Notfound;


                i := i + 1;

                vTiming(vTimingCnt).CF_RECORDS := vTiming(vTimingCnt).CF_RECORDS + 1;

                XXCP_GLOBAL.GCOMMON(1).CURRENT_SOURCE_ROWID      := CFGREC.SOURCE_ROWID;
                XXCP_GLOBAL.GCOMMON(1).CURRENT_TRANSACTION_TABLE := CFGREC.VT_TRANSACTION_TABLE;
                XXCP_GLOBAL.GCOMMON(1).CURRENT_TRANSACTION_ID    := CFGREC.VT_TRANSACTION_ID;
                XXCP_GLOBAL.GCOMMON(1).CURRENT_PARENT_TRX_ID     := CFGREC.VT_PARENT_TRX_ID;
                XXCP_GLOBAL.GCOMMON(1).CURRENT_INTERFACE_ID      := CFGREC.VT_INTERFACE_ID;
								XXCP_GLOBAL.GCOMMON(1).CURRENT_TRANSACTION_DATE  := CFGREC.VT_TRANSACTION_DATE;
								XXCP_GLOBAL.GCOMMON(1).CURRENT_INTERFACE_REF     := CFGREC.VT_TRANSACTION_REF;
                xxcp_global.gCommon(1).calc_legal_exch_rate      := CFGRec.calc_legal_exch_rate;

                XXCP_GLOBAL.GCOMMON(1).PREVIEW_PARENT_TRX_ID     := NULL;
                XXCP_GLOBAL.GCOMMON(1).PREVIEW_SOURCE_ROWID      := NULL;
                xxcp_global.gCommon(1).explosion_id              := CFGRec.ASG_Explosion_Id;

                vTRANSACTION_CLASS := CFGREC.VT_TRANSACTION_CLASS;
                
                -- 02.05.02
                -- If the duplicate check is enabled for the source
                IF vDuplicate_check = 'Y' then
                
                  vDuplicate_found := 'N';
                                    
                  -- this cursor checks to see if the record already exists. 
                  for x in (select 'Y'
                            from XXCP_cost_plus_interface 
                            where vt_source_assignment_id =  vSource_assignment_id
                            and   vt_transaction_table    =  CFGRec.VT_Transaction_Table
                            and   vt_transaction_id       =  CFGRec.VT_Transaction_ID
                            and   rowid                  !=  CFGRec.Source_Rowid 
                            and   vt_status              != 'DUPLICATE') loop
                     
                    vDuplicate_found := 'Y';                             
                         
                   end loop;            
                end if;                   

                IF XXCP_TE_BASE.IGNORE_CLASS(CFGREC.VT_TRANSACTION_TABLE, VTRANSACTION_CLASS) THEN
                  BEGIN
                    UPDATE XXCP_COST_PLUS_INTERFACE
                       SET VT_STATUS              = 'IGNORED',
                           VT_INTERNAL_ERROR_CODE = NULL,
                           VT_DATE_PROCESSED      = XXCP_GLOBAL.SYSTEMDATE
                     WHERE ROWID = CFGREC.SOURCE_ROWID;

                     EXCEPTION WHEN OTHERS THEN NULL;
                  END;
                ELSE

                --
                -- Get Cost Category
                -- 
                vInternalErrorCode := xxcp_te_base.Get_Cost_Category(CFGRec.company,
                                                        CFGRec.department,
                                                        CFGRec.account,
                                                        CFGRec.vt_transaction_date,
                                                        xxcp_wks.gActual_Costs_Rec(1).tu_Cost_Category_id,
                                                        xxcp_wks.gActual_Costs_Rec(1).tu_Category_Data_Source,
                                                        vCost_Category,
                                                        CFGRec.cost_plus_set_id);
                --
                -- Territory
                --
                open  TER(CFGRec.company);
                fetch TER into vTerritory;
                close TER;
                 
                --
                -- Get Payee Control Info
                --  
                xxcp_wks.Reset_Internal_Array;  -- Clear Internal Array

                vInternalErrorCode := xxcp_te_base.Get_Payer_Details (
                                                         cPayee            => CFGRec.company, 
                                                         cTerritory        => vTerritory,
                                                         cCost_Category_id => xxcp_wks.gActual_Costs_Rec(1).tu_Cost_Category_id, --vCost_Category,
                                                         cData_Source      => xxcp_wks.gActual_Costs_Rec(1).tu_Category_Data_Source,
                                                         cTransaction_Date => CFGRec.vt_transaction_date,
                                                         cPayer1           => xxcp_wks.I1009_Array(123),
                                                         cPayer1_Percent   => xxcp_wks.I1009_Array(124),
                                                         cPayer2           => xxcp_wks.I1009_Array(125),
                                                         cPayer2_Percent   => xxcp_wks.I1009_Array(126),
                                                         cInter_Payer      => xxcp_wks.I1009_Array(127),
                                                         cAccount_Type     => xxcp_wks.I1009_Array(128));

                  xxcp_wks.gActual_Costs_Rec(1).cost_plus_set_id := CFGRec.Cost_Plus_Set_Id;

                  -- Tax Agreement Number Internal Attribute
                  xxcp_wks.I1009_Array(171) := CFGrec.Tax_Agreement_Number;

                  -- Hold Internals for Use in configurator
                  xxcp_wks.I1009_ArrayGLOBAL := xxcp_wks.Clear_Internal;
                  xxcp_wks.I1009_ArrayGLOBAL := xxcp_wks.I1009_Array;
                 
                  -- Branch for Dynamic Explosions
                  IF cfgrec.Asg_Explosion_id > 0 THEN
                   -- Explosion Processing
                   IF cfgrec.Source_Type_id != vLast_Source_Type_id THEN
                      vInternalErrorCode := xxcp_dynamic_sql.ReadExplosionDef(cExplosion_id    => cfgrec.Asg_Explosion_id,
                                                                              cExplosion_sql   => vExplosion_Sql,
                                                                              cSummary_columns => vExplosion_Summary);

                      vLast_Source_Type_id := cfgrec.Source_Type_id;
                   END IF;

                   If vInternalErrorCode = 0 then
                     -- Call Explosion
                     vInternalErrorCode := xxcp_dynamic_sql.ExplosionInitProcess(
                                                           cSource_type_id    => cfgrec.Source_type_id,
                                                           cSource_rowid      => cfgrec.Source_rowid,
                                                           cInterface_id      => cfgrec.vt_interface_id,
                                                           cExplosion_sql     => vExplosion_Sql,
                                                           cExplosion_summary => vExplosion_summary,
                                                           cColumncount       => 2,
                                                           cRowsfound         => vRowsfound,
                                                           cTransaction_type  => cfgrec.vt_transaction_type,
                                                           cTransaction_class => cfgrec.vt_transaction_class);
                   End If;

                   IF vRowsFound > 0 AND vInternalErrorCode = 0 THEN

                        FOR jx IN 1 .. vRowsFound LOOP

                           i := i + 1;

                           vExplosion_Source_rowid := xxcp_dynamic_sql.gDataColumn1(jx);
                           vExplosion_Trx_id       := xxcp_dynamic_sql.gDataColumn2(jx);
                           vTransaction_Type       := xxcp_dynamic_sql.gDataColumn3(jx);
                           vTransaction_Class      := xxcp_dynamic_sql.gDataColumn4(jx);
                           vExplosion_Trx_type_id  := xxcp_dynamic_sql.gDataColumnTypeId(jx);
                           vExplosion_Trx_class_id := xxcp_dynamic_sql.gDataColumnClassId(jx);
                           
                           xxcp_global.gCommon(1).Explosion_Rowid          := vExplosion_Source_Rowid;
                           xxcp_global.gCommon(1).Explosion_Transaction_id := vExplosion_Trx_id;
                           xxcp_global.gCommon(1).Explosion_RowsFound      := vRowsFound;

                           -- Record Checks must work with the orginial interface values
                           If Record_Checks(cSource_Rowid      => CFGRec.SOURCE_ROWID, 
                                            cTransaction_Table => CFGRec.VT_Transaction_Table,
                                            cTransaction_Class => CFGRec.VT_Transaction_Class,
                                            cDuplicate_check   => vDuplicate_Check,
                                            cDuplicate_found   => vDuplicate_Found) = 0 then
                        
                                  XXCP_TE_CONFIGURATOR.Control(
                                               cSource_Assignment_ID     => vSource_Assignment_id,
                                               cSet_of_books_id          => CFGRec.Source_Set_of_books_id,
                                               cTransaction_Date         => CFGRec.VT_Transaction_Date,
                                               cSource_id                => vSource_id,
                                               cSource_Table_id          => CFGRec.Source_table_id,
                                               cSource_Type_id           => vExplosion_Trx_Type_id,
                                               cSource_Class_id          => vExplosion_Trx_Class_id,
                                               cTransaction_id           => vExplosion_Trx_id,
                                               cSourceRowid              => CFGRec.Source_Rowid,
                                               cParent_Trx_id            => CFGRec.VT_Parent_Trx_id,
                                               cTransaction_Set_id       => CFGRec.Transaction_Set_id,
                                               cSpecial_Array            => 'N', 
                                               cKey                      => NULL, 
                                               cTransaction_Table        => CFGRec.VT_Transaction_Table,
                                               cTransaction_Type         => CFGRec.VT_Transaction_Type,
                                               cTransaction_Class        => vTransaction_Class,
                                               cInternalErrorCode        => vInternalErrorCode);
                             End If;     
                                                                       
                           EXIT WHEN vInternalErrorCode > 0;
                           
                        END LOOP;
                     
                     ELSIF vInternalErrorCode = 0 THEN
                        vInternalErrorCode := 3441;
                     END IF;
                  
             Else -- Normal Processing  
                IF XXCP_TE_BASE.IGNORE_CLASS(CFGREC.VT_TRANSACTION_TABLE, vTransaction_Class) THEN
                  BEGIN
                    UPDATE XXCP_GL_INTERFACE
                       SET VT_STATUS              = 'IGNORED',
                           VT_INTERNAL_ERROR_CODE = NULL,
                           VT_DATE_PROCESSED      = XXCP_GLOBAL.SYSTEMDATE
                     WHERE ROWID = CFGREC.SOURCE_ROWID;

                     EXCEPTION WHEN OTHERS THEN NULL;
                  END;

                -- 02.05.02
                -- If it is a duplicate then it will be marked as DUPLICATE and not processed.
                ELSIF vDuplicate_check = 'Y' and vDuplicate_found = 'Y' then 
                
                     UPDATE XXCP_GL_INTERFACE
                       SET VT_STATUS              = 'DUPLICATE',
                           VT_INTERNAL_ERROR_CODE = NULL,
                           VT_DATE_PROCESSED      = XXCP_GLOBAL.SYSTEMDATE
                     WHERE ROWID = CFGREC.SOURCE_ROWID;
                     
                -- First/Second Passthrough
                ELSIF xxcp_global.gPassThroughCode > 0 then
              
                     Update xxcp_gl_interface p
                        set p.vt_status               = decode(xxcp_global.gPassThroughCode,7022,'SUCCESSFUL',
                                                                                            7023,'SUCCESSFUL',
                                                                                            7026,'PASSTHROUGH',
                                                                                            7027,'PASSTHROUGH')
                           ,p.vt_internal_Error_code  = Null
                           ,p.vt_status_code          = xxcp_global.gPassThroughCode
                           ,p.vt_date_processed       = xxcp_global.SystemDate
                      where p.rowid                   = CFGREC.SOURCE_ROWID;
     
                  ELSE

                      XXCP_TE_CONFIGURATOR.Control(
                                               cSource_Assignment_ID     => vSource_assignment_id,
                                               cSet_of_books_id          => CFGRec.Source_Set_of_books_id,
                                               cTransaction_Date         => CFGRec.VT_Transaction_Date,
                                               cSource_id                => vSource_id,
                                               cSource_Table_id          => CFGRec.Source_table_id,
                                               cSource_Type_id           => CFGRec.Source_type_id,
                                               cSource_Class_id          => CFGRec.Source_Class_id,
                                               cTransaction_id           => CFGRec.VT_Transaction_ID,
                                               cSourceRowid              => CFGRec.Source_Rowid,
                                               cParent_Trx_id            => CFGRec.VT_Parent_Trx_id,
                                               cTransaction_Set_id       => CFGRec.Transaction_Set_id,
                                               cSpecial_Array            => 'N', 
                                               cKey                      => NULL, 
                                               cTransaction_Table        => CFGRec.VT_Transaction_Table,
                                               cTransaction_Type         => CFGRec.VT_Transaction_Type,
                                               cTransaction_Class        => vTransaction_Class,
                                               cInternalErrorCode        => vInternalErrorCode);
                 
                  End If; -- NEW              

             END IF; -- End Assignment
                
                
/*                  XXCP_TE_CONFIGURATOR.Control(
                                               cSource_Assignment_ID     => vSource_assignment_id,
                                               cSet_of_books_id          => CFGRec.Source_Set_of_books_id,
																							 cTransaction_Date         => CFGRec.VT_Transaction_Date,
																							 cSource_id                => vSource_id,
                                               cSource_Table_id          => CFGRec.Source_table_id,
                                               cSource_Type_id           => CFGRec.Source_type_id,
                                               cSource_Class_id          => CFGRec.Source_Class_id,
                                               cTransaction_id           => CFGRec.VT_Transaction_ID,
                                               cSourceRowid              => CFGRec.Source_Rowid,
                                               cParent_Trx_id            => CFGRec.VT_Parent_Trx_id,
                                               cTransaction_Set_id       => CFGRec.Transaction_Set_id,
                                               cSpecial_Array            => 'N',
                                               cKey                      => Null,
																							 cTransaction_Table        => CFGRec.VT_Transaction_Table,
		                                           cTransaction_Type         => CFGRec.VT_Transaction_Type,
                                               cTransaction_Class        => vTransaction_Class,
                                               cInternalErrorCode        => vInternalErrorCode);
*/

                  -- check status
                  -- check status
                  IF xxcp_global.Get_Release_Date IS NOT NULL THEN -- Release Date
                    Stop_Whole_Transaction; 
                  ElsIf vInternalerrorcode = -1 then
                    update XXCP_cost_plus_interface
                       set vt_status              = 'TRIAL',
                           vt_internal_error_code = null,
                           vt_date_processed      = xxcp_global.systemdate,
                           vt_status_code         = 7001
                     where rowid = cfgrec.source_rowid;
                  Elsif nvl(vInternalErrorCode, 0) <> 0 then
                    update XXCP_cost_plus_interface
                       set vt_status              = 'ERROR',
                           vt_internal_error_code = 12000,
                           vt_date_processed      = xxcp_global.systemdate
                     where rowid = cfgrec.source_rowid;

                     -- Flag Error
                     gEngine_Error_Found := TRUE;

                  ELSIF nvl(vInternalErrorCode, 0) = 0 THEN

                    update XXCP_cost_plus_interface
                       set vt_status              = 'TRANSACTION',
                           vt_internal_error_code = null,
                           vt_date_processed      = xxcp_global.systemdate,
                           attribute1             = xxcp_wks.gActual_Costs_Rec(1).tu_Cost_Category_id,
                           attribute2             = xxcp_wks.gActual_Costs_Rec(1).tu_Category_Data_Source,
                           attribute3             = vCost_Category
                     where rowid = cfgrec.source_rowid;

                  End if;

									-- emergency exit (out of disk space)
									if vinternalerrorcode between 3550 and 3559 then
									  rollback;
										vjob_status := 6;
										exit;
									end if;

                End if; -- end Assignment

                -- ##
                -- ## COMMIT ASSIGNMENT ROWS
                -- ##

                If i >= 2000 then
                  xxcp_global.systemdate    := sysdate;
                  commit;
                  i           := 0;

                  vjob_status :=
									  xxcp_management.Has_Oracle_Been_Terminated(
                                         cRequest_id      => vRequest_id,
                                         cSource_Activity => vSource_activity,
                                         cSource_Group_id => cSource_group_id,
                                         cConc_Request_Id => cConc_request_id);
                  exit when vjob_status > 0; -- controlled exit
                End if;

          END Loop;
          CLOSE CFG;

          xxcp_dynamic_sql.Close_Session;
          -- #
          -- # Clear Common vars
          -- #
          xxcp_global.gCommon(1).current_transaction_table := Null;
          xxcp_global.gCommon(1).current_transaction_id    := Null;
          xxcp_global.gCommon(1).current_parent_trx_id     := Null;
					xxcp_global.gcommon(1).current_interface_ref     := Null;

          Commit; -- Final Configuration Commit.

          --
          If xxcp_global.gCommon(1).Custom_events = 'Y' then
            xxcp_custom_events.After_assignment_processing(cSource_id            => vSource_id,
                                                           cSource_assignment_id => vSource_assignment_id);
          End If;

          vTiming(vTimingCnt).CF          := xxcp_reporting.Get_Seconds_Diff(cSec1 => vTiming(vTimingCnt).CF_last, cSec2 => xxcp_reporting.Get_Seconds);
          vTiming(vTimingCnt).CF_End_Date := Sysdate;

          -- ***********************************************************************************************************
          --
          --  Set associated Transactions to error if anyone of the Transactions is a set has errored in configuration
          --
          -- ***********************************************************************************************************
          --
          xxcp_foundation.show('Reporting Configuration errors....');
          xxcp_global.SystemDate := Sysdate;
          vInternalErrorCode := 0;
          For ConfErrRec in ConfErr(vSource_Assignment_id, vRequest_id) loop

            Begin
              update XXCP_cost_plus_interface
                 set vt_status              = 'ERROR',
                     vt_internal_Error_code = 12824, -- Associated errors
                     vt_date_processed      = xxcp_global.SystemDate
               where vt_request_id = vRequest_id
                 and vt_Source_Assignment_id = vSource_Assignment_id
                 and vt_status IN ('ASSIGNMENT', 'TRANSACTION')
                 and vt_parent_trx_id = ConfErrRec.vt_parent_trx_id
                 and vt_transaction_table = ConfErrRec.vt_Transaction_table;

               -- Flag Error
               If SQL%ROWCOUNT > 0 then
                 gEngine_Error_Found := TRUE;  
               End If;            

            Exception
              When OTHERS then
                vInternalErrorCode := 12819; -- Update Failed.

            End;
          End Loop;
           

          --## ***********************************************************************************************************
          --##
          --##                                    Transaction Engine
          --##
          --## ***********************************************************************************************************

          vTiming(vTimingCnt).TE_Last       := xxcp_reporting.Get_Seconds;
          vTiming(vTimingCnt).TE_Records    := 0;
          vTiming(vTimingCnt).TE_Start_Date := Sysdate;

          vExtraLoop             := False;
          vExtraClass            := Null;
          xxcp_global.SystemDate := Sysdate;
          i                := 0;

          If vInternalErrorCode = 0 and vJob_Status = 0 then

            xxcp_foundation.show('Transactions....');
            xxcp_wks.Reset_Source_Segments;
            gBalancing_Array      := xxcp_wks.Clear_Segments;
            ClearWorkingStorage;
 
						Open GLI(pSource_id            => vSource_id,
						         pSource_Assignment_id => vSource_assignment_id,
										 pRequest_id           => vRequest_id);
						Loop

							    Fetch GLI into GLIRec;
							    Exit when GLI%Notfound;

                  vInternalErrorCode := 0;
                  Exit when vJob_Status > 0;
                  vTiming(vTimingCnt).TE_Records := vTiming(vTimingCnt).TE_Records + 1;

                  xxcp_global.gCommon(1).explosion_id := GLIRec.ASG_Explosion_Id; 
                  --
                  -- ***********************************************************************************************************
                  --        Set the balancing Setments to be written to the Column Rules Array
                  -- ***********************************************************************************************************
                  --
                  -- Poplulate the Balancing Array
                  xxcp_wks.Source_Balancing(1) := Null;
                  xxcp_wks.Source_Balancing(2) := GLIRec.be2; -- User_Je_Source_Name
                  xxcp_wks.Source_Balancing(3) := GLIRec.be3; -- Group Id
                  xxcp_wks.Source_Balancing(4) := GLIRec.be4; -- User Je Category Name
                  xxcp_wks.Source_Balancing(5) := Null;
                  xxcp_wks.Source_Balancing(6) := GLIRec.be6; -- Reference 1
                  xxcp_wks.Source_Balancing(7) := GLIRec.be7; -- Reference 2
                  xxcp_wks.Source_Balancing(8) := GLIRec.be8; -- Reference 3
                  xxcp_wks.Source_Balancing(9) := Null;
                  xxcp_wks.Source_Balancing(10):= Null;
                  --
                  -- ***********************************************************************************************************
                  --         Move Accounting Codes from Master Record to Array
                  -- ***********************************************************************************************************
                  -- Populate Source Account Segments
                  xxcp_wks.Source_Segments(01) := GLIRec.Segment1;
                  xxcp_wks.Source_Segments(02) := GLIRec.Segment2;
                  xxcp_wks.Source_Segments(03) := GLIRec.Segment3;
                  xxcp_wks.Source_Segments(04) := GLIRec.Segment4;
                  xxcp_wks.Source_Segments(05) := GLIRec.Segment5;
                  xxcp_wks.Source_Segments(06) := GLIRec.Segment6;
                  xxcp_wks.Source_Segments(07) := GLIRec.Segment7;
                  xxcp_wks.Source_Segments(08) := GLIRec.Segment8;
                  xxcp_wks.Source_Segments(09) := GLIRec.Segment9;
                  xxcp_wks.Source_Segments(10) := GLIRec.Segment10;
                  xxcp_wks.Source_Segments(11) := GLIRec.Segment11;
                  xxcp_wks.Source_Segments(12) := GLIRec.Segment12;
                  xxcp_wks.Source_Segments(13) := GLIRec.Segment13;
                  xxcp_wks.Source_Segments(14) := GLIRec.Segment14;
                  xxcp_wks.Source_Segments(15) := GLIRec.Segment15;

                  xxcp_global.Set_New_Trx_Flag('N');
                  --
                  -- ***********************************************************************************************************
                  --         When a New Transaction is detected then flush the current buffer
                  -- ***********************************************************************************************************
                  --
                  If (vCurrent_Parent_Trx_id <> GLIRec.vt_parent_Trx_id) then

                    vTransaction_Abort := False;

                    --
                    -- !! ******************************************************************************************************
                    --                             FLUSH TRANSACTION
                    -- !! ******************************************************************************************************
                    --
                    If vCurrent_Parent_Trx_id <> 0 then
                      CommitCnt := CommitCnt + 1; -- Number of records process since last commit.

                      k := FlushRecordsToTarget(cPeriod_Set_Name_id        => cCalendar_id,
                                                cGlobal_Rounding_Tolerance => vGlobal_Rounding_Tolerance,
                                                cGlobalPrecision           => vGlobalPrecision,
                                                cTransaction_Type          => vTransaction_Type,
                                                cInternalErrorCode         => vInternalErrorCode);
                    End If;

                    xxcp_global.Set_New_Trx_Flag('Y');

									  --
                    -- ***********************************************************************************************************
                    --            Store Variables that are only set once per Parent Trx Id
                    -- ***********************************************************************************************************
                    --
                    xxcp_wks.Trace_Log     := Null;
                    xxcp_global.gCommon(1).current_transaction_table := GLIRec.vt_Transaction_Table;
                    xxcp_global.gCommon(1).current_parent_trx_id     := GLIRec.vt_Parent_Trx_id;
                    vCurrent_Parent_Trx_id := GLIRec.vt_Parent_Trx_id;
                    vTransaction_Type      := GLIRec.vt_Transaction_Type;
										ClearWorkingStorage;
                    --
                    -- ***********************************************************************************************************
                    --                         Extra Loops for different Classes
                    -- ***********************************************************************************************************
                    --
                    vExtraLoop  := False;
                    vExtraClass := Null;
                    For j in 1 .. xxcp_global.gSRE.Count loop

                      If GLIRec.vt_Transaction_Table = xxcp_global.gSRE(j).Transaction_table then
                        vExtraClass := xxcp_global.gSRE(j).Extra_Record_type;
                        vExtraLoop  := TRUE;
                        Exit;
                      End If;
                    End Loop;

                  Else
                    vExtraLoop  := FALSE; -- !!!!
                    vExtraClass := Null;
                  End If;

                  xxcp_global.gCommon(1).current_transaction_id := GLIRec.vt_Transaction_id;
                  --
                  -- ***********************************************************************************************************
                  --                                   Store Working Variables
                  -- ***********************************************************************************************************
                  --
                  xxcp_global.gCommon(1).current_source_rowid    := GLIRec.Source_Rowid;
                  xxcp_global.gCommon(1).current_Interface_id    := GLIRec.vt_interface_id;
                  xxcp_global.gCommon(1).current_Accounting_date := vEnd_Date;
                  xxcp_global.gCommon(1).current_Batch_number    := GLIRec.Group_id;
                  xxcp_global.gCommon(1).current_Category_name   := GLIRec.Category_Name;
                  xxcp_global.gCommon(1).current_Source_name     := GLIRec.Source_name;
								  xxcp_global.gcommon(1).current_interface_ref   := GLIRec.vt_transaction_ref;
                  xxcp_global.gcommon(1).current_creation_date   := GLIRec.source_creation_date;

                  --
                  -- **************************************************************************************************
                  -- Cost-Plus Values
                  -- **************************************************************************************************
                  -- 
                  xxcp_wks.gActual_Costs_Rec(1).Company               := GLIRec.Company;              
                  xxcp_wks.gActual_Costs_Rec(1).Department            := GLIRec.Department;              
                  xxcp_wks.gActual_Costs_Rec(1).Account               := GLIRec.Account;              
                  xxcp_wks.gActual_Costs_Rec(1).Collection_Period_id  := GLIRec.Collection_Period_id;              
                  xxcp_wks.gActual_Costs_Rec(1).Currency_Code         := GLIRec.Currency_Code;              
                  xxcp_wks.gActual_Costs_Rec(1).Transaction_id        := GLIRec.VT_Transaction_id;              
                  xxcp_wks.gActual_Costs_Rec(1).Collector_id          := GLIRec.Collector_id;
                  xxcp_wks.gActual_Costs_Rec(1).period_set_name_id    := GLIRec.period_set_name_id; 
                  xxcp_wks.gActual_Costs_Rec(1).data_source           := GLIRec.data_source;         -- 02.04.02
                  xxcp_wks.gActual_Costs_Rec(1).Period_id             := xxcp_translations.Period_id(cPeriod_Name => cPeriod_Name,
                                                                                                     cInstance_id => GLIRec.vt_instance_id);               
                  xxcp_wks.gActual_Costs_Rec(1).Replan                := 'N';
                  xxcp_wks.gActual_Costs_Rec(1).Instance_id           := GLIRec.vt_instance_id;

                  --
                  -- Get Cost Category (stamped on interface prior to config)
                  --
                  xxcp_wks.gActual_Costs_Rec(1).tu_Cost_Category_id     := GLIRec.Cost_Category_Id;
                  xxcp_wks.gActual_Costs_Rec(1).tu_Category_Data_Source := GLIRec.Category_Data_Source;
                  vCost_Category                                        := GLIRec.Cost_Category;
                  xxcp_wks.gActual_Costs_Rec(1).cost_plus_set_id        := GLIRec.Cost_Plus_Set_Id;

                  -- Tax Agreement Number Internal Attribute
                  xxcp_wks.I1009_Array(171) := GLIrec.Tax_Agreement_Number;
                  xxcp_wks.gActual_Costs_Rec(1).tax_agreement_number    := GLIRec.tax_agreement_number;

                  --
                  -- ***********************************************************************************************************
                  --                                   Call the Transaction Engine
                  -- ***********************************************************************************************************
                  --
                  If vTransaction_Abort = False then
                    vInternalErrorCode := 0; -- Reset for each row

                    i := i + 1; -- Count the number of rows processed.

                    xxcp_wks.SOURCE_CNT  := xxcp_wks.SOURCE_CNT + 1;
                    xxcp_wks.SOURCE_ROWID(xxcp_wks.source_cnt)   := GLIRec.Source_Rowid;
                    xxcp_wks.SOURCE_SUB_CODE(xxcp_wks.source_cnt):= 0;
                    xxcp_wks.SOURCE_STATUS(xxcp_wks.source_cnt)  := '?';
                    xxcp_global.gDefCostCatUsed                  := 'N';

                    -- *********************************************************************
                    -- Class Mapping
                    -- *********************************************************************
										If GLIRec.class_mapping_req = 'Y' then
  									    vClass_Mapping_Name := xxcp_te_base.class_mapping(
												                                   cSource_id              => vSource_id,
										                                       cClass_Mapping_Required => GLIRec.class_mapping_req,
																													 cTransaction_Table      => GLIRec.vt_transaction_table,
																													 cTransaction_Type       => GLIRec.vt_transaction_type,
																													 cTransaction_Class      => GLIRec.vt_transaction_class,
																													 cTransaction_id         => GLIRec.vt_transaction_id,
																													 cCode_Combination_id    => GLIRec.code_combination_id,
																													 cAmount                 => GLIRec.Transaction_Cost);
                    End If;
                    -- *********************************************************************
                    -- End Class Mapping
                    -- *********************************************************************


                    XXCP_TE_ENGINE.Control(cSource_assignment_id    => vSource_Assignment_id,
                                           cSource_Table_id         => GLIRec.Source_Table_id,
																					 cSource_Type_id          => GLIRec.Source_Type_id,
                                           cSource_Class_id         => GLIRec.Source_Class_id,
                                           cTransaction_id          => GLIRec.vt_Transaction_id,
                                           cParent_Trx_id           => GLIRec.vt_Parent_Trx_id,
                                           cSourceRowid             => GLIRec.Source_Rowid,
                                           cParent_Entered_Amount   => GLIRec.Parent_Entered_Amount,
                                           cParent_Entered_Currency => GLIRec.Parent_Entered_Currency,
                                           cClass_Mapping           => GLIRec.class_mapping_req,
                                           cClass_Mapping_Name      => vClass_Mapping_Name,
                                           --
                                           cInterface_ID            => GLIRec.VT_Interface_id,
                                           cExtraLoop               => vExtraLoop,
                                           cExtraClass              => vExtraClass,
                                           cTransaction_set_id      => GLIRec.Transaction_Set_id,
																					 --
																					 cTransaction_Table       => GLIRec.VT_Transaction_Table,
																					 cTransaction_Type        => GLIRec.VT_Transaction_Type,
                                           cTransaction_Class       => GLIRec.vt_Transaction_Class,
                                           cInternalErrorCode       => vInternalErrorCode);



                    xxcp_global.gCommon(1).current_trading_set := Null;

                    -- Report Errors
                    If vInternalErrorCode = 7015 then-- CPA Replan Required
                         Create_Replan(cInternalErrorCode => vInternalErrorCode,
                                       cCalendar_id       => cCalendar_id,
                                       cInstance_id       => GLIRec.VT_Instance_id);                    
                         xxcp_wks.gActual_Costs_Rec(1).Replan := 'N';
                    End if;

                    If vInternalErrorCode <> 0 then
                        Error_Whole_Transaction(cInternalErrorCode => vInternalErrorCode);
                        vTransaction_Abort := True;
                    End If;

                  End If;
                  --
                  -- ***********************************************************************************************************
                  --                     Test to See if Oracle or the User has terminated this process
                  --                                   And Commit after Each 1000 Records processed
                  -- ***********************************************************************************************************
                  --
                  IF CommitCnt >= 2000 THEN
                    CommitCnt := 0;
                    xxcp_global.SystemDate    := sysdate;
                    Commit;

                    vJob_Status :=
										 xxcp_management.Has_Oracle_been_Terminated(cRequest_id      => vRequest_id,
                                                                cSource_Activity => vSource_activity,
                                                                cSource_Group_id => cSource_group_id,
                                                                cConc_Request_Id => cConc_request_id);
                    Exit when vJob_Status > 0; -- Controlled Exit
                  END IF;

            END LOOP;

			      Close GLI;

            xxcp_te_base.Close_Cursors;

            --
            --  !!***********************************************************************************************************
            --                     Flush the Buffer for a Final Time
            -- !! ***********************************************************************************************************
            --
            IF vCurrent_Parent_Trx_id <> 0 and vInternalErrorCode = 0 then
              If vInternalErrorCode = 0 then

                k := FlushRecordsToTarget(cPeriod_Set_Name_id        => cCalendar_id, 
                                          cGlobal_Rounding_Tolerance => vGlobal_Rounding_Tolerance,
                                          cGlobalPrecision           => vGlobalPrecision,
                                          cTransaction_Type          => vTransaction_type,
                                          cInternalErrorCode         => vInternalErrorCode);
              End If;
            End If;
          End If; -- Internal Error Check GLI
        End If; -- End Internal Error Check

        vTiming(vTimingCnt).TE := xxcp_reporting.Get_Seconds_Diff(cSec1 => vTiming(vTimingCnt).TE_last, cSec2 => xxcp_reporting.Get_Seconds);

        xxcp_foundation.show('Clear down...');
        xxcp_global.SystemDate := Sysdate;
        -- *******************************************************************
        -- Error out records remaining with a Processing status after the run
        -- *******************************************************************

        Begin
          update XXCP_cost_plus_interface
             set vt_status              = 'ERROR',
                 vt_internal_error_code = Decode(vt_status,'TRANSACTION',12999,'ASSIGNMENT',12998, 'GROUPING',12996),
                 vt_date_processed      = xxcp_global.SystemDate
           where Rowid = any
           (select g.rowid Source_Rowid
                    from XXCP_cost_plus_interface g
                   where g.vt_request_id = xxcp_global.gCommon(1).current_request_id
                     and g.vt_source_assignment_id = vSource_Assignment_id
                     and g.vt_status in ('ASSIGNMENT', 'TRANSACTION', 'GROUPING'));

           -- Flag Error
           If SQL%ROWCOUNT > 0 then
             gEngine_Error_Found := TRUE;  
           End If;            

           Exception when OTHERS then Null;
        End;

        -- #
        -- # Clear Common vars
        -- #
        xxcp_global.gCommon(1).current_transaction_table := Null;
        xxcp_global.gCommon(1).current_transaction_id    := Null;
        xxcp_global.gCommon(1).current_parent_trx_id     := Null;
				xxcp_global.gcommon(1).current_interface_ref     := Null;

        --
        -- ***********************************************************************************************************
        --                     Clear Down
        -- ***********************************************************************************************************
        --
        ClearWorkingStorage;
        --
        -- ***********************************************************************************************************
        --                     DeActive the Lock in XXCP_ACTIVITY_CONTROL
        -- ***********************************************************************************************************
        --
        Commit;
        vTiming(vTimingCnt).TE_End_Date := Sysdate;
				vTimingCnt := vTimingCnt + 1;
      End loop; -- SR1

      -- Error Completion Status
      If gEngine_Error_Found and vJob_Status = 0 then
        If vError_Completion_Status  = 'W' then
          gForce_Job_Warning := TRUE;
        Elsif vError_Completion_Status  = 'E' then
          gForce_Job_Error   := TRUE;
        End if;
      End If;

		  IF vJob_Status = 0 and not gForce_Job_Error THEN
         vJob_Status := xxcp_data_distributor.Control(
			                          cSource_id        => vSource_id,
																cSource_Group_id  => cSource_Group_id,
																cRequest_id       => Null, -- Leave as NULL
																cJournal_Group_id => xxcp_global.gCommon(1).current_trx_group_id);
      END IF;

      if gReplan_Created = 'Y' and cReplan_Reprocess = 'N' then  
        --
        -- Call Replan Job
        --
        xxcp_foundation.show('Replan process Started...');

        vInternalErrorCode := replan_control(cSource_group_id  => csource_group_id,
                                             cConc_request_id  => cconc_request_id,
                                             cTable_group_id   => ctable_group_id,
                                             cCalendar_id      => ccalendar_id,
                                             cPeriod_name      => cperiod_name,
                                             cRequest_id       => vRequest_id);

        xxcp_foundation.show('Replan process Finished...');
      End if;

      If cReplan_Reprocess = 'N' then
        xxcp_management.DeActivate_lock_process(cRequest_id      => vRequest_id,
                                                cSource_Activity => vSource_Activity,
                                                cSource_Group_id => cSource_Group_id);
      End if;

      If xxcp_global.gCommon(1).Custom_events = 'Y' then
        xxcp_custom_events.After_Processing(cSource_id       => vSource_id,
                                            cSource_Group_id => cSource_Group_id);
      End If;

      xxcp_te_base.ClearDown;
      xxcp_memory_pack.ClearDown;
      xxcp_dynamic_sql.Close_Session;


      Commit;

    Else
      --
      -- Report If this process was already in use
      --
      xxcp_foundation.show('Engine already in use');
      vJob_Status := 1;
    End If;

    -- Force Warning/Error
    If gForce_Job_Warning then
      vJob_Status := 15;
    Elsif gForce_Job_Error then
      vJob_Status := 16;
    End if;


    xxcp_te_base.Write_Timings(vTiming, vTiming_Start);
    --
    -- ************************************************************************************************
    --                     Email Job Details to Users
    -- ************************************************************************************************
    --
    xxcp_comms.Send_job_details(cRequest_id => xxcp_global.gCommon(1).Current_Request_id,
		                            cJob_Status => vJob_Status);
   --
    -- ************************************************************************************************
    --                     Show Time Statistics when called from SQL*Plus
    -- ************************************************************************************************
    --
    xxcp_global.gCommon(1).Current_Request_id := Null;
    --
    -- ***********************************************************************************************************
    --                     Return Job Status Code and Finish!!
    -- ***********************************************************************************************************
    --
    Return(vJob_Status);

  End Control;


Begin
  -- Trace Mode
  Begin
    Select Substr(Lookup_code, 1, 1)
      into gEngine_Trace
      from XXCP_lookups
     where lookup_type = 'TRACE CPA ENGINE'
       and enabled_flag = 'Y';

    Exception when Others then
          gEngine_Trace := 'N';
  End;
  

END XXCP_CPA_TRUE_UP_ENGINE;
/
