CREATE OR REPLACE PACKAGE BODY XXCP_REPORTING AS
  /******************************************************************************
                          V I R T A L  T R A D E R  L I M I T E D
              (Copyright 2003-2012 Virtual Trader Ltd. All rights Reserved)

     NAME:       XXCP_REPORTING
     PURPOSE:    This packages is called from concurrent requests and packages

     REVISIONS:
     Ver        Date      Author  Description
     ---------  --------  ------- ------------------------------------
     02.01.00   05/04/03  Keith   First Release
     02.01.01   08/06/03  Keith   Added Locking control
     02.01.02   11/12/03  Keith   Changed active control to use No and Yes.
     02.01.03   12/10/04  Keith   Added Get_Source_Assignment_id
     02.02.00   13/01/05  Keith   Class_Sign
		 02.02.01   03/06/05  Keith   Added Commit to Activate_Lock_Process and Deactivate
     02.02.02   14/10/05  Keith   changed Preview Mode
     02.02.03   02/11/05  Keith   Locks
		 02.02.04   02/01/06  Keith   Added trunc to cursor in Current_Period_Name
     02.02.05   02/01/09  Keith   Added cCalendar to period range
     02.02.06   15/05/09  Simon   Fix Get_Period_id_details
     02.02.07   09/11/09  Dave    Current_Period_Name now includes Period Set ID
                                  and instance id
     03.02.08   10/06/12  Keith   Add Address format
     03.02.09   19/06/12  Mark    Add Get_Legal_Text for a tax reg.
     03.02 10   01/11/12  Kieth   Added State_ode to Address format
  ******************************************************************************/

  gStringBlank             Constant Varchar2(3) := ' ';
  gDefaultFullPostalString Constant Varchar2(100) := 'ADDR1 NL ADDR2 NL ADDR3 NL CITY STATE NL ZIP NL COUNTRY';

  -- This PL/SQL table stores all the values of the custom attributes.
  Type gCustomAttributesTab Is Table Of Varchar2(100) Index By Binary_Integer;
  -- This a package global table of custom attributes.
  gCustomAttributes gCustomAttributesTab;

  -- This record type contains all fields for an address and is used to by the format address functions and procedures.
  Type addr_rec_type Is Record(
     customer_name        Varchar2(200)
    ,customer_name_base   Varchar2(200)
    ,alt_customer_name    Varchar2(200)
    ,account_name         Varchar2(200)
    ,address1             xxcp_address_details.bill_to_address_line_1%Type
    ,address2             xxcp_address_details.bill_to_address_line_2%Type
    ,address3             xxcp_address_details.bill_to_address_line_3%Type
    ,address4             xxcp_address_details.bill_to_address_line_3%Type
    ,city                 Varchar2(200)
    ,postal_code          Varchar2(200)
    ,state                Varchar2(200)
    ,State_Code           xxcp_address_details.bill_to_state_code%type
    ,province             Varchar2(200)
    ,county               Varchar2(200)
    ,country              Varchar2(80)
    ,country_code         Varchar2(2)
    ,contact_title        Varchar2(80)
    ,contact_first_name   Varchar2(100)
    ,contact_middle_names Varchar2(100)
    ,contact_last_name    Varchar2(100)
    ,contact_job_title    Varchar2(100)
    ,contact_mail_stop    Varchar2(100));

  -- *************************************************************
  --                     SOFTWARE_VERSION
  --                   --------------------
  -- This package can be checked with SQL to ensure it's installed
  -- select packagename.software_version from dual;
  -- *************************************************************
  FUNCTION Software_Version RETURN VARCHAR2 IS
  Begin
    Return('Version [03.02.10] Build Date [01-NOV-2012] NAME [XXCP_REPORTING]');
  END Software_Version;

  -- *************************************************************
  --                    GET_SOURCE_ASSIGNMENT_ID 
  -- *************************************************************
  Function Get_Source_Assignment_id(cSource_Assignment_Name in varchar2)
    RETURN NUMBER IS

    Cursor GX(cSource_Assignment_Name in varchar2) is
      Select Source_Assignment_id
        from XXCP_source_assignments
       where source_assignment_name = cSource_Assignment_Name;

    vSource_Assignment_id Number(15) := 0;

  Begin
    For Rec in GX(cSource_Assignment_Name) loop
      vSource_Assignment_id := Rec.Source_Assignment_id;
    End Loop;
    Return(vSource_Assignment_id);
  END Get_Source_Assignment_id;

  -- *************************************************************
  --                    Get_Trace_Mode 
  -- *************************************************************
  Function Get_Trace_Mode(cUser_id in number) Return varchar2 is
    vMode varchar2(3) := 'N';
  Begin

    Begin
	   Select PV_TRACE into vMode
     from XXCP_user_profiles
     where user_id = cUser_id
       and active = 'Y';
    Exception
      when others then
        null;
    End;
     
    
    Return(vMode);

  End Get_Trace_Mode;

  -- *************************************************************
  --                    Get_Timing_Mode 
  -- *************************************************************
  Function Get_Timing_Mode(cUser_id in number) Return varchar2 is
    vMode varchar2(3) := 'N';
  Begin

    Begin
      select 'Y'
        into vMode
        from XXCP_lookups
       where lookup_type = 'TIMING_PV_MODE_USR'
         and enabled_flag = 'Y'
         and numeric_code = cUser_id;
    Exception
      when others then
        null;
    End;
    Return(vMode);

  End Get_Timing_Mode;

  -- *************************************************************
  --                     PERIOD_DATE_RANGE 
  -- Given a Period Name return  the true date range
  -- *************************************************************
  PROCEDURE PERIOD_DATE_RANGE(cPeriod_Name in varchar2,
                              cStart_Date  out Date,
                              cEnd_Date    out Date,  
                              cCalendar    in varchar2 default null) IS

    vStart_Date Date := Null;
    vEnd_Date   Date := Null;

    Cursor c1(pPeriod_Name in varchar2, pCalendar in varchar2) is
      Select Start_Date, End_Date
        from XXCP_INSTANCE_GL_PERIODS_v g
       where g.period_name = pPeriod_Name
         and g.period_set_name = pCalendar;

  Begin

    For Rec in C1(pPeriod_Name => cPeriod_Name, pCalendar => cCalendar) loop
      vStart_Date := Rec.Start_Date;
      vEnd_Date   := Rec.End_Date;
    End Loop;

    cStart_Date := vStart_Date;
    cEnd_Date   := vEnd_Date;
    
  END PERIOD_DATE_RANGE;

  -- *************************************************************
  --                    CURRENT_PERIOD_NAME 
  -- Given a Period Name return  the true date range
  -- *************************************************************
  FUNCTION CURRENT_PERIOD_NAME(cDate       in Date,
                               cStart_Date out Date,
                               cEnd_Date   out Date,
                               cPeriodSetID in number default 1,
                               cInstanceID in number default 0) RETURN VARCHAR2 IS
    vStart_Date  Date;
    vEnd_Date    Date;
    vPeriod_Name varchar2(100);

    Cursor c1(pDate in Date) is
      Select g.period_name, g.Start_Date, g.End_Date
        from XXCP_INSTANCE_GL_PERIODS_v g
       where trunc(pDate) between g.start_Date and g.end_Date
       and period_set_name_id = cPeriodSetID
       and instance_id = cInstanceID;

  Begin
    -- 02.02.07
--    For Rec in c1(cDate) loop
--      vPeriod_Name := Rec.Period_Name;
--      vStart_Date  := Rec.Start_Date;
--      vEnd_Date    := Rec.End_Date;
--    End Loop;
    
    Open c1(cDate);
    Fetch c1 INTO vPeriod_Name, vStart_Date, vEnd_Date;
    Close c1;
    
    cStart_Date := vStart_Date;
    cEnd_Date   := vEnd_Date;

    Return(vPeriod_Name);
  END CURRENT_PERIOD_NAME;

  -- *************************************************************
  --                     SET_OF_BOOKS_INFO 
  --  Get Important Information from the GL_Sets of books for
  --  Displayin go the Reports.
  -- *************************************************************
  PROCEDURE SET_OF_BOOKS_INFO(cSet_of_books_id      in Number,
                              cDescription          out Varchar2,
                              cSHORT_NAME           out Varchar2,
                              cCHART_OF_ACCOUNTS_ID out Number,
                              cCURRENCY_CODE        out Varchar2,
                              cPERIOD_SET_NAME      out Varchar2) IS

    vDescription          varchar2(100) := 'Un-Assigned';

    Cursor c1(pSet_of_books_id in number) is
      Select Substr(Name, 1, 30) Description,
             Substr(SHORT_NAME, 1, 20) Short_Name,
             CHART_OF_ACCOUNTS_ID,
             Substr(CURRENCY_CODE, 1, 15) Currency_Code,
             Period_Set_Name
        from xxcp_instance_sob_v
       where set_of_books_id = pSet_of_books_id;

  BEGIN

    For Rec in c1(cSet_of_books_id) Loop
      vDescription          := Rec.Description;
      cSHORT_NAME           := Rec.Short_Name;
      cCHART_OF_ACCOUNTS_ID := Rec.Chart_of_Accounts_id;
      cCURRENCY_CODE        := Rec.Currency_Code;
      cPERIOD_SET_NAME      := Rec.Period_Set_Name;
    End Loop;
    cDescription := vDescription;
  END SET_OF_BOOKS_INFO;

  -- *************************************************************
  --                         GET_SECONDS 
  -- *************************************************************
  Function GET_SECONDS return number is
  
    vSeconds number;
  
  Begin
    --     vSeconds := to_number(to_char(current_timestamp,'HH24MISSXFF')); 
    vSeconds := to_number(to_char(sysdate, 'HH24')) * 3600 +
                to_number(to_char(sysdate, 'MI')) * 60 +
                to_number(to_char(current_timestamp, 'SSXFF'));
    Return(vSeconds);
  End GET_SECONDS;

  -- *************************************************************
  --                       GET_SECONDS_DIFF 
  -- *************************************************************
  Function GET_SECONDS_DIFF(cSec1 in number, cSec2 in number) return number is
  
    vSeconds number;
  
  Begin
    vSeconds := nvl(cSec2, 0) - nvl(cSec1, 0);
    Return(vSeconds);
  End GET_SECONDS_DIFF;

  --
  -- Currency_Conversions
  --
  Function Currency_Conversions(cTransaction_Currency          in varchar2
                                 ,cExchange_Currency             in varchar2
                                 ,cDefault_Common_Exch_Curr      in varchar2
                                 ,cExchange_Date                 in date
                                 ,cExchange_Type                 in varchar2
                                ) return number is

   vNew_Exch_Rate        number;
   vCommon_Exch_Currency varchar2(3);

  Begin
      vNew_Exch_Rate := 0;
        if xxcp_foundation.Direct_Exchange_Rate_Exists(cExchange_Currency
                                                      ,cTransaction_Currency
                                                      ,cExchange_Date
                                                      ,cExchange_Type ) then
         vCommon_Exch_Currency := cExchange_Currency;
        Else
         vCommon_Exch_Currency := cDefault_Common_Exch_Curr;
        End If;
      --

        If cExchange_Currency = cTransaction_Currency  then
         vNew_Exch_Rate  := 1; -- Remove Rounding Issues
        Else

          vNew_Exch_Rate  :=  xxcp_foundation.Calculate_Exchange_Rate(cTransaction_Currency
                                                                     ,cExchange_Currency
                                                                     ,vCommon_Exch_Currency
                                                                     ,'EUR'
                                                                     ,cExchange_Date
                                                                     ,cExchange_Type);
        End If;
				
      Return(vNew_Exch_Rate);

  End Currency_Conversions;

  --
  -- Get_Period_Offset
  --
  Function Get_Period_id_details ( cPeriod_id          in number,
                                   cPeriod_Set_Name_id in number,
                                   cPeriods_Offset     in number,
                                   cInstance_id        in number,
                                   cPeriod_Name       out varchar2,
                                   cPeriod_Year       out number,
                                   cPeriod_Num        out number,
                                   cStart_Date        out date,
                                   cEnd_Date          out date,
                                   cPeriod_Type       out varchar2
                               ) return number is
                               
    vPeriod_id number := 0;     
    
    cursor period_minus(pPeriod_id in number, pPeriod_Set_Name_id in number, pInstance_id in number) is
     select (period_year*100)+period_num Period_id, d.period_year, d.period_num, d.Instance_id, d.Start_Date, d.End_Date, d.period_type, d.period_name
       from xxcp_instance_gl_periods_v d
      where (period_year*100)+period_num  <= pPeriod_id   
        and  d.Period_Set_Name_id = pPeriod_Set_Name_id 
        and  d.instance_id        = pInstance_id          -- 02.02.06
      order by period_year desc, period_num desc;                       

    cursor period_plus(cPeriod_id in number, pPeriod_Set_Name_id in number, pInstance_id in number) is
     select (period_year*100)+period_num Period_id, d.period_year, d.period_num, d.Instance_id, d.Start_Date, d.End_Date, d.period_type, d.period_name
       from xxcp_instance_gl_periods_v d
      where (period_year*100)+period_num  >= cPeriod_id  
        and  d.period_set_name_id = pPeriod_Set_Name_id 
        and  d.instance_id        = pInstance_id         -- 02.02.06
      order by period_year asc, period_num asc;        

    -- 02.02.06
    cursor period_equal(cPeriod_id in number, pPeriod_Set_Name_id in number, pInstance_id in number) is
     select (period_year*100)+period_num Period_id, d.period_year, d.period_num, d.Instance_id, d.Start_Date, d.End_Date, d.period_type, d.period_name
       from xxcp_instance_gl_periods_v d
      where (period_year*100)+period_num  = cPeriod_id
        and  d.period_set_name_id = pPeriod_Set_Name_id
        and  d.instance_id        = pInstance_id 
      order by period_year asc, period_num asc;        

      rc number := 0;
                               
  Begin
  
     If cPeriods_Offset < 0 then
       For Rec in period_minus(cPeriod_id, cPeriod_Set_Name_id, cInstance_id) loop
         rc := rc + 1;         
         If rc > abs(cPeriods_Offset) then 
           vPeriod_id := rec.period_id;
           cPeriod_Year  := rec.Period_Year;
           cPeriod_Num   := rec.Period_Num;
           cStart_Date   := rec.Start_Date;
           cEnd_Date     := rec.End_Date;
           cPeriod_Type  := rec.Period_Type;
           cPeriod_Name  := rec.period_name;
           exit;
         End If;                 
       End Loop;
     Elsif cPeriods_Offset = 0 then
       For Rec in period_equal(cPeriod_id, cPeriod_Set_Name_id, cInstance_id) loop -- 02.02.06
           vPeriod_id := rec.period_id;
           cPeriod_Year  := rec.Period_Year;
           cPeriod_Num   := rec.Period_Num;
           cStart_Date   := rec.Start_Date;
           cEnd_Date     := rec.End_Date;
           cPeriod_Type  := rec.Period_Type;
           cPeriod_Name  := rec.period_name;
           exit;
       End Loop;     
     Elsif cPeriods_Offset > 0 then
       For Rec in period_plus(cPeriod_id, cPeriod_Set_Name_id, cInstance_id) loop
         rc := rc + 1;         
         If rc > cPeriods_Offset then 
           vPeriod_id := rec.period_id;
           cPeriod_Year  := rec.Period_Year;
           cPeriod_Num   := rec.Period_Num;
           cStart_Date   := rec.Start_Date;
           cEnd_Date     := rec.End_Date;
           cPeriod_Type  := rec.Period_Type;
           cPeriod_Name  := rec.period_name;
           exit;
         End If;                 
       End Loop;
     End If;
     Return(vPeriod_id);
  End Get_Period_id_details;

  --
  -- Get_Period_Offset
  -- 
  Function Get_Period_Offset ( cPeriod_id          in number,
                               cPeriod_Set_Name_id in number,
                               cPeriods_Offset     in number,
                               cInstance_id        in number) return number is
    vNew_Period_id number;

    vTmp_Period_Year   Number(4);
    vTmp_Period_Num    Number(2);
    vTmp_Start_Date    Date;
    vTmp_End_Date      Date;
    vTmp_Period_Type   Varchar2(15);
    vTmp_Period_Name   varchar2(15);

  Begin

    vNew_Period_id := Get_Period_id_details ( cPeriod_id          ,
                                              cPeriod_Set_Name_id ,
                                              cPeriods_Offset     ,
                                              cInstance_id        ,
                                              vTmp_Period_Name    ,
                                              vTmp_Period_Year    ,
                                              vTmp_Period_Num     ,
                                              vTmp_Start_Date     ,
                                              vTmp_End_Date       ,
                                              vTmp_Period_Type   
                                             );

     Return(vNew_Period_id);
  End Get_Period_Offset;                             
   
  --
  -- Get_Period_Offset_Name 
  --
  Function Get_Period_Offset_Name 
                             ( cPeriod_id          in number,
                               cPeriod_Set_Name_id in number,
                               cPeriods_Offset     in number,
                               cInstance_id        in number) return varchar2 is
                       
    vTmp_Period_Year   Number(4);
    vTmp_Period_Num    Number(2);
    vTmp_Start_Date    Date;
    vTmp_End_Date      Date;
    vTmp_Period_Type   Varchar2(15);
    vTmp_Period_Name   varchar2(15);
    vNew_Period_id     number(6);
  Begin

    vNew_Period_id := Get_Period_id_details ( cPeriod_id          ,
                                              cPeriod_Set_Name_id ,
                                              cPeriods_Offset     ,
                                              cInstance_id        ,
                                              vTmp_Period_Name    ,
                                              vTmp_Period_Year    ,
                                              vTmp_Period_Num     ,
                                              vTmp_Start_Date     ,
                                              vTmp_End_Date       ,
                                              vTmp_Period_Type   
                                             );
   Return(vTmp_Period_Name);

  End Get_Period_Offset_Name;  
  
  
  -- New Reporting addresses
  -- fetch_country_name
  Function fetch_country_name(cTerritoryCode Varchar2) Return Varchar2 Is
  
    Cursor c1(cTerritoryCode Varchar2) Is
      Select territory_short_name
        From fnd_territories_tl
       Where territory_code = cTerritoryCode;
  
    vTranslatedCountryName Varchar2(100);
  
  Begin
  
    If length(cTerritoryCode) = 2 then
  
    -- get the base country name (APPS)
    Open c1(cTerritoryCode);
  
    Fetch c1
      Into vTranslatedCountryName;
  
    If c1%Notfound Then
    
      Raise NO_DATA_FOUND;
    End If;
  
    Close c1;
  
   Else
   
      vTranslatedCountryName :=  cTerritoryCode; 
   
   End If;
  
   Return vTranslatedCountryName;
  
  Exception
    When NO_DATA_FOUND Then
      If c1%Isopen Then
        Close c1;
      End If;
    
      Return Null;
    
  End fetch_country_name;
  ---------------------------------
  -- Populate Custom Attributes
  ---------------------------------
  Procedure populate_custom_attribute(cPosition Number, cValue Varchar2) Is
  Begin
  
    gCustomAttributes(cPosition) := cValue;
  
  End populate_custom_attribute;
  --
  -- fetch_custom_attribute
  --
  Function fetch_custom_attribute(cPosition Number) Return Varchar2 Is
  Begin
  
    Return gCustomAttributes(cPosition);
  
  End fetch_custom_attribute;

  --
  /*---------------------------------------------------------------*
  *  FUNCTION: fmt_list                                           *
  *            Formats address or part of one such as a contact.  *
  *            Result is a string containing customer name,       *
  *            address lines, contact rec,  city                 *
  *            state, postal code and country so that they are in *
  *            right order for a particular country.  Uses a      *
  *            default address format string if the address       *
  *            string parameter is null.                          *
  *                                                               *
  *            This is the only formatting function that actually *
  *            does any work.  The others are just wrappers for   *
  *            this one. It is private because Reports 2.5 used   *
  *            by OMAR 10.7 cannot use Record parms.              *
  *---------------------------------------------------------------*/
  Function fmt_list(cFmtString In Varchar2, cAddrRec In addr_rec_type)
    Return Varchar2 Is
  
    nBeginPos       Number := 1;
    nEndPos         Number;
    lnTokCnt        Number := 0;
    nnLnTokCnt      Number := 0;
    vNextToken      Varchar2(60);
    vNextTokenValue Varchar2(500);
    vLastTokenValue Varchar2(500);
    vSeparator      Varchar2(60) := gStringBlank;
    vSepTokenFlag   Boolean;
    vResultString   Varchar2(1000);
    vFmtString      Varchar2(500);
  
  Begin
  
    If (cFmtString Is Null) Then
      vFmtString := 'CUST_NAME NL ADDR1 NL ADDR2 NL ADDR3 NL ADDR4 NL CITY STATE ZIP NL COUNTRY';
    Else
      vFmtString := RTRIM(cFmtString);
    End If;
  
    Loop
      vNextTokenValue := Null;
      vSepTokenFlag   := False;
    
      If INSTR(vFmtString, gStringBlank, nBeginPos) = 0 Then
        nEndPos := (LENGTH(vFmtString) + 1) - nBeginPos;
      Elsif INSTR(vFmtString, gStringBlank, nBeginPos) > 0 Then
        nEndPos := INSTR(vFmtString, gStringBlank, nBeginPos) - (nBeginPos);
      End If;
    
      vNextToken := SUBSTR(vFmtString, nBeginPos, nEndPos);
      If vNextToken = 'CUST_NAME' Then
        vNextTokenValue := cAddrRec.customer_name;
      Elsif vNextToken = 'ENTITY_NAME' Then
        vNextTokenValue := cAddrRec.customer_name;
      Elsif vNextToken = 'ALT_CUST_NAME' Then
        vNextTokenValue := cAddrRec.alt_customer_name;
      Elsif vNextToken = 'CUST_NAME_BASE' Then
        vNextTokenValue := cAddrRec.customer_name_base;
      Elsif vNextToken = 'ACCOUNT_NAME' Then
        vNextTokenValue := cAddrRec.Account_name;
      Elsif vNextToken = 'ADDR1' Then
        vNextTokenValue := cAddrRec.address1;
      Elsif vNextToken = 'ADDR2' Then
        vNextTokenValue := cAddrRec.address2;
      Elsif vNextToken = 'ADDR3' Then
        vNextTokenValue := cAddrRec.address3;
      Elsif vNextToken = 'ADDR4' Then
        vNextTokenValue := cAddrRec.address4;
      Elsif vNextToken = 'ZIP' Then
        vNextTokenValue := cAddrRec.postal_code;
      Elsif vNextToken = 'CITY' Then
        vNextTokenValue := cAddrRec.city;
      Elsif vNextToken = 'COUNTY' Then
        vNextTokenValue := cAddrRec.county;
      Elsif vNextToken = 'COUNTRY_CODE' Then
        vNextTokenValue := cAddrRec.country_code;
      Elsif vNextToken = 'STATE' Then
        vNextTokenValue := cAddrRec.state;
      Elsif vNextToken = 'STATE_CODE' Then
        vNextTokenValue := cAddrRec.state_code;
      Elsif vNextToken = 'PROVINCE' Then
        vNextTokenValue := cAddrRec.province;
      Elsif vNextToken = 'COUNTRY' Then
        vNextTokenValue := cAddrRec.country;
      Elsif vNextToken = 'CON_TITLE' Then
        vNextTokenValue := cAddrRec.contact_title;
      Elsif vNextToken = 'CON_FN' Then
        vNextTokenValue := cAddrRec.contact_first_name;
      Elsif vNextToken = 'CON_MN' Then
        vNextTokenValue := cAddrRec.contact_middle_names;
      Elsif vNextToken = 'CON_LN' Then
        vNextTokenValue := cAddrRec.contact_last_name;
      Elsif vNextToken = 'CON_JOB_TITLE' Then
        vNextTokenValue := cAddrRec.contact_job_title;
      Elsif vNextToken = 'CON_MAIL_STOP' Then
        vNextTokenValue := cAddrRec.contact_mail_stop;
      Elsif vNextToken = 'CUSTOM_ATTR1' Then
        vNextTokenValue := fetch_custom_attribute(1);
      Elsif vNextToken = 'CUSTOM_ATTR2' Then
        vNextTokenValue := fetch_custom_attribute(2);
      Elsif vNextToken = 'CUSTOM_ATTR3' Then
        vNextTokenValue := fetch_custom_attribute(3);
      Elsif vNextToken = 'CUSTOM_ATTR4' Then
        vNextTokenValue := fetch_custom_attribute(4);
      Elsif vNextToken = 'CUSTOM_ATTR5' Then
        vNextTokenValue := fetch_custom_attribute(5);
      Elsif vNextToken = 'COMMA' Then
        vSeparator    := ', ';
        vSepTokenFlag := True;
      Elsif vNextToken = 'HYPHEN' Then
        vSeparator    := '-';
        vSepTokenFlag := True;
      Elsif vNextToken = 'NL' Then
        vSeparator    := CHR(10);
        vSepTokenFlag := True;
      Elsif vNextToken = 'XNL' Then
        vSeparator    := CHR(10);
        vSepTokenFlag := True;
      Else
        -- Accepting next token as a literal - it's a feature!
        vNextTokenValue := vNextToken;
      End If;
    
      If (vNextToken = 'XNL') Then
        -- Special case of a forced new line.
        -- Normally, separators do not cause immediate action.
        vResultString   := vResultString || vLastTokenValue || vSeparator;
        vLastTokenValue := Null; -- This restarts next line
        nnLnTokCnt      := 0;
        lnTokCnt        := 0;
        vSeparator      := gStringBlank;
      End If;
    
      If (Not vSepTokenFlag) Then
        If SUBSTR(vSeparator, 1, 1) = CHR(10) And
           (nnLnTokCnt > 0 Or lnTokCnt = 0) Then
          vResultString := vResultString || vLastTokenValue || vSeparator;
          nnLnTokCnt    := 0;
          lnTokCnt      := 0;
        Else
          If vNextTokenValue Is Null Then
            If (vLastTokenValue Is Not Null) Then
              vResultString := vResultString || vLastTokenValue ||gStringBlank;
            End If;
          Else
            If (vLastTokenValue Is Not Null) Then
              vResultString := vResultString || vLastTokenValue || vSeparator;
            End If;
          End If;
        End If;
        vLastTokenValue := vNextTokenValue;
        vSeparator      := gStringBlank;
        lnTokCnt        := lnTokCnt + 1;
        If vNextTokenValue Is Not Null Then
          nnLnTokCnt := nnLnTokCnt + 1;
        End If;
      End If;
      Exit When INSTR(vFmtString, gStringBlank, nBeginPos) = 0;
    
      nBeginPos := INSTR(vFmtString, gStringBlank, nBeginPos) + 1;
    
    End Loop;
    Return(RTRIM(vResultString || vLastTokenValue, CHR(10)));
  End fmt_list;

  --
  --  fmt_standard_addres
  --
  Function fmt_standard_address(cCompany_name       in Varchar2, 
                                cAddress1           in Varchar2, 
                                cAddress2           in Varchar2, 
                                cAddress3           in Varchar2, 
                                cAddress4           in Varchar2, 
                                cCity               in Varchar2, 
                                cCounty             in Varchar2, 
                                cCountry            in Varchar2, 
                                cPostalCode         in Varchar2, 
                                cStateCode          in Varchar2,
                                cPostalFormatString In Varchar2)
    Return Varchar2 Is
  
    vAddrRec addr_rec_type;    
    vToAddr  varchar2(5000);
  
  Begin
    vAddrRec.customer_name := cCompany_Name;
    vAddrRec.address1 := cAddress1;
    vAddrRec.address2 := cAddress2;
    vAddrRec.address3 := cAddress3;
    vAddrRec.address4 := cAddress4;
    vAddrRec.city     := cCity;
    vAddrRec.county   := cCounty;
    vAddrRec.country  := cCountry;
    vAddrRec.postal_code := cPostalCode;
  
    vToAddr := fmt_list(NVL(cPostalFormatString, gDefaultFullPostalString),vAddrRec);
  
    Return ( vToAddr);
  
  End fmt_standard_address;
  --
  -- Get_Bill_To_Address
  --
  Function Get_Bill_To_Address(cTax_Reg_id In Number) Return Varchar2 Is
  
    Cursor bt(pTax_Reg_id in number) Is
      Select fg.bill_to_name,
             fg.bill_to_address_line_1,
             fg.bill_to_address_line_2,
             fg.bill_to_address_line_3,
             fg.bill_to_town_or_city,
             fg.bill_to_region,
             fg.bill_to_country,
             fg.bill_to_postal_code,
             fg.bill_to_telephone_number,
             fg.bill_to_display_style,
             x.description address_style,
             fg.bill_to_state_code
        From xxcp_address_details fg,
             xxcp_lookups_v x
       Where fg.tax_registration_id = pTax_Reg_id
         and x.category like 'ADDRESS FORMAT'
         and upper(fg.bill_to_display_style) = upper(x.category_subset);
  
    vAddress Varchar2(3000);
  
  Begin
  
    For rec In bt(cTax_Reg_id)
    Loop 
    
      vAddress := fmt_standard_address(
                                       cCompany_Name       => rec.bill_to_name
                                      ,cAddress1           => rec.bill_to_address_line_1
                                      ,cAddress2           => rec.bill_to_address_line_2
                                      ,cAddress3           => rec.bill_to_address_line_3
                                      ,cAddress4           => Null
                                      ,cCity               => rec.bill_to_town_or_city
                                      ,cCounty             => rec.bill_to_region
                                      ,cCountry            => rec.bill_to_country
                                      ,cPostalCode         => rec.bill_to_postal_code
                                      ,cStateCode          => rec.bill_to_state_Code
                                      ,cPostalFormatString => nvl(rec.address_style,gDefaultFullPostalString));
    
    End Loop;
  
    Return(vAddress);
  End Get_Bill_To_Address;

  --
  -- Get_Ship_To_Address
  --
  Function Get_Ship_To_Address(cTax_Reg_id In Number) Return Varchar2 Is
  
    Cursor st(pTax_Reg_id in number) Is
      Select fg.ship_to_name,
             fg.ship_to_address_line_1,
             fg.ship_to_address_line_2,
             fg.ship_to_address_line_3,
             fg.ship_to_town_or_city,
             fg.ship_to_region,
             fg.ship_to_country,
             fg.ship_to_postal_code,
             fg.ship_to_telephone_number,
             fg.ship_to_display_style,
             fg.ship_to_state_Code,
             x.description address_style
        From xxcp_address_details fg,
             xxcp_lookups_v x
       Where fg.tax_registration_id = pTax_Reg_id
         and x.category like 'ADDRESS FORMAT'
         and upper(fg.ship_to_display_style) = upper(x.category_subset);
       
  
    vAddress Varchar2(3000);
  
  Begin
  
    For rec In st(cTax_Reg_id)
    Loop
    
      vAddress := fmt_standard_address(
                                       cCompany_Name       => rec.ship_to_name
                                      ,cAddress1           => rec.ship_to_address_line_1
                                      ,cAddress2           => rec.ship_to_address_line_2
                                      ,cAddress3           => rec.ship_to_address_line_3
                                      ,cAddress4           => Null
                                      ,cCity               => rec.ship_to_town_or_city
                                      ,cCounty             => rec.ship_to_region
                                      ,cCountry            => rec.ship_to_country
                                      ,cPostalCode         => rec.ship_to_postal_code
                                      ,cStateCode          => rec.ship_to_state_Code
                                      ,cPostalFormatString => nvl(rec.address_style,gDefaultFullPostalString));
    End Loop;

    If vAddress is null then
        vAddress := Get_Bill_To_Address(cTax_Reg_id);    
    End If;                                      
    
  
    Return(vAddress);
  End Get_Ship_To_Address; 
  
  --
  -- Get_Legal_Text
  --
  Function Get_Legal_Text(cTax_Reg_id In Number) Return Varchar2 Is
  
    Cursor st(pTax_Reg_id in number) Is
      Select fg.legal_doc_text_1
            ,fg.legal_doc_text_2
            ,fg.legal_doc_text_3
        From xxcp_address_details fg
       Where fg.tax_registration_id = pTax_Reg_id;

    vReturn                         Varchar2(5000);
    vLegal_Doc_Text_1               xxcp_address_details.legal_doc_text_1%type;
    vLegal_Doc_Text_2               xxcp_address_details.legal_doc_text_2%type;
    vLegal_Doc_Text_3               xxcp_address_details.legal_doc_text_3%type;
  
  Begin
  
    For rec In st(cTax_Reg_id)
    Loop
      vLegal_Doc_Text_1 := rec.legal_doc_text_1;
      vLegal_Doc_Text_2 := rec.legal_doc_text_2;
      vLegal_Doc_Text_3 := rec.legal_doc_text_3;
    End Loop;

    If vLegal_Doc_Text_1 is not null then
      vReturn := vLegal_Doc_Text_1;
      If vLegal_Doc_Text_2 is not null then
        vReturn := vReturn||chr(10)||vLegal_Doc_Text_2;
        If vLegal_Doc_Text_3 is not null then
          vReturn := vReturn||chr(10)||vLegal_Doc_Text_3;
        End If;
      End If;
    Else -- Line 1 is null
      If vLegal_Doc_Text_2 is not null then
        vReturn := vLegal_Doc_Text_2;
        If vLegal_Doc_Text_3 is not null then
          vReturn := vReturn||chr(10)||vLegal_Doc_Text_3;
        End If;
      Else -- Line 2 is null
        If vLegal_Doc_Text_3 is not null then
          vReturn := vLegal_Doc_Text_3;
        End If;        
      End If;
    End If;
     
    Return(vReturn);
  End Get_Legal_Text;   


  --
  -- Get_Bill_To_Zip
  --
  Function Get_Bill_To_Zip(cTax_Reg_id In Number) Return Varchar2 is
  
    Cursor bt(pTax_Reg_id in number) Is
      Select fg.bill_to_name,
             fg.bill_to_address_line_1,
             fg.bill_to_address_line_2,
             fg.bill_to_address_line_3,
             fg.bill_to_town_or_city,
             fg.bill_to_region,
             fg.bill_to_country,
             fg.bill_to_postal_code,
             fg.bill_to_telephone_number,
             fg.bill_to_display_style,
             x.description address_style,
             fg.bill_to_state_code
        From xxcp_address_details fg,
             xxcp_lookups_v x
       Where fg.tax_registration_id = pTax_Reg_id
         and x.category like 'ADDRESS FORMAT'
         and upper(fg.bill_to_display_style) = upper(x.category_subset);
  
    vAddress Varchar2(3000);
  
  Begin
  
    For rec In bt(cTax_Reg_id) Loop 
    
      vAddress := fmt_standard_address(
                                       cCompany_Name       => rec.bill_to_name
                                      ,cAddress1           => rec.bill_to_address_line_1
                                      ,cAddress2           => rec.bill_to_address_line_2
                                      ,cAddress3           => rec.bill_to_address_line_3
                                      ,cAddress4           => Null
                                      ,cCity               => rec.bill_to_town_or_city
                                      ,cCounty             => rec.bill_to_region
                                      ,cCountry            => rec.bill_to_country
                                      ,cPostalCode         => rec.bill_to_postal_code
                                      ,cStateCode          => rec.bill_to_state_Code
                                      ,cPostalFormatString => nvl(rec.address_style,gDefaultFullPostalString));
    
    End Loop;
  
    Return(vAddress);
  End Get_Bill_To_Zip;

  
End XXCP_REPORTING;
/
