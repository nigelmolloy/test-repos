CREATE OR REPLACE PACKAGE XXCP_STATEMENT_CODE AS
/******************************************************************************
                          V I R T A L  T R A D E R  L I M I T E D 
             (Copyright 2005-2011 Virtual Trader Ltd. All rights Reserved)

     NAME:       XXCP_STATEMENT_CODE 
     PURPOSE:    Statement Code Generator. Creates the DML Insert/Update packages

     Version [03.03.15] Build Date [16-AUG-2012] Name [XXCP_STATEMENT_CODE]
     
     *****************************************************************************/
  --
  -- gColumnRulesRec Record 
  --
  gCR_Cnt pls_integer := 0;
	
  TYPE gCR_TYPE is RECORD(
    Request_id               xxcp_activity_control.request_id%type,
    Rule_id                  xxcp_column_rules_tables.rule_id%type,
		Rule_Category            xxcp_column_rules_tables.rule_category_1%type,
    Source_table             xxcp_sys_source_tables.transaction_table%type,
    Target_table             xxcp_sys_target_tables.transaction_table%type,
    Target_Instance_id       xxcp_target_assignments.target_instance_id%type,
    D0000_Pointers           varchar2(2000),
    D0000_Counter            pls_integer,
    I1009_Pointers           varchar2(2000),
    I1009_Counter            pls_integer,
    -- Hist Dynamic
    DynamicHistPointersCnt   pls_integer,
    DynamicHistPointers      varchar2(2000),
    Dynamic_Array            XXCP_DYNAMIC_ARRAY, -- Dynamic
    -- Hist Internal
    InternalHistPointersCnt  pls_integer,
    InternalHistPointers     varchar2(2000),
    Internal_Array           XXCP_DYNAMIC_ARRAY, -- Internal
    --
    Insert_statement         varchar2(32676),
    Direct_DML_statement     varchar2(32676),
    History_statement        varchar2(32676),
    Direct_History_statement varchar2(32676),
    Column_Count             pls_integer);

  TYPE gCR_REC is TABLE of gCR_TYPE INDEX by BINARY_INTEGER;
  gCR gCR_REC;


	FUNCTION  Software_Version RETURN VARCHAR2;

  Function Column_Validation(cPrevew_ctl   in number
	                          ,cErr_Message out varchar2) Return Boolean;

  Function Process_History(cPreview_Ctl   in number,
                           cPreview_mode  in varchar2 Default 'N',
                           cSQLERRM      out varchar2) Return Boolean;

  Function Process_DML(cPreview_Ctl   in number,
                       cPreview_mode  in varchar2 Default 'N',
                       cSQLERRM      out varchar2) Return Boolean;

  Procedure Get_Package_Names(cPreview_Ctl_id in number, 
                              cTarget_Name out varchar2,
                              cHistory_Name out varchar2, 
                              cPreview_Name out varchar2,
                              cICS_Preview_Name out varchar2,
                              cICS_Target_Name out varchar2);

  Function Validate_Column_Rules(cPreview_ctl  in number,
	                               cMessage     out varchar2) return varchar2;

  Function Generate_Column_Rules(cPreview_ctl  in number,
	                               cMessage     out varchar2) return varchar2;

END XXCP_STATEMENT_CODE;
/
