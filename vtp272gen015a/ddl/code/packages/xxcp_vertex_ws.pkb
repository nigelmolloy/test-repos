create or replace package body xxcp_vertex_ws is
   /**********************************************************************************************************
                           V I R T A L  T R A D E R  L I M I T E D 
               (Copyright 2002-2012 Virtual Trader Ltd. All rights Reserved)
   
      NAME:       XXCP_VERTEX_WS
      PURPOSE:    The controlling processes for single Dynamic SQL selects
   
      REVISIONS:
      Ver        Date      Author  Description
      ---------  --------  ------- ------------------------------------
      03.06.01   05/10/12  Simon   First Creation
   **********************************************************************************************************/

  TYPE tVert_Inv_Resp is RECORD (taxResult         varchar2(500),
                                 taxType           varchar2(500),
                                 taxDate           date,
                                 situs             varchar2(500),
                                 Jurisdiction      varchar2(500),
                                 JurisdictionLevel varchar2(500),
                                 CalculatedTax     number,
                                 EffectiveRate     number,
                                 Taxable           number,
                                 Imposition        varchar2(500),
                                 TaxRuleId         number,
                                 CurrencyCode      varchar2(10),
                                 ExtendedPrice     number
                                 );


  TYPE tab_Vert_Inv_Resp is table of tVert_Inv_Resp
       index by binary_Integer;

  gVert_In_Resp_Rec  tVert_Inv_Resp;
  gVert_In_Resp_Tab  tab_Vert_Inv_Resp;
  gTest_Mode_Enabled varchar2(1);
  gTax_Lookup_Trace  varchar2(32767);

   -- *************************************************************
   --                     Software Version Control
   -- This package can be checked with SQL to ensure it's installed
   -- select packagename.software_version from dual;
   -- *************************************************************
   FUNCTION Software_version RETURN VARCHAR2 IS
   BEGIN
      RETURN('Version [03.06.01] Build Date [05-NOV-2012] Name [XXCP_VERTEX_WS]');
   END Software_version;

   -- *************************************************************
   --                   Get_Vertex_Credentials
   --   Get Username and Password for Vertex Web Service Call 
   -- *************************************************************
  Procedure Get_Vertex_Credentials (cUsername OUT varchar2,
                                    cPassword OUT varchar2) is
  
  begin

    -- Username
    Begin
      select Profile_value
      into   cUsername
      from   xxcp_sys_profile
      where  profile_category = 'EV'
      and    profile_name     = 'VERTEX WS USERNAME'; 
    Exception
      When Others then
        cUsername := 'oracle';
    End;
    
    -- Password
    Begin
      select Profile_value
      into   cPassword
      from   xxcp_sys_profile
      where  profile_category = 'EV'
      and    profile_name     = 'VERTEX WS PASSWORD'; 
    Exception
      When Others then
        cPassword := 'vertex';
    End;
    
  end Get_Vertex_Credentials;

  function Get_Request_String (cRequest_Type    varchar2,
                               cSeller_Postcode varchar2,
                               cSeller_Country  varchar2,
                               cCust_Postcode   varchar2,
                               cCust_Country    varchar2,
                               cUnit_Price      varchar2,
                               cString_Request OUT varchar2,
                               cHost_Name      OUT varchar2,
                               cPort           OUT number,
                               cEndPoint       OUT varchar2) return number is

    vVert_UserName     varchar2(100) := 'oracle';
    vVert_Password     varchar2(100) := 'vertex';
    vSeller_PostCode   varchar2(30)  := '77001';                -- Multi Tax
    vSeller_Country    varchar2(30)  := 'UNITED STATES';
    vCust_PostCode     varchar2(30)  := '78711-3231';           -- Multi Tax
    vCust_COuntry      varchar2(30)  := 'UNITED STATES';
    vQuantity          number        := 1;
    vUnitPrice         number        := 800;
    vInternalErrorCode number        := 0;

    cursor c1 is select ws_request,
                        ws_hostname,
                        ws_port,
                        ws_endpoint
                 from   xxcp_web_service_ctl
                 where  ws_id = 2;  -- 2 is the static VERTEX id
    
  begin
        -- Hardcoded String at the moment
        -- Will be pulled from a table in final version
        -- and use bind variables (dynamic attributes as the params)

    If gTest_Mode_Enabled = 'Y' then     
        
        cString_Request := '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:vertexinc:o-series:tps:5:0">
         <soapenv:Header/>
         <soapenv:Body>
          <VertexEnvelope xmlns="urn:vertexinc:o-series:tps:5:0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="urn:vertexinc:o-series:tps:5:0 VertexInc_Envelope.xsd">
            <Login>
              <UserName>'||vVert_UserName||'</UserName>
              <Password>'||vVert_Password||'</Password>
            </Login>
       <'||cRequest_Type||' documentDate="2012-01-01" returnAssistedParametersIndicator="true" transactionType="SALE">
          <Seller>
           <PhysicalOrigin>
              <PostalCode>'||vSeller_PostCode||'</PostalCode>
              <Country>'||vSeller_Country||'</Country>
            </PhysicalOrigin>
          </Seller>
          <Customer>
            <Destination>
              <PostalCode>'||vCust_PostCode||'</PostalCode>
              <Country>'||vCust_Country||'</Country>
            </Destination>
          </Customer>
          <LineItem>
            <Quantity>'||vQuantity||'</Quantity>
            <UnitPrice>'||vUnitPrice||'</UnitPrice>
          </LineItem>
        </'||cRequest_Type||'>
          </VertexEnvelope>
         </soapenv:Body>
      </soapenv:Envelope>';  -- Replace with Correct SOAP envelope


      cHost_Name := 'dummy.server.com';
      cPort      := 8088;
      cEndPoint  := '/CalculateTax50';
    
    Else
      
      -- Get WS Request
      Open c1;
      fetch c1 into cString_Request,
                    cHost_Name,
                    cPort,
                    cEndpoint;
      close c1;
      
      -- Now replace bind variables
      cString_Request := replace(cString_Request,':REQUEST_TYPE',cRequest_Type);
      cString_Request := replace(cString_Request,':SELLER_POSTCODE',cSeller_Postcode);
      cString_Request := replace(cString_Request,':SELLER_COUNTRY',cSeller_Country);
      cString_Request := replace(cString_Request,':CUST_POSTCODE',cCust_Postcode);
      cString_Request := replace(cString_Request,':CUST_COUNTRY',cCust_Country);
      cString_Request := replace(cString_Request,':UNIT_PRICE',cUnit_Price);

      Get_Vertex_Credentials(cUsername => vVert_Username,  -- Out
                             cPassword => vVert_Password); -- Out
      
      cString_Request := replace(cString_Request,':USERNAME',vVert_Username);
      cString_Request := replace(cString_Request,':PASSWORD',vVert_Password);
      
      
      If cString_Request is null then
        vInternalErrorCode := 15501;     -- Error Retrieving WS_Request string
      End if;

    End if;

    return(vInternalErrorCode);

  Exception
    When others then
      return(15501);  -- Unhandled Exception
  end Get_Request_String;

  function Send_Vertex_Request (cString_Request IN varchar2,
                                cHost_Name      IN varchar2,
                                cPort           IN number,
                                cEndPoint       IN varchar2,
                                cHTTP_request   IN OUT NOCOPY UTL_HTTP.req) return number is

    vBuffer_size     number := 512;
    vSubstring_msg   varchar2(512);
    vRaw_data        raw(512);
    vURL             varchar2(2000) := 'http://'||cHost_Name||':'||cPort||cEndPoint;
  begin 

    -- If run in Test Mode then return static XML string
    If gTest_Mode_Enabled = 'Y' then
      null;
    Else
      UTL_HTTP.set_transfer_timeout(60);
      cHttp_request := UTL_HTTP.begin_request(url    => vURL,
                                              method => 'POST',
                                              http_version => 'HTTP/1.1');

      UTL_HTTP.set_header(cHttp_request, 'User-Agent','Mozilla/4.0');
      UTL_HTTP.set_header(cHttp_request, 'Host',cHost_Name||':'||cPort);
      UTL_HTTP.set_header(cHttp_request, 'Connection','close');
      UTL_HTTP.set_header(cHttp_request, 'Content-Type','text/xml;charset=UTF-8');
      UTL_HTTP.set_header(cHttp_request, 'SOAPAction','');
      UTL_HTTP.set_header(cHttp_request, 'Content-Length',length(cString_Request));

      -- Request Loop
      For i in 0..CEIL(LENGTH(cString_Request) / vBuffer_size) - 1 loop
        vSubstring_Msg := substr(cString_Request, i * vBuffer_size + 1, vBuffer_Size);

        BEGIN
          vRaw_data := utl_raw.cast_to_raw(vSubstring_msg);
          UTL_HTTP.write_raw(r    => cHttp_request,
                             data => vRaw_Data);
        EXCEPTION
          When NO_DATA_FOUND then
            exit;
        END;

      End loop;   
    End if; 
    return(0);
  Exception
    When Others then
      xxcp_foundation.fndwriteerror(15502,'URL <'||vURL||'> : '||SQLERRM, cString_Request);
      return(15502);
  end Send_Vertex_Request;

   -- *************************************************************
   --                   Read_Vertex_Response
   --         Insert Tax result into XXCP_TAX_RESULTS
   -- *************************************************************
  Function Read_Vertex_Response (cHTTP_request IN OUT NOCOPY UTL_HTTP.req,
                                 cXML_Resp        OUT NOCOPY XMLTYPE) return number is
    vHTTP_response     UTL_HTTP.resp;    
    vBuffer_size       number := 512;
    vRaw_data          raw(512);
    vClob_Response     CLOB;
    vInternalErrorCode number := 0;
  Begin

    -- If run in Test Mode then return static XML string
    If gTest_Mode_Enabled = 'Y' then
      cXML_Resp := XMLTYPE('<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
   <S:Body>
      <VertexEnvelope xsi:schemaLocation="urn:vertexinc:o-series:tps:5:0 VertexInc_Envelope.xsd" xmlns="urn:vertexinc:o-series:tps:5:0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
         <Login>
            <UserName>oracle</UserName>
            <Password>vertex</Password>
         </Login>
         <InvoiceResponse documentDate="2012-01-01" transactionType="SALE" postingDate="2012-08-06" returnAssistedParametersIndicator="true">
            <Currency isoCurrencyCodeAlpha="USD" isoCurrencyCodeNum="840" isoCurrencyName="US Dollar"/>
            <SubTotal>100.0</SubTotal>
            <Total>108.25</Total>
            <TotalTax>8.25</TotalTax>
            <LineItem lineItemNumber="1">
               <Seller>
                  <PhysicalOrigin taxAreaId="442010000">
                     <City>Houston</City>
                     <MainDivision>TX</MainDivision>
                     <SubDivision>Harris</SubDivision>
                     <PostalCode>77001</PostalCode>
                     <Country>US</Country>
                  </PhysicalOrigin>
                  <AdministrativeOrigin taxAreaId="442010000">
                     <City>Houston</City>
                     <MainDivision>TX</MainDivision>
                     <SubDivision>Harris</SubDivision>
                     <PostalCode>77001</PostalCode>
                     <Country>US</Country>
                  </AdministrativeOrigin>
               </Seller>
               <Customer>
                  <CustomerCode>1229</CustomerCode>
                  <Destination taxAreaId="444530150">
                     <City>Austin</City>
                     <MainDivision>TX</MainDivision>
                     <SubDivision>Travis</SubDivision>
                     <PostalCode>78711-3231</PostalCode>
                     <Country>US</Country>
                  </Destination>
               </Customer>
               <Quantity>1.0</Quantity>
               <FairMarketValue>100.0</FairMarketValue>
               <ExtendedPrice>100.0</ExtendedPrice>
               <Taxes taxResult="TAXABLE" taxType="SALES" situs="ADMINISTRATIVE_ORIGIN">
                  <Jurisdiction jurisdictionLevel="STATE" jurisdictionId="35763">TEXAS</Jurisdiction>
                  <CalculatedTax>6.25</CalculatedTax>
                  <EffectiveRate>0.0625</EffectiveRate>
                  <Taxable>100.0</Taxable>
                  <Imposition impositionType="General Sales and Use Tax">Sales and Use Tax</Imposition>
                  <TaxRuleId>18653</TaxRuleId>
               </Taxes>
               <Taxes taxResult="TAXABLE" taxType="SELLER_USE" situs="DESTINATION">
                  <Jurisdiction jurisdictionLevel="CITY" jurisdictionId="77848">AUSTIN</Jurisdiction>
                  <CalculatedTax>1.0</CalculatedTax>
                  <EffectiveRate>0.01</EffectiveRate>
                  <Taxable>100.0</Taxable>
                  <Imposition impositionType="General Sales and Use Tax">Local Sales and Use Tax</Imposition>
                  <TaxRuleId>26224</TaxRuleId>
               </Taxes>
               <Taxes taxResult="TAXABLE" taxType="SELLER_USE" situs="DESTINATION">
                  <Jurisdiction jurisdictionLevel="TRANSIT_DISTRICT" jurisdictionId="78110">AUSTIN METROPOLITAN TRANSIT AUTHORITY</Jurisdiction>
                  <CalculatedTax>1.0</CalculatedTax>
                  <EffectiveRate>0.01</EffectiveRate>
                  <Taxable>100.0</Taxable>
                  <Imposition impositionType="General Sales and Use Tax">Local Sales and Use Tax</Imposition>
                  <TaxRuleId>26633</TaxRuleId>
               </Taxes>
               <TotalTax>8.25</TotalTax>
            </LineItem>
         </InvoiceResponse>
         <ApplicationData/>
      </VertexEnvelope>
   </S:Body>
</S:Envelope>');  
    else
    
      vHTTP_response := UTL_HTTP.get_response(cHTTP_request);

      If gTest_Mode_Enabled = 'Y' then
        DBMS_OUTPUT.put_line('Response Status Code   <'||vHttp_response.status_code||'>');
        DBMS_OUTPUT.put_line('Response Reason Phrase <'||vHttp_response.reason_phrase||'>');
        DBMS_OUTPUT.put_line('Response HTTP Version  <'||vHttp_response.http_version||'>');
      End if;

      BEGIN
        -- Read Response
        LOOP
            UTL_HTTP.read_raw(vHttp_response, vRaw_data, vBuffer_size);
            vClob_response := vClob_response || UTL_RAW.cast_to_varchar2(vRaw_data);
        END LOOP;

      EXCEPTION
        WHEN UTL_HTTP.end_of_body THEN
          UTL_HTTP.end_response(vHttp_response);
      END;
      
      -- Clob to XMLTYPE
      cXML_Resp := xmltype.createxml(vClob_response);

    end if;
          
    return(vInternalErrorCode);
  Exception
    When Others then
      xxcp_foundation.fndwriteerror(15503,'Unhandled Exception: '||SQLERRM,vClob_response);
      return(15503);  -- Unhandled Exception
  End Read_Vertex_Response;

   -- *************************************************************
   --                   Insert_Tax_Results
   --         Insert Tax result into XXCP_TAX_RESULTS
   -- *************************************************************
  function Populate_Tax_Results (cRecord_Num       number,
                                 cTransaction_Date date,
                                 cTaxable          number,
                                 cTotal_Gross      number,
                                 cTotal_Tax        number,
                                 cTotal_Tax_Rate   number) return number is
    
  begin
   
   -- Need to verify all the values that are being inserted 
   if cRecord_Num = 1 then
     -- Tax Record Set (Header)
     xxcp_te_base.gTax_Record_Set(1).Tax_Engine_id    := 2;  -- Vertex
     xxcp_te_base.gTax_Record_Set(1).Element_Count    := gVert_In_Resp_Tab.count;
     xxcp_te_base.gTax_Record_Set(1).Taxable_Amount   := cTaxable;
     xxcp_te_base.gTax_Record_Set(1).Taxable_Currency := gVert_In_Resp_Tab(1).CurrencyCode;
     xxcp_te_base.gTax_Record_Set(1).Tax_Gross_Amount := cTotal_Gross;
     xxcp_te_base.gTax_Record_Set(1).Tax_Rate         := cTotal_Tax_Rate;   
     xxcp_te_base.gTax_Record_Set(1).Tax_Amount       := cTotal_Tax;
   End if;

   -- Tax Element Set (Detail)
   xxcp_te_base.gTax_Element_Set(cRecord_Num).Tax_Element_Number := cRecord_Num;
   xxcp_te_base.gTax_Element_Set(cRecord_Num).Tax_Rate_Code := gVert_In_Resp_Tab(cRecord_Num).TaxType;
   xxcp_te_base.gTax_Element_Set(cRecord_Num).Tax_Rate      := gVert_In_Resp_Tab(cRecord_Num).EffectiveRate;
   xxcp_te_base.gTax_Element_Set(cRecord_Num).Tax_Amount    := gVert_In_Resp_Tab(cRecord_Num).CalculatedTax;
   xxcp_te_base.gTax_Element_Set(cRecord_Num).Tax_Rate_id   := gVert_In_Resp_Tab(cRecord_Num).TaxRuleId;
   xxcp_te_base.gTax_Element_Set(cRecord_Num).Tax_Jurisdiction := gVert_In_Resp_Tab(cRecord_Num).Jurisdiction;
   xxcp_te_base.gTax_Element_Set(cRecord_Num).Imposition    := gVert_In_Resp_Tab(cRecord_Num).Imposition;

   If xxcp_global.Trace_on = 'Y' then
        gTax_Lookup_Trace := gTax_Lookup_Trace ||
        '  Element <'||to_char(cRecord_Num)||'>'||Chr(10);
                      
        gTax_Lookup_Trace := gTax_Lookup_Trace || '      Tax Amount <'||xxcp_te_base.gTax_Element_Set(cRecord_Num).Tax_Amount||'> Tax Rate <'||xxcp_te_base.gTax_Element_Set(cRecord_Num).Tax_Rate||'> Tax Rate id <'||xxcp_te_base.gTax_Element_Set(cRecord_Num).Tax_Rate_id||'>'||chr(10);
   End if;
  
                                    
    return(0);
  Exception when others then
    xxcp_foundation.fndwriteerror(15504,'Unhandled Exception: '||SQLERRM);
    return(15504);
  end populate_tax_results;

   -- *************************************************************
   --                   Test_Mode_Enabled
   --   Should a "Static" request and response be used for testing
   --   when a real vertex web service is not available? 
   -- *************************************************************
  function Test_Mode_Enabled return varchar2 is
    
    vResult varchar2(1);
  begin
    select substr(Profile_value,1,1)
    into   vResult
    from   xxcp_sys_profile
    where  profile_category = 'EV'
    and    profile_name     = 'VERTEX WS TEST MODE ENABLED';
  
    return(vResult);
  
  Exception
    When others then
      return('N');  
  end Test_Mode_Enabled;


   -- *************************************************************
   --                        Control
   --                    Main Entry Point
   -- *************************************************************
  function Control (cTaxable_Amount   number,
                    cTaxable_Currency varchar2,
                    cTransaction_Date date,
                    cSeller_PostCode  varchar2,
                    cSeller_Country   varchar2,
                    cCust_Postcode    varchar2,
                    cCust_Country     varchar2,
                    cPreview_Mode     varchar2 default 'N') return number is

    vHttp_request    UTL_HTTP.req;
    vHTTP_response   UTL_HTTP.resp;
    vString_Request  varchar2(32767);
    vXML_Resp        XMLTYPE;
    vFaultCode       varchar2(500);
    vFaultString     varchar2(32767);
    vCount           number;
    vTotalTax        number;
    vTaxable         number;
    vExtendedPrice   number;
    vCurrency_Code   varchar2(10);
    vTotal           number;
    vTotal_Tax_Rate  number;
    vDocumentDate    varchar2(20);

    -- Input Params
    vRequest_Type    varchar2(30);

    vSOAP_ns         varchar2(500) := 'xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"';
    vVert_ns         varchar2(500) := 'xmlns="urn:vertexinc:o-series:tps:5:0"';
    vFault_ns        varchar2(500) := 'xmlns:nsf="http://schemas.xmlsoap.org/soap/envelope/"';

    vHost_Name       varchar2(200);
    vPort            number;
    vEndPoint        varchar2(500);

    vInternalErrorCode number := -1;
  begin

    -- Run in Test Mode?
    gTest_Mode_Enabled := Test_Mode_Enabled;

-- Only ever call in Quotation mode

--    If nvl(cPreview_Mode,'N') = 'Y' then
      vRequest_Type := 'QuotationRequest';
--    Else
--      vRequest_Type := 'InvoiceRequest';
--    End if;      

    If xxcp_global.Trace_on = 'Y' then
      gTax_Lookup_Trace := 'Vertex Web Service Call : '||chr(10)||
                           ' Taxable Amount <'||cTaxable_Amount||'>'||
                           ' Taxable Currency <'||cTaxable_Currency||'>'||
                           ' Transaction Date <'||to_char(cTransaction_Date,'DD-MON-YYYY')||'>'||
                           ' Seller PostCode <'||cSeller_PostCode||'>'||
                           ' Seller Country <'||cSeller_Country||'>'||
                           ' Cust Postcode <'||cCust_Postcode||'>'||
                           ' Cust Country <'||cCust_Country||'>'||chr(10)||chr(10);                           
    End if;

                    
    -- Get Request_String
    vInternalErrorCode := Get_Request_String(cRequest_Type    => vRequest_Type,
                                             cSeller_Postcode => cSeller_Postcode,
                                             cSeller_Country  => cSeller_Country,
                                             cCust_Postcode   => cCust_Postcode,
                                             cCust_Country    => cCust_Country,
                                             cUnit_Price      => cTaxable_Amount,    
                                             cString_Request  => vString_Request,
                                             cHost_Name       => vHost_Name,
                                             cPort            => vPort,
                                             cEndPoint        => vEndPoint); -- out
    
    -- Send Request
    If vInternalErrorCode = 0 then 
      vInternalErrorCode := Send_Vertex_Request(cString_Request => vString_Request,
                                                cHost_Name      => vHost_Name,
                                                cPort           => vPort,
                                                cEndPoint       => vEndPoint,
                                                cHTTP_Request   => vHttp_request);  -- in/out
    End if;    

    -- Get Response
    If vInternalErrorCode = 0 then 
      vInternalErrorCode := Read_Vertex_Response(cHTTP_Request => vHttp_request,
                                                 cXML_Resp     => vXML_Resp);     -- out
    End if;

    If vInternalErrorCode = 0 then 
      -- Clean Vertex SOAP header
      SELECT EXTRACT(vXML_Resp, 'S:Envelope/S:Body/node()', vSOAP_ns)
      INTO vXML_Resp FROM dual;


      If vHttp_response.status_code <> 200 then

        -- Get Fault Code and Vertex Code
        SELECT EXTRACTVALUE(vXML_Resp, '/nsf:Fault/faultcode', vFault_ns),
               EXTRACTVALUE(vXML_Resp, '/nsf:Fault/faultstring', vFault_ns)
        INTO vFaultCode,
             vFaultString
        FROM dual;

        If gTest_Mode_Enabled = 'Y' then
          dbms_output.put_line(' ');
          dbms_output.put_line('FaultCode <'||vFaultCode||'>');
          dbms_output.put_line('FaultString <'||vFaultString||'>');
        End if;

        vInternalErrorCode := 15505; -- WS Response indicates a Fault
        xxcp_foundation.fndwriteerror(vInternalErrorCode,'FaultCode <'||vFaultCode||'> FaultString <'||vFaultString||'>');

        -- ALso Need to get Vertex fault?



      Else


        -- Get Currency
        SELECT EXTRACTVALUE(vXML_Resp, '/VertexEnvelope/InvoiceResponse/Currency/@isoCurrencyCodeAlpha', vVert_ns),
               EXTRACTVALUE(vXML_Resp, '/VertexEnvelope/InvoiceResponse/@documentDate', vVert_ns)
        into   vCurrency_Code,
               vDocumentDate
        from dual;

        --dbms_output.put_line('vCurrency_Code : '||vCurrency_Code);
        --dbms_output.put_line('vDocumentDate : '||vDocumentDate);

        -- Get Total Tax Result
        SELECT EXTRACTVALUE(vXML_Resp, '/VertexEnvelope/InvoiceResponse/SubTotal', vVert_ns),
               EXTRACTVALUE(vXML_Resp, '/VertexEnvelope/InvoiceResponse/TotalTax', vVert_ns),
               EXTRACTVALUE(vXML_Resp, '/VertexEnvelope/InvoiceResponse/Total', vVert_ns)
        INTO   vTaxable,
               vTotalTax,
               vTotal
        from   dual;


        SELECT EXTRACT(vXML_Resp, '//LineItem', vVert_ns)
        INTO vXML_Resp FROM dual;

        -- Get Count of Taxes nodes
        SELECT COUNT(*)
        INTO vCount
        FROM TABLE(xmlsequence(extract(vXML_Resp, '/LineItem/Taxes', vVert_ns))) d;

        If gTest_Mode_Enabled = 'Y' then
          dbms_output.put_line(' ');
          dbms_output.put_line('Taxable  <'||vTaxable||'>');
          dbms_output.put_line('Total    <'||vTotal||'>');
          dbms_output.put_line('TotalTax <'||vTotalTax||'>');
        End If;

        -- Get table of Tax Results

        for i in 1..vCount loop
          SELECT EXTRACTVALUE(vXML_Resp, '/LineItem/Taxes['||i||']/@taxResult', vVert_ns),
                 EXTRACTVALUE(vXML_Resp, '/LineItem/Taxes['||i||']/@taxType', vVert_ns),
                 to_date(vDocumentDate,'YYYY-MM-DD'),
                 EXTRACTVALUE(vXML_Resp, '/LineItem/Taxes['||i||']/@situs', vVert_ns),
                 EXTRACTVALUE(vXML_Resp, '/LineItem/Taxes['||i||']/Jurisdiction', vVert_ns),
                 EXTRACTVALUE(vXML_Resp, '/LineItem/Taxes['||i||']/Jurisdiction/@jurisdictionLevel', vVert_ns),
                 EXTRACTVALUE(vXML_Resp, '/LineItem/Taxes['||i||']/CalculatedTax', vVert_ns),
                 EXTRACTVALUE(vXML_Resp, '/LineItem/Taxes['||i||']/EffectiveRate', vVert_ns),
                 EXTRACTVALUE(vXML_Resp, '/LineItem/Taxes['||i||']/Taxable', vVert_ns),
                 EXTRACTVALUE(vXML_Resp, '/LineItem/Taxes['||i||']/Imposition', vVert_ns),
                 EXTRACTVALUE(vXML_Resp, '/LineItem/Taxes['||i||']/TaxRuleId', vVert_ns),
                 vCurrency_Code,
                 vExtendedPrice      
          INTO   gVert_In_Resp_Rec
          FROM   dual;

          gVert_In_Resp_Tab(i) := gVert_In_Resp_Rec;
          
          -- Total Tax Rate
          vTotal_Tax_Rate := nvl(vTotal_Tax_Rate,0) + nvl(gVert_In_Resp_Rec.EffectiveRate,0);
        End loop;

        If gTest_Mode_Enabled = 'Y' then
          -- Display Tax Breakdown
          For i in 1..gVert_In_Resp_Tab.count loop
            dbms_output.put_line(' ');
            dbms_output.put_line('Taxes <'||i||'>');
            dbms_output.put_line(' taxResult <'||gVert_In_Resp_Tab(i).taxResult||'>');
            dbms_output.put_line(' taxType <'||gVert_In_Resp_Tab(i).taxType||'>');
            dbms_output.put_line(' situs <'||gVert_In_Resp_Tab(i).situs||'>');
            dbms_output.put_line(' Jurisdiction <'||gVert_In_Resp_Tab(i).Jurisdiction||'>');
            dbms_output.put_line(' Jurisdiction Level <'||gVert_In_Resp_Tab(i).JurisdictionLevel||'>');
            dbms_output.put_line(' CalculatedTax <'||gVert_In_Resp_Tab(i).CalculatedTax||'>');
            dbms_output.put_line(' EffectiveRate <'||gVert_In_Resp_Tab(i).EffectiveRate||'>');
            dbms_output.put_line(' Taxable <'||gVert_In_Resp_Tab(i).Taxable||'>');
            dbms_output.put_line(' Imposition <'||gVert_In_Resp_Tab(i).Imposition||'>');
            dbms_output.put_line(' TaxRuleId <'||gVert_In_Resp_Tab(i).TaxRuleId||'>');
          End loop;
        End if;

        -- Insert into XXCP_TAX_RESULTS
        For i in 1..gVert_In_Resp_Tab.count loop
          vInternalErrorCode := populate_tax_results(cTransaction_Date => cTransaction_Date,
                                                     cRecord_Num       => i,
                                                     cTaxable          => vTaxable,
                                                     cTotal_Gross      => vTotal,
                                                     cTotal_Tax        => vTotalTax,
                                                     cTotal_Tax_Rate   => vTotal_Tax_Rate);
          
          If vInternalErrorCode <> 0 then
            exit;
          End if;
        End Loop;

      End if;  -- Error Check
    End if; 

    xxcp_wks.I1009_Array(92) := xxcp_te_base.gTax_Record_Set(1).Tax_Amount;
    xxcp_wks.I1009_Array(93) := xxcp_te_base.gTax_Record_Set(1).Tax_Rate;
    xxcp_wks.I1009_Array(94) := xxcp_te_base.gTax_Record_Set(1).Taxable_Amount;
    xxcp_wks.I1009_Array(95) := xxcp_te_base.gTax_Record_Set(1).Tax_Gross_Amount;
    xxcp_wks.I1009_Array(160):= xxcp_te_base.gTax_Record_Set(1).Taxable_Currency;

    If xxcp_global.Trace_on = 'Y' then
      xxcp_foundation.fndwriteerror(100,'TRACE (Vertex)',gTax_Lookup_Trace);
    End if;
    
    -- Tidy up
    If vHttp_request.private_hndl IS NOT NULL then
      UTL_HTTP.end_request(vHTTP_request);
    End if;

    If vHttp_response.private_hndl IS NOT NULL then
      UTL_HTTP.end_response(vHTTP_response);
    End if;
    
    commit;

    return(vInternalErrorCode);
    
  
  end Control;

end xxcp_vertex_ws;
/
