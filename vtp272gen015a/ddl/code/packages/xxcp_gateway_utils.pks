CREATE OR REPLACE PACKAGE XXCP_GATEWAY_UTILS AS
/******************************************************************************
                        V I R T A L  T R A D E R  L I M I T E D
            (Copyright 2004-2012 Virtual Trader Ltd. All rights Reserved)

   NAME:       XXCP_GATEWAY_UTILS
   PURPOSE:    This package gives clients access to Virtual Trader functionality
               protected from Virtual Trader development.
               
   Version [03.06.12] Build Date [19-NOV-2012] NAME [XXCP_GATEWAY_UTILS]

******************************************************************************/

  gSysdate      Date;
  gTruncSysdate Date;

  Type gBestMatchRec_Type is Record(
    Attribute1  XXCP_cust_data.attribute1%type,
    Attribute2  XXCP_cust_data.attribute1%type,
    Attribute3  XXCP_cust_data.attribute1%type,
    Attribute4  XXCP_cust_data.attribute1%type,
    Attribute5  XXCP_cust_data.attribute1%type,
    Attribute6  XXCP_cust_data.attribute1%type,
    Attribute7  XXCP_cust_data.attribute1%type,
    Attribute8  XXCP_cust_data.attribute1%type,
    Attribute9  XXCP_cust_data.attribute1%type,
    Attribute10 XXCP_cust_data.attribute1%type,
    Attribute11 XXCP_cust_data.attribute1%type,
    Attribute12 XXCP_cust_data.attribute1%type,
    Attribute13 XXCP_cust_data.attribute1%type,
    Attribute14 XXCP_cust_data.attribute1%type,
    Attribute15 XXCP_cust_data.attribute1%type,
    Attribute16 XXCP_cust_data.attribute1%type,
    Attribute17 XXCP_cust_data.attribute1%type,
    Attribute18 XXCP_cust_data.attribute1%type,
    Attribute19 XXCP_cust_data.attribute1%type,
    Attribute20 XXCP_cust_data.attribute1%type,
    Attribute21 XXCP_cust_data.attribute1%type,
    Attribute22 XXCP_cust_data.attribute1%type,
    Attribute23 XXCP_cust_data.attribute1%type,
    Attribute24 XXCP_cust_data.attribute1%type,
    Attribute25 XXCP_cust_data.attribute1%type,
    Attribute26 XXCP_cust_data.attribute1%type,
    Attribute27 XXCP_cust_data.attribute1%type,
    Attribute28 XXCP_cust_data.attribute1%type,
    Attribute29 XXCP_cust_data.attribute1%type,
    Attribute30 XXCP_cust_data.attribute1%type
    );

  TYPE gBestMatchTab_Type is TABLE of gBestMatchRec_Type
       INDEX BY BINARY_INTEGER;

  gBestMatchRec gBestMatchRec_Type;
  gBestMatchTab gBestMatchTab_Type;

  Function  Software_Version return varchar2;

  Procedure Show(cMessage in varchar2);

  Function Get_Source_Assignment_Name(cSource_Assignment_id in number) return varchar2;

  Function Get_Source_Group_by_Name(cSource_Group_Name in varchar2
                                  , cSource_id         in number default Null
                                  , cInstance_id       in number default 0                                  
                                  ) Return Number;

  Function Get_Source_Assignment_by_Name(cSource_Assignment_Name in varchar2
                                       , cSource_id              in number default Null
                                       , cInstance_id            in number default 0
                                       , cApplication_Name       in varchar2) Return Number;

  Function Get_Source_Assignment_by_Name(cSource_Assignment_Name in varchar2
                                       , cSource_id              in number default Null
                                       , cInstance_id            in number default 0) Return Number;

  Function Get_Source_Assignment_by_Book(cSet_of_books_id        in number
                                       , cSource_id              in number default Null
                                       , cInstance_id            in number default 0) Return Number;

  Function Get_Source_Assignment_by_Book(cSet_of_books_id        in number
                                       , cSource_id              in number default Null
                                       , cInstance_id            in number default 0
                                       , cApplication_Name       in varchar2) Return Number;

  Function Get_Source_Assignment_by_Org(
                                         cOrg_id                 in number
                                       , cSource_id              in number default Null
                                       , cInstance_id            in number default 0
                                       , cApplication_Name       in varchar2) Return Number;
                                       
  Function Get_Source_Assignment_by_Org(
                                         cOrg_id                 in number
                                       , cSource_id              in number default Null
                                       , cInstance_id            in number default 0) Return Number;

  Function Get_Grouping_Rule_by_Name(    cGrouping_Rule_Name     in varchar2
                                       , cSource_id              in number) Return Number;
                                       

  Function Assignment_Control_Active(cInstance_id          in number,
                                     cSet_of_books_id      in number,
                                     cSource_Name          in varchar2,
                                     cCategory_Name        in varchar2,
                                     cTransaction_Table    in varchar2,
                                     cTransaction_Type     in out varchar2,
                                     cTransaction_id       in varchar2, -- must be varchar2
                                     cSource_Assignment_id out number)
    Return Boolean;


  Function Attribute_Value(cColumn_Name in varchar2, cAttribute_id in number) return varchar2;

  Function Set_Custom_Data(cAttribute_Number in number,cOriginal_Value in varchar2) return varchar2;

  Function Set_RealTime_Attribute(cAttribute_Number in number,cValue in varchar2) return varchar2;

  Procedure Put_RealTime_Attribute(cAttribute_Number in number,cValue in varchar2);

  Procedure Clear_RealTime_Attributes;

  Function Debug_Write_Trace(cOriginal_Value in varchar2) return varchar2;

  Function Cache_by_id(cSource_id in number, cDynamic_Attribute_id in number, cAttribute_id in number) return varchar2;

  Function Cache_by_id_abs(cDynamic_Attribute_id in number, cAttribute_id in number, cSource_id in number default 0) return varchar2;

  Function Cache_by_Name(cSource_id in number, cDynamic_Attribute_name in varchar2, cAttribute_id in number) return varchar2;

  Function Cache_by_Name_Abs(cDynamic_Attribute_name in varchar2, cAttribute_id in number, cSource_id in number default 0) return varchar2;

  Function Internal_Error_Message(cInternal_Code in number,cLanguage      in varchar2 Default 'English') return varchar2;

  Function Safe_Divide (cValue in number, cQty in number) return number;

  Function Engine_Value(cDynamic_Attribute_Name  in varchar2) return varchar2;

  Function Engine_Value(cDynamic_Attribute_id in number) return varchar2;

  Procedure Set_Engine_Value(cDynamic_Attribute_id in number, cValue in varchar2);

  Procedure Set_Engine_Value(cDynamic_Attribute_Name in varchar2, cValue in varchar2);

  Function User_Preview_id return Number;

  Function Set_Preview_id(cPreview_id in number) return Number;

  Function Full_Sysdate return date;

  Function Truncated_Sysdate return date;

  Function Grouping_Rule_Type(cGrouping_Rule_id in number) return varchar2;

  -- Developed for Tektronix AR for use in the Dynamic Interfaces

  Function AR_Highest_Delivery_id return varchar2;

  Function AR_Highest_Ship_to(cField_Name in varchar2) return varchar2;

  Function AR_Highest_Ship_Actual return varchar2;

  -- End Tektronix Development

  Function Entered_Value_Calculation( cRule_Value_Name           in varchar2,
                                      cEntered_Value             in number,
                                      cAdjustment_Rate           in number,
                                      cTax_Rate                  in number,
                                      cChangeSign                in varchar2,
                                        cTransaction_Quantity      in number,
                                      cPrecision                 in number,
                                      cExtended_Entered_Value   out number,
                                      cInternalErrorCode     in out number) Return Number;

  Function Last_Step_Value( cSource_Assignment_id    in Number,
                            cParent_Trx_id           in Number,
                            cTransaction_Table       in varchar2,
                            cStep                    in varchar2,
                            cCustom_Attribute_Num    in Number) return varchar2;

  Function Last_Step_Values(cSource_Assignment_id    in Number,
                            cParent_Trx_id           in Number,
                            cTransaction_Table       in varchar2,
                            cStep                    in varchar2,
                            --
                            cIC_Unit_Price           out number,
                            cIC_Currency             out varchar2,
                            cAdjustment_Rate         out number,
                            cIC_Tax_Rate             out number,
                            cCustom_Attribute1       out varchar2,
                            cCustom_Attribute2       out varchar2,
                            cCustom_Attribute3       out varchar2,
                            cCustom_Attribute4       out varchar2,
                            cCustom_Attribute5       out varchar2,
                            cCustom_Attribute6       out varchar2,
                            cCustom_Attribute7       out varchar2,
                            cCustom_Attribute8       out varchar2,
                            cCustom_Attribute9       out varchar2,
                            cCustom_Attribute10      out varchar2
                            ) return varchar2;

  FUNCTION LongToCharFromTable( cTable_Name in varchar2, cColumn_Name in varchar2, cRowid in rowid ) return varchar2;

  FUNCTION CurrConv(       cAmount              in number
                         , cCurrency_Code       in varchar2
                         , cPrecision_Indicator in varchar2
                         , cExchange_Date       in Date     default null
                         , cExchange_Type       in varchar2 default null
                         , cTarget_Currency     in varchar2 default null
                         , cLabel               in varchar2 default null
   ) return number;

  Function CurrPrecision(cCurrency_Code          in Varchar2,
                         cUse_Extended_Precision in varchar2 default 'N',
                         cGlobalPrecision        in Number default 2
                         ) return number;

  Function CurrRound    (cAmount                 in Number,
                         cCurrency_Code          in Varchar2,
                         cUse_Extended_Precision in varchar2 default 'N',
                         cGlobalPrecision        in Number default 2
                        ) return number;


  FUNCTION CurrConvRate(   cCurrency_Code       in varchar2
                         , cPrecision_Indicator in varchar2
                         , cExchange_Date       in Date     default null
                         , cExchange_Type       in varchar2 default null
                         , cTarget_Currency     in varchar2 default null
   ) return number;

  FUNCTION CustLookupFetch(cCategory            in varchar2
                         , cCategory_Subset     in varchar2 default null
                         , cCode                in varchar2
                         , cTransaction_Date    in date
                         , cAttribute_Number    in number)
                         return Varchar2 ;

	FUNCTION CustLookupExists(
                           cCategory            in varchar2
												 , cCategory_Subset     in varchar2 default null
	                       , cCode                in varchar2
                         , cTransaction_Date    in date) return boolean;

  Procedure CustLookupFetchGroup(
                           cCategory            in varchar2
                         , cCategory_Subset     in varchar2 default null
                         , cCode                in varchar2
                         , cTransaction_Date    in date
                         , cAttribute1           out varchar2
                         , cAttribute2           out varchar2
                         , cAttribute3           out varchar2
                         , cAttribute4           out varchar2
                         , cAttribute5           out varchar2
                         , cAttribute6           out varchar2
                         , cAttribute7           out varchar2
                         , cAttribute8           out varchar2
                         , cAttribute9           out varchar2
                         , cAttribute10          out varchar2
                         ) ;

  FUNCTION CustLookupCodeFetch(
                           cCategory            in varchar2
                         , cCategory_Subset     in varchar2 default null
                         , cTransaction_Date    in date)
                         return Varchar2 ;

  Function Live_Assignment_Value(cDynamic_Attribute_Name in varchar2 default null,
                                 cDynamic_Attribute_id   in number   default 0) return varchar2;

  Function FindAdjustmentRate( cRate_Set_Name      IN VARCHAR2,
                               cRate_Set_id        IN NUMBER Default 0,
                               cTransaction_Date   IN DATE,
                               cOwner              IN VARCHAR2,
                               cPartner            IN VARCHAR2 default null,
                               cQualifier1         IN VARCHAR2 default null,
                               cQualifier2         IN VARCHAR2 default null
                               ) Return Number;

  Procedure Report_Error_Details(cInternalErrorCode in Number,
                                 cErrorMessage      in varchar2,
                                 cLong_Message      in Long Default Null);

Function CustomData_BestMatch_Ex( cCategory              IN varchar2,
                                   cAttributeNumber       IN number   default 1,
                                   cTransaction_Date      IN date,
                                   cRequest_id            IN number,
                                   cCalling_Program_Name  IN varchar2,
                                   cSource_Assignment_ID  IN Number,
                                   cTransaction_Table     IN varchar2,
                                   cTransaction_Type      IN varchar2,
                                   cTransaction_Ref       IN varchar2,
                                   cTransaction_id        IN Number,
                                   cAttribute1        IN varchar2 default '~',
                                   cAttribute2        IN varchar2 default '~',
                                   cAttribute3        IN varchar2 default '~',
                                   cAttribute4        IN varchar2 default '~',
                                   cAttribute5        IN varchar2 default '~',
                                   cAttribute6        IN varchar2 default '~',
                                   cAttribute7        IN varchar2 default '~',
                                   cAttribute8        IN varchar2 default '~',
                                   cAttribute9        IN varchar2 default '~',
                                   cAttribute10       IN varchar2 default '~',
                                   cAttribute11       IN varchar2 default '~',
                                   cAttribute12       IN varchar2 default '~',
                                   cAttribute13       IN varchar2 default '~',
                                   cAttribute14       IN varchar2 default '~',
                                   cAttribute15       IN varchar2 default '~',
                                   cAttribute16       IN varchar2 default '~',
                                   cAttribute17       IN varchar2 default '~',
                                   cAttribute18       IN varchar2 default '~',
                                   cAttribute19       IN varchar2 default '~',
                                   cAttribute20       IN varchar2 default '~',
                                   cAttribute21       IN varchar2 default '~',
                                   cAttribute22       IN varchar2 default '~',
                                   cAttribute23       IN varchar2 default '~',
                                   cAttribute24       IN varchar2 default '~',
                                   cAttribute25       IN varchar2 default '~',
                                   cAttribute26       IN varchar2 default '~',
                                   cAttribute27       IN varchar2 default '~',
                                   cAttribute28       IN varchar2 default '~',
                                   cAttribute29       IN varchar2 default '~',
                                   cAttribute30       IN varchar2 default '~'
                                   ) return varchar2;
 

  -- *******************************************************************************
  --
  -- CustData_BestMatch
  --
  -- *******************************************************************************
  Function CustomData_BestMatch   (cCategory         IN varchar2,
                                   cAttributeNumber  IN number   default 1,
                                   cTransaction_Date IN date,
                                   cAttribute1       IN varchar2 default '~',
                                   cAttribute2       IN varchar2 default '~',
                                   cAttribute3       IN varchar2 default '~',
                                   cAttribute4       IN varchar2 default '~',
                                   cAttribute5       IN varchar2 default '~',
                                   cAttribute6       IN varchar2 default '~',
                                   cAttribute7       IN varchar2 default '~',
                                   cAttribute8       IN varchar2 default '~',
                                   cAttribute9       IN varchar2 default '~',
                                   cAttribute10      IN varchar2 default '~',
                                   cAttribute11      IN varchar2 default '~',
                                   cAttribute12      IN varchar2 default '~',
                                   cAttribute13      IN varchar2 default '~',
                                   cAttribute14      IN varchar2 default '~',
                                   cAttribute15      IN varchar2 default '~',
                                   cAttribute16      IN varchar2 default '~',
                                   cAttribute17      IN varchar2 default '~',
                                   cAttribute18      IN varchar2 default '~',
                                   cAttribute19      IN varchar2 default '~',
                                   cAttribute20      IN varchar2 default '~',
                                   cAttribute21      IN varchar2 default '~',
                                   cAttribute22      IN varchar2 default '~',
                                   cAttribute23      IN varchar2 default '~',
                                   cAttribute24      IN varchar2 default '~',
                                   cAttribute25      IN varchar2 default '~',
                                   cAttribute26      IN varchar2 default '~',
                                   cAttribute27      IN varchar2 default '~',
                                   cAttribute28      IN varchar2 default '~',
                                   cAttribute29      IN varchar2 default '~',
                                   cAttribute30      IN varchar2 default '~'
                                   ) return varchar2;

  Function CustomData_BestMatch2_Ex(
                                   cCategory              IN varchar2,
                                   cTransaction_Date      IN date,
                                   cRequest_id            IN number,
                                   cCalling_Program_Name  IN varchar2,
                                   cSource_Assignment_ID  IN Number,
                                   cTransaction_Table     IN varchar2,
                                   cTransaction_Type      IN varchar2,
                                   cTransaction_Ref       IN varchar2,
                                   cTransaction_id        IN Number,
                                   cAttribute1       IN varchar2 default '~',
                                   cAttribute2       IN varchar2 default '~',
                                   cAttribute3       IN varchar2 default '~',
                                   cAttribute4       IN varchar2 default '~',
                                   cAttribute5       IN varchar2 default '~',
                                   cAttribute6       IN varchar2 default '~',
                                   cAttribute7       IN varchar2 default '~',
                                   cAttribute8       IN varchar2 default '~',
                                   cAttribute9       IN varchar2 default '~',
                                   cAttribute10      IN varchar2 default '~',
                                   cAttribute11      IN varchar2 default '~',
                                   cAttribute12      IN varchar2 default '~',
                                   cAttribute13      IN varchar2 default '~',
                                   cAttribute14      IN varchar2 default '~',
                                   cAttribute15      IN varchar2 default '~',
                                   cAttribute16      IN varchar2 default '~',
                                   cAttribute17      IN varchar2 default '~',
                                   cAttribute18      IN varchar2 default '~',
                                   cAttribute19      IN varchar2 default '~',
                                   cAttribute20      IN varchar2 default '~',
                                   cAttribute21      IN varchar2 default '~',
                                   cAttribute22      IN varchar2 default '~',
                                   cAttribute23      IN varchar2 default '~',
                                   cAttribute24      IN varchar2 default '~',
                                   cAttribute25      IN varchar2 default '~',
                                   cAttribute26      IN varchar2 default '~',
                                   cAttribute27      IN varchar2 default '~',
                                   cAttribute28      IN varchar2 default '~',
                                   cAttribute29      IN varchar2 default '~',
                                   cAttribute30      IN varchar2 default '~',
                                   cBestMatch_Record OUT gBestMatchRec%type
                                   ) return boolean;




  Function CustomData_BestMatch2  (cCategory         IN varchar2,
                                   cTransaction_Date IN date,
                                   cAttribute1       IN varchar2 default '~',
                                   cAttribute2       IN varchar2 default '~',
                                   cAttribute3       IN varchar2 default '~',
                                   cAttribute4       IN varchar2 default '~',
                                   cAttribute5       IN varchar2 default '~',
                                   cAttribute6       IN varchar2 default '~',
                                   cAttribute7       IN varchar2 default '~',
                                   cAttribute8       IN varchar2 default '~',
                                   cAttribute9       IN varchar2 default '~',
                                   cAttribute10      IN varchar2 default '~',
                                   cAttribute11      IN varchar2 default '~',
                                   cAttribute12      IN varchar2 default '~',
                                   cAttribute13      IN varchar2 default '~',
                                   cAttribute14      IN varchar2 default '~',
                                   cAttribute15      IN varchar2 default '~',
                                   cAttribute16      IN varchar2 default '~',
                                   cAttribute17      IN varchar2 default '~',
                                   cAttribute18      IN varchar2 default '~',
                                   cAttribute19      IN varchar2 default '~',
                                   cAttribute20      IN varchar2 default '~',
                                   cAttribute21      IN varchar2 default '~',
                                   cAttribute22      IN varchar2 default '~',
                                   cAttribute23      IN varchar2 default '~',
                                   cAttribute24      IN varchar2 default '~',
                                   cAttribute25      IN varchar2 default '~',
                                   cAttribute26      IN varchar2 default '~',
                                   cAttribute27      IN varchar2 default '~',
                                   cAttribute28      IN varchar2 default '~',
                                   cAttribute29      IN varchar2 default '~',
                                   cAttribute30      IN varchar2 default '~',
                                   cBestMatch_Record OUT gBestMatchRec%type
                                   ) return boolean;

  Function CustomData_Weighting   (cCategory         IN varchar2,
                                   cAttributeNumber  IN number   default 1,
                                   cTransaction_Date IN date,
                                   cAttribute1       IN varchar2 default '~',
                                   cAttribute2       IN varchar2 default '~',
                                   cAttribute3       IN varchar2 default '~',
                                   cAttribute4       IN varchar2 default '~',
                                   cAttribute5       IN varchar2 default '~',
                                   cAttribute6       IN varchar2 default '~',
                                   cAttribute7       IN varchar2 default '~',
                                   cAttribute8       IN varchar2 default '~',
                                   cAttribute9       IN varchar2 default '~',
                                   cAttribute10      IN varchar2 default '~',
                                   cAttribute11      IN varchar2 default '~',
                                   cAttribute12      IN varchar2 default '~',
                                   cAttribute13      IN varchar2 default '~',
                                   cAttribute14      IN varchar2 default '~',
                                   cAttribute15      IN varchar2 default '~',
                                   cAttribute16      IN varchar2 default '~',
                                   cAttribute17      IN varchar2 default '~',
                                   cAttribute18      IN varchar2 default '~',
                                   cAttribute19      IN varchar2 default '~',
                                   cAttribute20      IN varchar2 default '~',
                                   cAttribute21      IN varchar2 default '~',
                                   cAttribute22      IN varchar2 default '~',
                                   cAttribute23      IN varchar2 default '~',
                                   cAttribute24      IN varchar2 default '~',
                                   cAttribute25      IN varchar2 default '~',
                                   cAttribute26      IN varchar2 default '~',
                                   cAttribute27      IN varchar2 default '~',
                                   cAttribute28      IN varchar2 default '~',
                                   cAttribute29      IN varchar2 default '~',
                                   cAttribute30      IN varchar2 default '~'
                                   ) return varchar2;

  Function CustomData_Weighting2  (cCategory         IN varchar2,
                                   cTransaction_Date IN date,
                                   cAttribute1       IN varchar2 default '~',
                                   cAttribute2       IN varchar2 default '~',
                                   cAttribute3       IN varchar2 default '~',
                                   cAttribute4       IN varchar2 default '~',
                                   cAttribute5       IN varchar2 default '~',
                                   cAttribute6       IN varchar2 default '~',
                                   cAttribute7       IN varchar2 default '~',
                                   cAttribute8       IN varchar2 default '~',
                                   cAttribute9       IN varchar2 default '~',
                                   cAttribute10      IN varchar2 default '~',
                                   cAttribute11      IN varchar2 default '~',
                                   cAttribute12      IN varchar2 default '~',
                                   cAttribute13      IN varchar2 default '~',
                                   cAttribute14      IN varchar2 default '~',
                                   cAttribute15      IN varchar2 default '~',
                                   cAttribute16      IN varchar2 default '~',
                                   cAttribute17      IN varchar2 default '~',
                                   cAttribute18      IN varchar2 default '~',
                                   cAttribute19      IN varchar2 default '~',
                                   cAttribute20      IN varchar2 default '~',
                                   cAttribute21      IN varchar2 default '~',
                                   cAttribute22      IN varchar2 default '~',
                                   cAttribute23      IN varchar2 default '~',
                                   cAttribute24      IN varchar2 default '~',
                                   cAttribute25      IN varchar2 default '~',
                                   cAttribute26      IN varchar2 default '~',
                                   cAttribute27      IN varchar2 default '~',
                                   cAttribute28      IN varchar2 default '~',
                                   cAttribute29      IN varchar2 default '~',
                                   cAttribute30      IN varchar2 default '~',
                                   cBestMatch_Record OUT gBestMatchRec%type
                                   ) return boolean;

  Procedure Add_Weighting;
  
 Function CustData(               cCategory         IN varchar2,
                                   cAttributeNumber  IN number   default 1,
                                   cTransaction_Date IN date,
                                   cAttribute1       IN varchar2 default '~',
                                   cAttribute2       IN varchar2 default '~',
                                   cAttribute3       IN varchar2 default '~',
                                   cAttribute4       IN varchar2 default '~',
                                   cAttribute5       IN varchar2 default '~',
                                   cAttribute6       IN varchar2 default '~',
                                   cAttribute7       IN varchar2 default '~',
                                   cAttribute8       IN varchar2 default '~',
                                   cAttribute9       IN varchar2 default '~',
                                   cAttribute10      IN varchar2 default '~',
                                   cAttribute11      IN varchar2 default '~',
                                   cAttribute12      IN varchar2 default '~',
                                   cAttribute13      IN varchar2 default '~',
                                   cAttribute14      IN varchar2 default '~',
                                   cAttribute15      IN varchar2 default '~',
                                   cAttribute16      IN varchar2 default '~',
                                   cAttribute17      IN varchar2 default '~',
                                   cAttribute18      IN varchar2 default '~',
                                   cAttribute19      IN varchar2 default '~',
                                   cAttribute20      IN varchar2 default '~',
                                   cAttribute21      IN varchar2 default '~',
                                   cAttribute22      IN varchar2 default '~',
                                   cAttribute23      IN varchar2 default '~',
                                   cAttribute24      IN varchar2 default '~',
                                   cAttribute25      IN varchar2 default '~',
                                   cAttribute26      IN varchar2 default '~',
                                   cAttribute27      IN varchar2 default '~',
                                   cAttribute28      IN varchar2 default '~',
                                   cAttribute29      IN varchar2 default '~',
                                   cAttribute30      IN varchar2 default '~'
                                   ) return varchar2;

Function CustomData_BestMatch_Children(cCategory         IN varchar2,
                                         cTransaction_Date IN date,
                                         cAttribute1       IN varchar2 default '~',
                                         cAttribute2       IN varchar2 default '~',
                                         cAttribute3       IN varchar2 default '~',
                                         cAttribute4       IN varchar2 default '~',
                                         cAttribute5       IN varchar2 default '~',
                                         cAttribute6       IN varchar2 default '~',
                                         cAttribute7       IN varchar2 default '~',
                                         cAttribute8       IN varchar2 default '~',
                                         cAttribute9       IN varchar2 default '~',
                                         cAttribute10      IN varchar2 default '~',
                                         cAttribute11      IN varchar2 default '~',
                                         cAttribute12      IN varchar2 default '~',
                                         cAttribute13      IN varchar2 default '~',
                                         cAttribute14      IN varchar2 default '~',
                                         cAttribute15      IN varchar2 default '~',
                                         cAttribute16      IN varchar2 default '~',
                                         cAttribute17      IN varchar2 default '~',
                                         cAttribute18      IN varchar2 default '~',
                                         cAttribute19      IN varchar2 default '~',
                                         cAttribute20      IN varchar2 default '~',
                                         cAttribute21      IN varchar2 default '~',
                                         cAttribute22      IN varchar2 default '~',
                                         cAttribute23      IN varchar2 default '~',
                                         cAttribute24      IN varchar2 default '~',
                                         cAttribute25      IN varchar2 default '~',
                                         cAttribute26      IN varchar2 default '~',
                                         cAttribute27      IN varchar2 default '~',
                                         cAttribute28      IN varchar2 default '~',
                                         cAttribute29      IN varchar2 default '~',
                                         cAttribute30      IN varchar2 default '~',
                                         cChild_Tab        OUT gBestMatchTab%type
                                         ) return number;

  Function CustomData_Weighting_Children(cCategory         IN varchar2,
                                         cTransaction_Date IN date,
                                         cAttribute1       IN varchar2 default '~',
                                         cAttribute2       IN varchar2 default '~',
                                         cAttribute3       IN varchar2 default '~',
                                         cAttribute4       IN varchar2 default '~',
                                         cAttribute5       IN varchar2 default '~',
                                         cAttribute6       IN varchar2 default '~',
                                         cAttribute7       IN varchar2 default '~',
                                         cAttribute8       IN varchar2 default '~',
                                         cAttribute9       IN varchar2 default '~',
                                         cAttribute10      IN varchar2 default '~',
                                         cAttribute11      IN varchar2 default '~',
                                         cAttribute12      IN varchar2 default '~',
                                         cAttribute13      IN varchar2 default '~',
                                         cAttribute14      IN varchar2 default '~',
                                         cAttribute15      IN varchar2 default '~',
                                         cAttribute16      IN varchar2 default '~',
                                         cAttribute17      IN varchar2 default '~',
                                         cAttribute18      IN varchar2 default '~',
                                         cAttribute19      IN varchar2 default '~',
                                         cAttribute20      IN varchar2 default '~',
                                         cAttribute21      IN varchar2 default '~',
                                         cAttribute22      IN varchar2 default '~',
                                         cAttribute23      IN varchar2 default '~',
                                         cAttribute24      IN varchar2 default '~',
                                         cAttribute25      IN varchar2 default '~',
                                         cAttribute26      IN varchar2 default '~',
                                         cAttribute27      IN varchar2 default '~',
                                         cAttribute28      IN varchar2 default '~',
                                         cAttribute29      IN varchar2 default '~',
                                         cAttribute30      IN varchar2 default '~',
                                         cChild_Tab        OUT gBestMatchTab%type
                                         ) return number;  
 
  Procedure Clear_Diag(cDiag_num pls_integer default 1);

  Procedure Diag(cText in varchar2 default null, cDiag_num pls_integer default 1);
  
  Function Diag(cText in varchar2 default null, cDiag_num pls_integer default 1) return varchar2;

  Function Diag(cDiag_num pls_integer default 1) return varchar2;

  Function Company_Number(cTax_Reg_id in number) return varchar2;
  
  Function Tax_Reg_Code(cTax_Reg_id in number) return varchar2;
  
  Function Get_Internal_Conversion_Type(cUser_Conversion_Type in varchar2) Return varchar2;
  
  Procedure Set_Long_Reference(cLong_Reference_Number in number, cLong_Data varchar2);

  Procedure Set_RT_Interface(cInterface_id in Number, cColumn_Name in Varchar2, cColumn_Value in Varchar2, cInternalErrorCode out Number);

  Function Get_ExplosionTrxId Return Number;

  Function Get_ExplosionRowid Return Rowid;

  Function Get_InterfaceRowid Return Rowid;

  Function Get_Custom_Line_Number Return Number;  

  Procedure After_Assignment_User_Confirm;  
  
  Function IsNumber(cStr in varchar2) return varchar2;  
  
  Function Preview_Mode return varchar2;
  
  Function Get_Before_Insert_Value(cColumn_Name in varchar2) Return varchar2;
  
  Procedure Set_Invoice_Paid_Flag(cInvoice_Number in varchar2, cInvoice_Type_Code in varchar2, cPayment_Reference in varchar2 default null); 
  
  Function Element_Tax_Rate(cElement_Number in varchar2) return number;
   -- Tax Amount
  Function Element_Tax_Amount(cElement_Number in varchar2) return number;
   
  -- Tax_Rate_Code
  Function Element_Tax_Code(cElement_Number in varchar2) return varchar2;

  Function Element_Tax_Descr(cElement_Number in varchar2) return varchar2;
  
  Procedure Init;
  -- End of Functions
  --
  --
End XXCP_GATEWAY_UTILS;
/
