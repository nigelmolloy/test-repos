CREATE OR REPLACE PACKAGE XXCP_GL_ENGINE AS
  /******************************************************************************
                      V I R T U A L  T R A D E R  L I M I T E D
        (Copyright 2001-2012 Virtual Trader Ltd. All rights Reserved)

     NAME:       XXCP_GL_ENGINE
     PURPOSE:    GL Engine
     
     Version [03.06.24] Build Date [30-OCT-2012] Name [XXCP_GL_ENGINE]

  ******************************************************************************/

  FUNCTION Software_Version RETURN VARCHAR2;

  FUNCTION Control(cSource_Group_id    IN NUMBER,
                   cConc_Request_Id    IN NUMBER,
                   cTable_Group_id     IN NUMBER   DEFAULT 0,
                   cClearDownFrequency IN NUMBER   DEFAULT 0,
                   cDebug_on           IN VARCHAR2 DEFAULT 'N') RETURN NUMBER;
                   
END XXCP_GL_ENGINE;
/
