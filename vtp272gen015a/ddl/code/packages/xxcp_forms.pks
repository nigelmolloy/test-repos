CREATE OR REPLACE PACKAGE XXCP_FORMS AS
  /******************************************************************************
                          V I R T A L  T R A D E R  L I M I T E D
              (Copyright 2003-2012 Virtual Trader Ltd. All rights Reserved)

     NAME:       XXCP_FORMS
     PURPOSE:    This functions and procedures are used by the Forms
     
     Version [03.06.13] Build Date [09-NOV-2012] NAME [XXCP_FORMS]

  ******************************************************************************/

  gStart_Date date;
  gEnd_Date   date;
  gGroup_id   Number;

  Function Software_Version return varchar2;

  Function IsNumber(cStr in varchar2) return varchar2;

  Function IsDate(cStr in varchar2) return varchar2;

  Procedure SetDate(cDateName in varchar2, cDate in date);

  Function GetDate(cDateName in varchar2, cDefaultDate in date) return date;

  Function Adjustment_Comparision(cNew_Rate_Set_id         in number,
                                  cNew_Owner               in varchar2,
                                  cNew_Partner             in varchar2,
                                  cNew_Qualifier1          in varchar2,
                                  cNew_Qualifier2          in varchar2,
                                  cNew_Effective_from_Date in date,
                                  cNew_Effective_to_Date   in date,
                                  cOld_Rate_Set_id         in number,
                                  cOld_Owner               in varchar2,
                                  cOld_Partner             in varchar2,
                                  cOld_Qualifier1          in varchar2,
                                  cOld_Qualifier2          in varchar2,
                                  cOld_Effective_from_Date in date,
                                  cOld_Effective_to_Date   in date,
                                  cInternalErrorCode       in out Number)
    return boolean;

  Function Adjustment_Validation(cRate_Set_id         in number,
                                 cAdjustment_Rate_id  in number,
                                 cOwner               in varchar2,
                                 cPartner             in varchar2,
                                 cQualifier1          in varchar2,
                                 cQualifier2          in varchar2,
                                 cEffective_from_Date in date,
                                 cEffective_to_Date   in date,
                                 cInternalErrorCode   in out Number)
    return Number;

  Function Model_Ctl_Validation( cInsert_or_Update      in varchar2,
                                 cSource_id             in number,
                                 cModel_Ctl_id          in number,
                                 cEntity_Partnership_id in number,
                                 cTransaction_Set_id    in number,
                                 cModel_Name_id         in number,
                                 cTrading_Set_id        in number,
                                 cQualifier1            in varchar2,
                                 cQualifier2            in varchar2,
                                 cQualifier3            in varchar2,
                                 cQualifier4            in varchar2,
                                 cTrx_Qualifier1        in varchar2,
                                 cTrx_Qualifier2        in varchar2,
                                 cEffective_from_Date   in date,
                                 cEffective_to_Date     in date,
                                 cInternalErrorCode     in out Number)
    return Number;

  Function Tax_Code_Validation(cIC_Trad_Tax_Id      in number,
                               cOwner_tax_reg_id    in number,
                               cPartner_tax_reg_id  in number,
                               cTax_Qualifier1      in varchar2,
                               cTax_Qualifier2      in varchar2,
                               cAR_tax_code_id      in number,
                               cAP_tax_code_id      in number,
                               cEffective_from_date in date,
                               cEffective_to_date   in date,
                               cInternalErrorCode   in out Number)
    return number;

  Procedure COA_Structure(cCOA_ID      in number,
                          cInstance_id in number,
                          cMaxSegments out number,
                          cSegmentMap  out varchar2
                          );

  Function Online_help(cWeb_page in varchar2,
                       cProfile  in varchar2 default Null) return varchar2;

  Procedure Add_Sys_Profile;

  Function Get_Interface_Security(cSource_id in number, cInterface_Table in varchar2, cUser_id in number) return varchar2;

  Function Show_Interface_Security(cSecurity_Name in varchar2) return varchar2;

  Procedure Get_record_contents(cTransaction_Table in varchar2,
                                cvt_transaction_id in number,
                                cResults           out varchar2,
                                cInternalErrorCode in out number);

  Procedure Get_record_contents(cTransaction_Table in varchar2,
                                cvt_transaction_id in number,
                                cAll_Data_Columns  in varchar2,
                                cResults           out varchar2,
                                cInternalErrorCode in out number);

  Procedure Edit_Record_Contents(cTable_Name           in varchar2,
                                 cTransaction_table    in varchar2,
                                 cTransaction_type     in varchar2,
                                 cTransaction_id       in number,
                                 cSource_Rowid         in rowid,
                                 cForm_Context         in varchar2,
                                 cInterface_id         in varchar2,
                                 cData_Only            in varchar2,
                                 cSource_id            in number,
                                 cSource_Assignment_id in number,
                                 cUser_id              in number default -1,
                                 cLogin_id             in number default -1,
                                 cInternalErrorCode in out number);
      
  -- 02.04.10 Overload edit_record_contents for preview. 
  Procedure Edit_Record_Contents(cTable_Name        in varchar2,
                                 cTransaction_table in varchar2,
                                 cTransaction_type  in varchar2,
                                 cTransaction_id    in number,
                                 cSource_Rowid      in rowid,
                                 cForm_Context      in varchar2,
                                 cInterface_id      in varchar2,
                                 cPreview_id        in number,
                                 cData_Only         in varchar2,
                                 cSource_id         in number,
                                 cSource_Assignment_id in number,
                                 cUser_id              in number default -1,
                                 cLogin_id             in number default -1,                                 
                                 cInternalErrorCode in out number);                                 

  Function Get_sqlbuildstatements(cSQL_build_id     in Number,
                                  cAttribute_set_id in Number,
                                  cActivity_id      in Number)
    return varchar2;

  Function DI_Column_Definition(cColumn_Definition in varchar2) return varchar2;

  Function Convert_Column_definition(cColumn_Definition in varchar2)
    return varchar2;
  --
  Function Fetch_column(cSQL                in varchar2,
                        cSQLERRM           out varchar2,
                        cInternalErrorCode in out Number) return varchar2;

  Procedure DML_Statement(cSQL               in varchar2,
                          cSQLERRM           out varchar2,
                          cTest_only         in varchar2,
                          cInternalErrorCode in out Number);

  Procedure Execute_Immediate(cSQL               in varchar2,
                              cSQLERRM           out varchar2,
                              cInternalErrorCode in out Number);

  Procedure Execute_Rewind(cSID               in number,
                           cSQL               in varchar2,
                           cSQLERRM           out varchar2,
                           cInternalErrorCode in out Number);
  --
  Function Get_Transaction_Cache(cAttribute_id in number,
                                 cPreview_id   in number,
                                 cOrderBy      in varchar2 default 'ID') Return varchar2;

  Function Execute_immediate_clob(cStatement  in  clob, 
                                  cParse_Only in  varchar2, 
                                  csqlerrm    out varchar2) return boolean;
  -- Overloaded to return rows processed
  Function Execute_immediate_clob(cStatement      in  clob, 
                                  cParse_Only     in  varchar2, 
                                  csqlerrm        out varchar2,
                                  cRows_Processed out number) return boolean;

  Function Execute_Immediate(cStatement in varchar2, cSQLERRM out varchar2) Return Boolean;

  Function Execute_Immediate_Into(cSQL               in varchar2,
                                  cSQLERRM           out varchar2,
                                  cInternalErrorCode in out Number) return varchar2;

  Function Check_security(cCheck_String in Varchar2) Return Boolean;

  Function Set_security(cInput_number in number) Return Varchar2;

  Function Preview_Mode_Transaction(cPreview_id in number, cHeader_id in number) return boolean;

  Procedure Make_User_Profile(cUser_id in number);

  Function Make_History_Ctl(cTable_name in varchar2) return boolean;

  Function Administer_User_Profile(cUser_id            in number,
                                   cUser_Name          in varchar2,
                                   cSource_id          in number,
                                   cVerification       in varchar2,
                                   cEffective_From     in date default sysdate,
                                   cEffective_To       in date,
                                   cRemove_User        in varchar2 default 'N'
                                   ) return boolean;

  Function Administer_Resp_Profile(cResponsibility_id   in number,
                                   cSource_id           in varchar2,
                                   cVerification        in varchar2,
                                   cEffective_From      in date default sysdate,
                                   cEffective_To        in date,
                                   cDisable_User        in varchar2 default 'N',
                                   cRemove_User         in varchar2 default 'N'
                                   ) return boolean;

  Procedure Set_User_Profile(cUser_id in number, cField_Name in varchar2, cValue in varchar2, cType in varchar2, cInternalErrorCode out number);

  Function Get_User_Profile(cUser_id in number, cField_Name in varchar2, cInternalErrorCode out number) return varchar2;

  Function Compile_Custom_Events Return number;

  Function Condition_Test( cField_Value       in  varchar2
                         , cCondition         in  varchar2
                         , cData_Type         in  varchar2
                         , cInternalErrorCode out number) Return boolean;

  Function Excel_Insert(cSource_Assignment_id in number, 
                        cFileid               in Number, 
                        cMapping_Name         in VARCHAR2, 
                        cSQLERRM              OUT VARCHAR2,
                        cCriteria_Set_Id      in number default 0,
                        cRows_Processed       OUT number) return number;

  Procedure Write_ED_Audit(cAudit_reference in varchar2, cUser_id in number default -1, cSQLERRM OUT Varchar2) ;

  Procedure Write_ED_Audit(cUser_id in number default -1, cSQLERRM OUT Varchar2) ;
  
  Function Where_Attribute_Used(cSource_id in number, cAttribute_id in number) return varchar2;

  Function Source_Security(cSource_id in number) return varchar2;

  Function Validate_Apps_Triggers return number;

  Procedure Refresh_Engine_Names;

  Procedure Update_Engine_Names (cNew_Name    IN Varchar2
                               ,cCurrent_Name IN Varchar2
                               ,cRetCode     OUT Number
                               ,cSQLERRM     OUT Varchar2);

  Procedure Update_All_User_Engine_Names;
  
  Procedure Set_Engine_Name(cProgram_id in number, cUser_Name in varchar2);

  Procedure Generate_Instance_Views(cLast_View out varchar2, cInternal_Error_Code out number);
  
  Procedure Refresh_Cross_Source_Mapping;

  Function Get_Sys_Profile(cProfile_Category in varchar2, cProfile_Name in varchar2) Return varchar2;
  
  Procedure Get_Default_Effectivity_Dates(cStartDate out date, cEndDate out date);
  
  Function Create_Edit_Ref_Temp(cAttribute_id in number, cUser_id in number, cPreview_Flag in varchar2) return number;  
  
  Function Read_TrigDef_Clob (cTrigger_Name IN xxcp_trigger_definitions.trigger_name%TYPE,
                              cIndex        IN NUMBER) Return varchar2;

  Function Read_Utils_Clob   (cSource_Id IN xxcp_custom_utils_code.source_id%TYPE,
                              cCode_Name IN xxcp_custom_utils_code.code_name%TYPE, 
                              cIndex     IN NUMBER) Return varchar2;

 Procedure Clear_Utils_Clob  (cSource_Id IN xxcp_custom_utils_code.source_id%TYPE,
                              cCode_Name IN xxcp_custom_utils_code.code_name%TYPE);

 Procedure Append_Utils_Clob (cSource_Id IN xxcp_custom_utils_code.source_id%TYPE,
                              cCode_Name IN xxcp_custom_utils_code.code_name%TYPE, 
                              cString    IN VARCHAR2);  

 Function Read_Queue_Def_Select_Clob (cSource_Id  IN xxcp_queue_definitions.source_id%TYPE,
                                      cSqlBuildId IN xxcp_queue_definitions.sql_build_id%TYPE, 
                                      cIndex      IN NUMBER) Return VARCHAR2;

 Function Read_Queue_Def_Insert_Clob (cSource_Id  IN xxcp_queue_definitions.source_id%TYPE,
                                      cSqlBuildId IN xxcp_queue_definitions.sql_build_id%TYPE, 
                                      cIndex      IN NUMBER) Return VARCHAR2;

 Procedure Append_Queue_Def_Insert_Clob (cSource_Id    IN xxcp_queue_definitions.source_id%TYPE,
                                         cSQL_Build_Id IN xxcp_queue_definitions.sql_build_id%TYPE, 
                                         cString       IN VARCHAR2);

 Procedure Clear_Queue_Def_Clob  (cSource_Id    IN xxcp_queue_definitions.source_id%TYPE,
                                  cSQL_Build_Id IN xxcp_queue_definitions.sql_build_id%TYPE);

 Procedure Clear_Queue_Def_Insert_Clob  (cSource_Id    IN xxcp_queue_definitions.source_id%TYPE,
                                         cSQL_Build_Id IN xxcp_queue_definitions.sql_build_id%TYPE);

 Procedure Append_Queue_Def_Clob (cSource_Id    IN xxcp_queue_definitions.source_id%TYPE,
                                  cSQL_Build_Id IN xxcp_queue_definitions.sql_build_id%TYPE, 
                                  cString    IN VARCHAR2);  
  
 Function Read_Errors_Clob (cError_id  IN number, 
                            cIndex     IN NUMBER) Return varchar2;

 Function Read_PV_Errors_Clob (cError_id  IN number, 
                               cIndex     IN NUMBER) Return varchar2;
                           
 Function Read_WS_Clob (cSource_Id IN xxcp_web_service_ctl.source_id%TYPE,
                        cWS_name   IN xxcp_web_service_ctl.ws_name%TYPE, 
                        cIndex     IN NUMBER) Return varchar2;

 Procedure Clear_WS_Clob (cSource_Id IN xxcp_web_service_ctl.source_id%TYPE,
                          cWS_name   IN xxcp_web_service_ctl.ws_name%TYPE);

 Procedure Append_WS_Clob (cSource_Id IN xxcp_web_service_ctl.source_id%TYPE,
                           cWS_name   IN xxcp_web_service_ctl.ws_name%TYPE, 
                           cString    IN VARCHAR2);
                           
 Function Trim_Form_Max_Label( cText in varchar2, cMax_Size in number) return varchar2;

 PROCEDURE Update_Edit_Interface(cUser_id in number default -1
                                   ,cAudit_Reference in varchar2
                                   ,cInterface_Table_Name in varchar2
                                   ,cSource_Assignment_Id in number
                                   ,cTransaction_Table in varchar2
                                   ,cParent_Trx_Col in varchar2
                                   ,cParent_Trx_Val in varchar2
                                   ,cErr OUT Varchar2);                
 
 Function Submit_Journal_Entry(cJournal_Header_id in number, cUser_id in number) return number;
 
 Procedure Validate_Account_String( cAccount_String       in varchar2,
                                    cChart_of_Accounts_id in number,
                                    cInstance_id          in number,
                                    cSegment1             in out varchar2,
                                    cSegment2             in out varchar2,
                                    cSegment3             in out varchar2,
                                    cSegment4             in out varchar2,
                                    cSegment5             in out varchar2,
                                    cSegment6             in out varchar2,
                                    cSegment7             in out varchar2,
                                    cSegment8             in out varchar2,
                                    cSegment9             in out varchar2,
                                    cSegment10            in out varchar2,
                                    cSegment11            in out varchar2,
                                    cSegment12            in out varchar2,
                                    cSegment13            in out varchar2,
                                    cSegment14            in out varchar2,
                                    cSegment15            in out varchar2,
                                    cAccount_Description  out varchar2,
                                    cErrorMessage         out varchar2,
                                    cInternalErrorCode    out number); 
  -- 02.05.10
  function dbms_sql_execute(cStmt in dbms_sql.varchar2a) return number;
  procedure generate_ff_alias_views(cFile_type_id in number);                                    
  procedure drop_ff_alias_views(cFile_type_id in number);
  function check_ff_view_used(cFile_type_id in number) return varchar2;
  function isKeyword(cStr in varchar2) return boolean;

 Procedure Update_User_Profile_Source(cUser_id                in number,
                                       cSource_id              in number,
                                       cPV_Hist_Order_Type     in Varchar2,
                                       cPV_Hist_Order_Column1  in varchar2,
                                       cPV_Hist_Order_Column2  in varchar2,
                                       cPV_Hist_Order_Column3  in varchar2,
                                       cPV_Hist_Column_Asc     in varchar2,
                                       cPV_Hist_Order_Trx_Flag in varchar2);
                                       
  FUNCTION Clob_Replace (cClob IN CLOB,
                         cWhat IN VARCHAR2,
                         cWith IN VARCHAR2 ) RETURN CLOB;  
 
End XXCP_FORMS;
/
