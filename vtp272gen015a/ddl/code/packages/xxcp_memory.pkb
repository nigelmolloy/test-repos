CREATE OR REPLACE PACKAGE BODY XXCP_MEMORY_PACK IS
/******************************************************************************************************************
                        V I R T A L  T R A D E R  L I M I T E D
          (Copyright 2005 - 2012 Virtual Trader Ltd. All rights Reserved)

   NAME:       XXCP_MEMORY_package
   PURPOSE:    This package loads the global memory arrays

   REVISIONS:
   Ver        Date      Author  Description
   ---------  --------  ------- ------------------------------------
   02.01.00   21/09/04  Keith   First Coding
   02.01.00   12/10/05  Keith   Added Summary
   02.03.00   22/01/06  Keith   Changed loadTaxRegInfo to use target_id from target tables
   02.03.01   14/02/06  Keith   Changed loadsqlbuild to use colum names
                                to help debug as suggested by Chris Williams
                                at Gartner.
   02.03.03   09/11/06  Keith   Ensure no duplicate rows in Process packages
   02.03.04   09/12/06  Keith   auto format date to include cent.
   02.04.00   02/01/07  Keith   Reordered the replacements on CTA
   02.04.01   06/04/07  Keith   Use VT_HISTORY_ID only if specified.
   02.04.02   24/10/07  Keith   updated get_eua_set for single units.
   02.04.03   16/11/07  Keith   changed the get_eua_set function
   02.04.04   21/03/08  Keith   Tax_Reg_Short_Code
   02.05.00   18/05/08  Keith   Now looks for min intance xxcp_column_rules in Get_EUA_Set
   02.05.01   07/07/08  Keith   added vt_transaction_date and vt_source_creation_date to summarization
   02.05.02   20/08/08  Keith   Fixed xxcp_global.gEAU but with Instance zero (Rounding)
   02.05.03   15/12/08  Simon   Added Forecast table into LoadColumnRules.
   02.05.04   31/05/09  Simon   Added CPA_Type.
   02.06.01   01/07/09  Nigel   Made changes for "Common Assignment"
   02.06.02   29/09/09  Simon   Change status in LoadPassThroughRules.
   02.07.03   14/10/09  Nigel   Changed to loadColumnRules for flex summariztion.
   02.07.04   09/12/09  Keith   Added Calc_Legal_Exch_Rate in a common place.
   02.07.05   16/12/09  Simon   Added MakeSingleSQLBuildStatement to regenerate single DIs.
   02.07.06   24/12/09  Simon   Fix MakeSingleSQLBuildStatement and MakeSQLBuildStatements to convert {C} to :C attributes during gen.
   02.07.07   21/05/10  Keith   Added Cross_Validation to COA table and fixed DA trace column name issue
   02.07.08   19/08/10  Simon   Added Additional Journal Target Table Override.
   02.07.09   03/11/10  Nigel   Added xxcp_global.SET_SOURCE_ID(cSource_id) to MakeSingleSQLBuildStatement
   02.07.10   18/11/10  Simon   Fix incorrect order of VT_GROUPING_COLUMNs for summarization.
   02.07.11   02/08/11  Simon   Fix SQLBuildStatements - Bug 636.
   02.07.12   03/09/11  Keith   Added Journal View
   02.07.13   30/11/11  Nigel   Added in file_type_id population.
   02.07.14   03/01/12  Simon   Added API Column Rules Generation.
   02.07.15   23/01/12  Nigel   Added flat file interface to MakeSQLBuildStatement
   03.07.16   16/02/12  Simon   Fixed Clob / Long Issue.
   03.08.17   09/03/12  Mat     SUBSTR'd ATTRIBUTE_NAME (dya cursor) in MakeSQLBuildStatements and MakeSingleSQLBuildStatement to 30 characters
   03.08.18   05/06/12  Keith   Added IC Invoice Interface - Changed LoadColumnRules
   03.08.19   03/05/12  Nigel   OOD Fixes
   03.08.20   27/06/12  Nigel   ICS interface changes.
   03.08.21   18/07/12  Nigel   Changed the population of the settlement interface attribute1..10 columns to be taken from process history attributes1..10 (OOD-201 Issue 11)
                                Changed the way that the settlement grouping_columns1..10 are worked out (OOD-201 Issue 12)
   03.08.22   22/07/12  Keith   Added Position in API Rules Ordering.
   03.08.23   23/07/12  Nigel   Removed reference to xxcp_global.gCOMMON(1) variables in column rules package. 
   03.08.24   24/07/12  Nigel   Fixed issue with SettColRules cursor being called for all columns
   03.08.25   10/08/12  Simon   Fixed ICS generation issue (OOD-242).
   03.08.26   19/09/12  Simon   Added Cost_Plus_Set_id (OOD-293).
   03.08.27   12/10/12  Mat     OOD-349 - Added XXCP_PV_TRANSACTION_REFS_V   
   03.08.28   18/09/12  Nigel   Fixed Column Rules update for Matching/Voucher process
   03.08.29   26/09/12  Nigel   Fixed issue with update column rules and ICS/Summarization. 
   03.08.30   28/09/12  Nigel   Added ICS Approval Restricted attribute (OOD-314)
   03.08.31   26/10/12  Nigel   Added population of Target Type Id for ICS (OOD-362)
   03.08.32   13/11/12  Simon   Fixed Flat File/ICS column rules issue (OOD-548).                                
******************************************************************************************************************/

 Function Software_Version RETURN VARCHAR2 IS
 Begin
   RETURN('Version [03.08.32] Build Date [13-NOV-2012] Name [XXCP_MEMORY_PACK]');
 End Software_Version;
 -- #
 -- #  LoadTaxRegOnlyInfo
 -- #
 PROCEDURE LoadTaxRegOnlyInfo(cClearOnly in varchar2) is
  cursor tix is
      select tax_registration_id, reg_id
            ,r.legal_currency
            ,r.common_exch_curr
            ,r.exchange_rate_type
            ,r.trade_currency
            ,r.adjustment_rate_group,
            r.short_code,
            nvl(r.tax_reg_group_id,0) tax_reg_group_id
        from xxcp_tax_registrations r
       where substr(r.active,1,1) = 'Y'
       order by tax_registration_id;

  tf pls_integer := 0;

 Begin

   xxcp_global.gTRO.delete;

   If cClearOnly = 'N' then

     For rec in tix Loop
       tf := tf + 1;
       xxcp_global.gTRO(tf).Tax_registration_id    := rec.Tax_registration_id;
       xxcp_global.gTRO(tf).Legal_currency         := rec.Legal_currency;
       xxcp_global.gTRO(tf).Trade_currency         := rec.Trade_currency;
       xxcp_global.gTRO(tf).Exchange_rate_type     := rec.Exchange_rate_type;
       xxcp_global.gTRO(tf).Common_exch_currency   := rec.Common_exch_curr;
       xxcp_global.gTRO(tf).Reg_id                 := rec.Reg_id;
       xxcp_global.gTRO(tf).Adjustment_Rate_Group  := rec.Adjustment_Rate_Group;
       xxcp_global.gTRO(tf).Tax_Reg_Short_Code     := Rec.Short_Code;
       xxcp_global.gTRO(tf).Tax_Reg_Group_id       := Rec.Tax_Reg_Group_id;
     End Loop;

   End If;

   xxcp_global.gTRO_Cnt := tf;

 End LoadTaxRegOnlyInfo;

-- !!
-- !! LoadExtraRecordType
-- !!
 PROCEDURE LoadExtraRecordType(cSource_id in number, cClearOnly in varchar2) is

   Cursor c1(pSource_id in number) is
     Select t.TRANSACTION_TABLE, t.EXTRA_RECORD_TYPE
       from xxcp_sys_source_tables t
      where t.SOURCE_ID = pSource_id;

    x pls_integer := 0;

   Begin
     xxcp_global.gSRE_Cnt := 0;
     xxcp_global.gSRE.delete;

     If cClearOnly = 'N' then
       For Rec in c1(cSource_id) loop
         x := x + 1;
         xxcp_global.gSRE(x).transaction_table := rec.transaction_table;
         xxcp_global.gSRE(x).Extra_Record_type := rec.extra_record_type;
       End Loop;
     End If;

   xxcp_global.gSRE_Cnt := x;

 End LoadExtraRecordType;

 -- #
 -- #  FindTaxRegOnlyInfo
 -- #
 PROCEDURE FindTaxRegOnlyInfo( cTax_Reg_id             in number,
                               cLegal_currency        out varchar2,
                               cTrade_Currency        out varchar2,
                               cCommon_Exch_Curr      out varchar2,
                               cExch_Rate_type        out varchar2,
                               cAdjustment_Rate_Group out varchar2,
                               cTax_Reg_Short_Name    out varchar2
                               ) is

  tf     pls_integer := xxcp_global.gTRO_Cnt;
  j      pls_integer := 0;

 Begin
   for j in 1..tf
     loop
      if xxcp_global.gTRO(j).Tax_Registration_Id =  nvl(cTax_Reg_id,0) then
           cLegal_Currency        := xxcp_global.gTRO(j).Legal_currency;
           cTrade_Currency        := xxcp_global.gTRO(j).Trade_currency;
           cCommon_Exch_Curr      := xxcp_global.gTRO(j).Common_exch_currency;
           cExch_Rate_type        := xxcp_global.gTRO(j).Exchange_rate_type;
           cTrade_Currency        := xxcp_global.gTRO(j).Trade_Currency;
           cAdjustment_Rate_Group := xxcp_global.gTRO(j).Adjustment_Rate_Group;
           cTax_Reg_Short_Name    := xxcp_global.gTRO(j).Tax_Reg_Short_Code;
           exit;
      End If;
   End loop;
 End FindTaxRegOnlyInfo;

 -- #
 -- #  Get_Cach_Summary_Pointers
 -- #
 PROCEDURE Get_Cach_Summary_Pointers(cSource_id in number, cClearOnly in varchar2 ) is

  Cursor c1(pSource_id in number) is
    select bpts_id
         , replace(replace(nvl(trx_qualifier1_expr,'NULL'),'}',null),'{',':') QEXP1
         , replace(replace(nvl(trx_qualifier2_expr,'NULL'),'}',null),'{',':') QEXP2
      from xxcp_summary_break_pts
     where source_id = pSource_id
       and (trx_qualifier1_expr is not null
         or trx_qualifier2_expr is not null);

    vSelect_statement  varchar2(32676);
    vColumn_Count      pls_integer := 0;
    vColumn_Positions  varchar2(500);

    vMax_Internal      pls_integer := 0;
    wa                 pls_integer := 0;
    vMax_Dynamic       pls_integer := 0;
    vRow               pls_integer := 0;

    Begin

      vMax_Dynamic  := xxcp_wks.Max_Dynamic_Attribute;
      vMax_Internal := xxcp_wks.Max_Internal_Attribute;

      xxcp_global.gCSP.Delete;

      If cClearOnly = 'N' then
        For rec1 in c1(cSource_id) Loop

          vColumn_Count     := 2;
          vColumn_Positions := Null;

          vSelect_statement := 'Select '||Rec1.QExp1||','||Rec1.QExp2||' from dual';

          xxcp_global.gCSP(vRow+1).Column_Positions := vColumn_Positions;
          xxcp_global.gCSP(vRow+1).MaxArraySize     := 2;
          xxcp_global.gCSP(vRow+1).BPTS_id          := Rec1.bpts_id;

          xxcp_global.gCSP(vRow+1).D0000_Pointers := Null;
          xxcp_global.gCSP(vRow+1).D0000_Highest  := 0;

          -- Internal Array pointers
          xxcp_global.gCSP(vRow+1).I1009_Pointers := Null;
          xxcp_global.gCSP(vRow+1).I1009_Highest  := 0;

          If vColumn_Count > 0 then

            wa := 0;
            For pa in 1..vMax_Dynamic Loop

              If instr(vSelect_Statement,':D'||to_char(pa+1000)) > 0 Then
                wa := wa + 1;
                xxcp_global.gCSP(vRow+1).D0000_Pointers := xxcp_global.gCSP(vRow+1).D0000_Pointers || lpad(pa+1000,4,0);
                If xxcp_global.gCSP(vRow+1).D0000_Highest < wa Then
                 xxcp_global.gCSP(vRow+1).D0000_Highest := wa;
                End If;
              End If;
              If instr(vSelect_Statement,':D'||to_char(pa+2000)) > 0 Then
                wa := wa + 1;
                xxcp_global.gCSP(vRow+1).D0000_Pointers := xxcp_global.gCSP(vRow+1).D0000_Pointers || lpad(pa+2000,4,0);
                If xxcp_global.gCSP(vRow+1).D0000_Highest < wa Then
                 xxcp_global.gCSP(vRow+1).D0000_Highest := wa;
                End If;
              End If;
              If instr(vSelect_Statement,':D'||to_char(pa+3000)) > 0 Then
                wa := wa + 1;
                xxcp_global.gCSP(vRow+1).D0000_Pointers := xxcp_global.gCSP(vRow+1).D0000_Pointers || lpad(pa+3000,4,0);
                If xxcp_global.gCSP(vRow+1).D0000_Highest < wa then
                 xxcp_global.gCSP(vRow+1).D0000_Highest := wa;
                End If;
              End If;

            End loop;

            wa := 0;
            For pa in 1..vMax_Internal Loop
              If instr(vSelect_Statement,':P'||to_char(pa+9000)) > 0 then
                wa := wa + 1;
                xxcp_global.gCSP(vRow+1).I1009_Pointers := xxcp_global.gCSP(vRow+1).I1009_Pointers || lpad(pa+9000,4,0);
                If xxcp_global.gCSP(vRow+1).I1009_Highest < wa then
                  xxcp_global.gCSP(vRow+1).I1009_Highest := wa;
                End If;
              End If;
            End loop;

            vSelect_Statement := rtrim(vSelect_Statement);

            -- Store in Memory
            xxcp_global.gCSP(vRow+1).Select_statement  := vSelect_statement;
            xxcp_global.gCSP(vRow+1).column_count      := vColumn_Count;
            vRow := vRow + 1;

          End If;
        End Loop;
      End If;

      xxcp_global.gCSP_Cnt := vRow;

  End Get_Cach_Summary_Pointers;


 -- #
 -- #  LoadTaxRegInfo
 -- #
 PROCEDURE LoadTaxRegInfo(cClearOnly in varchar2) is

  cursor TI is
    Select   r.tax_registration_id
            ,r.legal_currency
            ,r.reg_id
            ,r.Exchange_Rate_Type
            ,r.Trade_currency
            ,r.common_exch_curr
            ,r.adjustment_rate_group
            -- Target Assignments
            ,t.Acc_segment_value
            ,t.target_type_id
            ,t.Target_set_of_books_id
            ,t.Target_org_id
            ,t.Balancing_segment
            ,t.target_assignment_id
            ,t.zero_flag target_zero_flag
            ,t.TARGET_ACCT_CURRENCY  currency_code
            ,t.Target_coa_id
            ,t.Target_instance_id
            ,t.TARGET_INSTANCE_ID instance_id
            ,t.TARGET_ACCT_CURRENCY sob_currency_code
            ,t.master Master
            ,r.short_code
            ,nvl(r.tax_reg_group_id,0) tax_reg_group_id
        from xxcp_tax_registrations  r,
             xxcp_target_assignments t,
             xxcp_sys_target_types   x
       where r.reg_id                   = t.reg_id
         and substr(r.Active,1,1)       = 'Y'
         and substr(t.Active,1,1)       = 'Y'
         and t.target_type_id           = x.target_type_id
         and x.target_id                = any(select distinct tt.target_id
                                                from xxcp_sys_target_tables tt
                                               where tt.source_id = xxcp_global.gCommon(1).Source_id
                                              Union
                                              select 5 target_id -- No Target
                                                from xxcp_sys_sources
                                               where interfaced = 'N'
                                                 and source_id != 14
                                                 and source_id = xxcp_global.gCommon(1).Source_id
                                             )
    order by r.tax_registration_id, x.master,t.target_type_id;

  t integer := 0;

  TYPE TI_t IS
      TABLE OF TI%rowtype INDEX BY BINARY_INTEGER;

   TIRec  TI_T;

   j  pls_integer;


 Begin

   xxcp_global.gTR.delete;

   If cClearOnly = 'N' Then

     Open TI;
       Fetch TI BULK COLLECT INTO TIRec;
     Close TI;

     For j in 1..TIRec.count Loop
         t := t + 1;
         xxcp_global.gTR(t).Tax_registration_id    := TIRec(j).Tax_registration_id;
         xxcp_global.gTR(t).Target_assignment_id   := TIRec(j).Target_assignment_id;
         xxcp_global.gTR(t).Target_type_id         := TIRec(j).Target_type_id;
         xxcp_global.gTR(t).Target_set_of_books_id := TIRec(j).Target_set_of_books_id;
         xxcp_global.gTR(t).Target_instance_id     := TIRec(j).Instance_id;
         xxcp_global.gTR(t).Trade_currency         := TIRec(j).Trade_currency;
         xxcp_global.gTR(t).Target_org_id          := TIRec(j).Target_org_id;
         xxcp_global.gTR(t).Legal_currency         := TIRec(j).Legal_currency;
         xxcp_global.gTR(t).Acc_segment_value      := TIRec(j).Acc_segment_value;
         xxcp_global.gTR(t).Adjustment_group       := TIRec(j).Adjustment_rate_group;
         xxcp_global.gTR(t).Balancing_segment      := TIRec(j).Balancing_segment;
         xxcp_global.gTR(t).Currency_code          := TIRec(j).Currency_code;
         xxcp_global.gTR(t).Exchange_rate_type     := TIRec(j).Exchange_rate_type;
         xxcp_global.gTR(t).Target_coa_id          := TIRec(j).Target_coa_id;
         xxcp_global.gTR(t).Target_instance_id     := TIRec(j).Target_instance_id;
         xxcp_global.gTR(t).Common_exch_currency   := TIRec(j).Common_exch_curr;
         xxcp_global.gTR(t).Reg_id                 := TIRec(j).Reg_id;
         xxcp_global.gTR(t).SOB_Currency_Code      := TIRec(j).Sob_currency_code;
         xxcp_global.gTR(t).Target_Master          := TIRec(j).Master;
         xxcp_global.gtr(t).Tax_Reg_Short_Code     := TIRec(j).Short_code;

         xxcp_global.gTR(t).Target_Zero_Flag       := TIRec(j).Target_Zero_flag;
         xxcp_global.gTR(t).Tax_Reg_Group_id       := TIRec(j).Tax_Reg_Group_id;
       End Loop;
   End If;

   xxcp_global.gTR_Cnt := t;

 End LoadTaxRegInfo;

 -- #
 -- #  LoadTaxRegInfo
 -- #
 PROCEDURE LoadTaxRegInfoNoTarget(cClearOnly in varchar2) is

  cursor TI is
    Select   r.tax_registration_id
            ,r.legal_currency
            ,r.reg_id
            ,r.Exchange_Rate_Type
            ,r.Trade_currency
            ,r.common_exch_curr
            ,r.adjustment_rate_group
        from xxcp_tax_registrations  r
       where substr(r.Active,1,1)       = 'Y'
    order by r.tax_registration_id;

  t integer := 0;

  TYPE TI_t IS
      TABLE OF TI%rowtype INDEX BY BINARY_INTEGER;

   TIRec  TI_T;

   j  pls_integer;


 Begin

   xxcp_global.gTR.delete;

   If cClearOnly = 'N' then

     Open TI;
       Fetch TI BULK COLLECT INTO TIRec;
     Close TI;

     For j in 1..TIRec.count Loop
         t := t + 1;
         xxcp_global.gTR(t).tax_registration_id    := TIRec(j).tax_registration_id;
         xxcp_global.gTR(t).target_assignment_id   := Null;
         xxcp_global.gTR(t).target_type_id         := Null;
         xxcp_global.gTR(t).target_set_of_books_id := Null;
         xxcp_global.gTR(t).Target_instance_id     := Null;
         xxcp_global.gTR(t).Trade_currency         := TIRec(j).Trade_currency;
         xxcp_global.gTR(t).target_org_id          := Null;
         xxcp_global.gTR(t).legal_currency         := TIRec(j).legal_currency;
         xxcp_global.gTR(t).acc_segment_value      := Null;
         xxcp_global.gTR(t).adjustment_group       := TIRec(j).adjustment_rate_group;
         xxcp_global.gTR(t).balancing_segment      := Null;
         xxcp_global.gTR(t).currency_code          := Null;
         xxcp_global.gTR(t).exchange_rate_type     := TIRec(j).exchange_rate_type;
         xxcp_global.gTR(t).target_coa_id          := Null;
         xxcp_global.gTR(t).target_instance_id     := Null;
         xxcp_global.gTR(t).common_exch_currency   := TIRec(j).common_exch_curr;
         xxcp_global.gTR(t).reg_id                 := TIRec(j).reg_id;
         xxcp_global.gTR(t).SOB_Currency_Code      := Null;
         xxcp_global.gTR(t).Target_Master          := 'Y';

       End Loop;
   End If;

   xxcp_global.gTR_Cnt := t;

 End LoadTaxRegInfoNoTarget;

 -- #
 -- #  LoadCurrencies
 -- #
 PROCEDURE LoadCurrencies(cClearOnly in varchar2) is

  cursor cux is
   select currency_code, precision, derive_effective, derive_type, derive_factor, f.extended_precision
     from fnd_currencies f
    where enabled_flag = 'Y'
      and length(currency_code) = 3
      and not currency_code in ('ANY')
    order by decode(currency_code,'USD',1,'EUR',2,3) ,1; -- Putting USD and EURO at the top.

    cu integer := 0;

  Begin

    xxcp_global.gCURR.delete;

    If cClearOnly = 'N' then

     For rec in cux Loop
       cu := cu + 1;
       xxcp_global.gCURR(cu).Currency_code    := rec.Currency_code;
       xxcp_global.gCURR(cu).Precision        := rec.Precision;
       xxcp_global.gCURR(cu).Ext_precision    := rec.Extended_precision;
       xxcp_global.gCURR(cu).Derive_effective := rec.Derive_effective;
       xxcp_global.gCURR(cu).Derive_type      := rec.Derive_type;
       xxcp_global.gCURR(cu).Derive_factor    := rec.Derive_factor;
     End loop;
    End If;

    xxcp_global.gCURR_Cnt := cu;

 End LoadCurrencies;


 -- #
 -- #  LoadEnabledSegments
 -- #
 PROCEDURE LoadEnabledSegments(cClearOnly in varchar2, cSource_id in number) is

  cursor esD(pSource_id in number) is
   select distinct chart_of_accounts_id coa_id, instance_id
     from xxcp_segment_rules r
     where r.rule_id = any( select c.rule_id
                              from xxcp_account_rules c
                              where c.source_id = pSource_id)
    order by 1,2;

  cursor esx(pcoa_id in number, pinstance_id in number) is
    select segment_num, 'Y' Enabled_flag
      from xxcp_instance_coa_segments_v
     where coa_id      = pCOA_ID
       and instance_id = pInstance_id
     order by segment_num;

  cursor maxseg(pCoa_id in number, pinstance_id in number) is
   select g.MAX_SEGMENTS
   from xxcp_instance_coa_seg_count_v g
    where coa_id      = pCOA_ID
      and instance_id = pInstance_id;

  k              integer := 0;
  es             integer := 0;
  vMax_Segments  number(2) := 0;
  vEnSegments    varchar2(30);

 Begin

   xxcp_global.gEnSegs.delete;

   If cClearOnly = 'N' then

     for recd in esD(cSource_id) Loop

         es := es + 1;
         xxcp_global.gEnSegs(es).coa_id       := recd.coa_id;
         xxcp_global.gEnSegs(es).instance_id  := recd.instance_id;
         vEnSegments := 'NNNNNNNNNNNNNNNNNNNNNNNNNNNNNN';
         k := 0;

         for rec in maxseg(recd.coa_id, recd.instance_id) loop
           vMax_Segments := Rec.Max_Segments;
         end loop;

         xxcp_global.gEnSegs(es).Max_Segments := vMax_Segments;

         for rec in esx(recd.coa_id, recd.instance_id)
          Loop
            k := rec.segment_num;
            If k = 1 then
              vEnSegments := 'YNNNNNNNNNNNNNNNNNNNNNNNNNNNNN';
            Else
              vEnSegments := Substr(vEnSegments,1,K-1)||'Y'||Substr(vEnSegments,K+1,Length(vEnSegments));
            End If;
          End Loop;
        xxcp_global.gEnSegs(es).enabled := Substr(vEnSegments||'NNNNNNNNNNNNNNNNNNNNNN',1,k);
    End Loop;
   End If;
   xxcp_global.gEnSegs_Cnt := es;

 End LoadEnabledSegments;

 -- #
 -- #  LoadSegmentRules
 -- #
 PROCEDURE LoadSegmentRules(cSource_id in number, cClearOnly in varchar2) is

 cursor sgr(pSource_id in number) is
   select
         sr.rule_id
        ,sr.chart_of_accounts_id
        ,sr.segment_number
        ,sr.segment_value
        ,sr.segment_attribute_id
        ,sr.account_set_id
        ,sr.instance_id
        ,null set_name
        ,nvl(sr.validation_type,0) validation_type
        ,nvl(c.cross_validation,'N') cross_validation
       from xxcp_account_rules r,
            xxcp_segment_rules_coa c,
            xxcp_segment_rules sr
     where r.source_id   = pSource_id
       and c.rule_id     = r.rule_id
       and c.instance_id = sr.instance_id
       and sr.rule_id    = c.rule_id
       and sr.chart_of_accounts_id = c.chart_of_accounts_id
       order by sr.rule_id, sr.chart_of_accounts_id, sr.segment_number;

   sr pls_integer := 0;

   TYPE SGR_t IS
      TABLE OF SGR%rowtype INDEX BY BINARY_INTEGER;

   SGRRec  SGR_T;

   j  pls_integer := 0;

Begin

   xxcp_global.gSR.delete;
   xxcp_global.gSR_Cnt := 0;

   If cClearOnly = 'N' then
    -- Load Segment Rules
    Open SGR(cSource_id);
        Fetch SGR BULK COLLECT INTO SGRRec;
    Close SGR;

    For j in 1..SGRRec.Count Loop
          sr := sr + 1;
          xxcp_global.gSR(sr).rule_id                := SGRRec(j).rule_id;
          xxcp_global.gSR(sr).chart_of_accounts_id   := SGRRec(j).chart_of_accounts_id;
          xxcp_global.gSR(sr).segment_number         := SGRRec(j).segment_number;
          xxcp_global.gSR(sr).segment_value          := SGRRec(j).segment_value;
          xxcp_global.gSR(sr).segment_attribute_id   := SGRRec(j).segment_attribute_id;
          xxcp_global.gSR(sr).account_set_id         := SGRRec(j).account_set_id;
          xxcp_global.gSR(sr).instance_id            := SGRRec(j).instance_id;
          xxcp_global.gSR(sr).set_name               := SGRRec(j).set_name;
          xxcp_global.gSR(sr).validation_type        := SGRRec(j).validation_type;
          xxcp_global.gSR(sr).cross_validation       := SGRRec(j).cross_validation;
    End Loop;

   End If;

   xxcp_global.gSR_Cnt := sr;

 End LoadSegmentRules;

 -- #
 -- #    LoadIgnoreClass
 -- #
 PROCEDURE LoadIgnoreClass(cClearOnly in varchar2,cSource_id in number) is

  c  pls_integer := 0;

  cursor CLA(pSource_id in number) is
      select t.transaction_table, c.CLASS transaction_class
        from xxcp_sys_source_tables  t,
             xxcp_sys_source_classes c
       where t.source_table_id = c.source_table_id
         and c.ignored         = 'Y'
         and t.source_id       = pSource_id;

 Begin

    xxcp_global.gCLA.delete;

    If cClearOnly = 'N' then
      For REC in CLA(cSource_id) Loop
          c := c + 1;
          xxcp_global.gCLA(c).Transaction_table := rec.Transaction_table;
          xxcp_global.gCLA(c).Transaction_class := rec.Transaction_class;
      End Loop;
    End If;
    xxcp_global.gCLA_Cnt := c;

 End LoadIgnoreClass;

 --
 -- ExtractHistory
 --

 PROCEDURE SubPackageVars(cStatement        in varchar2
                         ,cDirect_Statement out varchar2) is

   rv                 pls_integer;
   pa                 pls_integer := 0;
   vMaxInternal       integer;
   vInsert_Dummy      varchar2(32676);

 Begin

   vInsert_Dummy   := cStatement;

   Select max(id - ((column_type-1000)*1000)) into rv
     from xxcp_dynamic_attributes_v
    where dynamic_name_id = 1;

   For pa in 1..rv Loop
       -- Assignment
       IF instr(vInsert_Dummy,':D'||to_char(pa+1000)) > 0 THEN
        vInsert_Dummy := Replace(vInsert_Dummy,':D'||to_char(pa+1000),'cD1001_Array('||to_char(pa)||')');
       End If;
       -- IC Pricing
       IF instr(vInsert_Dummy,':D'||to_char(pa+2000)) > 0 THEN
        vInsert_Dummy := Replace(vInsert_Dummy,':D'||to_char(pa+2000),'cD1002_Array('||to_char(pa)||')');
       End If;
       -- Transaction
       IF instr(vInsert_Dummy,':D'||to_char(pa+3000)) > 0 THEN
        vInsert_Dummy := Replace(vInsert_Dummy,':D'||to_char(pa+3000),'cD1003_Array('||to_char(pa)||')');
       End If;
       -- Transaction Pricing
       IF instr(vInsert_Dummy,':D'||to_char(pa+4000)) > 0 THEN
        vInsert_Dummy := Replace(vInsert_Dummy,':D'||to_char(pa+4000),'cD1004_Array('||to_char(pa)||')');
       End If;
       -- Custom Events
       IF instr(vInsert_Dummy,':D'||to_char(pa+5000)) > 0 THEN
        vInsert_Dummy := Replace(vInsert_Dummy,':D'||to_char(pa+5000),'cD1005_Array('||to_char(pa)||')');
       End If;
       -- Column Attributes
       IF instr(vInsert_Dummy,':D'||to_char(pa+6000)) > 0 THEN
        vInsert_Dummy := Replace(vInsert_Dummy,':D'||to_char(pa+6000),'cD1006_Array('||to_char(pa)||')');
       End If;

   End loop;

   -- Internal Parameters
   select max(id)-9000 into vMaxInternal
     from xxcp_dynamic_attributes_v
    where dynamic_name_id = 2;

   For pa in 1..vMaxInternal loop
         vInsert_Dummy := Replace(vInsert_Dummy,':P'||to_char(pa+9000),'cI1009_Array('||to_char(pa)||')');
   End Loop;

   cDirect_Statement := vInsert_Dummy;

  End SubPackageVars;
  -- !!
  PROCEDURE ExtractArray(cAction      in varchar2,
                         cStatement   in varchar2,
                         cMax_array   in number,
                         cPointersCnt out number,
                         cPointers    out varchar2,
                         cCA_Pointers in out varchar2,
                         cArray       out xxcp_dynamic_array) is

    wa            pls_integer := 0;
    vInsert_Dummy varchar2(32676);
    rv            pls_integer := 0;
    pa            pls_integer := 0;

    vPointers        varchar2(4000);
    vCA_Str          varchar2(50);
    vCA_Pointers     varchar2(4000);
    vDArray          xxcp_dynamic_array := xxcp_dynamic_array(' ');

    -- This will build up a string like this
    -- 00010003200130454021

  Begin

    vCA_Pointers  := Null;
    vInsert_Dummy := cCA_Pointers;

    vDArray.Extend(cMax_array);

    If cAction = 'D' then

      rv := 999; --nvl(xxcp_wks.Max_Dynamic_Attribute,0)+nvl(xxcp_wks.Max_Internal_Attribute,0);

      While rv > 0 loop
        pa := rv;

        IF instr(vInsert_Dummy, ':D' || to_char(pa + 1000)) > 0 THEN
          wa := wa + 1;
          vDArray(wa) := pa + 1000;
          vPointers := vPointers || lpad(pa + 1000, 4, 0);
          vInsert_Dummy := Replace(vInsert_Dummy,
                                   ':D' || to_char(pa + 1000),
                                   Null);
        ELSIF instr(vInsert_Dummy, ':D' || to_char(pa + 2000)) > 0 THEN
          wa := wa + 1;
          vDArray(wa) := pa + 2000;
          vPointers := vPointers || lpad(pa + 2000, 4, 0);
          vInsert_Dummy := Replace(vInsert_Dummy,
                                   ':D' || to_char(pa + 2000),
                                   Null);
        ELSIF instr(vInsert_Dummy, ':D' || to_char(pa + 3000)) > 0 THEN
          wa := wa + 1;
          vDArray(wa) := pa + 3000;
          vPointers := vPointers || lpad(pa + 3000, 4, 0);
          vInsert_Dummy := Replace(vInsert_Dummy,
                                   ':D' || to_char(pa + 3000),
                                   Null);
        ELSIF instr(vInsert_Dummy, ':D' || to_char(pa + 4000)) > 0 THEN
          wa := wa + 1;
          vDArray(wa) := pa + 4000;
          vPointers := vPointers || lpad(pa + 4000, 4, 0);
          vInsert_Dummy := Replace(vInsert_Dummy,
                                   ':D' || to_char(pa + 4000),
                                   Null);
        ELSIF Instr(vInsert_Dummy, ':D' || to_char(pa + 5000)) > 0 THEN
          wa := wa + 1;
          vDArray(wa) := pa + 5000;
          vPointers := vPointers || lpad(pa + 5000, 4, 0);
          vCA_Str := to_char(pa + 5000);

          vInsert_Dummy := Replace(vInsert_Dummy,
                                   ':D' || to_char(pa + 5000),
                                   Null);
        ELSIF Instr(vInsert_Dummy, ':D' || to_char(pa + 6000)) > 0 THEN
          wa := wa + 1;
          vDArray(wa) := pa + 6000;
          vPointers := vPointers || lpad(pa + 6000, 4, 0);
          vCA_Str := to_char(pa + 6000);
          If instr(nvl(vCA_Pointers, '~Null~'), vCA_Str) = 0 then
            vCA_Pointers := vCA_Pointers || vCA_Str;
          End If;

          vInsert_Dummy := Replace(vInsert_Dummy,
                                   ':D' || to_char(pa + 6000),
                                   Null);
        END IF;

        rv := rv - 1;
      End loop;
    ElsIf cAction = 'S' then
     -- Summary Columns
      rv := 100;

      While rv > 0 loop
        pa := rv;

        IF instr(vInsert_Dummy, ':S' || to_char(pa + 3000)) > 0 THEN
          wa := wa + 1;
          vDArray(wa) := pa + 3000;
          vPointers := vPointers || lpad(pa + 3000, 4, 0);
          vInsert_Dummy := Replace(vInsert_Dummy,':S' || to_char(pa + 3000),Null);
        END IF;

        rv := rv - 1;
      End loop;
    Else
      -- INTERNAL
      -- Revers is not necessary for this has it uses 1000 range.
      wa := 0;
      For pa in 1 .. cMax_array Loop
        If instr(vInsert_Dummy, ':P' || to_char(pa + 9000)) > 0 then
          wa := wa + 1;
          vDArray(wa) := pa;
          vPointers := vPointers || lpad(pa + 9000, 4, 0);
          vInsert_Dummy := Replace(vInsert_Dummy,':P' || to_char(pa + 9000),Null);
        End If;
      End loop;
    End IF;

    cCA_Pointers := vCA_Pointers;
    cPointersCnt := wa;
    cPointers    := vPointers;
    cArray       := vDArray;

  End ExtractArray;

  --
  -- Load Column Rules
  --
   PROCEDURE LoadColumnRules( cSource_id               in number
                             ,cSource_Table            in varchar2
                             ,cSource_Instance_id      in number
                             ,cTarget_Table            in varchar2
                             ,cRequest_id              in number
                             ,cInternalErrorCode   in out number
    ) is

  vSet_History Number := 0;

   -- This cursor will retrieve all of the columns in alphabetical order.
   Cursor ColRules(pSource_id              in number
                 , pSource_Table           in varchar2
                 , pRule_id                in number
                 -- base Table
                 , pTarget_Table           in varchar2
                 , pTarget_Instance_ID     in number
                 -- Target Table
                 , pOvr_Target_Table       in varchar2
                 , pOvr_Target_Instance_ID in number
                 ) is
    SELECT Q.COLUMN_NAME, NVL(Q.COLUMN_DEFINITION,Q.COLUMN_NAME) COLUMN_DEFINITION, nvl(Q.BALANCING_ELEMENT,'N')
      FROM XXCP_COLUMN_RULES Q
      WHERE Q.TRANSACTION_TABLE = pTarget_Table
        AND Q.INSTANCE_ID       = pTarget_Instance_ID
        AND Q.SOURCE_ID         = pSource_ID
        AND Q.RULE_ID           = pRule_id
        -- 02.07.03 Exclude any summarization columns.
        AND Q.SUMMARIZATION_COLUMN = 'N'
        -- 03.08.21
        -- AND nvl(Q.SETTLEMENT_COLUMN,'N') = 'N'
        --  Matching Columns
        AND Q.COLUMN_NAME = any( SELECT T.COLUMN_NAME
                                   FROM XXCP_TABLE_COLUMNS T
                                  WHERE T.TABLE_NAME  = pOvr_Target_Table
                                    AND T.INSTANCE_ID = pOvr_Target_Instance_ID
                                )
    UNION
    -- COLUMNS FROM SOURCE INTERFACE TABLE
     SELECT T.COLUMN_NAME, T.COLUMN_NAME , 'N'
       FROM XXCP_TABLE_COLUMNS T
      WHERE T.TABLE_NAME  = pTarget_Table
        AND T.INSTANCE_ID = pTarget_Instance_ID
        --  Matching Columns
        AND T.COLUMN_NAME = any( SELECT S.COLUMN_NAME
                                   FROM XXCP_TABLE_COLUMNS S
                                  WHERE S.TABLE_NAME  = pSource_Table
                                    AND S.INSTANCE_ID = 0 -- Source Instance
                                )
        AND T.COLUMN_NAME = any( SELECT T.COLUMN_NAME
                                   FROM XXCP_TABLE_COLUMNS T
                                  WHERE T.TABLE_NAME  = pOvr_Target_Table
                                    AND T.INSTANCE_ID = pOvr_Target_Instance_ID
                                )
        -- ENSURE NO DUPLICATE ROWS
        AND NOT T.COLUMN_NAME = ANY(
             SELECT R.COLUMN_NAME
               FROM XXCP_COLUMN_RULES R
              WHERE R.TRANSACTION_TABLE = pTarget_Table
                AND R.INSTANCE_ID       = pTarget_Instance_ID
                AND R.SOURCE_ID         = pSource_ID
                AND R.RULE_ID           = pRule_id
                -- 02.07.03 Exclude any summarization columns.
                AND R.SUMMARIZATION_COLUMN = 'N'
                -- 03.08.21
                -- AND nvl(R.SETTLEMENT_COLUMN,'N') = 'N'
                --  Matching Columns
                AND R.COLUMN_NAME = any( SELECT T.COLUMN_NAME
                                           FROM XXCP_TABLE_COLUMNS T
                                          WHERE T.TABLE_NAME  = pOvr_Target_Table
                                            AND T.INSTANCE_ID = pOvr_Target_Instance_ID
                                       ))
       Order by 1;

       -- Process History Columns
       Cursor ColHist(pSource_id in number, pTarget_Table in varchar2, pRule_id in number, pInstance_id in number) is
          Select x.column_name , r.column_definition column_definition
            from xxcp_column_rules r,
                 xxcp_history_attributes x
           where r.transaction_table = pTarget_Table
             and r.instance_id       = pInstance_id
             and r.ph_attribute_id   = x.column_id
             and r.source_id         = pSource_id
             and nvl(substr(x.locked,1,1),'N') <> 'Y'
             and r.rule_id           = pRule_id
          UNION
          Select j.history_column_name, nvl(default_value,'Null') Default_Value
            from xxcp_history_ctl j
           where transaction_table = pTarget_Table
             and substr(Mandatory,1,1)  = 'Y'
             and NOT j.history_column_name = any(
                      select x.column_name
                        from xxcp_column_rules r,
                             xxcp_history_attributes x
                       where r.transaction_table = pTarget_Table
                         and r.instance_id       = pInstance_id
                         and r.source_id         = pSource_id
                         and r.rule_id           = pRule_id
                         and r.ph_attribute_id   = x.column_id)
          UNION
         Select x.column_name , r.column_definition column_definition
          from xxcp_column_rules r,
               xxcp_history_attributes x
         where r.transaction_table = pTarget_Table
           and r.ph_attribute_id   = x.column_id
           and r.instance_id       = pInstance_id
           and r.source_id         = pSource_id
           and nvl(substr(x.locked,1,1),'N') <> 'Y'
           and r.rule_id           = 0
           and NOT x.Column_name = any( select q.column_name
                                          from xxcp_column_rules r,
                                               xxcp_history_attributes q
                                         where r.transaction_table = pTarget_Table
                                           and r.instance_id       = pInstance_id
                                           and r.ph_attribute_id   = q.column_id
                                           and r.source_id         = pSource_id
                                           and r.rule_id           = pRule_id
                                           and nvl(substr(q.locked,1,1),'N') <> 'Y')
       order by 1;

       -- This cursor will find the individual rules for the target table
       Cursor DistColRules(pSource_id in number, pTarget_Table in varchar2) is
          Select distinct t.Transaction_table,
                          t.Instance_id,
                          nvl(t.rule_category_1,'*') Rule_Category_1,
                          t.Rule_id,
                          --
                          ib.db_link,
                          nvl(InitCap(t.dml_mode),'Insert') DML_MODE,
                          t.instance_id Target_instance_id ,
                          Decode(w.Target_table_override,Null,t.instance_id,0) Target_instance_override,
                          nvl(w.Target_table_override,t.transaction_table) Target_table_override,
                          Decode(w.Target_table_override,Null,'N','Y') OverRide,
                          w.staged_table,
                          to_char(t.source_id)||q.short_description||to_char(t.instance_id) Rule_Key,
                        --  Decode(t.transaction_table,'GL_INTERFACE',w.summarization,'N') Summarization,
                        -- 02.07.08
                          Case
                            when t.transaction_table = 'GL_INTERFACE' then
                               w.summarization
                            when t.transaction_table in (select profile_value
                                                         from   xxcp_sys_profile
                                                         where  profile_category = 'SZ'
                                                         and    profile_name = 'ADDITIONAL JOURNAL TARGET TABLE') then
                               w.summarization
                            else
                               'N'
                          End Summarization,
                          nvl(t.target_object,'TABLE') target_object,
                          -- 02.07.13
                          f.file_type_id,
                          f.record_type
           from xxcp_column_rules_tables t,
              (select a1.instance_name, a1.instance_id, a1.db_link
                  from xxcp_instance_db_links_v a1
                   union
                   select '*', -1, a2.db_link
                     from xxcp_instance_db_links_v a2
                      where a2.instance_id = 0 ) ib,
                xxcp_table_override_v  w,
                xxcp_sys_target_tables   q,
                xxcp_sys_sources         s,
                xxcp_ff_file_types_v   f
           where t.transaction_table = pTarget_Table
             and t.source_id         = pSource_id
             and t.source_id         = s.source_id
             and t.instance_id       = ib.instance_id
             --
             and t.source_id         = q.source_id
             and t.transaction_table = q.transaction_table
             --
             and t.source_id         = w.source_id(+)
             and t.instance_id       = w.target_instance_id(+)
             and t.transaction_table = w.transaction_table(+)
             --
             and t.transaction_table = f.view_name (+)
           order by decode(t.instance_id,-1,999,t.instance_id), t.transaction_table, nvl(t.rule_category_1,'*') desc; -- order by instance_id is important


    -- 02.07.03 Check which columns the user has specified
    Cursor SummColRules(pSource_id              in number
                       ,pSource_Table           in varchar2
                       ,pRule_id                in number
                       -- base Table
                       ,pTarget_Table           in varchar2
                       ,pTarget_Instance_ID     in number
                       -- Target Table
                       ,pOvr_Target_Table       in varchar2
                       ,pOvr_Target_Instance_ID in number
                       ,pColumn_name            in varchar2
                       ,pRule_category_1        in varchar2
                       ) is
      select R.COLUMN_NAME,
             NVL(R.COLUMN_DEFINITION, R.COLUMN_NAME) COLUMN_DEFINITION
        from xxcp_column_rules_tables q, XXCP_COLUMN_RULES r
       where Q.TRANSACTION_TABLE = pTarget_Table
         AND Q.INSTANCE_ID = pTarget_Instance_ID
         AND Q.SOURCE_ID = pSource_ID
         AND Q.RULE_ID = pRule_id
         AND NVL(Q.RULE_CATEGORY_1, '*') = nvl(pRule_category_1, '*')
         and R.COLUMN_DEFINITION = pColumn_name
         AND R.SUMMARIZATION_COLUMN = 'Y'
         -- 03.08.20
         AND nvl(R.SETTLEMENT_COLUMN,'N') = 'N'      
         --
         AND r.rule_id = q.rule_id
         and r.instance_id = q.instance_id
         and r.transaction_table = q.transaction_table
         and r.source_id = q.source_id;
         
    -- 03.08.20 ICS V4 Settlement Grouping
    Cursor SettColRules(pSource_id              in number
                       ,pSource_Table           in varchar2
                       ,pRule_id                in number
                       -- base Table
                       ,pTarget_Table           in varchar2
                       ,pTarget_Instance_ID     in number
                       -- Target Table
                       ,pOvr_Target_Table       in varchar2
                       ,pOvr_Target_Instance_ID in number
                       ,pColumn_name            in varchar2
                       ,pRule_category_1        in varchar2
                       ) is
      select R.COLUMN_NAME,
             NVL(R.COLUMN_DEFINITION, R.COLUMN_NAME) COLUMN_DEFINITION
        from xxcp_column_rules_tables q, XXCP_COLUMN_RULES r
       where Q.TRANSACTION_TABLE = pTarget_Table
         AND Q.INSTANCE_ID = pTarget_Instance_ID
         AND Q.SOURCE_ID = pSource_ID
         AND Q.RULE_ID = pRule_id
         AND NVL(Q.RULE_CATEGORY_1, '*') = nvl(pRule_category_1, '*')
         -- 03.08.21
         and R.COLUMN_NAME = pColumn_name
         AND R.SETTLEMENT_COLUMN = 'N'
         AND R.SUMMARIZATION_COLUMN = 'N'
         --
         AND r.rule_id = q.rule_id
         and r.instance_id = q.instance_id
         and r.transaction_table = q.transaction_table
         and r.source_id = q.source_id;        
         
    -- 03.08.21 
    Cursor SettColHist(pSource_id in number, pTarget_Table in varchar2, pRule_id in number, pInstance_id in number) is
    Select x.column_name, 
           r.column_definition column_definition
      from xxcp_column_rules r, 
           xxcp_history_attributes x
     where r.transaction_table = pTarget_Table
       and r.instance_id = pInstance_id
       and r.ph_attribute_id = x.column_id
       and r.source_id = pSource_id
       and nvl(substr(x.locked, 1, 1), 'N') <> 'Y'
       and r.rule_id = pRule_id
       -- Process histroy attributes 1..10
       and x.column_id between 38 and 47; 
                        
    --
    Cursor ParentTab(pSource_id in number, pTarget_table in varchar2) is
    select Lookup_Code Parent_Table
      from xxcp_lookups k
     where upper(lookup_type)        = 'COLUMN RULE TABLE MAP'
       and numeric_code              = pSource_id
       and upper(lookup_type_subset) = upper(pTarget_table)
       and k.enabled_flag            = 'Y';

    --
    Cursor TRT(pSource_id in number,pTarget_Table in varchar2 ) is
    select short_description Record_Type, t.history_id_column Process_History_Column
      from xxcp_sys_target_tables t
     where transaction_table = pTarget_Table
       and source_id         = pSource_id;

    -- 02.07.14
   Cursor APIControl(pTarget_table       in varchar2,
                     pSource_id          in number,
                     pTarget_Instance_Id in number) is
     select c.package_name,
            c.object_name,
            c.pragma_autonomous,
            decode(c.success_argument,'RETURN ARGUMENT','RETURN',c.success_argument) success_argument,
            c.success_operator,
            c.success_value,
            decode(ib.db_link,NULL,NULL,'@'||ib.db_link) db_link
     from   xxcp_api_control c,
            xxcp_instance_db_links_v ib
     where  c.api_name    = pTarget_table
     and    c.source_id   = pSource_id
     and    c.instance_id = pTarget_Instance_Id
     --
     and    ib.instance_id = c.instance_id;
   
   -- 03.08.22 added Position in ordering clause
   Cursor APIRules(pSource_id              in number
                 , pSource_Table           in varchar2
                 , pRule_id                in number
                 -- base Table
                 , pTarget_Table           in varchar2
                 , pTarget_Instance_ID     in number
                 -- Target Table
                 , pOvr_Target_Table       in varchar2
                 , pOvr_Target_Instance_ID in number
                 -- API Control
                 , pPackage_Name           in varchar2
                 , pObject_name            in varchar2
                 ) is
    SELECT  A.POSITION,
            Q.COLUMN_NAME, NVL(Q.COLUMN_DEFINITION,'Null') COLUMN_DEFINITION, Q.BALANCING_ELEMENT,
            decode(A.DATA_TYPE,'VARCHAR2','VARCHAR2(4000)',A.DATA_TYPE) data_type, A.API_TYPE, A.IN_OUT
      FROM  XXCP_COLUMN_RULES Q,
            XXCP_API_ARGUMENTS A
      WHERE Q.TRANSACTION_TABLE = pTarget_Table
        AND Q.INSTANCE_ID       = pTarget_Instance_ID
        AND Q.SOURCE_ID         = pSource_ID
        AND Q.RULE_ID           = pRule_id
        -- 02.07.03 Exclude any summarization columns.
        AND Q.SUMMARIZATION_COLUMN = 'N'
        -- 03.08.20
        AND nvl(Q.SETTLEMENT_COLUMN,'N') = 'N' 
        --  Matching Arguments
        AND Q.COLUMN_NAME  = A.ARGUMENT_NAME
        AND A.PACKAGE_NAME = pPackage_Name
        AND A.OBJECT_NAME  = pObject_Name
        AND A.INSTANCE_ID  = pOvr_Target_Instance_ID
    UNION
    -- UNDEFINED ARGUMENTS FROM API
     SELECT T.POSITION,
            T.ARGUMENT_NAME, 'Null' , 'N',
            decode(T.DATA_TYPE,'VARCHAR2','VARCHAR2(4000)',T.DATA_TYPE) data_type, T.API_TYPE, T.IN_OUT
       FROM XXCP_API_ARGUMENTS T
      WHERE T.PACKAGE_NAME = pPackage_Name
        AND T.OBJECT_NAME  = pObject_Name
        AND T.INSTANCE_ID  = pTarget_Instance_ID
        -- ENSURE NO DUPLICATE ROWS
        AND NOT T.ARGUMENT_NAME = ANY(
             SELECT R.COLUMN_NAME
               FROM XXCP_COLUMN_RULES R
              WHERE R.TRANSACTION_TABLE = pTarget_Table
                AND R.INSTANCE_ID       = pTarget_Instance_ID
                AND R.SOURCE_ID         = pSource_ID
                AND R.RULE_ID           = pRule_id
                -- 02.07.03 Exclude any summarization columns.
                AND R.SUMMARIZATION_COLUMN = 'N'
                -- 03.08.20
                AND nvl(R.SETTLEMENT_COLUMN,'N') = 'N' 
                --  Matching Columns
                AND R.COLUMN_NAME = any( SELECT T1.ARGUMENT_NAME
                                           FROM XXCP_API_ARGUMENTS T1
                                          WHERE T1.PACKAGE_NAME = pPackage_Name
                                            AND T1.OBJECT_NAME  = pObject_Name
                                            AND T1.INSTANCE_ID  = pOvr_Target_Instance_ID
                                       ))
       Order by 1;

    vDML_Statement             varchar2(32676);
    vHistory_statement         varchar2(32676);
    vSource_cols               varchar2(32676);
    vTarget_cols               varchar2(32676);
    -- 02.07.03
    vSumm_Source_cols          varchar2(32676);
    vSumm_Target_cols          varchar2(32676);
    -- 03.08.20
    vSett_Source_cols          varchar2(32676);
    vSett_Target_cols          varchar2(32676);
    vSkip_column               boolean := false;

    type vGrouping_columns_tab is table of varchar2(30) index by varchar2(30);
    vGrouping_columns          vGrouping_columns_tab;
    
    -- 03.08.20
    type vSettlement_columns_tab is table of varchar2(30) index by varchar2(30);      
    vSettlement_columns          vSettlement_columns_tab;

    vSumm_grouping_cols        xxcp_transaction_journals.vt_grouping_columns%type;
    idx                        varchar2(30);

    vSource_Table               xxcp_sys_source_tables.transaction_table%type;
    vTarget_table_override_only xxcp_sys_source_tables.transaction_table%type;

    vRecord_Type                xxcp_sys_target_tables.short_description%type;
    vProcess_History_Column     xxcp_sys_target_tables.history_id_column%type := Null;
    vProcess_History_Table      varchar2(30) := 'xxcp_process_history';

    vHist_Source_cols           varchar2(32676);
    vHist_Target_cols           varchar2(32676);

    wa                          pls_integer;
    pa                          pls_integer;
    rv                          pls_integer := 0;
    rt                          pls_integer := xxcp_global.gCR_CNT;
    q                           pls_integer := 0;
    vMax_Internal               pls_integer := 0;
    vMax_Dynamic                pls_integer := 0;
    vFirstLoop                  Boolean;
    vColumn_count               pls_integer;
    vCSA_Cnt                    pls_integer := 0;
    vStatement_Dummy            varchar2(32676);
    vCusAttPointers             varchar2(4000);
    vCustStdPointers            varchar2(4000);
    --
    vAPI_Declare_Stat           varchar2(32676);
    vAPI_Main_Stat              varchar2(32676);
    vAPI_Excep_Stat             varchar2(32676);
    vAPI_Statement              varchar2(32676);
    vAPI_Package_Name           varchar2(30);
    vAPI_Object_Name            varchar2(30);
    vPragma_autonomous          varchar2(1);
    vSuccess_argument           varchar2(30);
    vSuccess_operator           varchar2(30);
    vSuccess_value              varchar2(100);
    vDB_Link                    varchar2(128);

  Cursor HistColName(pTransaction_table in varchar2, pSource_id in number) is
   select history_id_column
     from xxcp_sys_target_tables
    where source_id = pSource_id
      and transaction_table = pTransaction_table;

   Cursor CustATTS(pSource_id in number) is
   select distinct c.dynamic_attribute_id
    from xxcp_column_set_rules c
    where source_id = pSource_id
    Order by 1;

   vTAB  VARCHAR2(50) := chr(9)||chr(9)||chr(9)||chr(9);
   vTAB4 VARCHAR2(50) := chr(9)||chr(9)||chr(9)||chr(9)||chr(9)||chr(9);

  vLineBk      pls_integer := 0;
  vParentTable varchar2(1) := 'N';

  vTarget_instance_override xxcp_sys_target_tables_ovr.target_instance_id%type;
  vTarget_table_override    xxcp_sys_target_tables_ovr.target_table_override%type;

  Begin
    xxcp_global.set_source_id(cSource_id);

   -- Load the Standard Custom Attributes
    vCustStdPointers:= Null;
    For Rec in CustATTS (cSource_id) loop
     vCustStdPointers := vCustStdPointers || To_Char(Rec.Dynamic_Attribute_Id);
    End Loop;

   -- Max Dynamic Attribute
   Select  max(d.ID-(to_number(substr(d.column_type,4,1))*1000))
     into xxcp_wks.Max_Dynamic_Attribute
     from xxcp_dynamic_attributes_v d
   where dynamic_name_id = 1;

   If nvl(xxcp_wks.Max_Dynamic_Attribute,0) < 10 then
     xxcp_wks.Max_Dynamic_Attribute := 30;
   End If;

   -- Max Internal Attribute
   Select max(d.ID-(to_number(substr(d.column_type,4,1))*1000))
     into xxcp_wks.Max_Internal_Attribute
     from xxcp_dynamic_attributes_v d
   where dynamic_name_id = 2;

   -- Setup
   cInternalErrorCode   := 12740;
   vMax_Dynamic         := xxcp_wks.Max_Dynamic_Attribute;
   vMax_Internal        := xxcp_wks.Max_Internal_Attribute;

   -- Process History Column
   For rec in HistColName(cTarget_Table, cSource_id) loop
    vProcess_History_Column := Rec.History_id_column;
   End Loop;

   vCSA_Cnt := xxcp_global.gCSA_Cnt;

   If (cSource_Table is not null) and (cTarget_Table is not Null) and (vProcess_History_Column is not null) then

     -- Find the maximum column_id used in the Dynamic and Internal Array
     vColumn_count := 0;
     For Rec1 in DistColRules(cSource_id,cTarget_Table) Loop

       vGrouping_columns.delete;
       -- get the rule_ids that are needed for this table_name
       vTarget_cols       := Null;
       vSource_cols       := Null;
       vSumm_target_cols  := Null;
       vSumm_source_cols  := Null;
       vSumm_grouping_cols:= Null;
       vDML_Statement     := Null;
       vHist_Target_Cols  := ',';
       vHist_Source_cols  := ',';
       vColumn_count      := 0;
       vParentTable       := 'N';
       -- 03.08.25
       vSett_Source_cols  := null;
       vSett_Target_cols  := null;


       vSource_Table      := cSource_Table;

       For PTRec in ParentTab(cSource_id, Rec1.Transaction_table) Loop
        vSource_Table := PTRec.parent_table;
        vParentTable  := 'Y';
       End Loop;

       For Rec3 in ColHist(cSource_id,cTarget_Table, Rec1.Rule_id, Rec1.Instance_id) Loop

       -- The columns in the list are automatically taken care of
       If REC3.COLUMN_NAME NOT in (
                   'PROCESS_HISTORY_ID',
                   'TARGET_ASSIGNMENT_ID',
                   'ATTRIBUTE_ID',
                   'RECORD_TYPE',
                   'REQUEST_ID',
                   'STATUS',
                   'INTERFACE_ID',
                   'RULE_ID',
                   'TAX_REGISTRATION_ID',
                   'ROUNDING_AMOUNT',
                   'ENTERED_ROUNDING_AMOUNT',
                   'TARGET_INSTANCE_ID',
                   'SOURCE_ACTIVITY',
                   'SOURCE_ID',
                   'MODEL_CTL_ID',
                   'CREATED_BY',
                   'CREATION_DATE') then

           vHist_Target_cols  := vHist_Target_cols||rec3.column_name||',';
           vHist_Source_cols  := vHist_Source_cols||replace(replace(rec3.column_definition,'}',null),'{',':')||',';
         End If;
       End loop;

       If xxcp_global.Preview_on in ('Y','J') then
         vProcess_History_Table := 'xxcp_pv_process_history';
         vHist_Target_cols := vHist_Target_cols ||'preview_id,';
         vHist_Source_cols := vHist_Source_cols ||'xxcp_global.gCommon(1).Preview_id'||',';
       End if;

       vHist_Target_cols := Substr(vHist_Target_cols,1,Length(vHist_Target_cols)-1);
       vHist_Source_cols := Substr(vHist_Source_cols,1,Length(vHist_Source_cols)-1);

       For TRTRec in TRT(cSource_id,cTarget_Table) Loop
         vRecord_Type := TRTRec.Record_Type;
         vProcess_History_Column := nvl(TRTRec.Process_History_Column,vProcess_History_Column);
       End Loop;

       If nvl(xxcp_global.Forecast_on,'N') = 'Y' then          -- CHG20081215SJS
         vProcess_History_Table := 'xxcp_fc_process_history';
         vHistory_Statement :=
         '    Insert into '||vProcess_History_Table||
                    '('||
                    'FORECAST_ID,'||
                    'CPA_TYPE,'||
                    'COST_PLUS_SET_ID,'||
                    'PROCESS_HISTORY_ID,'||
                    'TARGET_ASSIGNMENT_ID,'||
                    'ATTRIBUTE_ID,'||
                    'RECORD_TYPE,'||
                    'REQUEST_ID,'||
                    'STATUS,'||
                    'INTERFACE_ID,'||
                    'RULE_ID,'||
                    'TAX_REGISTRATION_ID,'||
                    'ROUNDING_AMOUNT,'||
                    'ENTERED_ROUNDING_AMOUNT,'||
                    'TARGET_INSTANCE_ID,'||
                    'SOURCE_ACTIVITY,'||
                    'SOURCE_ID,'||
                    'MODEL_CTL_ID,'||
                    'CREATED_BY,'||
                    'CREATION_DATE'||
                     vHist_Target_cols||
                     ')'||chr(10)||
         '     Select xxcp_global.gCommon(1).Forecast_ID,xxcp_global.gCommon(1).CPA_Type,xxcp_wks.gActual_Costs_Rec(1).Cost_Plus_Set_id,:H01,:H02,:H03,:H05,:H06,
                      :H07,:H08,:H09,:H13,:H14,
                      :H15,:H16,''A'','||to_char(cSource_id)||',:H17'||
                      '  ,xxcp_global.User_id, xxcp_global.SystemDate'||vHist_Source_cols||chr(10)||
          '       from '||vSource_Table||' c where c.rowid = :M0';
       else
         vHistory_Statement :=
         '    Insert into '||vProcess_History_Table||
                    '('||
                    'PROCESS_HISTORY_ID,'||
                    'TARGET_ASSIGNMENT_ID,'||
                    'ATTRIBUTE_ID,'||
                    'RECORD_TYPE,'||
                    'REQUEST_ID,'||
                    'STATUS,'||
                    'INTERFACE_ID,'||
                    'RULE_ID,'||
                    'TAX_REGISTRATION_ID,'||
                    'ROUNDING_AMOUNT,'||
                    'ENTERED_ROUNDING_AMOUNT,'||
                    'TARGET_INSTANCE_ID,'||
                    'SOURCE_ACTIVITY,'||
                    'SOURCE_ID,'||
                    'MODEL_CTL_ID,'||
                    'CREATED_BY,'||
                    'CREATION_DATE'||
                     vHist_Target_cols||
                     ')'||chr(10)||
         '     Select :H01,:H02,:H03,:H05,:H06,
                      :H07,:H08,:H09,:H13,:H14,
                      :H15,:H16,''A'','||to_char(cSource_id)||',:H17'||
                      '  ,xxcp_global.User_id, xxcp_global.SystemDate'||vHist_Source_cols||chr(10)||
          '       from '||vSource_Table||' c where c.rowid = :M0';
       end if;

       If vParentTable = 'Y' then
          vHistory_Statement := Replace(vHistory_Statement,' :M0',' :J0');
       End If;

       rt := rt+1;

       xxcp_global.gCR(rt).Direct_History_statement := Null;

       SubPackageVars(vHistory_Statement,xxcp_global.gCR(rt).Direct_History_statement);

       vCusAttPointers := Null;

       ExtractArray ('D' ,vHistory_Statement,vMax_Dynamic
                         ,xxcp_global.gCR(rt).DynamicHistPointersCnt
                         ,xxcp_global.gCR(rt).DynamicHistPointers
                         ,vCusAttPointers
                         ,xxcp_global.gCR(rt).Dynamic_Array);

       ExtractArray ('S' ,vHistory_Statement,vMax_Dynamic
                         ,xxcp_global.gCR(rt).SummaryHistPointersCnt
                         ,xxcp_global.gCR(rt).SummaryHistPointers
                         ,vCusAttPointers
                         ,xxcp_global.gCR(rt).Summary_Array);

       ExtractArray ('P' ,vHistory_Statement,vMax_Internal
                         ,xxcp_global.gCR(rt).InternalHistPointersCnt
                         ,xxcp_global.gCR(rt).InternalHistPointers
                         ,vCusAttPointers
                         ,xxcp_global.gCR(rt).Internal_Array);

       vCSA_Cnt := vCSA_Cnt + 1;
       xxcp_global.gCSA(vCSA_Cnt).Rule_id            := Rec1.Rule_id;
       xxcp_global.gCSA(vCSA_Cnt).Rule_Category      := Rec1.Rule_Category_1;
       xxcp_global.gCSA(vCSA_Cnt).Target_Instance_id := Rec1.Instance_id;
       xxcp_global.gCSA(vCSA_Cnt).Target_Table       := cTarget_Table;
       xxcp_global.gCSA(vCSA_Cnt).Rule_Key           := Rec1.Rule_key;
       xxcp_global.gCSA(vCSA_Cnt).Pointers           := vCustStdPointers||vCusAttPointers;
       xxcp_global.gCSA_Cnt := vCSA_Cnt;

       If Instr(rec1.Target_table_override,'@') > 0 then
         vTarget_table_override_only := Substr(rec1.Target_table_override,1,Instr(rec1.Target_table_override,'@')-1);
       Else
         vTarget_table_override_only := rec1.Target_table_override;
       End If;

       Q := 0;

       vLineBk      := 0;
       vSet_History := 0;

       vTarget_instance_override := rec1.Target_instance_override;
       vTarget_table_override    := rec1.Target_table_override;

       If Rec1.Summarization = 'Y' and nvl(xxcp_global.settlement_on,'N') = 'N' then
        vTarget_instance_override   := 0;
        vTarget_table_override_only := 'XXCP_TRANSACTION_JOURNALS';
        vTarget_table_override      := 'XXCP_TRANSACTION_JOURNALS';
       End If;
       
       -- 03.08.20 
       If nvl(xxcp_global.settlement_on,'N') = 'Y' then 
         -- 
         if xxcp_global.Preview_on = 'N' then 
           vTarget_table_override_only := 'XXCP_IC_SETTLEMENT_INTERFACE';
           vTarget_instance_override   := 0;
           vTarget_table_override      := 'XXCP_IC_SETTLEMENT_INTERFACE';
           vProcess_History_Column     := 'PROCESS_HISTORY_ID';
         else 
           vTarget_table_override_only := 'XXCP_PV_IC_SETTLEMENT_IFACE';
           vTarget_instance_override   := 0;
           vTarget_table_override      := 'XXCP_PV_IC_SETTLEMENT_IFACE';
           vProcess_History_Column     := 'PROCESS_HISTORY_ID';
         end if;
       end if;  

       -- 02.07.13
       -- If writing to stage flat file then set the vProcess_History_Column to be null as
       -- the column is automatically populated
       if rec1.OverRide = 'Y' and rec1.Staged_table = 'Y' and rec1.file_type_id is not null then
         vProcess_History_Column := null;
       end if;

       -- 02.07.10
       -- SJS Fix to re-order vt_grouping_columns in correct order (column_definition)
       update xxcp_column_rules a
       set    a.column_name = (select new_col_name from
                                (select 'VT_GROUPING_COLUMN'||row_number() over (order by column_definition) new_col_name,
                                         rowid
                                 from  xxcp_column_rules b
                                 where b.summarization_column = 'Y'
                                 and b.transaction_table = cTarget_Table
                                 and b.rule_id = rec1.rule_id
                                 and b.instance_id = rec1.Target_instance_id
                                 and b.source_id = cSource_id
                                )
                               where a.rowid = rowid
                              )
       where a.summarization_column = 'Y'
       and a.transaction_table = cTarget_Table
       and a.rule_id = rec1.rule_id
       and a.instance_id = rec1.Target_instance_id
       and a.source_id = cSource_id;
       
       -- 03.08.20
       -- NCM Fix to re-order vt_grouping_columns in correct order (column_definition)
       update xxcp_column_rules a
       set    a.column_name = (select new_col_name from 
                                (select 'GROUPING_ATTRIBUTE'||row_number() over (order by column_definition) new_col_name,
                                         rowid
                                 from  xxcp_column_rules b
                                 where b.settlement_column = 'Y'
                                 and b.transaction_table = cTarget_Table
                                 and b.rule_id = rec1.rule_id
                                 and b.instance_id = rec1.Target_instance_id
                                 and b.source_id = cSource_id
                                )
                               where a.rowid = rowid 
                              )
       where a.settlement_column = 'Y'
       and a.transaction_table = cTarget_Table
       and a.rule_id = rec1.rule_id
       and a.instance_id = rec1.Target_instance_id
       and a.source_id = cSource_id;   

       --
       -- TARGET TABLE:
       -- Make Column Rules

       If rec1.target_object = 'TABLE' then

--
-- Start Standard Column Rules
--

       If cTarget_Table != 'XXCP_IC_INVOICE_INTERFACE' Then
         For Rec2 in ColRules(cSource_id
                             ,vSource_Table
                             ,rec1.rule_id -- column rules id
                             -- Target
                             ,cTarget_Table
                             ,rec1.Target_instance_id
                             -- Override
                             ,vTarget_table_override_only
                             ,vTarget_instance_override)
         Loop

           Q := Q +1;
           
            vSkip_column := false;
           
           -- 03.06.18
           -- For seeded target tables that have a number of pre-populated columns we don't want to 
           -- copy from the source table, so if the column name matches any of the columns below then 
           -- skip the record.
           -- 
           if (vTarget_table_override in ('XXCP_STAGED_FLAT_FILE','XXCP_STAGED_JOURNALS')
               or
               vTarget_table_override like 'XXCP_FFO%'
              ) then 
             
             If rec2.column_name in ('VT_RECORD_TYPE','VT_FILE_TYPE_ID','VT_REQUEST_ID',
                                     'VT_INSTANCE_ID','VT_SOURCE_ASSIGNMENT_ID','VT_TRANSACTION_DATE', 
                                     'VT_SOURCE_CREATION_DATE') then 
               vSkip_column := true;                                           
             end if;        
                                            
           elsif vTarget_table_override in ('XXCP_IC_SETTLEMENT_INTERFACE','XXCP_PV_IC_SETTLEMENT_IFACE') then 
           
             If rec2.column_name in ('EXTERNAL_SOURCE','SOURCE_ASSIGNMENT_ID','PARENT_TRX_ID',
                                     'OWNER_TAX_REG_ID','PARTNER_TAX_REG_ID','TRANSACTION_CLASS',
                                     'TRANSACTION_TABLE','TRANSACTION_TYPE','TRANSACTION_REF1',
                                     'TRANSACTION_ID','TRADING_SET','ACCOUNTED_CURRENCY','AP_AR_IND',
                                     'CLASSIFICATION','TRANSACTION_REF2','TRANSACTION_REF3',
                                     'ATTRIBUTE_ID','INTERFACE_ID','HEADER_ID','RULE_ID', 
                                     'SOURCE_ID', 'ENTITY_TYPE', 'TRANSACTION_DATE', 
                                     'RULE_NAME', 'COA_ID', 'TARGET_TYPE_ID','RECORD_TYPE','INSTANCE_ID','REQUEST_ID',
                                     -- 03.08.21 Omit target table attribute columns 1..10
                                     'ATTRIBUTE1','ATTRIBUTE2','ATTRIBUTE3','ATTRIBUTE4','ATTRIBUTE5',
                                     'ATTRIBUTE6','ATTRIBUTE7','ATTRIBUTE8','ATTRIBUTE9','ATTRIBUTE10',
                                     -- 03.08.32
                                     'STATUS','DATE_CREATED','CREATED_BY','ACCOUNTING_DATE','CURRENCY_CODE') then 
               vSkip_column := true;                                           
             end if;
           else 
             vSkip_column := false;
           end if;

           -- 02.07.13
           -- The columns in the list are automatically taken care of for staged tables
           If not vSkip_column then
           
           -- 03.08.21 Don't do standard column processing for PROCESS HISTORY, or now VT_GROUPING_COLUMNx
           If ((rec2.column_name <>  'PROCESS HISTORY') and (substr(rec2.column_name,1,18) <> 'GROUPING_ATTRIBUTE')) then
            vColumn_count := vColumn_count + 1;

            If upper(rec1.DML_Mode) = 'INSERT' then

              If vLineBk >= 5 then
                vLineBk := 0;
                vTarget_cols  := vTarget_Cols||chr(10)||vTAB4||rec2.column_name||',';
                If rec2.column_name = vProcess_History_Column then
                  vSet_History := 1;
                  vSource_cols := vSource_Cols||chr(10)||vTAB4||':M15,';  -- Process History Column
                Else
                  vSource_cols := vSource_Cols||chr(10)||vTAB4||replace(replace(rec2.column_definition,'}',null),'{',':')||',';
                End If;

              Else
                vTarget_cols  := vTarget_Cols||rec2.column_name||',';
                If rec2.column_name = vProcess_History_Column then
                  vSource_cols := vSource_Cols||':M15,';  -- Process History Column
                  vSet_History := 1;
                Else
                  vSource_cols := vSource_Cols||replace(replace(rec2.column_definition,'}',null),'{',':')||',';
                End If;
                vLineBk := vLineBk + 1;
              End If;

            ElsIf upper(rec1.DML_Mode) = 'UPDATE' then
              -- Update Mode
              vTarget_cols  := Null;
              -- 03.06.26
              If rec2.column_name = vProcess_History_Column then
                vSource_cols := vSource_Cols||rec2.column_name||' = '||':M15,';  -- Process History Column
                vSet_History := 1;
              Else
                vSource_cols := vSource_Cols||rec2.column_name||' = '||case when rec2.column_definition is null then rec2.column_name else replace(replace(rec2.column_definition,'}',null),'{',':') end ||',';
              End If;
            End If;
            -- get the column rules columns
           End If;

           end if;
           -- 02.07.03 Checks to see if the column is part of summarization.
           for rec4 in SummColRules(cSource_id
                                   ,vSource_Table
                                   ,rec1.rule_id -- column rules id
                                   -- Target
                                   ,cTarget_Table
                                   ,rec1.Target_instance_id
                                   -- Override
                                   ,vTarget_table_override_only
                                   ,vTarget_instance_override
                                   ,rec2.column_name
                                   ,rec1.rule_category_1) loop

              -- Store the names of the columns being additionally grouped (In alphabetical order).
              vGrouping_columns(rec2.column_name) := rec2.column_name;         -- 02.07.10
              vSumm_target_cols := vSumm_target_cols||','||rec4.column_name;
              vSumm_Source_cols := vSumm_Source_cols||','||chr(10)||vTAB4||replace(replace(rec2.column_definition,'}',null),'{',':');
           end loop;
           
           -- 03.08.24 SettColRules should ONLY be called for ICS GROUPING_ATTRIBUTE columns.
           if substr(rec2.column_name,1,18) = 'GROUPING_ATTRIBUTE' then
             
             -- 03.08.20
             for rec5 in SettColRules(cSource_id
                                   ,vSource_Table
                                   ,rec1.rule_id -- column rules id
                                   -- Target
                                   ,cTarget_Table
                                   ,rec1.Target_instance_id
                                   -- Override
                                   ,vTarget_table_override_only
                                   ,vTarget_instance_override
                                   -- 03.08.21 changed it from column_name
                                   ,rec2.column_definition
                                   ,rec1.rule_category_1) loop
              
              -- 03.08.21 Changed code to return re2.column_name instead of rec5.column_name
              vGrouping_columns(rec5.column_name) := rec2.column_name;
              vSett_target_cols := vSett_target_cols||','||rec2.column_name;
              vSett_Source_cols := vSett_Source_cols||','||chr(10)||vTAB4||replace(replace(rec5.column_definition,'}',null),'{',':');
             end loop;   
           
           end if;
           
         End Loop;
       ElsIf cTarget_Table = 'XXCP_IC_INVOICE_INTERFACE' then
         
         vColumn_count := vColumn_count + 3;

         vSource_cols := vSource_Cols||chr(10)||vTAB4||'cI1009_Array(35),cI1009_Array(58),VT_INTERFACE_ID,';  -- Attribute id
         vTarget_cols  := vTarget_Cols||chr(10)||vTAB4||'VT_ATTRIBUTE_ID,VT_TRANSACTION_RULE_ID,VT_SOURCE_INTERFACE_ID,';

           
         For Rec2 in ColRules(cSource_id
                             ,vSource_Table
                             ,rec1.rule_id -- column rules id
                             -- Target
                             ,cTarget_Table
                             ,rec1.Target_instance_id
                             -- Override
                             ,vTarget_table_override_only
                             ,vTarget_instance_override)
         Loop

           Q := Q +1;
           
           -- 02.07.13
           -- The columns in the list are automatically taken care of for staged tables
           If rec2.column_name not in ('VT_RECORD_TYPE',
                                       'VT_CREATED_DATE',
                                       'VT_INTERFACE_ID',
                                       'VT_STATUS',
                                       'VT_TRANSACTION_RULE_ID',
                                       'VT_SOURCE_INTERFACE_ID',
                                       'VT_FILE_TYPE_ID') then

           If rec2.column_name <>  'PROCESS HISTORY' then
              vColumn_count := vColumn_count + 1;

              If vLineBk >= 5 then
                vLineBk := 0;
                
                If rec2.column_name = 'VT_INTERFACE_ID' then -- swap out
                   vTarget_cols  := vTarget_Cols||chr(10)||vTAB4||'VT_SOURCE_INTERFACE_ID,';
                Else
                   vTarget_cols  := vTarget_Cols||chr(10)||vTAB4||rec2.column_name||',';
                End If;
                
                If rec2.column_name = 'VT_ATTRIBUTE_ID' then                    
                  vSource_cols := vSource_Cols||chr(10)||vTAB4||'cI1009_Array(35),';  -- Attribute id
                ElsIf rec2.column_name = vProcess_History_Column then
                  vSet_History := 1;
                  vSource_cols := vSource_Cols||chr(10)||vTAB4||':M15,';  -- Process History Column
                Else
                  vSource_cols := vSource_Cols||chr(10)||vTAB4||replace(replace(rec2.column_definition,'}',null),'{',':')||',';
                End If;

              Else
                vTarget_cols  := vTarget_Cols||rec2.column_name||',';
                If rec2.column_name = vProcess_History_Column then
                  vSource_cols := vSource_Cols||':M15,';  -- Process History Column
                  vSet_History := 1;
                Else
                  vSource_cols := vSource_Cols||replace(replace(rec2.column_definition,'}',null),'{',':')||',';
                End If;
                vLineBk := vLineBk + 1;
              End If;

             -- get the column rules columns
             End If;
           End If;
         End Loop;

       End If;
              
       If vSet_History != 1 and vProcess_History_Column is not null then
            vTarget_cols := vTarget_cols||vProcess_History_Column||',';
            vSource_cols := vSource_Cols||':M15,';  -- Process History Column
       End If;

       vFirstLoop := False;
       vLineBk := 0;

       -- Write VT Request Id to target table
       If rec1.OverRide = 'Y' and rec1.Staged_table = 'Y' and nvl(xxcp_global.settlement_on,'N') = 'N' then
            vTarget_cols := vTarget_cols||'vt_request_id,vt_instance_id, vt_source_assignment_id,vt_process_history_id, vt_transaction_date, vt_source_creation_date ';
            vSource_cols := vSource_Cols||'xxcp_global.gCommon(1).current_request_id,cTarget_Instance_id,xxcp_global.gCommon(1).current_assignment_id, cProcess_history_id, cD1001_Array(1), cI1009_Array(104) ';  -- Process History Column

            -- 02.07.13
            -- If the table is a FFO view then include file type Id, and record_type
            if rec1.file_type_id is not null then
              vTarget_cols := vTarget_cols||',vt_file_type_id ';
              vSource_cols := vSource_Cols||','||rec1.file_type_id||' ';
              -- A record type may not be used.
              if rec1.record_type is not null then
                vTarget_cols := vTarget_cols||',vt_record_type ';
                vSource_cols := vSource_Cols||','''||rec1.record_type||''' ';
              end if;
            end if;

            If rec1.Summarization = 'Y' and nvl(xxcp_global.settlement_on,'N') = 'N' then

              -- 02.07.03 Add the vt_grouping_columns to the xxcp_transaction_journals insert.
              vTarget_cols := vTarget_cols||vSumm_target_cols;
              vSource_cols := vSource_Cols||vSumm_Source_cols;

              -- 02.07.03 Include the grouping columns in the xxcp_transaction_journals insert.
              vTarget_cols := vTarget_cols||',VT_GROUPING_COLUMNS ';

              -- 02.07.03 loop through the vGrouping_columns table to populate the values for VT_GROUPING_COLUMNS
              idx := vGrouping_columns.First;
              while idx is not null
              loop
                -- Comma separate the grouping columns.
                vSumm_grouping_cols := vSumm_grouping_cols||case when length(vSumm_grouping_cols) > 0 then ',' end||vGrouping_columns(idx);
                idx := vGrouping_columns.Next(idx);
              end loop;

              -- 02.07.03
              vSource_cols := vSource_Cols||','''||vSumm_grouping_cols||'''';
              vTarget_cols := vTarget_cols||',VT_ACCOUNTED_CURRENCY_CODE ';
              vSource_cols := vSource_Cols||',cI1009_Array(56) ';  -- Process History Column
            End if;
         End If;
         
         -- 03.08.20
         If nvl(xxcp_global.settlement_on,'N') = 'Y' then
           
           -- 03.08.21 Populates IC settlement interface attribute1..10 from process history attribute1..10
           For Rec6 in SettColHist(cSource_id,cTarget_Table, Rec1.Rule_id, Rec1.Instance_id) Loop       

             vSett_target_cols := vSett_target_cols||','||rec6.column_name;
             vSett_Source_cols := vSett_Source_cols||','||chr(10)||vTAB4||replace(replace(rec6.column_definition,'}',null),'{',':');             
             
           End Loop;

            -- 03.08.32
            -- STATUS, DATE_CREATED, CREATED_BY, ACCOUNTING_DATE, CURRENCY_CODE
  
            vTarget_cols := vTarget_cols||'EXTERNAL_SOURCE,SOURCE_ASSIGNMENT_ID,PARENT_TRX_ID,OWNER_TAX_REG_ID,PARTNER_TAX_REG_ID,TRANSACTION_CLASS,TRANSACTION_TABLE,TRANSACTION_TYPE,TRANSACTION_REF1,TRANSACTION_ID,TRADING_SET,ACCOUNTED_CURRENCY,AP_AR_IND,CLASSIFICATION,TRANSACTION_REF2,TRANSACTION_REF3,ATTRIBUTE_ID,INTERFACE_ID,HEADER_ID,RULE_ID, SOURCE_ID, ENTITY_TYPE, TRANSACTION_DATE, RULE_NAME, COA_ID, RECORD_TYPE,INSTANCE_ID,REQUEST_ID,APPROVE_RESTRICTED, TARGET_ASSIGNMENT_ID,'
                                        ||'STATUS, DATE_CREATED, CREATED_BY, ACCOUNTING_DATE, CURRENCY_CODE ';
            vSource_cols := vSource_Cols||'''N'',cI1009_Array(149), cI1009_Array(36), cI1009_Array(01), cI1009_Array(02), cI1009_Array(40), cI1009_Array(38), cI1009_Array(39),cD1001_Array(3), cI1009_Array(28), cI1009_Array(158), cI1009_Array(56),  cI1009_Array(151),cI1009_Array(152), cD1001_Array(4), cD1001_Array(5), cI1009_Array(35), vt_interface_id, cI1009_Array(68), cRule_id, cI1009_Array(150), cI1009_Array(96), cD1001_Array(1), cI1009_Array(27), cI1009_Array(97), cI1009_Array(66),cTarget_Instance_id,xxcp_global.gcommon(1).current_request_id,cI1009_Array(159), cI1009_Array(162), '
                                        ||'''NEW'', xxcp_global.SystemDate, xxcp_global.User_id, cI1009_Array(52), cI1009_Array(22) ';
            
            if xxcp_global.Preview_on = 'Y' then            
              vTarget_cols := vTarget_cols||',preview_id ';
              vSource_cols := vSource_Cols||',xxcp_global.gCOMMON(1).preview_id ';
            end if;
            
            -- ICS V4 Add the grouping_attributes to the cp_ic_settlement_interface insert.
            vTarget_cols := vTarget_cols||vSett_target_cols||',';
            vSource_cols := vSource_Cols||vSett_Source_cols||',';
         end if;         

         -- Remove the last comma character
         If Instr(vTarget_cols,',') > 0 then
             vTarget_cols := Substr(vTarget_cols,1,Length(vTarget_cols)-1);
         End if;

         If Instr(vSource_cols,',') > 0 then
             vSource_cols := Substr(vSource_cols,1,Length(vSource_cols)-1);
         End if;

         If upper(rec1.DML_Mode) = 'INSERT' then
           If rec1.OverRide = 'Y' then
             if nvl(xxcp_global.forecast_on,'N') = 'Y' then
               vDML_Statement := vTAB||'Insert into '||'XXCP_FC_TRANSACTION_JOURNALS'||' ('||chr(10)||'VT_FORECAST_ID,CPA_TYPE,COST_PLUS_SET_ID,'||vTarget_cols||')'||CHR(10)||vTAB||'Select xxcp_global.gCommon(1).Forecast_ID,xxcp_global.gCommon(1).CPA_Type,xxcp_wks.gActual_Costs_Rec(1).Cost_Plus_Set_id,'||vSource_cols||chr(10)||' from '||vSource_Table||' where rowid = :M0';
             else
               vDML_Statement := vTAB||'Insert into '||vTarget_table_override||' ('||chr(10)||vTarget_cols||')'||CHR(10)||vTAB||'Select '||vSource_cols||chr(10)||' from '||vSource_Table||' where rowid = :M0';
             end if;
           ElsIf rec1.db_link is null  then
             -- 03.08.20
             if nvl(xxcp_global.settlement_on,'N') = 'N' then
               vDML_Statement := vTAB||'Insert into '||cTarget_Table||' ('||chr(10)||vTAB4||vTarget_cols||') '||CHR(10)||vTab||'Select '||vSource_cols||chr(10)||vTAB4||' from '||vSource_Table||' where rowid = :M0';
             Else
               vDML_Statement := vTAB||'Insert into '||vTarget_table_override||' ('||chr(10)||vTAB4||vTarget_cols||') '||CHR(10)||vTab||'Select '||vSource_cols||chr(10)||vTAB4||' from '||vSource_Table||' where rowid = :M0';           
             End If; 
           Else
             vDML_Statement := vTAB||'Insert into '||cTarget_Table||'@'||rec1.db_link||' ('||chr(10)||vTAB4||vTarget_cols||')'||CHR(10)||vTAB||'Select '||vSource_cols||chr(10)||vTAB4||' from '||vSource_Table||' where rowid = :M0';
           End if;
         ElsIf upper(rec1.DML_Mode) = 'UPDATE' then
         
           -- 03.08.27 Only output the update statement if not settlement, or summarization.
           If nvl(xxcp_global.settlement_on,'N') = 'N' and rec1.summarization = 'N' then
           -- Update Mode
           If rec1.db_link is null  then
             vDML_Statement := vTAB||'Update '||vSource_Table||chr(10)||' Set '||vSource_cols||chr(10)||' where rowid = :M0';
           Else
             vDML_Statement := vTAB||'Update '||vSource_Table||'@'||rec1.db_link||chr(10)||' Set '||vSource_cols||chr(10)||' where rowid = :M0';
           End if;
           Else
             vDML_Statement := 'null'; 
           End If;
         End If;

         If vParentTable = 'Y' then
            vDML_Statement     := Replace(vDML_Statement,' :M0',' :J0');
         End If;

--
-- End Standard Column Rules
--
       Elsif rec1.target_object = 'API' then

         -- Get API Control Info
         open APIControl(pTarget_table       => cTarget_Table,
                         pSource_id          => cSource_id,
                         pTarget_Instance_Id => rec1.Target_instance_id);
         fetch APIControl into vAPI_Package_Name,
                               vAPI_Object_Name,
                               vPragma_autonomous,
                               vSuccess_argument,
                               vSuccess_operator,
                               vSuccess_value,
                               vDB_Link;
         close APIControl;

         vAPI_Declare_Stat := null;

         For APIRules_rec in APIRules(cSource_id
                                     ,vSource_Table
                                     ,rec1.rule_id -- column rules id
                                     -- Target
                                     ,cTarget_Table
                                     ,rec1.Target_instance_id
                                     -- Override
                                     ,vTarget_table_override_only
                                     ,vTarget_instance_override
                                     -- API Control
                                     ,vAPI_Package_Name
                                     ,vAPI_Object_Name)
         Loop

           Q := Q +1;

           -- Build Declare Section
           If Q = 1 then
             --vAPI_Declare_Stat := chr(10)||vTab||'DECLARE'||chr(10);

             -- Autonomous API?
             If vPragma_Autonomous = 'Y' then
               vAPI_Declare_Stat := vAPI_Declare_Stat ||vTab4|| '  PRAGMA AUTONOMOUS_TRANSACTION;'||chr(10)||chr(10);
             end if;

             vAPI_Declare_Stat := vAPI_Declare_Stat ||vTab4|| '  vSuccess boolean := FALSE;'||chr(10);
             vAPI_Declare_Stat := vAPI_Declare_Stat ||vTab4|| '  vReturn varchar2(4000);'||chr(10);
           End if;
           vAPI_Declare_Stat := vAPI_Declare_Stat||vTab4||'  v' || APIRules_rec.column_name||' '||APIRules_rec.data_type||';'||chr(10);

           -- Build Main Section
           if Q = 1 then
             vAPI_Main_Stat := vTab4||'Begin'||chr(10);
             -- Function or Procedure?
             If APIRules_rec.api_type = 'FUNCTION' then
               vAPI_Main_Stat := vAPI_Main_Stat ||vTab4|| '  vReturn := '||vAPI_Package_Name||'.'||vAPI_Object_Name||vDB_Link||'('||APIRules_rec.column_name ||' => '||replace(replace(APIRules_rec.Column_Definition,'}',null),'{',':');
             else
               vAPI_Main_Stat := vAPI_Main_Stat ||vTab4|| '  '||vAPI_Package_Name||'.'||vAPI_Object_Name||vDB_Link||'('||APIRules_rec.column_name ||' => '||replace(replace(APIRules_rec.Column_Definition,'}',null),'{',':');
             End if;
           Else
             -- Check IN/OUT type
             If APIRules_rec.in_out = 'IN' then
               vAPI_Main_Stat := vAPI_Main_Stat || chr(10)||vTab4||vTab|| ','||APIRules_rec.column_name ||' => '||replace(replace(APIRules_rec.Column_Definition,'}',null),'{',':');
             else
               vAPI_Main_Stat := vAPI_Main_Stat || chr(10)||vTab4||vTab|| ','||APIRules_rec.column_name ||' => v'||APIRules_rec.column_name;
             end if;
           end if;

         End Loop;

         vAPI_Main_Stat := vAPI_Main_Stat || ');' ||chr(10);

         -- Test API Output for Success?
         If vSuccess_argument is not null then
           vAPI_Main_Stat := vAPI_Main_Stat||chr(10)||vTab4||'  -- API Success?'||chr(10);
           vAPI_Main_Stat := vAPI_Main_Stat||vTab4||'  If v'||vSuccess_argument||' '||vSuccess_Operator||' '||vSuccess_Value||' then' ||chr(10);
           vAPI_Main_Stat := vAPI_Main_Stat||vTab4||'     vSuccess := TRUE;'||chr(10);
           vAPI_Main_Stat := vAPI_Main_Stat||vTab4||'  Else'||chr(10);
           vAPI_Main_Stat := vAPI_Main_Stat||vTab4|| '    cErrorMessage := ''Argument Name <'||vSuccess_argument||'> Argument Value <''||v'||vSuccess_argument||'||''> Argument Test < '||vSuccess_Operator||' '||replace(vSuccess_Value,'''','''''')||'> '';' ||chr(10);
           vAPI_Main_Stat := vAPI_Main_Stat||vTab4|| '    cInternalErrorCode := 12841;' ||chr(10);
           vAPI_Main_Stat := vAPI_Main_Stat||vTab4||'  End if;'||chr(10);
         End if;

         vAPI_Declare_Stat := vAPI_Declare_Stat||vAPI_Main_Stat;
         vAPI_Declare_Stat := vTab4||'Procedure API_Call is'||chr(10)||vAPI_Declare_Stat||chr(10);
         vAPI_Declare_Stat := vAPI_Declare_Stat||vTab4||'End API_Call;'||chr(10);

         vAPI_Main_Stat := vTab||'BEGIN'||chr(10);
         vAPI_Main_Stat := vAPI_Main_Stat||vTab4||'API_Call;'||chr(10);

         -- Build Exception Handler
         vAPI_Excep_Stat := vAPI_Excep_Stat ||vTab|| 'EXCEPTION' ||chr(10);
         vAPI_Excep_Stat := vAPI_Excep_Stat ||vTab|| '  WHEN OTHERS THEN' ||chr(10);
         vAPI_Excep_Stat := vAPI_Excep_Stat ||vTab|| '    cErrorMessage := ''API Target <''||cTarget_Table||''> ''||SQLERRM;' ||chr(10);
         vAPI_Excep_Stat := vAPI_Excep_Stat ||vTab|| '    cInternalErrorCode := 12840;' ||chr(10);


         vAPI_Statement := chr(10)||vTab||'DECLARE'||chr(10)||vAPI_Declare_Stat||vAPI_Main_Stat||vAPI_Excep_Stat||vTab||'END';
         vDML_Statement := vAPI_Statement;


       End if;


       --
       -- Set pointers to those Dynamic and Internal arrays that are used in the SQL.
       --

       -- DYNAMIC CUSTOM VARS
       xxcp_global.gCR(rt).D0000_Pointers       := Null;
       xxcp_global.gCR(rt).Direct_DML_statement := Null;
       SubPackageVars(vDML_Statement,xxcp_global.gCR(rt).Direct_DML_statement);

       vStatement_Dummy := vDML_Statement;
       rv := vMax_Dynamic;

       pa := 0;
       wa := 0;

       While rv > 0 loop
         pa := rv;

         If instr(vStatement_Dummy,':D'||to_char(pa+1000)) > 0 then
           wa := wa + 1;
           xxcp_global.gCR(rt).D0000_Pointers := xxcp_global.gCR(rt).D0000_Pointers || lpad(pa+1000,4,0);
           vStatement_Dummy := Replace(vStatement_Dummy,':D'||to_char(pa+1000),Null);
         End If;
         If instr(vStatement_Dummy,':D'||to_char(pa+2000)) > 0 then
           wa := wa + 1;
           xxcp_global.gCR(rt).D0000_Pointers := xxcp_global.gCR(rt).D0000_Pointers || lpad(pa+2000,4,0);
           vStatement_Dummy := Replace(vStatement_Dummy,':D'||to_char(pa+2000),Null);
         End If;
         If instr(vStatement_Dummy,':D'||to_char(pa+3000)) > 0 then
           wa := wa + 1;
           xxcp_global.gCR(rt).D0000_Pointers := xxcp_global.gCR(rt).D0000_Pointers || lpad(pa+3000,4,0);
           vStatement_Dummy := Replace(vStatement_Dummy,':D'||to_char(pa+3000),Null);
         End If;
         If instr(vStatement_Dummy,':D'||to_char(pa+4000)) > 0 then
           wa := wa + 1;
           xxcp_global.gCR(rt).D0000_Pointers := xxcp_global.gCR(rt).D0000_Pointers || lpad(pa+4000,4,0);
           vStatement_Dummy := Replace(vStatement_Dummy,':D'||to_char(pa+4000),Null);
         End If;
         If instr(vStatement_Dummy,':D'||to_char(pa+6000)) > 0 then
           wa := wa + 1;
           xxcp_global.gCR(rt).D0000_Pointers := xxcp_global.gCR(rt).D0000_Pointers || lpad(pa+6000,4,0);
           vStatement_Dummy := Replace(vStatement_Dummy,':D'||to_char(pa+6000),Null);
         End If;

         rv := rv -1;
       End loop;
       xxcp_global.gCR(rt).D0000_Counter := wa;

       -- INTERNAL
       wa := 0;
       vStatement_Dummy := vDML_Statement;
       xxcp_global.gCR(rt).I1009_Pointers    := Null;
       -- Revers is not necessary for this has it uses 9000 range.
       For pa in 1..vMax_Internal Loop
          If instr(vStatement_Dummy,':P'||to_char(pa+9000)) > 0 then
           wa := wa + 1;
           vStatement_Dummy := Replace(vStatement_Dummy,':P'||to_char(pa+9000),Null);
           xxcp_global.gCR(rt).I1009_Pointers  := xxcp_global.gCR(rt).I1009_Pointers  || lpad(pa+9000,4,0);
          End If;
       End loop;

       xxcp_global.gCR(rt).I1009_Counter       := wa;
       xxcp_global.gCR(rt).Target_Instance_id  := nvl(rec1.instance_id,0);
       xxcp_global.gCR(rt).Rule_id             := rec1.rule_id;
       xxcp_global.gCR(rt).Source_table        := cSource_Table;
       xxcp_global.gCR(rt).Target_table        := cTarget_Table;
       xxcp_global.gCR(rt).Insert_statement    := vDML_Statement;
       xxcp_global.gCR(rt).Rule_Category       := Rec1.Rule_Category_1;
       xxcp_global.gCR(rt).Rule_Key            := Rec1.Rule_key;
       xxcp_global.gCR(rt).History_statement   := vHistory_statement;
       xxcp_global.gCR(rt).Column_Count        := vColumn_count;

       cInternalErrorCode := 0;

     end loop;

     xxcp_global.gCR_Cnt := rt;

   end if;

   xxcp_global.gCSA_Cnt := vCSA_Cnt;

  End LoadColumnRules;

  --
  -- LoadPassThroughRules
  --
  PROCEDURE LoadPassThroughRules(
                                 cSource_id               in number
                                ,cSource_Table            in varchar2
                                ,cSource_Instance_id      in number
                                ,cTarget_Table            in varchar2
                                ,cRequest_id              in number
                                ,cPassThroughRules        in varchar2
                                ,cInternalErrorCode   in out number
    ) is

    vSet_History number(5) := 0;

   -- This cursor will retrieve all of the columns in alphabetical order.
   Cursor AllCols(pSource_id              in number
                 , pSource_Table           in varchar2
                 -- base Table
                 , pTarget_Table           in varchar2
                 , pTarget_Instance_ID     in number
                 -- Target Table
                 , pOvr_Target_Table       in varchar2
                 , pOvr_Target_Instance_ID in number
                 , pHistory_Column_Name    in varchar2
                 ) is
     SELECT T.COLUMN_NAME, T.COLUMN_NAME COLUMN_DEFINITION , 'N' BALANCING_ELEMENT
       FROM XXCP_TABLE_COLUMNS T
      WHERE T.TABLE_NAME  = pTarget_Table
        AND T.INSTANCE_ID = pTarget_Instance_ID
        AND T.COLUMN_NAME != pHistory_Column_Name
        --  Matching Columns
        AND T.COLUMN_NAME = any( SELECT S.COLUMN_NAME
                                   FROM XXCP_TABLE_COLUMNS S
                                  WHERE S.TABLE_NAME  = pSource_Table
                                    AND S.INSTANCE_ID = 0 -- Source Instance
                                )
        AND T.COLUMN_NAME = any( SELECT T.COLUMN_NAME
                                   FROM XXCP_TABLE_COLUMNS T
                                  WHERE T.TABLE_NAME  = pOvr_Target_Table
                                    AND T.INSTANCE_ID = pOvr_Target_Instance_ID
                                )
       Order by 1;


       -- This cursor will find the individual rules for the target table
       Cursor DistColRules(pSource_id in number, pTarget_Table in varchar2) is
          Select distinct t.Transaction_table,
                          t.Instance_id,
                          --
                          ib.db_link,
                          nvl(InitCap(t.dml_mode),'Insert') DML_MODE,
                          t.instance_id Target_instance_id ,
                          Decode(w.Target_table_override,Null,t.instance_id,0) Target_instance_override,
                          nvl(w.Target_table_override,t.transaction_table) Target_table_override,
                          Decode(w.Target_table_override,Null,'N','Y') OverRide,
                          w.staged_table,
                          to_char(t.source_id)||q.short_description||to_char(t.instance_id) Rule_Key,
                          Decode(t.transaction_table,'GL_INTERFACE',w.summarization,'N') Summarization,
                          f.file_type_id,
                          f.record_type
           from xxcp_column_rules_tables t,
                (select a1.instance_name, a1.instance_id, a1.db_link
                  from xxcp_instance_db_links_v a1
                   union
                   select '*', -1, a2.db_link
                     from xxcp_instance_db_links_v a2
                      where a2.instance_id = 0 ) ib,
                xxcp_table_override_v  w,
                xxcp_sys_target_tables   q,
                xxcp_sys_sources         s,
                xxcp_ff_file_types_v   f
           where t.transaction_table = pTarget_Table
             and t.source_id         = pSource_id
             and t.source_id         = s.source_id
             and t.instance_id       = ib.instance_id
             --
             and t.source_id         = q.source_id
             and t.transaction_table = q.transaction_table
             --
             and t.source_id         = w.source_id(+)
             and t.instance_id       = w.target_instance_id(+)
             and t.transaction_table = w.transaction_table(+)
             and nvl(t.target_object,'TABLE')= 'TABLE'
             -- 03.08.26 Only do passthrough for inserts 
             and nvl(InitCap(t.dml_mode),'Insert') = 'Insert'
             --
             and t.transaction_table = f.view_name (+)
           order by decode(t.instance_id,-1,9999,t.instance_id), t.transaction_table desc; -- order by instance_id is important

    Cursor ParentTab(pSource_id in number, pTarget_table in varchar2) is
    select Lookup_Code Parent_Table
      from xxcp_lookups k
     where upper(lookup_type)        = 'COLUMN RULE TABLE MAP'
       and numeric_code              = pSource_id
       and upper(lookup_type_subset) = upper(pTarget_table)
       and k.enabled_flag            = 'Y';

    Cursor TRT(pSource_id in number,pTarget_Table in varchar2 ) is
    select short_description Record_Type, t.history_id_column Process_History_Column
      from xxcp_sys_target_tables t
     where transaction_table = pTarget_Table
       and source_id         = pSource_id;

    vDML_Statement             varchar2(32676);
    vSource_cols               varchar2(32676);
    vTarget_cols               varchar2(32676);

    vSource_Table               xxcp_sys_source_tables.transaction_table%type;
    vTarget_Table               xxcp_sys_source_tables.transaction_Table%type;
    vTarget_table_override_only xxcp_sys_source_tables.transaction_table%type;
    vRecord_Type                xxcp_sys_target_tables.short_description%type;
    vProcess_History_Column     xxcp_sys_target_tables.history_id_column%type;


    rt                          pls_integer := xxcp_global.gCR_CNT;
    q                           pls_integer := 0;
    vFirstLoop                  Boolean;
    vCSA_Cnt                    pls_integer := 0;

  Cursor HistColName(pTransaction_table in varchar2, pSource_id in number) is
   select history_id_column
     from xxcp_sys_target_tables
    where source_id = pSource_id
      and transaction_table = pTransaction_table;

   vTAB  VARCHAR2(50) := chr(9)||chr(9)||chr(9)||chr(9);
   vTAB4 VARCHAR2(50) := chr(9)||chr(9)||chr(9)||chr(9)||chr(9)||chr(9);

  vLineBk      pls_integer := 0;
  vParentTable varchar2(1) := 'N';


  Begin

   vTarget_Table := cTarget_table;

   xxcp_global.set_source_id(cSource_id);
   -- Setup
   cInternalErrorCode   := 12741;

   xxcp_global.gCSA.Delete;
   xxcp_global.gCSA_Cnt := 0;

   -- Process History Column
   For rec in HistColName(cTarget_Table, cSource_id) loop
    vProcess_History_Column := Rec.History_id_column;
   End Loop;

   vCSA_Cnt := 0;

   vSource_Table := cSource_Table;

   If (vSource_Table is not null) and (vTarget_Table is not Null) and (vProcess_History_Column is not null) then


     rt := 0;
     For Rec1 in DistColRules(cSource_id,vTarget_Table)
     Loop
       -- get the rule_ids that are needed for this table_name
       vTarget_cols       := Null;
       vSource_cols       := Null;
       vDML_Statement     := Null;
       vParentTable       := 'N';

       vSource_Table := cSource_Table;

       For PTRec in ParentTab(cSource_id, Rec1.Transaction_table)
       Loop
        vSource_Table := PTRec.parent_table;
        vParentTable  := 'Y';
       End Loop;

       For TRTRec in TRT(cSource_id,vTarget_Table) Loop
         vRecord_Type := TRTRec.Record_Type;
         vProcess_History_Column := nvl(TRTRec.Process_History_Column,vProcess_History_Column);
       End Loop;

       rt := rt+1;

       vCSA_Cnt := vCSA_Cnt + 1;
       xxcp_global.gCSA(vCSA_Cnt).Rule_id            := rt;
       xxcp_global.gCSA(vCSA_Cnt).Rule_Category      := NULL;
       xxcp_global.gCSA(vCSA_Cnt).Target_Instance_id := Rec1.Instance_id;
       xxcp_global.gCSA(vCSA_Cnt).Target_Table       := vTarget_Table;
       xxcp_global.gCSA(vCSA_Cnt).Rule_Key           := Rec1.Rule_key;
       xxcp_global.gCSA(vCSA_Cnt).Pointers           := Null;
       xxcp_global.gCSA_Cnt := vCSA_Cnt;

       If Instr(rec1.DB_LINK,'@') > 0 then
         vTarget_table_override_only := Substr(rec1.Transaction_table,1,Instr(rec1.Transaction_table,'@')-1);
       Else
         vTarget_table_override_only := rec1.Transaction_table;
       End If;

       Q := 0;

       vLineBk      := 0;

       -- 02.07.13
       -- If writing to stage flat file then set the vProcess_History_Column to be null as
       -- the column is automatically populated
       if rec1.OverRide = 'Y' and rec1.Staged_table = 'Y' and rec1.file_type_id is not null then
         vProcess_History_Column := null;
       end if;

       --
       -- TARGET TABLE:
       -- Make Column Rules
       For Rec2 in AllCols( pSource_id                    => cSource_id
                           ,pSource_Table                 => vSource_Table
                           -- Target
                           ,pTarget_Table                 => vTarget_Table
                           ,pTarget_Instance_ID           => rec1.Instance_id
                           -- Override
                           ,pOvr_Target_Table             => Rec1.Transaction_table
                           ,pOvr_Target_Instance_ID       => rec1.Instance_id
                           ,pHistory_Column_Name          => vProcess_History_Column)
       Loop

         Q := Q +1;
         vSet_History := 0;

         -- 02.07.13
         -- The columns in the list are automatically taken care of for staged tables
         If rec2.column_name not in ('VT_RECORD_TYPE',
                                     'VT_FILE_TYPE_ID',
                                     'VT_REQUEST_ID',
                                     'VT_INSTANCE_ID',
                                     'VT_SOURCE_ASSIGNMENT_ID',
                                     'VT_TRANSACTION_DATE',
                                     'VT_SOURCE_CREATION_DATE') then

           If rec2.column_name <>  'PROCESS HISTORY' then

            If vLineBk >= 5 then
              vLineBk := 0;
              vTarget_cols  := vTarget_Cols||chr(10)||vTAB4||rec2.column_name||',';
              If rec2.column_name = vProcess_History_Column then
                vSet_History := 1;
                vSource_cols := vSource_Cols||chr(10)||vTAB4||':M15,';  -- Process History Column
              Else
                vSource_cols := vSource_Cols||chr(10)||vTAB4||replace(replace(rec2.column_definition,'}',null),'{',':')||',';
              End If;

            Else
              vTarget_cols  := vTarget_Cols||rec2.column_name||',';
              If rec2.column_name = vProcess_History_Column then
                vSource_cols := vSource_Cols||':M15,';  -- Process History Column
                vSet_History := 1;
              Else
                vSource_cols := vSource_Cols||replace(replace(rec2.column_definition,'}',null),'{',':')||',';
              End If;
              vLineBk := vLineBk + 1;
            End If;

           -- get the column rules columns
           End If;
         end if;
       End Loop;

       If vSet_History != 1 and vProcess_History_Column is not null then
          vTarget_cols := vTarget_cols||vProcess_History_Column||',';
          vSource_cols := vSource_Cols||':M15,';  -- Process History Column
       End If;

       vFirstLoop := False;
       vLineBk := 0;

       -- Write VT Request Id to target table
       If rec1.OverRide = 'Y' and rec1.staged_table = 'Y' then

          vTarget_cols := vTarget_cols||'vt_request_id,vt_instance_id, vt_source_assignment_id,vt_process_history_id,';
          vSource_cols := vSource_Cols||'xxcp_global.gCommon(1).current_request_id,cSource_Instance_id,xxcp_global.gCommon(1).current_assignment_id,cProcess_history_id,';  -- Process History Column

       End If;

       -- Remove the last comma character
       If Instr(vTarget_cols,',') > 0 then
           vTarget_cols := Substr(vTarget_cols,1,Length(vTarget_cols)-1);
       End if;

       If Instr(vSource_cols,',') > 0 then
           vSource_cols := Substr(vSource_cols,1,Length(vSource_cols)-1);
       End if;

       If rec1.OverRide = 'Y' then
           vDML_Statement := vTAB||'Insert into '||rec1.Target_table_override||' ('||chr(10)||vTarget_cols||')'||CHR(10)||vTAB||'Select '||vSource_cols||chr(10)||' from '||vSource_Table||chr(10)||
              vTAB||'Where vt_source_assignment_id = cSource_Assignment_id'||CHR(10)||
              vTAB||'  and vt_request_id           = cRequest_id'||CHR(10)||
              vTAB||'  and vt_status_code         in (7025,7026,7027)'||CHR(10)||
              vTAB||'  and vt_status               = ''PASSTHROUGH''';
       ElsIf rec1.db_link is null  then
           vDML_Statement := vTAB||'Insert into '||vTarget_Table||' ('||chr(10)||vTAB4||vTarget_cols||') '||CHR(10)||vTab||'Select '||vSource_cols||chr(10)||vTAB4||' from '||vSource_Table||chr(10)||
              vTAB||'Where vt_source_assignment_id = cSource_Assignment_id'||CHR(10)||
              vTAB||'  and vt_request_id           = cRequest_id'||CHR(10)||
              vTAB||'  and vt_status_code         in (7025,7026,7027)'||CHR(10)||
              vTAB||'  and vt_status               = ''PASSTHROUGH''';
      Else
           vDML_Statement := vTAB||'Insert into '||vTarget_Table||'@'||rec1.db_link||' ('||chr(10)||vTAB4||vTarget_cols||')'||CHR(10)||vTAB||'Select '||vSource_cols||chr(10)||vTAB4||' from '||vSource_Table||chr(10)||
              vTAB||'Where vt_source_assignment_id = cSource_Assignment_id'||CHR(10)||
              vTAB||'  and vt_request_id           = cRequest_id'||CHR(10)||
              vTAB||'  and vt_status_code         in (7025,7026,7027)'||CHR(10)||
              vTAB||'  and vt_status               = ''PASSTHROUGH''';
       End if;


       -- Create Internal Record
       xxcp_global.gCR(rt).Direct_DML_statement := vDML_Statement;
       xxcp_global.gCR(rt).D0000_Counter := 0;
       xxcp_global.gCR(rt).Target_Instance_id  := nvl(rec1.instance_id,0);
       xxcp_global.gCR(rt).Rule_id             := rt;
       xxcp_global.gCR(rt).Source_table        := cSource_Table;
       xxcp_global.gCR(rt).Target_table        := vTarget_Table;
       xxcp_global.gCR(rt).Insert_statement    := vDML_Statement;
       xxcp_global.gCR(rt).Rule_Key            := Rec1.Rule_key;

       cInternalErrorCode := 0;

     end loop;

     xxcp_global.gCR_Cnt := rt;

   end if;

   xxcp_global.gCSA_Cnt := vCSA_Cnt;

  End LoadPassThroughRules;



  -- !!
  -- !! Get Dynamic Set Attribute
  -- !!
  FUNCTION Get_Dynamic_Set_Attribute(cSql_build_id in number
                                   , cColumn_id    in number
                                   , cActivity_id  in number
                                   , cSequence    out number ) return varchar2 is

       cursor dya(pSql_build_id in number, pActivity_id in number) is
          select sequence_number, Attribute_Name,Column_Definition,'STD' set_type, Validation_type Validation
                   from xxcp_dynamic_attribute_sets ds,
                        xxcp_Dynamic_attributes_v c
                  where ds.attribute_set_id = pSql_build_id
                    and ds.SEQUENCE_NUMBER = c.id
                    and upper(nvl(ds.column_definition,'NULL')) <> 'NULL'
                Union
                select id column_id, Attribute_Name,'null','STD',0
                  from xxcp_Dynamic_attributes_v
                 where dynamic_name_id = 1
                  and column_type =  pActivity_id
                  and not id = any(select sequence_number
                                               from xxcp_dynamic_attribute_sets ds
                                              where ds.attribute_set_id = pSql_build_id
                                                 and upper(nvl(ds.column_definition,'NULL')) <> 'NULL')
                  order by 1;

    vAttribute_Name  varchar2(100) := 'Not found';
    vSequence_Number Number(15);
    w                pls_integer   := 0;

   Cursor c1(pSQL_Build_id in number) is
     select source_id
    from xxcp_sql_build
    where sql_build_id = pSQL_Build_id;

  Begin

    For Rec in c1(cSql_build_id) loop
      xxcp_global.set_source_id(rec.source_id);
    End Loop;

     For DyaRec in Dya(cSql_build_id, cActivity_id)
       Loop
         w := w + 1;
         If w = cColumn_id then
           vAttribute_Name  := DyaRec.Attribute_Name;
           vSequence_Number := DyaRec.Sequence_number;
         End If;
       End Loop;
     cSequence := vSequence_Number;
     Return(vAttribute_Name);
  End Get_Dynamic_Set_Attribute;

  -- !!
  -- !! MAKESQLBUILDSTATEMENTS
  -- !!
  PROCEDURE MakeSQLBuildStatements(cSource_id         in number,
                                   cInternalErrorCode in out number)  is

    cursor sbd(pSource_id in number) is
      select distinct b.sql_build_id, Chr(10)||b.sql_build sql_build,b.transaction_table,b.optimizer_hint, b.activity_id,  c.calc_legal_exch_rate
         from xxcp_sql_build b,
              xxcp_sys_source_tables c
       where b.transaction_table = c.transaction_table
          and c.SOURCE_ID         IN (pSource_id,40);

    cursor dya(pAttribute_set_id in number, pActivity_id in number) is
      select  sequence_number,Chr(10)||
              decode(Instr(upper(Column_Definition),'TO_CHAR'),0,decode(ds.AUTO_DATE_FORMAT,'Y','To_Char(',Null), NULL)||
              ds.Column_Definition||
              decode(Instr(upper(Column_Definition),'TO_CHAR'),0,decode(ds.AUTO_DATE_FORMAT,'Y',',''DD-MON-YYYY'')',Null), NULL)||
              ' "'||NVL(SUBSTR(xv.ATTRIBUTE_NAME, 1, 30), 'Col '||to_char(sequence_number))||'"' Column_Definition
              ,'STD' set_type, nvl(Validation_type,0) Validation
         from xxcp_dynamic_attribute_sets ds,
              xxcp_sql_build x,
              xxcp_dynamic_attributes_v xv
        where ds.attribute_set_id  = pAttribute_set_id
          and ds.attribute_set_id = x.sql_build_id
          and ds.sequence_number  = xv.id(+)
          and upper(nvl(ds.column_definition,'~null~')) <> '~NULL~'
        order by 1;

    vSelect_statement  varchar2(32676);
    vSource_cols       varchar2(32676);
    vColumn_Positions  varchar2(500);
    vColumn_Count      pls_integer := 0;

    vMax_Internal      pls_integer := 1000;
    wa                 pls_integer := 0;
    ca                 pls_integer := 0;
    pv                 pls_integer := 0;
    vMax_Dynamic       pls_integer := 1000;
    sb                 pls_integer := 0;

    vValidationStr     varchar2(250);
    vSB_MaxArraySize   pls_integer := 0;
    vCustBindCnt       pls_integer := 0;

    vSQL_BUILD         xxcp_sql_build.sql_build%type;
    -- new
    vSB                xxcp_dynamic_sql_gen%rowtype;
    vPMode             varchar2(1);

    vLoopCnt           pls_integer := 2;

    cursor sy(pSource_id in number) is
     select s.preview_ctl
       from xxcp_sys_sources s
      where source_id = pSource_id;

 Begin

    xxcp_global.SET_SOURCE_ID(cSource_id);

    For Rec in sy(pSource_id => cSource_id) Loop
      If rec.preview_ctl = 1 then
        vLoopCnt := 3;
      End If;
    End Loop;

    for pv in 1..vLoopCnt loop

      If pv = 1 then
        vPMode  := 'N';
      ElsIf pv = 2 then
        vPMode := 'Y';
      Elsif pv = 3 then
        vPMode := 'J';
      End If;

      For rec1 in sbd(cSource_id) Loop
          vSelect_statement := Null;
          vSource_cols      := Null;
          vColumn_Count     := 0;
          vColumn_Positions := Null;
          vValidationStr    := Null;
          vSB_MaxArraySize  := 0;
          vCustBindCnt      := 0;
          vSQL_BUILD        := rec1.sql_build;
          vSB.Sql_Build_Id := rec1.sql_build_id;

          cInternalErrorCode := 13050;  -- Dynamic generation failed.

          -- Build Column list and mark validation columns
          For rec2 in dya(rec1.sql_build_id, rec1.activity_id)
          Loop
             vColumn_Count  := vColumn_Count + 1;

             vSource_cols   := vSource_Cols||replace(replace(rec2.column_definition,'}',null),'{',':')||',';
             vValidationStr := vValidationStr||to_char(rec2.validation);

             vColumn_Positions := vColumn_Positions||lpad(to_char(rec2.sequence_number),4,'0');

             If rec2.sequence_number > vSB_MaxArraySize then
               vSB_MaxArraySize := rec2.sequence_number;
             End If;
          End loop;

          -- Replace Binds in main SQL
          vSQL_BUILD := replace(vSQL_BUILD,'}',Null);
          vSQL_BUILD := replace(vSQL_BUILD,'{D',':D');
          vSQL_BUILD := replace(vSQL_BUILD,'{P',':P');
          vSQL_BUILD := replace(vSQL_BUILD,'{M',':M');
          vSQL_BUILD := replace(vSQL_BUILD,'{C',':C');

          vSB.Activity_id      := rec1.activity_id;
          vSB.Column_Positions := vColumn_Positions;
          vSB.MaxArraySize     := vSB_MaxArraySize;

          If instr(vSource_cols,',') > 0 then
            vSource_cols := Substr(vSource_cols,1,Length(vSource_cols)-1);
          End if;

          -- Optimizer Hint
          If rec1.optimizer_hint is not null then
            vSelect_statement := 'Select '||rec1.optimizer_hint||' '||vSource_Cols||' '||vSQL_BUILD;
          Else
            vSelect_statement := 'Select '||vSource_Cols||' '||vSQL_BUILD;
          End If;

          -- 02.06.01
          vSB.C0000_Pointers := Null;
          vSB.C0000_Highest  := 0;
          vSB.D0000_Pointers := Null;
          vSB.D0000_Highest  := 0;

          -- Internal Array pointers
          vSB.I1009_Pointers := Null;
          vSB.I1009_Highest  := 0;

          If vColumn_Count > 0 then

            wa := 0;
            For pa in 1..vMax_Dynamic
            Loop
              If instr(vSelect_Statement,':C'||to_char(pa+1000)) > 0 then
                ca := ca + 1;
                vSB.C0000_Pointers := vSB.C0000_Pointers || lpad(pa+1000,4,0);
                If vSB.C0000_Highest < ca then
                 vSB.C0000_Highest := ca;
                End If;
              End If;
              If instr(vSelect_Statement,':D'||to_char(pa+1000)) > 0 then
                wa := wa + 1;
                vSB.D0000_Pointers := vSB.D0000_Pointers || lpad(pa+1000,4,0);
                If vSB.D0000_Highest < wa then
                 vSB.D0000_Highest := wa;
                End If;
              End If;
              If instr(vSelect_Statement,':D'||to_char(pa+2000)) > 0 then
                wa := wa + 1;
                vSB.D0000_Pointers := vSB.D0000_Pointers || lpad(pa+2000,4,0);
                If vSB.D0000_Highest < wa then
                 vSB.D0000_Highest := wa;
                End If;
              End If;
              If instr(vSelect_Statement,':D'||to_char(pa+3000)) > 0 then
                wa := wa + 1;
                vSB.D0000_Pointers := vSB.D0000_Pointers || lpad(pa+3000,4,0);
                If vSB.D0000_Highest < wa then
                 vSB.D0000_Highest := wa;
                End If;
              End If;
              If instr(vSelect_Statement,':D'||to_char(pa+4000)) > 0 then
                wa := wa + 1;
                vSB.D0000_Pointers := vSB.D0000_Pointers || lpad(pa+4000,4,0);
                If vSB.D0000_Highest < wa then
                 vSB.D0000_Highest := wa;
                End If;
              End If;

            End loop;

            wa := 0;
            For pa in 1..vMax_Internal
            Loop
              If instr(vSelect_Statement,':P'||to_char(pa+9000)) > 0 then
                wa := wa + 1;
                vSB.I1009_Pointers := vSB.I1009_Pointers || lpad(pa+9000,4,0);
                If vSB.I1009_Highest < wa then
                  vSB.I1009_Highest := wa;
                End If;
              End If;
            End loop;

            vSelect_Statement := rtrim(vSelect_Statement);

            -- Transalate Preview Mode Tables (j = manual journal)
            If vPMode in ('J', 'Y') then

              -- These table names should only be in UPPER or LOWER Case, not Init Cap. so that
              -- the user can specify a mixed case if they want VT not to swap out the name
              -- this technique is used by a few clients.

              vSelect_Statement := Replace(vSelect_Statement,'XXCP_TRANSACTION_CACHE','XXCP_PV_TRANSACTION_CACHE_V');
              vSelect_Statement := Replace(vSelect_Statement,'XXCP_TRANSACTION_REFERENCES','XXCP_PV_TRANSACTION_REFS_V');

              vSelect_Statement := Replace(vSelect_Statement,'XXCP_TRANSACTION_ATTRIBUTES_V','XXCP_PV_TRANSACTION_ATTRIB_V');
              vSelect_Statement := Replace(vSelect_Statement,'XXCP_TRANSACTION_HEADER_V','XXCP_PV_TRANSACTION_HEADER_V');

              vSelect_Statement := Replace(vSelect_Statement,'XXCP_PROCESS_HISTORY','XXCP_PV_PROCESS_HISTORY_V');
              vSelect_Statement := Replace(vSelect_Statement,'XXCP_TRANSACTION_HEADER','XXCP_PV_TRANSACTION_HEADER_V');
              vSelect_Statement := Replace(vSelect_Statement,'XXCP_PROCESS_HISTORY','XXCP_PV_PROCESS_HISTORY_V');
              vSelect_Statement := Replace(vSelect_Statement,'XXCP_VALIDATION_RESULTS','XXCP_PV_VALIDATION_RESULTS_V');
              --
              vSelect_Statement := Replace(vSelect_Statement,'xxcp_transaction_references','XXCP_PV_TRANSACTION_REFS_V');
              vSelect_Statement := Replace(vSelect_Statement,'xxcp_transaction_cache','XXCP_PV_TRANSACTION_CACHE_V');
              vSelect_Statement := Replace(vSelect_Statement,'xxcp_transaction_attributes','XXCP_PV_TRANSACTION_ATTRIB_V');
              vSelect_Statement := Replace(vSelect_Statement,'xxcp_transaction_header','XXCP_PV_TRANSACTION_HEADER_V');
              vSelect_Statement := Replace(vSelect_Statement,'xxcp_process_history','XXCP_PV_PROCESS_HISTORY_V');
              vSelect_Statement := Replace(vSelect_Statement,'xxcp_transaction_header','XXCP_PV_TRANSACTION_HEADER_V');
              vSelect_Statement := Replace(vSelect_Statement,'xxcp_process_history','XXCP_PV_PROCESS_HISTORY_V');
              vSelect_Statement := Replace(vSelect_Statement,'xxcp_validation_results','XXCP_PV_VALIDATION_RESULTS_V');

              vSelect_Statement := Replace(vSelect_Statement,'XXCP_TRANSACTION_ATTRIBUTES ','XXCP_PV_TRANSACTION_ATTRIB_V');
              vSelect_Statement := Replace(vSelect_Statement,'XXCP_TRANSACTION_HEADER ','XXCP_PV_TRANSACTION_HEADER_V');

              -- 02.07.15
              vSelect_Statement := Replace(vSelect_Statement,'XXCP_FLAT_FILE_INTERFACE','XXCP_PV_INTERFACE_V');
              vSelect_Statement := Replace(vSelect_Statement,'xxcp_flat_file_interface','XXCP_PV_INTERFACE_V');

              If vPMode = 'J' then
                vSelect_Statement := Replace(vSelect_Statement,'XXCP_GL_INTERFACE','XXCP_PV_GL_INTERFACE_V');
                vSelect_Statement := Replace(vSelect_Statement,'xxcp_gl_interface','XXCP_PV_GL_INTERFACE_V');
              End If;

            End If;

            -- Sub out variables to short code variables

            vSelect_statement := Replace(vSelect_statement,'{M0}',':M0');
            vSelect_statement := Replace(vSelect_statement,'{M3}',':M3');
            vSelect_statement := Replace(vSelect_statement,'{M4}',':M4');
            vSelect_statement := Replace(vSelect_statement,'{M7}',':M7');
            vSelect_statement := Replace(vSelect_statement,'{MS}',':MS');
            vSelect_statement := Replace(vSelect_statement,'{MP}',':MP');
            vSelect_statement := Replace(vSelect_statement,'{M6}',':M6');
            vSelect_statement := Replace(vSelect_statement,'{M8}',':M8');
            vSelect_statement := Replace(vSelect_statement,'{MKEY}',':MKEY');
            vSelect_statement := Replace(vSelect_statement,'{M5}',':M5');
            vSelect_statement := Replace(vSelect_statement,'{M9}',':M9');
            vSelect_statement := Replace(vSelect_statement,'{MA}',':MA');

            vSelect_statement := Replace(vSelect_statement,'{CB1}',':CB1');
            vSelect_statement := Replace(vSelect_statement,'{CB2}',':CB2');
            vSelect_statement := Replace(vSelect_statement,'{CB3}',':CB3');
            vSelect_statement := Replace(vSelect_statement,'{CB4}',':CB4');
            vSelect_statement := Replace(vSelect_statement,'{CB5}',':CB5');
            vSelect_statement := Replace(vSelect_statement,'{CB6}',':CB6');
            vSelect_statement := Replace(vSelect_statement,'{CB7}',':CB7');
            vSelect_statement := Replace(vSelect_statement,'{CB8}',':CB8');

            If instr(vSelect_statement,':m0') > 0 then
              vSelect_statement := Replace(vSelect_statement,':m0',':M0');
            End if;

            -- Source Rowid
            If instr(vSelect_statement,':M0') > 0 then
              vSB.Bind_M0 := 'Y';
            Else
              vSB.Bind_M0 := 'N';
            End If;
            -- Transaction Id
            If instr(vSelect_statement,':M3') > 0 then
              vSB.Bind_M3 := 'Y';
            Else
              vSB.Bind_M3 := 'N';
            End If;
            -- Transaction Id
            If instr(vSelect_statement,':M4') > 0 then
              vSB.Bind_M4 := 'Y';
            Else
              vSB.Bind_M4 := 'N';
            End If;
            -- Set of books id
            If instr(vSelect_statement,':M5') > 0 then
              vSB.Bind_M5 := 'Y';
            Else
              vSB.Bind_M5 := 'N';
            End If;
            -- Type
            If instr(vSelect_statement,':M6') > 0 then
              vSB.Bind_M6 := 'Y';
            Else
              vSB.Bind_M6 := 'N';
            End If;
            -- Trading Set
            If instr(vSelect_statement,':M7') > 0 then
              vSB.Bind_M7 := 'Y';
            Else
              vSB.Bind_M7 := 'N';
            End If;
            --
            If instr(vSelect_statement,':M8') > 0 then
              vSB.Bind_M8 := 'Y';
            Else
              vSB.Bind_M8 := 'N';
            End If;

            -- Previous Attribute
            If instr(vSelect_statement,':M9') > 0 then
              vSB.Bind_M9 := 'Y';
            Else
              vSB.Bind_M9 := 'N';
            End If;
            -- Key
            If instr(vSelect_statement,':MKEY') > 0 then
              vSB.Bind_MKEY := 'Y';
            Else
              vSB.Bind_MKEY := 'N';
            End If;
            -- Owner
            If instr(vSelect_statement,':MS') > 0 then
              vSB.Bind_MS := 'Y';
            Else
              vSB.Bind_MS := 'N';
            End If;
            -- Partner
            If instr(vSelect_statement,':MP') > 0 then
              vSB.Bind_MP := 'Y';
            Else
              vSB.Bind_MP := 'N';
            End If;
            If instr(vSelect_statement,':MA') > 0 then
              vSB.Bind_MA := 'Y';
            Else
              vSB.Bind_MA := 'N';
            End If;
            If instr(vSelect_statement,':MSTEP') > 0 then
              vSB.Bind_MSTEP := 'Y';
            Else
              vSB.Bind_MSTEP := 'N';
            End If;

            vCustBindCnt := 0;
              -- CB (Custom Binds)
            If instr(vSelect_statement,':CB1') > 0 then
              vSB.Bind_CB1 := 'Y';
              vCustBindCnt := 1;
            Else
              vSB.Bind_CB1 := 'N';
              vCustBindCnt := 1;
            End If;
            If instr(vSelect_statement,':CB2') > 0 then
              vSB.Bind_CB2 := 'Y';
              vCustBindCnt := 1;
            Else
              vSB.Bind_CB2 := 'N';
            End If;
            If instr(vSelect_statement,':CB3') > 0 then
              vSB.Bind_CB3 := 'Y';
              vCustBindCnt := 1;
            Else
              vSB.Bind_CB3 := 'N';
            End If;
            If instr(vSelect_statement,':CB4') > 0 then
              vSB.Bind_CB4 := 'Y';
              vCustBindCnt := 1;
            Else
              vSB.Bind_CB4 := 'N';
            End If;
            If instr(vSelect_statement,':CB5') > 0 then
              vSB.Bind_CB5 := 'Y';
              vCustBindCnt := 1;
            Else
              vSB.Bind_CB5 := 'N';
            End If;
            If instr(vSelect_statement,':CB5') > 0 then
              vSB.Bind_CB5 := 'Y';
              vCustBindCnt := 1;
            Else
              vSB.Bind_CB5 := 'N';
            End If;
            If instr(vSelect_statement,':CB6') > 0 then
              vSB.Bind_CB6 := 'Y';
              vCustBindCnt := 1;
            Else
              vSB.Bind_CB6 := 'N';
            End If;
            If instr(vSelect_statement,':CB7') > 0 then
              vSB.Bind_CB7 := 'Y';
              vCustBindCnt := 1;
            Else
              vSB.Bind_CB7 := 'N';
            End If;
            If instr(vSelect_statement,':CB8') > 0 then
              vSB.Bind_CB8 := 'Y';
              vCustBindCnt := 1;
            Else
              vSB.Bind_CB8 := 'N';
            End If;
            -- Future Use
            vSB.Bind_CB9  := 'N';
            vSB.Bind_CB10 := 'N';

            If vCustBindCnt > 0 then
              vSB.CustomBinds := 'Y';
            Else
              vSB.CustomBinds := 'N';
            End If;

            vSB.Transaction_Table := Rec1.transaction_table;

            -- Store in Memory

            sb := sb + 1;

          End If;


       -- Delete previous entry
        Begin
          Delete from  XXCP_DYNAMIC_SQL_GEN where sql_build_id = rec1.sql_build_id and preview_mode = vPMode;



        Insert into  XXCP_DYNAMIC_SQL_GEN(
           Source_id
          ,SQL_BUILD_ID
          ,PREVIEW_MODE
          ,ACTIVITY_ID
          ,TRANSACTION_TABLE
          ,SELECT_STATEMENT
          ,COLUMN_COUNT
          ,COLUMN_POSITIONS
          ,VALIDATION
          ,Bind_M0
          ,Bind_M3
          ,Bind_M4
          ,Bind_M5
          ,Bind_M6
          ,Bind_M7
          ,Bind_M8
          ,BIND_M9
          ,Bind_MS
          ,Bind_MP
          ,Bind_Mkey
          ,Bind_MA
          ,Bind_Mstep
          ,CUSTOMBINDS
          ,BIND_CB1
          ,BIND_CB2
          ,BIND_CB3
          ,BIND_CB4
          ,BIND_CB5
          ,BIND_CB6
          ,BIND_CB7
          ,BIND_CB8
          ,BIND_CB9
          ,BIND_CB10
          ,C0000_POINTERS
          ,C0000_HIGHEST
          ,D0000_POINTERS
          ,D0000_HIGHEST
          ,I1009_POINTERS
          ,I1009_HIGHEST
          ,MAXARRAYSIZE
           --
          ,CREATED_BY
          ,CREATION_DATE
          ,LAST_UPDATED_BY
          ,LAST_UPDATE_DATE
          ,LAST_UPDATE_LOGIN
          )
          Values(
           cSource_id,
           vSB.Sql_Build_Id,
           vPMode,
           vSB.Activity_Id,
           vSB.Transaction_Table,
           to_clob(vSelect_statement),  -- 03.07.16
           vColumn_Count,
           vsb.column_positions,
           vValidationStr,
           vSB.Bind_M0
          ,vSB.Bind_M3
          ,vSB.Bind_M4
          ,vSB.Bind_M5
          ,vSB.Bind_M6
          ,vSB.Bind_M7
          ,vSB.Bind_M8
          ,vSB.BIND_M9
          ,vSB.Bind_MS
          ,vSB.Bind_MP
          ,vSB.Bind_Mkey
          ,vSB.Bind_MA
          ,vSB.Bind_Mstep
          ,vSB.Custombinds
          ,vSB.BIND_CB1
          ,vSB.BIND_CB2
          ,vSB.BIND_CB3
          ,vSB.BIND_CB4
          ,vSB.BIND_CB5
          ,vSB.BIND_CB6
          ,vSB.BIND_CB7
          ,vSB.BIND_CB8
          ,vSB.BIND_CB9
          ,vSB.BIND_CB10,
           vSB.C0000_Pointers,
           vSB.C0000_Highest,
           vSB.D0000_Pointers,
           vSB.D0000_Highest,
           vSB.I1009_Pointers,
           vSB.I1009_Highest,
           vSB.Maxarraysize,
           fnd_global.USER_ID,
           sysdate,
           fnd_global.USER_ID,
           sysdate,
           fnd_global.LOGIN_ID
          );

            Commit;

            -- Successful
            cInternalErrorCode := 0;

            Exception when others then
               cInternalErrorCode := 13050;
               Rollback;
          End;
        End Loop;
    End Loop;

  End MakeSQLBuildStatements;

  -- !!
  -- !! MAKESINGLESQLBUILDSTATEMENT
  -- !!
  PROCEDURE MakeSingleSQLBuildStatement(cSource_id         in number,
                                        cSQL_Build_Id      in number,
                                        cInternalErrorCode in out number)  is

    cursor sbd(pSource_id    in number,
               pSQL_Build_Id in number) is
      select distinct b.sql_build_id, Chr(10)||
                      b.sql_build sql_build,
                      b.transaction_table,
                      b.optimizer_hint,
                      b.activity_id,
                      c.calc_legal_exch_rate
         from xxcp_sql_build b,
             xxcp_sys_source_tables c
        where b.transaction_table = c.transaction_table
         and c.SOURCE_ID         IN (pSource_id,40)
         AND b.sql_build_id      = pSQL_Build_id;

    cursor dya(pAttribute_set_id in number, pActivity_id in number) is
      select  sequence_number,Chr(10)||
              decode(Instr(upper(Column_Definition),'TO_CHAR'),0,decode(ds.AUTO_DATE_FORMAT,'Y','To_Char(',Null), NULL)||
              ds.Column_Definition||
              decode(Instr(upper(Column_Definition),'TO_CHAR'),0,decode(ds.AUTO_DATE_FORMAT,'Y',',''DD-MON-YYYY'')',Null), NULL)||
              ' "'||NVL(SUBSTR(xv.ATTRIBUTE_NAME, 1, 30),'Col '||to_char(sequence_number))||'"' Column_Definition
                ,'STD' set_type, nvl(Validation_type,0) Validation
         from xxcp_dynamic_attribute_sets ds,
              xxcp_sql_build x,
              xxcp_dynamic_attributes_v xv
        where ds.attribute_set_id  = pAttribute_set_id
          and ds.attribute_set_id = x.sql_build_id
          and ds.sequence_number  = xv.id(+)
          and upper(nvl(ds.column_definition,'~null~')) <> '~NULL~'
        order by 1;

    vSelect_statement  varchar2(32676);
    vSource_cols       varchar2(32676);
    vColumn_Positions  varchar2(500);
    vColumn_Count      pls_integer := 0;

    vMax_Internal      pls_integer := 1000;
    wa                 pls_integer := 0;
    ca                 pls_integer := 0;
    pv                 pls_integer := 0;
    vMax_Dynamic       pls_integer := 1000;
    sb                 pls_integer := 0;

    vValidationStr     varchar2(250);
    vSB_MaxArraySize   pls_integer := 0;
    vCustBindCnt       pls_integer := 0;

    vSQL_BUILD         xxcp_sql_build.sql_build%type;
    -- new
    vSB                xxcp_dynamic_sql_gen%rowtype;
    vPMode             varchar2(1);

    vLoopCnt           pls_integer := 2;

    cursor sy(pSource_id in number) is
     select s.preview_ctl
     from xxcp_sys_sources s
     where source_id = pSource_id;


    Begin

    -- 02.07.09 Not being set.
    xxcp_global.SET_SOURCE_ID(cSource_id);

    cInternalErrorCode := 13050;  -- Dynamic generation failed.

    For Rec in sy(pSource_id => cSource_id) loop
      If rec.preview_ctl = 1 then
         vLoopCnt := 3;
      End If;
    End Loop;


    for pv in 1..vLoopCnt loop

      If pv = 1 then
        vPMode  := 'N';
      ElsIf pv = 2 then
        vPMode := 'Y';
      ElsIf pv = 3 then
        vPMode := 'J';
      End If;

      For rec1 in sbd(pSource_id =>  cSource_id, pSQL_build_id => cSQL_Build_id) Loop
          vSelect_statement := Null;
          vSource_cols      := Null;
          vColumn_Count     := 0;
          vColumn_Positions := Null;
          vValidationStr    := Null;
          vSB_MaxArraySize  := 0;
          vCustBindCnt      := 0;
          vSQL_BUILD        := rec1.sql_build;
          vSB.Sql_Build_Id  := rec1.sql_build_id;

          -- Build Column list and mark validation columns
          For rec2 in dya(rec1.sql_build_id, rec1.activity_id)
          Loop
             vColumn_Count  := vColumn_Count + 1;

             vSource_cols   := vSource_Cols||replace(replace(rec2.column_definition,'}',null),'{',':')||',';
             vValidationStr := vValidationStr||to_char(rec2.validation);

             vColumn_Positions := vColumn_Positions||lpad(to_char(rec2.sequence_number),4,'0');

             If rec2.sequence_number > vSB_MaxArraySize then
               vSB_MaxArraySize := rec2.sequence_number;
             End If;
          End loop;

          -- Replace Binds in main SQL
          vSQL_BUILD := replace(vSQL_BUILD,'}',Null);
          vSQL_BUILD := replace(vSQL_BUILD,'{D',':D');
          vSQL_BUILD := replace(vSQL_BUILD,'{P',':P');
          vSQL_BUILD := replace(vSQL_BUILD,'{M',':M');
          vSQL_BUILD := replace(vSQL_BUILD,'{C',':C');

          vSB.Activity_id      := rec1.activity_id;
          vSB.Column_Positions := vColumn_Positions;
          vSB.MaxArraySize     := vSB_MaxArraySize;

          If instr(vSource_cols,',') > 0 then
            vSource_cols := Substr(vSource_cols,1,Length(vSource_cols)-1);
          End if;

          -- Optimizer Hint
          If rec1.optimizer_hint is not null then
            vSelect_statement := 'Select '||rec1.optimizer_hint||' '||vSource_Cols||' '||vSQL_BUILD;
          Else
            vSelect_statement := 'Select '||vSource_Cols||' '||vSQL_BUILD;
          End If;

          -- 02.06.01
          vSB.C0000_Pointers := Null;
          vSB.C0000_Highest  := 0;
          vSB.D0000_Pointers := Null;
          vSB.D0000_Highest  := 0;

          -- Internal Array pointers
          vSB.I1009_Pointers := Null;
          vSB.I1009_Highest  := 0;

          If vColumn_Count > 0 then

            wa := 0;
            For pa in 1..vMax_Dynamic
            Loop
              If instr(vSelect_Statement,':C'||to_char(pa+1000)) > 0 then
                ca := ca + 1;
                vSB.C0000_Pointers := vSB.C0000_Pointers || lpad(pa+1000,4,0);
                If vSB.C0000_Highest < ca then
                 vSB.C0000_Highest := ca;
                End If;
              End If;
              If instr(vSelect_Statement,':D'||to_char(pa+1000)) > 0 then
                wa := wa + 1;
                vSB.D0000_Pointers := vSB.D0000_Pointers || lpad(pa+1000,4,0);
                If vSB.D0000_Highest < wa then
                 vSB.D0000_Highest := wa;
                End If;
              End If;
              If instr(vSelect_Statement,':D'||to_char(pa+2000)) > 0 then
                wa := wa + 1;
                vSB.D0000_Pointers := vSB.D0000_Pointers || lpad(pa+2000,4,0);
                If vSB.D0000_Highest < wa then
                 vSB.D0000_Highest := wa;
                End If;
              End If;
              If instr(vSelect_Statement,':D'||to_char(pa+3000)) > 0 then
                wa := wa + 1;
                vSB.D0000_Pointers := vSB.D0000_Pointers || lpad(pa+3000,4,0);
                If vSB.D0000_Highest < wa then
                 vSB.D0000_Highest := wa;
                End If;
              End If;
              If instr(vSelect_Statement,':D'||to_char(pa+4000)) > 0 then
                wa := wa + 1;
                vSB.D0000_Pointers := vSB.D0000_Pointers || lpad(pa+4000,4,0);
                If vSB.D0000_Highest < wa then
                 vSB.D0000_Highest := wa;
                End If;
              End If;

            End loop;

            wa := 0;
            For pa in 1..vMax_Internal
            Loop
              If instr(vSelect_Statement,':P'||to_char(pa+9000)) > 0 then
                wa := wa + 1;
                vSB.I1009_Pointers := vSB.I1009_Pointers || lpad(pa+9000,4,0);
                If vSB.I1009_Highest < wa then
                  vSB.I1009_Highest := wa;
                End If;
              End If;
            End loop;

            vSelect_Statement := rtrim(vSelect_Statement);

            -- Transalate Preview Mode Tables
            If vPMode IN ('Y','J') then

              vSelect_Statement := Replace(vSelect_Statement,'XXCP_TRANSACTION_CACHE','XXCP_PV_TRANSACTION_CACHE_V');

              vSelect_Statement := Replace(vSelect_Statement,'XXCP_TRANSACTION_ATTRIBUTES_V','XXCP_PV_TRANSACTION_ATTRIB_V');
              vSelect_Statement := Replace(vSelect_Statement,'XXCP_TRANSACTION_HEADER_V','XXCP_PV_TRANSACTION_HEADER_V');

              vSelect_Statement := Replace(vSelect_Statement,'XXCP_PROCESS_HISTORY','XXCP_PV_PROCESS_HISTORY_V');
              vSelect_Statement := Replace(vSelect_Statement,'XXCP_TRANSACTION_HEADER','XXCP_PV_TRANSACTION_HEADER_V');
              vSelect_Statement := Replace(vSelect_Statement,'XXCP_PROCESS_HISTORY','XXCP_PV_PROCESS_HISTORY_V');
              vSelect_Statement := Replace(vSelect_Statement,'XXCP_VALIDATION_RESULTS','XXCP_PV_VALIDATION_RESULTS_V');
              --
              vSelect_Statement := Replace(vSelect_Statement,'xxcp_transaction_cache','XXCP_PV_TRANSACTION_CACHE_V');
              vSelect_Statement := Replace(vSelect_Statement,'xxcp_transaction_attributes','XXCP_PV_TRANSACTION_ATTRIB_V');
              vSelect_Statement := Replace(vSelect_Statement,'xxcp_transaction_header','XXCP_PV_TRANSACTION_HEADER_V');
              vSelect_Statement := Replace(vSelect_Statement,'xxcp_process_history','XXCP_PV_PROCESS_HISTORY_V');
              vSelect_Statement := Replace(vSelect_Statement,'xxcp_transaction_header','XXCP_PV_TRANSACTION_HEADER_V');
              vSelect_Statement := Replace(vSelect_Statement,'xxcp_process_history','XXCP_PV_PROCESS_HISTORY_V');
              vSelect_Statement := Replace(vSelect_Statement,'xxcp_validation_results','XXCP_PV_VALIDATION_RESULTS_V');

              vSelect_Statement := Replace(vSelect_Statement,'XXCP_TRANSACTION_ATTRIBUTES ','XXCP_PV_TRANSACTION_ATTRIB_V');
              vSelect_Statement := Replace(vSelect_Statement,'XXCP_TRANSACTION_HEADER ','XXCP_PV_TRANSACTION_HEADER_V');

              If vPMode = 'J' then
                vSelect_Statement := Replace(vSelect_Statement,'XXCP_GL_INTERFACE','XXCP_PV_GL_INTERFACE_V');
                vSelect_Statement := Replace(vSelect_Statement,'xxcp_gl_interface','XXCP_PV_GL_INTERFACE_V');
              End If;

            End If;

            -- Sub out variables to short code variables

            vSelect_statement := Replace(vSelect_statement,'{M0}',':M0');
            vSelect_statement := Replace(vSelect_statement,'{M3}',':M3');
            vSelect_statement := Replace(vSelect_statement,'{M7}',':M7');
            vSelect_statement := Replace(vSelect_statement,'{MS}',':MS');
            vSelect_statement := Replace(vSelect_statement,'{MP}',':MP');
            vSelect_statement := Replace(vSelect_statement,'{M6}',':M6');
            vSelect_statement := Replace(vSelect_statement,'{M8}',':M8');
            vSelect_statement := Replace(vSelect_statement,'{MKEY}',':MKEY');
            vSelect_statement := Replace(vSelect_statement,'{M5}',':M5');
            vSelect_statement := Replace(vSelect_statement,'{M9}',':M9');
            vSelect_statement := Replace(vSelect_statement,'{MA}',':MA');

            vSelect_statement := Replace(vSelect_statement,'{CB1}',':CB1');
            vSelect_statement := Replace(vSelect_statement,'{CB2}',':CB2');
            vSelect_statement := Replace(vSelect_statement,'{CB3}',':CB3');
            vSelect_statement := Replace(vSelect_statement,'{CB4}',':CB4');
            vSelect_statement := Replace(vSelect_statement,'{CB5}',':CB5');
            vSelect_statement := Replace(vSelect_statement,'{CB6}',':CB6');
            vSelect_statement := Replace(vSelect_statement,'{CB7}',':CB7');
            vSelect_statement := Replace(vSelect_statement,'{CB8}',':CB8');

            If instr(vSelect_statement,':m0') > 0 then
              vSelect_statement := Replace(vSelect_statement,':m0',':M0');
            End if;

            -- Source Rowid
            If instr(vSelect_statement,':M0') > 0 then
              vSB.Bind_M0 := 'Y';
            Else
              vSB.Bind_M0 := 'N';
            End If;
            -- Transaction Id
            If instr(vSelect_statement,':M3') > 0 then
              vSB.Bind_M3 := 'Y';
            Else
              vSB.Bind_M3 := 'N';
            End If;
            -- Transaction Id
            If instr(vSelect_statement,':M4') > 0 then
              vSB.Bind_M4 := 'Y';
            Else
              vSB.Bind_M4 := 'N';
            End If;
            -- Set of books id
            If instr(vSelect_statement,':M5') > 0 then
              vSB.Bind_M5 := 'Y';
            Else
              vSB.Bind_M5 := 'N';
            End If;
            -- Type
            If instr(vSelect_statement,':M6') > 0 then
              vSB.Bind_M6 := 'Y';
            Else
              vSB.Bind_M6 := 'N';
            End If;
            -- Trading Set
            If instr(vSelect_statement,':M7') > 0 then
              vSB.Bind_M7 := 'Y';
            Else
              vSB.Bind_M7 := 'N';
            End If;
            --
            If instr(vSelect_statement,':M8') > 0 then
              vSB.Bind_M8 := 'Y';
            Else
              vSB.Bind_M8 := 'N';
            End If;

            -- Previous Attribute
            If instr(vSelect_statement,':M9') > 0 then
              vSB.Bind_M9 := 'Y';
            Else
              vSB.Bind_M9 := 'N';
            End If;
            -- Key
            If instr(vSelect_statement,':MKEY') > 0 then
              vSB.Bind_MKEY := 'Y';
            Else
              vSB.Bind_MKEY := 'N';
            End If;
            -- Owner
            If instr(vSelect_statement,':MS') > 0 then
              vSB.Bind_MS := 'Y';
            Else
              vSB.Bind_MS := 'N';
            End If;
            -- Partner
            If instr(vSelect_statement,':MP') > 0 then
              vSB.Bind_MP := 'Y';
            Else
              vSB.Bind_MP := 'N';
            End If;
            If instr(vSelect_statement,':MA') > 0 then
              vSB.Bind_MA := 'Y';
            Else
              vSB.Bind_MA := 'N';
            End If;
            If instr(vSelect_statement,':MSTEP') > 0 then
              vSB.Bind_MSTEP := 'Y';
            Else
              vSB.Bind_MSTEP := 'N';
            End If;

            vCustBindCnt := 0;
              -- CB (Custom Binds)
            If instr(vSelect_statement,':CB1') > 0 then
              vSB.Bind_CB1 := 'Y';
              vCustBindCnt := 1;
            Else
              vSB.Bind_CB1 := 'N';
              vCustBindCnt := 1;
            End If;
            If instr(vSelect_statement,':CB2') > 0 then
              vSB.Bind_CB2 := 'Y';
              vCustBindCnt := 1;
            Else
              vSB.Bind_CB2 := 'N';
            End If;
            If instr(vSelect_statement,':CB3') > 0 then
              vSB.Bind_CB3 := 'Y';
              vCustBindCnt := 1;
            Else
              vSB.Bind_CB3 := 'N';
            End If;
            If instr(vSelect_statement,':CB4') > 0 then
              vSB.Bind_CB4 := 'Y';
              vCustBindCnt := 1;
            Else
              vSB.Bind_CB4 := 'N';
            End If;
            If instr(vSelect_statement,':CB5') > 0 then
              vSB.Bind_CB5 := 'Y';
              vCustBindCnt := 1;
            Else
              vSB.Bind_CB5 := 'N';
            End If;
            If instr(vSelect_statement,':CB5') > 0 then
              vSB.Bind_CB5 := 'Y';
              vCustBindCnt := 1;
            Else
              vSB.Bind_CB5 := 'N';
            End If;
            If instr(vSelect_statement,':CB6') > 0 then
              vSB.Bind_CB6 := 'Y';
              vCustBindCnt := 1;
            Else
              vSB.Bind_CB6 := 'N';
            End If;
            If instr(vSelect_statement,':CB7') > 0 then
              vSB.Bind_CB7 := 'Y';
              vCustBindCnt := 1;
            Else
              vSB.Bind_CB7 := 'N';
            End If;
            If instr(vSelect_statement,':CB8') > 0 then
              vSB.Bind_CB8 := 'Y';
              vCustBindCnt := 1;
            Else
              vSB.Bind_CB8 := 'N';
            End If;
            -- Future Use
            vSB.Bind_CB9  := 'N';
            vSB.Bind_CB10 := 'N';

            If vCustBindCnt > 0 then
              vSB.CustomBinds := 'Y';
            Else
              vSB.CustomBinds := 'N';
            End If;

            vSB.Transaction_Table := Rec1.transaction_table;

            -- Store in Memory

            sb := sb + 1;

          End If;


       -- Delete previous entry
        Begin
          Delete from  XXCP_DYNAMIC_SQL_GEN where sql_build_id = rec1.sql_build_id and preview_mode = vPMode;

          Insert into  XXCP_DYNAMIC_SQL_GEN(
           Source_id
          ,SQL_BUILD_ID
          ,PREVIEW_MODE
          ,ACTIVITY_ID
          ,TRANSACTION_TABLE
          ,SELECT_STATEMENT
          ,COLUMN_COUNT
          ,COLUMN_POSITIONS
          ,VALIDATION
          ,Bind_M0
          ,Bind_M3
          ,Bind_M4
          ,Bind_M5
          ,Bind_M6
          ,Bind_M7
          ,Bind_M8
          ,BIND_M9
          ,Bind_MS
          ,Bind_MP
          ,Bind_Mkey
          ,Bind_MA
          ,Bind_Mstep
          ,CUSTOMBINDS
          ,BIND_CB1
          ,BIND_CB2
          ,BIND_CB3
          ,BIND_CB4
          ,BIND_CB5
          ,BIND_CB6
          ,BIND_CB7
          ,BIND_CB8
          ,BIND_CB9
          ,BIND_CB10
          ,C0000_POINTERS
          ,C0000_HIGHEST
          ,D0000_POINTERS
          ,D0000_HIGHEST
          ,I1009_POINTERS
          ,I1009_HIGHEST
          ,MAXARRAYSIZE
           --
          ,CREATED_BY
          ,CREATION_DATE
          ,LAST_UPDATED_BY
          ,LAST_UPDATE_DATE
          ,LAST_UPDATE_LOGIN
          )
          Values(
           cSource_id,
           vSB.Sql_Build_Id,
           vPMode,
           vSB.Activity_Id,
           vSB.Transaction_Table,
           vSelect_statement,
           vColumn_Count,
           vsb.column_positions,
           vValidationStr,
           vSB.Bind_M0
          ,vSB.Bind_M3
          ,vSB.Bind_M4
          ,vSB.Bind_M5
          ,vSB.Bind_M6
          ,vSB.Bind_M7
          ,vSB.Bind_M8
          ,vSB.BIND_M9
          ,vSB.Bind_MS
          ,vSB.Bind_MP
          ,vSB.Bind_Mkey
          ,vSB.Bind_MA
          ,vSB.Bind_Mstep
          ,vSB.Custombinds
          ,vSB.BIND_CB1
          ,vSB.BIND_CB2
          ,vSB.BIND_CB3
          ,vSB.BIND_CB4
          ,vSB.BIND_CB5
          ,vSB.BIND_CB6
          ,vSB.BIND_CB7
          ,vSB.BIND_CB8
          ,vSB.BIND_CB9
          ,vSB.BIND_CB10,
           vSB.C0000_Pointers,
           vSB.C0000_Highest,
           vSB.D0000_Pointers,
           vSB.D0000_Highest,
           vSB.I1009_Pointers,
           vSB.I1009_Highest,
           vSB.Maxarraysize,
           fnd_global.USER_ID,
           sysdate,
           fnd_global.USER_ID,
           sysdate,
           fnd_global.LOGIN_ID
          );

            Commit;

            -- Successful
            cInternalErrorCode := 0;

            Exception when others then
               cInternalErrorCode := 13050;  -- DI generation failed.
               Rollback;
          End;
        End Loop;
    End Loop;

  End MakeSingleSQLBuildStatement;


  -- !!
  -- !! LOADSQLBUILDSTATEMENTS
  -- !!
  PROCEDURE LoadSQLBuildStatements(cSource_id in number, cClearOnly in varchar2, cPreview_Mode in varchar2)  is

    Cursor DIB(pSource_id in number, pPreview_Mode in varchar2) is
     select w.SQL_BUILD_ID
           ,ACTIVITY_ID
           ,TRANSACTION_TABLE
           ,SELECT_STATEMENT
           ,COLUMN_COUNT
           ,COLUMN_POSITIONS
           ,VALIDATION
           ,BIND_M0
           ,BIND_M3
           ,BIND_M4
           ,BIND_M5
           ,BIND_M6
           ,BIND_M7
           ,BIND_M8
           ,BIND_M9
           ,BIND_MS
           ,BIND_MP
           ,BIND_MKEY
           ,BIND_MA
           ,BIND_MSTEP
           ,CUSTOMBINDS
           ,BIND_CB1
           ,BIND_CB2
           ,BIND_CB3
           ,BIND_CB4
           ,BIND_CB5
           ,BIND_CB6
           ,BIND_CB7
           ,BIND_CB8
           ,BIND_CB9
           ,BIND_CB10
           ,C0000_POINTERS
           ,C0000_HIGHEST
           ,D0000_POINTERS
           ,D0000_HIGHEST
           ,I1009_POINTERS
           ,I1009_HIGHEST
           ,MAXARRAYSIZE
     from xxcp_dynamic_sql_gen w
     where w.source_id  IN (pSource_id,40)
       and preview_mode = pPreview_Mode;

    wa                 pls_integer := 0;
    ca                 pls_integer := 0;
    sb                 pls_integer := 0;

    vPreview_Mode      varchar2(1);

    Begin

      xxcp_global.gSB.Delete;
      xxcp_global.set_source_id(cSource_id);

      vPreview_Mode := cPreview_Mode;

      If xxcp_global.Journal_Preview_on = 'Y' and xxcp_global.Preview_on = 'Y'  then
        vPreview_mode := 'J';
      End If;

      If cClearOnly = 'N' then
        For rec1 in dib(pSource_id => cSource_id, pPreview_mode => vPreview_Mode)
        Loop

          xxcp_global.gSB(sb+1).Activity_id      := rec1.activity_id;
          xxcp_global.gSB(sb+1).Column_Positions := rec1.column_positions;
          xxcp_global.gSB(sb+1).MaxArraySize     := rec1.maxarraysize;

          -- 02.06.01
          xxcp_global.gSB(sb+1).C0000_Pointers := rec1.c0000_pointers;
          xxcp_global.gSB(sb+1).C0000_Highest  := rec1.c0000_highest;
          xxcp_global.gSB(sb+1).D0000_Pointers := rec1.d0000_pointers;
          xxcp_global.gSB(sb+1).D0000_Highest  := rec1.d0000_highest;

          -- Internal Array pointers
          xxcp_global.gSB(sb+1).I1009_Pointers := rec1.i1009_pointers;
          xxcp_global.gSB(sb+1).I1009_Highest  := rec1.i1009_highest;

            xxcp_global.gSB(sb+1).Bind_M0 := rec1.bind_m0;
            xxcp_global.gSB(sb+1).Bind_M3 := rec1.bind_m3;
            xxcp_global.gSB(sb+1).Bind_M5 := rec1.bind_m5;
            xxcp_global.gSB(sb+1).Bind_M6 := rec1.bind_m6;
            xxcp_global.gSB(sb+1).Bind_M7 := rec1.bind_m7;
            xxcp_global.gSB(sb+1).Bind_M8 := rec1.bind_m8;
            xxcp_global.gSB(sb+1).Bind_M9 := rec1.bind_m9;

            xxcp_global.gSB(sb+1).Bind_MKEY  := rec1.bind_mkey;
            xxcp_global.gSB(sb+1).Bind_MS    := rec1.bind_ms;
            xxcp_global.gSB(sb+1).Bind_MP    := rec1.bind_mp;
            xxcp_global.gSB(sb+1).Bind_MA    := rec1.bind_ma;
            xxcp_global.gSB(sb+1).Bind_MSTEP := rec1.bind_mstep;

            xxcp_global.gSB(sb+1).Bind_CB1 := rec1.bind_cb1;
            xxcp_global.gSB(sb+1).Bind_CB2 := rec1.bind_cb2;
            xxcp_global.gSB(sb+1).Bind_CB3 := rec1.bind_cb3;
            xxcp_global.gSB(sb+1).Bind_CB4 := rec1.bind_cb4;
            xxcp_global.gSB(sb+1).Bind_CB5 := rec1.bind_cb5;
            xxcp_global.gSB(sb+1).Bind_CB6 := rec1.bind_cb6;
            xxcp_global.gSB(sb+1).Bind_CB7 := rec1.bind_cb7;
            xxcp_global.gSB(sb+1).Bind_CB8 := rec1.bind_cb8;
            -- Future Use
            xxcp_global.gSB(sb+1).Bind_CB9  := 'N';
            xxcp_global.gSB(sb+1).Bind_CB10 := 'N';

            xxcp_global.gSB(sb+1).CustomBinds := rec1.custombinds;
            -- Store in Memory
            xxcp_global.gSB(sb+1).SQL_build_id      := rec1.sql_build_id;
            xxcp_global.gSB(sb+1).Transaction_table := rec1.transaction_table;
            xxcp_global.gSB(sb+1).Validation        := rec1.validation;
            xxcp_global.gSB(sb+1).Select_statement  := rec1.select_statement;

            xxcp_global.gSB(sb+1).Select_statement  := dbms_lob.substr(rec1.select_statement,32000,1);
            xxcp_global.gSB(sb+1).column_count      := rec1.column_count;
            sb := sb + 1;

        End Loop;
      End If;

      Get_Cach_Summary_Pointers(cSource_id => cSource_id, cClearOnly => cClearOnly);

      xxcp_global.gSB_cnt := sb;
  End LoadSQLBuildStatements;



  -- !!
  -- !! LoadCurrTolerences
  -- !!
  PROCEDURE LoadCurrTolerences(cSource_id in number, cClearOnly in varchar2)  is

   Cursor ARUC(pSource_id in number) is
       select currency_code, entered_tolerence, accounted_tolerence
         from xxcp_rounding_tolerances
         where source_id = pSource_id;

   TYPE ARUC_t IS
      TABLE OF ARUC%rowtype INDEX BY BINARY_INTEGER;

   ARUCRec  ARUC_T;

   cy pls_integer := 0;
   j  pls_integer;
  Begin

   xxcp_global.gCY.delete;

   If cClearOnly = 'N' then

    Open ARUC(cSource_id);
      Fetch ARUC BULK COLLECT INTO ARUCRec;
    Close ARUC;

    For j in 1..ARUCRec.Count Loop
      cy := cy + 1;
      xxcp_global.gCY(cy).Currency_Code       := ARUCRec(j).Currency_Code;
      xxcp_global.gCY(cy).Entered_Tolerence   := ARUCRec(j).Entered_Tolerence;
      xxcp_global.gCY(cy).Accounted_Tolerence := ARUCRec(j).Accounted_Tolerence;
    End Loop;
   End If;

   xxcp_global.gCY_Cnt := cy;

  End LoadCurrTolerences;

 -- #
 -- #    Clear_Column_Rule_Array
 -- #
 PROCEDURE Clear_Column_Rule_Array is
 Begin
    xxcp_global.gCR.delete;
 End Clear_Column_Rule_Array;



  PROCEDURE ClearDown is
  -- Clear down all memory structures created by xxcp_global.
  Begin

   xxcp_global.gTR_Cnt     := 0;
   xxcp_global.gCURR_Cnt   := 0;
   xxcp_global.gCY_Cnt     := 0;
   xxcp_global.gSR_Cnt     := 0;
   xxcp_global.gEnSegs_Cnt := 0;
   xxcp_global.gSB_Cnt     := 0;
   xxcp_global.gCR_Cnt     := 0;

   xxcp_global.gTRO.delete;
   xxcp_global.gTR.delete;
   xxcp_global.gCURR.delete;
   xxcp_global.gCY.delete;
   xxcp_global.gEnSegs.delete;
   xxcp_global.gSR.delete;
   xxcp_global.gCR.delete;
   xxcp_global.gSB.delete;

  End ClearDown;

  --
  -- Clear Column Rules Array
  --
  PROCEDURE ClearColumnRules is
  Begin
    xxcp_global.gCR.delete;
  End ClearColumnRules;


--- ##
Begin

   xxcp_global.gCSA.delete;
   xxcp_global.gCSA_Cnt := 0;

END XXCP_MEMORY_PACK;
/
