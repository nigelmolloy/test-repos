CREATE OR REPLACE PACKAGE XXCP_CPA_TRUE_UP_ENGINE AS
  /******************************************************************************
                      V I R T A L  T R A D E R  L I M I T E D
        (Copyright 2008-2012 Virtual Trader Ltd. All rights Reserved)

     NAME:       XXCP_CPA_TRUE_UP_ENGINE
     PURPOSE:    Cost Plus True Up Engine.

     Version [03.06.07A] Build Date [10-DEC-2012] Name [XXCP_CPA_TRUE_UP_ENGINE]
     
  ******************************************************************************/

  Function Software_Version RETURN VARCHAR2;

  Function Replan_Control(cSource_Group_id    IN NUMBER,
                          cConc_Request_Id    IN NUMBER,
                          cTable_Group_id     IN NUMBER   DEFAULT 0, 
                          cCalendar_id        IN NUMBER   DEFAULT 0,
                          cPeriod_Name        IN VARCHAR2 DEFAULT Null,
                          cRequest_Id         IN NUMBER   DEFAULT 0,
                          cPrev_Forecast_id   IN NUMBER   DEFAULT 0,
                          cCost_Plus_Set_id   IN NUMBER   DEFAULT NULL) return number;

  Function Control(cSource_Group_id    IN NUMBER,
                   cConc_Request_Id    IN NUMBER,
                   cTable_Group_id     IN NUMBER   DEFAULT 0, 
                   cCalendar_id        IN NUMBER   DEFAULT 0,
                   cPeriod_Name        IN VARCHAR2 DEFAULT Null,
                   cRestart            IN VARCHAR2 DEFAULT 'N',
                   cReplan_Reprocess   IN VARCHAR2 DEFAULT 'N',
                   cRequest_id         IN NUMBER   DEFAULT 0,
                   cPrev_Forecast_id   IN NUMBER   DEFAULT 0,
                   cCost_Plus_Set_id   IN NUMBER   DEFAULT NULL) return number;

  Function Commit_True_Up(cSource_Group_ID  IN NUMBER,
                          cCost_Plus_Set_id IN NUMBER DEFAULT NULL) return Number;


END XXCP_CPA_TRUE_UP_ENGINE;
/
