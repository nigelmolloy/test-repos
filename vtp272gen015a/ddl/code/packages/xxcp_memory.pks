CREATE OR REPLACE PACKAGE XXCP_MEMORY_PACK IS
/******************************************************************************
                        V I R T A L  T R A D E R  L I M I T E D 
            (Copyright 2005-2012 Virtual Trader Ltd. All rights Reserved) 

   NAME:       XXCP_MEMORY_PACK
   PURPOSE:    This package loads the global memory arrays

  Version [03.08.32] Build Date [13-NOV-2012] Name [XXCP_MEMORY_PACK]

******************************************************************************/

 FUNCTION  Software_Version RETURN VARCHAR2;

 PROCEDURE LoadTaxRegOnlyInfo(cClearOnly in varchar2);
 
 PROCEDURE LoadExtraRecordType(cSource_id in number, cClearOnly in varchar2);

 PROCEDURE FindTaxRegOnlyInfo( cTax_Reg_id             in number,
                               cLegal_currency        out varchar2,
                               cTrade_Currency        out varchar2,
                               cCommon_Exch_Curr      out varchar2,
                               cExch_Rate_type        out varchar2,
                               cAdjustment_Rate_Group out varchar2,
                               cTax_Reg_Short_Name    out varchar2
                               );
  
 -- New                                
 PROCEDURE Get_Cach_Summary_Pointers(cSource_id in number, cClearOnly in varchar2 );                             

 PROCEDURE LoadTaxRegInfo(cClearOnly in varchar2);

 PROCEDURE LoadTaxRegInfoNoTarget(cClearOnly in varchar2);

 PROCEDURE LoadCurrencies(cClearOnly in varchar2);

 PROCEDURE LoadEnabledSegments(cClearOnly in varchar2, cSource_id in number);

 PROCEDURE LoadSegmentRules(cSource_id in number, cClearOnly in varchar2);
 
 PROCEDURE LoadIgnoreClass(cClearOnly in varchar2,cSource_id in number);

 PROCEDURE LoadColumnRules(cSource_id               in number
                          ,cSource_Table            in varchar2
                          ,cSource_Instance_id      in number
                          ,cTarget_Table            in varchar2
                          ,cRequest_id              in number
                          ,cInternalErrorCode in out number);

 PROCEDURE LoadPassThroughRules(
                           cSource_id               in number
                          ,cSource_Table            in varchar2
                          ,cSource_Instance_id      in number
                          ,cTarget_Table            in varchar2
                          ,cRequest_id              in number
                          ,cPassThroughRules        in varchar2
                          ,cInternalErrorCode in out number);

 FUNCTION Get_Dynamic_Set_Attribute(cSql_build_id in number, cColumn_id in number, cActivity_id in number, cSequence out number) return varchar2;
 
 PROCEDURE MakeSQLBuildStatements(cSource_id in number, cInternalErrorCode in out number); 

 PROCEDURE MakeSingleSQLBuildStatement(cSource_id in number, cSQL_Build_Id number, cInternalErrorCode in out number); 

 PROCEDURE LoadSQLBuildStatements(cSource_id in number, cClearOnly in varchar2, cPreview_Mode varchar2);

 PROCEDURE LoadCurrTolerences(cSource_id in number, cClearOnly in varchar2);
 
 PROCEDURE ClearColumnRules;

 PROCEDURE Clear_Column_Rule_Array;

 PROCEDURE ClearDown;


--
--
--
END XXCP_MEMORY_PACK;
/
