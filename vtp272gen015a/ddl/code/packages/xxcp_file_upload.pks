CREATE OR REPLACE PACKAGE XXCP_FILE_UPLOAD AUTHID CURRENT_USER AS
  /******************************************************************************
      V I R T A L  T R A D E R  L I M I T E D 
         (Copyright 2001-2012 Virtual Trader Ltd. All rights Reserved)

     NAME:  XXCP_FOUNDATION
     PURPOSE:    This script installs the foundation packages and functions

     Version [03.07.05] Build Date [07-JAN-2013] NAME [XXCP_FILE_UPLOAD]
     
  ******************************************************************************/

  Function Software_Version return varchar2;

  -- UploadCompleteMessage
  --
  --    Displays file upload compelte message.
  --

  PROCEDURE UploadCompleteMessage(cFile	    VARCHAR2
                                 ,cAccessId	NUMBER
															   ,cSourceId NUMBER
															   ,cSourceAssignmentID NUMBER);

  -- CancelProcess
  --    Handles upload cancel situations.
  --

  PROCEDURE CancelProcess;

  FUNCTION isValidFile(cFileId number) RETURN NUMBER;

  PROCEDURE parseExcelXML(cFileId             IN NUMBER,
                          cSourceID           IN NUMBER,
												  cSourceAssignmentID IN NUMBER,
                          cMapping_set_id     IN NUMBER default null);


  --
  -- GFM Support
  --
  PROCEDURE DisplayLOADForm(access_id            IN NUMBER
                           ,l_server_url          IN VARCHAR2
												   ,cSource_Assignment_id IN NUMBER);
                           
  FUNCTION loadCSV(cSource_id IN NUMBER   default null,
                   cDebug IN VARCHAR2 default null) RETURN NUMBER;
  
  -- 02.06.05
  Function exportFile(cSource_id IN NUMBER   default null,
                    cDebug       IN VARCHAR2 default null,
                    cRequest_id  IN NUMBER   default null) return number;       
                    
  Function isFileTypeValid(cFile_name in varchar2) return boolean;
  
  -- 03.07.03
  Function insertStagedFlatFile(cStaged_flat_file_rec xxcp_staged_flat_file%rowtype, 
                                cErr_msg out varchar2) return number;
                                
  -- 03.07.03
  function updateStagedFlatFile(cColumn_name  in varchar2,
                                cColumn_value in varchar2,
                                cRequest_id   in number,
                                cFile_type_id in number,
                                cInstance_id  in number,
                                cQualifier1   in varchar2,
                                cQualifier2   in varchar2,
                                cQualifier3   in varchar2,
                                cQualifier4   in varchar2,
                                cErr_msg out varchar2) return number;   

END XXCP_FILE_UPLOAD;
/
