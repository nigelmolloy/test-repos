-- ***********************************************************************************
-- VTML Copyright 2012. Virtual Trader Ltd. All rights reserved.
-- ***********************************************************************************
-- "$Revision: 1 $ $Modtime: 02/05/12 00:00 $  $Workfile: vt_packages_12.sql $"
-- ***********************************************************************************
set pagesize 1500
set linesize 200


Prompt > Installing VT R12 Specific Packages into APPS Schema (Patch 272.1)

Select to_char(sysdate,'DD-MON-YYYY HH24:MI') "Install Time" from dual;

prompt > Package specifications



prompt > Package bodies



Prompt > See details see log : vt_packages_12.sql.log

Prompt > Type Exit to move onto the next script.
