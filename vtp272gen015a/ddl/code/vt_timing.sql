-- ***********************************************************************************
-- Copyright 2010. Virtual Trader Ltd. All rights reserved.
-- ***********************************************************************************
-- "$Revision: 1 $ $Modtime: 22/11/10 00:00 $  $Workfile: vt_timing.sql $"
-- ***********************************************************************************
set head off;
set feedback off;

select '***** TIME --> '||to_char(sysdate,'YYYY MM DD HH:MI:SS')||chr(10) curr_time from dual;

set feedback on;
