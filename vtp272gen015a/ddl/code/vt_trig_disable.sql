-- ***********************************************************************************
-- VT Copyright 2011 Virtual Trader Ltd. All Rights Reserved.
-- ***********************************************************************************

Prompt >
Prompt > VT Patch: vt_trig_disable.sql for release VT272 upgrade.
Prompt >
Prompt > The script should be run as the APPS  user
Prompt >

set define off

Prompt > Disable Audit Triggers
@./ddl/code/vt_timing.sql
prompt
exec xxcp_audit_triggers.Disable_Enable_Triggers(cSwitch_on => FALSE);

@./ddl/code/vt_timing.sql
prompt

Prompt > Type Exit to move onto the next script.
