/*===============================================================================================+
|                                Virtual Trader Re-Generation                        Version 1.08
+================================================================================================+
|
|    File Name:         XXCPREGEN.sql
|    Description:       This script will re-generate all Instance Views, Perform Column Reset,
|                       Run Metadata Sequences, Metadata Sequences, re-generate Custom Utils
|                       and column rules.
|
|    Revision History:
|
|    Version   Date      Name              Revision Description
|    =======   ========= ===============   =======================================================
|      1.00    15/JUL/08 Simon Swallow     First Release
|      1.01    14/JUL/09 Dave Howells      Allows continuation if a stage fails, but sets Conc Req
|                                          to Warning. Stripped out GOTOs.
|      1.02    06/SEP/09 Simon Swallow     Replace dbms_output with FndWriteLog.
|      1.03    22/OCT/09 Simon Swallow     Add Gen_Formula call.
|      1.04    21/APR/11 Nigel Molloy      Add check_Invalid_xml_chars.
|      1.05    30/SEP/11 Nigel Molloy	     Disable/Enable audit triggers.
|      1.06    14/MAY/12 Nigel Molloy	     Added gen_Alias_views.
|      1.07    12/JUL/12 Mat Spring        Added call for Data Collectors
|      1.08    12/JUL/12 Simon Swallow     Added call for SLA Dynamic Views
+================================================================================================*/

SET termout OFF
clear breaks

SET pagesize 0
SET linesize 100
SET heading ON
SET feedback OFF
SET verify OFF
SET Serveroutput on size 1000000 for wra

DECLARE
  vReturn         Boolean := True;
  vErrList        Varchar2(1000) := '';
  vSQLERRM        Varchar2(512);
  vMessage_level  Varchar2(2500);  
  
BEGIN

  xxcp_audit_triggers.disable_audit(cuser_id => fnd_global.user_id);

  -- Clear Log
  delete from XXCP_pv_errors
  where preview_id = xxcp_global.user_id
  and   internal_error_code = 500;

  -- Display Header
  xxcp_foundation.FndWriteLog('***************** VIRTAL TRADER LTD. *******************');
  xxcp_foundation.FndWriteLog('(Copyright 2011 Virtual Trader Ltd. All rights Reserved)');
  xxcp_foundation.FndWriteLog('********************************************************');
  
  xxcp_foundation.FndWriteLog(chr(10)||'VT Re-generation started...');
  
  xxcp_foundation.FndWriteLog(chr(10)||'*** Generate Instance Views ***');
  if not xxcp_regenerate.gen_instance_views then
    xxcp_foundation.FndWriteLog(chr(10)||'*******************************************************');
    xxcp_foundation.FndWriteLog('Warning. Generate Instance Views failed.');
    xxcp_foundation.FndWriteLog('*******************************************************');
    vErrList := vErrList||'Generate Instance Views'||CHR(10);
    vReturn := False;
  end if;
  
  -- 1.06 Alias views need to be generated before Column Reset
  xxcp_foundation.FndWriteLog(chr(10)||'*** Generate Alias Views ***');
  if not xxcp_regenerate.gen_alias_views then
    xxcp_foundation.FndWriteLog(chr(10)||'********************************************');
    xxcp_foundation.FndWriteLog('Warning. Generate Alias Views failed.');
    xxcp_foundation.FndWriteLog('********************************************');
    vErrList := vErrList||'Alias Views'||CHR(10);
    vReturn := False;
  end if;    

  xxcp_foundation.FndWriteLog(chr(10)||'*** Column Reset ***');
  if not xxcp_regenerate.column_reset then
    xxcp_foundation.FndWriteLog(chr(10)||'********************************************');
    xxcp_foundation.FndWriteLog('Warning. Column Reset failed.');
    xxcp_foundation.FndWriteLog('********************************************');
    vErrList := vErrList||'Column Reset'||CHR(10);
    vReturn := False;
  end if;

  execute immediate('alter package XXCP_SEQUENCE_CTL compile body');

  xxcp_foundation.FndWriteLog(chr(10)||'*** Metadata Sequence Reset ***');
  if not xxcp_regenerate.sequence_reset then
    xxcp_foundation.FndWriteLog(chr(10)||'*******************************************************');
    xxcp_foundation.FndWriteLog('Warning. Metadata Sequence failed.');
    xxcp_foundation.FndWriteLog('*******************************************************');
    vErrList := vErrList||'Metadata Sequence Reset'||CHR(10);
    vReturn := False;
  end if;

  xxcp_foundation.FndWriteLog(chr(10)||'*** Generate Custom Utils ***');
  if not xxcp_regenerate.gen_custom_utils then
    xxcp_foundation.FndWriteLog(chr(10)||'*****************************************************');
    xxcp_foundation.FndWriteLog('Warning. Generate Custom Utils failed.');
    xxcp_foundation.FndWriteLog('*****************************************************');
    vErrList := vErrList||'Generate Custom Utils'||CHR(10);
    vReturn := False;
  end if;

  xxcp_foundation.FndWriteLog(chr(10)||'*** Generate Custom Events ***');
  if not xxcp_regenerate.gen_custom_events then
    xxcp_foundation.FndWriteLog(chr(10)||'******************************************************');
    xxcp_foundation.FndWriteLog('Warning. Generate Custom Events failed.');
    xxcp_foundation.FndWriteLog('******************************************************');
    vErrList := vErrList||'Generate Custom Events'||CHR(10);
    vReturn := False;
  end if;

  xxcp_foundation.FndWriteLog(chr(10)||'*** Generate Formulas ***');
  if not xxcp_regenerate.gen_formula then
    xxcp_foundation.FndWriteLog(chr(10)||'*****************************************************');
    xxcp_foundation.FndWriteLog('Warning. Generate Formulas failed.');
    xxcp_foundation.FndWriteLog('*****************************************************');
    vErrList := vErrList||'Generate Formulas'||CHR(10);
    vReturn := False;
  end if;

  xxcp_foundation.FndWriteLog(chr(10)||'*** Generate Column Rules ***');
  if not xxcp_regenerate.gen_column_rules then
    xxcp_foundation.FndWriteLog(chr(10)||'*****************************************************');
    xxcp_foundation.FndWriteLog('Warning. Generate Column Rules failed.');
    xxcp_foundation.FndWriteLog('*****************************************************');
    vErrList := vErrList||'Generate Column Rules'||CHR(10);
    vReturn := False;
  end if;
  
  xxcp_foundation.FndWriteLog(chr(10)||'*** Generate Dynamic Interfaces ***');
  if not xxcp_regenerate.Gen_Dynamic_Interfaces then
    xxcp_foundation.FndWriteLog(chr(10)||'*****************************************************');
    xxcp_foundation.FndWriteLog('Warning. Generate Dynamic Interfaces failed.');
    xxcp_foundation.FndWriteLog('*****************************************************');
    vErrList := vErrList||'Generate Dynamic Interfaces'||CHR(10);
    vReturn := False;
  end if;

  xxcp_foundation.FndWriteLog(chr(10)||'*** Generate Trigger Events ***');
  if not xxcp_regenerate.Gen_Trigger_Events then
    xxcp_foundation.FndWriteLog(chr(10)||'*****************************************************');
    xxcp_foundation.FndWriteLog('Warning. Generate Trigger Events failed.');
    xxcp_foundation.FndWriteLog('*****************************************************');
    vErrList := vErrList||'Generate Trigger Events'||CHR(10);
    vReturn := False;
  end if;
  
  xxcp_foundation.FndWriteLog(chr(10)||'*** Generate Data Collectors ***');
  if xxcp_regenerate.Gen_All_Data_Collectors(NULL, NULL, vSQLERRM, vMessage_level ) <> 0 THEN
     xxcp_foundation.FndWriteLog(chr(10)||'*****************************************************');
     xxcp_foundation.FndWriteLog('Warning. Generate Data Collectors failed.');
     xxcp_foundation.FndWriteLog('*****************************************************');
     vErrList := vErrList||'Generate Data Collectors'||CHR(10);
     vReturn := False;
  end if;  

  xxcp_foundation.FndWriteLog(chr(10)||'*** Sync Direct Tax Registrations ***');
  if not xxcp_regenerate.direct_tax_registrations then
    xxcp_foundation.FndWriteLog(chr(10)||'********************************************');
    xxcp_foundation.FndWriteLog('Warning. Sync Direct Tax Registrations failed.');
    xxcp_foundation.FndWriteLog('********************************************');
    vErrList := vErrList||'Column Reset'||CHR(10);
    vReturn := False;
  end if;
  
  xxcp_foundation.FndWriteLog(chr(10)||'*** Update Engine Names ***');
  if not xxcp_regenerate.Update_Engine_Names then
    xxcp_foundation.FndWriteLog(chr(10)||'********************************************');
    xxcp_foundation.FndWriteLog('Warning. Update Engine Names failed.');
    xxcp_foundation.FndWriteLog('********************************************');
    vErrList := vErrList||'Column Reset'||CHR(10);
    vReturn := False;
  end if;  

  -- 1.04 
  xxcp_foundation.FndWriteLog(chr(10)||'*** Check Invalid XML Characters ***');
  if not xxcp_regenerate.Check_Invalid_Xml_Chars then
    xxcp_foundation.FndWriteLog(chr(10)||'********************************************');
    xxcp_foundation.FndWriteLog('Warning. Invalid XML Characters found.');
    xxcp_foundation.FndWriteLog('********************************************');
    vErrList := vErrList||'Invalid XML Characters'||CHR(10);
    vReturn := False;
  end if;

  xxcp_foundation.FndWriteLog(chr(10)||'*** Generate SLA Dynamic Views ***');
  if not xxcp_regenerate.Gen_SLA_views then
    xxcp_foundation.FndWriteLog(chr(10)||'********************************************');
    xxcp_foundation.FndWriteLog('Warning. Generate SLA Dynamic Views failed.');
    xxcp_foundation.FndWriteLog('********************************************');
    vErrList := vErrList||'Generate SLA Dynamic Views'||CHR(10);
    vReturn := False;
  end if;

  xxcp_foundation.FndWriteLog(chr(10)||'*** Generate VT Objects (Views, Packages and Triggers) ***');
  if not xxcp_regenerate.Compile_All('Y')then
    xxcp_foundation.FndWriteLog(chr(10)||'***************************************************');
    xxcp_foundation.FndWriteLog('Warning. Generate VT Objects failed.');
    xxcp_foundation.FndWriteLog('***************************************************');
    vErrList := vErrList||'Generate VT Objects'||CHR(10);
    vReturn := False;
  end if;

  If vReturn = True Then
    xxcp_foundation.FndWriteLog(Chr(10)||'VT Re-generation Complete!');
    xxcp_foundation.FndWriteLog('********************************************************');
  Else
    xxcp_foundation.FndWriteLog(Chr(10)||'VT Re-generation Completed with errors!');
    xxcp_foundation.FndWriteLog('********************************************************');
    vReturn := FND_CONCURRENT.SET_COMPLETION_STATUS('WARNING', 'VT Regeneration completed with errors in...'||CHR(10)||vErrList);
  End If;
  
  xxcp_audit_triggers.enable_audit(cuser_id => fnd_global.user_id);

End;
/

select error_message
from XXCP_pv_errors
where preview_id = xxcp_global.get_user_id
and   internal_error_code = 500
order by error_id
/
